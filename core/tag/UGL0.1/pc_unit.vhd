-------------------------------------------------------------------------------
-- pc_unit        : プログラムカウンタ制御器
-- LastUpdate: 2011/10/14 15:31 
-- Author    : ultraredrays
-- Email     : ultraredrays@gmail.com
-- Todo      : すべて
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity pc_unit is
  
  port (
    clk       : in  std_logic;
    Pc        : out std_logic_vector(31 downto 0);
    PcBranch  : in  std_logic_vector(31 downto 0);
    PcJump    : in  std_logic_vector(25 downto 0);
    PcJumpr   : in  std_logic_vector(31 downto 0);
    Branch    : in  std_logic;
    Jump      : in  std_logic;
    Jumpr     : in  std_logic;
    IO_Wait   : in  std_logic);

end pc_unit;

architecture archi of pc_unit is

  signal pctmp  : std_logic_vector(31 downto 0) := x"00000000";
  signal pcnext : std_logic_vector(31 downto 0) := x"00000000";

begin  -- archi

  Pc <= pctmp;
  pcnext <= pctmp when IO_Wait = '1' else
            PcJumpr when Jumpr = '1' else
            pctmp(31 downto 26) & PcJump   when Jump = '1' else
            PcBranch + pctmp + x"00000001" when Branch = '1' else
            pctmp + x"00000001";
  
  renew_pc : process(clk)
  begin 
    if rising_edge(clk) then
      pctmp <= pcnext;
    end if;
  end process;

end archi;
