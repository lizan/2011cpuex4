core係用作業ディレクトリ

UGL0の要求仕様
-単一クロック
-fib関数の実装
-1Byte入力に対し、そのfibを返す(1Byte)
-メモリサイズ
--命令メモリ = BlockRom 1024Byte
--データメモリ = BlockRam 1024Byte
-サポート命令
--UGL0 Instruction Set 参照

使い方説明
-iseで論理合成してimpactで送信
-ハードウェア演習第四回プログラムなど送信される文字列を受け取る準備をする
-cat **.bin > /dev/ttyUSB0 などでbit列を送信する
-送信されたデータ1Byteにつきそのfibを返してくれば成功

What's new
-2011/10/12 trunkの作成
-2011/11/15 完成
