-------------------------------------------------------------------------------
-- UGL0 top
-- LastUpdate: 2011/10/14 14:13 
-- Author    : ultraredrays
-- Email     : ultraredrays@gmail.com
-- Todo      : ramの論理合成が終わらない
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity top is
  Port ( MCLK1 : in  STD_LOGIC;
         RS_TX : out  STD_LOGIC;
         RS_RX : in  STD_LOGIC );
end top;

architecture example of top is

  component alu
    port (
      A     : in  std_logic_vector(31 downto 0);  -- input data
      B     : in  std_logic_vector(31 downto 0);  -- input data
      code  : in  std_logic_vector( 3 downto 0);  -- alu制御入力
      O     : out std_logic_vector(31 downto 0);  -- output data
      Z     : out std_logic);
  end component;

  component alu_controller port (
    ALUOp : in   std_logic_vector(1 downto 0);  -- ALUOp信号 from controller
    funct : in   std_logic_vector(5 downto 0);  -- funct from OperationMemory
    code  : out  std_logic_vector(3 downto 0));  -- code for ALU
  end component;

  component controller
      port (
    -- 命令コード (31 downto 26)
    Opcode   : in  std_logic_vector(5 downto 0);
    -- Jump命令用制御信号         0 -> PC=PC+4,    1 -> PC=Im
    Jump     : out std_logic;
    -- Jumpr命令用制御信号         0 -> PC=PC+4,    1 -> PC=Im
    Jumpr    : out std_logic;
    -- レジスタ書き込み先制御信号
    -- 00 -> OP[15-11], 01 -> OP[20-16], 10 -> OP[25-21], 11 -> 31
    RegDst   : out std_logic_vector(1 downto 0);
    -- branch命令用制御信号       0 -> PC=PC+4,    1 -> PC=PC+4+Im
    Branch   : out std_logic;
    -- 読み出しデータ出力制御信号 0 -> NOP,        1 -> MDout=M[Addr]
    MemRead  : out std_logic;
    -- 書き込みレジスタ用データ
    -- 00 -> RWD=ALUOut, 01 -> RWD=MDout, 10 -> r_data, 11 -> pc + 1
    MemtoReg : out std_logic_vector(1 downto 0);
    -- ALU制御に送るOpの情報 主にR,I,J形式の情報を送る
    ALUOp    : out std_logic_vector(1 downto 0);
    -- データメモリに書き込む     0 -> NOP,        1 -> M[Addr]=MDin
    MemWrite : out std_logic;
    -- ALUに送る値の情報          0 -> ALUin2=RRD, 1 -> ALUin2=OP[15-0]
    ALUSrc   : out std_logic;
    -- レジスタに書き込む         0 -> NOP,        1 -> RW = RWD 
    RegWrite : out std_logic;
    -- Input命令用制御信号         0 -> PC=PC+4,    1 -> PC=Im
    Recv     : out  std_logic;
    -- Output命令用制御信号         0 -> PC=PC+4,    1 -> PC=Im
    Send     : out std_logic;
    -- Input命令用制御信号
    CanRecv  : in std_logic;
    -- Output命令用制御信号
    CanSend  : in std_logic;
    -- IO待機信号を送信する
    IO_Wait  : out std_logic);                    
  end component;

  component pc_unit
    port (      clk       : in  std_logic;
      Pc        : out std_logic_vector(31 downto 0);
      PcBranch  : in  std_logic_vector(31 downto 0);
      PcJump    : in  std_logic_vector(25 downto 0);
      PcJumpr   : in  std_logic_vector(31 downto 0);
      Branch    : in  std_logic;
      Jump      : in  std_logic;
      Jumpr     : in  std_logic;
      IO_Wait   : in  std_logic);  
  end component;

  component regs
    port (
      clk      : in  std_logic;                      -- クロック 
      R1       : in  std_logic_vector(4 downto 0);   -- 読み出し用レジスタその1
      R2       : in  std_logic_vector(4 downto 0);   -- 読み出し用レジスタその2
      W        : in  std_logic_vector(4 downto 0);   -- 書き込み用レジスタ
      Reg_Data1   : out std_logic_vector(31 downto 0);  -- 読み出しデータその1
      Reg_Data2   : out std_logic_vector(31 downto 0);  -- 読み出しデータその2
      Wdata    : in  std_logic_vector(31 downto 0);  -- 書き込みデータ
      RegWrite : in  std_logic);                     -- レジスタ書き込み制御信号
  end component;

  component ram
    port (
      clk      : in  std_logic;                      -- クロック 
      Addr     : in  std_logic_vector(31 downto 0);
      Rdata    : out std_logic_vector(31 downto 0);
      Wdata    : in  std_logic_vector(31 downto 0);
      R        : in  std_logic;
      W        : in  std_logic);
  end component;

  component rom
    port (
      Addr     : in  std_logic_vector(31 downto 0);
      Rdata    : out std_logic_vector(31 downto 0);
      R        : in  std_logic);
  end component;

  component rs232c
    generic (
      constant wtime : std_logic_vector(15 downto 0) := x"1ABD");
    port (
      -- clk   : アーキテクチャ全体のクロック
      clk       : in  std_logic;
      --S_Byte : 送信する1Byteのデータ 
      S_Byte    : in  std_logic_vector(7 downto 0);
      --R_Byte : 受信した1Byteのデータ
      R_Byte    : out std_logic_vector(7 downto 0);
      --Send   : 送信データ使用可能FLAG シグナルが立ち上がると送信ステートマシンが動作      
      Send      : in  std_logic;
      --Recv   : 受信データ使用可能FLAG 初期状態は0,信号受信中は0,それ以外は1 　　　　　　　　
      Recv      : in std_logic;
      --CanSend : データ送信可能FLAG 
      CanSend   : out std_logic;
      --CanRecv : データ送信可能FLAG
      CanRecv   : out std_logic;
      --Tx     : 送信用信号線
      Tx        : out std_logic;
      --Rx     : 受信用信号線
      Rx        : in  std_logic);
  end component;
  
  signal clk, iclk: std_logic;
  signal op       : std_logic_vector(31 downto 0) := x"00000000";
  signal pc       : std_logic_vector(31 downto 0) := x"FFFFFFFF";
  signal w        : std_logic_vector(4 downto 0) := "00000";
  signal reg_data1   : std_logic_vector(31 downto 0) := x"00000000";
  signal reg_data2   : std_logic_vector(31 downto 0) := x"00000000";
  signal wdata    : std_logic_vector(31 downto 0) := x"00000000";
  signal a        : std_logic_vector(31 downto 0) := x"00000000";
  signal b        : std_logic_vector(31 downto 0) := x"00000000";
  signal code     : std_logic_vector(3 downto 0) := "1111";
  signal o        : std_logic_vector(31 downto 0) := x"00000000";
  signal z        : std_logic := '1';
  signal memdata  : std_logic_vector(31 downto 0) := x"00000000";
  signal j        : std_logic := '0';
  signal jr       : std_logic := '0';
  signal regdst   : std_logic_vector(1 downto 0) := "00";
  signal prebranch: std_logic := '0';
  signal branch   : std_logic := '0';
  signal memread  : std_logic := '0';
  signal memtoreg : std_logic_vector(1 downto 0) := "00";
  signal aluop    : std_logic_vector(1 downto 0) := "11";
  signal memwrite : std_logic := '0';
  signal alusrc   : std_logic := '0';
  signal regwrite : std_logic := '0';
  signal pcbranch : std_logic_vector(31 downto 0) := x"00000000";
  signal pcjump   : std_logic_vector(25 downto 0) := "00000000000000000000000000";
  signal pcjumpr  : std_logic_vector(31 downto 0) := x"00000000";
  signal r_byte   : std_logic_vector(7 downto 0) := "00000000";
  signal s_byte   : std_logic_vector(7 downto 0) := "00000000";
  signal recv     : std_logic := '0';
  signal send     : std_logic := '0';
  signal cansend : std_logic := '1';
  signal canrecv : std_logic := '0';
  signal io_wait  : std_logic := '0';

begin
  ib: IBUFG port map (
    i=>MCLK1,
    o=>iclk);
  bg: BUFG port map (
    i=>iclk,
    o=>clk);
  PC0 : pc_unit port map (
    clk      => clk,
    Pc       => pc,
    PcBranch => pcbranch,
    PcJump   => pcjump,
    Pcjumpr  => pcjumpr,
    Branch   => branch,
    Jump     => j,
    Jumpr    => jr,
    IO_Wait  => io_wait);
  IMem : rom port map (
    Addr  => pc,
    Rdata => op,
    R     => '1');
  
  Controller0 : controller port map (
    Opcode   => op(31 downto 26),
    Jump     => j,
    Jumpr    => jr,
    RegDst   => regdst,
    Branch   => prebranch,
    MemRead  => memread,
    MemtoReg => memtoreg,
    ALUOp    => aluop,
    MemWrite => memwrite,
    ALUSrc   => alusrc,
    RegWrite => regwrite,
    Recv     => recv,
    Send     => send,
    CanSend  => cansend,
    CanRecv  => canrecv,
    IO_Wait  => io_wait);
  
  RegWindow : regs port map (
    clk      => clk,
    R1       => op(25 downto 21),
    R2       => op(20 downto 16),
    W        => w,
    Reg_Data1   => reg_data1,
    Reg_Data2   => reg_data2,
    Wdata    => wdata,
    RegWrite => regwrite);
  ALU0 : alu port map (
    A    => a,
    B    => b,
    code => code,
    O    => o,
    Z    => z);
  ALUController0 : alu_controller port map (
    ALUOp => aluop,
    funct => op(5 downto 0),
    code  => code);
  DMem : ram port map (
    clk   => clk,
    Addr  => o,
    Rdata => memdata,
    Wdata => reg_data2,
    R     => memread,
    W     => memwrite);
  rs232c0 : rs232c port map (
    clk    => clk,
    S_Byte => s_byte,
    R_Byte => r_byte,
    Send   => send,
    Recv   => recv,
    CanSend=> cansend,
    CanRecv=> canrecv,
    Tx     => RS_TX,
    Rx     => RS_RX);

  
  pcbranch <= op(15)&x"0000"&op(14 downto 0) when op(15) = '0' else
              op(15)&x"FFFF"&op(14 downto 0);
  pcjumpr  <= reg_data1;
  pcjump   <= op(25 downto 0);
  branch   <= prebranch and z;
  w <= "11111" when regdst = "11" else
       op(25 downto 21) when regdst = "10" else
       op(20 downto 16) when regdst = "01" else
       op(15 downto 11);
  a <= reg_data1;
  b <= op(15)&x"0000"&op(14 downto 0) when alusrc = '1' and op(15) = '0' else
       op(15)&x"FFFF"&op(14 downto 0) when alusrc = '1' and op(15) = '1' else
       reg_data2;
  wdata <= pc + 1            when memtoreg = "11" else
           x"000000"&r_byte  when memtoreg = "10" else
           memdata           when memtoreg = "01" else
           o;
  s_byte <= o(7 downto 0);
    
  init_machine: process(clk)
  begin
    if rising_edge(clk) then
    end if;
  end process;
  
end example;

