-------------------------------------------------------------------------------
-- alu_controller
-- LastUpdate: 2011/10/14 11:23
-- Author    : ultraredrays
-- Email     : ultraredrays@gmail.com
-- Todo      : 動作確認
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity alu_controller is
  
  port (
    ALUOp : in   std_logic_vector(1 downto 0);  -- ALUOp信号 from controller
    funct : in   std_logic_vector(5 downto 0);  -- funct from OperationMemory
    code  : out  std_logic_vector(3 downto 0));  -- code for ALU

end alu_controller;

architecture archi of alu_controller is

begin  -- archi

  process(ALUOp,funct)
  begin
    case ALUOp is
      when "10" =>
        case funct is
          when "100000" =>
            code <= "0010";             --Add
          when "100010" =>
            code <= "0110";             --Sub
          when "100100" =>
            code <= "0000";             --And
          when "100101" =>
            code <= "0001";             --Or
          when "101010" =>
            code <= "0111";             --Slt
          when "101011" =>
            code <= "0101";             --Seq
          when others =>
            code <= "1111";             --else
        end case;
      when "01" =>
        code <= "0110";                 --Sub
      when "00" =>
        code <= "0010";                 --Add
      when others =>
        code <= "1111";
    end case;
  end process;

end archi;
