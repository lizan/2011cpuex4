--------------------------------------------------------------------------------
-- Copyright (c) 1995-2011 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: O.40d
--  \   \         Application: netgen
--  /   /         Filename: rs232c_synthesis.vhd
-- /___/   /\     Timestamp: Fri Oct 14 12:37:13 2011
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm rs232c -w -dir netgen/synthesis -ofmt vhdl -sim rs232c.ngc rs232c_synthesis.vhd 
-- Device	: xc5vlx50t-2-ff1136
-- Input file	: rs232c.ngc
-- Output file	: /home/ultraredrays/Documents/works/EXCPU/src/2011cpuex4/core/trunk/UGL0/netgen/synthesis/rs232c_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: rs232c
-- Xilinx	: /home/ultraredrays/Xilinx/ISE_DS/ISE/
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity rs232c is
  port (
    clk : in STD_LOGIC := 'X'; 
    s_valid : in STD_LOGIC := 'X'; 
    r_valid : out STD_LOGIC; 
    rx : in STD_LOGIC := 'X'; 
    tx : out STD_LOGIC; 
    s_busy : out STD_LOGIC; 
    r_data : out STD_LOGIC_VECTOR ( 7 downto 0 ); 
    s_data : in STD_LOGIC_VECTOR ( 7 downto 0 ) 
  );
end rs232c;

architecture Structure of rs232c is
  signal Mcount_s_timer_cy_0_rt_1 : STD_LOGIC; 
  signal Mcount_s_timer_eqn_0 : STD_LOGIC; 
  signal Mcount_s_timer_eqn_1 : STD_LOGIC; 
  signal Mcount_s_timer_eqn_10 : STD_LOGIC; 
  signal Mcount_s_timer_eqn_11_19 : STD_LOGIC; 
  signal Mcount_s_timer_eqn_12 : STD_LOGIC; 
  signal Mcount_s_timer_eqn_13 : STD_LOGIC; 
  signal Mcount_s_timer_eqn_14 : STD_LOGIC; 
  signal Mcount_s_timer_eqn_15 : STD_LOGIC; 
  signal Mcount_s_timer_eqn_2 : STD_LOGIC; 
  signal Mcount_s_timer_eqn_3 : STD_LOGIC; 
  signal Mcount_s_timer_eqn_4 : STD_LOGIC; 
  signal Mcount_s_timer_eqn_5 : STD_LOGIC; 
  signal Mcount_s_timer_eqn_6 : STD_LOGIC; 
  signal Mcount_s_timer_eqn_7 : STD_LOGIC; 
  signal Mcount_s_timer_eqn_8 : STD_LOGIC; 
  signal Mcount_s_timer_eqn_9 : STD_LOGIC; 
  signal Msub_r_timer_addsub0000_cy_0_rt_48 : STD_LOGIC; 
  signal N0 : STD_LOGIC; 
  signal N01 : STD_LOGIC; 
  signal N02 : STD_LOGIC; 
  signal N1 : STD_LOGIC; 
  signal N10 : STD_LOGIC; 
  signal N101 : STD_LOGIC; 
  signal N102 : STD_LOGIC; 
  signal N103 : STD_LOGIC; 
  signal N104 : STD_LOGIC; 
  signal N105 : STD_LOGIC; 
  signal N106 : STD_LOGIC; 
  signal N12 : STD_LOGIC; 
  signal N18 : STD_LOGIC; 
  signal N181 : STD_LOGIC; 
  signal N19 : STD_LOGIC; 
  signal N21 : STD_LOGIC; 
  signal N42 : STD_LOGIC; 
  signal N43 : STD_LOGIC; 
  signal N45 : STD_LOGIC; 
  signal N50 : STD_LOGIC; 
  signal N60 : STD_LOGIC; 
  signal N61 : STD_LOGIC; 
  signal N63 : STD_LOGIC; 
  signal N64 : STD_LOGIC; 
  signal N7 : STD_LOGIC; 
  signal N72 : STD_LOGIC; 
  signal N74 : STD_LOGIC; 
  signal N76 : STD_LOGIC; 
  signal N9 : STD_LOGIC; 
  signal N94 : STD_LOGIC; 
  signal N95 : STD_LOGIC; 
  signal N97 : STD_LOGIC; 
  signal N99 : STD_LOGIC; 
  signal clk_BUFGP_128 : STD_LOGIC; 
  signal r_buf_0_not0001_130 : STD_LOGIC; 
  signal r_data_0_146 : STD_LOGIC; 
  signal r_data_1_147 : STD_LOGIC; 
  signal r_data_2_148 : STD_LOGIC; 
  signal r_data_3_149 : STD_LOGIC; 
  signal r_data_4_150 : STD_LOGIC; 
  signal r_data_5_151 : STD_LOGIC; 
  signal r_data_6_152 : STD_LOGIC; 
  signal r_data_7_153 : STD_LOGIC; 
  signal r_data_or0000_inv_154 : STD_LOGIC; 
  signal r_state_and0000 : STD_LOGIC; 
  signal r_state_or0000 : STD_LOGIC; 
  signal r_timer_1_rstpot_173 : STD_LOGIC; 
  signal r_timer_2_rstpot_175 : STD_LOGIC; 
  signal r_timer_4_rstpot_178 : STD_LOGIC; 
  signal r_timer_mux0000_0_Q : STD_LOGIC; 
  signal r_timer_mux0000_10_1 : STD_LOGIC; 
  signal r_timer_mux0000_11_1 : STD_LOGIC; 
  signal r_timer_mux0000_12_Q : STD_LOGIC; 
  signal r_timer_mux0000_14_1 : STD_LOGIC; 
  signal r_timer_mux0000_15_Q : STD_LOGIC; 
  signal r_timer_mux0000_1_Q : STD_LOGIC; 
  signal r_timer_mux0000_2_Q : STD_LOGIC; 
  signal r_timer_mux0000_3_Q : STD_LOGIC; 
  signal r_timer_mux0000_4_Q : STD_LOGIC; 
  signal r_timer_mux0000_5_Q : STD_LOGIC; 
  signal r_timer_mux0000_6_Q : STD_LOGIC; 
  signal r_timer_mux0000_7_Q : STD_LOGIC; 
  signal r_timer_mux0000_8_Q : STD_LOGIC; 
  signal r_timer_mux0000_9_Q : STD_LOGIC; 
  signal r_valid_OBUF_216 : STD_LOGIC; 
  signal rx_IBUF_218 : STD_LOGIC; 
  signal s_buf_not0001 : STD_LOGIC; 
  signal s_busy_OBUF_239 : STD_LOGIC; 
  signal s_data_0_IBUF_248 : STD_LOGIC; 
  signal s_data_1_IBUF_249 : STD_LOGIC; 
  signal s_data_2_IBUF_250 : STD_LOGIC; 
  signal s_data_3_IBUF_251 : STD_LOGIC; 
  signal s_data_4_IBUF_252 : STD_LOGIC; 
  signal s_data_5_IBUF_253 : STD_LOGIC; 
  signal s_data_6_IBUF_254 : STD_LOGIC; 
  signal s_data_7_IBUF_255 : STD_LOGIC; 
  signal s_state_cmp_eq0000104_260 : STD_LOGIC; 
  signal s_state_not0002_inv : STD_LOGIC; 
  signal s_timer_not0001 : STD_LOGIC; 
  signal s_valid_IBUF_284 : STD_LOGIC; 
  signal Mcount_s_timer_cy : STD_LOGIC_VECTOR ( 14 downto 0 ); 
  signal Mcount_s_timer_lut : STD_LOGIC_VECTOR ( 15 downto 1 ); 
  signal Msub_r_timer_addsub0000_cy : STD_LOGIC_VECTOR ( 14 downto 0 ); 
  signal Msub_r_timer_addsub0000_lut : STD_LOGIC_VECTOR ( 15 downto 1 ); 
  signal Result : STD_LOGIC_VECTOR ( 15 downto 0 ); 
  signal r_buf : STD_LOGIC_VECTOR ( 7 downto 0 ); 
  signal r_state : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal r_state_mux0001 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal r_timer : STD_LOGIC_VECTOR ( 15 downto 0 ); 
  signal r_timer_addsub0000 : STD_LOGIC_VECTOR ( 15 downto 0 ); 
  signal s_buf : STD_LOGIC_VECTOR ( 8 downto 0 ); 
  signal s_buf_mux0000 : STD_LOGIC_VECTOR ( 8 downto 0 ); 
  signal s_state : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal s_state_mux0000 : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal s_timer : STD_LOGIC_VECTOR ( 15 downto 0 ); 
begin
  XST_GND : GND
    port map (
      G => N0
    );
  XST_VCC : VCC
    port map (
      P => N1
    );
  s_buf_0 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_buf_not0001,
      D => s_buf_mux0000(0),
      Q => s_buf(0)
    );
  s_buf_1 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_buf_not0001,
      D => s_buf_mux0000(1),
      Q => s_buf(1)
    );
  s_buf_2 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_buf_not0001,
      D => s_buf_mux0000(2),
      Q => s_buf(2)
    );
  s_buf_3 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_buf_not0001,
      D => s_buf_mux0000(3),
      Q => s_buf(3)
    );
  s_buf_4 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_buf_not0001,
      D => s_buf_mux0000(4),
      Q => s_buf(4)
    );
  s_buf_5 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_buf_not0001,
      D => s_buf_mux0000(5),
      Q => s_buf(5)
    );
  s_buf_6 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_buf_not0001,
      D => s_buf_mux0000(6),
      Q => s_buf(6)
    );
  s_buf_7 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_buf_not0001,
      D => s_buf_mux0000(7),
      Q => s_buf(7)
    );
  s_buf_8 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_buf_not0001,
      D => s_buf_mux0000(8),
      Q => s_buf(8)
    );
  s_state_0 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_state_not0002_inv,
      D => s_state_mux0000(3),
      Q => s_state(0)
    );
  s_state_1 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_state_not0002_inv,
      D => s_state_mux0000(2),
      Q => s_state(1)
    );
  s_state_2 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_state_not0002_inv,
      D => s_state_mux0000(1),
      Q => s_state(2)
    );
  s_state_3 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_state_not0002_inv,
      D => s_state_mux0000(0),
      Q => s_state(3)
    );
  r_state_0 : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      D => r_state_mux0001(3),
      Q => r_state(0)
    );
  r_state_1 : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      D => r_state_mux0001(2),
      Q => r_state(1)
    );
  r_state_2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      D => r_state_mux0001(1),
      Q => r_state(2)
    );
  r_state_3 : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      D => r_state_mux0001(0),
      Q => r_state(3)
    );
  r_timer_0 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      D => r_timer_mux0000_15_Q,
      Q => r_timer(0)
    );
  r_timer_3 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      D => r_timer_mux0000_12_Q,
      Q => r_timer(3)
    );
  r_timer_6 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      D => r_timer_mux0000_9_Q,
      Q => r_timer(6)
    );
  r_timer_7 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      D => r_timer_mux0000_8_Q,
      Q => r_timer(7)
    );
  r_timer_8 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      D => r_timer_mux0000_7_Q,
      Q => r_timer(8)
    );
  r_timer_9 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      D => r_timer_mux0000_6_Q,
      Q => r_timer(9)
    );
  r_timer_10 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      D => r_timer_mux0000_5_Q,
      Q => r_timer(10)
    );
  r_timer_11 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      D => r_timer_mux0000_4_Q,
      Q => r_timer(11)
    );
  r_timer_12 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      D => r_timer_mux0000_3_Q,
      Q => r_timer(12)
    );
  r_timer_13 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      D => r_timer_mux0000_2_Q,
      Q => r_timer(13)
    );
  r_timer_14 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      D => r_timer_mux0000_1_Q,
      Q => r_timer(14)
    );
  r_timer_15 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      D => r_timer_mux0000_0_Q,
      Q => r_timer(15)
    );
  r_buf_7 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      CE => r_buf_0_not0001_130,
      D => rx_IBUF_218,
      Q => r_buf(7)
    );
  r_buf_6 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      CE => r_buf_0_not0001_130,
      D => r_buf(7),
      Q => r_buf(6)
    );
  r_buf_5 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      CE => r_buf_0_not0001_130,
      D => r_buf(6),
      Q => r_buf(5)
    );
  r_buf_4 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      CE => r_buf_0_not0001_130,
      D => r_buf(5),
      Q => r_buf(4)
    );
  r_buf_3 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      CE => r_buf_0_not0001_130,
      D => r_buf(4),
      Q => r_buf(3)
    );
  r_buf_2 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      CE => r_buf_0_not0001_130,
      D => r_buf(3),
      Q => r_buf(2)
    );
  r_buf_1 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      CE => r_buf_0_not0001_130,
      D => r_buf(2),
      Q => r_buf(1)
    );
  r_buf_0 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_BUFGP_128,
      CE => r_buf_0_not0001_130,
      D => r_buf(1),
      Q => r_buf(0)
    );
  r_data_0 : FDE
    port map (
      C => clk_BUFGP_128,
      CE => r_data_or0000_inv_154,
      D => r_buf(0),
      Q => r_data_0_146
    );
  r_data_1 : FDE
    port map (
      C => clk_BUFGP_128,
      CE => r_data_or0000_inv_154,
      D => r_buf(1),
      Q => r_data_1_147
    );
  r_data_2 : FDE
    port map (
      C => clk_BUFGP_128,
      CE => r_data_or0000_inv_154,
      D => r_buf(2),
      Q => r_data_2_148
    );
  r_data_3 : FDE
    port map (
      C => clk_BUFGP_128,
      CE => r_data_or0000_inv_154,
      D => r_buf(3),
      Q => r_data_3_149
    );
  r_data_4 : FDE
    port map (
      C => clk_BUFGP_128,
      CE => r_data_or0000_inv_154,
      D => r_buf(4),
      Q => r_data_4_150
    );
  r_data_5 : FDE
    port map (
      C => clk_BUFGP_128,
      CE => r_data_or0000_inv_154,
      D => r_buf(5),
      Q => r_data_5_151
    );
  r_data_6 : FDE
    port map (
      C => clk_BUFGP_128,
      CE => r_data_or0000_inv_154,
      D => r_buf(6),
      Q => r_data_6_152
    );
  r_data_7 : FDE
    port map (
      C => clk_BUFGP_128,
      CE => r_data_or0000_inv_154,
      D => r_buf(7),
      Q => r_data_7_153
    );
  s_timer_0 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_timer_not0001,
      D => Mcount_s_timer_eqn_0,
      Q => s_timer(0)
    );
  s_timer_1 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_timer_not0001,
      D => Mcount_s_timer_eqn_1,
      Q => s_timer(1)
    );
  s_timer_2 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_timer_not0001,
      D => Mcount_s_timer_eqn_2,
      Q => s_timer(2)
    );
  s_timer_3 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_timer_not0001,
      D => Mcount_s_timer_eqn_3,
      Q => s_timer(3)
    );
  s_timer_4 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_timer_not0001,
      D => Mcount_s_timer_eqn_4,
      Q => s_timer(4)
    );
  s_timer_5 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_timer_not0001,
      D => Mcount_s_timer_eqn_5,
      Q => s_timer(5)
    );
  s_timer_6 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_timer_not0001,
      D => Mcount_s_timer_eqn_6,
      Q => s_timer(6)
    );
  s_timer_7 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_timer_not0001,
      D => Mcount_s_timer_eqn_7,
      Q => s_timer(7)
    );
  s_timer_8 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_timer_not0001,
      D => Mcount_s_timer_eqn_8,
      Q => s_timer(8)
    );
  s_timer_9 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_timer_not0001,
      D => Mcount_s_timer_eqn_9,
      Q => s_timer(9)
    );
  s_timer_10 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_timer_not0001,
      D => Mcount_s_timer_eqn_10,
      Q => s_timer(10)
    );
  s_timer_11 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_timer_not0001,
      D => Mcount_s_timer_eqn_11_19,
      Q => s_timer(11)
    );
  s_timer_12 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_timer_not0001,
      D => Mcount_s_timer_eqn_12,
      Q => s_timer(12)
    );
  s_timer_13 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_timer_not0001,
      D => Mcount_s_timer_eqn_13,
      Q => s_timer(13)
    );
  s_timer_14 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_timer_not0001,
      D => Mcount_s_timer_eqn_14,
      Q => s_timer(14)
    );
  s_timer_15 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      CE => s_timer_not0001,
      D => Mcount_s_timer_eqn_15,
      Q => s_timer(15)
    );
  Msub_r_timer_addsub0000_cy_0_Q : MUXCY
    port map (
      CI => N1,
      DI => N0,
      S => Msub_r_timer_addsub0000_cy_0_rt_48,
      O => Msub_r_timer_addsub0000_cy(0)
    );
  Msub_r_timer_addsub0000_xor_0_Q : XORCY
    port map (
      CI => N1,
      LI => Msub_r_timer_addsub0000_cy_0_rt_48,
      O => r_timer_addsub0000(0)
    );
  Msub_r_timer_addsub0000_cy_1_Q : MUXCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(0),
      DI => N1,
      S => Msub_r_timer_addsub0000_lut(1),
      O => Msub_r_timer_addsub0000_cy(1)
    );
  Msub_r_timer_addsub0000_xor_1_Q : XORCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(0),
      LI => Msub_r_timer_addsub0000_lut(1),
      O => r_timer_addsub0000(1)
    );
  Msub_r_timer_addsub0000_cy_2_Q : MUXCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(1),
      DI => N1,
      S => Msub_r_timer_addsub0000_lut(2),
      O => Msub_r_timer_addsub0000_cy(2)
    );
  Msub_r_timer_addsub0000_xor_2_Q : XORCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(1),
      LI => Msub_r_timer_addsub0000_lut(2),
      O => r_timer_addsub0000(2)
    );
  Msub_r_timer_addsub0000_cy_3_Q : MUXCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(2),
      DI => N1,
      S => Msub_r_timer_addsub0000_lut(3),
      O => Msub_r_timer_addsub0000_cy(3)
    );
  Msub_r_timer_addsub0000_xor_3_Q : XORCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(2),
      LI => Msub_r_timer_addsub0000_lut(3),
      O => r_timer_addsub0000(3)
    );
  Msub_r_timer_addsub0000_cy_4_Q : MUXCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(3),
      DI => N1,
      S => Msub_r_timer_addsub0000_lut(4),
      O => Msub_r_timer_addsub0000_cy(4)
    );
  Msub_r_timer_addsub0000_xor_4_Q : XORCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(3),
      LI => Msub_r_timer_addsub0000_lut(4),
      O => r_timer_addsub0000(4)
    );
  Msub_r_timer_addsub0000_cy_5_Q : MUXCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(4),
      DI => N1,
      S => Msub_r_timer_addsub0000_lut(5),
      O => Msub_r_timer_addsub0000_cy(5)
    );
  Msub_r_timer_addsub0000_xor_5_Q : XORCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(4),
      LI => Msub_r_timer_addsub0000_lut(5),
      O => r_timer_addsub0000(5)
    );
  Msub_r_timer_addsub0000_cy_6_Q : MUXCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(5),
      DI => N1,
      S => Msub_r_timer_addsub0000_lut(6),
      O => Msub_r_timer_addsub0000_cy(6)
    );
  Msub_r_timer_addsub0000_xor_6_Q : XORCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(5),
      LI => Msub_r_timer_addsub0000_lut(6),
      O => r_timer_addsub0000(6)
    );
  Msub_r_timer_addsub0000_cy_7_Q : MUXCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(6),
      DI => N1,
      S => Msub_r_timer_addsub0000_lut(7),
      O => Msub_r_timer_addsub0000_cy(7)
    );
  Msub_r_timer_addsub0000_xor_7_Q : XORCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(6),
      LI => Msub_r_timer_addsub0000_lut(7),
      O => r_timer_addsub0000(7)
    );
  Msub_r_timer_addsub0000_cy_8_Q : MUXCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(7),
      DI => N1,
      S => Msub_r_timer_addsub0000_lut(8),
      O => Msub_r_timer_addsub0000_cy(8)
    );
  Msub_r_timer_addsub0000_xor_8_Q : XORCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(7),
      LI => Msub_r_timer_addsub0000_lut(8),
      O => r_timer_addsub0000(8)
    );
  Msub_r_timer_addsub0000_cy_9_Q : MUXCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(8),
      DI => N1,
      S => Msub_r_timer_addsub0000_lut(9),
      O => Msub_r_timer_addsub0000_cy(9)
    );
  Msub_r_timer_addsub0000_xor_9_Q : XORCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(8),
      LI => Msub_r_timer_addsub0000_lut(9),
      O => r_timer_addsub0000(9)
    );
  Msub_r_timer_addsub0000_cy_10_Q : MUXCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(9),
      DI => N1,
      S => Msub_r_timer_addsub0000_lut(10),
      O => Msub_r_timer_addsub0000_cy(10)
    );
  Msub_r_timer_addsub0000_xor_10_Q : XORCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(9),
      LI => Msub_r_timer_addsub0000_lut(10),
      O => r_timer_addsub0000(10)
    );
  Msub_r_timer_addsub0000_cy_11_Q : MUXCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(10),
      DI => N1,
      S => Msub_r_timer_addsub0000_lut(11),
      O => Msub_r_timer_addsub0000_cy(11)
    );
  Msub_r_timer_addsub0000_xor_11_Q : XORCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(10),
      LI => Msub_r_timer_addsub0000_lut(11),
      O => r_timer_addsub0000(11)
    );
  Msub_r_timer_addsub0000_cy_12_Q : MUXCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(11),
      DI => N1,
      S => Msub_r_timer_addsub0000_lut(12),
      O => Msub_r_timer_addsub0000_cy(12)
    );
  Msub_r_timer_addsub0000_xor_12_Q : XORCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(11),
      LI => Msub_r_timer_addsub0000_lut(12),
      O => r_timer_addsub0000(12)
    );
  Msub_r_timer_addsub0000_cy_13_Q : MUXCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(12),
      DI => N1,
      S => Msub_r_timer_addsub0000_lut(13),
      O => Msub_r_timer_addsub0000_cy(13)
    );
  Msub_r_timer_addsub0000_xor_13_Q : XORCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(12),
      LI => Msub_r_timer_addsub0000_lut(13),
      O => r_timer_addsub0000(13)
    );
  Msub_r_timer_addsub0000_cy_14_Q : MUXCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(13),
      DI => N1,
      S => Msub_r_timer_addsub0000_lut(14),
      O => Msub_r_timer_addsub0000_cy(14)
    );
  Msub_r_timer_addsub0000_xor_14_Q : XORCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(13),
      LI => Msub_r_timer_addsub0000_lut(14),
      O => r_timer_addsub0000(14)
    );
  Msub_r_timer_addsub0000_xor_15_Q : XORCY
    port map (
      CI => Msub_r_timer_addsub0000_cy(14),
      LI => Msub_r_timer_addsub0000_lut(15),
      O => r_timer_addsub0000(15)
    );
  Mcount_s_timer_cy_0_Q : MUXCY
    port map (
      CI => N1,
      DI => N0,
      S => Mcount_s_timer_cy_0_rt_1,
      O => Mcount_s_timer_cy(0)
    );
  Mcount_s_timer_xor_0_Q : XORCY
    port map (
      CI => N1,
      LI => Mcount_s_timer_cy_0_rt_1,
      O => Result(0)
    );
  Mcount_s_timer_cy_1_Q : MUXCY
    port map (
      CI => Mcount_s_timer_cy(0),
      DI => N1,
      S => Mcount_s_timer_lut(1),
      O => Mcount_s_timer_cy(1)
    );
  Mcount_s_timer_xor_1_Q : XORCY
    port map (
      CI => Mcount_s_timer_cy(0),
      LI => Mcount_s_timer_lut(1),
      O => Result(1)
    );
  Mcount_s_timer_cy_2_Q : MUXCY
    port map (
      CI => Mcount_s_timer_cy(1),
      DI => N1,
      S => Mcount_s_timer_lut(2),
      O => Mcount_s_timer_cy(2)
    );
  Mcount_s_timer_xor_2_Q : XORCY
    port map (
      CI => Mcount_s_timer_cy(1),
      LI => Mcount_s_timer_lut(2),
      O => Result(2)
    );
  Mcount_s_timer_cy_3_Q : MUXCY
    port map (
      CI => Mcount_s_timer_cy(2),
      DI => N1,
      S => Mcount_s_timer_lut(3),
      O => Mcount_s_timer_cy(3)
    );
  Mcount_s_timer_xor_3_Q : XORCY
    port map (
      CI => Mcount_s_timer_cy(2),
      LI => Mcount_s_timer_lut(3),
      O => Result(3)
    );
  Mcount_s_timer_cy_4_Q : MUXCY
    port map (
      CI => Mcount_s_timer_cy(3),
      DI => N1,
      S => Mcount_s_timer_lut(4),
      O => Mcount_s_timer_cy(4)
    );
  Mcount_s_timer_xor_4_Q : XORCY
    port map (
      CI => Mcount_s_timer_cy(3),
      LI => Mcount_s_timer_lut(4),
      O => Result(4)
    );
  Mcount_s_timer_cy_5_Q : MUXCY
    port map (
      CI => Mcount_s_timer_cy(4),
      DI => N1,
      S => Mcount_s_timer_lut(5),
      O => Mcount_s_timer_cy(5)
    );
  Mcount_s_timer_xor_5_Q : XORCY
    port map (
      CI => Mcount_s_timer_cy(4),
      LI => Mcount_s_timer_lut(5),
      O => Result(5)
    );
  Mcount_s_timer_cy_6_Q : MUXCY
    port map (
      CI => Mcount_s_timer_cy(5),
      DI => N1,
      S => Mcount_s_timer_lut(6),
      O => Mcount_s_timer_cy(6)
    );
  Mcount_s_timer_xor_6_Q : XORCY
    port map (
      CI => Mcount_s_timer_cy(5),
      LI => Mcount_s_timer_lut(6),
      O => Result(6)
    );
  Mcount_s_timer_cy_7_Q : MUXCY
    port map (
      CI => Mcount_s_timer_cy(6),
      DI => N1,
      S => Mcount_s_timer_lut(7),
      O => Mcount_s_timer_cy(7)
    );
  Mcount_s_timer_xor_7_Q : XORCY
    port map (
      CI => Mcount_s_timer_cy(6),
      LI => Mcount_s_timer_lut(7),
      O => Result(7)
    );
  Mcount_s_timer_cy_8_Q : MUXCY
    port map (
      CI => Mcount_s_timer_cy(7),
      DI => N1,
      S => Mcount_s_timer_lut(8),
      O => Mcount_s_timer_cy(8)
    );
  Mcount_s_timer_xor_8_Q : XORCY
    port map (
      CI => Mcount_s_timer_cy(7),
      LI => Mcount_s_timer_lut(8),
      O => Result(8)
    );
  Mcount_s_timer_cy_9_Q : MUXCY
    port map (
      CI => Mcount_s_timer_cy(8),
      DI => N1,
      S => Mcount_s_timer_lut(9),
      O => Mcount_s_timer_cy(9)
    );
  Mcount_s_timer_xor_9_Q : XORCY
    port map (
      CI => Mcount_s_timer_cy(8),
      LI => Mcount_s_timer_lut(9),
      O => Result(9)
    );
  Mcount_s_timer_cy_10_Q : MUXCY
    port map (
      CI => Mcount_s_timer_cy(9),
      DI => N1,
      S => Mcount_s_timer_lut(10),
      O => Mcount_s_timer_cy(10)
    );
  Mcount_s_timer_xor_10_Q : XORCY
    port map (
      CI => Mcount_s_timer_cy(9),
      LI => Mcount_s_timer_lut(10),
      O => Result(10)
    );
  Mcount_s_timer_cy_11_Q : MUXCY
    port map (
      CI => Mcount_s_timer_cy(10),
      DI => N1,
      S => Mcount_s_timer_lut(11),
      O => Mcount_s_timer_cy(11)
    );
  Mcount_s_timer_xor_11_Q : XORCY
    port map (
      CI => Mcount_s_timer_cy(10),
      LI => Mcount_s_timer_lut(11),
      O => Result(11)
    );
  Mcount_s_timer_cy_12_Q : MUXCY
    port map (
      CI => Mcount_s_timer_cy(11),
      DI => N1,
      S => Mcount_s_timer_lut(12),
      O => Mcount_s_timer_cy(12)
    );
  Mcount_s_timer_xor_12_Q : XORCY
    port map (
      CI => Mcount_s_timer_cy(11),
      LI => Mcount_s_timer_lut(12),
      O => Result(12)
    );
  Mcount_s_timer_cy_13_Q : MUXCY
    port map (
      CI => Mcount_s_timer_cy(12),
      DI => N1,
      S => Mcount_s_timer_lut(13),
      O => Mcount_s_timer_cy(13)
    );
  Mcount_s_timer_xor_13_Q : XORCY
    port map (
      CI => Mcount_s_timer_cy(12),
      LI => Mcount_s_timer_lut(13),
      O => Result(13)
    );
  Mcount_s_timer_cy_14_Q : MUXCY
    port map (
      CI => Mcount_s_timer_cy(13),
      DI => N1,
      S => Mcount_s_timer_lut(14),
      O => Mcount_s_timer_cy(14)
    );
  Mcount_s_timer_xor_14_Q : XORCY
    port map (
      CI => Mcount_s_timer_cy(13),
      LI => Mcount_s_timer_lut(14),
      O => Result(14)
    );
  Mcount_s_timer_xor_15_Q : XORCY
    port map (
      CI => Mcount_s_timer_cy(14),
      LI => Mcount_s_timer_lut(15),
      O => Result(15)
    );
  s_state_mux0000_2_1 : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => s_state(1),
      I1 => s_state(0),
      O => s_state_mux0000(2)
    );
  s_state_mux0000_1_1 : LUT4
    generic map(
      INIT => X"C9C8"
    )
    port map (
      I0 => s_state(0),
      I1 => s_state(2),
      I2 => s_state(1),
      I3 => s_state(3),
      O => s_state_mux0000(1)
    );
  s_state_mux0000_0_1 : LUT4
    generic map(
      INIT => X"CCC9"
    )
    port map (
      I0 => s_state(0),
      I1 => s_state(3),
      I2 => s_state(1),
      I3 => s_state(2),
      O => s_state_mux0000(0)
    );
  s_state_mux0000_3_1 : LUT5
    generic map(
      INIT => X"55755555"
    )
    port map (
      I0 => s_state(0),
      I1 => s_state(2),
      I2 => s_state(3),
      I3 => s_valid_IBUF_284,
      I4 => s_state(1),
      O => s_state_mux0000(3)
    );
  r_buf_0_not0001_SW0 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => r_state(1),
      I1 => r_state(0),
      O => N02
    );
  r_buf_0_not0001 : LUT6
    generic map(
      INIT => X"0F0E000000000000"
    )
    port map (
      I0 => r_state(3),
      I1 => r_state(2),
      I2 => r_timer(2),
      I3 => N02,
      I4 => N19,
      I5 => r_state_and0000,
      O => r_buf_0_not0001_130
    );
  r_data_or0000_inv : LUT6
    generic map(
      INIT => X"0001000000000000"
    )
    port map (
      I0 => r_state(3),
      I1 => r_state(2),
      I2 => r_timer(2),
      I3 => N02,
      I4 => N19,
      I5 => r_state_and0000,
      O => r_data_or0000_inv_154
    );
  Mcount_s_timer_eqn_91 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => s_state_not0002_inv,
      I1 => Result(9),
      O => Mcount_s_timer_eqn_9
    );
  Mcount_s_timer_eqn_81 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => s_state_not0002_inv,
      I1 => Result(8),
      O => Mcount_s_timer_eqn_8
    );
  Mcount_s_timer_eqn_71 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => s_state_not0002_inv,
      I1 => Result(7),
      O => Mcount_s_timer_eqn_7
    );
  Mcount_s_timer_eqn_61 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => s_state_not0002_inv,
      I1 => Result(6),
      O => Mcount_s_timer_eqn_6
    );
  Mcount_s_timer_eqn_51 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => Result(5),
      I1 => s_state_not0002_inv,
      O => Mcount_s_timer_eqn_5
    );
  Mcount_s_timer_eqn_41 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => s_state_not0002_inv,
      I1 => Result(4),
      O => Mcount_s_timer_eqn_4
    );
  Mcount_s_timer_eqn_31 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => s_state_not0002_inv,
      I1 => Result(3),
      O => Mcount_s_timer_eqn_3
    );
  Mcount_s_timer_eqn_21 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => Result(2),
      I1 => s_state_not0002_inv,
      O => Mcount_s_timer_eqn_2
    );
  Mcount_s_timer_eqn_151 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => s_state_not0002_inv,
      I1 => Result(15),
      O => Mcount_s_timer_eqn_15
    );
  Mcount_s_timer_eqn_141 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => s_state_not0002_inv,
      I1 => Result(14),
      O => Mcount_s_timer_eqn_14
    );
  Mcount_s_timer_eqn_131 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => s_state_not0002_inv,
      I1 => Result(13),
      O => Mcount_s_timer_eqn_13
    );
  Mcount_s_timer_eqn_121 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => s_state_not0002_inv,
      I1 => Result(12),
      O => Mcount_s_timer_eqn_12
    );
  Mcount_s_timer_eqn_111 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => s_state_not0002_inv,
      I1 => Result(11),
      O => Mcount_s_timer_eqn_11_19
    );
  Mcount_s_timer_eqn_101 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => s_state_not0002_inv,
      I1 => Result(10),
      O => Mcount_s_timer_eqn_10
    );
  Mcount_s_timer_eqn_11 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => s_state_not0002_inv,
      I1 => Result(1),
      O => Mcount_s_timer_eqn_1
    );
  Mcount_s_timer_eqn_01 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => s_state_not0002_inv,
      I1 => Result(0),
      O => Mcount_s_timer_eqn_0
    );
  r_valid_cmp_eq00001 : LUT4
    generic map(
      INIT => X"0800"
    )
    port map (
      I0 => r_state(0),
      I1 => r_state(1),
      I2 => r_state(2),
      I3 => r_state(3),
      O => r_valid_OBUF_216
    );
  r_state_or00001 : LUT5
    generic map(
      INIT => X"00040210"
    )
    port map (
      I0 => r_timer(0),
      I1 => r_timer(1),
      I2 => r_timer(2),
      I3 => r_timer(3),
      I4 => r_timer(4),
      O => r_state_or0000
    );
  r_data_and000021 : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => r_timer(0),
      I1 => r_timer(4),
      I2 => r_timer(1),
      I3 => r_timer(3),
      O => N19
    );
  s_state_cmp_eq0000104 : LUT6
    generic map(
      INIT => X"0000000000000001"
    )
    port map (
      I0 => s_timer(14),
      I1 => s_timer(15),
      I2 => s_timer(13),
      I3 => s_timer(12),
      I4 => s_timer(11),
      I5 => s_timer(10),
      O => s_state_cmp_eq0000104_260
    );
  r_state_mux0001_2_SW0 : LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      I0 => r_timer(4),
      I1 => r_timer(3),
      I2 => r_timer(1),
      I3 => r_timer(0),
      O => N7
    );
  r_state_mux0001_2_Q : LUT6
    generic map(
      INIT => X"CDC9CCC8CCCCCCCC"
    )
    port map (
      I0 => r_timer(2),
      I1 => r_state(1),
      I2 => r_state(0),
      I3 => N7,
      I4 => N19,
      I5 => r_state_and0000,
      O => r_state_mux0001(2)
    );
  r_state_mux0001_0_SW1 : LUT4
    generic map(
      INIT => X"FE00"
    )
    port map (
      I0 => r_state(2),
      I1 => r_state(1),
      I2 => r_state(0),
      I3 => N19,
      O => N10
    );
  r_state_mux0001_0_Q : LUT6
    generic map(
      INIT => X"FFFFAAAAF7F5A2A0"
    )
    port map (
      I0 => r_state(3),
      I1 => r_timer(2),
      I2 => r_state_or0000,
      I3 => N10,
      I4 => N9,
      I5 => N01,
      O => r_state_mux0001(0)
    );
  r_data_and00001_SW0 : LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
    port map (
      I0 => r_timer(8),
      I1 => r_timer(14),
      I2 => r_timer(15),
      I3 => r_timer(10),
      I4 => r_timer(9),
      I5 => r_timer(5),
      O => N12
    );
  r_data_and00001 : LUT6
    generic map(
      INIT => X"0000000000000001"
    )
    port map (
      I0 => r_timer(13),
      I1 => r_timer(11),
      I2 => r_timer(7),
      I3 => r_timer(6),
      I4 => r_timer(12),
      I5 => N12,
      O => r_state_and0000
    );
  s_valid_IBUF : IBUF
    port map (
      I => s_valid,
      O => s_valid_IBUF_284
    );
  rx_IBUF : IBUF
    port map (
      I => rx,
      O => rx_IBUF_218
    );
  s_data_7_IBUF : IBUF
    port map (
      I => s_data(7),
      O => s_data_7_IBUF_255
    );
  s_data_6_IBUF : IBUF
    port map (
      I => s_data(6),
      O => s_data_6_IBUF_254
    );
  s_data_5_IBUF : IBUF
    port map (
      I => s_data(5),
      O => s_data_5_IBUF_253
    );
  s_data_4_IBUF : IBUF
    port map (
      I => s_data(4),
      O => s_data_4_IBUF_252
    );
  s_data_3_IBUF : IBUF
    port map (
      I => s_data(3),
      O => s_data_3_IBUF_251
    );
  s_data_2_IBUF : IBUF
    port map (
      I => s_data(2),
      O => s_data_2_IBUF_250
    );
  s_data_1_IBUF : IBUF
    port map (
      I => s_data(1),
      O => s_data_1_IBUF_249
    );
  s_data_0_IBUF : IBUF
    port map (
      I => s_data(0),
      O => s_data_0_IBUF_248
    );
  r_valid_OBUF : OBUF
    port map (
      I => r_valid_OBUF_216,
      O => r_valid
    );
  tx_OBUF : OBUF
    port map (
      I => s_buf(0),
      O => tx
    );
  s_busy_OBUF : OBUF
    port map (
      I => s_busy_OBUF_239,
      O => s_busy
    );
  r_data_7_OBUF : OBUF
    port map (
      I => r_data_7_153,
      O => r_data(7)
    );
  r_data_6_OBUF : OBUF
    port map (
      I => r_data_6_152,
      O => r_data(6)
    );
  r_data_5_OBUF : OBUF
    port map (
      I => r_data_5_151,
      O => r_data(5)
    );
  r_data_4_OBUF : OBUF
    port map (
      I => r_data_4_150,
      O => r_data(4)
    );
  r_data_3_OBUF : OBUF
    port map (
      I => r_data_3_149,
      O => r_data(3)
    );
  r_data_2_OBUF : OBUF
    port map (
      I => r_data_2_148,
      O => r_data(2)
    );
  r_data_1_OBUF : OBUF
    port map (
      I => r_data_1_147,
      O => r_data(1)
    );
  r_data_0_OBUF : OBUF
    port map (
      I => r_data_0_146,
      O => r_data(0)
    );
  r_timer_5 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      D => r_timer_mux0000_10_1,
      S => N21,
      Q => r_timer(5)
    );
  Msub_r_timer_addsub0000_cy_0_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => r_timer(0),
      O => Msub_r_timer_addsub0000_cy_0_rt_48
    );
  Mcount_s_timer_cy_0_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => s_timer(0),
      O => Mcount_s_timer_cy_0_rt_1
    );
  r_timer_mux0000_14_11 : LUT6
    generic map(
      INIT => X"BBA8BB88A0A00000"
    )
    port map (
      I0 => r_timer(1),
      I1 => r_valid_OBUF_216,
      I2 => r_state_or0000,
      I3 => r_timer_addsub0000(1),
      I4 => r_state_and0000,
      I5 => N01,
      O => r_timer_mux0000_14_1
    );
  r_timer_mux0000_11_11 : LUT6
    generic map(
      INIT => X"BBA8BB88A0A00000"
    )
    port map (
      I0 => r_timer(4),
      I1 => r_valid_OBUF_216,
      I2 => r_state_or0000,
      I3 => r_timer_addsub0000(4),
      I4 => r_state_and0000,
      I5 => N01,
      O => r_timer_mux0000_11_1
    );
  r_timer_mux0000_8_1 : LUT6
    generic map(
      INIT => X"CFCC8F80CCCC0000"
    )
    port map (
      I0 => rx_IBUF_218,
      I1 => r_timer(7),
      I2 => r_valid_OBUF_216,
      I3 => r_timer_addsub0000(7),
      I4 => N181,
      I5 => N01,
      O => r_timer_mux0000_8_Q
    );
  r_timer_mux0000_7_1 : LUT6
    generic map(
      INIT => X"CFCC8F80CCCC0000"
    )
    port map (
      I0 => rx_IBUF_218,
      I1 => r_timer(8),
      I2 => r_valid_OBUF_216,
      I3 => r_timer_addsub0000(8),
      I4 => N181,
      I5 => N01,
      O => r_timer_mux0000_7_Q
    );
  r_timer_mux0000_6_1 : LUT6
    generic map(
      INIT => X"CFCC8F80CCCC0000"
    )
    port map (
      I0 => rx_IBUF_218,
      I1 => r_timer(9),
      I2 => r_valid_OBUF_216,
      I3 => r_timer_addsub0000(9),
      I4 => N181,
      I5 => N01,
      O => r_timer_mux0000_6_Q
    );
  r_timer_mux0000_5_1 : LUT6
    generic map(
      INIT => X"CFCC8F80CCCC0000"
    )
    port map (
      I0 => rx_IBUF_218,
      I1 => r_timer(10),
      I2 => r_valid_OBUF_216,
      I3 => r_timer_addsub0000(10),
      I4 => N181,
      I5 => N01,
      O => r_timer_mux0000_5_Q
    );
  r_timer_mux0000_4_1 : LUT6
    generic map(
      INIT => X"CFCC8F80CCCC0000"
    )
    port map (
      I0 => rx_IBUF_218,
      I1 => r_timer(11),
      I2 => r_valid_OBUF_216,
      I3 => r_timer_addsub0000(11),
      I4 => N181,
      I5 => N01,
      O => r_timer_mux0000_4_Q
    );
  r_timer_mux0000_3_1 : LUT6
    generic map(
      INIT => X"CFCC8F80CCCC0000"
    )
    port map (
      I0 => rx_IBUF_218,
      I1 => r_timer(12),
      I2 => r_valid_OBUF_216,
      I3 => r_timer_addsub0000(12),
      I4 => N181,
      I5 => N01,
      O => r_timer_mux0000_3_Q
    );
  r_timer_mux0000_2_1 : LUT6
    generic map(
      INIT => X"CFCC8F80CCCC0000"
    )
    port map (
      I0 => rx_IBUF_218,
      I1 => r_timer(13),
      I2 => r_valid_OBUF_216,
      I3 => r_timer_addsub0000(13),
      I4 => N181,
      I5 => N01,
      O => r_timer_mux0000_2_Q
    );
  r_timer_mux0000_1_1 : LUT6
    generic map(
      INIT => X"CFCC8F80CCCC0000"
    )
    port map (
      I0 => rx_IBUF_218,
      I1 => r_timer(14),
      I2 => r_valid_OBUF_216,
      I3 => r_timer_addsub0000(14),
      I4 => N181,
      I5 => N01,
      O => r_timer_mux0000_1_Q
    );
  r_timer_mux0000_0_1 : LUT6
    generic map(
      INIT => X"CFCC8F80CCCC0000"
    )
    port map (
      I0 => rx_IBUF_218,
      I1 => r_timer(15),
      I2 => r_valid_OBUF_216,
      I3 => r_timer_addsub0000(15),
      I4 => N181,
      I5 => N01,
      O => r_timer_mux0000_0_Q
    );
  r_timer_mux0000_13_31 : LUT6
    generic map(
      INIT => X"0400000000000000"
    )
    port map (
      I0 => rx_IBUF_218,
      I1 => r_state(3),
      I2 => r_state(2),
      I3 => r_state(1),
      I4 => r_state(0),
      I5 => N01,
      O => N18
    );
  r_data_and00001_SW1 : LUT6
    generic map(
      INIT => X"FFFFFFFFFFEFFDFA"
    )
    port map (
      I0 => r_timer(1),
      I1 => r_timer(2),
      I2 => r_timer(3),
      I3 => r_timer(4),
      I4 => r_timer(0),
      I5 => r_timer(11),
      O => N45
    );
  r_timer_mux0000_13_11 : LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
    port map (
      I0 => r_timer(12),
      I1 => r_timer(6),
      I2 => r_timer(13),
      I3 => r_timer(7),
      I4 => N45,
      I5 => N12,
      O => N01
    );
  r_timer_1_rstpot : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => N18,
      I1 => r_timer_mux0000_14_1,
      O => r_timer_1_rstpot_173
    );
  r_timer_1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      D => r_timer_1_rstpot_173,
      Q => r_timer(1)
    );
  r_timer_2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      D => r_timer_2_rstpot_175,
      Q => r_timer(2)
    );
  r_timer_4_rstpot : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => N18,
      I1 => r_timer_mux0000_11_1,
      O => r_timer_4_rstpot_178
    );
  r_timer_4 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_BUFGP_128,
      D => r_timer_4_rstpot_178,
      Q => r_timer(4)
    );
  r_data_and00001_SW2 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => r_timer(12),
      I1 => r_timer(11),
      O => N50
    );
  r_timer_mux0000_13_22_SW0 : LUT6
    generic map(
      INIT => X"0000000000010000"
    )
    port map (
      I0 => r_timer(6),
      I1 => r_timer(13),
      I2 => r_timer(7),
      I3 => N50,
      I4 => N19,
      I5 => N12,
      O => N42
    );
  r_timer_mux0000_13_22_SW1 : LUT6
    generic map(
      INIT => X"0000000000010000"
    )
    port map (
      I0 => r_timer(6),
      I1 => r_timer(13),
      I2 => r_timer(7),
      I3 => N50,
      I4 => r_state_or0000,
      I5 => N12,
      O => N43
    );
  r_timer_mux0000_12_1 : LUT6
    generic map(
      INIT => X"FFAAD58080808080"
    )
    port map (
      I0 => r_timer(3),
      I1 => r_state_or0000,
      I2 => r_state_and0000,
      I3 => N60,
      I4 => N61,
      I5 => N01,
      O => r_timer_mux0000_12_Q
    );
  r_timer_mux0000_10_12 : LUT6
    generic map(
      INIT => X"FF80D580AA808080"
    )
    port map (
      I0 => r_timer(5),
      I1 => r_state_or0000,
      I2 => r_state_and0000,
      I3 => N01,
      I4 => N64,
      I5 => N63,
      O => r_timer_mux0000_10_1
    );
  r_data_and00001_SW4 : LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFEF"
    )
    port map (
      I0 => r_timer(12),
      I1 => r_timer(13),
      I2 => r_state(3),
      I3 => r_timer(7),
      I4 => r_state(1),
      I5 => r_state(0),
      O => N72
    );
  r_data_and00001_SW5 : LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
    port map (
      I0 => r_timer(7),
      I1 => r_timer(13),
      I2 => r_timer(12),
      I3 => r_state(2),
      I4 => r_state(1),
      I5 => r_state(0),
      O => N74
    );
  r_state_mux0001_0_SW0 : LUT6
    generic map(
      INIT => X"0000000000000100"
    )
    port map (
      I0 => r_timer(11),
      I1 => r_timer(6),
      I2 => r_timer(2),
      I3 => N19,
      I4 => N74,
      I5 => N12,
      O => N9
    );
  r_data_and00001_SW6 : LUT6
    generic map(
      INIT => X"FFFEFFFFFFFFFFDB"
    )
    port map (
      I0 => r_timer(4),
      I1 => r_timer(2),
      I2 => r_timer(1),
      I3 => r_timer(12),
      I4 => r_timer(3),
      I5 => r_timer(0),
      O => N76
    );
  r_timer_mux0000_8_1_SW0 : LUT6
    generic map(
      INIT => X"0000000000000001"
    )
    port map (
      I0 => r_timer(11),
      I1 => r_timer(6),
      I2 => r_timer(13),
      I3 => r_timer(7),
      I4 => N76,
      I5 => N12,
      O => N181
    );
  r_state_mux0001_1_21 : LUT6
    generic map(
      INIT => X"0000000100000000"
    )
    port map (
      I0 => r_timer(3),
      I1 => r_timer(1),
      I2 => r_timer(4),
      I3 => r_timer(0),
      I4 => r_timer(2),
      I5 => r_state_and0000,
      O => N21
    );
  r_timer_mux0000_10_12_SW0_SW0 : LUT5
    generic map(
      INIT => X"DFFF0000"
    )
    port map (
      I0 => r_state(3),
      I1 => r_state(2),
      I2 => r_state(1),
      I3 => r_state(0),
      I4 => r_timer_addsub0000(5),
      O => N63
    );
  r_timer_mux0000_12_1_SW0_SW0 : LUT5
    generic map(
      INIT => X"DFFF0000"
    )
    port map (
      I0 => r_state(3),
      I1 => r_state(2),
      I2 => r_state(1),
      I3 => r_state(0),
      I4 => r_timer_addsub0000(3),
      O => N60
    );
  r_timer_2_rstpot_SW0 : LUT6
    generic map(
      INIT => X"F7FFFFFF04000000"
    )
    port map (
      I0 => rx_IBUF_218,
      I1 => r_state(3),
      I2 => r_state(2),
      I3 => r_state(1),
      I4 => r_state(0),
      I5 => r_timer_addsub0000(2),
      O => N94
    );
  r_timer_2_rstpot_SW1 : LUT5
    generic map(
      INIT => X"FFFF2000"
    )
    port map (
      I0 => r_state(3),
      I1 => r_state(2),
      I2 => r_state(1),
      I3 => r_state(0),
      I4 => r_timer_addsub0000(2),
      O => N95
    );
  r_timer_2_rstpot : LUT6
    generic map(
      INIT => X"FFEEF5E4E4E4E4E4"
    )
    port map (
      I0 => r_timer(2),
      I1 => N42,
      I2 => N43,
      I3 => N94,
      I4 => N95,
      I5 => N01,
      O => r_timer_2_rstpot_175
    );
  r_timer_mux0000_10_12_SW0_SW1 : LUT6
    generic map(
      INIT => X"FBFFFFFF08000000"
    )
    port map (
      I0 => rx_IBUF_218,
      I1 => r_state(3),
      I2 => r_state(2),
      I3 => r_state(1),
      I4 => r_state(0),
      I5 => r_timer_addsub0000(5),
      O => N64
    );
  r_timer_mux0000_12_1_SW0_SW1 : LUT6
    generic map(
      INIT => X"FBFFFFFF08000000"
    )
    port map (
      I0 => rx_IBUF_218,
      I1 => r_state(3),
      I2 => r_state(2),
      I3 => r_state(1),
      I4 => r_state(0),
      I5 => r_timer_addsub0000(3),
      O => N61
    );
  s_state_cmp_eq0000108_SW0 : LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
    port map (
      I0 => s_timer(4),
      I1 => s_timer(3),
      I2 => s_timer(2),
      I3 => s_timer(1),
      I4 => s_timer(0),
      I5 => s_timer(8),
      O => N97
    );
  s_state_cmp_eq0000108 : LUT6
    generic map(
      INIT => X"0000000000010000"
    )
    port map (
      I0 => s_timer(9),
      I1 => s_timer(7),
      I2 => s_timer(6),
      I3 => s_timer(5),
      I4 => s_state_cmp_eq0000104_260,
      I5 => N97,
      O => s_state_not0002_inv
    );
  s_timer_not00011 : LUT6
    generic map(
      INIT => X"FFFFF7FFF7FFF7FF"
    )
    port map (
      I0 => s_state(0),
      I1 => s_state(1),
      I2 => s_state(2),
      I3 => s_state(3),
      I4 => s_valid_IBUF_284,
      I5 => s_state_not0002_inv,
      O => s_timer_not0001
    );
  s_buf_not00011 : LUT6
    generic map(
      INIT => X"FBFFFFFF00000000"
    )
    port map (
      I0 => s_valid_IBUF_284,
      I1 => s_state(3),
      I2 => s_state(2),
      I3 => s_state(1),
      I4 => s_state(0),
      I5 => s_state_not0002_inv,
      O => s_buf_not0001
    );
  r_timer_mux0000_9_1_SW2 : LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      I0 => r_timer(12),
      I1 => r_timer(7),
      I2 => r_timer(13),
      I3 => N45,
      O => N99
    );
  r_timer_mux0000_9_1 : LUT6
    generic map(
      INIT => X"8F8F80808F8C8080"
    )
    port map (
      I0 => rx_IBUF_218,
      I1 => r_timer(6),
      I2 => r_valid_OBUF_216,
      I3 => N12,
      I4 => r_timer_addsub0000(6),
      I5 => N99,
      O => r_timer_mux0000_9_Q
    );
  s_buf_mux0000_8_1 : LUT5
    generic map(
      INIT => X"FFDFFFFF"
    )
    port map (
      I0 => s_state(0),
      I1 => s_data_7_IBUF_255,
      I2 => s_state(1),
      I3 => s_state(2),
      I4 => s_state(3),
      O => s_buf_mux0000(8)
    );
  s_buf_mux0000_0_1 : LUT5
    generic map(
      INIT => X"C4CCCCCC"
    )
    port map (
      I0 => s_state(0),
      I1 => s_buf(1),
      I2 => s_state(2),
      I3 => s_state(1),
      I4 => s_state(3),
      O => s_buf_mux0000(0)
    );
  s_buf_mux0000_7_1 : LUT6
    generic map(
      INIT => X"CECCCCCCC4CCCCCC"
    )
    port map (
      I0 => s_state(0),
      I1 => s_buf(8),
      I2 => s_state(2),
      I3 => s_state(1),
      I4 => s_state(3),
      I5 => s_data_6_IBUF_254,
      O => s_buf_mux0000(7)
    );
  s_buf_mux0000_6_1 : LUT6
    generic map(
      INIT => X"CECCCCCCC4CCCCCC"
    )
    port map (
      I0 => s_state(0),
      I1 => s_buf(7),
      I2 => s_state(2),
      I3 => s_state(1),
      I4 => s_state(3),
      I5 => s_data_5_IBUF_253,
      O => s_buf_mux0000(6)
    );
  s_buf_mux0000_5_1 : LUT6
    generic map(
      INIT => X"CECCCCCCC4CCCCCC"
    )
    port map (
      I0 => s_state(0),
      I1 => s_buf(6),
      I2 => s_state(2),
      I3 => s_state(1),
      I4 => s_state(3),
      I5 => s_data_4_IBUF_252,
      O => s_buf_mux0000(5)
    );
  s_buf_mux0000_4_1 : LUT6
    generic map(
      INIT => X"CECCCCCCC4CCCCCC"
    )
    port map (
      I0 => s_state(0),
      I1 => s_buf(5),
      I2 => s_state(2),
      I3 => s_state(1),
      I4 => s_state(3),
      I5 => s_data_3_IBUF_251,
      O => s_buf_mux0000(4)
    );
  s_buf_mux0000_3_1 : LUT6
    generic map(
      INIT => X"CECCCCCCC4CCCCCC"
    )
    port map (
      I0 => s_state(0),
      I1 => s_buf(4),
      I2 => s_state(2),
      I3 => s_state(1),
      I4 => s_state(3),
      I5 => s_data_2_IBUF_250,
      O => s_buf_mux0000(3)
    );
  s_buf_mux0000_2_1 : LUT6
    generic map(
      INIT => X"CECCCCCCC4CCCCCC"
    )
    port map (
      I0 => s_state(0),
      I1 => s_buf(3),
      I2 => s_state(2),
      I3 => s_state(1),
      I4 => s_state(3),
      I5 => s_data_1_IBUF_249,
      O => s_buf_mux0000(2)
    );
  s_buf_mux0000_1_1 : LUT6
    generic map(
      INIT => X"CECCCCCCC4CCCCCC"
    )
    port map (
      I0 => s_state(0),
      I1 => s_buf(2),
      I2 => s_state(2),
      I3 => s_state(1),
      I4 => s_state(3),
      I5 => s_data_0_IBUF_248,
      O => s_buf_mux0000(1)
    );
  s_busy1 : LUT4
    generic map(
      INIT => X"DFFF"
    )
    port map (
      I0 => s_state(0),
      I1 => s_state(2),
      I2 => s_state(1),
      I3 => s_state(3),
      O => s_busy_OBUF_239
    );
  clk_BUFGP : BUFGP
    port map (
      I => clk,
      O => clk_BUFGP_128
    );
  Msub_r_timer_addsub0000_lut_1_INV_0 : INV
    port map (
      I => r_timer(1),
      O => Msub_r_timer_addsub0000_lut(1)
    );
  Msub_r_timer_addsub0000_lut_2_INV_0 : INV
    port map (
      I => r_timer(2),
      O => Msub_r_timer_addsub0000_lut(2)
    );
  Msub_r_timer_addsub0000_lut_3_INV_0 : INV
    port map (
      I => r_timer(3),
      O => Msub_r_timer_addsub0000_lut(3)
    );
  Msub_r_timer_addsub0000_lut_4_INV_0 : INV
    port map (
      I => r_timer(4),
      O => Msub_r_timer_addsub0000_lut(4)
    );
  Msub_r_timer_addsub0000_lut_5_INV_0 : INV
    port map (
      I => r_timer(5),
      O => Msub_r_timer_addsub0000_lut(5)
    );
  Msub_r_timer_addsub0000_lut_6_INV_0 : INV
    port map (
      I => r_timer(6),
      O => Msub_r_timer_addsub0000_lut(6)
    );
  Msub_r_timer_addsub0000_lut_7_INV_0 : INV
    port map (
      I => r_timer(7),
      O => Msub_r_timer_addsub0000_lut(7)
    );
  Msub_r_timer_addsub0000_lut_8_INV_0 : INV
    port map (
      I => r_timer(8),
      O => Msub_r_timer_addsub0000_lut(8)
    );
  Msub_r_timer_addsub0000_lut_9_INV_0 : INV
    port map (
      I => r_timer(9),
      O => Msub_r_timer_addsub0000_lut(9)
    );
  Msub_r_timer_addsub0000_lut_10_INV_0 : INV
    port map (
      I => r_timer(10),
      O => Msub_r_timer_addsub0000_lut(10)
    );
  Msub_r_timer_addsub0000_lut_11_INV_0 : INV
    port map (
      I => r_timer(11),
      O => Msub_r_timer_addsub0000_lut(11)
    );
  Msub_r_timer_addsub0000_lut_12_INV_0 : INV
    port map (
      I => r_timer(12),
      O => Msub_r_timer_addsub0000_lut(12)
    );
  Msub_r_timer_addsub0000_lut_13_INV_0 : INV
    port map (
      I => r_timer(13),
      O => Msub_r_timer_addsub0000_lut(13)
    );
  Msub_r_timer_addsub0000_lut_14_INV_0 : INV
    port map (
      I => r_timer(14),
      O => Msub_r_timer_addsub0000_lut(14)
    );
  Msub_r_timer_addsub0000_lut_15_INV_0 : INV
    port map (
      I => r_timer(15),
      O => Msub_r_timer_addsub0000_lut(15)
    );
  Mcount_s_timer_lut_1_INV_0 : INV
    port map (
      I => s_timer(1),
      O => Mcount_s_timer_lut(1)
    );
  Mcount_s_timer_lut_2_INV_0 : INV
    port map (
      I => s_timer(2),
      O => Mcount_s_timer_lut(2)
    );
  Mcount_s_timer_lut_3_INV_0 : INV
    port map (
      I => s_timer(3),
      O => Mcount_s_timer_lut(3)
    );
  Mcount_s_timer_lut_4_INV_0 : INV
    port map (
      I => s_timer(4),
      O => Mcount_s_timer_lut(4)
    );
  Mcount_s_timer_lut_5_INV_0 : INV
    port map (
      I => s_timer(5),
      O => Mcount_s_timer_lut(5)
    );
  Mcount_s_timer_lut_6_INV_0 : INV
    port map (
      I => s_timer(6),
      O => Mcount_s_timer_lut(6)
    );
  Mcount_s_timer_lut_7_INV_0 : INV
    port map (
      I => s_timer(7),
      O => Mcount_s_timer_lut(7)
    );
  Mcount_s_timer_lut_8_INV_0 : INV
    port map (
      I => s_timer(8),
      O => Mcount_s_timer_lut(8)
    );
  Mcount_s_timer_lut_9_INV_0 : INV
    port map (
      I => s_timer(9),
      O => Mcount_s_timer_lut(9)
    );
  Mcount_s_timer_lut_10_INV_0 : INV
    port map (
      I => s_timer(10),
      O => Mcount_s_timer_lut(10)
    );
  Mcount_s_timer_lut_11_INV_0 : INV
    port map (
      I => s_timer(11),
      O => Mcount_s_timer_lut(11)
    );
  Mcount_s_timer_lut_12_INV_0 : INV
    port map (
      I => s_timer(12),
      O => Mcount_s_timer_lut(12)
    );
  Mcount_s_timer_lut_13_INV_0 : INV
    port map (
      I => s_timer(13),
      O => Mcount_s_timer_lut(13)
    );
  Mcount_s_timer_lut_14_INV_0 : INV
    port map (
      I => s_timer(14),
      O => Mcount_s_timer_lut(14)
    );
  Mcount_s_timer_lut_15_INV_0 : INV
    port map (
      I => s_timer(15),
      O => Mcount_s_timer_lut(15)
    );
  r_state_mux0001_1_Q : MUXF7
    port map (
      I0 => N101,
      I1 => N102,
      S => r_state(2),
      O => r_state_mux0001(1)
    );
  r_state_mux0001_1_F : LUT6
    generic map(
      INIT => X"0000000000010000"
    )
    port map (
      I0 => N12,
      I1 => r_timer(2),
      I2 => r_timer(11),
      I3 => r_timer(6),
      I4 => N19,
      I5 => N72,
      O => N101
    );
  r_state_mux0001_1_G : LUT6
    generic map(
      INIT => X"FFFFFF44FFFFFF40"
    )
    port map (
      I0 => r_timer(2),
      I1 => N19,
      I2 => r_state(1),
      I3 => N01,
      I4 => r_state_or0000,
      I5 => r_state(0),
      O => N102
    );
  r_timer_mux0000_15_1 : MUXF7
    port map (
      I0 => N103,
      I1 => N104,
      S => N01,
      O => r_timer_mux0000_15_Q
    );
  r_timer_mux0000_15_1_F : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => r_state_and0000,
      I1 => r_state_or0000,
      I2 => r_timer(0),
      O => N103
    );
  r_timer_mux0000_15_1_G : LUT6
    generic map(
      INIT => X"AAFFAA8080FF8080"
    )
    port map (
      I0 => r_timer(0),
      I1 => r_state_or0000,
      I2 => r_state_and0000,
      I3 => r_valid_OBUF_216,
      I4 => r_timer_addsub0000(0),
      I5 => rx_IBUF_218,
      O => N104
    );
  r_state_mux0001_3_Q : MUXF7
    port map (
      I0 => N105,
      I1 => N106,
      S => r_state(0),
      O => r_state_mux0001(3)
    );
  r_state_mux0001_3_F : LUT3
    generic map(
      INIT => X"08"
    )
    port map (
      I0 => r_state_and0000,
      I1 => N19,
      I2 => r_timer(2),
      O => N105
    );
  r_state_mux0001_3_G : LUT5
    generic map(
      INIT => X"F888FAAA"
    )
    port map (
      I0 => N01,
      I1 => rx_IBUF_218,
      I2 => r_state_or0000,
      I3 => r_state_and0000,
      I4 => r_valid_OBUF_216,
      O => N106
    );

end Structure;

