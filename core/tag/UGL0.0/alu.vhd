-------------------------------------------------------------------------------
-- alu
-- LastUpdate: 2011/10/12 02:04 
-- Author    : ultraredrays
-- Email     : ultraredrays@gmail.com
-- Todo      : 動作確認
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.std_logic_signed.all;

entity alu is
  
  port (
    A     : in  std_logic_vector(31 downto 0);  -- input data
    B     : in  std_logic_vector(31 downto 0);  -- input data
    code  : in  std_logic_vector( 3 downto 0);  -- alu制御入力
    O     : out std_logic_vector(31 downto 0);  -- output data
    Z     : out std_logic);

end alu;

architecture archi of alu is

  signal Otmp : std_logic_vector(31 downto 0) := x"00000000";

begin  -- archi

  Z <= '1' when Otmp = x"00000000" else '0';
  O <= Otmp;
  
  calc: process(A,B,code)
  begin
    case code is
      when "0000" =>
        Otmp <= A and B;
      when "0001" =>
        Otmp <= A or B;
      when "0010" =>
        Otmp <= A + B;
      when "0110" =>
        Otmp <= A - B;
      when "0111" =>
        case A < B  is
          when true =>
            Otmp <= x"00000001";
          when others =>
            Otmp <= x"00000000";
        end case;
      when "0101" =>
        case A = B is
          when true =>
            Otmp <= x"00000001";
          when others =>
            Otmp <= x"00000000";
        end case;
      when "1100" =>
        Otmp <= A nor B;
      when others =>
        Otmp <= A;
    end case;
  end process;
end archi;
