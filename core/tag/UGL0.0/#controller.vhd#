-------------------------------------------------------------------------------
-- controller データパス制御器
-- LastUpdate: 2011/10/13 21:13 
-- Author    : ultraredrays
-- Email     : ultraredrays@gmail.com
-- Todo      : 動作確認
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity controller is
  
  port (
    -- 命令コード (31 downto 26)
    Opcode   : in  std_logic_vector(5 downto 0);
    -- Jump命令用制御信号         0 -> PC=PC+4,    1 -> PC=Im
    Jump     : out std_logic;
    -- Jumpr命令用制御信号         0 -> PC=PC+4,    1 -> PC=Im
    Jumpr    : out std_logic;
    -- レジスタ書き込み先制御信号
    -- 00 -> OP[15-11], 01 -> OP[20-16], 10 -> OP[25-21], 11 -> 31
    RegDst   : out std_logic_vector(1 downto 0);
    -- branch命令用制御信号       0 -> PC=PC+4,    1 -> PC=PC+4+Im
    Branch   : out std_logic;
    -- 読み出しデータ出力制御信号 0 -> NOP,        1 -> MDout=M[Addr]
    MemRead  : out std_logic;
    -- 書き込みレジスタ用データ
    -- 00 -> RWD=ALUOut, 01 -> RWD=MDout, 10 -> R_Dara
    MemtoReg : out std_logic_vector(1 downto 0);
    -- ALU制御に送るOpの情報 主にR,I,J形式の情報を送る
    ALUOp    : out std_logic_vector(1 downto 0);
    -- データメモリに書き込む     0 -> NOP,        1 -> M[Addr]=MDin
    MemWrite : out std_logic;
    -- ALUに送る値の情報          0 -> ALUin2=RRD, 1 -> ALUin2=OP[15-0]
    ALUSrc   : out std_logic;
    -- レジスタに書き込む         0 -> NOP,        1 -> RW = RWD 
    RegWrite : out std_logic;
    -- Input命令用制御信号         0 -> PC=PC+4,    1 -> PC=Im
    Recv     : in  std_logic;
    -- Output命令用制御信号         0 -> PC=PC+4,    1 -> PC=Im
    Send     : out std_logic;
    -- Output命令用制御信号
    S_Busy   : in std_logic;
    -- IO待機信号を送信する
    IO_Wait  : out std_logic);                                      

end controller;

architecture archi of controller is

  signal io_mode : std_logic_vector(1 downto 0) := "00";  -- 00 wait, 10 send, 11 recv

begin  -- archi
  RegDst   <= "11" when Opcode(5 downto 0) = "000011" else
              "10" when Opcode(5 downto 0) = "111111" else
              "00" when Opcode(5 downto 0) = "000000" else
              "01";
  
  Jump     <= '1'  when (Opcode(5 downto 0) = "000010") or
                        (Opcode(5 downto 0) = "000011") else
              '0';

  Jumpr    <= '1'  when Opcode(5 downto 0) = "000001" else
              '0';
  
  Branch   <= '1'  when Opcode(5 downto 0) = "000100" else
              '0';
  
  MemRead  <= '1'  when Opcode(5 downto 0) = "100011" else
              '0';
  
  MemtoReg <= "11" when Opcode(5 downto 0) = "000011" else
              "10" when Opcode(5 downto 0) = "111111" else
              "01" when Opcode(5 downto 0) = "100011" else
              "00";
  
  ALUOp    <= "00" when (Opcode(5 downto 0) = "100011" or
                         Opcode(5 downto 0) = "101011" or
                         Opcode(5 downto 0) = "001000") else
              "01" when Opcode(5 downto 0) = "000100" else
              "10";
  
  MemWrite <= '1'  when Opcode(5 downto 0) = "101011" else
              '0';

  ALUSrc   <= '0'  when (Opcode(5 downto 0) = "000000" or
                         Opcode(5 downto 0) = "000100") else
              '1';
  
  RegWrite <= '1'  when (Opcode(5 downto 0) = "000000" or
                         Opcode(5 downto 0) = "100011" or
                         Opcode(5 downto 0) = "001000" or
                         Opcode(5 downto 0) = "000011" or
                         Opcode(5 downto 0) = "111111") else
              '0';
  
  io_mode <= "11" when Opcode(5 downto 0) = "111111" else
             "10" when Opcode(5 downto 0) = "111110" else
             "00";
  
  IO_Wait <= '1' when ((io_mode = "11" and recv = '0') or
                       (io_mode = "10" and s_busy = '1')) else
             '0';
  Send <= '1' when io_mode = "10" and s_busy = '0' else
          '0';
             
  

end archi;
