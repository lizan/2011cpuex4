#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "com.h"
#include "util.h"

int main(int argc, char* argv[])
{
    FILE *fp;
    com_settings com_setting;
    char buf[8];

    if(argc != 2){
        printf("ERROR: Bad arguments\n");
        printf("Usage: sender sendfile\n");
        exit(1);
    }
    printf("args is OK\n");

    //パラメータの設定
    com_setting.baud = 9600;
    com_setting.stopbit_len = 0;
    setup_comm(&com_setting);
    printf("SetUp is OK\n");

    //ファイルを開く
    fp = fopen(argv[1],"r");
    if(fp == NULL){
        printf("ERROR: No such file exist\n");
        exit(1);
    }
    printf("File is Opened\n");

    //書き込む
    while(fread(buf,1,1,fp)){
        com_putc(buf[0]);
	usleep(20000);
    }

    //communicationの終了
    close_comm();
    //ファイルを閉じる
    fclose(fp);

    return 0;
}
