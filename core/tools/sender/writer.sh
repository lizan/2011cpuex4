#!/bin/bash

SENDER=./sender
RECEIVER=../recver/recver

TMPDIR=`mktemp -d`

echo "Executing receiver..."
$RECEIVER $TMPDIR/$1.received &
sleep 1

echo "Sending $1"
cp $1 $TMPDIR/$1
dd if=/dev/zero bs=1 count=30 | cat >> $TMPDIR/$1
$SENDER $TMPDIR/$1

echo "Waiting..."
wait

echo "Comparing..."
dd if=$TMPDIR/$1 bs=1M count=1 conv=sync | md5sum 2> /dev/null
dd if=$TMPDIR/$1.received bs=1M count=1 conv=sync | md5sum 2> /dev/null

echo $TMPDIR
