#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[]){
    FILE *fp;
    char c = 0;
    int state = 0;
    int size = 0;
    int byte = 0;
    
    if(argc != 2){
        printf("ERROR: Bad arguments\n");
        printf("Usage: sender sendfile\n");
        exit(1);
    }

    //ファイルを開く
    fp = fopen(argv[1],"r");
    if(fp == NULL){
        printf("ERROR: No such file exist\n");
        exit(1);
    }
    printf("File is Opened\n");

    //書き込む
    while(fread(&c,1,1,fp)){
        switch(state){
        case 0:
            if(c == '0'){
                state++;
            }
            break;
        case 1:
            if(c == 'x'){
                state++;
            }else{
                state = 0;
            }
            break;
        case 2:
            if(byte == 0){
                printf("x\"%c",c);
            }else{
                printf("%c",c);
            }
            state++;
            break;
        case 3:
            if(byte == 3){
                printf("%c\",",c);
                byte = 0;
                size++;  
            }else{
                printf("%c",c);
                byte++;
            }
            state = 0;
            break;
        }

        if(c == EOF){
            break;
        }
    }
    while(1){
        printf("x\"00000000\"");
        if(size < 4095){
            size++;
            printf(",");
        }else{
            break;
        }
    }
    
    fclose(fp);

    return 0;

}
