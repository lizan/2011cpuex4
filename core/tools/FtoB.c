#include <stdio.h>

typedef union _bitfloat{
	int bit;
	float f;
}bitfloat;

int main(void)
{	
	int i;
	int bit[32];
	bitfloat data;
	
	scanf("%f",&(data.f));
	printf("input:%f\n",data.f);
	for(i=0;i<32;i++){
		bit[i] = data.bit % 2;
		data.bit = data.bit >> 1;
		printf("bit%2d=%d\n",i,bit[i]);
	}
	for(i=31;i>=0;i--){
		printf("%d",bit[i]);
	}
	printf("\n");
	return 1;
}
