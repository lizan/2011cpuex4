#include <stdio.h>
#include <stdlib.h>
#include "com.h"
#include "util.h"

int main(void)
{
    com_settings com_setting;
    int c = 1;
    //パラメータの設定
    com_setting.baud = 9600;
    com_setting.stopbit_len = 0;
    setup_comm(&com_setting);

    //書き込む
    while(c != EOF){
        c = com_getc();
        putchar((char)c);
    }

    //communicationの終了
    close_comm();

    return 0;
}
