#include <stdio.h>
#include <stdlib.h>
#include "com.h"
#include "util.h"

int main(int argc, char* argv[])
{
    FILE *fp;
    com_settings com_setting;
    char buf[8];
    int count = 0; //メモリ検査用カウンタ

    if(argc != 2){
        printf("ERROR: Bad arguments\n");
        printf("Usage: sender sendfile\n");
        exit(1);
    }
    printf("args is OK\n");

    //パラメータの設定
    com_setting.baud = 9600;
    com_setting.stopbit_len = 0;
    setup_comm(&com_setting);
    printf("SetUp is OK\n");

    //ファイルを開く
    fp = fopen(argv[1],"w");
    if(fp == NULL){
        printf("ERROR: Can't create file:%s\n",argv[1]);
        exit(1);
    }
    printf("File is Opened\n");

    //書き込む
    while(1){
        buf[0] = com_getc();
        fwrite(buf,1,1,fp);
        if(buf[0] == 0){
            count++;
        }else{
            count = 0;
        }
        if(count > 7){
            break;
        }
    }

    //communicationの終了
    close_comm();
    //ファイルを閉じる
    fclose(fp);

    return 0;
}
