
//fslt a b でa < bなら 1, a >= bなら 0

#include <string>
#include <queue>
#include <vector>
#include <fstream>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
using namespace std;
ofstream missdata("missdata");
#define INPUTOPTION 1
//1の時はrandによる乱数,2の時はindata_sの値をとりだす
#define NODATA 0
//1の時はindata,outdataの出力を行わない
#define ONTEST 1
//1の時はデバッグ用関数を呼ぶ。indata_sの値を取り出すときは常に呼ぶ

//デバッグ用共用体
union Ap{
  u_int32_t ii;
  float ff;
};
union Bp{
  u_int32_t ii;
  float ff;
};
union Xp{
  u_int32_t ii;
  float ff;
};
union Ep{
  u_int32_t ii;
  float ff;
};
union Apb{
  u_int32_t ii;
  float ff;
}; 

//u_int32_tをk文字の文字列に変換
string to_string(long l){
  string x;
  for(int i = 0; i < 32; i++){
    if(l % 2 == 1)
      x = "1" + x;
    else
      x = "0" + x;
    l /= 2;
  }
  return x;
}
string to_string1(int i){
  string x;
  if(i == 1)
    x = "1";
  else
    x = "0";
  return x;
}


/*
  デバッグ用関数
  異なる結果ををmissdataに出力
 */
void t_or_f(u_int32_t a, u_int32_t b, int x, string i1,string i2, int i){
  float max,diff;
  u_int32_t ae,be,abe;
  int sol;
  union Ap app;
  union Bp bpp;

  ae = a & 0x7f800000;
  be = b & 0x7f800000;
  if(ae == 0x7f800000 || ae == 0)
    app.ii = 0;
  else
    app.ii = a;

  if(be == 0x7f800000 || be == 0)
    bpp.ii = 0;
  else
    bpp.ii = b;


  if(app.ff < bpp.ff)
    sol = 1;
  else
    sol = 0;

  if(x != sol)
    missdata << i << "番目\n" << 
      i1 << "\t" << app.ff << "\tA\n" <<
      i2 << "\t" << bpp.ff << "\tB\n" <<
      x << "\tFSLT(A,B)\n" <<
      sol << "\t(A < B)\n";
}


//x(a downto b)に相当
u_int32_t subu(u_int32_t x, int a, int b){//x(a downto b)
  u_int32_t y = (x << (32 - a -1)) >> (32 - a + b - 1);
  return y;
}
   
int fslt(u_int32_t x1, u_int32_t x2){
  u_int32_t f1,f2;
  u_int32_t s1 = x1 >> 31;
  u_int32_t s2 = x2 >> 31;  
  u_int32_t e1 = (x1 & 0x7f800000) >> 23;
  u_int32_t e2 = (x2 & 0x7f800000) >> 23;
  int ans;
  
  if(e1 == 0 || e1 == 255)
    f1 = 0x80000000;
  else if(s1 == 1)
    f1 = ~subu(x1,31,0);
  else
    f1 = x1 + 0x80000000;
  
  if(e2 == 0 || e2 == 255)
    f2 = 0x80000000;
  else if(s2 == 1)
    f2 = ~subu(x2,31,0);
  else
    f2 = x2 + 0x80000000;
  
  if(f1 < f2)
    ans = 1;
  else
    ans = 0;
    
  return ans;
}


//ランダムに値を生成し演算を実行する。
void randin(){
  long int k,i,s;
  ofstream indata("indata");
  ofstream e_outdata("e_outdata");
  string i1,i2, o;
  u_int32_t a,b;
  int x;

  cout << "回数　srand定数\n";
  cin >> k >> s;  
  srand(s);

  for(i = 0;i < k;i++){
    a = rand() + rand();
    b = rand() + rand();

    x = fslt(a, b);

    i1 = to_string(a);
    i2 = to_string(b);

#if NODATA != 1
    o = to_string1(x);
    indata << i1 << " " << i2 << "\n";
    e_outdata << o << "\n";    
#endif

#if ONTEST == 1
    t_or_f(a,b,x,i1,i2,i);//デバッグ
#endif

    if((i*100) % k == 0)
      cout << i*100/k << '\n';
  }
}

//01文字列をu_intに変換
u_int32_t to_int(string x){
  int k = x.size();
  u_int32_t ans = 0;
  for(int i = 0; i<k;i++){
    ans <<= 1;
    if(x.at(i) == '1'){
      ans++;
    }
  }
  return ans;
}

/*
  indata_sに入っているデータについて演算を行う
  indata_sには間違いが起こりやすそうなものが入っている。
*/
void org_in(){
  int i = 0;
  ifstream indata_s("indata_s");
  ofstream e_outdata("e_outdata");
  u_int32_t a,b;
  int x;
  string o,as,bs;
  char c;

  while(indata_s >> as >> bs){
    i++;
    a = to_int(as);
    b = to_int(bs);

    x = fslt(a, b);

#if NODATA != 1
    o = to_string1(x);
    e_outdata << o << "\n";    
#endif
    
    t_or_f(a,b,x,to_string(a),to_string(b),i);//デバッグ
    indata_s.get(c);
  }
}

int main(){

#if INPUTOPTION == 1
  randin();
#elif INPUTOPTION == 2
  org_in();
#endif
    
return 0;
}
  

