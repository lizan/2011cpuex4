library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity FSUB is
  port (I1, I2 : in  std_logic_vector(31 downto 0);
        ANS    : out std_logic_vector(31 downto 0));
end FSUB;

architecture FSUB_LOGIC of FSUB is
  component FADD
    port (I1, I2 : in  std_logic_vector(31 downto 0);
          ANS    : out std_logic_vector(31 downto 0));
  end component;

  signal j1,j2 : std_logic_vector(31 downto 0);
  
begin  -- FSUB_LOGIC

  j1 <= I1;
  j2 <= (not I2(31)) & I2(30 downto 0);
  
  FADD0 : FADD port map (I1 => j1, I2 =>j2, ANS => ANS);
  
end FSUB_LOGIC;
