
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.std_logic_signed.all;

entity fpu is  
  port (
    A     : in  std_logic_vector(31 downto 0);  -- input data
    B     : in  std_logic_vector(31 downto 0);  -- input data
    code  : in  std_logic_vector( 3 downto 0);  -- alu制御入力
    O     : out std_logic_vector(31 downto 0);  -- output data
    Z     : out std_logic);
end fpu;

architecture fpumain of fpu is
  signal Otmp : std_logic_vector(31 downto 0) := x"00000000";

  component FADD
    port (I1, I2 : in  std_logic_vector(31 downto 0);
          ANS    : out std_logic_vector(31 downto 0));
  end component;
  component FSUB
    port (I1, I2 : in  std_logic_vector(31 downto 0);
          ANS    : out std_logic_vector(31 downto 0));
  end component;
  component FMUL
    port (I1, I2 : in  std_logic_vector(31 downto 0);
          ANS    : out std_logic_vector(31 downto 0));
  end component;
  component FDIV
    port (I1, I2 : in  std_logic_vector(31 downto 0);
          ANS    : out std_logic_vector(31 downto 0));
  end component;
  component SQRT
    port (I      : in  std_logic_vector(31 downto 0);
          ANS    : out std_logic_vector(31 downto 0));
  end component;
  component FEQ
    port (I1, I2 : in  std_logic_vector(31 downto 0);
          ANS    : out std_logic_vector(31 downto 0));
  end component;
  component FSLT
    port (I1, I2 : in  std_logic_vector(31 downto 0);
          ANS    : out std_logic_vector(31 downto 0));
  end component;


begin  -- fpumain

  Z <= '1' when Otmp = x"00000000" else '0';
  O <= Otmp;
  
  calc: process(A,B,code)
  begin
    case code is
      when "0000" =>
        FADD0 : FADD port map (I1 => A, I2 => B, ANS => Otmp);
      when "0001" =>
        FSUB1 : FSUB port map (I1 => A, I2 => B, ANS => Otmp);
      when "0010" =>
        FMUL2 : FMUL port map (I1 => A, I2 => B, ANS => Otmp);
      when "0011" =>
        FDIV3 : FDIV port map (I1 => A, I2 => B, ANS => Otmp);
      when "0100" =>
        SQRT4 : SQRT port map (I => A, ANS => Otmp);
      when "0101" =>
        FEQ5  : FEQ  port map (I1 => A, I2 => B, ANS => Otmp);
      when "0110" =>
        FSLT5 : FSLT port map (I1 => A, I2 => B, ANS => Otmp);
    end case;
  end process;
end fpumain;
