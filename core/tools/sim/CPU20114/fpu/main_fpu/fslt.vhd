library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity FSLT is
  port (I1, I2 : in  std_logic_vector(31 downto 0);
        ANS    : out std_logic);
end FSLT;

architecture FSLT_LOGIC of FSLT is
  alias s1 : std_logic is I1(31);
  alias s2 : std_logic is I2(31);
  alias e1 : std_logic_vector(7 downto 0) is I1(30 downto 23);
  alias e2 : std_logic_vector(7 downto 0) is I2(30 downto 23);
  signal f1,f2 : std_logic_vector(31 downto 0) := (others => '0');

begin  -- FSLT_LOGIC
  
  f1 <= x"80000000" when e1 = x"00" or e1 = x"ff" else
        '0' & (not I1(30 downto 0)) when s1 = '1' else
        '1' & I1(30 downto 0);

  f2 <= x"80000000" when e2 = x"00" or e2 = x"ff" else
        '0' & (not I2(30 downto 0)) when s2 = '1' else
        '1' & I2(30 downto 0);
  
  --stage1fin

  ANS <= '1' when f1 < f2 else '0';
         
  
end FSLT_LOGIC;
