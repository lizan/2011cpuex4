library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity FEQ is
  port (I1, I2 : in  std_logic_vector(31 downto 0);
        ANS    : out std_logic);
end FEQ;

architecture FEQ_LOGIC of FEQ is
  alias e1 : std_logic_vector(7 downto 0) is I1(30 downto 23);
  alias e2 : std_logic_vector(7 downto 0) is I2(30 downto 23);

begin  -- FEQ_LOGIC

  ANS <= '1' when i1 = i2 or ((e1 = x"00" or e1 = x"ff") and (e2 = x"00" or e2 = x"ff")) else
         '0';
         
end FEQ_LOGIC;
