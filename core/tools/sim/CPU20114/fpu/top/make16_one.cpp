#include <string>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <iomanip>
using namespace std;

uint to_uint(string x){
  int k = x.size();
  uint ans = 0;
  for(int i=0; i<k;i++){
    ans <<= 1;
    if(x.at(i) == '1'){
      ans++;
    }
  }
  return ans;
}

void one_to_one_in(){
  string a;
  char c;
  ifstream in_two("../sqrt/indata");
  ofstream in1("in1arr");
  
  in1 << "constant in1arr : data :=\n(";
  while(in_two >> a){
      in1 << hex;
      in1 << "x\"";
      in1.width(8);
      in1.fill('0');
      in1 << to_uint(a) << "\",";
      in1.width();
      in1.fill();
    in_two.get(c);
  }
  in1 << "x\"00000000\");" << endl;
}

void two_to_one(){
  string a, b;
  char c;
  ifstream in_two("../fdiv/indata");
  ofstream in1("in1arr");
  ofstream in2("in2arr");
  
  in1 << "constant in1arr : data :=\n(";
  in2 << "constant in2arr : data :=\n(";
  while(in_two >> a >> b){
      in1 << hex;
      in2 << hex;
      in1 << "x\"";
      in2 << "x\"";
      in1.width(8);
      in2.width(8);
      in1.fill('0');
      in2.fill('0');
      in1 << to_uint(a) << "\",";
      in2 << to_uint(b) << "\",";
      in1.width();
      in2.width();
      in1.fill();
      in2.fill();
     
    in_two.get(c);
  }
  in1 << "x\"00000000\");" << endl;
  in2 << "x\"00000000\");" << endl;
}

void one_to_one_out(){
  string a;
  char c;
  ifstream in_one("../sqrt/e_outdata");
  ofstream out16("outarr");
  
  out16 << "constant outarr : data :=\n(";
  while(in_one >> a){
    out16 << hex;
    out16 << "x\""; 
    out16.width(8);
    out16.fill('0');
    out16<< to_uint(a) << "\",";
    out16.width();
    out16.fill();
    
    in_one.get(c);
  }
  out16 << "x\"ffffffff\");" << endl;
}

int main(){
  int i;
  cout << "indata(1,2) or indata(3)\n";
  cin >> i; 

  if(i == 1 || i == 2){
    two_to_one();
    one_to_one_out();
  }
  if(i == 3){
    one_to_one_in();
    one_to_one_out();
  }
  cout << i << "__fin" << endl;
  return 0;
}
