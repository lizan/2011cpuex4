library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity SQRT is
  port (I : in  std_logic_vector(31 downto 0);
        ANS    : out std_logic_vector(31 downto 0));
end SQRT;

architecture SQRT_LOGIC of SQRT is
  component FMUL
    port (I1, I2 : in  std_logic_vector(31 downto 0);
          ANS    : out std_logic_vector(31 downto 0));
  end component;

  component SQRT_INV  
    port (I : in  std_logic_vector(31 downto 0);
          ANS : out std_logic_vector(31 downto 0));
  end component;
  
  signal j : std_logic_vector(31 downto 0);
  
begin  -- SQRT_LOGIC

  SQRT_INV0 : SQRT_INV port map (I => I, ANS => j);
  FMUL0 : FMUL port map (I1 => I, I2 =>j, ANS => ANS);
  
end SQRT_LOGIC;
