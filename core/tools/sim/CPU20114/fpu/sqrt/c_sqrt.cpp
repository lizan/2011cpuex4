// 全数チェック完了
// -8以下=0
// -7~359791
// -3=3169031
// -2=18307939
// -1=352130868
//  0=975643091
//  1=561606573
//  2=224113344
//  3=12152503
// ~7 508
// 8以上=0

#include <string>
#include <queue>
#include <vector>
#include <fstream>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#define INPUTOPTION 1
//1の時はrandによる乱数,2の時はindata_sの値をとりだす
//3の時は上位8bitの指定を行ったのち仮数部全数チェックをする
//4の時は符号部以外を全数チェックする
#define NODATA 0
//1の時はindata,outdataの出力を行わない
#define ONTEST 1
//1の時はデバッグ用関数を呼ぶ。indata_sの値を取り出すときは常に呼ぶ
using namespace std;
ofstream missdata("missdata");

//デバッグ用共用体
union Ap{
  u_int32_t ii;
  float ff;
};
union Bp{
  u_int32_t ii;
  float ff;
};
union Xp{
  u_int32_t ii;
  float ff;
};
union Ep{
  u_int32_t ii;
  float ff;
};

//u_int32_tをk文字の文字列に変換
string to_string(u_int32_t l){
  string x;
  for(int i = 0; i < 32; i++){
    if(l & 1U == 1U)
      x = "1" + x;
    else
      x = "0" + x;
    l >>= 1;
  }
  return x;
}
//u_int32_tをk文字の文字列に変換
string to_string64(u_int64_t l){
  string x;
  for(int i = 0; i < 64; i++){
    if(l & 1U == 1U)
      x = "1" + x;
    else
      x = "0" + x;
    l >>= 1;
  }
  return x;
}

//x(a downto b)に相当
u_int64_t subu64(u_int64_t x, int a, int b){//x(a downto b)
  u_int64_t y = (x << (64 - a -1)) >> (64 - a + b - 1);
  return y;
}

u_int32_t mov=0,mfo=0,mth=0,mtw=0,mon=0,zer=0,pon=0,ptw=0,pth=0,pfo=0,pov=0;

/*
  デバッグ用関数
  異なる結果ををmissdataに出力
 */
void t_or_f(u_int32_t a, u_int32_t x, string in, string o, int i){//正誤判定
  u_int32_t ae,be;
  union Ap app;
  union Xp xpp;
  union Bp bpp; 

  ae = a & 0x7f800000;
  if(ae == 0x7f800000 || ae == 0)
    app.ii = 0;
  else
    app.ii = a;

  xpp.ii = x;

  bpp.ff = sqrt(app.ff);
  be = bpp.ii & 0x7f800000;
  if(be == 0x7f800000 || be == 0)
    bpp.ii = 0; 

#if INPUTOPTION == 1 || INPUTOPTION == 2
  union Ep epp;
  float max,diff;
  epp.ii = 0x0c800000;//2^-104
  max = epp.ff;

  if(max < fabs(bpp.ff))
    max = fabs(bpp.ff);
  
  max /= 1048576;//pow(2,20);

  diff = fabs(xpp.ff - bpp.ff);
  if(diff >= max && (xpp.ii!= 00 && bpp.ii != 0))
    missdata << i << "番目\n" << 
      in << "\t" << app.ff << "\tA\n" <<
      o << "\t" << xpp.ff << "\tFSQRT_M(A)\n" <<
      to_string(bpp.ii) << "\t" << bpp.ff << "\t√A\n"<<
      diff << "\t" << max <<  "\t" << epp.ff << "\n";
#elif INPUTOPTION == 3 || INPUTOPTION == 4 

  int dif;
  dif = subu64(xpp.ii,22,0) - subu64(bpp.ii,22,0);

  if(subu64(dif,31,23) == 0x1ff && subu64(dif,22,22) == 0)
    dif -= 0xff800000;
  else if(subu64(dif,31,23) == 0 && subu64(dif,22,22) == 1)
    dif += 0xff800000;

  if(dif <= 7 && dif >= -7)
    switch (dif){
    case -7:
    case -6:
    case -5:
    case -4: mfo++; break;
    case -3: mth++; break;
    case -2: mtw++; break;
    case -1: mon++; break;
    case 0: zer++; break;
    case 1: pon++; break;
    case 2: ptw++; break;
    case 3: pth++; break;
    default : pfo++; break;
    }
  else{
    if(dif > 7)
      pov++;
    else
      mov++;
    missdata << i << "番目\n" << 
      in << "\t" << app.ff << "\tA\n" <<
      o << "\t" << xpp.ff << "\tSQRT_INV(A)\n" <<
      to_string(bpp.ii) << "\t" << bpp.ff << "\t1/√A\n" <<
      to_string(dif) << "\t" << dif << "\n";
  }
#endif
}

/*関数本体   
  u_int32_t型の2数を浮動小数点で足したときの和をu_int32_t型で出力する
  e = 255 は0
 */
u_int32_t fmul(u_int32_t x1, u_int32_t x2){
  u_int32_t s1 = x1 >> 31;
  u_int32_t s2 = x2 >> 31;
  u_int32_t sp;
  u_int32_t e1 = (x1 & 0x7f800000) >> 23;
  u_int32_t e2 = (x2 & 0x7f800000) >> 23;
  u_int32_t ep;
  u_int32_t fu1 = (0x00800000 | (x1 & 0x007ff800)) >> 11;
  u_int32_t fu2 = (0x00800000 | (x2 & 0x007ff800)) >> 11;
  u_int32_t fd1 = (x1 & 0x000007ff);
  u_int32_t fd2 = (x2 & 0x000007ff);
  u_int32_t hh, hl, lh, f3;
  
  if(e1 == 0 || e2 == 0 || e1 == 255 || e2 == 255)
    return 0;
    
  hh = fu1 * fu2;
  hl = fu1 * fd2;
  lh = fd1 * fu2;
  f3 = hh + (hl >> 11) + (lh >> 11) + 2U;
  
  ep = e1 + e2 + 129;
  sp = s1 ^ s2; // xor
  if(f3 > 0x02000000){
    f3 >>= 1;
    ep++;
  }
    
  if(ep > 0x000001fe || ep <= 0x00000100)
    return 0;
  else
    return (sp << 31) | ((0x000000ff & ep) << 23) | ((f3 >> 1)& 0x007fffff); 
} 



union Test{
  u_int32_t ii;
  float ff;
};

u_int32_t keymake_sqrt_inv(u_int32_t f1){
  u_int32_t key;
  if(f1 > 0x119ccaf)
    if(f1 > 0x1754aa5)
      if(f1 > 0x1b4b989)
	if(f1 > 0x1daaf01)
	  if(f1 > 0x1ef8bdd)
	    if(f1 > 0x1fa7f51)    key = 0x1a;
	    else    key = 0x01b;
	  else
	    if(f1 > 0x1e4f25d)    key = 0x1c;
	    else    key = 0x1d;
	else
	  if(f1 > 0x1c71c75)
	    if(f1 > 0x1d0be2d)    key = 0x1e;
	    else    key = 0x1f;
	  else
	    if(f1 > 0x1bdc699)    key = 0x20;
	    else    key = 0x21;
      else
	if(f1 > 0x19324b9)
	  if(f1 > 0x1a36e31)
	    if(f1 > 0x1abf253)    key = 0x22;
	    else    key = 0x23;
	  else
	    if(f1 > 0x19b2a7f)    key = 0x24;
	    else    key = 0x25;
	else
	  if(f1 > 0x183c979)
	    if(f1 > 0x18b5a79)    key = 0x26;
	    else    key = 0x27;
	  else
	    if(f1 > 0x17c6f8f)    key = 0x28;
	    else    key = 0x29;
    else      
      if(f1 > 0x142bcf3)
	if(f1 > 0x15aa2ff)
	  if(f1 > 0x167980f)
	    if(f1 > 0x16e58c7)    key = 0x2a;
	    else    key = 0x2b;
	  else
	    if(f1 > 0x16106b5)    key = 0x2c;
	    else    key = 0x2d;
	else
	  if(f1 > 0x14e5e0b)
	    if(f1 > 0x1546b4d)    key = 0x2e;
	    else    key = 0x2f;
	  else
	    if(f1 > 0x14879bd)    key = 0x30;
	    else    key = 0x31;
      else
	if(f1 > 0x12d3a79)
	  if(f1 > 0x137b483)
	    if(f1 > 0x13d2651)    key = 0x32;
	    else    key = 0x33;
	  else
	    if(f1 > 0x132664d)    key = 0x34;
	    else    key = 0x35;
	else
	  if(f1 > 0x1234569)
	    if(f1 > 0x1282fdf)    key = 0x36;
	    else    key = 0x37;
	  else
	    if(f1 > 0x11e7a05)    key = 0x38;
	    else    key = 0x39;
  else
    if(f1 > 0xbf7c5b)
      if(f1 > 0xf0b84e)
	if(f1 > 0x1083107)
	  if(f1 > 0x110c855)
	    if(f1 > 0x1153c6f)    key = 0x3a;
	    else    key = 0x3b;
	  else
	    if(f1 > 0x10c6f7b)    key = 0x3c;
	    else    key = 0x3d;
	else
	  if(f1 > 0x1000001)
	    if(f1 > 0x1040c21)    key = 0x3e;
	    else    key = 0x3f;
	  else
	    if(f1 > 0xf82f06)    key = 0x00;
	    else    key = 0x01;
      else
	if(f1 > 0xd5fec0)
	  if(f1 > 0xe2c4a8)
	    if(f1 > 0xe99681)    key = 0x02;
	    else    key = 0x03;
	  else
	    if(f1 > 0xdc3e2a)    key = 0x04;
	    else    key = 0x05;
	else
	  if(f1 > 0xca4589)
	    if(f1 > 0xd00271)    key = 0x06;
	    else    key = 0x07;
	  else
	    if(f1 > 0xc4c494)    key = 0x08;
	    else    key = 0x09;
    else      
      if(f1 > 0x9bf1fe)
	if(f1 > 0xac598b)
	  if(f1 > 0xb58a49)
	    if(f1 > 0xba69dd)    key = 0x0a;
	    else    key = 0x0b;
	  else
	    if(f1 > 0xb0db00)    key = 0x0c;
	    else    key = 0x0d;
	else
	  if(f1 > 0xa3d70b)
	    if(f1 > 0xa8039d)    key = 0x0e;
	    else    key = 0x0f;
	  else
	    if(f1 > 0x9fd1ce)    key = 0x10;
	    else    key = 0x11;
      else
	if(f1 > 0x8dc6ae)
	  if(f1 > 0x949b93)
	    if(f1 > 0x9835cf)    key = 0x12;
	    else    key = 0x13;
	  else
	    if(f1 > 0x9121b3)    key = 0x14;
	    else    key = 0x15;
	else
	  if(f1 > 0x8767ac)
	    if(f1 > 0x8a891e)    key = 0x16;
	    else    key = 0x17;
	  else
	    if(f1 > 0x846116)    key = 0x18;
	    else if (f1 > 0x81742e)   key = 0x19;
	    else key = 0x1a;
  return key;
}


/*関数本体   
  u_int32_t型の2数を浮動小数点で足したときの和をu_int32_t型で出力する
  e = 255 は0
  Newton法を2回適用。1回目は6桁(+1)、2回目は11桁(+1)で適用し値を出す。
*/
u_int32_t sqrt_inv(u_int32_t x){
  u_int32_t s1 = x >> 31;
  if(s1 == 1)
    return 0;
  u_int32_t e1 = (x & 0x7f800000) >> 23;
  if(e1 == 0 || e1 == 255)
    return 0;
  u_int32_t ep;
  u_int32_t f1 = (0x00800000 | (x & 0x007fffff));//1+23bit
  u_int64_t f;
  ep = 126U - (e1-127U)/2;
  if(e1%2 == 1)
    f = f1 << 1;
  else
    f = f1;
  u_int64_t fi;
  u_int64_t fp1 = 0x00000040 | keymake_sqrt_inv(f);
  
  u_int64_t fs1,fs2,fs3,fs4,fs5,fs6,fs7,fs8,fp2,fp3;
  
  if(e1%2 == 1){
    if(f1 == 8388608)
      fp3 = 0;
    else{
      fs1 = subu64(f1,23,9)*fp1;//13bit*7bit
      fs2 = subu64(fs1,21,7)*fp1;
      fs3 = subu64(fs2,20,6)*fp1;
      fs4 = fp1 * 3 << 15;
      fp2 = subu64(fs4 - fs3,22,9);

      fs5 = f1 * fp2;//24bit*14bit
      fs6 = subu64(fs5,37,14) * fp2;
      fs7 = subu64(fs6,37,13) * fp2;
      fs8 = fp2 * 3 << 24;
      fp3 = fs8 - fs7 >> 15;
    }
  }
  else{
    fs1 = subu64(f1,23,9)*fp1;//13bit*7bit
    fs2 = subu64(fs1,20,6)*fp1;
    fs3 = subu64(fs2,21,6)*fp1;
    fs4 = fp1 * 3 << 15;
    fp2 = subu64(fs4 - fs3,22,9);

    fs5 = f1 * fp2;//24bit*14bit
    fs6 = subu64(fs5,37,13) * fp2;
    fs7 = subu64(fs6,37,13) * fp2;
    fs8 = fp2 * 3 << 24;
    fp3 = (fs8 - fs7 >> 15)+1;
  }
    
  fi = subu64(fp3,23,0);
  if(fi == 0)
    ep++;
    
  //fi = fp1 << 17;
  //fi = subu64(fp3*f1,46,23);

#if INPUTOPTION == 2  
  cout << 
    to_string64(f1)  << " " << f1  << endl <<
    to_string64(fp1) << " " << fp1 << endl <<
    to_string64(fs1) << " " << fs1 << endl <<
    to_string64(fs2) << " " << fs2 << endl <<
    to_string64(fs3) << " " << fs3 << endl <<
    to_string64(fs4) << " " << fs4 << endl <<
    to_string64(fp2) << " " << fp2 << endl << endl;
  cout << 
    to_string64(f1)  << " " << f1  << endl <<
    to_string64(fp2) << " " << fp2 << endl <<
    to_string64(fs5) << " " << fs5 << endl <<
    to_string64(fs6) << " " << fs6 << endl <<
    to_string64(fs7) << " " << fs7 << endl <<
    to_string64(fs8) << " " << fs8 << endl <<
    to_string64(fp3) << " " << fp3 << endl << endl <<
    to_string64(fi)  << " " << fi  << endl << endl <<
    to_string(ep)  << " " << ep  << endl << endl;
#endif
    
  if(ep >> 8 == 1)
    return 0;
  else
    return (s1 << 31) | (ep << 23) | (fi & 0x007fffff);
}


u_int32_t sqrt_m(u_int32_t x){
  if(x >> 31 == 1)
    return 0;
  else
    return fmul(x,sqrt_inv(x));
}

/*関数本体   
  u_int32_t型の2数を浮動小数点で足したときの和をu_int32_t型で出力する
  e = 255 は0
  Newton法を2回適用。1回目は6桁(+1)、2回目は11桁(+1)で適用し値を出す。
 */
u_int32_t sqrt2(u_int32_t x){
  u_int32_t s1 = x >> 31;
  u_int32_t e1 = (x & 0x7f800000) >> 23;
  if(e1 == 0)
    return 0;
  u_int32_t ep;
  u_int64_t f1 = (0x00800000 | (x & 0x007fffff));//1+23bit
  union Test tes;
  if(e1 % 2 == 1)
    tes.ff = sqrt_m(f1<<1);
  else
    tes.ff = sqrt_m(f1);
  ep = (e1-127U)/2+127U;

  u_int64_t fi;
  //u_int64_t fp1 = (0x00000040 | keymake_sqrt(f1));
  
  u_int64_t fp1 = 0x00000040 | subu64(tes.ii, 22, 17);
  u_int64_t fs1,fs2,fs3,fs4,fs5,fp2;
    
  if(fp1 == 0)
    return 0;
  fs1 = f1 / fp1;
  if(e1 % 2 == 1){
    fs2 = (fp1<<11) + (fs1);
    fp2 = subu64(fs2,18,7);
    if(fp2 == 0)
      return 0;
    fs4 = (f1<<10) / fp2;
    fs5 = (fp2<<11) + fs4;
    fi = fs5;
  }
  else{
    fs2 = (fp1<<10) + (fs1);    
    fp2 = subu64(fs2,17,6);
    if(fp2 == 0)
      return 0;

    fs4 = (f1<<11) / fp2;
    fs5 = (fp2<<11) + fs4;
    fi = fs5;
  }

  if(ep >= 0x00000100 || ep == 0U)
    return 0;
  else 
    return (s1 << 31) | (ep << 23) | (fi & 0x007fffff);
}



//ランダムに値を生成し演算を実行する。
void randin(){
  long int k,i,s;
  ofstream indata("indata");
  ofstream e_outdata("e_outdata");
  string in,o;
  u_int32_t a, x;
  
  cout << "回数　srand定数\n";
  cin >> k >> s;  
  srand(s);

  for(i = 0; i < k; i++){
    a = rand() + rand();

    x = sqrt_m(a);

    in = to_string(a);
    o = to_string(x);

    indata << in << "\n";
    e_outdata << o << "\n";

#if ONTEST == 1   
    t_or_f(a, x, in, o, i);//デバッグ
#endif

    if((i*100) % k == 0)
      cout << i*100/k << '\n';
  }
}

//01文字列をu_intに変換
u_int32_t to_int(string x){
  int k = x.size();
  u_int32_t ans = 0;
  for(int i = 0; i<k;i++){
    ans <<= 1;
    if(x.at(i) == '1'){
      ans++;
    }
  }
  return ans;
}

/*
  indata_sに入っているデータについて演算を行う
  indata_sには間違いが起こりやすそうなものが入っている。
*/
void org_in(){
  int i = 0;
  ifstream indata_s("indata_s");
  ofstream e_outdata("e_outdata");
  u_int32_t a,x;
  string o,as;
  char c;
  
  while(indata_s >> as){
    i++;
    a = to_int(as);

    x = sqrt_m(a);
    
    o = to_string(x);

    e_outdata << o << endl;
    
    t_or_f(a,x,to_string(a), o, i);
    indata_s.get(c);
  }
}

//指数部最下位bitと仮数部全数チェック
void all_in(){
  string in,o;
  u_int32_t i=0,a,b,x;
  ofstream indata("indata");
  ofstream e_outdata("e_outdata");
  a = 0x76;
  printf("上位8bitは0x%x\nカウント256で終了します\n",a);
  a <<= 24;
  while(i < 0x01000000){
    b = a + i;
    
    x = sqrt_m(b);

    in = to_string(b);
    o = to_string(x);
    
#if NODATA != 1
    indata << in << endl;
    e_outdata << o << endl;
#endif

    t_or_f(b,x,in,o,i);
    i++;

    if((i & 0x0000ffff) == 0)
      cout << (i >>16)<< '\n';
  }
}

void forall_in(){
  u_int32_t i=0,a,x;
  ofstream indata("indata");
  ofstream e_outdata("e_outdata");
  a <<= 23;
  for(i=0;i < 0x80000000; i++){
    
    x = sqrt_m(i);

    t_or_f(i,x,to_string(i),to_string(x),i);
    
    if((i & 0x003fffff) == 0){
      cout << (i >> 22)<< '\n';
      if((i & 0x1fffffff) == 0)
	cout << "全数チェック中...カウント512で終了します\n";
    }
  }
}



//main
int main(){

#if INPUTOPTION == 1
  randin();
#elif INPUTOPTION == 2
  org_in();
#elif INPUTOPTION == 3
  all_in();
  printf("-8以下=%d\n-7~%d\n-3=%d\n-2=%d\n-1=%d\n 0=%d\n 1=%d\n 2=%d\n 3=%d\n~7 %d\n8以上=%d\n",
	 mov,mfo,mth,mtw,mon,zer,pon,ptw,pth,pfo,pov);
#elif INPUTOPTION == 4
  forall_in();
  printf("-8以下=%d\n-7~%d\n-3=%d\n-2=%d\n-1=%d\n 0=%d\n 1=%d\n 2=%d\n 3=%d\n~7 %d\n8以上=%d\n",
	 mov,mfo,mth,mtw,mon,zer,pon,ptw,pth,pfo,pov);
#endif
    
  return 0;
}

 
