library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity FMUL is  
  port (I1, I2 : in  std_logic_vector(31 downto 0);
        ANS    : out std_logic_vector(31 downto 0));
end FMUL;

architecture FMUL_LOGIC of FMUL is
  alias s1 : std_logic is I1(31);
  alias s2 : std_logic is I2(31);
  alias e1 : std_logic_vector(7 downto 0) is I1(30 downto 23);
  alias e2 : std_logic_vector(7 downto 0) is I2(30 downto 23);
  alias f1a : std_logic_vector(11 downto 0) is I1(22 downto 11);
  alias f2a : std_logic_vector(11 downto 0) is I2(22 downto 11);
  alias f1b : std_logic_vector(10 downto 0) is I1(10 downto 0);
  alias f2b : std_logic_vector(10 downto 0) is I2(10 downto 0);
  signal hh,f3 : std_logic_vector(25 downto 0) := (others => '0');
  signal fh : std_logic_vector(26 downto 0) := (others => '0');
  signal hl,lh : std_logic_vector(23 downto 0) := (others => '0');
  signal ep,epx,ex : std_logic_vector(8 downto 0) := (others => '0');
  signal s,sp,spx,ek1,ek2,ek3 : std_logic := '0';

begin  -- FMUL_LOGIC
  
  ep <= ("0"& e1) + ("0" & e2) + "010000001";  --+129
  s  <= s1 xor s2;
  hh <= ("1" & f1a) * ("1" & f2a);
  hl <= f1b * ("1" & f2a);
  lh <= ("1" & f1a) * f2b;
  ek1 <= '1' when e1 = x"00" or e1 = x"ff" or e2 = x"00" or e2 = x"ff" else
         '0';
  
  fh <= ('0' & hh) + ("00000000000000" & hl(23 downto 11)) + ("00000000000000" & lh(23 downto 11))
        + "000000000000000000000000010";
  ex <= ep;
  sp <= s;
  ek2 <= ek1;

  
  f3 <= '0' & fh(26 downto 2) when fh >= "100000000000000000000000000" else
        fh(26 downto 1) when fh >= "010000000000000000000000000" else
        fh(25 downto 0);
  epx <= ep + "000000010" when fh >= "100000000000000000000000000" else
         ep + "000000001" when fh >= "010000000000000000000000000" else
        ep;
  spx <= sp;
  ek3 <= ek2;
  
  ANS <= x"00000000" when epx > "111111110" or epx <= "100000000" or ek3 = '1' else
         sp & epx(7 downto 0) & f3(23 downto 1);
end FMUL_LOGIC;
