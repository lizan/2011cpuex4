#-------------------------------------------------
#
# Project created by QtCreator 2011-10-07T20:02:01
#
#-------------------------------------------------

QT       += core

QT       -= gui

LIBS  += -L../ugl0sim-lib -lugl0sim-lib
INCLUDEPATH += ../ugl0sim-lib

TARGET = ugl0sim-cui
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cc
