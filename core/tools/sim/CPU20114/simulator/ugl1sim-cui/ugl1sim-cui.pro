#-------------------------------------------------
#
# Project created by QtCreator 2011-10-07T20:02:01
#
#-------------------------------------------------

QT       += core

QT       -= gui

LIBS  += -L../ugl1sim-lib -lugl1sim-lib
INCLUDEPATH += ../ugl1sim-lib

TARGET = ugl1sim-cui
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cc
