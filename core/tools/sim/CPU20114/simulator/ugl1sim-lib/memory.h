#ifndef MEMORY_H
#define MEMORY_H

#include <QObject>
#include <QHash>

namespace ugl0sim {

union Cell {
  int i;
  float f;
};


class Memory : public QObject
{
  Q_OBJECT
 public:
  explicit Memory(QObject *parent = 0);
  virtual ~Memory();
  static const int BLOCK_SIZE;
  static const int WORD_SIZE;

 private:

  Cell* data;
 signals:

 public slots:
  int getWord(unsigned int index);
  void setWord(unsigned int index, int data);
  float getFloat(unsigned int index);
  void setFloat(unsigned int index, float data);

};

inline int Memory::getWord(unsigned int index) {
  return data[index].i;
}

inline void Memory::setWord(unsigned int index, int data) {
  this->data[index].i = data;
}


} // namespace ugl0sim

#endif // MEMORY_H
