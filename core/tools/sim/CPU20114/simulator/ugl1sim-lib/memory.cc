#include "memory.h"

namespace ugl0sim {

const int Memory::BLOCK_SIZE(8192);
const int Memory::WORD_SIZE(4);

Memory::Memory(QObject *parent) : QObject(parent), data(new Cell[20000000]) {
}

Memory::~Memory() {
}

float Memory::getFloat(unsigned int index) {
  return data[index].f;
}

void Memory::setFloat(unsigned int index, float data) {
  this->data[index].f = data;
}

} // namespace ugl0sim
