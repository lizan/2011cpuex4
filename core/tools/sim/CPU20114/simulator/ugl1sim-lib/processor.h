#ifndef PROCESSOR_H
#define PROCESSOR_H

#include <QObject>
#include <QDataStream>
#include <vector>
#include "memory.h"
#include "instruction.h"
#include "ugl0sim-lib_global.h"

namespace ugl0sim {

const unsigned int kInstructionSize = sizeof(unsigned int);

class Processor : public QObject
{
  Q_OBJECT
 public:
  explicit Processor(QObject *parent = 0);
  void setInput(QDataStream* input) { this->input = input; }
  void setOutput(QDataStream* output) { this->output = output; }
 private:
  unsigned int* register_;

  unsigned int program_counter_;
  Memory memory_;
  QDataStream *input, *output;
  Program program_;

  bool isRunning;

 signals:
  void registerChanged(int, int);

 public slots:
  void reset();
  void incrementCounter();
  void setCounter(unsigned int count);
  unsigned int getCounter();
  void setRegister(unsigned int index, int data);
  int getRegister(unsigned int index);
  Memory* getMemory();
  void loadProgram(const Program& program);
  const Program& getProgram();
  void run();
  void stop();
  void read(unsigned int index);
  void write(unsigned int index);

};

inline void Processor::incrementCounter() {
  ++program_counter_;
}

inline void Processor::setCounter(unsigned int count) {
  program_counter_ = count;
}

inline unsigned int Processor::getCounter() {
  return program_counter_;
}

inline void Processor::setRegister(unsigned int index, int data) {
  register_[index] = data;
  qDebug("%010d: set r%02d = %x", program_counter_, index, data);
}

inline int Processor::getRegister(unsigned int index) {
  return register_[index];
}

inline Memory* Processor::getMemory() {
  return &memory_;
}

inline void Processor::read(unsigned int index) {
  quint8 data;
  (*input) >> data;
  setRegister(index, data);
}

inline void Processor::write(unsigned int index) {
  (*output) << quint8(getRegister(index));
}

} // namespace ugl0sim

#endif // PROCESSOR_H
