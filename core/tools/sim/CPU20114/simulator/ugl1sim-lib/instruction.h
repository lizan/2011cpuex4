#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#include <QObject>
#include <QString>

namespace ugl0sim {

class Processor;

class Instruction {
 protected:
  QString description;

  bool isBreakPoint;

 public:
  int run_count;
  Instruction() : run_count(0), isBreakPoint(false) {}

#ifdef ENABLE_BREAKPOINT
  void setBreakPoint(bool b) {
    isBreakPoint = b;
  }

  bool isBreak() const {
    return isBreakPoint;
  }

  void operator()(Processor* processor) {
    run(processor);
    if (isBreakPoint)
      break_(processor);
  }
#else
  void operator()(Processor* processor) {
    run(processor);
//    ++run_count;
  }
#endif

  void break_(Processor* processor);

  const QString& getDescription() const {
    return description;
  }

  virtual void run(Processor* processor) = 0;
};

Instruction* newInstruction(const unsigned int& op);
Instruction* endInstruction();

typedef QList<Instruction*> Program;

} // namespace ugl0sim

#endif // INSTRUCTION_H
