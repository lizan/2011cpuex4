#-------------------------------------------------
#
# Project created by QtCreator 2011-11-07T03:07:25
#
#-------------------------------------------------

QT       += core gui

TARGET = ugl1sim-gui
TEMPLATE = app

LIBS  += -L../ugl1sim-lib -lugl1sim-lib
INCLUDEPATH += ../ugl1sim-lib

SOURCES +=\
    mainwidget.cc \
    mainwindow.cc \
    main.cc

HEADERS  += mainwindow.h \
    mainwidget.h






