#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>

QT_BEGIN_NAMESPACE
class QTableWidget;
class QLabel;
class QPushButton;
QT_END_NAMESPACE

namespace ugl0sim {
class Processor;
}

class MainWidget : public QWidget
{
  Q_OBJECT
 public:
  explicit MainWidget(ugl0sim::Processor* processor, QWidget *parent = 0);

 signals:

 public slots:
  void syncStatus();
  void run();
  void step();
  void threadFinishied();

 private:
  QTableWidget* registerWidget;
  QLabel* pcLabel;
  QPushButton* runButton;
  QPushButton* stepButton;
  QPushButton* resetButton;
  ugl0sim::Processor* processor;
  bool isRunning;
};

#endif // MAINWIDGET_H
