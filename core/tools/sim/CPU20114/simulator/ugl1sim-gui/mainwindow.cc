#include <QtGui>

#include "mainwindow.h"
#include "mainwidget.h"
#include "instruction.h"
#include "processor.h"

using ugl0sim::Instruction;
using ugl0sim::Program;
using ugl0sim::Processor;
using ugl0sim::newInstruction;

/*
MainWindow::MainWindow() {
  createActions();
  createMenus();
  createStatusBar();

  readSettings();

  setUnifiedTitleAndToolBarOnMac(true);
}*/

MainWindow::MainWindow(const QString &fileName) : processor(new Processor), QMainWindow() {
  createActions();
  createMenus();
  createStatusBar();

  readSettings();

  setUnifiedTitleAndToolBarOnMac(true);
  setCentralWidget(new QLabel(" Please open a program file."));
  if (!fileName.isEmpty()) {
    loadFile(fileName);
  }
}

void MainWindow::closeEvent(QCloseEvent *event) {
/*  if (maybeSave()) {
    writeSettings();
    event->accept();
  } else {
    event->ignore();
  }*/
  event->accept();
}

void MainWindow::open() {
  QString fileName;
  do {
    fileName = QFileDialog::getOpenFileName(this);
    if (fileName.isEmpty() && !windowFilePath().isEmpty()) break;
  } while (!loadFile(fileName));
}

void MainWindow::about() {
   QMessageBox::about(this, tr("About Application"),
      tr("The <b>Application</b> example demonstrates how to "
         "write modern GUI applications using Qt, with a menu bar, "
         "toolbars, and a status bar."));
}

void MainWindow::documentWasModified() {
}

void MainWindow::createActions() {
  openAct = new QAction(tr("&Open..."), this);
  openAct->setShortcuts(QKeySequence::Open);
  openAct->setStatusTip(tr("Open an existing file"));
  connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

  exitAct = new QAction(tr("E&xit"), this);
  exitAct->setShortcuts(QKeySequence::Quit);
  exitAct->setStatusTip(tr("Exit the application"));
  connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));

  cutAct = new QAction(tr("Cu&t"), this);
  cutAct->setShortcuts(QKeySequence::Cut);
  cutAct->setStatusTip(tr("Cut the current selection's contents to the "
              "clipboard"));
//  connect(cutAct, SIGNAL(triggered()), textEdit, SLOT(cut()));

  copyAct = new QAction(tr("&Copy"), this);
  copyAct->setShortcuts(QKeySequence::Copy);
  copyAct->setStatusTip(tr("Copy the current selection's contents to the "
               "clipboard"));
//  connect(copyAct, SIGNAL(triggered()), textEdit, SLOT(copy()));

  aboutAct = new QAction(tr("&About"), this);
  aboutAct->setStatusTip(tr("Show the application's About box"));
  connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

  aboutQtAct = new QAction(tr("About &Qt"), this);
  aboutQtAct->setStatusTip(tr("Show the Qt library's About box"));
  connect(aboutQtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));

  cutAct->setEnabled(false);
  copyAct->setEnabled(false);
//  connect(textEdit, SIGNAL(copyAvailable(bool)),
//      cutAct, SLOT(setEnabled(bool)));
//  connect(textEdit, SIGNAL(copyAvailable(bool)),
//      copyAct, SLOT(setEnabled(bool)));
}

void MainWindow::createMenus() {
  fileMenu = menuBar()->addMenu(tr("&File"));
  fileMenu->addAction(openAct);
  fileMenu->addSeparator();
  fileMenu->addAction(exitAct);

  editMenu = menuBar()->addMenu(tr("&Edit"));
  editMenu->addAction(cutAct);
  editMenu->addAction(copyAct);

  menuBar()->addSeparator();

  helpMenu = menuBar()->addMenu(tr("&Help"));
  helpMenu->addAction(aboutAct);
  helpMenu->addAction(aboutQtAct);
}

void MainWindow::createToolBars()
{
  fileToolBar = addToolBar(tr("File"));
  fileToolBar->addAction(openAct);

  editToolBar = addToolBar(tr("Edit"));
  editToolBar->addAction(cutAct);
  editToolBar->addAction(copyAct);
}

void MainWindow::createStatusBar() {
  statusBar()->showMessage(tr("Ready"));
}

void MainWindow::readSettings() {
  QSettings settings("Trolltech", "Application Example");
  QPoint pos = settings.value("pos", QPoint(200, 200)).toPoint();
  QSize size = settings.value("size", QSize(600, 400)).toSize();
  resize(size);
  move(pos);
}

void MainWindow::writeSettings() {
  QSettings settings("Trolltech", "Application Example");
  settings.setValue("pos", pos());
  settings.setValue("size", size());
}

bool MainWindow::loadFile(const QString &fileName) {
  QFile file(fileName);
  if (!file.open(QFile::ReadOnly)) {
    setCentralWidget(new QLabel(
               tr("Cannot read file %1:\n%2.")
               .arg(fileName)
               .arg(file.errorString())));
    return false;
  }

  QApplication::setOverrideCursor(Qt::WaitCursor);

  QDataStream program_stream(&file);
  program_stream.setByteOrder(QDataStream::BigEndian);

  qDebug("parsing...");
  while(!program_stream.atEnd()) {
    unsigned int instruction;
    program_stream >> instruction;
    Instruction* inst = newInstruction(instruction);
    if (inst == NULL) {
      qDebug("Unable parse %08x at %d", instruction, program.size());
      statusBar()->showMessage(tr("File load failure"), 2000);
      QApplication::restoreOverrideCursor();
      return false;
    } else {
//        qDebug("%03d: %s", program.size());//, inst->getDescription().toUtf8());
      qDebug() << program.size() << inst->getDescription();
    }
    program.push_back(inst);
  }

  file.close();

  processor->loadProgram(program);

  QApplication::restoreOverrideCursor();

  setCurrentFile(fileName);
  statusBar()->showMessage(tr("File loaded"), 2000);

  mainWidget = new MainWidget(processor, this);
  setCentralWidget(mainWidget);
  return true;
}

void MainWindow::setCurrentFile(const QString &fileName) {
  curFile = fileName;
  setWindowModified(false);

  QString shownName = curFile;
  if (curFile.isEmpty())
    shownName = "untitled";
  setWindowFilePath(shownName);
}

QString MainWindow::strippedName(const QString &fullFileName) {
  return QFileInfo(fullFileName).fileName();
}
