#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "instruction.h"
namespace ugl0sim {
class Processor;
}

QT_BEGIN_NAMESPACE
class QAction;
class QMenu;
class QTextEdit;
QT_END_NAMESPACE

class MainWidget;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(const QString &fileName = "");

protected:
    void closeEvent(QCloseEvent *event);

private slots:
    void open();
    void about();
    void documentWasModified();

private:
    void init();
    void createActions();
    void createMenus();
    void createToolBars();
    void createStatusBar();
    void readSettings();
    void writeSettings();
    bool loadFile(const QString &fileName);
    void setCurrentFile(const QString &fileName);
    QString strippedName(const QString &fullFileName);
    MainWindow *findMainWindow(const QString &fileName);

    QString curFile;
    bool isUntitled;

    MainWidget *mainWidget;

    QMenu *fileMenu;
    QMenu *editMenu;
    QMenu *helpMenu;
    QToolBar *fileToolBar;
    QToolBar *editToolBar;
    QAction *openAct;
    QAction *closeAct;
    QAction *exitAct;
    QAction *cutAct;
    QAction *copyAct;
    QAction *aboutAct;
    QAction *aboutQtAct;

    ugl0sim::Program program;
    ugl0sim::Processor* processor;
};

#endif
