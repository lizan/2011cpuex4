#include "mainwidget.h"
#include "processor.h"
#include <QtGui>

using ugl0sim::Processor;

namespace {
union Cell {
  unsigned int ui;
  int i;
  float f;
};

class RunningThread : public QThread {
 private:
  Processor* processor;
 public:
  explicit RunningThread(Processor *processor) : QThread(), processor(processor) {}
  void run() {
    connect(this, SIGNAL(finished()), this, SLOT(deleteLater()));
    processor->run();
  }
};

}

MainWidget::MainWidget(Processor* processor, QWidget *parent)
    : QWidget(parent), processor(processor), isRunning(false) {
  registerWidget = new QTableWidget(32, 5);
  QStringList registerLabelList;
  QFont monospace("monospace");
  monospace.setStyleHint(QFont::TypeWriter);
  for (int i = 0; i < 32; ++i) {
    registerLabelList.append(QString("r[%1]").arg(i, 2, 16, QChar('0')));
    registerWidget->setItem(i, 0, new QTableWidgetItem);
    registerWidget->setItem(i, 1, new QTableWidgetItem);
    registerWidget->item(i, 0)->setTextAlignment(Qt::AlignRight);
    registerWidget->item(i, 1)->setTextAlignment(Qt::AlignRight);
    registerWidget->item(i, 0)->setFont(monospace);
    registerWidget->item(i, 1)->setFont(monospace);
  }
  for (int i = 0; i < 32; ++i) {
    registerWidget->setItem(i, 3, new QTableWidgetItem);
    registerWidget->setItem(i, 4, new QTableWidgetItem);
    registerWidget->item(i, 3)->setTextAlignment(Qt::AlignRight);
    registerWidget->item(i, 4)->setTextAlignment(Qt::AlignRight);
    registerWidget->item(i, 3)->setFont(monospace);
    registerWidget->item(i, 4)->setFont(monospace);
  }
  registerWidget->resizeRowsToContents();
  registerWidget->setVerticalHeaderLabels(registerLabelList);
  registerWidget->setHorizontalHeaderLabels(QString("Int Value,Int Hex, ,Float Value, Float Hex").split(","));

  QVBoxLayout* registerLayout = new QVBoxLayout;
  registerLayout->addWidget(new QLabel("Registers: "));
  registerLayout->addWidget(registerWidget);

  QFormLayout* rightLayout = new QFormLayout;
  pcLabel = new QLabel("000000");
  rightLayout->addRow("Program Counter: ", pcLabel);

  QGridLayout* buttonsLayout = new QGridLayout;
  runButton = new QPushButton("Run");
  stepButton = new QPushButton("Step");
  resetButton = new QPushButton("Reset");
  buttonsLayout->addWidget(runButton, 0, 0, 1, 2);
  buttonsLayout->addWidget(stepButton, 1, 0);
  buttonsLayout->addWidget(resetButton, 1, 1);

  connect(runButton, SIGNAL(pressed()), this, SLOT(run()));
  connect(stepButton, SIGNAL(pressed()), this, SLOT(step()));
  connect(resetButton, SIGNAL(pressed()), processor, SLOT(reset()));
  connect(resetButton, SIGNAL(pressed()), this, SLOT(syncStatus()));

  rightLayout->addRow(buttonsLayout);

//  buttonsLayout->addRow();

  QHBoxLayout* mainLayout = new QHBoxLayout;
  mainLayout->addLayout(registerLayout);
  mainLayout->addLayout(rightLayout);
  setLayout(mainLayout);
//  addWidget(new QTextEdit(this));

  syncStatus();
//  pcLabel->setEnabled(false);
//  registerWidget->setEnabled(false);
}

void MainWidget::syncStatus() {
  for (int i = 0; i < 32; ++i) {
    Cell r;
    r.ui = processor->getRegister(i);
    registerWidget->item(i, 0)->setText(QString("%1").arg(r.i, 10));
    registerWidget->item(i, 1)->setText(QString("0x%1").arg(r.ui, 8, 16, QChar('0')));
  }
  for (int i = 0; i < 32; ++i) {
    Cell r;
    r.ui = processor->getRegister(i | 0x20);
    registerWidget->item(i, 3)->setText(QString("%1").arg(r.f, 0, 'e'));
    registerWidget->item(i, 4)->setText(QString("0x%1").arg(r.ui, 8, 16, QChar('0')));
  }
  registerWidget->resizeColumnsToContents();
  pcLabel->setText(QString("%1").arg(processor->getCounter(), 6, 10, QChar('0')));
  if (processor->getCounter() >= processor->getProgram().size()) {
    runButton->setEnabled(false);
    stepButton->setEnabled(false);
  } else {
    runButton->setEnabled(true);
    stepButton->setEnabled(true);
  }
}

void MainWidget::run() {
  if (!isRunning) {
    runButton->setText("Pause");
    RunningThread* thread = new RunningThread(processor);
    connect(thread, SIGNAL(finished()), this, SLOT(threadFinishied()));
    thread->start();
    isRunning = true;
    pcLabel->setEnabled(false);
    registerWidget->setEnabled(false);
    stepButton->setEnabled(false);
  } else {
    processor->stop();
  }
}

void MainWidget::threadFinishied() {
  isRunning = false;
  runButton->setText("Run");
  stepButton->setEnabled(true);
  qDebug("finished");
  syncStatus();
  registerWidget->setEnabled(true);
  pcLabel->setEnabled(true);
}

void MainWidget::step() {
  if (processor->getCounter() < processor->getProgram().size()) {
    (*processor->getProgram()[processor->getCounter()])(processor);
  }
  syncStatus();
}
