#include <QtGui/QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainWindow* w;

    if (a.argc() > 1) {
      w = new MainWindow(a.arguments().at(1));
    } else {
      w = new MainWindow;
    }

    w->show();

    return a.exec();
}
