#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#include <QObject>
#include <QPointer>

namespace ugl0sim {

class Processor;

class Instruction : public QObject {
  Q_OBJECT
 public:
  int run_count;
  Instruction() : run_count(0) {}

  void operator()(Processor* processor) {
    run(processor);
//    ++run_count;
  }

  virtual void run(Processor* processor) = 0;
};

Instruction* newInstruction(const unsigned int& op);

typedef QList<Instruction*> Program;

} // namespace ugl0sim

#endif // INSTRUCTION_H
