#-------------------------------------------------
#
# Project created by QtCreator 2011-10-06T00:47:54
#
#-------------------------------------------------

QT       -= gui

TARGET = ugl0sim-lib
TEMPLATE = lib
CONFIG += staticlib

DEFINES += UGL0SIMLIB_LIBRARY

SOURCES += \
    processor.cc \
    memory.cc \
    instruction.cc

HEADERS +=\
        ugl0sim-lib_global.h \
    processor.h \
    memory.h \
    instruction.h

unix {
    target.path = /usr/local/lib
    INSTALLS += target
}
