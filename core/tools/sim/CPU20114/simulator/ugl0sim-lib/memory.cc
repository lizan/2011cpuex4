#include "memory.h"

namespace ugl0sim {

const int Memory::BLOCK_SIZE(8192);
const int Memory::WORD_SIZE(4);

Memory::Memory(QObject *parent) : QObject(parent), data() {
}

Memory::~Memory() {
}

std::string& Memory::getBlock(unsigned int index) {
  if (data.find(index) == data.end()) {
    data[index].assign(BLOCK_SIZE, 0);
  }
  return data[index];
}

unsigned char Memory::getByte(unsigned int index) {
  unsigned int block_index = index / BLOCK_SIZE;
  unsigned int local_index = index % BLOCK_SIZE;
  return getBlock(block_index)[local_index];
}

int Memory::getWord(unsigned int index) {
  int retval = 0;
  for (int i = 0; i < WORD_SIZE; ++i) {
    retval <<= 8;
    retval |= getByte(index + i);
  }
  return retval;
}

void Memory::setByte(unsigned int index, unsigned char data) {
  unsigned int block_index = index / BLOCK_SIZE;
  unsigned int local_index = index % BLOCK_SIZE;
  getBlock(block_index)[local_index] = data;
}

void Memory::setWord(unsigned int index, int data) {
  for (int i = WORD_SIZE - 1; i >= 0; --i) {
    setByte(index + i, data & 0xFF);
//    qDebug("%08x", data);
    data >>= 8;
  }
}

} // namespace ugl0sim
