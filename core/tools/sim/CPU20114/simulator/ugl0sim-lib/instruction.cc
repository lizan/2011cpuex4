#include "instruction.h"
#include "processor.h"
#include <functional>

namespace ugl0sim {

namespace {

#define DEFINE_INSTRUCTION_PART(NAME, MASK, BIT) \
  const unsigned int k##NAME##Mask = MASK; \
  const unsigned int k##NAME##Bit = BIT; \
  inline unsigned int get##NAME(const unsigned int op) { \
    return (op & k##NAME##Mask) >> k##NAME##Bit; \
  }

DEFINE_INSTRUCTION_PART(Opcode, 0xFC000000, 26)
DEFINE_INSTRUCTION_PART(Funct,  0x0000003F,  0)
DEFINE_INSTRUCTION_PART(Rs,     0x03E00000, 21)
DEFINE_INSTRUCTION_PART(Rt,     0x001F0000, 16)
DEFINE_INSTRUCTION_PART(Rd,     0x0000F800, 11)
DEFINE_INSTRUCTION_PART(ImI,    0x0000FFFF,  0)
DEFINE_INSTRUCTION_PART(ImJ,    0x03FFFFFF,  0)

#undef DEFINE_INSTRUCTION_PART

template<typename _T>
class RInstruction : public Instruction {
 protected:
  unsigned int rs, rt, rd;
  _T funct;
 public:
  RInstruction(unsigned int op) : Instruction(), funct() {
    rs = getRs(op);
    rt = getRt(op);
    rd = getRd(op);
  }
  virtual void run(Processor* processor) {
    processor->setRegister(
        rd,
        funct(processor->getRegister(rs), processor->getRegister(rt)));
    processor->incrementCounter();
  }
};

typedef RInstruction<std::plus<int> > AddInstruction;
typedef RInstruction<std::minus<int> > SubInstruction;
#if __APPLE__
template<typename _Tp>
  struct bit_and : public std::binary_function<_Tp, _Tp, _Tp>
  {
    _Tp
    operator()(const _Tp& __x, const _Tp& __y) const
    { return __x & __y; }
  };

template<typename _Tp>
  struct bit_or : public std::binary_function<_Tp, _Tp, _Tp>
  {
    _Tp
    operator()(const _Tp& __x, const _Tp& __y) const
    { return __x | __y; }
  };
#else
using std::bit_and;
using std::bit_or;
#endif
typedef RInstruction<bit_and<int> > AndInstruction;
typedef RInstruction<bit_or<int> > OrInstruction;
typedef RInstruction<std::less<int> > SltInstruction;
typedef RInstruction<std::equal_to<int> > SeqInstruction;

template<typename _T>
class IInstruction : public Instruction {
 protected:
  unsigned int rs, rt;
  short im;
  _T funct;
 public:
  IInstruction(unsigned int op) : Instruction(), funct() {
    rs = getRs(op);
    rt = getRt(op);
    im = (short)getImI(op);
  }
  virtual void run(Processor* processor) {
    processor->setRegister(
        rt,
        funct(processor->getRegister(rs), im));
    processor->incrementCounter();
  }
};

class BeqInstruction : public IInstruction<std::plus<int> > {
 public:
  BeqInstruction(unsigned int op) : IInstruction<std::plus<int> >(op) {}
  virtual void run(Processor* processor) {
    if (processor->getRegister(rs) == processor->getRegister(rt)) {
      processor->setCounter(processor->getCounter() + im);
    }
    processor->incrementCounter();
  }
};

class JrInstruction : public IInstruction<std::plus<int> > {
 public:
  JrInstruction(unsigned int op) : IInstruction<std::plus<int> >(op) {}
  virtual void run(Processor* processor) {
    processor->setCounter(processor->getRegister(rs));
  }
};

class LwInstruction : public IInstruction<std::plus<int> > {
 public:
  LwInstruction(unsigned int op) : IInstruction<std::plus<int> >(op) {}
  virtual void run(Processor* processor) {
    processor->setRegister(
        rt,
        processor->getMemory()->getWord(
            processor->getRegister(rs) + im));
    processor->incrementCounter();
  }
};

class SwInstruction : public IInstruction<std::plus<int> > {
 public:
  SwInstruction(unsigned int op) : IInstruction<std::plus<int> >(op) {}
  virtual void run(Processor* processor) {
    processor->getMemory()->setWord(processor->getRegister(rs) + im,
                                    processor->getRegister(rt));
    processor->incrementCounter();
  }
};

class ReadInstruction : public IInstruction<std::plus<int> > {
 public:
  ReadInstruction(unsigned int op) : IInstruction<std::plus<int> >(op) {}
  virtual void run(Processor* processor) {
    processor->read(rs);
    processor->incrementCounter();
  }
};

class WriteInstruction : public IInstruction<std::plus<int> > {
 public:
  WriteInstruction(unsigned int op) : IInstruction<std::plus<int> >(op) {}
  virtual void run(Processor* processor) {
    processor->write(rs);
    processor->incrementCounter();
  }
};

typedef IInstruction<std::plus<int> > AddiInstruction;
typedef IInstruction<std::less<int> > SltiInstruction;
typedef IInstruction<std::equal_to<int> > SeqiInstruction;

class JInstruction : public Instruction {
 protected:
  unsigned int im;
 public:
  JInstruction(unsigned int op) : Instruction() {
    im = getImJ(op);
  }
  virtual void run(Processor* processor) {
    processor->setCounter(im);
  }
};

class JalInstruction : public JInstruction {
 public:
  JalInstruction(unsigned int op) : JInstruction(op) {}
  virtual void run(Processor* processor) {
    processor->incrementCounter();
    processor->setRegister(31, processor->getCounter());
    processor->setCounter(im);
  }
};

}

Instruction* newRInstruction(const unsigned int& op) {
  switch(getFunct(op)) {
  case 0x20: // ADD
    return new AddInstruction(op);
  case 0x22: // SUB
    return new SubInstruction(op);
  case 0x24: // AND
    return new AndInstruction(op);
  case 0x25: // OR
    return new OrInstruction(op);
  case 0x2A: // SLT
    return new SltInstruction(op);
  case 0x2B: // SEQ
    return new SeqInstruction(op);
  }
  return NULL;
}

Instruction* newInstruction(const unsigned int& op) {
  switch(getOpcode(op)) {
  case 0x00: // R
    return newRInstruction(op);
  case 0x04: // BEQ
    return new BeqInstruction(op);
  case 0x01: // JR
    return new JrInstruction(op);
  case 0x23: // LW
    return new LwInstruction(op);
  case 0x2B: // SW
    return new SwInstruction(op);
  case 0x08: // ADDI
    return new AddiInstruction(op);
  case 0x0A: // SLTI
    return new SltiInstruction(op);
  case 0x0B: // SEQI
    return new SeqiInstruction(op);
  case 0x02: // J
    return new JInstruction(op);
  case 0x03: // JAL
    return new JalInstruction(op);
  case 0x3f:
    return new ReadInstruction(op);
  case 0x3e:
    return new WriteInstruction(op);
  }
  return NULL;
}

} // namespace ugl0sim
