#include <QDateTime>

#include "processor.h"
#include "memory.h"
#include "instruction.h"
#include "ugl0sim-lib_global.h"

namespace ugl0sim {

const unsigned int kProgramMemoryStart = 0x00;

Processor::Processor(QIODevice* input, QIODevice* output, QObject* parent) :
    QObject(parent), program_counter_(0), memory_(),
    input(input), output(output) {
  reset();
}

void Processor::reset() {
  for (int i = 0; i < 32; ++i) {
    register_[i] = 0;
  }
  program_counter_ = kProgramMemoryStart;
}

void Processor::run(Program* program) {
  int n = 0;
//  qDebug("%d", program->size());
  for(;; ++n) {
    int program_index = program_counter_;
    if (program_index >= program->size()) {
      break;
    }
    (*program->at(program_index))(this);
  }
  qDebug("Total Ops: %d", n);
}

} //  namespace ugl0sim
