第３回課題

小巻千洋
05-111013


自由変数有り

let rec f x = 
	let rec loop n =
		if n = 0 then
			0
		else if (n mod 2 = 0 ) then
			x / 2 + loop (n / 2)
		else
			loop (n / 2)
	in loop 10;;
	

自由変数なし

let rec loop n x =
	if n = 0 then
		0
	else if (n mod 2 = 0) then
		x / 2 + loop (n / 2) x
	else
		loop (n / 2) x
	
let rec f x = loop 10 x;;



３番

a. Lf, z 
b. 作られない
c. 作られない
d. 作られない、Li, Lh
e. 作られない、Lf, z
f. 作られない
