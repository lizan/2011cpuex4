-------------------------------------------------------------------------------
-- alu
-- LastUpdate: 2011/10/12 02:04 
-- Author    : ultraredrays
-- Email     : ultraredrays@gmail.com
-- Todo      : 動作確認
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.std_logic_signed.all;
--use IEEE.std_logic_unsigned.all;

entity alu is
  
  port (
    A     : in  std_logic_vector(31 downto 0);  -- input data
    B     : in  std_logic_vector(31 downto 0);  -- input data
    code  : in  std_logic_vector( 3 downto 0);  -- alu制御入力
    O     : out std_logic_vector(31 downto 0);  -- output data
    Z     : out std_logic);

end alu;

architecture archi of alu is

  signal Otmp : std_logic_vector(31 downto 0) := x"00000000";
  signal Aint : integer := 0;
  signal Bint : integer := 0;
  signal Oand : std_logic_vector(31 downto 0);
  signal Oor  : std_logic_vector(31 downto 0);
  signal Oadd : std_logic_vector(31 downto 0);
  signal Osub : std_logic_vector(31 downto 0);
  signal Omul : std_logic_vector(63 downto 0) := x"0000000000000000";
  signal Odiv : std_logic_vector(31 downto 0);
  signal Oslt : std_logic;
  signal Oeq  : std_logic;
  signal Oshr : std_logic_vector(31 downto 0);
  signal Oshl : std_logic_vector(31 downto 0);

begin  -- archi

  Z <= '1' when Otmp = x"00000000" else '0';
  O <= Otmp;


  Aint <= conv_integer(A);
  Bint <= conv_integer(B);
  Oand <= A and B;
  Oor  <= A or B;
  Oadd <= A + B;
  Osub <= A - B;
  Omul <= A * B;
  Odiv <= x"00000000" when Bint = 0 else
          conv_std_logic_vector(Aint,32);
--          conv_std_logic_vector((Aint/Bint),32);
  Oslt <= '1' when A < B else
          '0';
  Oeq  <= '1' when A = B else
          '0';
  Oshr <= to_stdLogicVector((to_bitVector(A) srl Bint));
  Oshl <= to_stdLogicVector((to_bitVector(A) sll Bint));
  
  Otmp <= Oand when code = "0000" else
          Oor  when code = "0001" else
          Oadd when code = "0010" else
          Osub when code = "0110" else
          Omul(31 downto 0) when code = "0011" else
          Odiv when code = "0100" else
          x"0000000"&"000"&Oslt when code = "0111" else
          x"0000000"&"000"&Oeq  when code = "0101" else
          Oshr when code = "1000" else
          Oshl when code = "1001" else
          A;
    
end archi;
