
# PlanAhead Launch Script for Post-Synthesis floorplanning, created by Project Navigator

create_project -name UGL1 -dir "/home/ultraredrays/Documents/works/EXCPU/src/2011cpuex4/core/trunk/UGL1/planAhead_run_1" -part xc5vlx50tff1136-2
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "/home/ultraredrays/Documents/works/EXCPU/src/2011cpuex4/core/trunk/UGL1/top.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {/home/ultraredrays/Documents/works/EXCPU/src/2011cpuex4/core/trunk/UGL1} }
set_param project.paUcfFile  "/home/ultraredrays/Documents/works/EXCPU/src/2011cpuex4/core/trunk/top.ucf"
add_files "/home/ultraredrays/Documents/works/EXCPU/src/2011cpuex4/core/trunk/top.ucf" -fileset [get_property constrset [current_run]]
open_netlist_design
