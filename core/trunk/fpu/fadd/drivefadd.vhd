library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_textio.all;

library STD;
use STD.textio.all;

entity fadd_test_f is
end fadd_test_f;

architecture sim_fadd of fadd_test_f is
  
  component fadd
    port (I1, I2 : in std_logic_vector(31 downto 0);
          ANS : out std_logic_vector(31 downto 0));
  end component;
  signal S1, S2 : std_logic_vector(31 downto 0);
  signal ANS : std_logic_vector(31 downto 0);
  type rom_t is array (0 to 7) of std_logic_vector(31 downto 0);

begin  -- SIM_FA
  
  P: process
    file VECTOR : text is in "/home/kozai/2011cpuex4/fpu/fadd/indata";
    file OVECTOR : text is out "/home/kozai/2011cpuex4/fpu/fadd/outdata";
    
    variable v_li : line;
    variable v_li2 : line;
    variable v_s1 : std_logic_vector(31 downto 0);
    variable v_s2 : std_logic_vector(31 downto 0);
  begin
    s1 <= (others => '0'); s2 <= (others => '0');
    for i in 0 to 110000000 loop     
      readline(vector, v_li);
      read(v_li, v_s1);
      read(v_li, v_s2);
      s1 <= v_s1;
      s2 <= v_s2;
      wait for 1 ns;
      write(v_li2, ANS);
      writeline(OVECTOR, v_li2);
    end loop;  -- i
    wait;
  end process;

  HFLA : FADD port map (S1,S2,ANS);
  
end sim_fadd;
