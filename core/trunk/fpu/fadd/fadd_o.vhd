library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity FADD is  
  port (I1,I2  : in  std_logic_vector(31 downto 0);
        ANS : out std_logic_vector(31 downto 0));
end FADD;

architecture FADD_LOGIC of FADD is
  alias s1 : std_logic is I1(31);
  alias s2 : std_logic is I2(31);
  alias e1 : std_logic_vector(7 downto 0) is I1(30 downto 23);
  alias e2 : std_logic_vector(7 downto 0) is I2(30 downto 23);
  alias f1 : std_logic_vector(22 downto 0) is I1(22 downto 0);
  alias f2 : std_logic_vector(22 downto 0) is I2(22 downto 0);
  
  constant zero : std_logic_vector(31 downto 0) := (others => '0');     
  signal sp,spx,p : std_logic := '0';
  signal ep,epx : std_logic_vector(7 downto 0) := (others => '0');
  signal fla,fsm,f3 : std_logic_vector(26 downto 0) := (others => '0');
  signal nlz : std_logic_vector(4 downto 0) := (others => '0');
  signal mar : std_logic_vector(2 downto 0) := (others => '0');
  
begin  -- FADD_LOGIC
  --stage 1
  fla <= (others => '0') when ((e1 = x"ff" or e1 = x"00") and (e2 = x"ff" or e2 = x"00")) else
         "01" & f1 & "00" when e2 = x"ff"else
         "01" & f2 & "00" when e1 = x"ff"else 
         "01" & f1 & "00" when e1 > e2 else
         "01" & f2 & "00" when e2 > e1 else 
         "01" & f1 & "00" when f1 > f2 else
         "01" & f2 & "00";  
  
  fsm <= (others => '0') when e1 = x"00" or e1 = x"ff" or e2 = x"00" or e2 = x"ff" else
         (others => '0') when e1 > e2 and (e1 - e2) >= "00011010" else     --26
         (others => '0') when e2 > e1 and (e2 - e1) >= "00011010" else     --26
         "000000000000000000000000001" when (e1 - e2) = "00011001" else     --25
         "000000000000000000000000001" when (e2 - e1) = "00011001" else     --25
         SHR("01" & f2 & "00", e1 - e2) when e1 > e2 else  --
         SHR("01" & f1 & "00", e2 - e1) when e2 > e1 else  --
         "01" & f2 & "00" when f1 > f2 else
         "01" & f1 & "00";  

  ep <= x"00" when ((e1 = x"ff" or e1 = x"00") and (e2 = x"ff" or e2 = x"00")) else
        e1 when e2 = x"ff" else
        e2 when e1 = x"ff" else
        e1 when e1 > e2 else
        e2 when e2 > e1 else 
        e1 when f1 > f2 else
        e2;

  sp <= '0' when ((e1 = x"ff" or e1 = x"00") and (e2 = x"ff" or e2 = x"00")) else
        s1 when e2 = x"ff" else
        s2 when e1 = x"ff" else
        s1 when e1 > e2 else
        s2 when e2 > e1 else 
        s1 when f1 > f2 else
        s2; 

  p <= s1 xor s2;


  
  
  Stage23: process (fla,fsm)
    variable fcal : std_logic_vector(26 downto 0);
    variable n : std_logic_vector(4 downto 0);
    variable i : std_logic_vector(1 downto 0);
    variable ftt : std_logic_vector(26 downto 0);
    variable ett : std_logic_vector(7 downto 0);
  begin  -- process Stage23
    if (p = '0') then
      fcal := fla + fsm;
    else
      fcal := fla - fsm;
    end if;
    
    --NLZ--
    if fcal(26) = '1' then
      n := "00000";
    elsif fcal(25 downto 14) = "000000000000"then
      if fcal(13 downto 8) = "000000" then
        if fcal(7 downto 5) = "000" then
          if fcal(4 downto 2) = "000" then      --25
            n := "11001";    
          elsif fcal(4 downto 3) = "00" then     --24
            n := "11000";
          elsif fcal(4) = '0' then      --23
            n := "10111";
          else                      --22
            n := "10110";
          end if;                
        else
          if fcal(7 downto 6) = "00" then  --21
            n := "10101";
          elsif fcal(7) = '0' then      --20
            n := "10100";
          else                      --19
            n := "10011";
          end if;               
        end if;
      else
        if fcal(13 downto 11) = "000" then
          if fcal(10 downto 9) = "00" then  --18
            n := "10010";
          elsif fcal(10) = '0' then      --17
            n := "10001";
          else                      --16
            n := "10000";
          end if;    
        else
          if fcal(13 downto 12) = "00" then  --15
            n := "01111";
          elsif fcal(13) = '0' then      --14
            n := "01110";
          else                      --13
            n := "01101";
          end if;            
        end if;            
      end if; 
    else   
      if fcal(25 downto 20) = "000000" then
        if fcal(19 downto 17) = "000" then
          if fcal(16 downto 15) = "00" then  --12
            n := "01100";
          elsif fcal(16) = '0' then      --11
            n := "01011";
          else                      --10
            n := "01010";
          end if;            
        else
          if fcal(19 downto 18) = "00" then  --9
            n := "01001";
          elsif fcal(19) = '0' then      --8
            n := "01000";
          else                      --7
            n := "00111";
          end if;            
         end if;          
      else
        if fcal(25 downto 23) = "000" then
          if fcal(22 downto 21) = "00" then  --6
            n := "00110";
          elsif fcal(22) = '0' then      --5
            n := "00101";
          else                      --4
            n := "00100";
          end if;            
        else
          if fcal(25 downto 24) = "00" then  --3
            n := "00011";
          elsif fcal(25) = '0' then      --2
            n := "00010";
          else                      --1
            n := "00001";
          end if;            
        end if;
      end if;   
    end if;
    --NLZ--fin

    if fcal(26 downto 2) = "1111111111111111111111111" then
      i := "10";
    elsif fcal(25 downto 1) = "1111111111111111111111111"  then
      i := "01";
    elsif fcal(24 downto 0) = "1111111111111111111111111"  then
      i := "01";
    else
      i := "00";
    end if;

    
    case n(1 downto 0) is
      when "00" => f3 <= fcal;
      when "01" => f3 <= fcal(25 downto 0) & '0';
      when "10" => f3 <= fcal(24 downto 0) & "00";
      when others => f3 <= fcal(23 downto 0) & "000";
    end case;
   
    case n(1 downto 0) is
      when "11" => mar <= "000";
      when others => mar <= "100";
    end case;

    if ep = x"00" then
      epx <= x"00";
    else
      epx <= ep + i;     
    end if;
    spx <= sp;
    nlz <= n;
    --stage 2 end

    --stage 3
    case nlz(4 downto 2) is
      when "000" => ftt := f3 + (zero(26 downto 3) & mar);
      when "001" => ftt := f3(22 downto 0) & x"0";
      when "010" => ftt := f3(18 downto 0) & x"00";
      when "011" => ftt := f3(14 downto 0) & x"000";
      when "100" => ftt := f3(10 downto 0) & x"0000";
      when "101" => ftt := f3(6 downto 0) & x"00000";
      when "110" => ftt := f3(2 downto 0) & x"000000";
      when others => ftt := zero(26 downto 0);
    end case;

    
    if nlz = "11010" or epx < nlz or epx = x"00"then
      ett := x"00";
    else
      ett := epx - nlz + '1';
    end if;

    ANS <= spx & ett & ftt(25 downto 3);
    
    
    --wait for 1 ns;--
  end process Stage23;

end FADD_LOGIC;
