#include <string>
#include <queue>
#include <vector>
#include <fstream>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#define INPUTOPTION 1
//1の時はrandによる乱数,2の時はindata_sの値をとりだす
#define ONTEST 1
//1の時はデバッグ用関数を呼ぶ。indata_sの値を取り出すときは常に呼ぶ
using namespace std;
ofstream missdata("missdata");


//デバッグ用共用体
union Ap{
  u_int32_t ii;
  float ff;
};
union Bp{
  u_int32_t ii;
  float ff;
};
union Xp{
  u_int32_t ii;
  float ff;
};
union Ep{
  u_int32_t ii;
  float ff;
};
union Apb{
  u_int32_t ii;
  float ff;
}; 

//u_int32_tを01文字列に変換
string to_string(long l){
  string x;
  for(int i = 0; i < 32; i++){
    if(l % 2 == 1)
      x = "1" + x;
    else
      x = "0" + x;
    l >>= 1;
  }
  return x;
}

/*
  デバッグ用関数
  異なる結果ををmissdataに出力
 */
void t_or_f(u_int32_t a, u_int32_t b, u_int32_t x, string i1,string i2, string o, int i){
  float max,diff;
  u_int32_t ae,be,abe;
  union Ap app;
  union Bp bpp;
  union Xp xpp;
  union Ep epp;
  union Apb ab;

  ae = a & 0x7f800000;
  be = b & 0x7f800000;
  if(ae == 0x7f800000 || ae == 0)
    app.ii = 0;
  else
    app.ii = a;

  if(be == 0x7f800000 || be == 0)
    bpp.ii = 0;
  else
    bpp.ii = b;
  
  xpp.ii = x;
  
  ab.ff = app.ff / bpp.ff;

  abe = ab.ii & 0x7f800000;
  if(abe == 0x7f800000 || abe == 0)
    ab.ii = 0;
  
  epp.ii = 0x0c800000;//2^-106
  max = epp.ff;
  if(max < fabs(ab.ff))
    max = fabs(ab.ff);

  max /= 1048576;//pow(2,20);
  
  diff = fabs(xpp.ff - ab.ff);
  if(diff >= max)
    missdata << i << "番目\n" << 
      i1 << "\t" << app.ff << "\tA\n" <<
      i2 << "\t" << bpp.ff << "\tB\n" <<
      o << "\t" << xpp.ff << "\tFDIV(A,B)\n" <<
      to_string(ab.ii) << "\t" << ab.ff << "\t(A/B)\n"<<
      diff << "\t" << max <<  "\t" << epp.ff << "\n";
}

//x(a downto b)に相当
u_int64_t subu64(u_int64_t x, int a, int b){//x(a downto b)
  u_int64_t y = (x << (64 - a -1)) >> (64 - a + b - 1);
  return y;
}

//それぞれのuintに対する6bitのkeyを生成する
u_int32_t keymake_inv(u_int32_t f1){
  u_int32_t key;
  if(f1 > 0xaaaaab)
    if(f1 > 0xcccccd)
      if(f1 > 0xe38e39)
	if(f1 > 0xf0f0f1)
	  if(f1 > 0xf83e10)
	    if(f1 > 0xfc0fc1)    key = 0x00;
	    else    key = 0x01;
	  else
	    if(f1 > 0xf4898e)    key = 0x02;
	    else    key = 0x03;
	else
	  if(f1 > 0xea0ea1)
	    if(f1 > 0xed7304)    key = 0x04;
	    else    key = 0x05;
	  else
	    if(f1 > 0xe6c2b5)    key = 0x06;
	    else    key = 0x07;
      else
	if(f1 > 0xd79436)
	  if(f1 > 0xdd67c9)
	    if(f1 > 0xe07038)    key = 0x08;
	    else    key = 0x09;
	  else
	    if(f1 > 0xda740e)    key = 0x0a;
	    else    key = 0x0b;
	else
	  if(f1 > 0xd20d21)
	    if(f1 > 0xd4c77b)    key = 0x0c;
	    else    key = 0x0d;
	  else
	    if(f1 > 0xcf6475)    key = 0x0e;
	    else    key = 0x0f;
    else      
      if(f1 > 0xba2e8c)
	if(f1 > 0xc30c31)
	  if(f1 > 0xc7ce0d)
	    if(f1 > 0xca4588)    key = 0x10;
	    else    key = 0x11;
	  else
	    if(f1 > 0xc565c9)    key = 0x12;
	    else    key = 0x13;
	else
	  if(f1 > 0xbe82fa)
	    if(f1 > 0xc0c0c1)    key = 0x14;
	    else    key = 0x15;
	  else
	    if(f1 > 0xbc5264)    key = 0x16;
	    else    key = 0x17;
      else
	if(f1 > 0xb21643)
	  if(f1 > 0xb60b61)
	    if(f1 > 0xb81703)    key = 0x18;
	    else    key = 0x19;
	  else
	    if(f1 > 0xb40b41)    key = 0x1a;
	    else    key = 0x1b;
	else
	  if(f1 > 0xae4c41)
	    if(f1 > 0xb02c0b)    key = 0x1c;
	    else    key = 0x1d;
	  else
	    if(f1 > 0xac7691)    key = 0x1e;
	    else    key = 0x1f;
  else
    if(f1 > 0x924924)
      if(f1 > 0x9d89d8)
	if(f1 > 0xa3d70a)
	  if(f1 > 0xa72f05)
	    if(f1 > 0xa8e83f)    key = 0x20;
	    else    key = 0x21;
	  else
	    if(f1 > 0xa57eb5)    key = 0x22;
	    else    key = 0x23;
	else
	  if(f1 > 0xa0a0a1)
	    if(f1 > 0xa237c3)    key = 0x24;
	    else    key = 0x25;
	  else
	    if(f1 > 0x9f1166)    key = 0x26;
	    else    key = 0x27;
      else
	if(f1 > 0x97b426)
	  if(f1 > 0x9a90e8)
	    if(f1 > 0x9c09c0)    key = 0x28;
	    else    key = 0x29;
	  else
	    if(f1 > 0x991f1a)    key = 0x2a;
	    else    key = 0x2b;
	else
	  if(f1 > 0x94f209)
	    if(f1 > 0x964fda)    key = 0x2c;
	    else    key = 0x2d;
	  else
	    if(f1 > 0x939a86)    key = 0x2e;
	    else    key = 0x2f;
    else      
      if(f1 > 0x888888)
	if(f1 > 0x8d3dcb)
	  if(f1 > 0x8fb824)
	    if(f1 > 0x90fdbc)    key = 0x30;
	    else    key = 0x31;
	  else
	    if(f1 > 0x8e7835)    key = 0x32;
	    else    key = 0x33;
	else
	  if(f1 > 0x8ad8f3)
	    if(f1 > 0x8c08c0)    key = 0x34;
	    else    key = 0x35;
	  else
	    if(f1 > 0x89ae40)    key = 0x36;
	    else    key = 0x37;
      else
	if(f1 > 0x842108)
	  if(f1 > 0x864b8a)
	    if(f1 > 0x8767ab)    key = 0x38;
	    else    key = 0x39;
	  else
	    if(f1 > 0x853408)    key = 0x3a;
	    else    key = 0x3b;
	else
	  if(f1 > 0x820820)
	    if(f1 > 0x83126e)    key = 0x3c;
	    else    key = 0x3d;
	  else
	    if(f1 > 0x810204)    key = 0x3e;
	    else if(f1 == 0x800000)    key = 0x00;
	    else    key = 0x3f;
   
 
  return key;
}

union Tes{
  u_int32_t ii;
  float ff;
};

/*関数本体   
  u_int32_t型の2数を浮動小数点で足したときの和をu_int32_t型で出力する
  e = 255 は0
 */
u_int32_t finv(u_int32_t x){
  u_int32_t s1 = x >> 31;
  u_int32_t e1 = (x & 0x7f800000) >> 23;
  if(e1 == 0 || e1 == 255)
    return 0;
  u_int32_t ep;
  u_int32_t f1 = (0x00800000 | (x & 0x007fffff));//1+23bit

  ep = 253U - e1;

  u_int64_t fi;
  if(f1 == 0x00800000){
    fi = 0;
    ep++;
  }
  else{
    u_int64_t fp1 = (0x00000040 | keymake_inv(f1));
    
    u_int64_t fs1 = subu64(f1,23,9) * fp1;//13*7
    u_int64_t fs2 = subu64(~fs1,21,0);
    u_int64_t fs3 = subu64(fs2,21,7) * fp1;
    u_int64_t fp2 = subu64(fs3,20,7);

    u_int64_t fs4 = f1 * fp2;//24*14
    u_int64_t fs5 = subu64(~fs4,37,0);
    u_int64_t fs6 = subu64(fs5,37,14) * fp2;
    u_int64_t fp3 = subu64(fs6,36,13)+1;
    
    fi = fp3;

  }
  if(ep >= 0x00000100 || ep == 0U)
    return 0;
  else 
    return (s1 << 31) | (ep << 23) | (fi & 0x007fffff);
}

/*関数本体   
  u_int32_t型の2数を浮動小数点で足したときの和をu_int32_t型で出力する
  e = 255 は0
 */
u_int32_t fmul(u_int32_t x1, u_int32_t x2){
  u_int32_t s1 = x1 >> 31;
  u_int32_t s2 = x2 >> 31;
  u_int32_t sp;
  u_int32_t e1 = (x1 & 0x7f800000) >> 23;
  u_int32_t e2 = (x2 & 0x7f800000) >> 23;
  u_int32_t ep;
  u_int32_t fu1 = (0x00800000 | (x1 & 0x007ff800)) >> 11;
  u_int32_t fu2 = (0x00800000 | (x2 & 0x007ff800)) >> 11;
  u_int32_t fd1 = (x1 & 0x000007ff);
  u_int32_t fd2 = (x2 & 0x000007ff);
  u_int32_t hh, hl, lh, f3;
  
  if(e1 == 0 || e2 == 0 || e1 == 255 || e2 == 255)
    return 0;
    
  hh = fu1 * fu2;
  hl = fu1 * fd2;
  lh = fd1 * fu2;
  f3 = hh + (hl >> 11) + (lh >> 11) +2;
  
  ep = e1 + e2 + 129;
  sp = s1 ^ s2; // xor
  if(f3 >= 0x04000000){
    f3 = (f3 >> 2);
    ep+=2;
  }
  else if(f3 >= 0x02000000){
    f3 = (f3 >> 1);
    ep++;
  }
    
  if(ep > 0x000001fe || ep <= 0x00000100)
    return 0;
  else
    return (sp << 31) | ((0x000000ff & ep) << 23) | ((f3 >> 1)& 0x007fffff); 
} 


/* 
   x1/x2
   x2の指数部が大きいの場合,inv(x2)が0になってしまうので
   (x1の指数分-2)だけ指数部を引く。
 */
u_int32_t fdiv(u_int32_t x1, u_int32_t x2){
  u_int32_t x1m,x2m,x1n,x2n,c,d;
  x2m = (x2 & 0x7f800000) >> 23;
  x1m = (x1 & 0x7f800000) >> 23;
  if(x1m == 0 || x1m == 0xff || x2m == 0 || x2m == 0xff)
    return 0;

  c = finv(x2);
  if(c == 0 && x2m != 0 && x2m != 0xff){
    x2m -= (x1m-3);
    x1m = 3;
    x1n = (x1 & 0x807fffff) | (x1m << 23);
    x2n = (x2 & 0x807fffff) | (x2m << 23); 
    c = finv(x2n);
  }
  else
    x1n = x1;
  
  d = fmul(x1n,c);

  return d;
}

//ランダムに値を生成し演算を実行する。
void randin(){
  long k,i=0,s;
  ofstream indata("indata");
  ofstream e_outdata("e_outdata");
  string i1,i2, o;
  u_int32_t a,b,x;

  cout << "回数　srand定数\n";
  cin >> k >> s;  
  srand(s);

  for(i = 0;i < k;i++){
    a = rand() + rand();
    b = rand() + rand();

    x = fdiv(a, b);

    i1 = to_string(a);
    i2 = to_string(b);
    o = to_string(x);
    
    indata << i1 << " " << i2 << "\n";
    e_outdata << o << "\n";    
    
#if ONTEST == 1   
    t_or_f(a,b,x,i1,i2,o,i);//デバッグ
#endif

    if((i*100) % k == 0)
      cout << i*100/k << '\n';
  }
}

//01文字列をu_intに変換
u_int32_t to_int(string x){
  int k = x.size();
  u_int32_t ans = 0;
  for(int i = 0; i<k;i++){
    ans <<= 1;
    if(x.at(i) == '1'){
      ans++;
    }
  }
  return ans;
}

/*
  indata_sに入っているデータについて演算を行う
  indata_sには間違いが起こりやすそうなものが入っている。
*/
void org_in(){
  int i = 0;
  ifstream indata_s("indata_s");
  ofstream e_outdata("e_outdata");
  u_int32_t a,b,x;
  string o,as,bs;
  char c;

  while(indata_s >> as >> bs){
    i++;
    a = to_int(as);
    b = to_int(bs);

    x = fdiv(a, b);

    o = to_string(x);
    
    e_outdata << o << "\n";    
    
    t_or_f(a,b,x,to_string(a),to_string(b),o,i);//デバッグ
    indata_s.get(c);
  }
}
    

//main
int main(){

#if INPUTOPTION == 1
  randin();
#elif INPUTOPTION == 2
  org_in();
#endif
    
return 0;
}
  


