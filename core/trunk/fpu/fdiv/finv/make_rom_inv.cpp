//1024bitのROMを生成する.±1のパラメータ調整を行ってる.
#include <string>
#include <queue>
#include <vector>
#include <fstream>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <iomanip>
using namespace std;

union Xp{
  u_int32_t ii;
  float ff;
};

int main(){
  u_int64_t i,a0=0,x0,x1,rom1,rom2;
  ofstream rom_v1("rom_vhdl1.txt");
  ofstream rom_v2("rom_vhdl2.txt");
  ofstream rom_c1("rom_cpp1.txt");
  ofstream rom_c2("rom_cpp2.txt");
  union Xp xpp;
  rom_c1 << "u_int32_t rom_c1[1025] = {";
  rom_c2 << "u_int32_t rom_c2[1025] = {";
  rom_v1 << "constant rom1 : data1 :=\n(";
  rom_v2 << "constant rom2 : data2 :=\n(";
  for(i = 0; i < 0x400; i++){
    a0 = 0x400 + i;
    xpp.ff = (1.0/a0 + 1.0/(a0+1));
    x0 = (0x00800000 | (xpp.ii & 0x007fffff));
    x1 = (x0 * x0 >> 23);
    if(i == 3 || i == 10 || i == 14 || i == 32)
      rom1 = (x0 << 1) - (x1 * a0 >> 11)+1; 
    else if(i >= 0x1c8)
      rom1 = (x0 << 1) - (x1 * a0 >> 11)-1;  
    else
      rom1 = (x0 << 1) - (x1 * a0 >> 11); 
    rom2 = (x1 >> 12);
    rom_c1 << "0x" << hex << rom1 << ',';
    rom_c2 << "0x" << hex << rom2 << ',';
    rom_v1 << "x\"" << hex << rom1 << "\",";
    rom_v2 << "x\"" << setw(4) << setfill('0') << hex << rom2 << "\",";
  }
  rom_c1 << "0};" << endl;
  rom_c2 << "0};" << endl;
  rom_v1 << "x\"000000\");" << endl;
  rom_v2 << "x\"0000\");" << endl;

  return 0;
}
