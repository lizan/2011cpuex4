library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity FADD is  
  port (I1,I2  : in  std_logic_vector(31 downto 0);
        ANS : out std_logic_vector(31 downto 0));
end FADD;

architecture archi of FADD is
  alias s1 : std_logic is I1(31);
  alias s2 : std_logic is I2(31);
  alias e1 : std_logic_vector(7 downto 0) is I1(30 downto 23);
  alias e2 : std_logic_vector(7 downto 0) is I2(30 downto 23);
  alias f1 : std_logic_vector(22 downto 0) is I1(22 downto 0);
  alias f2 : std_logic_vector(22 downto 0) is I2(22 downto 0);
  
  constant zero : std_logic_vector(31 downto 0) := (others => '0');     
  signal sp1,sp2,sp3,sp4,sp5,p : std_logic := '0';
  signal ep1,ep2,ep3,ep4,ep5 : std_logic_vector(7 downto 0) := (others => '0');
  signal fla,fsm,fcal,f4,f5 : std_logic_vector(26 downto 0) := (others => '0');
  --signal fpl,fmn : std_logic_vector(26 downto 0) := (others => '0');
  signal n,nlz : std_logic_vector(4 downto 0) := (others => '0');
  signal i : std_logic_vector(1 downto 0) := "00";
  signal mar : std_logic_vector(2 downto 0) := (others => '0');
  
begin  -- FADD_LOGIC
  --stage 1
  fla <= (others => '0') when ((e1 = x"ff" or e1 = x"00") and (e2 = x"ff" or e2 = x"00")) else
         "01" & f1 & "00" when e2 = x"ff"else
         "01" & f2 & "00" when e1 = x"ff"else 
         "01" & f1 & "00" when e1 > e2 else
         "01" & f2 & "00" when e2 > e1 else 
         "01" & f1 & "00" when f1 > f2 else
         "01" & f2 & "00";  
  
  fsm <= (others => '0') when e1 = x"00" or e1 = x"ff" or e2 = x"00" or e2 = x"ff" else
         (others => '0') when e1 > e2 and (e1 - e2) >= "00011010" else     --26
         (others => '0') when e2 > e1 and (e2 - e1) >= "00011010" else     --26
         "000000000000000000000000001" when (e1 - e2) = "00011001" else     --25
         "000000000000000000000000001" when (e2 - e1) = "00011001" else     --25
         SHR("01" & f2 & "00", e1 - e2) when e1 > e2 else  --
         SHR("01" & f1 & "00", e2 - e1) when e2 > e1 else  --
         "01" & f2 & "00" when f1 > f2 else
         "01" & f1 & "00";  
  --fpl <= (others => '0') when ((e1 = x"ff" or e1 = x"00") and (e2 = x"ff" or e2 = x"00")) else
  --        "01" & f1 & "00" when e2 = x"ff"else
  --        "01" & f2 & "00" when e1 = x"ff"else
  --        "01" & f1 & "00" when e1 > e2 and (e1 - e2) >= "00011010" else     --26
  --        "01" & f2 & "00" when e2 > e1 and (e2 - e1) >= "00011010" else     --26
  --        "01" & f1 & "01" when (e1 - e2) = "00011001" else     --25
  --        "01" & f2 & "01" when (e2 - e1) = "00011001" else     --25
  --        "01" & f1 & "00" + SHR("01" & f2 & "00", e1 - e2) when e1 > e2 else
  --        "01" & f2 & "00" + SHR("01" & f1 & "00", e2 - e1) when e2 > e1 else 
  --        "01" & f1 & "00" + "01" & f2 & "00";
  
  --fmn <= (others => '0') when ((e1 = x"ff" or e1 = x"00") and (e2 = x"ff" or e2 = x"00")) else
  --        "01" & f1 & "00" when e2 = x"ff"else
  --        "01" & f2 & "00" when e1 = x"ff"else
  --        "01" & f1 & "00" when e1 > e2 and (e1 - e2) >= "00011010" else     --26
  --        "01" & f2 & "00" when e2 > e1 and (e2 - e1) >= "00011010" else     --26
  --        "01" & f1 & "00" - "000000000000000000000000001" when (e1 - e2) = "00011001" else     --25
  --        "01" & f2 & "00" - "000000000000000000000000001" when (e2 - e1) = "00011001" else     --25
  --        "01" & f1 & "00" - SHR("01" & f2 & "00", e1 - e2) when e1 > e2 else
  --        "01" & f2 & "00" - SHR("01" & f1 & "00", e2 - e1) when e2 > e1 else 
  --        "01" & f1 & "00" - "01" & f2 & "00"  when f1 > f2 else
  --        "01" & f2 & "00" - "01" & f2 & "00";

  ep1 <= x"00" when ((e1 = x"ff" or e1 = x"00") and (e2 = x"ff" or e2 = x"00")) else
        e1 when e2 = x"ff" else
        e2 when e1 = x"ff" else
        e1 when e1 > e2 else
        e2 when e2 > e1 else 
        e1 when f1 > f2 else
        e2;

  sp1 <= '0' when ((e1 = x"ff" or e1 = x"00") and (e2 = x"ff" or e2 = x"00")) else
        s1 when e2 = x"ff" else
        s2 when e1 = x"ff" else
        s1 when e1 > e2 else
        s2 when e2 > e1 else 
        s1 when f1 > f2 else
        s2; 

  p <= s1 xor s2;

  -----------------------------------------------------------------------------

  fcal <= fla + fsm when p = '0' else
          fla - fsm;

  ep2 <= ep1;
  sp2 <= sp1;

  -----------------------------------------------------------------------------

  n <= "00000" when fcal(26) = '1' else
       "11001" when fcal(25 downto 2)  = "000000000000000000000000" else
       "11000" when fcal(25 downto 3)  = "00000000000000000000000" else
       "10111" when fcal(25 downto 4)  = "0000000000000000000000" else
       "10110" when fcal(25 downto 5)  = "000000000000000000000" else
       "10101" when fcal(25 downto 6)  = "00000000000000000000" else
       "10100" when fcal(25 downto 7)  = "0000000000000000000" else
       "10011" when fcal(25 downto 8)  = "000000000000000000" else
       "10010" when fcal(25 downto 9)  = "00000000000000000" else
       "10001" when fcal(25 downto 10) = "0000000000000000" else
       "10000" when fcal(25 downto 11) = "000000000000000" else
       "01111" when fcal(25 downto 12) = "00000000000000" else
       "01110" when fcal(25 downto 13) = "0000000000000" else
       "01101" when fcal(25 downto 14) = "000000000000" else
       "01100" when fcal(25 downto 15) = "00000000000" else
       "01011" when fcal(25 downto 16) = "0000000000" else
       "01010" when fcal(25 downto 17) = "000000000" else
       "01001" when fcal(25 downto 18) = "00000000" else
       "01000" when fcal(25 downto 19) = "0000000" else
       "00111" when fcal(25 downto 20) = "000000" else
       "00110" when fcal(25 downto 21) = "00000" else
       "00101" when fcal(25 downto 22) = "0000" else
       "00100" when fcal(25 downto 23) = "000" else
       "00011" when fcal(25 downto 24) = "00" else
       "00010" when fcal(25) = '0' else
       "00001";

  i <= "10" when fcal(26 downto 2) = "1111111111111111111111111" else
       "01" when fcal(25 downto 1) = "1111111111111111111111111" else
       "01" when fcal(24 downto 0) = "1111111111111111111111111" else
       "00";

  ep3 <= ep2;
  sp3 <= sp2;
  -----------------------------------------------------------------------------

  mar <= "000" when n(1 downto 0) = "11" else
         "100";

  f4 <=  fcal when n(1 downto 0) = "00" else
         fcal(25 downto 0) & '0' when n(1 downto 0) = "01" else
         fcal(24 downto 0) & "00" when n(1 downto 0) = "10" else
         fcal(23 downto 0) & "000";

  ep4 <= x"00" when ep3 = x"00" else
         ep3 + i;
  sp4 <= sp3;
  nlz <= n;

  -----------------------------------------------------------------------------

  f5 <= f4 + (zero(26 downto 3) & mar) when nlz(4 downto 2) = "000" else
        f4(22 downto 0) & x"0" when nlz(4 downto 2) = "001" else
        f4(18 downto 0) & x"00" when nlz(4 downto 2) = "010" else
        f4(14 downto 0) & x"000" when nlz(4 downto 2) = "011" else
        f4(10 downto 0) & x"0000" when nlz(4 downto 2) = "100" else
        f4(6 downto 0) & x"00000" when nlz(4 downto 2) = "101" else
        f4(2 downto 0) & x"000000" when nlz(4 downto 2) = "110" else
        zero(26 downto 0);

  ep5 <= x"00" when nlz = "11010" or ep4 < nlz or ep4 = x"00" else
         ep4 - nlz + '1';
  sp5 <= sp4;

  -----------------------------------------------------------------------------

  ANS <= sp5 & ep5 & f5(25 downto 3);

end archi;
