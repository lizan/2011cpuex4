library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity FDIV is
  port (I1, I2 : in  std_logic_vector(31 downto 0);
        ANS    : out std_logic_vector(31 downto 0));
end FDIV;

architecture archi of FDIV is
  component FMUL
    port (I1, I2 : in  std_logic_vector(31 downto 0);
          ANS    : out std_logic_vector(31 downto 0));
  end component;

  component FINV  
    port (I : in  std_logic_vector(31 downto 0);
          ANS : out std_logic_vector(31 downto 0));
  end component;
  
  signal j,j1,j2,j1a,j2a,m : std_logic_vector(31 downto 0);
  signal s1a,s2a,ek1,ek2,ek3 : std_logic;
  signal e1a,e2a : std_logic_vector(7 downto 0);
  signal f1a,f2a : std_logic_vector(22 downto 0);
  
begin  -- FDIV_LOGIC
  FINV0 : FINV port map (I => I2, ANS => j);
  FINV1 : FINV port map (I => j2, ANS => j2a);
  FMUL0 : FMUL port map (I1 => j1a, I2 => j2a, ANS => m);

  s1a <= I1(31);
  s2a <= I2(31);
  e1a <= I1(30 downto 23);
  e2a <= I2(30 downto 23);
  f1a <= I1(22 downto 0);
  f2a <= I2(22 downto 0);
  
  ek1 <= '1' when e1a = x"00" or e1a = x"ff" or e2a = x"00" or e2a = x"ff" else
         '0';
  j1 <= s1a & x"03" & f1a when j = 0 and e2a /= x"00" and e2a /= x"ff" else
        I1;
  j2 <= s2a & (e2a - (e1a - x"03")) & f2a when j = 0 and e2a /= x"00" and e2a /= x"ff" else
        I2;

  ek2 <= ek1;
  j1a <= j1;

  ek3 <= ek2;
  
  ANS <= (others => '0') when ek3 = '1' else
         m;
  
end archi;
