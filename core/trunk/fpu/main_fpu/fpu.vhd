
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.std_logic_signed.all;

entity fpu is  
  port (
    A     : in  std_logic_vector(31 downto 0);  -- input data
    B     : in  std_logic_vector(31 downto 0);  -- input data
    funct : in  std_logic_vector( 5 downto 0);  -- alu制御入力
    O     : out std_logic_vector(31 downto 0);  -- output data
    Z     : out std_logic);
end fpu;

architecture archi of fpu is
  signal Otmp : std_logic_vector(31 downto 0) := x"00000000";
  signal fadd_ans : std_logic_vector(31 downto 0);
  signal fsub_ans : std_logic_vector(31 downto 0);
  signal fmul_ans : std_logic_vector(31 downto 0);
  signal fdiv_ans : std_logic_vector(31 downto 0);
  signal sqrt_ans : std_logic_vector(31 downto 0);
  signal feq_ans : std_logic;
  signal fslt_ans : std_logic;
  
  component FADD
    port (I1, I2 : in  std_logic_vector(31 downto 0);
          ANS    : out std_logic_vector(31 downto 0));
  end component;
  component FSUB
    port (I1, I2 : in  std_logic_vector(31 downto 0);
          ANS    : out std_logic_vector(31 downto 0));
  end component;
  component FMUL
    port (I1, I2 : in  std_logic_vector(31 downto 0);
          ANS    : out std_logic_vector(31 downto 0));
  end component;
  component FDIV
    port (I1, I2 : in  std_logic_vector(31 downto 0);
          ANS    : out std_logic_vector(31 downto 0));
  end component;
  component SQRT
    port (I      : in  std_logic_vector(31 downto 0);
          ANS    : out std_logic_vector(31 downto 0));
  end component;
  component FEQ
    port (I1, I2 : in  std_logic_vector(31 downto 0);
          ANS    : out std_logic);
  end component;
  component FSLT
    port (I1, I2 : in  std_logic_vector(31 downto 0);
          ANS    : out std_logic);
  end component;

begin  -- fpumain    
  FADD0 : FADD port map
    (I1 => A,
     I2 => B,
     ANS => fadd_ans);
  FSUB0 : FSUB port map
    (I1 => A,
     I2 => B,
     ANS => fsub_ans);
  FMUL0 : FMUL port map
    (I1 => A,
     I2 => B,
     ANS => fmul_ans);
  FDIV0 : FDIV port map
    (I1 => A,
     I2 => B,
     ANS => fdiv_ans);
  SQRT0 : SQRT port map
    (I => A,
     ANS => sqrt_ans);
  FEQ0 : FEQ port map
    (I1 => A,
     I2 => B,
     ANS => feq_ans);
  FSLT0 : FSLT port map
    (I1 => A,
     I2 => B,
     ANS => fslt_ans);
  
  Z <= '1' when Otmp = x"00000000" else '0';
  O <= Otmp;

  Otmp <= fadd_ans when funct = "100000" else
          fsub_ans when funct = "100010" else
          fmul_ans when funct = "011000" else
          fdiv_ans when funct = "011010" else
          x"0000000"&"000"&fslt_ans when funct = "101010" else
          x"0000000"&"000"&feq_ans  when funct = "101011" else
          sqrt_ans;
end archi;

configuration fpu_config of fpu is
  for archi
    for FADD0 : fadd
      use entity work.FADD(archi);
    end for;
    for FSUB0 : fsub
      use entity work.FSUB(archi);
    end for;
    for FMUL0 : fmul
      use entity work.FMUL(archi);
    end for; 
    for FDIV0 : fdiv
      use entity work.FDIV(archi);
    end for;
    for SQRT0 : sqrt
      use entity work.SQRT(archi);
    end for;
    for FEQ0 : feq
      use entity work.FEQ(archi);
    end for;
    for FSLT0 : fslt
      use entity work.FSLT(archi);
    end for;
  end for;
end fpu_config;
