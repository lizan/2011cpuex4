//feq a b でa = bなら 1, a != bなら 0

#include <string>
#include <queue>
#include <vector>
#include <fstream>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
using namespace std;
ofstream missdata("missdata");
#define NODATA 0
//1の時はindata_s,outdataの出力を行わない

//デバッグ用共用体
union Ap{
  u_int32_t ii;
  float ff;
};
union Bp{
  u_int32_t ii;
  float ff;
};
union Xp{
  u_int32_t ii;
  float ff;
};
union Ep{
  u_int32_t ii;
  float ff;
};
union Apb{
  u_int32_t ii;
  float ff;
}; 

//u_int32_tをk文字の文字列に変換
string to_string(long l){
  string x;
  for(int i = 0; i < 32; i++){
    if(l % 2 == 1)
      x = "1" + x;
    else
      x = "0" + x;
    l /= 2;
  }
  return x;
}
string to_string1(int i){
  string x;
  if(i == 1)
    x = "1";
  else
    x = "0";
  return x;
}


/*
  デバッグ用関数
  異なる結果ををmissdataに出力
 */
void t_or_f(u_int32_t a, u_int32_t b, int x, string i1,string i2, int i){
  float max,diff;
  u_int32_t ae,be,abe;
  int sol;
  union Ap app;
  union Bp bpp;

  ae = a & 0x7f800000;
  be = b & 0x7f800000;
  if(ae == 0x7f800000 || ae == 0)
    app.ii = 0;
  else
    app.ii = a;

  if(be == 0x7f800000 || be == 0)
    bpp.ii = 0;
  else
    bpp.ii = b;


  if(app.ff == bpp.ff)
    sol = 1;
  else
    sol = 0;

  if(x != sol)
    missdata << i << "番目\n" << 
      i1 << "\t" << app.ff << "\tA\n" <<
      i2 << "\t" << bpp.ff << "\tB\n" <<
      x << "\tFEQ(A,B)\n" <<
      sol << "\t(A < B)\n";
}

int feq(u_int32_t x1, u_int32_t x2){
  u_int32_t e1 = (x1 & 0x7f800000) >> 23;
  u_int32_t e2 = (x2 & 0x7f800000) >> 23;
  int ans;
  
  if((e1 == 0 || e1 ==255) && (e2 == 0 || e2 == 255))
    ans = 1;
  else if(x1 == x2)
    ans = 1;
  else
    ans = 0;

  return ans;
}

//01文字列をu_intに変換
u_int32_t to_int(string x){
  int k = x.size();
  u_int32_t ans = 0;
  for(int i = 0; i<k;i++){
    ans <<= 1;
    if(x.at(i) == '1'){
      ans++;
    }
  }
  return ans;
}

/*
  indata_sに入っているデータについて演算を行う
  indata_sには間違いが起こりやすそうなものが入っている。
*/
void org_in(){
  int i = 0;
  ifstream indata_s("indata_s");
  ofstream e_outdata("e_outdata");
  u_int32_t a,b;
  int x;
  string o,as,bs;
  char c;

  while(indata_s >> as >> bs){
    i++;
    a = to_int(as);
    b = to_int(bs);

    x = feq(a, b);

#if NODATA != 1
    o = to_string1(x);
    e_outdata << o << "\n";    
#endif
    
    t_or_f(a,b,x,to_string(a),to_string(b),i);//デバッグ
    indata_s.get(c);
  }
}



  
int main(){
  org_in();    
return 0;
}
  

