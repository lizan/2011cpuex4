library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_textio.all;

library STD;
use STD.textio.all;
entity sqrt_inv_test_f is
end sqrt_inv_test_f;

architecture sim_sqrt_inv of sqrt_inv_test_f is
  component sqrt_inv
    port (I : in std_logic_vector(31 downto 0);
          ANS : out std_logic_vector(31 downto 0));
  end component;
  signal S1 : std_logic_vector(31 downto 0);
  signal S2 : std_logic_vector(31 downto 0);
  signal ANS : std_logic_vector(31 downto 0);
  type rom_t is array (0 to 7) of std_logic_vector(31 downto 0);

begin  -- sim_sqrt_inv

  P: process
    file VECTOR : text is in "/home/kozai/2011cpuex4/fpu/sqrt/sqrt_inv/indata";
    file OVECTOR : text is out "/home/kozai/2011cpuex4/fpu/sqrt/sqrt_inv/outdata";
    
    variable v_li : line;
    variable v_li2 : line;
    variable v_s1 : std_logic_vector(31 downto 0);
  begin
    s1 <= (others => '0');
    for i in 0 to 110000000 loop         --100000000
      readline(vector, v_li);
      read(v_li, v_s1);
      s1 <= v_s1;
      wait for 1 ns;
      write(v_li2,ANS);
      writeline(OVECTOR, v_li2);    
    end loop;  -- i
    wait;
  end process;

  HINV : SQRT_INV port map (S1,ANS);
  

end sim_sqrt_inv;
