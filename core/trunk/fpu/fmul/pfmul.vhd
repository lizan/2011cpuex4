library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity PFMUL is  
  port (CLK : in std_logic;
        FA, FB : in  std_logic_vector(31 downto 0);
        Q    : out std_logic_vector(31 downto 0));
end PFMUL;

architecture RTL of PFMUL is
  signal MA1, MA2, MB1, MB2 : std_logic_vector(23 downto 0);
  signal EA1, EA2, EB1, EB2 : std_logic_vector(9 downto 0);
  signal MQ1, MQ2, MQ3      : std_logic_vector(25 downto 0);
  signal EQ1, EQ2, EQ3, EQ4 : std_logic_vector(9 downto 0);
  signal EQ5, EQ6, EQ       : std_logic_vector(7 downto 0);
  signal MQ                 : std_logic_vector(22 downto 0);
  signal S1, S2, SQ, ZERO_FLAG_A1, ZERO_FLAG_A2, ZERO_FLAG_A3, ZERO_FLAG_B1, ZERO_FLAG_B2, ZERO_FLAG_B3:  std_logic;
  
begin  -- RTL

  MA1 <= '1' & FA(22 downto 0);
  MB1 <= '1' & FB(22 downto 0);
  EA1 <= "00" & FA(30 downto 23);
  EB1 <= "00" & FB(30 downto 23);
  S1 <= FA(31) xor FB(31);
  ZERO_FLAG_A1 <= '1' when FA(22 downto 0) = "00000000000000000000000"
                  and FA(30 downto 23) = "00000000" else
                  '0';
  ZERO_FLAG_B1 <= '1' when FB(22 downto 0) = "00000000000000000000000"
                  and FB(30 downto 23) = "00000000" else
                  '0';
  process
  begin  -- process
    wait until rising_edge(CLK);
    MA2 <= MA1;
    MB2 <= MB1;
    EA2 <= EA1;
    EB2 <= EB1;
    S2 <= S1;
    ZERO_FLAG_A2 <= ZERO_FLAG_A1;
    ZERO_FLAG_B2 <= ZERO_FLAG_B1;
  end process;

  process (MA2, MB2)
    variable TMQ : std_logic_vector(47 downto 0);
  begin  -- process
    TMQ := MA2 * MB2;
    MQ1 <= TMQ(47 downto 22);
  end process;
  
  EQ1 <= "0011111111" when EA2(7 downto 0) = "11111111" or EB2(7 downto 0) = "11111111" else
         "0000000000" when EA2(7 downto 0) = "00000000" or EB2(7 downto 0) = "00000000" else
         EA2 + EB2 - "0001111111";
  EQ2 <= "0011111111" when EA2(7 downto 0) = "11111111" or EB2(7 downto 0) = "11111111" else
         "0000000000" when EA2(7 downto 0) = "00000000" or EB2(7 downto 0) = "00000000" else
         EA2 + EB2 - "0001111110";

  process
  begin  -- process
    wait until rising_edge(CLK);
    SQ <= S2;
    MQ2 <= MQ1;
    EQ3 <= EQ1;
    EQ4 <= EQ2;
    ZERO_FLAG_A3 <= ZERO_FLAG_A2;
    ZERO_FLAG_B3 <= ZERO_FLAG_B2;    
  end process;

  MQ3 <= MQ2 + "00000000000000000000000001" when MQ2(25) = '0' else
         MQ2 + "00000000000000000000000010";
  
  EQ5 <= "00000000" when EQ3(9 downto 8) = "11" else
         "11111111" when EQ3(9 downto 8) = "01" else
         EQ3(7 downto 0);
  
  EQ& <= "00000000" when EQ4(9 downto 8) = "11" else
         "11111111" when EQ4(9 downto 8) = "01" else
         EQ4(7 downto 0);

  MQ <= "00000000000000000000000" when ZERO_FLAG_A3 = '1' or ZERO_FLAG_B3 = '1' else
        MQ3(23 downto 1) when MQ3(25) = '0' else
        MQ3(24 downto 2);

  EQ <= EQ5 when MQ3(25) = '0' else
        EQ6;
  
  Q <= SQ & EQ & MQ;
end RTL;
