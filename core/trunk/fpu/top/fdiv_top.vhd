
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity top is
  Port ( MCLK1 : in  STD_LOGIC;
         RS_TX : out  STD_LOGIC);
end top;

architecture example of top is
  signal clk,iclk: std_logic;
  signal rom_o: std_logic_vector(7 downto 0) := x"0e";
  signal rom_os,rom_in1,rom_in2: std_logic_vector(31 downto 0);
  constant in1arr : std_logic_vector(31 downto 0) := x"b738ccb4";
  constant in2arr : std_logic_vector(31 downto 0) :=x"5395a483";
  constant outarr : std_logic_vector(31 downto 0):= x"a31e1282"; 


  component FDIV
  port (I1,I2 : in std_logic_vector(31 downto 0);
        ANS : out std_logic_vector(31 downto 0));
  end component;
  
  component u232c
    generic (wtime: std_logic_vector(15 downto 0) := x"1ADB");
    Port ( clk  : in  STD_LOGIC;
           data : in  STD_LOGIC_VECTOR (7 downto 0);
           go   : in  STD_LOGIC;
           busy : out STD_LOGIC;
           tx   : out STD_LOGIC);
  end component;
  signal uart_go: std_logic;
  signal uart_busy: std_logic := '0';
  
begin
  rom_in1 <= in1arr;
  rom_in2 <= in2arr;
  rom_o <= x"00" when rom_os = outarr else
           x"ff";

  
  M_FLOAT : FDIV port map (
    I1 => rom_in1,
    I2 => rom_in2,
    ANS => rom_os);
  ib: IBUFG port map (
    i=>MCLK1,
    o=>iclk);
  bg: BUFG port map (
    i=>iclk,
    o=>clk);
  rs232c: u232c generic map (wtime=>x"1ADB")
  port map (
    clk=>clk,
    data=>rom_o,
    go=>uart_go,
    busy=>uart_busy,
    tx=>rs_tx);
  
  send_msg: process(clk)    
  begin
    if rising_edge(clk) then   
      if uart_busy='0' and uart_go='0' then
        uart_go<='1';
      else
        uart_go<='0';
      end if;
    end if;
  end process;
end example;
