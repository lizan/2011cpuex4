
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity top is
  Port ( MCLK1 : in  STD_LOGIC;
         RS_TX : out  STD_LOGIC);
end top;

architecture example of top is
  signal clk,iclk,CLOCK: std_logic;
  type data is array(0 to 18) of std_logic_vector(31 downto 0);
  signal rom_o: std_logic_vector(7 downto 0);
  signal rom_os,rom_in1,rom_in2: std_logic_vector(31 downto 0);
  constant in1arr : data :=
(x"50d6291c",x"4f32bbf3",x"727bb90e",x"b5e45b33",x"5ab0edd3",x"f8bd2968",x"b9ec32dc",x"5e7eca0a",x"af5c4fba",x"6983790c",x"4a4f5dba",x"48cea324",x"c10a212f",x"72012550",x"ca297f9e",x"5884f891",x"d002bff1",x"e485433e",x"00000000");
  constant in2arr : data :=
(x"9b800000",x"7b000000",x"76fbb910",x"3ee45b2c",x"f130edd3",x"e5bd2966",x"aeec32d5",x"727eca00",x"aedc4fb5",x"5e03790c",x"2dcf5dbc",x"f5e9305f",x"fd745106",x"4c9323c4",x"4dc152bf",x"34766e03",x"9cfa9dba",x"0bf5e3ed",x"00000000");
  constant outarr : data :=
(x"acd6291d",x"00000000",x"00000000",x"b54bb28d",x"00000000",x"00000000",x"2959edc9",x"00000000",x"1ebd991c",x"00000000",x"38a7f8b6",x"ff3c3987",x"7f03d358",x"7f1474f0",x"d87fffff",x"4d800000",x"2d800000",x"b1000000",x"ffffffff");
  
  component FMUL
  port (I1,I2 : in std_logic_vector(31 downto 0);
        ANS : out std_logic_vector(31 downto 0));
  end component;
  
  component u232c
    generic (wtime: std_logic_vector(15 downto 0) := x"1ADB");
    Port ( clk  : in  STD_LOGIC;
           data : in  STD_LOGIC_VECTOR (7 downto 0);
           go   : in  STD_LOGIC;
           busy : out STD_LOGIC;
           tx   : out STD_LOGIC);
  end component;
  signal uart_go: std_logic;
  signal uart_busy: std_logic := '0';
  signal N : integer := 0;
  
begin
  P: process (CLOCK)
    variable PN : integer := 0;
  begin
    if rising_edge(CLOCK) then
      PN := N + 1;
      if N = 18 then
        N <= 0;
      else
        N <= PN;
      end if; 
    end if;  
  end process P;
  rom_in1 <= in1arr(N);
  rom_in2 <= in2arr(N);
  rom_o <= x"00" when rom_os = outarr(N) else
           x"ff";
  
  
  M_FLOAT : FMUL port map (
    I1 => rom_in1,
    I2 => rom_in2,
    ANS => rom_os);
  ib: IBUFG port map (
    i=>MCLK1,
    o=>iclk);
  bg: BUFG port map (
    i=>iclk,
    o=>clk);
  rs232c: u232c generic map (wtime=>x"1ADB")
  port map (
    clk=>clk,
    data=>rom_o,
    go=>uart_go,
    busy=>uart_busy,
    tx=>rs_tx);
  
  send_msg: process(clk)    
  begin
    if rising_edge(clk) then   
      if uart_busy='0' and uart_go='0' then
        uart_go<='1';
        CLOCK <= '1';
      else
        uart_go<='0';
        CLOCK <= '0';
      end if;
    end if;
  end process;
end example;
