-------------------------------------------------------------------------------
-- IOqueue
-- LastUpdate: 2012/3/11 01:50 
-- Author    : ultraredrays
-- Email     : ultraredrays@gmail.com
-- Todo      : おおん

-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity Iqueue is
  port (
    -- clk   : アーキテクチャ全体のクロック
    clk       : in  std_logic;
    R_Byte    : out std_logic_vector(7 downto 0);
    Recv      : in  std_logic;
    CanRecv   : out std_logic;
    rs_R_Byte : in  std_logic_vector(7 downto 0);
    rs_Recv   : out std_logic;
    rs_CanRecv: in std_logic);
end Iqueue;

architecture archi of Iqueue is
  type Queue is array (0 to 2047) of std_logic_vector(7 downto 0);
  --read用のqueue関係
  signal IQueue : Queue;
  signal Itop : std_logic_vector(10 downto 0) := "00000000000";
  signal Itail : std_logic_vector(10 downto 0) := "00000000000";
  signal tmpcanrecv : std_logic;

begin  

  CanRecv <= '1' when tmpcanrecv = '1'else
             '0';
  tmpcanrecv <= '1' when Itop /= Itail else
                '0';
  R_Byte <= IQueue(conv_integer(Itail));

  rs_Recv <= '1';

  I: process(clk)
  begin
    if rising_edge(clk) then
      --IQueueへのpop
      if Recv = '1' and tmpcanrecv = '1' then
        Itail <= Itail + "00000000001";
      end if;
      -- IQueueへのpush
      if rs_CanRecv = '1' then
        IQueue(conv_integer(Itop)) <= rs_R_Byte;
        Itop <= Itop + "0000000001";
      end if;
    end if;
  end process;
  
end archi;

