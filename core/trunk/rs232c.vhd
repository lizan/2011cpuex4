-------------------------------------------------------------------------------
-- rs232c
-- LastUpdate: 2011/11/08 02:04 
-- Author    : ultraredrays
-- Email     : ultraredrays@gmail.com
-- Todo      : loopback出来てない

-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity rs232c is
  generic (
    constant wtime : std_logic_vector(15 downto 0) := x"1ABD");
  port (
    -- clk   : アーキテクチャ全体のクロック
    clk       : in  std_logic;
    --S_Byte : 送信する1Byteのデータ 
    S_Byte    : in  std_logic_vector(7 downto 0);
    --R_Byte : 受信した1Byteのデータ
    R_Byte    : out std_logic_vector(7 downto 0);
    --Send   : 送信データ使用FLAG シグナルが受け取ると送信ステートマシンが動作      
    Send      : in  std_logic;
    --Recv   : 受信データ使用FLAG シグナルを受け取ると受信ステータスが変化 　　　　　　　　
    Recv      : in  std_logic;
    --S_busy : データ送信可能FLAG 初期状態は0,信号送信中は1,それ以外は0
    CanSend   : out std_logic;
    --S_busy : データ受信可能FLAG 初期状態は0,信号送信中は1,それ以外は0
    CanRecv   : out std_logic;
    --Tx     : 送信用信号線
    Tx        : out std_logic;
    --Rx     : 受信用信号線
    Rx        : in  std_logic);
end rs232c;

architecture archi of rs232c is
  signal valid : std_logic := '0';
  signal s_timer : std_logic_vector(15 downto 0) := x"0000";   -- 送信用カウンタ
  signal r_timer : std_logic_vector(15 downto 0) := x"0000";   -- 受信用カウンタ
  signal s_state : std_logic_vector(3 downto 0) := "1010";     -- 送信用状態変数
  signal r_state : std_logic_vector(3 downto 0) := "1010";     -- 受信用状態変数
  signal s_buf : std_logic_vector(8 downto 0) := "111111111";  -- 送信用バッファ
  signal r_buf : std_logic_vector(8 downto 0) := "111111111";  -- 受信用バッファ

begin  -- archi

  Tx<=s_buf(0);                          --下位ビットから順次右シフトして送信
  CanSend<= '1' when s_state="1010" else
            '0';

  r_buf(8) <= Rx;                        --下位ビットから順次右シフトして受信
  CanRecv<= '1' when r_state="1010" and valid = '1' else
            '0';

  S: process(clk,Send)
  begin
    if rising_edge(clk) then
      case s_state is
        when "1010" =>
          if Send = '1' then
            s_buf <= S_Byte & "0";
            s_state <= s_state - 1;
            s_timer <= wtime;
          end if;       
        when "0000" =>
          if s_timer = 0 then
            s_buf <= "111111111";
            s_state <= "1010";
            s_timer <= x"0000";
          else
            s_timer <= s_timer - 1;              
          end if;
        when others =>
          if s_timer = x"0000" then
            s_buf <= "1"&s_buf(8 downto 1);
            s_state <= s_state - 1;
            s_timer <= wtime;
          else
            s_timer <= s_timer - 1;  
          end if;
      end case;
    end if;
  end process;
      
  R: process(clk,r_buf(8))
  begin
    if rising_edge(clk) then
      if Recv = '1' then
        valid <= '0';
      end if;
   
      case r_state is
        when "1010" =>
          if Rx = '0' then
            r_buf(7 downto 0) <= r_buf(8 downto 1);
            r_state <= r_state - 1;
            r_timer <= '0'&wtime(15 downto 1);
          end if;
        when "0000" =>
          if r_timer = 0 then
            R_Byte <= r_buf(7 downto 0);
            r_state <= "1010";
            r_timer <= x"0000";
            valid <= '1';
          else
            r_timer <= r_timer - 1;
          end if;
        when others =>
          if r_timer = 0 then
            r_buf(7 downto 0) <= r_buf(8 downto 1);
            r_state <= r_state - 1;
            r_timer <= wtime;
          else
            r_timer <= r_timer - 1;
          end if;
      end case;
    end if;
  end process;

end archi;
