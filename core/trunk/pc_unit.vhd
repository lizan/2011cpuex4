-------------------------------------------------------------------------------
-- pc_unit        : プログラムカウンタ制御器
-- LastUpdate: 2011/10/14 15:31 
-- Author    : ultraredrays
-- Email     : ultraredrays@gmail.com
-- Todo      : すべて
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity pc_unit is
  
  port (
    clk       : in  std_logic;
    PCSource  : in  std_logic_vector(1 downto 0);
    PCWrite   : in  std_logic;
    PC        : out std_logic_vector(31 downto 0);
    PCJump    : in  std_logic_vector(25 downto 0);
    PCJumpr   : in  std_logic_vector(31 downto 0);
    PCBranch  : in  std_logic_vector(31 downto 0));

end pc_unit;

architecture archi of pc_unit is

  signal pctmp  : std_logic_vector(31 downto 0) := x"00000000";
  signal pcnext : std_logic_vector(31 downto 0) := x"00000000";

begin  -- archi

  PC <= pctmp;
  pcnext <= pctmp(31 downto 26) & PCJump when PCSource = "01" else
            PCJumpr when PCSource = "10" else
            PcBranch + x"00000001" when PCSource = "11" else
            pctmp + x"00000001";
  
  renew_pc : process(clk)
  begin 
    if rising_edge(clk) then
      if PCWrite = '1' then
        pctmp <= pcnext;        
      end if;
    end if;
  end process;

end archi;
