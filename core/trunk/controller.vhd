-------------------------------------------------------------------------------
-- controller データパス制御器
-- LastUpdate: 2012/1/17 6:05 
-- Author    : ultraredrays
-- Email     : ultraredrays@gmail.com
-- Todo      : 動作確認
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;
use work.instruction_pkg.all;

entity controller is
  port (
    clk         : in  std_logic;
    Opcode      : in  std_logic_vector(5 downto 0);
    f_funct     : in  std_logic_vector(5 downto 0);
    Zero        : in  std_logic;                     -- ALUの出力が0であるか
    PCSource    : out std_logic_vector(1 downto 0);  --次のPCにどのアドレスを使用するか 00:PC+1,01:IR[25-0],10:Reg(IR[25-21]),11:ALUOut  
    PCWrite     : out std_logic;                     --PC書き込み制御信号
    MemSource   : out std_logic;                     --メモリ参照アドレス元制御信号 0:PC,1:ALU
    MemRead     : out std_logic;                     --メモリ読み出し制御信号
    MemWrite    : out std_logic;                     --メモリ書き出し制御信号
    IRWrite     : out std_logic;                     --命令レジスタ書き込み制御信号
    MDRWrite    : out std_logic;                     --MDR書き込み制御信号 
    ALUOp       : out std_logic_vector(1 downto 0);  --ALU制御信号 00:ADD,01:SUB,10:RTYPE,11:I形式命令
    ALUSourceA  : out std_logic_vector(1 downto 0);  --Aレジスタ書き込み元制御信号 00:IR[25-21]Reg,01:IR[25-21]FReg,10:PC
    ALUSourceB  : out std_logic_vector(1 downto 0);  --Bレジスタ書き込み元制御信号 00:IR[20-16]Reg,01:IR[20-16]FReg,10:offset
    ALUWriteA   : out std_logic;                     --Aレジスタ書き込み制御信号
    ALUWriteB   : out std_logic;                     --Bレジスタ書き込み制御信号
    ALUOutSource : out std_logic;                    --ALU出力レジスタ書き込み元制御信号 0:ALU, 1:FPU
    ALUOutWrite : out std_logic;                     --ALU出力レジスタ書き込み信号
    RegSource   : out std_logic_vector(1 downto 0);  --レジスタ書き込み元制御信号 00:ALUOut,01:MDR,10:PC+1,11:RS232C
    RegWrite    : out std_logic;                     --レジスタ書き込み制御信号
    FRegWrite   : out std_logic;                     --浮動小数点レジスタ書き込み制御信号
    RegDest     : out std_logic_vector(1 downto 0);
    Send        : out std_logic;
    Recv        : out std_logic;
    CanSend     : in  std_logic;
    CanRecv     : in  std_logic); --レジスタ書き込み先制御信号 00:IR[20-16],01:IR[15-11],10:(31),11:IR[25-21]
  

end controller;

architecture archi of controller is

  signal state : std_logic_vector(3 downto 0) := "0000";
  
begin  -- archi

  PCSource <= "01" when (Opcode = J and state = x"3") or
                        (Opcode = J_LINK and state = x"3") else
              "10" when Opcode = J_REG and state = x"3" else
              "11" when Opcode = BEQ and state = x"5" and Zero = '1' else
              "00";
  
  PCWrite <= '1' when
              (Opcode = RTYPE and state = x"5") or
              (Opcode = ADDI and state = x"5") or
              (Opcode = SHRI and state = x"5") or
              (Opcode = SHLI and state = x"5") or
              (Opcode = F_RTYPE and state = x"A") or
              (Opcode = F_SQRT and state = x"A") or
              (Opcode = LOAD and state = x"8") or
              (Opcode = STORE and state = x"7") or
              (Opcode = F_LOAD and state = x"8") or
              (Opcode = F_STORE and state = x"7") or
              (Opcode = J and state = x"3") or
              (Opcode = J_REG and state = x"3") or
              (Opcode = J_LINK and state = x"3") or
              (Opcode = BEQ and state = x"5") or
              (Opcode = IOREAD and state = x"4") or
              (Opcode = IOWRITE and state = x"5")
              else
             '0';

  MemSource <= '1'
              when
              (Opcode = LOAD and state = x"5") or
              (Opcode = STORE and state = x"5") or
              (Opcode = F_LOAD and state = x"5") or
              (Opcode = F_STORE and state = x"5")
              else
               '0';

  MemWrite <= '1'
             when
             (Opcode = STORE and state = x"5") or
             (Opcode = F_STORE and state = x"5")
             else
             '0';

  MemRead <= '1'
              when
              state = "0000" or
              (Opcode = LOAD and state = x"5") or
              (Opcode = F_LOAD and state = x"5")
              else
              '0';

  IRWrite  <= '1' when state = x"2" else
              '0';

  MDRWrite <= '1'
              when
              (Opcode = LOAD and state = "0111") or
              (Opcode = F_LOAD and state = "0111")
              else
              '0';

  ALUOp <= "10"
           when
           Opcode = RTYPE or
           Opcode = F_RTYPE or
           Opcode = F_SQRT or
           (Opcode = SHLI and state = x"4") or
           (Opcode = SHRI and state = x"4")
           else
           "01" when Opcode = BEQ and state = "0101" else
           "00";
  
  ALUSourceA <= "10"
                when Opcode = BEQ and state = "0011"
                else
                "01"
                when
                (Opcode = F_RTYPE and state = "0011") or
                (Opcode = F_SQRT and state = "0011")
                else   
                "00";

  ALUSourceB <= "10"
                when
                (Opcode = ADDI and state = "0011") or
                (Opcode = SHRI and state = "0011") or
                (Opcode = SHLI and state = "0011") or
                (Opcode = LOAD and state = "0011") or
                (Opcode = STORE and state = "0011") or
                (Opcode = F_LOAD and state = "0011") or
                (Opcode = F_STORE and state = "0011") or
                (Opcode = BEQ and state = "0011")
                else
                "01"
                when
                (Opcode = F_RTYPE and state = x"3") or
                (Opcode = F_STORE and state = x"6")
                else   
                "00";

  ALUWriteA <= '1'
               when
               (Opcode = RTYPE and state = "0011") or
               (Opcode = ADDI and state = "0011") or
               (Opcode = SHRI and state = "0011") or
               (Opcode = SHLI and state = "0011") or
               (Opcode = F_RTYPE and state = "0011") or
               (Opcode = F_SQRT and state = "0011") or
               (Opcode = LOAD and state = "0011") or
               (Opcode = STORE and state = "0011") or
               (Opcode = F_LOAD and state = "0011") or
               (Opcode = F_STORE and state = "0011") or
               (Opcode = BEQ and state = "0011") or
               (Opcode = BEQ and state = "0100") or
               (Opcode = IOWRITE and state = "0011")
               else
               '0';

  ALUWriteB <= '1'
               when
               (Opcode = RTYPE and state = "0011") or
               (Opcode = ADDI and state = "0011") or
               (Opcode = SHRI and state = "0011") or
               (Opcode = SHLI and state = "0011") or
               (Opcode = F_RTYPE and state = "0011") or
               (Opcode = F_SQRT and state = "0011") or
               (Opcode = LOAD and state = "0011") or
               (Opcode = STORE and state = "0011") or
               (Opcode = STORE and state = "0110") or
               (Opcode = F_LOAD and state = "0011") or
               (Opcode = F_STORE and state = "0011") or
               (Opcode = F_STORE and state = "0110") or
               (Opcode = BEQ and state = "0011") or
               (Opcode = BEQ and state = "0100")
               else
               '0';

  ALUOutSource <= '1' when Opcode = F_RTYPE or Opcode = F_SQRT else
                  '0';
  
  ALUOutWrite <= '1'
                 when
                 state = "0100" or
                 (Opcode = F_RTYPE and state = x"9") or
                 (Opcode = F_SQRT and state = x"9")
                 else
                 '0';

  RegSource <= "11" when Opcode = IOREAD and state = x"4" else
               "10" when Opcode = J_LINK and state = "0011" else
               "01"
               when
               (Opcode = LOAD and state = "1000") or
               (Opcode = F_LOAD and state = "1000")
               else
               "00";

  RegWrite <= '1'
              when
              (Opcode = RTYPE and state = "0101") or
              (Opcode = ADDI and state = "0101") or
              (Opcode = SHRI and state = "0101") or
              (Opcode = SHLI and state = "0101") or
              (Opcode = LOAD and state = "1000") or
              (Opcode = J_LINK and state = "0011") or
              (Opcode = IOREAD and state = "0100") or
              (Opcode = F_RTYPE and f_funct = FR_LT and state = x"A") or
              (Opcode = F_RTYPE and f_funct = FR_EQ and state = x"A")
              else
              '0';
  FRegWrite <= '1'
              when
              (Opcode = F_RTYPE and f_funct = FR_ADD and state = x"A") or
              (Opcode = F_RTYPE and f_funct = FR_SUB and state = x"A") or
              (Opcode = F_RTYPE and f_funct = FR_MUL and state = x"A") or
              (Opcode = F_RTYPE and f_funct = FR_DIV and state = x"A") or
              (Opcode = F_SQRT and state = x"A") or
              (Opcode = F_LOAD and state = x"8") else
               '0';

  RegDest <= "11" when Opcode = IOREAD and state = "0100" else
             "10" when Opcode = J_LINK and state = "0011" else
             "01"
               when
               (Opcode = RTYPE and state = x"5") or
               (Opcode = F_RTYPE and state = x"A")
             else
             "00";
  
  Send <= '1' when Opcode = IOWRITE and state = x"4" else
          '0';
  Recv <= '1' when Opcode = IOREAD and state = x"4" else
          '0';

  Ctrl: process (clk)
  begin  -- process Ctrl
    if rising_edge(clk) then
      if not((Opcode = IOREAD and CanRecv = '0' and state = x"3") or
             (Opcode = IOWRITE and CanSend = '0' and state = x"4"))
      then
        state <= state + 1;        
      end if;

      if (Opcode = RTYPE and state = x"5") or
        (Opcode = ADDI and state = x"5") or
        (Opcode = SHRI and state = x"5") or
        (Opcode = SHLI and state = x"5") or
        (Opcode = F_RTYPE and state = x"A") or
        (Opcode = F_SQRT and state = x"A") or
        (Opcode = LOAD and state = x"8") or
        (Opcode = STORE and state = x"7") or
        (Opcode = F_LOAD and state = x"8") or
        (Opcode = F_STORE and state = x"7") or
        (Opcode = J and state = x"3") or
        (Opcode = J_REG and state = x"3") or
        (Opcode = J_LINK and state = x"3") or
        (Opcode = BEQ and state = x"5") or
        (Opcode = IOREAD and state = x"4") or
        (Opcode = IOWRITE and state = x"5")
      then
        state <= "0000";
      end if;
    end if;
  end process Ctrl;

end archi;
