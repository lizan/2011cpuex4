-------------------------------------------------------------------------------
-- InstructionPkg
-- LastUpdate : 2011/12/08 14:53
-- Author     : ultraredrays
-- Email      : ultraredrays@gmail.com
-- Comment    :
--  命令セットなどをマクロ化したもの
--  読み込むには、このソースファイルを論理合成する際に同じディレクトリに突っ込み
--  use.instruction_pkg.all でおk
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.std_logic_signed.all;

package instruction_pkg is

  -- Opcode用マクロ
  constant RTYPE   : std_logic_vector(5 downto 0) := "000000";  -- R形式算術命令
  constant ADDI    : std_logic_vector(5 downto 0) := "001000";  -- ADDI
  constant SHRI    : std_logic_vector(5 downto 0) := "001010";  -- SHIFT_RIGHT_Immidiate
  constant SHLI    : std_logic_vector(5 downto 0) := "001011";  -- SHIFT_LEFT_Immidiate
  constant F_RTYPE : std_logic_vector(5 downto 0) := "010000";  -- 浮動小数点R形式算術命令
  constant F_SQRT  : std_logic_vector(5 downto 0) := "010001";  -- 平方根の計算
  constant LOAD    : std_logic_vector(5 downto 0) := "100011";
  constant STORE   : std_logic_vector(5 downto 0) := "100010";
  constant F_LOAD  : std_logic_vector(5 downto 0) := "111001";
  constant F_STORE : std_logic_vector(5 downto 0) := "111000";
  constant J       : std_logic_vector(5 downto 0) := "000010";
  constant J_REG   : std_logic_vector(5 downto 0) := "000001";
  constant J_LINK  : std_logic_vector(5 downto 0) := "000011";
  constant BEQ     : std_logic_vector(5 downto 0) := "000100";
  constant IOREAD  : std_logic_vector(5 downto 0) := "111111";
  constant IOWRITE : std_logic_vector(5 downto 0) := "111110";
  

  -- R形式算術命令用マクロ
  constant R_ADD : std_logic_vector(5 downto 0) := "100000";
  constant R_SUB : std_logic_vector(5 downto 0) := "100010";
  constant R_MUL : std_logic_vector(5 downto 0) := "011000";
  constant R_DIV : std_logic_vector(5 downto 0) := "011010";
  constant R_AND : std_logic_vector(5 downto 0) := "100100";
  constant R_OR  : std_logic_vector(5 downto 0) := "100101";
  constant R_SHR : std_logic_vector(5 downto 0) := "000010";
  constant R_SHL : std_logic_vector(5 downto 0) := "000011";
  constant R_LT  : std_logic_vector(5 downto 0) := "101010";
  constant R_EQ  : std_logic_vector(5 downto 0) := "101011";

  -- 浮動小数点R形式算術命令用マクロ
  constant FR_ADD : std_logic_vector(5 downto 0) := "100000";
  constant FR_SUB : std_logic_vector(5 downto 0) := "100010";
  constant FR_MUL : std_logic_vector(5 downto 0) := "011000";
  constant FR_DIV : std_logic_vector(5 downto 0) := "011010";
  constant FR_LT  : std_logic_vector(5 downto 0) := "101010";
  constant FR_EQ  : std_logic_vector(5 downto 0) := "101011";

end instruction_pkg;
