-------------------------------------------------------------------------------
-- alu_controller
-- LastUpdate: 2011/10/14 11:23
-- Author    : ultraredrays
-- Email     : ultraredrays@gmail.com
-- Todo      : 動作確認
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;
use work.instruction_pkg.all;

entity alu_controller is
  
  port (
    ALUOp : in   std_logic_vector(1 downto 0);  -- ALUOp信号 from controller
    funct : in   std_logic_vector(5 downto 0);  -- funct from OperationMemory
    code  : out  std_logic_vector(3 downto 0));  -- code for ALU

end alu_controller;

architecture archi of alu_controller is

begin  -- archi

  process(ALUOp,funct)
  begin
    case ALUOp is           
      when "10" =>
        case funct is
          when R_ADD =>
            code <= "0010";
          when R_SUB =>
            code <= "0110";
          when R_MUL =>
            code <= "0011";
          when R_DIV =>
            code <= "0100";
          when R_AND =>
            code <= "0000";             --And
          when R_OR =>
            code <= "0001";             --Or
          when R_SHR =>
            code <= "1000";             --shift right
          when R_SHL =>
            code <= "1001";             --shift left
          when R_LT =>
            code <= "0111";             --Slt
          when R_EQ =>
            code <= "0101";             --Seq
          when others =>
            code <= "1111";             --else
        end case;
      when "01" =>
        code <= "0110";                 --Sub
      when "00" =>
        code <= "0010";                 --Add
      when others =>
        code <= "1111";
    end case;
  end process;

end archi;
