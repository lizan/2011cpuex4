-------------------------------------------------------------------------------
-- UGL1 top
-- LastUpdate: 2011/10/14 14:13 
-- Author    : ultraredrays
-- Email     : ultraredrays@gmail.com
-- Todo      : ramの論理合成が終わらない
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.instruction_pkg.all;

library UNISIM;
use UNISIM.VComponents.all;

entity top is
  Port ( MCLK1 : in  STD_LOGIC;
         RS_TX : out  STD_LOGIC;
         RS_RX : in STD_LOGIC;
         ZD     : inout std_logic_vector(31 downto 0);  -- SRAMとやりとりするデータ
         ZA     : out    std_logic_vector(19 downto 0);  -- SRAMに読み書きするAddress
         XE1    : out    std_logic;     -- チップのenable信号、0でactiveとなる 電力消費を気にしないのであれば0固定
         E2A    : out    std_logic;     -- チップのenable信号、1でactiveとなる 電力消費を気にしないのであれば1固定
         XE3    : out    std_logic;     -- チップのenable信号、0でactiveとなる 電力消費を気にしないのであれば0固定
         XZBE   : out    std_logic_vector(3 downto 0);  --各Byteの書き込みマスク、全部0で固定
         XGA    : out    std_logic;     -- 出力のenable信号、0でactiveとなる 小休止とかしないのであれば0固定
         XWA    : out    std_logic;     -- 書き込みのenable信号、0でactive(書き込み可能)となる。この信号が来たときのアドレスに、2clock後のデータが書き込まれる
         XZCKE  : out    std_logic;     --clockのenable信号、さすがに0固定で良いと思う 
         ZCLKMA : out    std_logic_vector(1 downto 0);  --clock、遠いので2bitあるが、どちらにも同じ物を送れば良い
         ADVA   : out    std_logic;     --Burstアクセス(アドレスを連続して使用する)モード用に使う 今は0で良い  
         XFT    : out    std_logic;     --Pipelineモードか、FlowThroughモードかを切り替える 1固定
         XLBO   : out    std_logic;     --バーストアクセス用のアドレス順を指定する、active lowとあるが、とりあえず1で固定
         ZZA    : out    std_logic);    --1で省電力モード、とりあえず0で固定
end top;

architecture example of top is

  component sram_controller
    port (
      clk          : in std_logic;
      MemRead      : in std_logic;
      MemWrite     : in std_logic;
      XWA          : out std_logic;
      MemReadData  : out std_logic_vector(31 downto 0);
      MemWriteData : in std_logic_vector(31 downto 0);
      ZD           : inout std_logic_vector(31 downto 0));
  end component;

  component alu
    port (
      A     : in  std_logic_vector(31 downto 0);  -- input data
      B     : in  std_logic_vector(31 downto 0);  -- input data
      code  : in  std_logic_vector( 3 downto 0);  -- alu制御入力
      O     : out std_logic_vector(31 downto 0);  -- output data
      Z     : out std_logic);
  end component;

  component alu_controller port (
    ALUOp : in   std_logic_vector(1 downto 0);  -- ALUOp信号 from controller
    funct : in   std_logic_vector(5 downto 0);  -- funct from OperationMemory
    code  : out  std_logic_vector(3 downto 0));  -- code for ALU
  end component;

  component controller
    port (
      clk         : in  std_logic;
      Opcode      : in  std_logic_vector(5 downto 0);
      f_funct     : in  std_logic_vector(5 downto 0);
      Zero        : in  std_logic;                     -- ALUの出力が0であるか
      PCSource    : out std_logic_vector(1 downto 0);  --
      PCWrite     : out std_logic;                     -- PC書き込み制御信号
      MemSource   : out std_logic;                     -- メモリ参照アドレス元制御信号 0:PC,1:ALU
      MemRead     : out std_logic;                     -- メモリ読み出し制御信号
      MemWrite    : out std_logic;                     -- メモリ書き出し制御信号
      IRWrite     : out std_logic;                     -- 命令レジスタ書き込み制御信号
      MDRWrite    : out std_logic;                     -- MDR書き込み制御信号 
      ALUOp       : out std_logic_vector(1 downto 0); 
      ALUSourceA  : out std_logic_vector(1 downto 0); 
      ALUSourceB  : out std_logic_vector(1 downto 0); 
      ALUWriteA   : out std_logic;                     --Aレジスタ書き込み制御信号
      ALUWriteB   : out std_logic;                     --Bレジスタ書き込み制御信号
      ALUOutSource: out std_logic;
      ALUOutWrite : out std_logic;                     -- ALU出力レジスタ書き込み信号
      RegSource   : out std_logic_vector(1 downto 0);  --レジスタ書き込み元制御信号 00:ALUOut,01:MDR,10:PC+1
      RegWrite    : out std_logic;                     --レジスタ書き込み制御信号
      FRegWrite   : out std_logic;                     --Fレジスタ書き込み制御信号
      RegDest     : out std_logic_vector(1 downto 0);  --レジスタ書き込み先制御信号 00:IR[20-16],01:IR[15-11],10:(31)
      Send        : out std_logic;
      Recv        : out std_logic;
      CanSend     : in  std_logic;
      CanRecv     : in  std_logic); 
    
  end component;

  component pc_unit
    port (
      clk       : in  std_logic;
      PCSource  : in  std_logic_vector(1 downto 0);
      PCWrite   : in  std_logic;
      PC        : out std_logic_vector(31 downto 0);
      PCJump    : in  std_logic_vector(25 downto 0);
      PCJumpr   : in  std_logic_vector(31 downto 0);
      PCBranch  : in  std_logic_vector(31 downto 0));
  end component;

  component regs
    port (
      clk        : in  std_logic;                      -- クロック 
      SourceReg1 : in  std_logic_vector(4 downto 0);   -- 読み出し用レジスタその1
      SourceReg2 : in  std_logic_vector(4 downto 0);   -- 読み出し用レジスタその2
      DestReg    : in  std_logic_vector(4 downto 0);   -- 書き込み用レジスタ
      RegData1   : out std_logic_vector(31 downto 0);  -- 読み出しデータその1
      RegData2   : out std_logic_vector(31 downto 0);  -- 読み出しデータその2
      DestData   : in  std_logic_vector(31 downto 0);  -- 書き込みデータ
      RegWrite   : in  std_logic);                     -- レジスタ書き込み制御信号
  end component;

  component fpu
    port (
      A     : in  std_logic_vector(31 downto 0);  -- input data
      B     : in  std_logic_vector(31 downto 0);  -- input data
      funct : in  std_logic_vector(5 downto 0);   -- alu制御入力
      O     : out std_logic_vector(31 downto 0);  -- output data
      Z     : out std_logic);
  end component;

  component Iqueue is
    port (
      -- clk   : アーキテクチャ全体のクロック
      clk       : in  std_logic;
      R_Byte    : out std_logic_vector(7 downto 0);
      Recv      : in  std_logic;
      CanRecv   : out std_logic;
      rs_R_Byte : in  std_logic_vector(7 downto 0);
      rs_Recv   : out std_logic;
      rs_CanRecv: in std_logic);
  end component;
    
  component rs232c
    generic (
      constant wtime : std_logic_vector(15 downto 0) := x"1ABD");
    port (
      -- clk   : アーキテクチャ全体のクロック
      clk       : in  std_logic;
      --S_Byte : 送信する1Byteのデータ 
      S_Byte    : in  std_logic_vector(7 downto 0);
      --R_Byte : 受信した1Byteのデータ
      R_Byte    : out std_logic_vector(7 downto 0);
      --Send   : 送信データ使用可能FLAG シグナルが立ち上がると送信ステートマシンが動作      
      Send      : in  std_logic;
      --Recv   : 受信データ使用可能FLAG 初期状態は0,信号受信中は0,それ以外は1 　　　　　　　　
      Recv      : in std_logic;
      --CanSend : データ送信可能FLAG 
      CanSend   : out std_logic;
      --CanRecv : データ送信可能FLAG
      CanRecv   : out std_logic;
      --Tx     : 送信用信号線
      Tx        : out std_logic;
      --Rx     : 受信用信号線
      Rx        : in  std_logic);
  end component;

--  component Rom
--    port (
--      Addr  : in  std_logic_vector(19 downto 0);
--      Rdata : out std_logic_vector(31 downto 0);
--      R     : in  std_logic);
--  end component;
  
  signal clk, iclk  : std_logic;
  signal pc         : std_logic_vector(31 downto 0) := x"00000000";
  signal memaddr    : std_logic_vector(19 downto 0) := x"00000";
  signal memreaddata : std_logic_vector(31 downto 0) := x"00000000";
  signal memwritedata : std_logic_vector(31 downto 0) := x"00000000";
  signal ir         : std_logic_vector(31 downto 0) := x"00000000";
  signal mdr        : std_logic_vector(31 downto 0) := x"00000000";
  signal pcjump     : std_logic_vector(25 downto 0) := "11111111111111111111111111";
  signal pcjumpr    : std_logic_vector(31 downto 0) := x"FFFFFFFF";
  signal regdata1   : std_logic_vector(31 downto 0) := x"00000000";
  signal regdata2   : std_logic_vector(31 downto 0) := x"00000000";
  signal fregdata1  : std_logic_vector(31 downto 0) := x"00000000";
  signal fregdata2  : std_logic_vector(31 downto 0) := x"00000000";
  signal destdata   : std_logic_vector(31 downto 0) := x"00000000";
  signal destreg    : std_logic_vector(4 downto 0) := "00000";
  signal a          : std_logic_vector(31 downto 0) := x"00000000";
  signal b          : std_logic_vector(31 downto 0) := x"00000000";
  signal code       : std_logic_vector(3 downto 0) := "1111";
  signal o          : std_logic_vector(31 downto 0) := x"00000000";
  signal fo         : std_logic_vector(31 downto 0) := x"00000000";
  signal z          : std_logic := '1';
  signal fz         : std_logic := '1';
  signal aluout     : std_logic_vector(31 downto 0) := x"00000000";
  signal pcsource   : std_logic_vector(1 downto 0) := "00";
  signal pcwrite    : std_logic := '0';
  signal memsource  : std_logic := '0';
  signal memread    : std_logic := '0';
  signal memwrite   : std_logic := '0';
  signal irwrite    : std_logic := '0';
  signal mdrwrite   : std_logic := '0';
  signal aluop      : std_logic_vector(1 downto 0) := "11";
  signal alusourceA : std_logic_vector(1 downto 0) := "00";
  signal alusourceB : std_logic_vector(1 downto 0) := "00";
  signal aluwriteA  : std_logic := '0';
  signal aluwriteB  : std_logic := '0';
  signal aluoutsource: std_logic := '0';
  signal aluoutwrite: std_logic := '0';
  signal regsource  : std_logic_vector(1 downto 0) := "00";
  signal regdest    : std_logic_vector(1 downto 0) := "00";
  signal regwrite   : std_logic := '0';
  signal fregwrite  : std_logic := '0';
  signal r_byte     : std_logic_vector(7 downto 0) := "00000000";
  signal s_byte     : std_logic_vector(7 downto 0) := "00000000";
  signal recv       : std_logic := '0';
  signal send       : std_logic := '0';
  signal cansend    : std_logic := '1';
  signal canrecv    : std_logic := '0';
  signal rs_r_byte  : std_logic_vector(7 downto 0);
  signal rs_recv    : std_logic;
  signal rs_canrecv : std_logic;
  signal funct      : std_logic_vector(5 downto 0);
  signal f_funct    : std_logic_vector(5 downto 0);

begin
  ib: IBUFG port map (
    i=>MCLK1,
    o=>iclk);
  bg: BUFG port map (
    i=>iclk,
    o=>clk);
  SRAM_CONTROLLER0 : sram_controller port map (
    clk          => clk,
    MemRead      => memread,
    MemWrite     => memwrite,
    XWA          => XWA,
    MemReadData  => memreaddata,
    MemWriteData => memwritedata,
    ZD           => ZD);
  
  PC0 : pc_unit port map (
    clk      => clk,
    PCSource => pcsource,
    PCWrite  => pcwrite,
    PC       => pc,
    PCJump   => pcjump,
    PCJumpr  => pcjumpr,
    PCBranch => aluout);
  
  Controller0 : controller port map (
    clk         => clk,
    Opcode      => ir(31 downto 26),
    f_funct     => f_funct,
    Zero        => z,
    PCSource    => pcsource,
    PCWrite     => pcwrite,
    MemSource   => memsource,
    MemRead     => memread,
    MemWrite    => memwrite,
    IRWrite     => irwrite,
    MDRWrite    => mdrwrite,
    ALUOp       => aluop,
    ALUSourceA  => alusourceA,
    ALUSourceB  => alusourceB,
    ALUWriteA   => aluwriteA,
    ALUWriteB   => aluwriteB,
    ALUOutSource=> aluoutsource,
    ALUOutWrite => aluoutwrite,
    RegSource   => regsource,
    RegWrite    => regwrite,
    FRegWrite   => fregwrite,
    RegDest     => regdest,
    Send        => send,
    Recv        => recv,
    CanSend     => cansend,
    CanRecv     => canrecv);
  
  RegWindow : regs port map (
    clk        => clk,
    SourceReg1 => ir(25 downto 21),
    SourceReg2 => ir(20 downto 16),
    DestReg    => destreg,
    RegData1   => regdata1,
    RegData2   => regdata2,
    DestData   => destdata,
    RegWrite   => regwrite);

  FRegWindow : regs port map (
    clk        => clk,
    SourceReg1 => ir(25 downto 21),
    SourceReg2 => ir(20 downto 16),
    DestReg    => destreg,
    RegData1   => fregdata1,
    RegData2   => fregdata2,
    DestData   => destdata,
    RegWrite   => fregwrite);

  ALU0 : alu port map (
    A    => a,
    B    => b,
    code => code,
    O    => o,
    Z    => z);

  FPU0 : fpu port map (
    A    => a,
    B    => b,
    funct=> f_funct,
    O    => fo,
    Z    => fz);

  ALUController0 : alu_controller port map (
    ALUOp => aluop,
    funct => funct,
    code  => code);
  
  rs232c0 : rs232c port map (
    clk    => clk,
    S_Byte => s_byte,
    R_Byte => rs_r_byte,
    Send   => send,
    Recv   => rs_recv,
    CanSend=> cansend,
    CanRecv=> rs_canrecv,
    Tx     => RS_TX,
    Rx     => RS_RX);
  Iqueue0 : Iqueue port map (
    clk        => clk,
    R_Byte     => r_byte,
    Recv       => recv,
    CanRecv    => canrecv,
    rs_R_Byte  => rs_r_byte,
    rs_Recv    => rs_recv,
    rs_CanRecv => rs_canrecv);
--  Rom0 : Rom port map (
--    Addr  => memaddr,
--    Rdata => romdata,
--    R     => '1');

  ZA       <= memaddr;
  XE1      <= '0';
  E2A      <= '1';
  XE3      <= '0';
  XZBE     <= "0000";
  XGA      <= '0';
  XZCKE    <= '0';
  ADVA     <= '0';
  XFT      <= '1';
  XLBO     <= '1';
  ZZA      <= '0';
  ZCLKMA(0)<= clk;
  ZCLKMA(1)<= clk;  
  
  pcjump   <= ir(25 downto 0);
  pcjumpr  <= regdata1;
  memaddr  <= pc(19 downto 0) when memsource = '0' else
              aluout(19 downto 0);
  memwritedata <= b;
  destreg <= ir(20 downto 16) when regdest = "00" else
             ir(15 downto 11) when regdest = "01" else
             "11111" when regdest = "10" else
             ir(25 downto 21);
  destdata <= aluout when regsource = "00" else
              mdr    when regsource = "01" else
              pc+1   when regsource = "10" else
              x"000000"&r_byte;
  s_byte <= a(7 downto 0);
  funct <= R_SHL when ir(31 downto 26) = SHLI else
           R_SHR when ir(31 downto 26) = SHRI else
           ir(5 downto 0);
  f_funct <= "111111" when ir(31 downto 26) = F_SQRT else
             ir(5 downto 0);
    
  P: process(clk)
  begin
    if rising_edge(clk) then
      if irwrite = '1' then
        ir <= memreaddata;
        --ir <=romdata;
      end if;
      if mdrwrite = '1' then
        mdr <= memreaddata;
      end if;
      if aluwriteA = '1' then
        case alusourceA is
          when "10" =>
            a <= pc;
          when "01" =>
            a <= fregdata1;
          when others =>
            a <= regdata1;
        end case;
      end if;
      if aluwriteB = '1' then
        case alusourceB is
          when "10" =>
            if ir(15) = '1' then
              b <= ir(15)&x"FFFF"&ir(14 downto 0);             
            else
              b <= ir(15)&x"0000"&ir(14 downto 0);
            end if;
          when "01" =>
            b <= fregdata2;
          when others =>
            b <= regdata2;
        end case;
      end if;
      if aluoutwrite = '1' then
        case aluoutsource is
          when '1' =>
            aluout <= fo;
          when others =>
            aluout <= o;
        end case;
      end if;     
    end if;
  end process;
  
end example;

configuration top_config of top is
  for example
    for SRAM_CONTROLLER0 : sram_controller
      use entity work.sram_controller(archi);
    end for;
    for PC0 : pc_unit
      use entity work.pc_unit(archi);
    end for; 
    for Controller0 : controller
      use entity work.controller(archi);
    end for;
    for RegWindow : regs
      use entity work.regs(archi);
    end for;
    for FRegWindow : regs
      use entity work.regs(archi);
    end for;
    for ALU0 : alu
      use entity work.alu(archi);
    end for;
    for ALUController0 : alu_controller
      use entity work.alu_controller(archi);
    end for;
    for FPU0 : fpu
      use entity work.fpu(archi);
    end for;
    for rs232c0 : rs232c
      use entity work.rs232c(archi);
    end for;
    for Iqueue0 : Iqueue
      use entity work.Iqueue(archi);
    end for;
  end for;
end top_config;
