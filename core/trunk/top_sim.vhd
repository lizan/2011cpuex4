-------------------------------------------------------------------------------
-- UGL0 top
-- LastUpdate: 2011/10/14 14:13 
-- Author    : ultraredrays
-- Email     : ultraredrays@gmail.com
-- Todo      : ramの論理合成が終わらない
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.instruction_pkg.all;

entity top is
end top;

architecture example of top is
  component sram_controller
    port (
      clk          : in std_logic;
      MemRead      : in std_logic;
      MemWrite     : in std_logic;
      XWA          : out std_logic;
      MemReadData  : out std_logic_vector(31 downto 0);
      MemWriteData : in std_logic_vector(31 downto 0);
      ZD           : inout std_logic_vector(31 downto 0));
  end component;

  component sram_sim
  port (
    clk    : in    std_logic;
    Enable : in    std_logic;
    ZD     : inout std_logic_vector(31 downto 0);
    ZA     : in    std_logic_vector(19 downto 0);
    XWA    : in    std_logic);             -- 0で書き込み可能
  end component;

  component alu
    port (
      A     : in  std_logic_vector(31 downto 0);  -- input data
      B     : in  std_logic_vector(31 downto 0);  -- input data
      code  : in  std_logic_vector( 3 downto 0);  -- alu制御入力
      O     : out std_logic_vector(31 downto 0);  -- output data
      Z     : out std_logic);
  end component;

  component alu_controller port (
    ALUOp : in   std_logic_vector(1 downto 0);  -- ALUOp信号 from controller
    funct : in   std_logic_vector(5 downto 0);  -- funct from OperationMemory
    code  : out  std_logic_vector(3 downto 0));  -- code for ALU
  end component;

  component controller
    port (
      clk         : in  std_logic;
      Opcode      : in  std_logic_vector(5 downto 0);
      f_funct     : in  std_logic_vector(5 downto 0);
      Zero        : in  std_logic;                     -- ALUの出力が0であるか
      PCSource    : out std_logic_vector(1 downto 0);  --次のPCにどのアドレスを使用するか 00:PC+1,01:IR[25-0],10:Reg(IR[25-21]),11:ALUOut       
      PCWrite     : out std_logic;                     -- PC書き込み制御信号
      MemSource   : out std_logic;                     -- メモリ参照アドレス元制御信号 0:PC,1:ALU
      MemRead     : out std_logic;                     -- メモリ読み出し制御信号
      MemWrite    : out std_logic;                     -- メモリ書き出し制御信号
      IRWrite     : out std_logic;                     -- 命令レジスタ書き込み制御信号
      MDRWrite    : out std_logic;                     -- MDR書き込み制御信号 
      ALUOp       : out std_logic_vector(1 downto 0);  --ALU制御信号 00:ADD,01:SUB,10:RTYPE
      ALUSourceA  : out std_logic_vector(1 downto 0);  --Aレジスタ書き込み元制御信号 0:IR[25-21],1:PC
      ALUSourceB  : out std_logic_vector(1 downto 0);  --Bレジスタ書き込み元制御信号 0:IR[20-15],1:offset
      ALUWriteA   : out std_logic;                     --Aレジスタ書き込み制御信号
      ALUWriteB   : out std_logic;                     --Bレジスタ書き込み制御信号
      ALUOutSource: out std_logic;
      ALUOutWrite : out std_logic;                     --ALU出力レジスタ書き込み信号
      RegSource   : out std_logic_vector(1 downto 0);  --レジスタ書き込み元制御信号 00:ALUOut,01:MDR,10:PC+1
      RegWrite    : out std_logic;                     --レジスタ書き込み制御信号
      FRegWrite   : out std_logic;                     --Fレジスタ書き込み制御信号
      RegDest     : out std_logic_vector(1 downto 0);
      Send        : out std_logic;
      Recv        : out std_logic;
      CanSend     : in  std_logic;
      CanRecv     : in  std_logic); --レジスタ書き込み先制御信号 00:IR[20-16],01:IR[15-11],10:(31)
  end component;

  component pc_unit
    port (
      clk       : in  std_logic;
      PCSource  : in  std_logic_vector(1 downto 0);
      PCWrite   : in  std_logic;
      PC        : out std_logic_vector(31 downto 0);
      PCJump    : in  std_logic_vector(25 downto 0);
      PCJumpr   : in  std_logic_vector(31 downto 0);
      PCBranch  : in  std_logic_vector(31 downto 0));
  end component;

  component regs
    port (
      clk        : in  std_logic;                      -- クロック 
      SourceReg1 : in  std_logic_vector(4 downto 0);   -- 読み出し用レジスタその1
      SourceReg2 : in  std_logic_vector(4 downto 0);   -- 読み出し用レジスタその2
      DestReg    : in  std_logic_vector(4 downto 0);   -- 書き込み用レジスタ
      RegData1   : out std_logic_vector(31 downto 0);  -- 読み出しデータその1
      RegData2   : out std_logic_vector(31 downto 0);  -- 読み出しデータその2
      DestData   : in  std_logic_vector(31 downto 0);  -- 書き込みデータ
      RegWrite   : in  std_logic);                     -- レジスタ書き込み制御信号
  end component;

  component fpu is  
    port (
      A     : in  std_logic_vector(31 downto 0);  -- input data
      B     : in  std_logic_vector(31 downto 0);  -- input data
      funct : in  std_logic_vector( 5 downto 0);  -- alu制御入力
      O     : out std_logic_vector(31 downto 0);  -- output data
      Z     : out std_logic);
  end component;

  component Iqueue_sim
    port (
      clk       : in  std_logic;
      R_Byte    : out std_logic_vector(7 downto 0);
      Recv      : in  std_logic;
      CanRecv   : out std_logic);
  end component;


  signal clk, iclk  : std_logic := '1';
  signal pc         : std_logic_vector(31 downto 0) := x"00000000";
  signal memaddr    : std_logic_vector(19 downto 0) := x"00000";
  signal memreaddata : std_logic_vector(31 downto 0) := x"00000000";
  signal memwritedata : std_logic_vector(31 downto 0) := x"00000000";
  signal ir         : std_logic_vector(31 downto 0) := x"00000000";
  signal mdr        : std_logic_vector(31 downto 0) := x"00000000";
  signal pcjump     : std_logic_vector(25 downto 0) := "11111111111111111111111111";
  signal pcjumpr    : std_logic_vector(31 downto 0) := x"FFFFFFFF";
  signal regdata1   : std_logic_vector(31 downto 0) := x"00000000";
  signal regdata2   : std_logic_vector(31 downto 0) := x"00000000";
  signal fregdata1  : std_logic_vector(31 downto 0) := x"00000000";
  signal fregdata2  : std_logic_vector(31 downto 0) := x"00000000";
  signal destdata: std_logic_vector(31 downto 0) := x"00000000";
  signal destreg    : std_logic_vector(4 downto 0) := "00000";
  signal a          : std_logic_vector(31 downto 0) := x"00000000";
  signal b          : std_logic_vector(31 downto 0) := x"00000000";
  signal code       : std_logic_vector(3 downto 0) := "1111";
  signal o          : std_logic_vector(31 downto 0) := x"00000000";
  signal fo         : std_logic_vector(31 downto 0) := x"00000000";
  signal z          : std_logic := '1';
  signal fz         : std_logic := '1';
  signal aluout     : std_logic_vector(31 downto 0) := x"00000000";
  signal pcsource   : std_logic_vector(1 downto 0) := "00";
  signal pcwrite    : std_logic := '0';
  signal memsource  : std_logic := '0';
  signal memread    : std_logic := '0';
  signal memwrite   : std_logic := '0';
  signal irwrite    : std_logic := '0';
  signal mdrwrite   : std_logic := '0';
  signal aluop      : std_logic_vector(1 downto 0) := "11";
  signal alusourceA : std_logic_vector(1 downto 0) := "00";
  signal alusourceB : std_logic_vector(1 downto 0) := "00";
  signal aluwriteA  : std_logic := '0';
  signal aluwriteB  : std_logic := '0';
  signal aluoutsource: std_logic := '0';
  signal aluoutwrite: std_logic := '0';
  signal regsource  : std_logic_vector(1 downto 0) := "00";
  signal regdest    : std_logic_vector(1 downto 0) := "00";
  signal regwrite   : std_logic := '0';
  signal fregwrite  : std_logic := '0';
  signal r_byte     : std_logic_vector(7 downto 0);
  signal s_byte     : std_logic_vector(7 downto 0);
  signal recv       : std_logic := '0';
  signal send       : std_logic := '0';
  signal cansend    : std_logic := '1';
  signal canrecv    : std_logic := '1';
  signal xwa        : std_logic := '0';
  signal zd         : std_logic_vector(31 downto 0) := x"00000000";
  signal funct      : std_logic_vector(5 downto 0);
  signal f_funct    : std_logic_vector(5 downto 0);

begin
  
  SRAM_CONTROLLER0 : sram_controller port map (
    clk          => clk,
    MemRead      => memread,
    MemWrite     => memwrite,
    XWA          => xwa,
    MemReadData  => memreaddata,
    MemWriteData => memwritedata,
    ZD           => zd);
  
  SRAM0 : sram_sim port map (
    clk    => clk,
    Enable => '1',
    ZD     => zd,
    ZA     => memaddr,
    XWA    => xwa);

  PC0 : pc_unit port map (
    clk      => clk,
    PCSource => pcsource,
    PCWrite  => pcwrite,
    PC       => pc,
    PCJump   => pcjump,
    PCJumpr  => pcjumpr,
    PCBranch => aluout);
  
  Controller0 : controller port map (
    clk         => clk,
    Opcode      => ir(31 downto 26),
    f_funct     => f_funct,
    Zero        => z,
    PCSource    => pcsource,
    PCWrite     => pcwrite,
    MemSource   => memsource,
    MemRead     => memread,
    MemWrite    => memwrite,
    IRWrite     => irwrite,
    MDRWrite    => mdrwrite,
    ALUOp       => aluop,
    ALUSourceA  => alusourceA,
    ALUSourceB  => alusourceB,
    ALUWriteA   => aluwriteA,
    ALUWriteB   => aluwriteB,
    ALUOutSource=> aluoutsource,
    ALUOutWrite => aluoutwrite,
    RegSource   => regsource,
    RegWrite    => regwrite,
    FRegWrite   => fregwrite,
    RegDest     => regdest,
    Send        => send,
    Recv        => recv,
    CanSend     => cansend,
    CanRecv     => canrecv);
  
  RegWindow : regs port map (
    clk        => clk,
    SourceReg1 => ir(25 downto 21),
    SourceReg2 => ir(20 downto 16),
    DestReg    => destreg,
    RegData1   => regdata1,
    RegData2   => regdata2,
    DestData   => destdata,
    RegWrite   => regwrite);

  FRegWindow : regs port map (
    clk        => clk,
    SourceReg1 => ir(25 downto 21),
    SourceReg2 => ir(20 downto 16),
    DestReg    => destreg,
    RegData1   => fregdata1,
    RegData2   => fregdata2,
    DestData   => destdata,
    RegWrite   => fregwrite);

  ALU0 : alu port map (
    A    => a,
    B    => b,
    code => code,
    O    => o,
    Z    => z);

  FPU0 : fpu port map (
    A    => a,
    B    => b,
    funct=> f_funct,
    O    => fo,
    Z    => fz);

  ALUController0 : alu_controller port map (
    ALUOp => aluop,
    funct => funct,
    code  => code);

  Iqueue_sim0 : Iqueue_sim port map (
    clk     => clk,
    R_byte  => r_byte,
    Recv    => recv,
    CanRecv => canrecv);
  
  pcjump   <= ir(25 downto 0);
  pcjumpr  <= regdata1;
  memaddr <= pc(19 downto 0) when memsource = '0' else
             aluout(19 downto 0);
  memwritedata <= b;
  destreg <= ir(20 downto 16) when regdest = "00" else
             ir(15 downto 11) when regdest = "01" else
             "11111" when regdest = "10" else
             ir(25 downto 21);
  destdata <= aluout when regsource = "00" else
              mdr    when regsource = "01" else
              pc+1   when regsource = "10" else
              x"000000"&r_byte;
  s_byte <= a(7 downto 0);
  funct <= R_SHL when ir(31 downto 26) = SHLI else
           R_SHR when ir(31 downto 26) = SHRI else
           ir(5 downto 0);
  f_funct <= "111111" when ir(31 downto 26) = F_SQRT else
             ir(5 downto 0);
  
    
  Renew_clock: process
  begin
    if clk = '0' then
      clk <= '1';
    else
      clk <= '0';
    end if;
    wait for 1 ns;
  end process;

  P:process(clk)
  begin 
    if rising_edge(clk) then
      if irwrite = '1' then
        ir <= memreaddata;
      end if;
      if mdrwrite = '1' then
        mdr <= memreaddata;
      end if;
      if aluwriteA = '1' then
        case alusourceA is
          when "10" =>
            a <= pc;
          when "01" =>
            a <= fregdata1;
          when others =>
            a <= regdata1;
        end case;
      end if;
      if aluwriteB = '1' then
        case alusourceB is
          when "10" =>
            if ir(15) = '1' then
              b <= ir(15)&x"FFFF"&ir(14 downto 0);             
            else
              b <= ir(15)&x"0000"&ir(14 downto 0);
            end if;
          when "01" =>
            b <= fregdata2;
          when others =>
            b <= regdata2;
        end case;
      end if;
      if aluoutwrite = '1' then
        case aluoutsource is
          when '1' =>
            aluout <= fo;
          when others =>
            aluout <= o;
        end case;
      end if;
    end if;
  end process;
  
end example;

configuration config of top is
  for example
    for SRAM0 : sram_sim
      use entity work.sram_sim(archi);
    end for;
    for SRAM_CONTROLLER0 : sram_controller
      use entity work.sram_controller(archi);
    end for;
    for PC0 : pc_unit
      use entity work.pc_unit(archi);
    end for; 
    for Controller0 : controller
      use entity work.controller(archi);
    end for;
    for RegWindow : regs
      use entity work.regs(archi);
    end for;
    for FRegWindow : regs
      use entity work.regs(archi);
    end for;
    for ALU0 : alu
      use entity work.alu(archi);
    end for;
    for ALUController0 : alu_controller
      use entity work.alu_controller(archi);
    end for;
    for FPU0 : fpu
      use entity work.fpu(archi);
    end for;
    for Iqueue_sim0 : Iqueue_sim
      use entity work.Iqueue_sim(archi);
    end for;
  end for;
end config;
