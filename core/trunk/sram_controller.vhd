-------------------------------------------------------------------------------
-- controller データパス制御器
-- LastUpdate: 2012/1/17 6:05 
-- Author    : ultraredrays
-- Email     : ultraredrays@gmail.com
-- Todo      : 動作確認
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity sram_controller is
  
  port (
    clk          : in std_logic;
    MemRead      : in std_logic;
    MemWrite     : in std_logic;
    XWA          : out std_logic;
    MemReadData  : out std_logic_vector(31 downto 0);
    MemWriteData : in std_logic_vector(31 downto 0);
    ZD           : inout std_logic_vector(31 downto 0));
    
end sram_controller;

architecture archi of sram_controller is
  --modeを記憶する 0はWriteの処理, 1はReadの処理を行う
  signal read_buf : std_logic_vector(1 downto 0) := "00";
  signal write_buf : std_logic_vector(1 downto 0) := "00";
  
begin  -- archi

  XWA <= '0' when MemWrite = '1' else
         '1';
  MemReadData <= ZD when read_buf(1) = '1' and write_buf(1) = '0' else
                 (others => '0');
  
  ZD <= MemWriteData when write_buf(1) = '1' else
        (others => 'Z');
  
  P:process(clk)
  begin
    if rising_edge(clk) then
      read_buf(1) <= read_buf(0);
      write_buf(1) <= write_buf(0);
      read_buf(0) <= MemRead;
      write_buf(0) <= MemWrite;
    end if;
  end process;
end archi;
