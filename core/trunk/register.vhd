-------------------------------------------------------------------------------
-- register
-- LastUpdate: 2011/10/12 02:04 
-- Author    : ultraredrays
-- Email     : ultraredrays@gmail.com
-- Todo      : 動作確認
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;
use work.instruction_pkg.all;

entity regs is
  
  port (
    clk        : in  std_logic;                      -- クロック 
    SourceReg1 : in  std_logic_vector(4 downto 0) := "00000";   -- 読み出し用レジスタその1
    SourceReg2 : in  std_logic_vector(4 downto 0) := "00000";   -- 読み出し用レジスタその2
    DestReg    : in  std_logic_vector(4 downto 0) := "00000";   -- 書き込み用レジスタ
    RegData1: out std_logic_vector(31 downto 0):= x"00000000";  -- 読み出しデータその1
    RegData2: out std_logic_vector(31 downto 0):= x"00000000";  -- 読み出しデータその2
    DestData   : in  std_logic_vector(31 downto 0):= x"00000000";  -- 書き込みデータ
    RegWrite   : in  std_logic := '0');                     -- レジスタ書き込み制御信号

end regs;

architecture archi of regs is
  type reg_window is array(0 to 31) of std_logic_vector(31 downto 0);
  signal Reg : reg_window := (x"00000000",x"00000000",x"00000000",x"00000000",x"00000000",x"00000000",x"00000000",x"00000000",
                              x"00000000",x"00000000",x"00000000",x"00000000",x"00000000",x"00000000",x"00000000",x"00000000",
                              x"00000000",x"00000000",x"00000000",x"00000000",x"00000000",x"00000000",x"00000000",x"00000000",
                              x"00000000",x"00000000",x"00000000",x"00000000",x"00000000",x"00000000",x"00000000",x"00000000");
  
begin  -- archi

  RegData1 <= Reg(conv_integer(SourceReg1));
  RegData2 <= Reg(conv_integer(SourceReg2));

  write_register : process(clk)
  begin 
    if rising_edge(clk) then
      if RegWrite = '1' then
        Reg(conv_integer(DestReg)) <= DestData;
      end if;
    end if;
  end process;

end archi;
