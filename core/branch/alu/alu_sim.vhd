-------------------------------------------------------------------------------
-- alu_simulator
-- LastUpdate: 2011/10/12 02:04 
-- Author    : ultraredrays
-- Email     : ultraredrays@gmail.com
-- Todo      : 何もかも
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
 
entity alu_sim is
  
end alu_sim;

architecture archi of alu_sim is
  component ALU
    port (
      A    : in  std_logic_vector(31 downto 0);
      B    : in  std_logic_vector(31 downto 0);
      code : in  std_logic_vector( 3 downto 0);
      O    : out std_logic_vector(31 downto 0);
      Z    : out std_logic);
  end component;

  signal CLK   : std_logic := '0';
  signal A     : std_logic_vector(31 downto 0) := x"00000000";
  signal B     : std_logic_vector(31 downto 0) := x"00000000";
  signal code  : std_logic_vector( 3 downto 0) := "0000";
  signal O     : std_logic_vector(31 downto 0) := x"00000000";
  signal Z     : std_logic := '0';

begin  -- archi

  ALU0 : alu port map (
    A => A,
    B => B,
    code => code,
    O  => O,
    Z  => Z);
  
  P:process
    variable V : integer := 0;
  begin
    if(CLK = '0')then
      CLK <= '1';
    else
      CLK <= '0';
      V := V + 1;
    end if;

    A <= x"01010101";
    B <= x"10101010";
    code <= conv_std_logic_vector((V mod 16), 4);
    
    wait for 1 ns;
    
  end process;

end archi;

configuration config of alu_sim is
  for archi
    for ALU0 : ALU
      use entity work.alu(archi);
    end for;
  end for;
end config;
