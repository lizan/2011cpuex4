-------------------------------------------------------------------------------
-- IO.vhd
-- LastUpdate: 2011/11/12 01:48 
-- Author    : ultraredrays
-- Email     : ultraredrays@gmail.com
-- Todo      : 動作確認
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.std_logic_signed.all;

entity io is
  port (
    -- clk : アーキテクチャ全体のクロック
    clk      : in  std_logic;
    -- Rx  : rs232cの受信信号線
    Rx       : in  std_logic;
    -- Tx  : rs232cの送信信号線
    Tx       : out  std_logic;
    -- R_Data : 受信した1Wordのデータ  
    R_Data   : out std_logic_vector(31 downto 0);       
    -- S_Data : 送信する1Wordのデータ  
    S_Data   : in  std_logic_vector(31 downto 0);
    -- Recv  : 1Word受信データ使用可能FLAG init 0, sending 1, other 0
    Recv    : out std_logic;
    -- Send  : 1Word送信データ使用可能FLAG move 1, other 0
    Send    : in  std_logic;
    -- S_Busy : io.vhdにおける送信ステートマシン動作中FLAG
    S_busy   : out  std_logic);
end io;

architecture archi of io is
  signal s_byte   : std_logic_vector(7 downto 0) := "11111111";
  signal r_byte   : std_logic_vector(7 downto 0) := "11111111";
  signal sendB     : std_logic := '0';
  signal recvB     : std_logic := '0';
  signal sb_busy   : std_logic := '0';

  component rs232c
    generic (
      wtime : std_logic_vector(15 downto 0) := x"1ADB");
    port (
      clk       : in  std_logic;                     --アーキテクチャ全体のクロック
      S_Byte    : in  std_logic_vector(7 downto 0);  --送信する1Byteのデータ 
      R_Byte    : out std_logic_vector(7 downto 0);  --受信する1Byteのデータ
      Send      : in  std_logic;                     --送信データ使用可能      
      Recv      : out std_logic;                     --受信データ使用可能
      S_busy    : out std_logic;                     --データ送信中
      Tx        : out std_logic;                     --送信用信号線
      Rx        : in  std_logic);                    --受信用信号線
  end component;

  component io_converter is
  port (
    -- clk : アーキテクチャ全体のクロック
    clk      : in  std_logic;
    -- R_Data : 受信した1Wordのデータ  
    R_Data   : out std_logic_vector(31 downto 0);       
    -- S_Data : 送信する1Wordのデータ  
    S_Data   : in  std_logic_vector(31 downto 0);
    -- R_Byte : 受信した1Byteのデータ
    R_Byte   : in  std_logic_vector(7 downto 0);
    -- S_Byte : 送信する1Wordのデータ
    S_Byte   : out std_logic_vector(7 downto 0);
    -- RecvB  : 1Byte受信データ使用可能FLAG init 0, sending 1, other 0
    RecvB    : in  std_logic;
    -- RecvD  : 1Word受信データ使用可能FLAG init 0, sending 1, other 0
    RecvD    : out std_logic;
    -- SendB  : 1Byte送信データ使用可能FLAG move 1, other 0
    SendB    : out std_logic;
    -- SendD  : 1Word送信データ使用可能FLAG move 1, other 0
    SendD    : in  std_logic;
    -- SB_Busy : rs232c.vhdにおける送信ステートマシン動作中FLAG
    SB_busy   : in  std_logic;
    -- SD_Busy : io.vhdにおける送信ステートマシン動作中FLAG
    SD_busy   : out  std_logic);
  end component;


begin
  rs232c0: rs232c port map (
    Clk => clk,
    S_byte => s_byte,
    R_byte => r_byte,
    Send => sendB,
    Recv => recvB,
    S_busy => sb_busy,
    Rx => Rx,
    Tx => Tx);
  io_converter0 : io_converter port map (
    clk     => clk,
    R_Data  => R_Data,
    S_Data  => S_Data,
    R_Byte  => r_byte,
    S_Byte  => s_byte,
    RecvB   => recvB,
    RecvD   => Recv,
    SendB   => sendB,
    SendD   => Send,
    SB_busy => sb_busy,
    SD_busy => S_busy);
  
end archi;

configuration config of io is

  for archi
    for rs232c0 : rs232c
      use entity work.rs232c(archi);
    end for;
    for IO_Converter0 : IO_Converter
      use entity work.IO_Converter(archi);
    end for;
  end for;

end config;
