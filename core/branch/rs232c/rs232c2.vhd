-------------------------------------------------------------------------------
-- rs232c
-- LastUpdate: 2011/11/08 02:04 
-- Author    : ultraredrays
-- Email     : ultraredrays@gmail.com
-- Todo      : loopbackしかできない
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity rs232c is
  generic (
    constant wtime : std_logic_vector(15 downto 0) := x"1ADB");
  port (
    Clk       : in  std_logic;                     --アーキテクチャ全体のクロック
    S_byte    : in  std_logic_vector(7 downto 0);  --送信する1Byteのデータ 
    R_byte    : out std_logic_vector(7 downto 0);  --受信した1Byteのデータ
    Send      : in  std_logic;                     --送信flag      
    Recv      : out std_logic;                     --受信flag
    S_busy    : out std_logic;                     --データ送信中
    Tx        : out std_logic;                     --送信用信号線
    Rx        : in  std_logic);                    --受信用信号線
end rs232c;

architecture archi of rs232c is
  signal rx_buf : std_logic_vector(1 downto 0) := "00";        -- 受信データ
  signal s_timer : std_logic_vector(15 downto 0) := x"0000";   -- 送信用カウンタ
  signal r_timer : std_logic_vector(15 downto 0) := x"0000";   -- 受信用カウンタ
  signal s_state : std_logic_vector(3 downto 0) := "1011";     -- 送信用状態変数
  signal r_state : std_logic_vector(3 downto 0) := "1011";     -- 受信用状態変数
  signal s_buf : std_logic_vector(8 downto 0) := "000000001";  -- 送信用バッファ
  signal r_buf : std_logic_vector(8 downto 0) := "100000000";  -- 受信用バッファ

begin  -- archi
  Tx<=s_buf(0);
  S_busy<= '0' when s_state="1011" else '1';

  S: process(clk)
  begin
    if rising_edge(clk) then
      case s_state is
        when "1011"=>
          if Send ='1' then
            s_buf <= S_byte&"0";
            s_state <= s_state - 1;
            s_timer <= wtime;
          end if;
        when others=>
          if s_timer = x"0000" then
            s_buf <= "1"&s_buf(8 downto 1);
            s_timer<=wtime;
            s_state<=s_state-1;
          else
            s_timer<=s_timer-1;
          end if;
      end case;
    end if;
  end process;
      
  R: process(Clk)
  begin
  end process;

end archi;
