-------------------------------------------------------------------------------
-- top
-- LastUpdate: 2011/10/07 13:04 
-- Author    : ultraredrays
-- Email     : ultraredrays@gmail.com
-- Todo      : 何もかも
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity top is
  Port ( MCLK1 : in  STD_LOGIC;
         RS_TX : out  STD_LOGIC;
         RS_RX : in  STD_LOGIC );
end top;

architecture example of top is
  signal clk, iclk: std_logic;
  signal s_byte   : std_logic_vector(7 downto 0) := "11111111";
  signal r_byte   : std_logic_vector(7 downto 0) := "11111111";
  signal s_data   : std_logic_vector(31 downto 0) := x"FFFFFFFF";
  signal r_data   : std_logic_vector(31 downto 0) := x"FFFFFFFF";
  signal data     : std_logic_vector(31 downto 0) := x"FFFFFFFF";
  signal states   : std_logic_vector(1 downto 0) := "10";
  signal sendB     : std_logic := '0';
  signal recvB     : std_logic := '0';
  signal sendD     : std_logic := '0';
  signal recvD     : std_logic := '0';
  signal sb_busy   : std_logic := '0';
  signal sd_busy   : std_logic := '0';

  component rs232c
    generic (
      wtime : std_logic_vector(15 downto 0) := x"1ADB");
    port (
      clk       : in  std_logic;                     --アーキテクチャ全体のクロック
      S_Byte    : in  std_logic_vector(7 downto 0);  --送信する1Byteのデータ 
      R_Byte    : out std_logic_vector(7 downto 0);  --受信する1Byteのデータ
      Send      : in  std_logic;                     --送信データ使用可能      
      Recv      : out std_logic;                     --受信データ使用可能
      S_busy    : out std_logic;                     --データ送信中
      Tx        : out std_logic;                     --送信用信号線
      Rx        : in  std_logic);                    --受信用信号線
  end component;

  component io_converter is
  port (
    -- clk : アーキテクチャ全体のクロック
    clk      : in  std_logic;
    -- R_Data : 受信した1Wordのデータ  
    R_Data   : out std_logic_vector(31 downto 0);       
    -- S_Data : 送信する1Wordのデータ  
    S_Data   : in  std_logic_vector(31 downto 0);
    -- R_Byte : 受信した1Byteのデータ
    R_Byte   : in  std_logic_vector(7 downto 0);
    -- S_Byte : 送信する1Wordのデータ
    S_Byte   : out std_logic_vector(7 downto 0);
    -- RecvB  : 1Byte受信データ使用可能FLAG init 0, sending 1, other 0
    RecvB    : in  std_logic;
    -- RecvD  : 1Word受信データ使用可能FLAG init 0, sending 1, other 0
    RecvD    : out std_logic;
    -- SendB  : 1Byte送信データ使用可能FLAG move 1, other 0
    SendB    : out std_logic;
    -- SendD  : 1Word送信データ使用可能FLAG move 1, other 0
    SendD    : in  std_logic;
    -- SB_Busy : rs232c.vhdにおける送信ステートマシン動作中FLAG
    SB_busy   : in  std_logic;
    -- SD_Busy : io.vhdにおける送信ステートマシン動作中FLAG
    SD_busy   : out  std_logic);
  end component;


begin
  ib: IBUFG port map (
    i=>MCLK1,
    o=>iclk);
  bg: BUFG port map (
    i=>iclk,
    o=>clk);
  rs232c0: rs232c port map (
    Clk => clk,
    S_byte => s_byte,
    R_byte => r_byte,
    Send => sendB,
    Recv => recvB,
    S_busy => sb_busy,
    Rx => RS_RX,
    Tx => RS_TX);
  io_converter0 : io_converter port map (
    clk     => clk,
    R_Data  => r_data,
    S_Data  => s_data,
    R_Byte  => r_byte,
    S_Byte  => s_byte,
    RecvB   => recvB,
    RecvD   => recvD,
    SendB   => sendB,
    SendD   => sendD,
    SB_busy => sb_busy,
    SD_busy => sd_busy);
  
  loopback_msg: process(clk)
  begin
    if rising_edge(clk) then
      if recvD = '1' then
        data <= r_data;
        if states = "00" then
          if sd_busy = '0' then
            states <= "01";
          end if;
        else
          states <= "10";
        end if;
      else
        states <= "00";          
      end if;
    end if;
  end process;

  sendD <= '1' when states = "01" else
          '0';
  s_data <= data;  
  
end example;

