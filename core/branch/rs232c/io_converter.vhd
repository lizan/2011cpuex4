-------------------------------------------------------------------------------
-- IO_Converter.vhd
-- LastUpdate: 2011/11/10 22:22 
-- Author    : ultraredrays
-- Email     : ultraredrays@gmail.com
-- Todo      : 動作確認
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.std_logic_signed.all;

entity io_converter is
  port (
    -- clk : アーキテクチャ全体のクロック
    clk      : in  std_logic;
    -- R_Data : 受信した1Wordのデータ  
    R_Data   : out std_logic_vector(31 downto 0);       
    -- S_Data : 送信する1Wordのデータ  
    S_Data   : in  std_logic_vector(31 downto 0);
    -- R_Byte : 受信した1Byteのデータ
    R_Byte   : in  std_logic_vector(7 downto 0);
    -- S_Byte : 送信する1Wordのデータ
    S_Byte   : out std_logic_vector(7 downto 0);
    -- RecvB  : 1Byte受信データ使用可能FLAG init 0, sending 1, other 0
    RecvB    : in  std_logic;
    -- RecvD  : 1Word受信データ使用可能FLAG init 0, sending 1, other 0
    RecvD    : out std_logic;
    -- SendB  : 1Byte送信データ使用可能FLAG move 1, other 0
    SendB    : out std_logic;
    -- SendD  : 1Word送信データ使用可能FLAG move 1, other 0
    SendD    : in  std_logic;
    -- SB_Busy : rs232c.vhdにおける送信ステートマシン動作中FLAG
    SB_busy   : in  std_logic;
    -- SD_Busy : io.vhdにおける送信ステートマシン動作中FLAG
    SD_busy   : out  std_logic);
end io_converter;

architecture archi of io_converter is
  signal r_databuf : std_logic_vector(31 downto 0) := x"00000000";
  signal s_databuf : std_logic_vector(31 downto 0) := x"00000000";
  signal r_state : std_logic_vector(2 downto 0) := "111";  
  signal s_state : std_logic_vector(2 downto 0) := "111";

begin  -- archi

  SD_busy <= '0' when s_state = "111" else
             '1';
  RecvD <= '1' when r_state = "000" else
           '0';
  
  S: process(clk)
  begin
    if rising_edge(clk) then
      case s_state is
        when "111" =>
          if SendD  = '1' then
            s_databuf <= S_Data;
            S_Byte <= S_Data(31 downto 24);
            if SB_Busy = '0' then
              SendB <= '1';
              s_state <= s_state - 1;
            else
              SendB <= '0';
            end if;
          end if;
        when others =>
          case s_state(2 downto 1) is
            when "11" =>
              S_Byte <= s_databuf(31 downto 24);
            when "10" =>
              S_Byte <= s_databuf(23 downto 16);
            when "01" =>
              S_Byte <= s_databuf(15 downto 8);
            when others =>
              S_Byte <= s_databuf(7 downto 0);
          end case;
          SendB <= s_state(0);
          if SB_Busy = '0' then
            s_state <= s_state - 1;
          end if;
      end case;
    end if;
  end process;

  R: process(clk)
  begin
    if rising_edge(clk) then
      if r_state(0) = RecvB then
        r_state <= r_state - 1;
      end if;
      case r_state is
        when "111" =>
          r_databuf(31 downto 24) <= R_Byte;
        when "101" =>
          r_databuf(23 downto 16) <= R_Byte;
        when "011" =>
          r_databuf(15 downto 8) <= R_Byte;
        when "001" =>
          r_databuf(7 downto 0) <= R_Byte;
          R_Data <= r_databuf(31 downto 8)&R_Byte;
        when others =>
          null;
      end case;
    end if;
  end process;

end archi;
