library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity top is
  Port ( MCLK1 : in  STD_LOGIC;
         RS_TX : out  STD_LOGIC;
--         RS_RX : in STD_LOGIC;
         ZD     : inout std_logic_vector(31 downto 0);  -- SRAMとやりとりするデータ
         ZA     : out    std_logic_vector(19 downto 0);  -- SRAMに読み書きするAddress
         XE1    : out    std_logic;     -- チップのenable信号、0でactiveとなる 電力消費を気にしないのであれば0固定
         E2A    : out    std_logic;     -- チップのenable信号、1でactiveとなる 電力消費を気にしないのであれば1固定
         XE3    : out    std_logic;     -- チップのenable信号、0でactiveとなる 電力消費を気にしないのであれば0固定
         XZBE   : out    std_logic_vector(3 downto 0);  --各Byteの書き込みマスク、全部0で固定
         XGA    : out    std_logic;     -- 出力のenable信号、0でactiveとなる 小休止とかしないのであれば0固定
         XWA    : out    std_logic;     -- 書き込みのenable信号、0でactive(書き込み可能)となる。この信号が来たときのアドレスに、2clock後のデータが書き込まれる
         XZCKE  : out    std_logic;     --clockのenable信号、さすがに0固定で良いと思う 
         ZCLKMA : out    std_logic_vector(1 downto 0);  --clock、遠いので2bitあるが、どちらにも同じ物を送れば良い
         ADVA   : out    std_logic;     --Burstアクセス(アドレスを連続して使用する)モード用に使う 今は0で良い  
         XFT    : out    std_logic;     --Pipelineモードか、FlowThroughモードかを切り替える 1固定
         XLBO   : out    std_logic;     --バーストアクセス用のアドレス順を指定する、active lowとあるが、とりあえず1で固定
         ZZA    : out    std_logic);    --1で省電力モード、とりあえず0で固定
end top;

architecture example of top is
  signal clk,iclk: std_logic;
  signal clear : std_logic := '0';
  signal out_port : std_logic_vector(7 downto 0) := "00000000";
  signal data : std_logic_vector(31 downto 0);
  signal readdata : std_logic_vector(31 downto 0);
  signal writedata : std_logic_vector(31 downto 0);
  signal address : std_logic_vector(19 downto 0) := "00000000000000000000";
  signal counter : std_logic_vector(21 downto 0) := "0000000000000000000000";
  signal uart_go: std_logic;
  signal uart_busy: std_logic := '0';
  signal err : std_logic_vector(20 downto 0) := "000000000000000000000";

  component u232c
    generic (wtime: std_logic_vector(15 downto 0) := x"1ADB");
    Port ( clk  : in  STD_LOGIC;
           data : in  STD_LOGIC_VECTOR (7 downto 0);
           go   : in  STD_LOGIC;
           busy : out STD_LOGIC;
           tx   : out STD_LOGIC);
  end component;

begin
  ib: IBUFG port map (
    i=>MCLK1,
    o=>iclk);
  bg: BUFG port map (
    i=>iclk,
    o=>clk);
  
  rs232c: u232c generic map (wtime=>x"1ADB")
    port map (
      clk=>clk,
      data=>out_port,
      go=>uart_go,
      busy=>uart_busy,
      tx=>rs_tx);
  
  -- MODE CONTROL --
  ZD <= data;
  ZA <= address;
  XE1 <= '0';
  E2A <= '1';
  XE3 <= '0';
  XZBE <= "0000";
  XGA  <= '0';
  XZCKE <= '0';
  ZCLKMA(0) <= clk;
  ZCLKMA(1) <= clk;
  ADVA <= '0';
  XFT  <= '1';
  XLBO <= '1';
  ZZA  <= '0';
  
  XWA <= '1' when counter(21) = '1' or counter(20) = '1' else
         '0';
  address <= counter(19 downto 0);
  writedata <= (x"000"&counter(19 downto 0))-2;
  

  P: process(clk)
  begin
    if rising_edge(clk) then
      -- renew data
      if counter > "0100000000000000000001" then
        readdata <= data;
        if not readdata = writedata then
          err <= err + 1;
        end if;
      elsif counter > "0000000000000000000001" then
        data <= writedata;
      end if;
      -- add counter & send message
      if counter = "1000000000000000000010" then
        if err = 0 then
          out_port <= "00000000";
        else
          out_port <= "11111111";  
        end if;
        uart_go <= "1";
      else
        counter <= counter + 1;
      end if;
    end if;
  end process;
end example;

