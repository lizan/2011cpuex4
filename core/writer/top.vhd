library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity top is
  Port ( MCLK1 : in  STD_LOGIC;
         RS_TX : out  STD_LOGIC;
         RS_RX : in STD_LOGIC;
         ZD     : inout std_logic_vector(31 downto 0);  -- SRAMとやりとりするデータ
         ZA     : out    std_logic_vector(19 downto 0);  -- SRAMに読み書きするAddress
         XE1    : out    std_logic;     -- チップのenable信号、0でactiveとなる 電力消費を気にしないのであれば0固定
         E2A    : out    std_logic;     -- チップのenable信号、1でactiveとなる 電力消費を気にしないのであれば1固定
         XE3    : out    std_logic;     -- チップのenable信号、0でactiveとなる 電力消費を気にしないのであれば0固定
         XZBE   : out    std_logic_vector(3 downto 0);  --各Byteの書き込みマスク、全部0で固定
         XGA    : out    std_logic;     -- 出力のenable信号、0でactiveとなる 小休止とかしないのであれば0固定
         XWA    : out    std_logic;     -- 書き込みのenable信号、0でactive(書き込み可能)となる。この信号が来たときのアドレスに、2clock後のデータが書き込まれる
         XZCKE  : out    std_logic;     --clockのenable信号、さすがに0固定で良いと思う 
         ZCLKMA : out    std_logic_vector(1 downto 0);  --clock、遠いので2bitあるが、どちらにも同じ物を送れば良い
         ADVA   : out    std_logic;     --Burstアクセス(アドレスを連続して使用する)モード用に使う 今は0で良い  
         XFT    : out    std_logic;     --Pipelineモードか、FlowThroughモードかを切り替える 1固定
         XLBO   : out    std_logic;     --バーストアクセス用のアドレス順を指定する、active lowとあるが、とりあえず1で固定
         ZZA    : out    std_logic);    --1で省電力モード、とりあえず0で固定
end top;

architecture example of top is
  component sram_controller
    port (
      clk          : in std_logic;
      MemRead      : in std_logic;
      MemWrite     : in std_logic;
      XWA          : out std_logic;
      MemReadData  : out std_logic_vector(31 downto 0);
      MemWriteData : in std_logic_vector(31 downto 0);
      ZD           : inout std_logic_vector(31 downto 0));
  end component;
  component rs232c
    generic (
      constant wtime : std_logic_vector(15 downto 0) := x"1ABD");
    port (
      clk       : in  std_logic;
      S_Byte    : in  std_logic_vector(7 downto 0);
      R_Byte    : out std_logic_vector(7 downto 0);
      Send      : in  std_logic;
      Recv      : in std_logic;
      CanSend   : out std_logic;
      CanRecv   : out std_logic;
      Tx        : out std_logic;
      Rx        : in  std_logic);
  end component;

  component Iqueue is
    port (
      clk       : in  std_logic;
      R_Byte    : out std_logic_vector(7 downto 0);
      Recv      : in  std_logic;
      CanRecv   : out std_logic;
      rs_R_Byte : in  std_logic_vector(7 downto 0);
      rs_Recv   : out std_logic;
      rs_CanRecv: in std_logic);
  end component;
  
  signal clk      : std_logic;
  signal iclk     : std_logic;
  signal state    : std_logic_vector(3 downto 0) := "0000";
  signal addr     : std_logic_vector(19 downto 0) := x"00000";
  signal maxaddr  : std_logic_vector(19 downto 0) := x"00000";
  signal memreaddata : std_logic_vector(31 downto 0);
  signal Rdata    : std_logic_vector(31 downto 0);
  signal Wdata    : std_logic_vector(31 downto 0);
  signal memread  : std_logic;
  signal memwrite : std_logic;
  signal count    : std_logic_vector(1 downto 0) := "00";
  signal S_Byte   : std_logic_vector(7 downto 0);
  signal R_Byte   : std_logic_vector(7 downto 0);
  signal send     : std_logic := '0';
  signal recv     : std_logic := '1';
  signal cansend  : std_logic;
  signal canrecv  : std_logic;
  signal rs_S_Byte : std_logic_vector(7 downto 0);
  signal rs_R_Byte : std_logic_vector(7 downto 0);
  signal rs_send : std_logic;
  signal rs_recv : std_logic;
  signal rs_cansend : std_logic;
  signal rs_canrecv : std_logic;
  
begin
  ib: IBUFG port map (
    i=>MCLK1,
    o=>iclk);
  bg: BUFG port map (
    i=>iclk,
    o=>clk);
  SRAM_CONTROLLER0 : sram_controller port map (
    clk          => clk,
    MemRead      => memread,
    MemWrite     => memwrite,
    XWA          => XWA,
    MemReadData  => memreaddata,
    MemWriteData => Wdata,
    ZD           => ZD);
  rs232c0 : rs232c port map (
    clk    => clk,
    S_Byte => rs_S_Byte,
    R_Byte => rs_R_Byte,
    Send   => rs_send,
    Recv   => rs_recv,
    CanSend=> rs_cansend,
    CanRecv=> rs_canrecv,
    Tx     => RS_TX,
    Rx     => RS_RX);
  Iqueue0 : Iqueue port map (
    clk        => clk,
    R_Byte     => R_Byte,
    Recv       => recv,
    CanRecv    => canrecv,
    rs_R_Byte  => rs_R_Byte,
    rs_Recv    => rs_recv,
    rs_CanRecv => rs_canrecv);
  
  -- MODE CONTROL --
  ZA <= addr;
  XE1 <= '0';
  E2A <= '1';
  XE3 <= '0';
  XZBE <= "0000";
  XGA  <= '0';
  XZCKE <= '0';
  ZCLKMA(0) <= clk;
  ZCLKMA(1) <= clk;
  ADVA <= '0';
  XFT  <= '1';
  XLBO <= '1';
  ZZA  <= '0';

  memwrite <= '1' when state = "0100" else
              '0';
  memread <= '1' when state = "1000" else
             '0';
  rs_S_Byte <= Rdata(31 downto 24) when state = x"B" else
               Rdata(23 downto 16) when state = x"C" else
               Rdata(15 downto 8) when state = x"D" else
               Rdata(7 downto 0);
  recv <= '1'
          when
          (state = x"0" and canrecv = '1') or
          (state = x"1" and canrecv = '1') or
          (state = x"2" and canrecv = '1') or
          (state = x"3" and canrecv = '1') else
          '0';
  rs_send <= '1'
          when
          (state = x"B" and rs_cansend = '1') or
          (state = x"C" and rs_cansend = '1') or
          (state = x"D" and rs_cansend = '1') or
          (state = x"E" and rs_cansend = '1') else
          '0';

  
  P: process(clk)
  begin
    if rising_edge(clk) then
      case state is
        when "0000" =>
          if canrecv = '1' then
            Wdata(31 downto 24) <= R_Byte;
            state <= "0001";
          end if;
        when "0001" =>
          if canrecv = '1' then
            Wdata(23 downto 16) <= R_Byte;
            state <= "0010";
          end if;
        when "0010" =>
          if canrecv = '1' then
            Wdata(15 downto 8) <= R_Byte;
            state <= "0011";
          end if;
        when "0011" =>
          if canrecv = '1' then
            Wdata(7 downto 0) <= R_Byte;
            state <= "0100";
          end if;
        when "0100" =>
          state <= "0101";
        when "0101" =>
          state <= "0110";
        when "0110" =>
          state <= "0111";
          addr <= addr + 1;
        when "0111" =>
          if Wdata = x"00000000" then
            count <= count + 1;
          else
            count <= "00";
          end if;
          if count > "01" then
            state <= x"8";
            maxaddr <= addr;
            addr <= x"00000";
          else
            state <= x"0";
          end if;
        when x"8" =>
          state <= x"9";
        when x"9" =>
          state <= x"A";
        when x"A" =>
          state <= x"B";
          Rdata <= memreaddata;
        when x"B" =>
          if rs_cansend = '1' then
            state <= x"C";
          end if;
        when x"C" =>
          if rs_cansend = '1' then
            state <= x"D";
          end if;
        when x"D" =>
          if rs_cansend = '1' then
            state <= x"E";
          end if;
        when x"E" =>
          if rs_cansend = '1' then
            state <= x"F";
          end if;
        when x"F" =>
          if addr > maxaddr then
            state <= x"F";
          else
            state <= x"8";
            addr <= addr + 1;
          end if;
        when others =>
          null;
      end case;
    end if;
  end process;
end example;

