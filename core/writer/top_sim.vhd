library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity top is
end top;

architecture example of top is
  component sram_controller
    port (
      clk          : in std_logic;
      MemRead      : in std_logic;
      MemWrite     : in std_logic;
      XWA          : out std_logic;
      MemReadData  : out std_logic_vector(31 downto 0);
      MemWriteData : in std_logic_vector(31 downto 0);
      ZD           : inout std_logic_vector(31 downto 0));
  end component;

  component sram_sim is
  port (
    clk    : in    std_logic;
    Enable : in    std_logic;
    ZD     : inout std_logic_vector(31 downto 0);
    ZA     : in    std_logic_vector(19 downto 0);
    XWA    : in    std_logic);
  end component;

  signal clk      : std_logic;
  signal state    : std_logic_vector(3 downto 0) := "0000";
  signal zd       : std_logic_vector(31 downto 0) := x"00000000";
  signal addr     : std_logic_vector(19 downto 0) := x"00000";
  signal Rdata    : std_logic_vector(31 downto 0);
  signal Wdata    : std_logic_vector(31 downto 0);
  signal memread  : std_logic;
  signal memwrite : std_logic;
  signal count    : std_logic_vector(1 downto 0) := "00";
  signal send     : std_logic := '0';
  signal recv     : std_logic := '1';
  signal cansend  : std_logic;
  signal canrecv  : std_logic;
  signal xwa : std_logic;
  
begin
  SRAM_SIM0 : sram_sim port map (
    clk    => clk,
    Enable => '1',
    ZD     => zd,
    ZA     => addr,
    XWA    => xwa);
  
  SRAM_CONTROLLER0 : sram_controller port map (
    clk          => clk,
    MemRead      => memread,
    MemWrite     => memwrite,
    XWA          => xwa,
    MemReadData  => Rdata,
    MemWriteData => Wdata,
    ZD           => zd);  

  Wdata <= x"208403E8";
  memwrite <= '1' when state = "0100" else
              '0';
  memread <= '1';-- when state = "1000" else
             --'0';

  Renew_clock: process
  begin
    if clk = '0' then
      clk <= '1';
    else
      clk <= '0';
    end if;
    wait for 1 ns;
  end process;
  
  P: process(clk)
  begin
    if rising_edge(clk) then
      case state is
        when "0000" =>
          state <= "0001";
        when "0001" =>
          state <= "0010";
        when "0010" =>
          state <= "0011";
        when "0011" =>
          state <= "0100";
        when "0100" =>
          state <= "0101";
        when "0101" =>
          state <= "0110";
        when "0110" =>
          state <= "0111";
          addr <= addr + 1;
        when "0111" =>
          if Wdata = x"00000000" then
            count <= count + 1;
          else
            count <= "00";
          end if;
          if count > "01" then
            state <= "1000";
            addr <= x"00000";
          else
            state <= "0000";
          end if;
        when others =>
          if cansend = '1' then
            --addr <= addr + 1;
          end if;
      end case;
    end if;
  end process;
end example;

