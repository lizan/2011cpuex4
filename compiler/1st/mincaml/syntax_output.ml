(* type inference/reconstruction *)

open Syntax

exception Unify of Type.t * Type.t
exception Error of t * Type.t * Type.t

let extenv = ref M.empty

let cnt_tab = ref 0
let ofs = open_out "test_ck/syntax_output.dat"

let rec print_tab cnt =
	if cnt = 0 then 	()
	else ( Printf.fprintf ofs ";" ; print_tab (cnt - 1) );;
	
let rec print_colon cnt =
	if cnt = 0 then ()
	else ( Printf.fprintf ofs ":" ; print_colon (cnt - 1) ) 

let print_inst str0 cnt_tab =
	print_tab cnt_tab ;
	Printf.fprintf ofs "%s\n" str0;;
	
let print_const_var cnt_tab = function
	| Bool (b) -> print_tab cnt_tab ; if b then Printf.fprintf ofs "True\n" else Printf.fprintf ofs "False\n"
	| Int (i) -> print_tab cnt_tab ; Printf.fprintf ofs "Int(%d)\n" i 
	| Float (f) -> print_tab cnt_tab ; Printf.fprintf ofs "Float(%f)\n" f
	| Var (v) -> print_tab cnt_tab ; Printf.fprintf ofs "Var(%s)\n" v 
	| t -> print_tab cnt_tab ; Printf.fprintf ofs "PRINT_CONST_TAB\n" 
	

(* for pretty printing (and type normalization) *)
let rec deref_typ = function (* 型変数を中身でおきかえる関数 (caml2html: typing_deref) *)
  | Type.Fun(t1s, t2) -> Type.Fun(List.map deref_typ t1s, deref_typ t2)
  | Type.Tuple(ts) -> Type.Tuple(List.map deref_typ ts)
  | Type.Array(t) -> Type.Array(deref_typ t)
  | Type.Var({ contents = None } as r) ->
      Format.eprintf "uninstantiated type variable detected; assuming int@.";
      r := Some(Type.Int);
      Type.Int
  | Type.Var({ contents = Some(t) } as r) ->
  		let t' = deref_typ t in
      r := Some(t');
      t'
  | t -> t



	
let rec deref_id_typ cnt_tab (x, t) =
	print_colon cnt_tab ; 
	Printf.fprintf ofs "Let %s\n" x ; 
	(x, deref_typ t);;

let rec deref_term cnt_tab= function
  | Not(e) -> print_inst "Not" cnt_tab ; Not(deref_term (cnt_tab + 1 ) e)
  | Neg(e) -> print_inst "Neg" cnt_tab ; Neg(deref_term (cnt_tab + 1 ) e)
  | Add(e1, e2) -> print_inst "ADD" cnt_tab ; Add(deref_term (cnt_tab + 1) e1, deref_term (cnt_tab + 1) e2)
  | Sub(e1, e2) -> print_inst "SUB" cnt_tab ; Sub(deref_term (cnt_tab + 1) e1, deref_term (cnt_tab + 1) e2)
  | Eq(e1, e2) -> print_inst "EQ" cnt_tab ; Eq(deref_term (cnt_tab + 1) e1, deref_term (cnt_tab + 1) e2)
  | LE(e1, e2) -> print_inst "LE" cnt_tab ; LE(deref_term (cnt_tab + 1) e1, deref_term (cnt_tab + 1) e2)
  | FNeg(e) -> print_inst "FNeg" cnt_tab ; FNeg(deref_term (cnt_tab + 1) e)
  | FAdd(e1, e2) -> print_inst "FAdd" cnt_tab ; FAdd(deref_term (cnt_tab + 1) e1, deref_term (cnt_tab + 1) e2)
  | FSub(e1, e2) -> print_inst "FSub" cnt_tab ; FSub(deref_term (cnt_tab + 1) e1, deref_term (cnt_tab + 1) e2)
  | FMul(e1, e2) -> print_inst "FMul" cnt_tab ; FMul(deref_term (cnt_tab + 1) e1, deref_term (cnt_tab + 1) e2)
  | FDiv(e1, e2) -> print_inst "FDiv" cnt_tab ; FDiv(deref_term (cnt_tab + 1) e1, deref_term (cnt_tab + 1) e2)
  | If(e1, e2, e3) -> print_inst "If" cnt_tab ; If(deref_term (cnt_tab + 1) e1, deref_term (cnt_tab + 1) e2, deref_term (cnt_tab + 1) e3)
  | Let(xt, e1, e2) ->
  		print_colon cnt_tab ;
  		Printf.fprintf ofs "Let\n" ;
  		let arg0 = deref_id_typ (cnt_tab + 1) xt in
  		let arg1 = deref_term (cnt_tab + 2) e1 in
  		Printf.fprintf ofs "\n" ;
  		Let( arg0 , arg1 , deref_term (cnt_tab + 1) e2)
  | LetRec({ name = xt; args = yts; body = e1 }, e2) ->
  		print_colon cnt_tab ;
  		Printf.fprintf ofs "LetRec\n" ;
  		let arg0 = (deref_id_typ (cnt_tab + 1)) xt in
  		let arg1 = List.map (deref_id_typ (cnt_tab + 1)) yts in
  		let arg2 = deref_term (cnt_tab + 2) e1 in
  		Printf.fprintf ofs "\n" ;
      LetRec({ name = arg0 ;
	       args = arg1;
	       body = arg2},
	     deref_term (cnt_tab + 1) e2)
  | App(e, es) ->
  		print_inst "App" cnt_tab ; 
  		let arg0 = deref_term (cnt_tab + 1) e in
  		App(arg0,  List.map (deref_term (cnt_tab + 1)) es)
  | Tuple(es) -> print_inst "Tuple" cnt_tab ; Tuple(List.map (deref_term (cnt_tab + 1)) es)
  | LetTuple(xts, e1, e2) -> print_inst "LetTuple" cnt_tab ; LetTuple(List.map (deref_id_typ (cnt_tab + 1)) xts, deref_term (cnt_tab + 1) e1, deref_term (cnt_tab +1) e2)
  | Array(e1, e2) -> print_inst "Array" cnt_tab ; Array(deref_term (cnt_tab + 1) e1, deref_term (cnt_tab + 1) e2)
  | Get(e1, e2) -> print_inst "Get" cnt_tab ; Get(deref_term (cnt_tab + 1) e1, deref_term (cnt_tab + 1) e2)
  | Put(e1, e2, e3) -> print_inst "Put" cnt_tab ; Put(deref_term (cnt_tab + 1) e1, deref_term (cnt_tab + 1) e2, deref_term (cnt_tab + 1) e3)
  | e ->( print_const_var cnt_tab e ; e )






let rec occur r1 = function (* occur check (caml2html: typing_occur) *)
  | Type.Fun(t2s, t2) -> List.exists (occur r1) t2s || occur r1 t2
  | Type.Tuple(t2s) -> List.exists (occur r1) t2s
  | Type.Array(t2) -> occur r1 t2
  | Type.Var(r2) when r1 == r2 -> true
  | Type.Var({ contents = None }) -> false
  | Type.Var({ contents = Some(t2) }) -> occur r1 t2
  | _ -> false

let rec unify t1 t2 = (* 型が合うように、型変数への代入をする (caml2html: typing_unify) *)
  match t1, t2 with
  | Type.Unit, Type.Unit | Type.Bool, Type.Bool | Type.Int, Type.Int | Type.Float, Type.Float -> ()
  | Type.Fun(t1s, t1'), Type.Fun(t2s, t2') ->
      (try List.iter2 unify t1s t2s
      with Invalid_argument("List.iter2") -> raise (Unify(t1, t2)));
      unify t1' t2'
  | Type.Tuple(t1s), Type.Tuple(t2s) ->
      (try List.iter2 unify t1s t2s
      with Invalid_argument("List.iter2") -> raise (Unify(t1, t2)))
  | Type.Array(t1), Type.Array(t2) -> unify t1 t2
  | Type.Var(r1), Type.Var(r2) when r1 == r2 -> ()
  | Type.Var({ contents = Some(t1') }), _ -> unify t1' t2
  | _, Type.Var({ contents = Some(t2') }) -> unify t1 t2'
  | Type.Var({ contents = None } as r1), _ -> (* 一方が未定義の型変数の場合 (caml2html: typing_undef) *)
      if occur r1 t2 then raise (Unify(t1, t2));
      r1 := Some(t2)
  | _, Type.Var({ contents = None } as r2) ->
      if occur r2 t1 then raise (Unify(t1, t2));
      r2 := Some(t1)
  | _, _ -> raise (Unify(t1, t2))

let rec g env e = (* 型推論ルーチン (caml2html: typing_g) *)
  try
    match e with
    | Unit -> Type.Unit
    | Bool(_) -> Type.Bool
    | Int(_) -> Type.Int
    | Float(_) -> Type.Float
    | Not(e) ->
	unify Type.Bool (g env e);
	Type.Bool
    | Neg(e) ->
	unify Type.Int (g env e);
	Type.Int
    | Add(e1, e2) | Sub(e1, e2) -> (* 足し算（と引き算）の型推論 (caml2html: typing_add) *)
	unify Type.Int (g env e1);
	unify Type.Int (g env e2);
	Type.Int
    | FNeg(e) ->
	unify Type.Float (g env e);
	Type.Float
    | FAdd(e1, e2) | FSub(e1, e2) | FMul(e1, e2) | FDiv(e1, e2) ->
	unify Type.Float (g env e1);
	unify Type.Float (g env e2);
	Type.Float
    | Eq(e1, e2) | LE(e1, e2) ->
	unify (g env e1) (g env e2);
	Type.Bool
    | If(e1, e2, e3) ->
	unify (g env e1) Type.Bool;
	let t2 = g env e2 in
	let t3 = g env e3 in
	unify t2 t3;
	t2
    | Let((x, t), e1, e2) -> (* letの型推論 (caml2html: typing_let) *)
	unify t (g env e1);
	g (M.add x t env) e2
    | Var(x) when M.mem x env -> M.find x env (* 変数の型推論 (caml2html: typing_var) *)
    | Var(x) when M.mem x !extenv -> M.find x !extenv
    | Var(x) -> (* 外部変数の型推論 (caml2html: typing_extvar) *)
	Format.printf "free variable %s assumed as external@." x;
	let t = Type.gentyp () in
	extenv := M.add x t !extenv;
	t
    | LetRec({ name = (x, t); args = yts; body = e1 }, e2) -> (* let recの型推論 (caml2html: typing_letrec) *)
	let env = M.add x t env in
	unify t (Type.Fun(List.map snd yts, g (M.add_list yts env) e1));
	g env e2
    | App(e, es) -> (* 関数適用の型推論 (caml2html: typing_app) *)
	let t = Type.gentyp () in
	unify (g env e) (Type.Fun(List.map (g env) es, t));
	t
    | Tuple(es) -> Type.Tuple(List.map (g env) es)
    | LetTuple(xts, e1, e2) ->
	unify (Type.Tuple(List.map snd xts)) (g env e1);
	g (M.add_list xts env) e2
    | Array(e1, e2) -> (* must be a primitive for "polymorphic" typing *)
	unify (g env e1) Type.Int;
	Type.Array(g env e2)
    | Get(e1, e2) ->
	let t = Type.gentyp () in
	unify (Type.Array(t)) (g env e1);
	unify Type.Int (g env e2);
	t
    | Put(e1, e2, e3) ->
	let t = g env e3 in
	unify (Type.Array(t)) (g env e1);
	unify Type.Int (g env e2);
	Type.Unit
  with Unify(t1, t2) -> raise (Error(deref_term 0 e, deref_typ t1, deref_typ t2))

let f e =
  extenv := M.empty;
(*
  (match deref_typ (g M.empty e) with
  | Type.Unit -> ()
  | _ -> Format.efprintf ofs "warning: final result does not have type unit@.");
*)
  (try unify Type.Unit (g M.empty e)
  with Unify _ -> failwith "top level does not have type unit");
  extenv := M.map deref_typ !extenv;
  Printf.printf "\nStart syntax_output \n" ;
  deref_term 0 e;
  Printf.printf "print to test_ck/syntax_output.dat\n" ;
  Printf.printf "End syntax_output \n" ;
  e
