open Asm


let stackset = ref S.empty (* すでに Save された変数の集合 *)
let stackmap = ref [] (* Save された変数のスタックにおける位置 *)
let save x = 
  stackset := S.add x !stackset;
  if not (List.mem x !stackmap) then
    stackmap := !stackmap @ [x]
let savef x = 
  stackset := S.add x !stackset;
  if not (List.mem x !stackmap) then
    (let pad = 
       if List.length !stackmap mod 2 = 0 then [] else [Id.gentmp Type.Int] in
       stackmap := !stackmap @ pad @ [x; x])
let locate x = 
  let rec loc = function 
    | [] -> []
    | y :: zs when x = y -> 0 :: List.map succ (loc zs)
    | y :: zs -> List.map succ (loc zs) in
    loc !stackmap
let offset x = 4 * List.hd (locate x)
let stacksize () = align ((List.length !stackmap + 1) * 4)

let reg r =
	if is_reg r
	then String.sub r 1 (String.length r - 1)
	else r


(* 関数呼び出しのために引数を並べ替える (register shuffling) *)
let rec shuffle sw xys = 
  (* remove identical moves *)
  let (_, xys) = List.partition (fun (x, y) -> x = y) xys in
    (* find acyclic moves *)
    match List.partition (fun (_, y) -> List.mem_assoc y xys) xys with
      | ([], []) -> []
      | ((x, y) :: xys, []) -> (* no acyclic moves; resolve a cyclic move *)
	  (y, sw) :: (x, y) :: 
	    shuffle sw (List.map (function 
				    | (y', z) when y = y' -> (sw, z)
				    | yz -> yz) xys)
      | (xys, acyc) -> acyc @ shuffle sw xys


type dest = Tail | NonTail of Id.t
let rec g oc = function
	| (dest, Ans (exp)) -> g' oc (dest,exp)
	| (dest, Let ((x,t),exp,e)) -> g' oc (NonTail(x),exp) ; g oc (dest,e)

and g' oc = function
	| (NonTail(x), Li(i)) when -32768 <= i && i < 32768 -> Printf.fprintf oc "\tADDI\t%s r0 %d\n" (reg x) i
	| (NonTail(x),Add(y, V(z))) -> Printf.fprintf oc "\tADD\t%s %s %s\n" (reg x) (reg y) (reg z)
	| (NonTail(x),Add(y, C(z))) -> Printf.fprintf oc "\tADDI\t%s %s %d\n" (reg x) (reg y) z
	| (NonTail(x),Sub(y, V(z))) -> Printf.fprintf oc "\tSUB\t%s %s %s\n" (reg x) (reg y) (reg z)
	| (NonTail(x),Sub(y, C(z))) -> Printf.fprintf oc "\tADDI\t%s %s %d\n" (reg x) (reg y) (0 - z)
	| (NonTail(_),Stw(x, y, V(z))) -> Printf.fprintf oc "\tSW\t%s %s %s\n" (reg x) (reg y) (reg z)
	| (NonTail(_),Stw(x, y, C(z))) -> Printf.fprintf oc "\tSWI\t%s %s %d\n" (reg x) (reg y) z
	| (NonTail(_),Save(x, y)) when List.mem x allregs && not (S.mem y !stackset) ->
		save y ; Printf.fprintf oc "\tSWI\t%s %s %d\n" (reg x) reg_sp (offset y)
	| (NonTail(x),Restore(y)) when List.mem x allregs ->
		Printf.fprintf oc "\tLWI\t%s %s %d\n" (reg x) reg_sp (offset y)
	| (NonTail(a),CallDir(Id.L(x),ys,zs)) ->
		Printf.fprintf oc "#mflr [ NonTail CallDir ]\n" ;
		g'_args oc [] ys zs ;
		let ss = stacksize () in
		Printf.fprintf oc "#SW %s %d(%s) [ NonTail CallDir ]\n" reg_tmp (ss - 4) reg_sp ;
		Printf.fprintf oc "\tSWPC\t %s %d # to remenber program counter\n" reg_sp ss ;
		Printf.fprintf oc "\tADDI\t%s %s %d # This is semi-original\n" reg_sp reg_sp (ss + 4);
		Printf.fprintf oc "\tJL\t%s\n" x;
		Printf.fprintf oc "\tADDI\t%s %s %d # This is semi-original\n" reg_sp reg_sp (0 - (ss + 4)) ;
		Printf.fprintf oc "#LW %s %d(%s) [ NonTail CallDir ]\n" reg_tmp (ss - 4) reg_sp ;
		(if List.mem a allregs && a <> regs.(0) then 
			Printf.fprintf oc "\tADDI\t%s %s 0\n" (reg a) (reg regs.(0))
		 else if List.mem a allfregs && a <> fregs.(0) then
	   	Printf.fprintf oc "\tFADDI\t%s %s 0\n" (reg a) (reg fregs.(0)));
		Printf.fprintf oc "#mtlr [ NonTail CallDir ]\n" 
	
	
	| (Tail, (Li _ | SetL _ | Mr _ | Neg _ | Add _ | Sub _ | Slw _ | Lwz _ as exp)) ->
		g' oc (NonTail(regs.(0)),exp) ;
		Printf.fprintf oc "#blr\n" ; 
		Printf.fprintf oc "\tLWI\tr30 r3 -4\n" ;
		Printf.fprintf oc "\tJR\tr30\n" ;
		Printf.fprintf oc "#blr\n" 
	| (Tail, IfEq(x,V(y),e1,e2)) ->
		Printf.fprintf oc "[ Tail , IfEq ] not implemented \n" ;
		g'_tail_if oc e1 e2 "ifeq0" "ifeq1"
	| (Tail, IfEq(x,C(y),e1,e2)) ->
		Printf.fprintf oc "# [ Tail IfEq ] start \n" ;
		Printf.fprintf oc "\tADDI\tr27 r0 %d\n" y ;
		Printf.fprintf oc "\tADDI\tr30 r0 1\n" ;
		Printf.fprintf oc "\tSLT\tr28 %s r27\n" x ;
		Printf.fprintf oc "\tSLT\tr29 r27 %s\n" x ;
		Printf.fprintf oc "\tOR\tr29 r28 r29\n" ;
		Printf.fprintf oc "\tSLT\tr30 r29 r30\n" ;
		Printf.fprintf oc "# [ Tail IfEq ] end \n" ;
		g'_tail_if oc e1 e2 "IfEq" "JEQ"
		| _ -> Printf.fprintf oc "[ g' oc ] not implemented\n"

	  
and g'_args oc x_reg_cl ys zs = 
  let (i, yrs) = 
    List.fold_left
      (fun (i, yrs) y -> (i + 1, (y, regs.(i)) :: yrs))
      (0, x_reg_cl) ys in
    List.iter
      (fun (y, r) -> Printf.fprintf oc "\tADDI\t%s %s 0\n" (reg r) (reg y))
      (shuffle reg_sw yrs);
    let (d, zfrs) = 
      List.fold_left
	(fun (d, zfrs) z -> (d + 1, (z, fregs.(d)) :: zfrs))
	(0, []) zs in
      List.iter
        (fun (z, fr) -> Printf.fprintf oc "\tFADDI\t%s %s 0\n" (reg fr) (reg z))
	(shuffle reg_fsw zfrs)
	
and g'_tail_if oc e1 e2 b bn =
	let b_else = Id.genid (b ^ "_else") in
	Printf.fprintf oc "\t%s\tr30 r0 %s\n" bn b_else ;
	let stackset_back = !stackset in
	g oc (Tail, e1) ;
	Printf.fprintf oc "\n%s:\n" b_else;
	stackset := stackset_back ;
	g oc (Tail ,e2)

let h oc { name = Id.L(x); args = _ ; fargs = _; body = e; ret = _ } =
	Printf.fprintf oc "%s:\n" x;
	stackset := S.empty;
	stackmap := [];
	g oc (Tail, e)


let f oc (Prog(data, fundefs, e)) =
	Printf.printf "\nmy_emit start\n" ;
	Printf.fprintf oc "JL __main__\n" ;
	List.iter (fun fundef -> h oc fundef) fundefs ;
	Printf.fprintf oc "\n__main__:\n" ;
	stackset := S.empty;
	stackmap := [];
	g oc (NonTail("_R_0"), e) ;
	Printf.printf "my_emit end\n\n"
