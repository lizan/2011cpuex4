(* rename identifiers to make them unique (alpha-conversion) *)

open KNormal



let cnt_tab = ref 0
let ofs = open_out "kNormal_output.dat"

let rec print_tab cnt =
	if cnt = 0 then 	()
	else ( Printf.fprintf ofs  ";" ; print_tab (cnt - 1) );;
	
let rec print_colon cnt =
	if cnt = 0 then ()
	else ( Printf.fprintf ofs  ":" ; print_colon (cnt - 1) ) 

let print_inst str0 cnt_tab =
	print_tab cnt_tab ;
	Printf.fprintf ofs  "%s\n" str0;;
	
	
	

let find x env = try M.find x env with Not_found -> x

let find_output x env cnt_tab =
	print_inst (find x env) cnt_tab ;
	find x env

let rec g env cnt_tab = function (* α変換ルーチン本体 (caml2html: alpha_g) *)
  | Unit -> print_inst "Unit" cnt_tab ; Unit
  | Int(i) -> print_tab cnt_tab ; Printf.fprintf ofs  "%d\n" i ; Int(i)
  | Float(d) -> print_tab cnt_tab ; Printf.fprintf ofs  "%f\n" d ; Float(d)
  | Neg(x) -> print_inst "Neg" cnt_tab ; Neg(find_output x env cnt_tab)
  | Add(x, y) -> print_inst "Add" cnt_tab ; Add(find_output x env (cnt_tab + 1), find_output y env (cnt_tab + 1))
  | Sub(x, y) -> print_inst "Sub" cnt_tab ; Sub(find_output x env (cnt_tab + 1), find_output y env (cnt_tab + 1))
  | FNeg(x) -> print_inst "FNeg" cnt_tab ; FNeg(find_output x env (cnt_tab + 1))
  | FAdd(x, y) -> print_inst "FAdd" cnt_tab ; FAdd(find_output x env (cnt_tab + 1), find_output y env (cnt_tab + 1))
  | FSub(x, y) -> print_inst "FSub" cnt_tab ; FSub(find_output x env (cnt_tab + 1), find_output y env (cnt_tab + 1))
  | FMul(x, y) -> print_inst "FMul" cnt_tab ; FMul(find_output x env (cnt_tab + 1), find_output y env (cnt_tab + 1))
  | FDiv(x, y) -> print_inst "FDiv" cnt_tab ; FDiv(find_output x env (cnt_tab + 1), find_output y env (cnt_tab + 1))
  | IfEq(x, y, e1, e2) -> print_inst "IfEq" cnt_tab ; IfEq(find_output x env (cnt_tab + 1), find_output y env (cnt_tab + 1), g env (cnt_tab + 1) e1, g env (cnt_tab + 1) e2)
  | IfLE(x, y, e1, e2) -> print_inst "IfLE" cnt_tab ; IfLE(find_output x env (cnt_tab + 1), find_output y env (cnt_tab + 1), g env (cnt_tab + 1) e1, g env (cnt_tab + 1) e2)
  | Let((x, t), e1, e2) -> (* letのα変換 (caml2html: alpha_let) *)
  		print_inst "Let" cnt_tab ;
  		print_tab (cnt_tab + 1) ;
  		Printf.fprintf ofs  "%s\n" (Id.genid x) ;
      let x' = Id.genid x in
      let arg1 = g env (cnt_tab + 2) e1 in
      Printf.fprintf ofs  "\n" ;
  		let arg2 = g (M.add x x' env) (cnt_tab + 1) e2 in
      Let((x', t), arg1 , arg2)
  | Var(x) -> print_inst "Var" cnt_tab ; Var(find_output x env cnt_tab)
  | LetRec({ name = (x, t); args = yts; body = e1 }, e2) -> (* let recのα変換 (caml2html: alpha_letrec) *)
  		print_inst "LetRec" cnt_tab ;
      let env = M.add x (Id.genid x) env in
      let ys = List.map fst yts in
      let env' = M.add_list2 ys (List.map Id.genid ys) env in
      let arg0 = (find_output x env (cnt_tab + 1), t) in
      let arg1 = List.map (fun (y, t) -> (find_output y env' (cnt_tab + 2), t)) yts in
      let arg2 = g env' (cnt_tab + 3) e1 in
      Printf.fprintf ofs  "\n" ;
      LetRec({ name = arg0;
	       args = arg1;
	       body = arg2 }, g env (cnt_tab + 1) e2)
  | App(x, ys) ->
  		print_inst "App" cnt_tab ;
  		let arg0 = find_output x env (cnt_tab + 1) in
  		App(arg0, List.map (fun y -> find_output y env (cnt_tab + 1)) ys)
  | Tuple(xs) -> print_inst "Tuple" cnt_tab ; Tuple(List.map (fun x -> find_output x env cnt_tab) xs)
  | LetTuple(xts, y, e) -> (* LetTupleのα変換 (caml2html: alpha_lettuple) *)	
  		print_inst "LetTuple" cnt_tab ; 
      let xs = List.map fst xts in
      let env' = M.add_list2 xs (List.map Id.genid xs) env in
      LetTuple(List.map (fun (x, t) -> (find_output x env' cnt_tab, t)) xts, find_output y env cnt_tab, g env' (cnt_tab + 1) e)
  | Get(x, y) -> print_inst "Get" cnt_tab ; Get(find_output x env cnt_tab, find_output y env cnt_tab)
  | Put(x, y, z) -> print_inst "Put" cnt_tab ; Put(find_output x env cnt_tab, find_output y env cnt_tab, find_output z env cnt_tab)
  | ExtArray(x) -> print_inst "ExtArray" cnt_tab ; ExtArray(x)
  | ExtFunApp(x, ys) -> 
  		print_inst "ExtFunApp" cnt_tab ; 
  		print_tab (cnt_tab + 1) ;
  		Printf.fprintf ofs  "%s\n" x ;
  		ExtFunApp(x, List.map (fun y -> find_output y env (cnt_tab + 2)) ys)

let f var =
	Printf.printf "\nStart KNormal_output\n" ;
	g M.empty 0 var ;
	Printf.printf "print to kNormal_output.dat\n" ;
	Printf.printf "End KNormal_output\n\n" ;
	var
