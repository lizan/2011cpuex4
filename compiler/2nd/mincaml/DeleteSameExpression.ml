open KNormal

module Env0 =
	Map.Make
		(struct
			type t = KNormal.t
			let compare = compare
		end)
include Env0

let isAppPutGet = function 
	| Unit | App(_) | Get(_) | Put(_) | ExtArray(_) | ExtFunApp(_) -> true
	| _ -> false


let rec g env= function
	| Unit -> Unit
	| Int(i) -> Int(i)
	| Float(f) -> Float(f)
	| Var(x) -> Var(x)
	| Neg(x) -> Neg(x)
	| Add(x,y) -> Add(x,y)
	| Sub(x,y) -> Sub(x,y)
	| Mul(x,y) -> Mul(x,y)
	| Div(x,y) -> Div(x,y)
	| FNeg(x) -> FNeg(x)
	| FAdd(x,y) -> FAdd(x,y)
	| FSub(x,y) -> FSub(x,y)
	| FMul(x,y) -> FMul(x,y)
	| FDiv(x,y) -> FDiv(x,y)
	| IfEq(a,b,c,d) -> IfEq(a,b,g env c,g env d)
	| IfLE(a,b,c,d) -> IfLE(a,b,g env c,g env d)
	| Let((a,b),c,d) ->
			let cc = g env c in
			if (Env0.mem cc env)  &&  not ( isAppPutGet cc ) then
			(
				Format.eprintf "delete : %s\n" a ;
				Let((a,b),(Env0.find cc env),g env d)
			)
			else
				Let((a,b),cc,g (Env0.add cc (Var(a)) env) d)
	| LetRec({name=(x,t);args=a;body=b},y) ->  
			LetRec({name=(x,t);args=a;body=g env b},g env y)
	| App(x,y) -> App(x,y)
	| Tuple(y) -> Tuple(y) 
	| LetTuple(x,y,z) -> LetTuple(x,y,z)
	| Get(x,y) -> Get(x,y)
	| Put(x,y,z) -> Put(x,y,z)
	| ExtArray(x) -> ExtArray(x)
	| ExtFunApp(x,y) -> ExtFunApp(x,y)

let f x = ( 
	print_string "START\n";
	let res = g Env0.empty x in
	print_string "END\n";
	res
)
