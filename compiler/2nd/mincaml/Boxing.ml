type t =
	| Ans of exp * (Id.t * int) list * (Id.t * int) list
	| Let of (Id.t * Type.t) * exp * t * (Id.t * int) list * (Id.t * int) list
and exp =
	| Nop of (Id.t * int) list * (Id.t * int) list
	| Li of int * (Id.t * int) list * (Id.t * int) list
	| FLi of Id.l * (Id.t * int) list * (Id.t * int) list
	| SetL of Id.l * (Id.t * int ) list * (Id.t * int) list
	| Mr   of Id.t * (Id.t * int ) list * (Id.t * int) list
	| Neg  of Id.t * (Id.t * int ) list * (Id.t * int) list
	| Add  of Id.t * Asm.id_or_imm * (Id.t * int) list * (Id.t * int) list
	| Sub  of Id.t * Asm.id_or_imm * (Id.t * int) list * (Id.t * int) list
	| Mul  of Id.t * Asm.id_or_imm * (Id.t * int) list * (Id.t * int) list
	| Div  of Id.t * Asm.id_or_imm * (Id.t * int) list * (Id.t * int) list
	| Slw  of Id.t * Asm.id_or_imm * (Id.t * int) list * (Id.t * int) list
	| Lwz  of Id.t * Asm.id_or_imm * (Id.t * int) list * (Id.t * int) list
	| Stw  of Id.t * Id.t * Asm.id_or_imm * (Id.t * int ) list * (Id.t * int) list
	| FMr  of Id.t * (Id.t * int ) list * (Id.t * int) list
	| FNeg of Id.t * (Id.t * int ) list * (Id.t * int ) list
	| FAdd of Id.t * Id.t * (Id.t * int ) list * (Id.t * int) list
	| FSub of Id.t * Id.t * (Id.t * int ) list * (Id.t * int) list
	| FMul of Id.t * Id.t * (Id.t * int ) list * (Id.t * int) list
	| FDiv of Id.t * Id.t * (Id.t * int ) list * (Id.t * int) list
	| Lfd  of Id.t * Asm.id_or_imm * (Id.t * int ) list * (Id.t * int) list
	| Stfd of Id.t * Id.t * Asm.id_or_imm * (Id.t * int ) list * (Id.t * int) list
	| Comment of string * (Id.t * int) list  * (Id.t * int) list
	| IfEq of Id.t * Asm.id_or_imm * t * t * (Id.t * int ) list * (Id.t * int) list
	| IfLE of Id.t * Asm.id_or_imm * t * t * (Id.t * int ) list * (Id.t * int) list
	| IfGE of Id.t * Asm.id_or_imm * t * t * (Id.t * int ) list * (Id.t * int) list
	| IfFEq of Id.t * Id.t * t * t * (Id.t * int ) list * (Id.t * int ) list
	| IfFLE of Id.t * Id.t * t * t * (Id.t * int ) list * (Id.t * int ) list
	| CallCls of Id.t * Id.t list * Id.t list * (Id.t * int ) list * (Id.t * int) list
	| CallDir of Id.l * Id.t list * Id.t list * (Id.t * int ) list * (Id.t * int) list
	| Save of Id.t * Id.t * (Id.t * int) list * (Id.t * int) list
	| Restore of Id.t * (Id.t * int) list * (Id.t * int) list


type fundef =
    { myname : Id.l; myargs : Id.t list; myfargs : Id.t list; mybody : t; myret : Type.t }
(* プログラム全体 = 浮動小数点数テーブル + トップレベル関数 + メインの式 *)
type prog = Prog of (Id.l * float) list * fundef list * t

let get_list_t = function
	| Ans(_,il,fl)     -> (il,fl)
	| Let(_,_,_,il,fl) -> (il,fl)
let get_list_e = function
	| Nop(il,fl)       -> (il,fl)
	| Li(_,il,fl)      -> (il,fl)
	| FLi(_,il,fl)     -> (il,fl)
	| SetL(_,il,fl)    -> (il,fl)
	| Comment(_,il,fl) -> (il,fl)
	| Mr(_,il,fl)      -> (il,fl)
	| Neg(_,il,fl)     -> (il,fl)
	| Add(_,_,il,fl)   -> (il,fl)
	| Sub(_,_,il,fl)   -> (il,fl)
	| Mul(_,_,il,fl)   -> (il,fl)
	| Div(_,_,il,fl)   -> (il,fl)
	| Slw(_,_,il,fl)   -> (il,fl)
	| Lwz(_,_,il,fl)   -> (il,fl)
	| Stw(_,_,_,il,fl) -> (il,fl)
	| FMr(_,il,fl)     -> (il,fl)
	| FNeg(_,il,fl)    -> (il,fl)
	| FAdd(_,_,il,fl)  -> (il,fl)
	| FSub(_,_,il,fl)  -> (il,fl)
	| FMul(_,_,il,fl)  -> (il,fl)
	| FDiv(_,_,il,fl)  -> (il,fl)
	| Lfd(_,_,il,fl)   -> (il,fl)
	| Stfd(_,_,_,il,fl)-> (il,fl)
	| IfEq(_,_,_,_,il,fl) -> (il,fl)
	| IfLE(_,_,_,_,il,fl) -> (il,fl)
	| IfGE(_,_,_,_,il,fl) -> (il,fl)
	| IfFEq(_,_,_,_,il,fl)-> (il,fl)
	| IfFLE(_,_,_,_,il,fl)-> (il,fl)
	| CallCls(_,_,_,il,fl)-> (il,fl)
	| CallDir(_,_,_,il,fl)-> (il,fl)
	| Save(_,_,il,fl)  -> (il,fl)
	| Restore(_,il,fl)-> (il,fl)
	
	
let exist k = function
	| [] -> false
	| hd :: tl -> if (fst hd) = k then true else false 

let depth = ref 0

let add0 k m = 
	depth := !depth + 1 ;
	[(k, !depth)] @ m
	
let add1 iv m =
	match iv with
	| Asm.V(v) -> add0 v m
	| _ -> m

let rec remove k = function
	| [] -> []
	| hd :: tl -> if (fst hd) = k then (remove k tl) else ( [hd] @ (remove k tl))
	
let rec find k = function
	| [] -> -1
	| hd :: tl -> if (fst hd) = k then (snd hd) else (find k tl)
	
let rec union_map m0 m1 =
	match m0 with
	| [] -> m1
	| hd0 :: tl0 ->
	(	match m1 with
		| [] -> m0
		| hd1 :: tl1 -> 
		(
			if (snd hd0) < (snd hd1) then
				[hd1] @ (union_map (remove (fst hd1) m0) (remove (fst hd1) tl1))
			else
				[hd0] @ (union_map (remove (fst hd0) tl0) (remove (fst hd0) m1))
		)
	)


let fst (e0, _, _) = e0
let snd (_, e1, _) = e1
let thd (_, _, e2) = e2

exception ASM1
exception NoReg of Id.t * Type.t

let rec asm0 ineed fneed = function
	| Asm.Ans(e) -> 
		let next0 = asm1 ineed fneed e in
		let next = get_list_e next0 in
		Ans(next0, (fst next), (snd next))
	| Asm.Let((x, t), a, e) -> 
		let next0 = asm0 ineed fneed e in
		let next1 = asm1 (fst (get_list_t next0)) (snd (get_list_t next0)) a in
		let next = get_list_e next1 in
		Printf.eprintf "\n" ;
		List.iter (fun x -> Printf.eprintf "%s : %d\n" (fst x) (snd x) ) (fst next);
		Let((x, t), next1, next0, remove x (fst next), remove x (snd next))
and asm1 ineed fneed = function
	| Asm.Comment(s)    -> Comment(s, ineed, fneed)
	| Asm.Nop            -> Nop(ineed, fneed)
	| Asm.Li(i)          -> Li(i, ineed, fneed)
	| Asm.FLi(l)         -> FLi(l, ineed, fneed)
	| Asm.SetL(l)        -> SetL(l, ineed, fneed)
	| Asm.Mr(x)          -> Mr(x, add0 x ineed, fneed)
	| Asm.Neg(x)         -> Neg(x, add0 x ineed, fneed)
	| Asm.Add(x,y)       -> Add(x, y, add0 x ( add1 y ineed), fneed)
	| Asm.Sub(x,y)       -> Sub(x, y, add0 x ( add1 y ineed), fneed)
	| Asm.Mul(x,y)       -> Mul(x, y, add0 x ( add1 y ineed), fneed)
	| Asm.Div(x,y)       -> Div(x, y, add0 x ( add1 y ineed), fneed)
	| Asm.Slw(x,y)       -> Slw(x, y, add0 x ( add1 y ineed), fneed)
	| Asm.Lwz(x,y)       -> Lwz(x, y, add0 x ( add1 y ineed), fneed)
	| Asm.Stw(x,y,z)     -> Stw(x, y, z, add0 x ( add0 y (add1 z ineed)), fneed)
	| Asm.FMr(x)         -> FMr(x, ineed, add0 x fneed)
	| Asm.FNeg(x)        -> FNeg(x, ineed, add0 x fneed)
	| Asm.FAdd(x,y)      -> FAdd(x, y, ineed, add0 x ( add0 y fneed))
	| Asm.FSub(x,y)      -> FSub(x, y, ineed, add0 x ( add0 y fneed))
	| Asm.FMul(x,y)      -> FMul(x, y, ineed, add0 x ( add0 y fneed))
	| Asm.FDiv(x,y)      -> FDiv(x, y, ineed, add0 x ( add0 y fneed))
	| Asm.Lfd(x,y)       -> Lfd(x, y, add0 x ( add1 y ineed), fneed)
	| Asm.Stfd(x,y,z)    -> Stfd(x, y, z, add0 y (add1 z ineed), add0 x fneed)
	| Asm.IfEq(x,y,e0,e1) ->
		let next0 = asm0 ineed fneed e0 in
		let next1 = asm0 ineed fneed e1 in
		let nextF = union_map (fst (get_list_t next0)) (fst (get_list_t next1)) in
		let nextS = union_map (snd (get_list_t next0)) (snd (get_list_t next1)) in
		IfEq(x, y, next0, next1, add0 x (add1 y nextF), nextS)
	| Asm.IfLE(x,y,e0,e1) ->
		let next0 = asm0 ineed fneed e0 in
		let next1 = asm0 ineed fneed e1 in
		let nextF = union_map (fst (get_list_t next0)) (fst (get_list_t next1)) in
		let nextS = union_map (snd (get_list_t next0)) (snd (get_list_t next1)) in
		IfLE(x, y, next0, next1, add0 x (add1 y nextF), nextS)
	| Asm.IfGE(x,y,e0,e1) ->
		let next0 = asm0 ineed fneed e0 in
		let next1 = asm0 ineed fneed e1 in
		let nextF = union_map (fst (get_list_t next0)) (fst (get_list_t next1)) in
		let nextS = union_map (snd (get_list_t next0)) (snd (get_list_t next1)) in
		IfGE(x, y, next0, next1, add0 x (add1 y nextF), nextS)
	| Asm.IfFEq(x,y,e0,e1) ->
		let next0 = asm0 ineed fneed e0 in
		let next1 = asm0 ineed fneed e1 in
		let nextF = union_map (fst (get_list_t next0)) (fst (get_list_t next1)) in
		let nextS = union_map (snd (get_list_t next0)) (snd (get_list_t next1)) in
		IfFEq(x, y, next0, next1, nextF, add0 x (add0 y nextS))
	| Asm.IfFLE(x,y,e0,e1) ->
		let next0 = asm0 ineed fneed e0 in
		let next1 = asm0 ineed fneed e1 in
		let nextF = union_map (fst (get_list_t next0)) (fst (get_list_t next1)) in
		let nextS = union_map (snd (get_list_t next0)) (snd (get_list_t next1)) in
		IfFLE(x, y, next0, next1, nextF, add0 x (add0 y nextS))
	| Asm.CallCls(x,y,z) -> 
		CallCls(x, y, z, add0 x (List.fold_right add0 y ineed), List.fold_right add0 z fneed)
	| Asm.CallDir(x,y,z) ->
		CallDir(x, y, z, List.fold_right add0 y ineed, List.fold_right add0 z fneed)
	| _ -> raise(ASM1)
	
	

let rec box0 = function
	| Ans(exp,il,fl)   -> Asm.Ans(box1 exp)
	| Let(x,y,z,il,fl) -> Asm.Let(x, box1 y, box0 z)
and box1 = function
	| Comment(s,_,_)   -> Asm.Comment(s)
	| Nop(_,_)           -> Asm.Nop
	| Li(i,_,_)         -> Asm.Li(i)
	| FLi(l,_,_)        -> Asm.FLi(l)
	| SetL(l,_,_)       -> Asm.SetL(l)
	| Mr(x,il,fl)       -> Asm.Mr(x)
	| Neg(x,il,fl)      -> Asm.Neg(x)
	| Add(x,y,il,fl)    -> Asm.Add(x,y)
	| Sub(x,y,il,fl)    -> Asm.Sub(x,y)
	| Mul(x,y,il,fl)    -> Asm.Mul(x,y)
	| Div(x,y,il,fl)    -> Asm.Div(x,y)
	| Slw(x,y,il,fl)    -> Asm.Slw(x,y)
	| Lwz(x,y,il,fl)    -> Asm.Lwz(x,y)
	| Stw(x,y,z,il,fl)  -> Asm.Stw(x,y,z)
	| FMr(x,il,fl)      -> Asm.FMr(x)
	| FNeg(x,il,fl)     -> Asm.FNeg(x)
	| FAdd(x,y,il,fl)   -> Asm.FAdd(x,y)
	| FSub(x,y,il,fl)   -> Asm.FSub(x,y)
	| FMul(x,y,il,fl)   -> Asm.FMul(x,y)
	| FDiv(x,y,il,fl)   -> Asm.FDiv(x,y)
	| Lfd(x,y,il,fl)    -> Asm.Lfd(x,y)
	| Stfd(x,y,z,il,fl) -> Asm.Stfd(x,y,z)
	| IfEq(x,y,e0,e1,il,fl) -> Asm.IfEq (x,y,box0 e0,box0 e1)
	| IfLE(x,y,e0,e1,il,fl) -> Asm.IfLE (x,y,box0 e0,box0 e1)
	| IfGE(x,y,e0,e1,il,fl) -> Asm.IfGE (x,y,box0 e0,box0 e1)
	| IfFEq(x,y,e0,e1,il,fl)-> Asm.IfFEq(x,y,box0 e0,box0 e1)
	| IfFLE(x,y,e0,e1,il,fl)-> Asm.IfFLE(x,y,box0 e0,box0 e1)
	| CallCls(x,y,z,il,fl) -> Asm.CallCls(x,y,z)
	| CallDir(x,y,z,il,fl) -> Asm.CallDir(x,y,z)
	| Save(x,y,il,fl)    -> Asm.Save(x,y)
	| Restore(x,il,fl)  -> Asm.Restore(x)


let find_furthest base regs life =
	let rec loop n res res_num =
		if n = Array.length base then
			res
		else
		(
			let tmp = find regs.(n) life in
			if res_num < tmp then
				loop (n + 1) res res_num
			else
				loop (n + 1) n tmp
		)
	in
	loop 0 0 1000000000

let find_reg regs base life target t =
	let rec loop n =
		if n = Array.length base then
			raise NoReg(target, t)
		else if regs.(n) = target then
			base.(n)
		else
			loop (n + 1)
	in
	loop 0
			

let g dest cont iregs fregs = function
	| Ans(exp,il,fl) -> g'_and_restore dest cont iregs fregs il fl exp
	|
and g'_and_restore dest cont iregs fregs il fl exp =
	try g' dest cont iregs fregs il fl exp 
	with NoReg(x, t) -> g dest cont regenv (Let((x, t), Restore(x), Ans(exp)))
and g' dest cont iregs fregs il fl = function
	| Nop | Li _ | SetL _ | Comment _ | Restore _ | FLi _ as exp -> (Ans(exp),iregs,fregs)
	| Mr(x,il,fl) -> (Ans(Mr(find_reg iregs Asm.regs il x Type.Int,il,fl),il,fl), iregs, fregs)
	| Neg(x,il,fl) -> (Ans(Neg(find_reg iregs Asm.regs il x Type.Int),il,fl,il,fl), iregs, fregs)
	| Add(x,V(y)) -> (Ans(Add(find_reg iregs Asm.regs il x Type.Int,find_reg iregs Asm.regs il y Type.Int,il,fl),il,fl),iregs,fregs)
	| Sub(x,V(y)) -> (Ans(Sub(find_reg iregs Asm.regs il x Type.Int,find_reg iregs Asm.regs il y Type.Int,il,fl),il,fl),iregs,fregs)
	| Div(x,V(y)) -> (Ans(Div(find_reg iregs Asm.regs il x Type.Int,find_reg iregs Asm.regs il y Type.Int,il,fl),il,fl),iregs,fregs)
	| Mul(x,V(y)) -> (Ans(Mul(find_reg iregs Asm.regs il x Type.Int,find_reg iregs Asm.regs il y Type.Int,il,fl),il,fl),iregs,fregs)
	| Slw(x,V(y)) -> (Ans(Slw(find_reg iregs Asm.regs il x Type.Int,find_reg iregs Asm.regs il y Type.Int,il,fl),il,fl),iregs,fregs)
	| Lwz(x,V(y)) -> (Ans(Lwz(find_reg iregs Asm.regs il x Type.Int,find_reg iregs Asm.regs il y Type.Int,il,fl),il,fl),iregs,fregs)
	| Add(x,C(y)) -> (Ans(Add(find_reg iregs Asm.regs il x Type.Int,y,il,fl),il,fl),iregs,fregs)
	| Sub(x,C(y)) -> (Ans(Sub(find_reg iregs Asm.regs il x Type.Int,y,il,fl),il,fl),iregs,fregs)
	| Div(x,C(y)) -> (Ans(Div(find_reg iregs Asm.regs il x Type.Int,y,il,fl),il,fl),iregs,fregs)
	| Mul(x,C(y)) -> (Ans(Mul(find_reg iregs Asm.regs il x Type.Int,y,il,fl),il,fl),iregs,fregs)
	| Slw(x,C(y)) -> (Ans(Slw(find_reg iregs Asm.regs il x Type.Int,y,il,fl),il,fl),iregs,fregs)
	| Lwz(x,C(y)) -> (Ans(Lwz(find_reg iregs Asm.regs il x Type.Int,y,il,fl),il,fl),iregs,fregs)
	| Slw(x,y,V(z)) -> (Ans(Slw(find_reg iregs Asm.regs il x Type.Int,find_reg iregs Asm.regs il y Type.Int,find_reg iregs Asm.regs il z Type.Int,il,fl),il,fl),iregs,fregs)
	| Slw(x,y,C(z)) -> (Ans(Slw(find_reg iregs Asm.regs il x Type.Int,find_reg iregs Asm.regs il y Type.Int,z,il,fl),il,fl),iregs,fregs)
	| Nop | Li _ | SetL _ | Comment _ | Restore _ | FLi _ as exp -> (Ans(exp),iregs,fregs)
	| FMr(x,il,fl) -> (Ans(Mr(find_reg iregs Asm.regs il x Type.Int,il,fl),il,fl), iregs, fregs)
	| FNeg(x,il,fl) -> (Ans(Neg(find_reg iregs Asm.regs fl x Type.Int),il,fl,il,fl), iregs, fregs)
	| FAdd(x,V(y)) -> (Ans(FAdd(find_reg fregs Asm.fregs fl x Type.Float,find_reg fregs Asm.fregs fl y Type.Float,il,fl),il,fl),iregs,fregs)
	| FSub(x,V(y)) -> (Ans(FSub(find_reg fregs Asm.fregs fl x Type.Float,find_reg fregs Asm.fregs fl y Type.Float,il,fl),il,fl),iregs,fregs)
	| FDiv(x,V(y)) -> (Ans(FDiv(find_reg fregs Asm.fregs fl x Type.Float,find_reg fregs Asm.fregs fl y Type.Float,il,fl),il,fl),iregs,fregs)
	| FMul(x,V(y)) -> (Ans(FMul(find_reg fregs Asm.fregs fl x Type.Float,find_reg fregs Asm.fregs fl y Type.Float,il,fl),il,fl),iregs,fregs)
	| Lfd(x,V(y)) ->  (Ans(Lfd (find_reg fregs Asm.fregs fl x Type.Float,find_reg fregs Asm.fregs fl y Type.Float,il,fl),il,fl),iregs,fregs)
	| Lfd(x,C(y)) ->  (Ans(Lfd (find_reg fregs Asm.fregs fl x Type.Float,y,il,fl),iregs,fregs)


let asm2box_h { name = Id.L(x); args = ys; fargs = zs; body = e; ret = t} =
	depth := 0 ;
	let yl = List.fold_right add0 ys [] in
	let zl = List.fold_right add0 zs [] in
	Printf.eprintf "Func\n" ;
	{ myname = Id.L(x); myargs = ys; myfargs = zs; mybody = asm0 yl zl e; myret = t}

let asm2box (Asm.Prog(data, fundefs, e)) =
	Printf.eprintf "asm2box\n" ;
	let fundefs' = List.map asm2box_h fundefs in
	Printf.eprintf "Main\n" ;
	depth := 0 ;
	let e' = asm0 [] [] e in
	Prog(data, fundefs', e')

let box2asm_h { myname = Id.L(x); myargs = ys; myfargs = zs; mybody = e; myret = t} =
	{ name = Id.L(x); args = ys; fargs = zs; body = box0 e; ret = t}

let box2asm (Prog(data, fundefs, e)) =
	Format.eprintf "box2asm\n" ;
	let fundefs' = List.map box2asm_h fundefs in
	let e' = box0 e in
	Asm.Prog(data, fundefs', e')

