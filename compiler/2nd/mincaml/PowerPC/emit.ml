open Asm

external gethi : float -> int32 = "gethi"
external getlo : float -> int32 = "getlo"

let stackset = ref S.empty (* すでに Save された変数の集合 *)
let stackmap = ref [] (* Save された変数のスタックにおける位置 *)
let save x = 
  stackset := S.add x !stackset;
  if not (List.mem x !stackmap) then
    stackmap := !stackmap @ [x]

let savef x = 
  stackset := S.add x !stackset;
  (let pad = 
       if List.length !stackmap mod 2 = 0 then [] else [Id.gentmp Type.Int] in
       stackmap := !stackmap @ pad @ [x])

let locate x = 
  let rec loc = function 
    | [] -> []
    | y :: zs when x = y -> 0 :: List.map succ (loc zs)
    | y :: zs -> List.map succ (loc zs) in
    loc !stackmap
let offset x = 1 * List.hd (locate x)
let stacksize () = align ((List.length !stackmap + 1) * 1)

let reg r = 
  if is_reg r 
  then String.sub r 1 (String.length r - 1)
  else r 


let load_label r label =
  "\tload_label\t" ^ (reg r) ^ ", ha16(" ^ label ^ ")\n" ^
  "\tload_label\t" ^ (reg r) ^ ", " ^ (reg r) ^ ", lo16(" ^ label ^ ")\n"


(* 関数呼び出しのために引数を並べ替える (register shuffling) *)
let rec shuffle sw xys = 
  (* remove identical moves *)
  let (_, xys) = List.partition (fun (x, y) -> x = y) xys in
    (* find acyclic moves *)
    match List.partition (fun (_, y) -> List.mem_assoc y xys) xys with
      | ([], []) -> []
      | ((x, y) :: xys, []) -> (* no acyclic moves; resolve a cyclic move *)
	  (y, sw) :: (x, y) :: 
	    shuffle sw (List.map (function 
				    | (y', z) when y = y' -> (sw, z)
				    | yz -> yz) xys)
      | (xys, acyc) -> acyc @ shuffle sw xys

type dest = Tail | NonTail of Id.t (* 末尾かどうかを表すデータ型 *)
let rec g oc = function (* 命令列のアセンブリ生成 *)
  | (dest, Ans (exp)) -> g' oc (dest, exp)
  | (dest, Let((x, t), exp, e)) -> g' oc (NonTail (x), exp); g oc (dest, e)
and g' oc = function (* 各命令のアセンブリ生成 *)
    (* 末尾でなかったら計算結果を dest にセット *)
  | (NonTail(_), Nop) -> ()
  | (NonTail(x), Li(i)) when i >= -32768 && i < 32768 -> 
      Printf.fprintf oc "\taddi\t%s, r0 %d\n" (reg x) i
  | (NonTail(x), Li(i)) ->
      let n = i lsr 16 in
      let m = i lxor (n lsl 16) in
      let r = reg x in
	Printf.fprintf oc "\tlis\t%s, %d\n" r n;
	Printf.fprintf oc "\tori\t%s, %s, %d\n" r r m
  | (NonTail(x), FLi(Id.L(l))) ->
  		Printf.fprintf oc "\tflwl\t%s %s\n" x l 
  | (NonTail(x), SetL(Id.L(y))) -> 
(*      let s = load_label x y in *)
      Printf.fprintf oc "\tclosure_address\t%s %s\n" x y
  | (NonTail(x), Mr(y)) when x = y -> ()
  | (NonTail(x), Mr(y)) -> Printf.fprintf oc "\taddi\t%s, %s 0\n" (reg x) (reg y)
  | (NonTail(x), Neg(y)) -> Printf.fprintf oc "\tsub\t%s r0 %s\n" (reg x) (reg y)
  | (NonTail(x), Add(y, V(z))) -> Printf.fprintf oc "\tadd\t%s, %s, %s\n" (reg x) (reg y) (reg z)
  | (NonTail(x), Add(y, C(z))) -> Printf.fprintf oc "\taddi\t%s, %s, %d\n" (reg x) (reg y) z
  | (NonTail(x), Sub(y, V(z))) -> Printf.fprintf oc "\tsub\t%s, %s, %s\n" (reg x) (reg y) (reg z)
  | (NonTail(x), Sub(y, C(z))) -> Printf.fprintf oc "\taddi\t%s, %s, %d\n" (reg x) (reg y) (0 - z)
  | (NonTail(x), Mul(y, V(z))) -> Printf.fprintf oc "\tmul\t%s, %s, %s\n" (reg x) (reg y) (reg z)
  | (NonTail(x), Mul(y, C(z))) -> Printf.fprintf oc "\tmuli\t%s, %s, %d\n" (reg x) (reg y) z
  | (NonTail(x), Div(y, V(z))) -> Printf.fprintf oc "\tdiv\t%s, %s, %s\n" (reg x) (reg y) (reg z)
  | (NonTail(x), Div(y, C(z))) -> Printf.fprintf oc "\tdivi\t%s, %s, %d\n" (reg x) (reg y) z
  | (NonTail(x), Slw(y, V(z))) -> Printf.fprintf oc "\tslw\t%s, %s, %s\n" (reg x) (reg y) (reg z)
  | (NonTail(x), Slw(y, C(z))) -> Printf.fprintf oc "\tsli\t%s, %s, %d\n" (reg x) (reg y) z
  | (NonTail(x), Lwz(y, V(z))) -> 
		Printf.fprintf oc "\tadd\t%s %s %s\n" (reg x) (reg y) (reg z) ;
		Printf.fprintf oc "\tlwi\t%s, %s 0\n" (reg x) (reg x);
  | (NonTail(x), Lwz(y, C(z))) -> Printf.fprintf oc "\tlwi\t%s, %s %d\n" (reg x) (reg y) z
  | (NonTail(_), Stw(x, y, V(z))) ->
		Printf.fprintf oc "\tadd\tr0 %s %s\n" (reg y) (reg z) ;
		Printf.fprintf oc "\tswi\t%s, r0 0\n" (reg x);
		Printf.fprintf oc "\tsub\tr0 r0 r0\n"
  | (NonTail(_), Stw(x, y, C(z))) -> Printf.fprintf oc "\tswi\t%s, %s %d\n" (reg x) (reg y) z
  | (NonTail(x), FMr(y)) when x = y -> ()
  | (NonTail(x), FMr(y)) -> Printf.fprintf oc "\tfadd\t%s, %s f0\n" (reg x) (reg y)
  | (NonTail(x), FNeg(y)) -> Printf.fprintf oc "\tfsub\t%s, f0 %s\n" (reg x) (reg y)
  | (NonTail(x), FAdd(y, z)) -> Printf.fprintf oc "\tfadd\t%s, %s, %s\n" (reg x) (reg y) (reg z)
  | (NonTail(x), FSub(y, z)) -> Printf.fprintf oc "\tfsub\t%s, %s, %s\n" (reg x) (reg y) (reg z)
  | (NonTail(x), FMul(y, z)) -> Printf.fprintf oc "\tfmul\t%s, %s, %s\n" (reg x) (reg y) (reg z)
  | (NonTail(x), FDiv(y, z)) -> Printf.fprintf oc "\tfdiv\t%s, %s, %s\n" (reg x) (reg y) (reg z)
  | (NonTail(x), Lfd(y, V(z))) ->
  		Printf.fprintf oc "\tadd\tr0 %s %s\n" (reg y) (reg z);
      Printf.fprintf oc "\tflwi\t%s, r0 0\n" (reg x);
      Printf.fprintf oc "\tsub\tr0 r0 r0\n" 
  | (NonTail(x), Lfd(y, C(z))) -> Printf.fprintf oc "\tflwi\t%s, %s %d #\n" (reg x) (reg y) z
  | (NonTail(_), Stfd(x, y, V(z))) ->
  		Printf.fprintf oc "\tadd\tr0 %s %s\n" (reg y) (reg z) ;
      Printf.fprintf oc "\tfswi\t%s, r0 0\n" (reg x);
      Printf.fprintf oc "\tsub\tr0 r0 r0\n" 
  | (NonTail(_), Stfd(x, y, C(z))) -> Printf.fprintf oc "\tfswi\t%s, %s %d\n" (reg x) (reg y) z
  | (NonTail(_), Comment(s)) -> Printf.fprintf oc "#\t%s\n" s
  (* 退避の仮想命令の実装 *)
  | (NonTail(_), Save(x, y))
      when (List.mem x allregs || x = reg_cl || x = reg_sw) && not (S.mem y !stackset) ->
      save y;
	Printf.fprintf oc "\tswi\t%s, %s %d\n" (reg x) reg_sp (offset y)
  | (NonTail(_), Save(x, y)) 
      when List.mem x allfregs && not (S.mem y !stackset) ->
      savef y;
	Printf.fprintf oc "\tfswi\t%s, %s %d\n" (reg x) reg_sp (offset y)
  | (NonTail(_), Save(x, y)) -> assert (S.mem y !stackset); ()
  (* 復帰の仮想命令の実装 *)
  | (NonTail(x), Restore(y)) when (List.mem x allregs || x = reg_cl || x = reg_sw) ->
      Printf.fprintf oc "\tlwi\t%s, %s %d\n" (reg x) reg_sp (offset y)
  | (NonTail(x), Restore(y)) ->
      assert (List.mem x allfregs);
      Printf.fprintf oc "\tflwi\t%s, %s %d #off\n" (reg x) reg_sp (offset y)
  (* 末尾だったら計算結果を第一レジスタにセット *)
  | (Tail, (Nop | Stw _ | Stfd _ | Comment _ | Save _ as exp)) ->
      g' oc (NonTail(Id.gentmp Type.Unit), exp);
      Printf.fprintf oc "\tjr\tr31\n";
  | (Tail, (Li _ | SetL _ | Mr _ | Neg _ | Add _ | Sub _ | Mul _ | Div _ | Slw _ |
            Lwz _ as exp)) -> 
      g' oc (NonTail(regs.(0)), exp);
      Printf.fprintf oc "\tjr\tr31\n";
  | (Tail, (FLi _ | FMr _ | FNeg _ | FAdd _ | FSub _ | FMul _ | FDiv _ |
            Lfd _ as exp)) ->
      g' oc (NonTail(fregs.(0)), exp);
      Printf.fprintf oc "\tjr\tr31\n";
  | (Tail, (Restore(x) as exp)) ->
      (match locate x with
	 | [i] -> g' oc (NonTail(regs.(0)), exp)
	 | [i; j] when (i + 1 = j) -> g' oc (NonTail(fregs.(0)), exp)
	 | _ -> assert false);
      Printf.fprintf oc "\tjr\tr31\n";
  | (Tail, IfEq(x, V(y), e1, e2)) ->
  		Printf.fprintf oc "\tseq\t%s, %s, %s\n" reg_branch (reg x) (reg y);
      g'_tail_if oc e1 e2 "beq" "bne"
  | (Tail, IfEq(x, C(y), e1, e2)) ->	
  		Printf.fprintf oc "\taddi\t%s, r0, %d\n" reg_branch y;
      Printf.fprintf oc "\tseq\t%s, %s, %s\n" reg_branch (reg x) reg_branch;
      g'_tail_if oc e1 e2 "beq" "bne"
  | (Tail, IfLE(x, V(y), e1, e2)) ->
      Printf.fprintf oc "\tsgt\t%s, %s, %s\n" reg_branch (reg x) (reg y);
  		Printf.fprintf oc "\tseq\t%s, %s, r0\n" reg_branch reg_branch ;
      g'_tail_if oc e1 e2 "ble" "bgt"
  | (Tail, IfLE(x, C(y), e1, e2)) ->
  		Printf.fprintf oc "\taddi\t%s, r0, %d\n" reg_branch y;
      Printf.fprintf oc "\tsgt\t%s, %s, %s\n" reg_branch (reg x) reg_branch;
  		Printf.fprintf oc "\tseq\t%s, %s, r0\n" reg_branch reg_branch;
      g'_tail_if oc e1 e2 "ble" "bgt"
  | (Tail, IfGE(x, V(y), e1, e2)) ->
      Printf.fprintf oc "\tslt\t%s, %s, %s\n" reg_branch (reg x) (reg y);
  		Printf.fprintf oc "\tseq\t%s, %s, r0\n" reg_branch reg_branch;
      g'_tail_if oc e1 e2 "bge" "blt"
  | (Tail, IfGE(x, C(y), e1, e2)) ->
  		Printf.fprintf oc "\taddi\t%s, r0, %d\n" reg_branch y;
      Printf.fprintf oc "\tslt\t%s, %s, %s\n" reg_branch (reg x) reg_branch;
  		Printf.fprintf oc "\tseq\t%s, %s, r0\n" reg_branch reg_branch;
      g'_tail_if oc e1 e2 "bge" "blt"
  | (Tail, IfFEq(x, y, e1, e2)) ->
      Printf.fprintf oc "\tfeq\t%s, %s, %s\n" reg_branch (reg x) (reg y);
      g'_tail_if oc e1 e2 "beq" "bne"
  | (Tail, IfFLE(x, y, e1, e2)) ->
(*
      Printf.fprintf oc "\tflt\tr21, %s, %s\n" (reg x) (reg y);
*)
			Printf.fprintf oc "\tfgt\t%s %s %s\n" reg_branch (reg x) (reg y ) ;
			Printf.fprintf oc "\tseq\t%s %s r0\n" reg_branch reg_branch;
      g'_tail_if oc e1 e2 "ble" "bgt"
  | (NonTail(z), IfEq(x, V(y), e1, e2)) ->
      Printf.fprintf oc "\tseq\t%s, %s, %s\n" reg_branch (reg x) (reg y);
      g'_non_tail_if oc (NonTail(z)) e1 e2 "beq" "bne"
  | (NonTail(z), IfEq(x, C(y), e1, e2)) ->
  		Printf.fprintf oc "\taddi\t%s, r0, %d\n" reg_branch y;
      Printf.fprintf oc "\tseq\t%s, %s, %s\n" reg_branch (reg x) reg_branch ;
      g'_non_tail_if oc (NonTail(z)) e1 e2 "beq" "bne"
  | (NonTail(z), IfLE(x, V(y), e1, e2)) ->
      Printf.fprintf oc "\tsgt\t%s, %s, %s\n" reg_branch (reg x) (reg y);
  		Printf.fprintf oc "\tseq\t%s, %s, r0\n" reg_branch reg_branch;
      g'_non_tail_if oc (NonTail(z)) e1 e2 "ble" "bgt"
  | (NonTail(z), IfLE(x, C(y), e1, e2)) ->
  		Printf.fprintf oc "\taddi\t%s, r0, %d\n" reg_branch y;
      Printf.fprintf oc "\tsgt\t%s, %s, %s\n" reg_branch (reg x) reg_branch;
  		Printf.fprintf oc "\tseq\t%s, %s, r0\n" reg_branch reg_branch;
      g'_non_tail_if oc (NonTail(z)) e1 e2 "ble" "bgt"
  | (NonTail(z), IfGE(x, V(y), e1, e2)) ->
      Printf.fprintf oc "\tslt\t%s, %s, %s\n" reg_branch (reg x) (reg y);
  		Printf.fprintf oc "\tseq\t%s, %s, r0\n" reg_branch reg_branch;
      g'_non_tail_if oc (NonTail(z)) e1 e2 "bge" "blt"
  | (NonTail(z), IfGE(x, C(y), e1, e2)) ->
  		Printf.fprintf oc "\taddi\t%s, r0, %d\n" reg_branch y;
      Printf.fprintf oc "\tslt\t%s, %s, %s\n" reg_branch (reg x) reg_branch;
  		Printf.fprintf oc "\tseq\t%s %s, r0\n" reg_branch reg_branch; 
      g'_non_tail_if oc (NonTail(z)) e1 e2 "bge" "blt"
  | (NonTail(z), IfFEq(x, y, e1, e2)) ->
      Printf.fprintf oc "\tfeq\t%s, %s, %s\n" reg_branch (reg x) (reg y);
      g'_non_tail_if oc (NonTail(z)) e1 e2 "beq" "bne"
  | (NonTail(z), IfFLE(x, y, e1, e2)) ->
			Printf.fprintf oc "\tfgt\t%s %s %s\n" reg_branch (reg x) (reg y ) ;
			Printf.fprintf oc "\tseq\t%s %s r0\n" reg_branch reg_branch;
      g'_non_tail_if oc (NonTail(z)) e1 e2 "ble" "bgt"
  (* 関数呼び出しの仮想命令の実装 *)
  | (Tail, CallCls(x, ys, zs)) -> (* 末尾呼び出し *)
      g'_args oc [(x, reg_cl)] ys zs;
      Printf.fprintf oc "\tlwi\t%s, %s 0\n" (reg reg_sw) (reg reg_cl);
      Printf.fprintf oc "\tjr\t%s\n" (reg reg_sw)
  | (Tail, CallDir(Id.L(x), ys, zs)) -> (* 末尾呼び出し *)
      g'_args oc [] ys zs;
      Printf.fprintf oc "\tjl\t%s\n" x
  | (NonTail(a), CallCls(x, ys, zs)) ->
		g'_args oc [(x, reg_cl)] ys zs;
		let ss = stacksize () in
		Printf.fprintf oc "\tswi\tr31 %s %d\n" reg_sp (ss - 1);
		Printf.fprintf oc "\taddi\t%s, %s, %d\n" reg_sp reg_sp ss;

		Printf.fprintf oc "\tlwi\t%s, %s 0\n" reg_sw (reg reg_cl);
		Printf.fprintf oc "\tclosure_jump\t%s\n" reg_sw;
(*
		Printf.fprintf oc "\tlwi\tr31 %s 0\n" (reg reg_cl);
		Printf.fprintf oc "\tclosure_jump\tr31\n" ;
*)
		Printf.fprintf oc "\taddi\t%s, %s, %d\n" reg_sp reg_sp (0 - ss);
		Printf.fprintf oc "\tlwi\tr31, %s %d\n" reg_sp (ss - 1);
		(if (List.mem a allregs || a = reg_cl || a = reg_sw) && a <> regs.(0) then 
		   Printf.fprintf oc "\taddi\t%s, %s 0 #\n" (reg a) (reg regs.(0)) 
		 else if List.mem a allfregs && a <> fregs.(0) then 
		   Printf.fprintf oc "\tfadd\t%s, %s f0\n" (reg a) (reg fregs.(0)))
  | (NonTail(a), CallDir(Id.L(x), ys, zs)) -> 
		g'_args oc [] ys zs;
		let ss = stacksize () in
		Printf.fprintf oc "\tswi\tr31 %s %d\n" reg_sp (ss - 1) ;
		Printf.fprintf oc "\taddi\t%s, %s, %d\n" reg_sp reg_sp ss;
		Printf.fprintf oc "\tjal\t%s\n" x;
		Printf.fprintf oc "\taddi\t%s, %s, %d\n" reg_sp reg_sp (0 - ss);
		Printf.fprintf oc "\tlwi\tr31 %s %d\n" reg_sp (ss - 1);
		(if (List.mem a allregs || a = reg_cl || a = reg_sw ) && a <> regs.(0) then
		   Printf.fprintf oc "\taddi\t%s, %s 0 ##\n" (reg a) (reg regs.(0))
		 else if List.mem a allfregs && a <> fregs.(0) then
		   Printf.fprintf oc "\tfadd\t%s, %s f0\n" (reg a) (reg fregs.(0)))
and g'_tail_if oc e1 e2 b bn = 
  let b_else = Id.genid (b ^ "_else") in
    Printf.fprintf oc "\tjeql\t%s, r0, %s\n" reg_branch b_else;
    let stackset_back = !stackset in
      g oc (Tail, e1);
      Printf.fprintf oc "%s:\n" b_else;
      stackset := stackset_back;
      g oc (Tail, e2)
and g'_non_tail_if oc dest e1 e2 b bn = 
  let b_else = Id.genid (b ^ "_else") in
  let b_cont = Id.genid (b ^ "_cont") in
    Printf.fprintf oc "\tjeql\t%s, r0, %s\n" reg_branch b_else;
    let stackset_back = !stackset in
      g oc (dest, e1);
      let stackset1 = !stackset in
	Printf.fprintf oc "\tjl\t%s\n" b_cont;
	Printf.fprintf oc "%s:\n" b_else;
	stackset := stackset_back;
	g oc (dest, e2);
	Printf.fprintf oc "%s:\n" b_cont;
	let stackset2 = !stackset in
	  stackset := S.inter stackset1 stackset2
and g'_args oc x_reg_cl ys zs = 
	let (i, yrs) = 
		List.fold_left 
		(fun (i, yrs) y -> (i + 1, (y, regs.(i)) :: yrs)) 
		(0, x_reg_cl) ys in
	List.iter
		(fun (y, r) -> Printf.fprintf oc "\taddi\t%s, %s 0 #\n" (reg r) (reg y))
		(shuffle reg_sw yrs);
	let (d, zfrs) = 
		List.fold_left
		(fun (d, zfrs) z -> (d + 1, (z, fregs.(d)) :: zfrs))
		(0, []) zs in
	List.iter
		(fun (z, fr) -> Printf.fprintf oc "\tfadd\t%s, %s f0\n" (reg fr) (reg z))
		(shuffle reg_fsw zfrs)

let h oc { name = Id.L(x); args = _; fargs = _; body = e; ret = _ } =
  Printf.fprintf oc "%s:\n" x;
  stackset := S.empty;
  stackmap := [];
  g oc (Tail, e)

let f oc (Prog(data, fundefs, e)) =
  Format.eprintf "generating assembly...@.";
  Printf.fprintf oc "\tjal __start__\n" ;
  Printf.fprintf oc "\tASITIS 00000000000000010101111110010000\n" ;
(*  Printf.fprintf oc "\tASITIS 00000000000000010000000000000000\n" ; *)
  Printf.fprintf oc "__start__: # hoge\n" ;
  Printf.fprintf oc "\taddi\t%s %s 300\n" reg_sp reg_sp ;
  Printf.fprintf oc "\tmul\t%s %s %s\n" reg_sp reg_sp reg_sp ;
	Printf.fprintf oc "\taddi\t%s %s 330\n" reg_hp reg_hp ;
	Printf.fprintf oc "\tmul\t%s %s %s\n" reg_hp reg_hp reg_hp;	
  (if data <> [] then
    (
    	List.iter (fun (Id.L(x), d) -> Printf.fprintf oc "\tINIT\t%f %s\n" d x) data
    )
  );
	Printf.fprintf oc "\tjal\t_min_caml_start\n" ;
  List.iter (fun fundef -> h oc fundef) fundefs;
  Printf.fprintf oc "_min_caml_start: # main entry point\n";
  Printf.fprintf oc "   # main program start\n";
  stackset := S.empty;
  stackmap := [];
  g oc (NonTail("_R_0"), e);
  
  Printf.fprintf oc "\tjl\tAsmToBinStopLabel\n" ;
  Printf.fprintf oc "   # main program end\n"	

