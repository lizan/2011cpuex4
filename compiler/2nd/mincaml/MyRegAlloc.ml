open Asm

let importance = ref (M.add "INIT" 1 M.empty)


let rec findWorst regenv all = function
	| [] -> "MAGIC_STRING"
	| hd :: tl ->
	(
		let res = findWorst regenv all tl in
		if (not (is_reg hd) && try List.mem (M.find hd regenv) all with Not_found -> false) then
		(	if res = "MAGIC_STRING" then
				"MAGIC_STRING2"
			else if res ="MAGIC_STRING2" then
				hd
			else if not (M.mem hd !importance) then
				hd
			else if not (M.mem res !importance) then
				res
			else if (M.find res !importance) < (M.find hd !importance) then
				hd
			else
				res
		)
		else
			res
	)
	

(* for register coalescing *)
(* [XXX] Callがあったら、そこから先は無意味というか逆効果なので追わない。
         そのために「Callがあったかどうか」を返り値の第1要素に含める。 *)
let rec target' src (dest, t) = function
  | Mr(x) when x = src && is_reg dest ->
      assert (t <> Type.Unit);
      assert (t <> Type.Float);
      false, [dest]
  | FMr(x) when x = src && is_reg dest ->
      assert (t = Type.Float);
      false, [dest]
  | IfEq(_, _, e1, e2) | IfLE(_, _, e1, e2) | IfGE(_, _, e1, e2)
  | IfFEq(_, _, e1, e2) | IfFLE(_, _, e1, e2) ->
      let c1, rs1 = target src (dest, t) e1 in
      let c2, rs2 = target src (dest, t) e2 in
      c1 && c2, rs1 @ rs2
  | CallCls(x, ys, zs) ->
      true, (target_args src regs 0 ys @
	     target_args src fregs 0 zs @
             if x = src then [reg_cl] else [])
  | CallDir(_, ys, zs) ->
      true, (target_args src regs 0 ys @
	     target_args src fregs 0 zs)
  | _ -> false, []
and target src dest = function (* register targeting (caml2html: regalloc_target) *)
  | Ans(exp) -> target' src dest exp
  | Let(xt, exp, e) ->
      let c1, rs1 = target' src xt exp in
      if c1 then true, rs1 else
      let c2, rs2 = target src dest e in
      c2, rs1 @ rs2
and target_args src all n = function (* auxiliary function for Call *)
  | [] -> []
  | y :: ys when src = y -> all.(n) :: target_args src all (n + 1) ys
  | _ :: ys -> target_args src all (n + 1) ys

type alloc_result = (* allocにおいてspillingがあったかどうかを表すデータ型 *)
  | Alloc of Id.t (* allocated register *)
  | Spill of Id.t (* spilled variable *)
let rec alloc dest cont regenv x t =
  (* allocate a register or spill a variable *)
  assert (not (M.mem x regenv));
  let all =
    match t with
    | Type.Unit -> ["%r0"] (* dummy *)
    | Type.Float -> allfregs
    | _ -> allregs in
  if all = ["%r0"] then Alloc("%r0") else (* [XX] ad hoc optimization *)
  if is_reg x then Alloc(x) else
  let free = fv cont in
  try
    let (c, prefer) = target x dest cont in
    let live = (* 生きているレジスタ *)
      List.fold_left
        (fun live y ->
	  if is_reg y then S.add y live else
          try S.add (M.find y regenv) live
          with Not_found -> live)
        S.empty
        free in
    let r = (* そうでないレジスタを探す *)
      List.find
        (fun r -> not (S.mem r live))
        (prefer @ all) in
    (* Format.eprintf "allocated %s to %s@." x r; *)
    Alloc(r)
  with Not_found ->
    Format.eprintf "register allocation failed for %s@." x;
(*
    let y = findWorst regenv all (List.rev free) in
*)
    let y = (* 型の合うレジスタ変数を探す *)
      List.find
        (fun y -> not (is_reg y) && try List.mem (M.find y regenv) all with Not_found -> false)
        (List.rev free) in

    Format.eprintf "spilling %s from %s@." y (M.find y regenv);
    Spill(y)

(* auxiliary function for g and g'_and_restore *)
let add x r regenv =
  if is_reg x then (assert (x = r); regenv) else
  M.add x r regenv

(* auxiliary functions for g' *)
exception NoReg of Id.t * Type.t
let find x t regenv =
  if is_reg x then x else
  try M.find x regenv
  with Not_found -> raise (NoReg(x, t))
let find' x' regenv =
  match x' with
  | V(x) -> V(find x Type.Int regenv)
  | c -> c

let rec g dest cont regenv = function (* 命令列のレジスタ割り当て (caml2html: regalloc_g) *)
	| Ans(exp) -> g'_and_restore dest cont regenv exp
	| Let((x, t) as xt, exp, e) ->
		assert (not (M.mem x regenv));
		let cont' = concat e dest cont in
		let (e1', regenv1) = g'_and_restore xt cont' regenv exp in
			(match alloc dest cont' regenv1 x t with
				| Spill(y) ->
					let r = M.find y regenv1 in
					let (e2', regenv2) = g dest cont (add x r (M.remove y regenv1)) e in
					let save = try Save(M.find y regenv, y) with Not_found -> Nop in	    
					(seq(save, concat e1' (r, t) e2'), regenv2)
				| Alloc(r) ->
					let (e2', regenv2) = g dest cont (add x r regenv1) e in
					(concat e1' (r, t) e2', regenv2))
and g'_and_restore dest cont regenv exp = (* 使用される変数をスタックからレジスタへRestore (caml2html: regalloc_unspill) *)
  try g' dest cont regenv exp
  with NoReg(x, t) ->
    ((* Format.eprintf "restoring %s@." x; *)
     g dest cont regenv (Let((x, t), Restore(x), Ans(exp))))
and g' dest cont regenv = function (* 各命令のレジスタ割り当て (caml2html: regalloc_gprime) *)
  | Nop | Li _ | SetL _ | Comment _ | Restore _ | FLi _ as exp -> (Ans(exp), regenv)
  | Mr(x) -> (Ans(Mr(find x Type.Int regenv)), regenv)
  | Neg(x) -> (Ans(Neg(find x Type.Int regenv)), regenv)
  | Add(x, y') -> (Ans(Add(find x Type.Int regenv, find' y' regenv)), regenv)
  | Sub(x, y') -> (Ans(Sub(find x Type.Int regenv, find' y' regenv)), regenv)
  | Mul(x, y') -> (Ans(Mul(find x Type.Int regenv, find' y' regenv)), regenv)
  | Div(x, y') -> (Ans(Div(find x Type.Int regenv, find' y' regenv)), regenv)
  | Slw(x, y') -> (Ans(Slw(find x Type.Int regenv, find' y' regenv)), regenv)
  | Lwz(x, y') -> (Ans(Lwz(find x Type.Int regenv, find' y' regenv)), regenv)
  | Stw(x, y, z') -> (Ans(Stw(find x Type.Int regenv, find y Type.Int regenv, find' z' regenv)), regenv)
  | FMr(x) -> (Ans(FMr(find x Type.Float regenv)), regenv)
  | FNeg(x) -> (Ans(FNeg(find x Type.Float regenv)), regenv)
  | FAdd(x, y) -> (Ans(FAdd(find x Type.Float regenv, find y Type.Float regenv)), regenv)
  | FSub(x, y) -> (Ans(FSub(find x Type.Float regenv, find y Type.Float regenv)), regenv)
  | FMul(x, y) -> (Ans(FMul(find x Type.Float regenv, find y Type.Float regenv)), regenv)
  | FDiv(x, y) -> (Ans(FDiv(find x Type.Float regenv, find y Type.Float regenv)), regenv)
  | Lfd(x, y') -> (Ans(Lfd(find x Type.Int regenv, find' y' regenv)), regenv)
  | Stfd(x, y, z') -> (Ans(Stfd(find x Type.Float regenv, find y Type.Int regenv, find' z' regenv)), regenv)
  | IfEq(x, y', e1, e2) as exp -> g'_if dest cont regenv exp (fun e1' e2' -> IfEq(find x Type.Int regenv, find' y' regenv, e1', e2')) e1 e2
  | IfLE(x, y', e1, e2) as exp -> g'_if dest cont regenv exp (fun e1' e2' -> IfLE(find x Type.Int regenv, find' y' regenv, e1', e2')) e1 e2
  | IfGE(x, y', e1, e2) as exp -> g'_if dest cont regenv exp (fun e1' e2' -> IfGE(find x Type.Int regenv, find' y' regenv, e1', e2')) e1 e2
  | IfFEq(x, y, e1, e2) as exp -> g'_if dest cont regenv exp (fun e1' e2' -> IfFEq(find x Type.Float regenv, find y Type.Float regenv, e1', e2')) e1 e2
  | IfFLE(x, y, e1, e2) as exp -> g'_if dest cont regenv exp (fun e1' e2' -> IfFLE(find x Type.Float regenv, find y Type.Float regenv, e1', e2')) e1 e2
  | CallCls(x, ys, zs) as exp -> g'_call dest cont regenv exp (fun ys zs -> CallCls(find x Type.Int regenv, ys, zs)) ys zs
  | CallDir(l, ys, zs) as exp -> g'_call dest cont regenv exp (fun ys zs -> CallDir(l, ys, zs)) ys zs
  | Save(x, y) -> assert false
and g'_if dest cont regenv exp constr e1 e2 = (* ifのレジスタ割り当て (caml2html: regalloc_if) *)
	let (e1', regenv1) = g dest cont regenv e1 in
	let (e2', regenv2) = g dest cont regenv e2 in
	let regenv' = (* 両方に共通のレジスタ変数だけ利用 *)
		List.fold_left
			(fun regenv' x ->
				try
					if is_reg x then regenv' else
						let r1 = M.find x regenv1 in
						let r2 = M.find x regenv2 in
						if r1 <> r2 then regenv' else M.add x r1 regenv'
				with Not_found -> regenv')
			M.empty (fv cont) in
	(List.fold_left
		(fun e x ->
			if x = fst dest || not (M.mem x regenv) || M.mem x regenv' then e else
			seq(Save(M.find x regenv, x), e)) (* そうでない変数は分岐直前にセーブ *)
		(Ans(constr e1' e2')) (fv cont), regenv')
and g'_call dest cont regenv exp constr ys zs = (* 関数呼び出しのレジスタ割り当て (caml2html: regalloc_call) *)
  (List.fold_left
     (fun e x ->
       if x = fst dest || not (M.mem x regenv) then e else
       seq(Save(M.find x regenv, x), e))
     (Ans(constr
	    (List.map (fun y -> find y Type.Int regenv) ys)
	    (List.map (fun z -> find z Type.Float regenv) zs)))
     (fv cont),
   M.empty)


let good_cnt = ref 0
let bad_cnt = ref 100000


let count_numS s =
	let cnt = ref 0 in
	S.iter (fun e -> cnt := !cnt + 1) s ;
	!cnt

let count_numM m =
	let cnt = ref 0 in
	M.iter (fun k v -> cnt := !cnt + 1) m ;
	!cnt






let get  x  = S.add x S.empty
let get' x' =
	match x' with
	| V(x) -> get x
	| _ -> S.empty
	
let rec get_first = function
	| [] -> S.empty
	| hd :: tl -> S.add hd  (get_first tl)

let rec analyze dest cont e edgesI edgesF =
	let index = ref "MAGIC_STRING" in
	let index_type = ref Type.Int in
	let (surviveI,surviveF) =
	(
		match e with
		| Ans(exp) -> analyze' dest cont exp edgesI edgesF
		| Let((x, y), exp, e') -> 
		(	
			index := x ; 
			index_type := y ; 
			let (sI0, sF0) = analyze  dest cont e' edgesI edgesF in
			let (sI1, sF1) = analyze' dest cont exp  edgesI edgesF in
			(S.union sI0 sI1, S.union sF0 sF1)
		)
	) 
	in
	S.iter (fun k -> if M.mem k !edgesI then () else edgesI := M.add k (ref S.empty) !edgesI) surviveI ;
	S.iter (fun k -> if M.mem k !edgesF then () else edgesF := M.add k (ref S.empty) !edgesF) surviveF ;
	(S.iter (fun k0 -> (S.iter (fun k1 ->
		(M.find k0 !edgesI) := S.add k1 !(M.find k0 !edgesI) ; 
		(M.find k1 !edgesI) := S.add k0 !(M.find k1 !edgesI)
		)) surviveI) surviveI ) ;
	(S.iter (fun k0 -> (S.iter (fun k1 ->
		(M.find k0 !edgesF) := S.add k1 !(M.find k0 !edgesF) ; 
		(M.find k1 !edgesF) := S.add k0 !(M.find k1 !edgesF)
		)) surviveF) surviveF ) ;
	(
		if !index = "MAGIC_STRING" then
			(surviveI, surviveF)
		else 
		(
			match !index_type with
			| Type.Int -> (S.remove !index surviveI, surviveF)
			| _ -> (surviveI, S.remove !index surviveF)
		)
	)
and analyze' dest cont e edgesI edgesF =
	match e with
	| Nop | Li _ | SetL _ | Comment _ | Restore _ | FLi _ -> (S.empty, S.empty)
	| Mr(x) | Neg(x) -> (S.add x S.empty, S.empty)
	| FMr(x) | FNeg(x) -> (S.empty, S.add x S.empty)
	| Add(x, y') | Sub(x, y') | Mul(x, y') | Div(x, y') | Lfd(x, y')
	| Slw(x, y') | Lwz(x, y') -> (S.union (get x) (get' y'), S.empty)
	| Stw(x, y, z') | Stfd(x, y, z') -> (S.union (get x) (S.union (get y) (get' z')), S.empty)
	| FAdd(x, y) | FSub(x, y) | FMul(x, y) | FDiv(x, y) -> (S.empty, S.union (get x) (get y))
	| IfEq(x, y', e1, e2) | IfLE(x, y', e1, e2) | IfGE(x, y', e1, e2) 
		-> 
		(
			let (sI0, sF0) = analyze dest cont e1 edgesI edgesF in
			let (sI1, sF1) = analyze dest cont e2 edgesI edgesF in
			(S.union (get x) (S.union (get' y') (S.union sI0 sI1)), S.union sF0 sF1)
		)
	| IfFEq(x, y, e1, e2) | IfFLE(x, y, e1, e2) 
		-> 
		(
			let (sI0, sF0) = analyze dest cont e1 edgesI edgesF in
			let (sI1, sF1) = analyze dest cont e2 edgesI edgesF in
			(S.union (get x) (S.union (get  y ) (S.union sI0 sI1)), S.union sF0 sF1)
		)
	| CallCls(_, ys, zs) | CallDir(_, ys, zs) -> (get_first ys, get_first zs)
	| _ -> (S.empty, S.empty)

let myAnalyze dest cont regenv e =
Format.eprintf "START myanalyze@." ;
	importance := M.empty ;
	good_cnt := 0 ;
	bad_cnt := 0 ;
	let edgesI = ref M.empty in
	let edgesF = ref M.empty in
	ignore(analyze dest cont e edgesI edgesF) ;
Format.eprintf "ok analyzied@." ; 
	(
		while not (M.is_empty !edgesI) do
Format.eprintf "remain : %d@." (count_numM !edgesI) ;
			let updated = ref 1 in
			while !updated = 1 do
				updated := 0 ;
				let index = ref "MAGIC_STRING" in
				M.iter (fun k v -> if (count_numS !v) < 25 then index := k else () ) !edgesI ;
				(
					if !index = "MAGIC_STRING" then
						()
					else
					(
						updated := 1 ;
						M.iter (fun k v -> try ( v := (S.remove !index !v) ) with Not_found -> () ) !edgesI ;
						edgesI := (M.remove !index !edgesI) ;
						importance := (M.add !index !good_cnt !importance) ;
						good_cnt := !good_cnt + 1 
					)
				)
			done ;
			if (count_numM !edgesI) = 0 then
				()
			else
			(
				let index = ref "MAGIC_STRING" in
				M.iter (fun k v -> index := k ) !edgesI ;
				importance := M.add !index !bad_cnt !importance ;
				M.iter (fun k v -> try ( v := (S.remove !index !v) ) with Not_found -> () ) !edgesI ;
				bad_cnt := !bad_cnt + 1 ;
				edgesI := M.remove !index !edgesI
			)
		done
	) ;
	(
		while not (M.is_empty !edgesF) do
Format.eprintf "remain : %d@." (count_numM !edgesF) ;
			let updated = ref 1 in
			while !updated = 1 do
				updated := 0 ;
				let index = ref "MAGIC_STRING" in
				M.iter (fun k v -> if (count_numS !v) < 29 then index := k else () ) !edgesF ;
				(
					if !index = "MAGIC_STRING" then
						()
					else
					(
						updated := 1 ;
						M.iter (fun k v -> try ( v := (S.remove !index !v) ) with Not_found -> () ) !edgesF ;
						edgesF := (M.remove !index !edgesF) ;
						importance := (M.add !index !good_cnt !importance) ;
						good_cnt := !good_cnt + 1 
					)
				)
			done ;
			if (count_numM !edgesF) = 0 then
				()
			else
			(
				let index = ref "MAGIC_STRING" in
				M.iter (fun k v -> index := k ) !edgesF ;
				importance := M.add !index !bad_cnt !importance ;
				M.iter (fun k v -> try ( v := (S.remove !index !v) ) with Not_found -> () ) !edgesF ;
				bad_cnt := !bad_cnt + 1 ;
				edgesF := M.remove !index !edgesF
			)
		done
	) ;
Format.eprintf "end my analyze@." ;
	g dest cont regenv e

	

let h { name = Id.L(x); args = ys; fargs = zs; body = e; ret = t } = (* 関数のレジスタ割り当て (caml2html: regalloc_h) *)
  let regenv = M.add x reg_cl M.empty in
  let (i, arg_regs, regenv) =
    List.fold_left
      (fun (i, arg_regs, regenv) y ->
        let r = regs.(i) in
        (i + 1,
	 arg_regs @ [r],
	 (assert (not (is_reg y));
	  M.add y r regenv)))
      (0, [], regenv)
      ys in
  let (d, farg_regs, regenv) =
    List.fold_left
      (fun (d, farg_regs, regenv) z ->
        let fr = fregs.(d) in
        (d + 1,
	 farg_regs @ [fr],
	 (assert (not (is_reg z));
	  M.add z fr regenv)))
      (0, [], regenv)
      zs in
  let a =
    match t with
    | Type.Unit -> Id.gentmp Type.Unit
    | Type.Float -> fregs.(0)
    | _ -> regs.(0) in
  let (e', regenv') = myAnalyze (a, t) (Ans(Mr(a))) regenv e in
  { name = Id.L(x); args = arg_regs; fargs = farg_regs; body = e'; ret = t }

let f (Prog(data, fundefs, e)) = (* プログラム全体のレジスタ割り当て (caml2html: regalloc_f) *)
  Format.eprintf "register allocation: may take some time (up to a few minutes, depending on the size of functions)@.";
  let fundefs' = List.map h fundefs in
  let e', regenv' = myAnalyze (Id.gentmp Type.Unit, Type.Unit) (Ans(Nop)) M.empty e in
  Prog(data, fundefs', e')
