





(**************** library start *******************)

let rec fiszero x = ( x = 0.0 ) in
let rec fispos x = ( 0.0 < x ) in
let rec fisneg x = ( x < 0.0 ) in
let rec fneg x = ( 0.0 -. x ) in
let rec fsqr x = ( x *. x ) in 
let rec fless x y = ( x < y ) in
let rec fhalf x = x *. 0.5 in
let rec fabs x = if fispos x then x else fneg x in 

let rec float_of_int x = 0.0 in (* yet *)
let rec int_of_float x = 0 in (* yet *)



let rec print_int x = () in (* yet *)
let rec print_char x = () in (* yet *)
let rec read_float x = 0.0 in (* yet *)
let rec read_int x = 0 in (* yet *)



(* bad implementation *)
(* floor *)
(* sqrt *)
(* sin cos atan *)
let rec floor_rec n low high target =
	if n = 0 then
		low
	else
		let mid =(low +. high) *. 0.5 in
		if target < mid then
			floor_rec (n - 1) low mid target 
		else
			floor_rec (n - 1) mid high target in


let rec floor x = floor_rec 11 (0.0 -. 1024.0) 1024.0 x in


let rec sqrt_rec n low high target =
	if n = 100 then 
		low
	else
		let mid = (low +. high) *. 0.5 in
		let tmp = mid *. mid in
		if tmp < target then
			sqrt_rec (n + 1) mid high target
		else
			sqrt_rec (n + 1) low mid target in

let rec sqrt x = sqrt_sub 0 0.0 x x in



let my_pi = 3.14159265358979323846264338327950288 in

let rec cos_rec x n fn sum =
	if n = 100 then 
		0.0 
	else
		sum -. (cos_rec x (n + 1) (fn +. 2.0) (sum *. x *. x /. (fn +. 1.0) /. (fn +. 2.0))) in

let rec cos x = cos_rec x 0 0.0 1.0 in

let rec sin x = fneg ( cos ( x +. my_pi *. 0.5 ) ) in

let rec atan_rec n low high target =
	if n = 100 then
		low
	else
		let mid = (low +. high) *. 0.5 in
		let tmp = (sin mid) /. (cos mid) in
		if tmp < target then 
			atan_rec (n + 1) mid high target
		else
			atan_rec (n + 1) low mid target in

let rec atan x = atan_rec 0 (fneg my_pi) my_pi x in


(**************** library end **********************)






