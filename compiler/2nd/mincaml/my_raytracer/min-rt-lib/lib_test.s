	.data
	.literal8
	.align 3
l.52:	 # 3.000000
	.long	0
	.long	1074266112
	.align 3
l.51:	 # 0.000000
	.long	0
	.long	0
	.text
	.globl  _min_caml_start
	.align 2
fiszero.5:
	lis	r31, ha16(l.51)
	addi	r31, r31, lo16(l.51)
	lfd	f1, 0(r31)
	fcmpu	cr7, f0, f1
	bne	cr7, beq_else.56
	li	r2, 1
	blr
beq_else.56:
	li	r2, 0
	blr
print_int.22:
	blr
_min_caml_start: # main entry point
	mflr	r0
	stmw	r30, -8(r1)
	stw	r0, 8(r1)
	stwu	r1, -96(r1)
   # main program start
	lis	r31, ha16(l.52)
	addi	r31, r31, lo16(l.52)
	lfd	f0, 0(r31)
	stfd	f0, 0(r3)
	mflr	r31
	stw	r31, 12(r3)
	addi	r3, r3, 16
	bl	fiszero.5
	subi	r3, r3, 16
	lwz	r31, 12(r3)
	mtlr	r31
	lfd	f0, 0(r3)
	mflr	r31
	stw	r31, 12(r3)
	addi	r3, r3, 16
	bl	print_int.22
	subi	r3, r3, 16
	lwz	r31, 12(r3)
	mtlr	r31
   # main program end
	lwz	r1, 0(r1)
	lwz	r0, 8(r1)
	mtlr	r0
	lmw	r30, -8(r1)
	blr
