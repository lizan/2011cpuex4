(**************** library start *******************)

let rec fiszero x = ( x = 0.0 ) in
let rec fispos x = ( 0.0 < x ) in
let rec fisneg x = ( x < 0.0 ) in
let rec fneg x = ( 0.0 -. x ) in
let rec fsqr x = ( x *. x ) in 
let rec fless x y = ( x < y ) in
let rec float_of_int x = 0.0 in (* yet *)
let rec int_of_float x = 0 in (* yet *)
let rec print_int x = () in (* yet *)
let rec print_char x = () in (* yet *)
let rec floor x = 0.0 in (* yet *)
let rec fhalf x = 0.0 in (* yet *)
let rec fabs x = 0.0 in (* yet *)
let rec sqrt x = 0.0 in (* yet *)
let rec cos x = 0.0 in (* yet *)
let rec sin x = 0.0 in (* yet *)
let rec atan x = 0.0 in (* yet *)
let rec read_float x = 0.0 in (* yet *)
let rec read_int x = 0 in (* yet *)

(**************** library end **********************)


let a = 3.0 in
let b = 3.0 in
let c = fiszero a in
print_int ( a )
