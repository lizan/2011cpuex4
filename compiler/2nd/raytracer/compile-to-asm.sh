#!/bin/sh


cd ../
make
cd raytracer

cd output
rm -f min-rt.s debug2.dat debug16.dat min-rt.bin
cd ../


cd min-rt
../.././min-caml min-rt 
cat min-rt.s >> ../output/min-rt.s
cat min-rt-lib/Komaki/create_array.s >> ../output/min-rt.s
rm -f min-rt.s kNormal_output.dat syntax_output.dat
cd ../


cd ../
make clean
cd raytracer


