

min_caml_read_float_sub:
	read r29
	addi r28 r0 46
	jeql r28 r29 min_caml_read_float_comma
	
	addi r28 r0  32
	jeql r29 r28 min_caml_read_int_sub_exit
	addi r28 r0  10
	jeql r29 r28 min_caml_read_int_sub_exit
	jeql r29 r0  min_caml_read_int_sub_exit
	
	mul  r28 r28 r26
	swi  r31 r3  0
	swi  r29 r3  4
	addi r3  r3  8
	jal  min_caml_read_float_sub
	addi r3  r3  -8
	lwi  r29 r3  4
	lwi  r31 r3  0
	addi r29 r29 -48
	mul  r29 r29 r25
	add  r27 r27 r29
	addi r29 r0  10
	mul  r25 r25 r29
	jr r31
	
min_caml_read_float_comma:
	addi r26 r0 10
	jl min_caml_read_float_sub
min_caml_read_float_exit:
	addi r27 r27 0
	addi r25 r25 1
	jr   r31
	
	
min_caml_read_float:
	swi  r31 r3  0
	addi r3  r3  4
	
		jal min_caml_read_float_sub
		swi  r25 r3  0
		addi r3  r3  4
		
			addi r2  r27 0
			jal min_caml_float_of_int
		
		addi r3 r3 -4
		fswi f1  r3  0
		addi r3  r3  4

			jal min_caml_float_of_int
		
		addi r3  r3  -4
		flwi f29 r3  0
		fadd f1  f1  f29
	
	addi r3  r3  -4
	lwi  r31 r3  0
	jr   r31
	
	
