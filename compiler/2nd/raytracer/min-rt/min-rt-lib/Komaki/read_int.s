#min_caml_read_int tested


min_caml_read_int_sub:
	read r29
	addi r28 r0  32
	jeql r29 r28 min_caml_read_int_sub_exit
	addi r28 r0  10
	jeql r29 r28 min_caml_read_int_sub_exit
	jeql r29 r0  min_caml_read_int_sub_exit
	
	swi  r29 r3  0
	swi  r31 r3  4
	addi r3  r3  8
	jal  min_caml_read_int_sub
	addi r3  r3  -8
	lwi  r31 r3  4
	lwi  r29 r3  0
	
	addi r29 r29 -48
	mul  r29 r29 r27
	add  r2  r2  r29
	mul  r27 r27 r26
	jr   r31
min_caml_read_int_sub_exit:
	addi r2 r0 0
	addi r27 r0 1
	jr   r31
	
	
min_caml_read_int:
	addi  r26 r26 10
	swi   r31 r3  0
	addi  r3  r3  4
	jal   min_caml_read_int_sub
	addi  r3  r3  -4
	lwi   r31 r3  0
	jr    r31
	
	
	
	
	
