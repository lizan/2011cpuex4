
#### f1 ####################################
#### r1 : loop counter , r2 base value  ####
#### r1 : previous heap pointer  ###########
#### use r28 ,r29               ############

 
min_caml_create_array:	
	addi	r29	r1	0
	addi	r1	r4	0
create_array_loop:
	seq		r28	r29	r0
	seq		r28 r28 r0
	jeql	r28 r0	create_array_exit
	swi		r2	r4	0
	addi	r29	r29	-1
	addi	r4	r4	1 #?
	jl		create_array_loop
create_array_exit:
	jr	r31



min_caml_create_float_array:
	addi	r29	r1	0
	addi	r1	r4	0
create_float_array_loop:
	seq		r28	r29	r0
	seq		r28	r28	r0
	jeql	r28	r0	create_float_array_exit
	fswi	f1  r4 0 # ?
	addi	r29	r29	-1
	addi	r4	r4	1 # ?
	jl	create_float_array_loop
create_float_array_exit:
	jr	r31
	
