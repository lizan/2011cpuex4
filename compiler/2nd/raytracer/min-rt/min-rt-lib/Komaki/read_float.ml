
let rec read_float_rec commaed numerator denominator =
	let a = read () in
	if a = 46 then
		read_float_rec 10 numerator denominator 
	else if a < 48 then
		(float_of_int numerator) /. (float_of_int denominator)
	else
		read_float_rec commaed (numerator * 10 + ( a - 48 ) ) (denominator * commaed ) 
in

let rec read_float x =
	let a = read () in
	if a = 45 then
		( 0.0 -. (read_float ()))
	else if a < 48 then
		read_float ()
	else
		read_float_rec 1 ( a - 48 )  1
in


