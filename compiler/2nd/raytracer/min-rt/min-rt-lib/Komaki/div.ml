

let rec my_div n m =
	let rec div_sub l r =
		if l + 1 = r then
			l
		else
		(
			let mid = (l + r) / 2 in
			if n < m * mid then
				div_sub mid r
			else
				div_sub l mid
		)
	in div_sub (0 - n) n
in



