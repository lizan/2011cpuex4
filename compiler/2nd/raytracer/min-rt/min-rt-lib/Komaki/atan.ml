let rec atan x =
  let pi = 3.14159265358979323846264 in
  let sgn =
    if x > 1.0 then 1
    else if x < -1.0 then -1
    else 0
  in
  let x =
    if (fabs x) > 1.0 then 1.0 /. x
    else x
  in
  let rec atan_sub i xx y =
    if i < 0.5 then y
    else atan_sub (i -. 1.0) xx ((i *. i *. xx) /. (2.0 *. i +. 1.0 +. y))
  in
  let a = atan_sub 11.0 (x *. x) 0.0 in
  let b = x /. (1.0 +. a) in
    if sgn > 0 then pi /. 2.0 -. b
    else if sgn < 0 then -. pi /. 2.0 -. b
    else b
in
