
let rec read_int_rec x =	
	let a = read () in
	if a < 48 then
		x
	else
		read_int_rec ( x * 10 + (a - 48) )
in

let rec read_int x =
	let a = read () in
	if a = 45 then
		0 - ( read_int () )
	else if a < 48 then
		read_int ()
	else
		read_int_rec ( a - 48 ) 
in



