

let rec my_div n m =
	let rec my_div_sub l r =
		if (l + 1 = r) then
			l
		else
		(
			let mid = (l + r) / 2 in
			if n < (m * mid) then
				(my_div_sub l mid)
			else
				(my_div_sub mid r)
		)
	in (my_div_sub (0 - n) n)
in

let rec print_int_rec x =

	if x = 0 then
		()
	else
	(

		print_int_rec ( my_div x 10 ) ; 
		print_int_sub ( x - ( my_div x 10 ) * 10 )
	)
in

let rec print_int x =
	if x = 0 then
		print_int_sub 0 
	else if x < 0 then
		(
			print_char 45 ;
			print_int_rec ( 0 - x )
		)
	else
		print_int_rec x
in



