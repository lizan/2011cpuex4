let rec div x y = 
  let rec div_sub x y ans = 
    if x >= y then div_sub (x-y) y (ans+1)
    else ans
  in div_sub x y 0
