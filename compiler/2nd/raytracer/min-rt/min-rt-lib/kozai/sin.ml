let rec sin x =
  let pi = 3.1415926535897932 in
  let hpi= 1.5707963267948966 in
  let qpi= 0.7853981633974483 in
  let flag = if x > 0.0 then true else false in
  let x0 = fabs x in
  let p = pi *. 2.0 in
  let rec redp p =
    if x0 >= p then redp (p *. 2.0)
    else p 
  in
  let fp = redp p in
  let rec redx fp fx =
    if fx >= p then
      if fx >= fp then redx (fp *. 0.5) (fx -. fp) 
      else redx (fp *. 0.5) fx
    else fx
  in
  let rx = redx fp x0 in
  (*let rx = x0 -. p *. (floor (x0 /. p)) in*)
  let rx0 = if rx >= pi then rx -. pi else rx in
  let flag0 = if rx >= pi then (not flag) else flag in 
  let rx1 = if rx0 >= hpi then pi -. rx0 else rx0 in
  let rec k_sin x = 
    let x2 = x *. x in
    let x4 = x2 *. x2 in
    let x6 = x2 *. x4 in
    let stay1 = 0.16666668 in
    let stay2 = 0.008332824 in
    let stay3 = 0.00019587841 in
      x *. (1.0 -. x2 *. stay1 +. x4 *. stay2 -. x6 *. stay3)
  in     
  let rec k_cos x =
    let x2 = x *. x in
    let x4 = x2 *. x2 in
    let x6 = x2 *. x4 in
    let ctay1 = 0.5 in
    let ctay2 = 0.04166368 in
    let ctay3 = 0.0013695068 in
      1.0 -. x2 *. ctay1 +. x4 *. ctay2 -. x6 *. ctay3 
  in
  let rx2 = if rx1 <= qpi then k_sin rx1
  else k_cos (hpi -. rx1) in
  if flag0 then rx2 else 0.0 -. rx2
in
