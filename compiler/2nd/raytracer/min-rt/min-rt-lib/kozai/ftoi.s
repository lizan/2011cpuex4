

min_caml_ftoi_sub:
	fswi	f2, r3, 4
min_caml_ftoi_large:	
	flt	r27, f2, f1
	jeql	r27, r0, min_caml_ftoi_small
	fsub	f1, f1, f2
	addi	r26, r26, 1
	jl	min_caml_ftoi_large
min_caml_ftoi_small:
	fadd	f26, f1, f2
	addi	r2, r26, 0
	fswi	f26, r3, 8
	lwi	r28, r3, 8
	lwi	r27, r3, 4
	sub	r1, r28, r27
	jr	r31
min_caml_ftoi_zero_end:
	flwi	f1, r3, 8
	lwi	r2, r3, 24
	flwi	f2, r3, 28
	addi	r1, r0, 0
	jr	r31
min_caml_ftoi:
min_caml_int_of_float:
	swi	r2, r3, 24
	fswi	f2, r3, 28
	fswi	f1, r3, 8
	lwi	r29, r3, 8
	slt	r26, r29, r0		#if(r26 == 0) then + else -
	swi	r26, r3, 12
	jeql	r26, r0, min_caml_ftoi_plus
	fsub	f1, f0, f1
	sub	r29, r0, r29
min_caml_ftoi_plus:	
	jeql	r29, r0, min_caml_ftoi_zero_end	#if(f == 0)
	addi	r26, r0, 16384
	addi	r27, r0, 512
	mul	r26, r26, r27	#0x00800000
	swi	r26, r3, 16
	addi	r27, r0, 150
	mul	r27, r26, r27	#0x4b000000(8388608.0)
	swi	r27, r3, 20
	addi	r26, r0, 20224		##0x00004f00
	addi	r27, r0, 256
	mul	r26, r26, r27		##0x004f0000
	mul	r26, r26, r27		# 0x4f000000
	slt	r28, r29, r26
	jeql	r28, r0, min_caml_ftoi_zero_end	#if(i >= 0x4f000000)
	flwi	f2, r3, 20		#0x4b000000
	swi	r31, r3, 4
	addi	r3, r3, 32
	jal	min_caml_ftoi_sub	#(f1,f2)=(f,8388608.0)を渡し,(r1,r2)=(ftoi(fn),m)を返す.  #f=8388608.0*m+fn
	addi	r3, r3, -32
	lwi	r31, r3, 4
	lwi	r26, r3, 16
	mul	r27, r26, r2
	add	r1, r1, r27
	lwi	r28, r3, 12
	jeql	r28, r0, min_caml_ftoi_end
	sub	r1, r0, r1
min_caml_ftoi_end:
	flwi	f1, r3, 8
	lwi	r2, r3, 24
	flwi	f2, r3, 28
	jr	r31
	
	
	
