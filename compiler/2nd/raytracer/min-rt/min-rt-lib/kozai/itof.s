

min_caml_itof_small:
	add	r26, r1, r2
	swi	r2, r3, 4
	swi	r26, r3, 8
	flwi	f26, r3, 4
	flwi	f27, r3, 8
	fsub	f1, f27, f26
	jr	r31
min_caml_itof_large:
	div	r26, r1, r2	#m
	mul	r27, r26, r2
	sub	r27, r1, r27	#n
	swi	r5, r3, 4
	flwi	f26, r3, 4
min_caml_itof_large_sub:
	jeql	r26, r0, min_caml_itof_large_exit
	fadd	f27, f27, f26
	addi	r26, r26, -1
	jl	min_caml_itof_large_sub	
min_caml_itof_large_exit:
	fswi	f27, r3, 8 
	addi	r1, r27, 0
	lwi	r2, r3, 4
	swi	r31, r3, 0
	addi	r3, r3, 12
	jal	min_caml_itof_small
	addi	r3, r3, -12
	lwi	r31, r3, 0
	flwi	f27, r3, 8
	fadd	f1, f1, f27
	jr 	r31
min_caml_itof_zero:
	fadd	f1, f0, f0
	jr 	r31
	
min_caml_itof:	
min_caml_float_of_int:
	swi	r1, r3, 8
	swi	r2, r3, 12
	swi	r5, r3, 16
	jeql	r1, r0, min_caml_itof_zero
	slt	r26, r1, r0	#if(r26 == 0) then + else -
	swi	r26, r3, 20
	jeql	r26, r0, min_caml_itof_plus
	sub	r1, r0, r1
min_caml_itof_plus:	
	addi	r26, r0, 16384	
	addi	r27, r0, 512
	mul	r26, r26, r27	#8388608(0x00800000)	
	addi	r27, r0, 150
	mul	r27, r26, r27	#0x4b000000(8388608.0)
	slt	r28, r26, r1
	addi	r29, r0, 1
	addi	r2, r27, 0
	jeql	r28, r29, min_caml_itof_golarge
	swi	r31, r3, 4
	addi	r3, r3, 24
	jal	min_caml_itof_small	#if(i < 8388608)((r1,r2)=(i,0x4b000000))
	addi	r3, r3, -24
	lwi	r31, r3, 4
	jl	min_caml_itof_last
min_caml_itof_golarge:
	addi	r2, r26, 0
	addi	r5, r27, 0
	swi	r31, r3, 4
	addi	r3, r3, 24
	jal	min_caml_itof_large	#else((r1,r2,r5)=(i,0x00800000,0x4b000000))
	addi	r3, r3, -24
	lwi	r31, r3, 4
min_caml_itof_last:
	lwi	r26, r3, 20
	jeql	r26, r0, min_caml_itof_end
	fsub	f1, f0, f1
min_caml_itof_end:
	lwi	r1, r3, 8
	lwi	r2, r3, 12
	lwi	r5, r3, 16
	jr 	r31
	
	
