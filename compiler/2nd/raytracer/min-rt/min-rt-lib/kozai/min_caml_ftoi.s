
min_caml_ftoi_sub:
	fswi	f27, r3, 4
	addi	r26, r0, 0
min_caml_ftoi_large:	
	flt	r27, f27, f26
	jeql	r27, r0, min_caml_ftoi_small
	fsub	f26, f26, f27
	addi	r26, r26, 1
	jl	min_caml_ftoi_large
min_caml_ftoi_small:
	fadd	f28, f26, f27
	fswi	f28, r3, 8
	lwi	r28, r3, 8
	lwi	r27, r3, 4
	sub	r1, r28, r27
	jr	r31
min_caml_ftoi_zero_end:
	addi	r1, r0, 0
	lwi	r26, r3, 28
	lwi	r27, r3, 32
	lwi	r28, r3, 36
	lwi	r29, r3, 40
	flwi	f26, r3, 44
	flwi	f27, r3, 48
	flwi	f28, r3, 52
	jr	r31
min_caml_ftoi:
min_caml_int_of_float:
	swi	r26, r3, 28
	swi	r27, r3, 32
	swi	r28, r3, 36
	swi	r29, r3, 40
	fswi	f26, r3, 44
	fswi	f27, r3, 48
	fswi	f28, r3, 52
	fadd	f26, f1, f0
	fswi	f26, r3, 8
	lwi	r29, r3, 8
	slt	r26, r29, r0		#if(r26 == 0) then + else -
	swi	r26, r3, 12
	jeql	r26, r0, min_caml_ftoi_plus
	fsub	f26, f0, f26
	sub	r29, r0, r29
min_caml_ftoi_plus:	
	jeql	r29, r0, min_caml_ftoi_zero_end	#if(f == 0)
	addi	r26, r0, 16384
	addi	r27, r0, 512
	mul	r26, r26, r27	#0x00800000
	swi	r26, r3, 16
	addi	r27, r0, 150
	mul	r27, r26, r27	#0x4b000000(8388608.0)
	swi	r27, r3, 20
	flwi	f27, r3, 20
		#以後f27=0x4b000000で固定
	addi	r26, r0, 20224		##0x00004f00
	addi	r27, r0, 256
	mul	r26, r26, r27		##0x004f0000
	mul	r26, r26, r27		# 0x4f000000
	swi	r26, r3, 24
	flwi	f28, r3, 24
	flt	r28, f26, f28
	jeql	r28, r0, min_caml_ftoi_zero_end	#if(i >= 0x4f000000)
	swi	r31, r3, 4
	addi	r3, r3, 56
	jal	min_caml_ftoi_sub
	addi	r3, r3, -56
	lwi	r31, r3, 4
	lwi	r27, r3, 16
	mul	r28, r27, r26
	add	r1, r1, r28
	lwi	r29, r3, 12
	jeql	r29, r0, min_caml_ftoi_end
	sub	r1, r0, r1
min_caml_ftoi_end:
	lwi	r26, r3, 28
	lwi	r27, r3, 32
	lwi	r28, r3, 36
	lwi	r29, r3, 40
	flwi	f26, r3, 44
	flwi	f27, r3, 48
	flwi	f28, r3, 52
	jr	r31
