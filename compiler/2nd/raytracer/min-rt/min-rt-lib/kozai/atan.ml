let rec atan x = 
  let hpi= 1.57079632679489661923132 in
  let qpi= 0.78539816339744830911566 in
  let flag = if x > 0.0 then true else false in
  let x0 = fabs x in
  let rec k_atan x = 
    let x2 = x *. x in 
    let tay1 = 0.3333333 in
    let tay2 = 0.2 in
    let tay3 = 0.142857142 in
    let tay4 = 0.111111104 in
    let tay5 = 0.08976446 in
    let tay6 = 0.060035485 in
      x *. (1.0 -. x2 *. (tay1 +. x2 *. (tay2 +. x2 *. (tay3 -. x2 *. (tay4 +. x2 *. (tay5 -. x2 *. tay6))))))
  in
  let rx = if x0 < 0.4375 then k_atan x 
  else if x0 < 2.4375 then qpi +. k_atan ((x0 -. 1.0) /. (x0 +. 1.0))
  else hpi -. k_atan (1.0 /. x0) in
    if flag then rx else 0.0 -. rx
in
