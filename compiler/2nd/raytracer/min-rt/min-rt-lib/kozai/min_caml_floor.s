min_caml_floor:
	swi	r1, r3, 24
	swi	r26, r3, 28
	swi	r27, r3, 32
	swi	r28, r3, 36
	swi	r29, r3, 40
	fswi	f26, r3, 44
	fswi	f27, r3, 48
	flt	r26, f1, f0
	swi	r26, r3, 8
	jeql	r26, r0, min_caml_floor_plus
	fsub	f1, f0, f1	
min_caml_floor_plus:
	addi	r26, r0, 16384
	addi	r27, r0, 512
	mul	r26, r26, r27		#0x00800000	
	addi	r27, r0, 150
	mul	r27, r26, r27		#0x4b000000
	swi	r27, r3, 12
	flwi	f27, r3, 12
	fswi	f1, r3, 16
	flt	r29, f1, f27
	jeql	r29, r0, min_caml_floor_jump
	swi	r31, r3, 4
	addi	r3, r3, 52
	jal	min_caml_ftoi
	jal	min_caml_itof
	addi	r3, r3, -52
	lwi	r31, r3, 4
	flwi	f26, r3, 16
	flt	r29, f26, f1	#if(元のf < floor(f)) then r29 = 1 else 0
min_caml_floor_jump:
	addi	r26, r0, 1016
	addi	r27, r0, 4096
	mul	r26, r26, r27
	addi	r27, r0, 256
	mul	r26, r26, r27		#0x3f800000(1.0)
	swi	r26, r3, 20
	flwi	f27, r3, 20
	jeql	r29, r0, min_caml_floor_end
	fsub	f1, f1, f27	
min_caml_floor_end:
	lwi	r26, r3, 8
	jeql	r26, r0, min_caml_floor_plus_end
	flwi	f26, r3, 16
	feq	r29, f1, f26
	addi	r28, r0, 1
	jeql	r29, r28, min_caml_floor_minus_end
	fadd	f1, f1, f27
min_caml_floor_minus_end:	
	fsub	f1, f0, f1
min_caml_floor_plus_end:
	lwi	r1, r3, 24
	lwi	r26, r3, 28
	lwi	r27, r3, 32
	lwi	r28, r3, 36
	lwi	r29, r3, 40
	flwi	f26, r3, 44
	flwi	f27, r3, 48	
	jr	r31
