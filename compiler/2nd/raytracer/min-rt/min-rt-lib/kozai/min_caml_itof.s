min_caml_itof_small:
	add	r28, r29, r27
	swi	r27, r3, 4
	swi	r28, r3, 8
	flwi	f27, r3, 4
	flwi	f28, r3, 8
	fsub	f1, f28, f27
	jr	r31
min_caml_itof_large:
	div	r28, r1, r26	#m
	mul	r29, r28, r26
	sub	r29, r1, r29	#n
	swi	r27, r3, 4
	flwi	f26, r3, 4
	fadd	f27, f0, f0
min_caml_itof_large_sub:
	jeql	r28, r0, min_caml_itof_large_exit
	fadd	f27, f27, f26
	addi	r28, r28, -1
	jl	min_caml_itof_large_sub	
min_caml_itof_large_exit:
	fswi	f27, r3, 8 
	swi	r31, r3, 0
	addi	r3, r3, 12
	jal	min_caml_itof_small
	addi	r3, r3, -12
	lwi	r31, r3, 0
	flwi	f27, r3, 8
	fadd	f1, f1, f27
	jr 	r31
min_caml_itof_zero:
	fadd	f1, f0, f0
	lwi	r26, r3, 16
	lwi	r27, r3, 20
	lwi	r28, r3, 24
	lwi	r29, r3, 28
	flwi	f26, r3, 32
	flwi	f27, r3, 36
	flwi	f28, r3, 40
	jr 	r31
min_caml_itof:	
min_caml_float_of_int:
	swi	r26, r3, 16
	swi	r27, r3, 20
	swi	r28, r3, 24
	swi	r29, r3, 28
	fswi	f26, r3, 32
	fswi	f27, r3, 36
	fswi	f28, r3, 40
	jeql	r1, r0, min_caml_itof_zero
	slt	r26, r1, r0	#if(r26 == 0) then + else -
	swi	r26, r3, 12
	jeql	r26, r0, min_caml_itof_plus
	sub	r1, r0, r1
min_caml_itof_plus:
	addi	r26, r0, 16384	
	addi	r27, r0, 512
	mul	r26, r26, r27	#8388608(0x00800000)	
	addi	r27, r0, 150
	mul	r27, r26, r27	#0x4b000000(8388608.0)
		#以後r26=0x00800000,r27=0x4b000000で固定
	slt	r28, r26, r1
	addi	r29, r0, 1
	jeql	r28, r29, min_caml_itof_golarge
	addi	r29, r1, 0
	swi	r31, r3, 4
	addi	r3, r3, 44
	jal	min_caml_itof_small
	addi	r3, r3, -44
	lwi	r31, r3, 4
	jl	min_caml_itof_last
min_caml_itof_golarge:
	swi	r31, r3, 4
	addi	r3, r3, 44
	jal	min_caml_itof_large
	addi	r3, r3, -44
	lwi	r31, r3, 4
min_caml_itof_last:
	lwi	r28, r3, 12
	jeql	r28, r0, min_caml_itof_end
	fsub	f1, f0, f1
	sub	r1, r0, r1
min_caml_itof_end:
	lwi	r26, r3, 16
	lwi	r27, r3, 20
	lwi	r28, r3, 24
	lwi	r29, r3, 28
	flwi	f26, r3, 32
	flwi	f27, r3, 36
	flwi	f28, r3, 40
	jr 	r31	
	