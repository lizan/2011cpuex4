#include <string>
#include <queue>
#include <vector>
#include <fstream>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
using namespace std;

//x(a downto b)に相当
u_int32_t subu(u_int32_t x, int a, int b){//x(a downto b)
  u_int32_t y = (x << (32 - a -1)) >> (32 - a + b - 1);
  return y;
}
/*
  vhdl実装と同じもの
 */
u_int32_t fadd(u_int32_t x1, u_int32_t x2){
  u_int32_t s1 = x1 >> 31;
  u_int32_t s2 = x2 >> 31;
  u_int32_t sp;
  u_int32_t e1 = (x1 & 0x7f800000) >> 23;
  u_int32_t e2 = (x2 & 0x7f800000) >> 23;
  u_int32_t ep,epx;
  u_int32_t f1 = (x1 & 0x7fffffff);//23bit
  u_int32_t f2 = (x2 & 0x7fffffff);
  u_int32_t fla,fsm,f3,f4,fcal,n,ftt,ett,i,mar,ans;
  if((e1 == 255 || e1 == 0) && (e2 == 255 || e2 == 0))
    fla = 0;
  else if(e1 > e2)
    fla = ((x1 & 0x007fffff) | 0x00800000) << 2;
  else if(e2 > e1)
    fla = ((x2 & 0x007fffff) | 0x00800000) << 2;
  else if(f1 > f2)
    fla = ((x1 & 0x007fffff) | 0x00800000) << 2;
  else
    fla = ((x2 & 0x007fffff) | 0x00800000) << 2;
  if(e1 == 0U || e1 == 255U || e2 == 0U || e2 == 255U)
    fsm = 0;
  else if(e1 > e2 && (e1-e2 > 26U))
    fsm = 0;
  else if(e2 > e1 && (e2-e1 > 26U))
    fsm = 0;
  else if(e1 - e2 == 25U)
    fsm = 1;
  else if(e2 - e1 == 25U)
    fsm = 1;
  else if(e1 > e2)
    fsm = (((x2 & 0x007fffff) | 0x00800000) << 2) >> (e1-e2);
  else if(e2 > e1)
    fsm = (((x1 & 0x007fffff) | 0x00800000) << 2) >> (e2-e1);
  else if(f1 > f2)
    fsm = ((x2 & 0x007fffff) | 0x00800000) << 2;
  else
    fsm = ((x1 & 0x007fffff) | 0x00800000) << 2;  
  if((e1 == 0U || e1 == 255U) && (e2 == 0U || e2 == 255U))
    ep = 0;
  else if(e1 > e2){
    ep = e1;
    sp = s1;
  }
  else if (e2 > e1){
    ep = e2;
    sp = s2;
  }
  else if(f1 > f2){
    ep = e1;
    sp = s1;
  }
  else{
    ep = e2;
    sp = s2;
  }
  if(s1 == s2)
    fcal = fla + fsm;
  else
    fcal = fla - fsm;    
  if(fcal >> 26 == 1)
    n=0;
  else if(subu(fcal,25,14) == 0)
    if(subu(fcal,13,8) == 0)
      if(subu(fcal,7,5) == 0)
	if(subu(fcal,4,2) == 0)
	  n=25;
	else if(subu(fcal,4,3) == 0)
	  n=24;
	else if(subu(fcal,4,4) == 0)
	  n=23;
	else
	  n=22;
      else
	if(subu(fcal,7,6) == 0)
	  n=21;
	else if(subu(fcal,7,7) == 0)
	  n=20;
	else
	  n=19;
    else
      if(subu(fcal,13,11) == 0)
	if(subu(fcal,10,9) == 0)
	  n=18;
	else if(subu(fcal,10,10) == 0)
	  n=17;
	else
	  n=16;
      else
	if(subu(fcal,13,12) == 0)
	  n=15;
	else if(subu(fcal,13,13) == 0)
	  n=14;
	else
	  n=13;
  else
    if(subu(fcal,25,20) == 0)
      if(subu(fcal,19,17) == 0)
	if(subu(fcal,16,15) == 0)
	  n=12;
	else if(subu(fcal,16,16) == 0)
	  n=11;
	else
	  n=10;
      else
	if(subu(fcal,19,18) == 0)
	  n=9;
	else if(subu(fcal,19,19) == 0)
	  n=8;
	else
	  n=7;
    else
      if(subu(fcal,25,23) == 0)
	if(subu(fcal,22,21) == 0)
	  n=6;
	else if(subu(fcal,22,22) == 0)
	  n=5;
	else
	  n=4;
      else
	if(subu(fcal,25,24) == 0)
	  n=3;
	else if(subu(fcal,25,25) == 0)
	  n=2;
	else
	  n=1;  
  if(subu(fcal,26,2) == 0x01ffffff)
    i = 2;
  else if(subu(fcal,25,1) == 0x01ffffff)
    i = 1;
  else if(subu(fcal,24,0) == 0x01ffffff)
    i = 1;
  else 
    i = 0;
  switch (subu(n,1,0)){
  case 0 : f3 = fcal; break;
  case 1 : f3 = (subu(fcal,25,0) << 1); break;
  case 2 : f3 = (subu(fcal,24,0) << 2); break;
  default: f3 = (subu(fcal,23,0) << 3); break;
  }
  switch (subu(n,1,0)){
  case 3 : mar = 0; break;
  default: mar = 4; break;
  }
  epx = ep+i;  
  switch (subu(n,4,2)){
  case 0 : ftt = f3 + mar; break;
  case 1 : ftt = (subu(f3,22,0) << 4); break;
  case 2 : ftt = (subu(f3,18,0) << 8); break;
  case 3 : ftt = (subu(f3,14,0) << 12); break;
  case 4 : ftt = (subu(f3,10,0) << 16); break;
  case 5 : ftt = (subu(f3,6,0) << 20); break;
  case 6 : ftt = (subu(f3,2,0) << 24); break;
  default: ftt = 0; break;
  }  
  if(n == 26 || epx < n)
    epx = 0;
  else
    epx = epx - n + 1;  
  if(ep == 0)
    ans = (sp << 31) + subu(ftt,25,3);
  else
    ans = (sp << 31) + (epx << 23) + subu(ftt,25,3);  
  return ans;
}



union Ap{
  int ii;
  float ff;
};
union Bp{
  int ii;
  float ff;
};
union Xp{
  int ii;
  float ff;
};

int ftoi_m(float f){
  union Ap app;
  union Bp bpp;
  union Xp xpp;
  app.ff = f;
  int i=0;
  if(f < 0){
    app.ff = -f;
    i = 1;
  }
  if(app.ii >= 0x4f000000)
    //return 0x7fffffff;
    return 0;
  if(f == 0)
    return 0;
  else{
    u_int32_t m=0;
    float a;
    
    while(app.ff >= 8388608.0){
      app.ff -= 8388608.0;
      m++;
    }
    xpp.ii = fadd(app.ii,0x4b000000) - 0x4b000000;
    xpp.ii += m * 8388608;
  }

  if(i == 1)
    xpp.ii = -xpp.ii;
  return xpp.ii;
}


float itof_m(int i){
  union Ap app;
  union Bp bpp;
  union Xp xpp;
  int j=0;
  if(i < 0){
    i = -i;
    j = 1;
  }
  if(i == 0)
    return 0; 
  else if(i < 8388608){
    app.ii = i + 0x4b000000;
    xpp.ii = fadd(app.ii,0xcb000000);
  }
  else{
    u_int32_t m,n,k;
    m = i / 8388608; 
    n = i % 8388608; 
    
    app.ii = 0;
    for(k=0;k<m;k++)
      app.ii = fadd(app.ii,0x4b000000); 

    bpp.ff = itof_m(n);
    
    xpp.ii = fadd(app.ii,bpp.ii);

  }
  if(j == 1)
    xpp.ff = -xpp.ff;
  return xpp.ff;
}

float floor_m(float f){
  int i;
  float fl;
  int j=0;
  if(f < 0){
    f = -f;
    j = 1;
  }
  if(f < 8388608.0){
    i = ftoi_m(f);
    fl = itof_m(i);
    if(fl > f)
      fl -= 1.0;
  }
  else
    fl = f;
  
  if(j == 1){
    if(f != fl)
      fl += 1.0;
    fl = -fl;
  }
  return fl;
}

union Dp{
  int ii;
  float ff;
};
union Cp{
  int ii;
  float ff;
};

union Up{
  u_int32_t ii;
  float ff;
};

int main(){
  union Dp dpp;
  union Cp cpp;
  int i =0;
  float f = 0;
  cpp.ii = 0x0201027a;
  cpp.ii = 0xcaffd808;
  cpp.ff = 1000000000;
  //cpp.ii = 0x4c0040a0;
  printf("%f\t%d\t0x%x\n",cpp.ff, cpp.ii, cpp.ii);
  f = cpp.ff;
  i = cpp.ii;
  //cin >> f;
  dpp.ff = itof_m(i);
  printf("itof =%f\t%x\n",dpp.ff,dpp.ii);
  printf("ftoi =%d\n",ftoi_m(f));
  printf("floor=%f\t%e\n",floor_m(f), floor_m(f));
  printf("o_flr=%f\t%e\n",floor(f), floor(f));
  return 0;
}
/*
int main(){
  union Up upp;
  upp.ii = 0x00000000;
  //upp.ii = 0x80000000;
  while(++upp.ii != 0){
    if(floor(upp.ff) != floor_m(upp.ff)){
      printf("%f\t%d\t0x%x\n",upp.ff, upp.ii, upp.ii);
      printf("ftoi =%d\n",ftoi_m(upp.ff));
      printf("itof =%f\n",itof_m(ftoi_m(upp.ff)));
      printf("floor=%f\t%e\n",floor_m(upp.ff), floor_m(upp.ff));
      printf("o_flr=%f\t%e\n",floor(upp.ff), floor(upp.ff));
      break;
    }
    if(upp.ii % 0x2000000 == 0)
      printf("%d\n", upp.ii/0x2000000);
  }
  return 0;
}
*/
