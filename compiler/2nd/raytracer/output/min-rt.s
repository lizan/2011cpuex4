	addi	%r4 %r4 10000
	INIT	128.000000 l.6776
	INIT	0.900000 l.6743
	INIT	0.200000 l.6742
	INIT	150.000000 l.6648
	INIT	-150.000000 l.6646
	INIT	0.100000 l.6630
	INIT	-2.000000 l.6627
	INIT	0.003906 l.6625
	INIT	20.000000 l.6603
	INIT	0.050000 l.6602
	INIT	0.250000 l.6598
	INIT	10.000000 l.6594
	INIT	0.300000 l.6590
	INIT	255.000000 l.6589
	INIT	0.150000 l.6588
	INIT	3.141593 l.6586
	INIT	30.000000 l.6585
	INIT	15.000000 l.6584
	INIT	0.000100 l.6583
	INIT	100000000.000000 l.6540
	INIT	1000000000.000000 l.6536
	INIT	-0.100000 l.6516
	INIT	0.010000 l.6503
	INIT	-0.200000 l.6502
	INIT	-200.000000 l.6278
	INIT	200.000000 l.6276
	INIT	0.017453 l.6272
	INIT	-1.570796 l.6189
	INIT	11.000000 l.6188
	INIT	1.570796 l.6187
	INIT	-1.000000 l.6186
	INIT	1.570796 l.6185
	INIT	6.283185 l.6184
	INIT	3.141593 l.6183
	INIT	9.000000 l.6182
	INIT	1.000000 l.6181
	INIT	2.000000 l.6180
	INIT	2.500000 l.6179
	INIT	0.500000 l.6178
	INIT	0.000000 l.6177
	jl	_min_caml_start
fabs.2499:
	flwl	%f2 l.6177
	fgt	r29, f2, f1
	jeql	r29, r0, ble_else.8852
	jr	r31
ble_else.8852:
	flwl	%f2 l.6177
	fsub	f1, f2, f1
	jr	r31
fhalf.2501:
	flwl	%f2 l.6178
	fmul	f1, f1, f2
	jr	r31
fisneg.2503:
	flwl	%f2 l.6177
	fgt	r29, f2, f1
	jeql	r29, r0, ble_else.8853
	addi	r2, r0 0
	jr	r31
ble_else.8853:
	addi	r2, r0 1
	jr	r31
fispos.2505:
	flwl	%f2 l.6177
	fgt	r29, f1, f2
	jeql	r29, r0, ble_else.8854
	addi	r2, r0 0
	jr	r31
ble_else.8854:
	addi	r2, r0 1
	jr	r31
fiszero.2507:
	flwl	%f2 l.6177
	feq	r29, f1, f2
	jeql	r29, r0, beq_else.8855
	addi	r2, r0 1
	jr	r31
beq_else.8855:
	addi	r2, r0 0
	jr	r31
fless.2509:
	fgt	r29, f2, f1
	jeql	r29, r0, ble_else.8856
	addi	r2, r0 0
	jr	r31
ble_else.8856:
	addi	r2, r0 1
	jr	r31
fneg.2512:
	flwl	%f2 l.6177
	fsub	f1, f2, f1
	jr	r31
fsqr.2514:
	fmul	f1, f1, f1
	jr	r31
tan_sub.6155:
	flwl	%f4 l.6179
	fgt	r29, f4, f1
	jeql	r29, r0, ble_else.8857
	flwl	%f4 l.6180
	fsub	f4, f1, f4
	fsub	f1, f1, f3
	fdiv	f3, f2, f1
	fadd	f1, f4 f0
	jl	tan_sub.6155
ble_else.8857:
	fadd	f1, f3 f0
	jr	r31
tan.6125:
	flwl	%f2 l.6181
	flwl	%f3 l.6182
	fmul	f4, f1, f1
	flwl	%f5 l.6177
	fswi	f1, r3 0
	fswi	f2, r3 8
	addi	r30 r31 0
	fadd	f2, f4 f0
	fadd	f1, f3 f0
	fadd	f3, f5 f0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	tan_sub.6155
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	flwi	f2, r3 8
	fsub	f1, f2, f1
	flwi	f2, r3 0
	fdiv	f1, f2, f1
	jr	r31
sin.2516:
	flwl	%f2 l.6183
	flwl	%f3 l.6184
	flwl	%f4 l.6185
	flwl	%f5 l.6177
	fgt	r29, f1, f5
	jeql	r29, r0, ble_else.8858
	addi	r2, r0 0
	jl	ble_cont.8859
ble_else.8858:
	addi	r2, r0 1
ble_cont.8859:
	fswi	f4, r3 0
	swi	r2, r3 8
	fswi	f2, r3 16
	fswi	f3, r3 24
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	fabs.2499
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	flwi	f2, r3 24
	fdiv	f3, f1, f2
	fswi	f1, r3 32
	addi	r30 r31 0
	fadd	f1, f3 f0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	min_caml_floor
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	flwi	f2, r3 24
	fmul	f1, f2, f1
	flwi	f3, r3 32
	fsub	f1, f3, f1
	flwi	f3, r3 16
	fgt	r29, f1, f3
	jeql	r29, r0, ble_else.8861
	lwi	r2, r3 8
	jl	ble_cont.8862
ble_else.8861:
	lwi	r2, r3 8
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8863
	addi	r2, r0 1
	jl	beq_cont.8864
beq_else.8863:
	addi	r2, r0 0
beq_cont.8864:
ble_cont.8862:
	fgt	r29, f1, f3
	jeql	r29, r0, ble_else.8865
	jl	ble_cont.8866
ble_else.8865:
	fsub	f1, f2, f1
ble_cont.8866:
	flwi	f2, r3 0
	fgt	r29, f1, f2
	jeql	r29, r0, ble_else.8867
	jl	ble_cont.8868
ble_else.8867:
	fsub	f1, f3, f1
ble_cont.8868:
	flwl	%f2 l.6178
	fmul	f1, f1, f2
	swi	r2, r3 40
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	tan.6125
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	lwi	r2, r3 40
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8869
	flwl	%f2 l.6186
	jl	beq_cont.8870
beq_else.8869:
	flwl	%f2 l.6181
beq_cont.8870:
	flwl	%f3 l.6180
	fmul	f3, f3, f1
	flwl	%f4 l.6181
	fmul	f1, f1, f1
	fadd	f1, f4, f1
	fdiv	f1, f3, f1
	fmul	f1, f2, f1
	jr	r31
cos.2518:
	flwl	%f2 l.6187
	fsub	f1, f2, f1
	jl	sin.2516
atan_sub.6088:
	flwl	%f4 l.6178
	fgt	r29, f4, f1
	jeql	r29, r0, ble_else.8871
	flwl	%f4 l.6181
	fsub	f4, f1, f4
	fmul	f5, f1, f1
	fmul	f5, f5, f2
	flwl	%f6 l.6180
	fmul	f1, f6, f1
	flwl	%f6 l.6181
	fadd	f1, f1, f6
	fadd	f1, f1, f3
	fdiv	f3, f5, f1
	fadd	f1, f4 f0
	jl	atan_sub.6088
ble_else.8871:
	fadd	f1, f3 f0
	jr	r31
atan.2520:
	flwl	%f2 l.6181
	fgt	r29, f1, f2
	jeql	r29, r0, ble_else.8872
	flwl	%f2 l.6186
	fgt	r29, f2, f1
	jeql	r29, r0, ble_else.8874
	addi	r2, r0 0
	jl	ble_cont.8875
ble_else.8874:
	addi	r2, r0 -1
ble_cont.8875:
	jl	ble_cont.8873
ble_else.8872:
	addi	r2, r0 1
ble_cont.8873:
	swi	r2, r3 0
	fswi	f1, r3 8
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	fabs.2499
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	flwl	%f2 l.6181
	fgt	r29, f1, f2
	jeql	r29, r0, ble_else.8877
	flwi	f1, r3 8
	jl	ble_cont.8878
ble_else.8877:
	flwl	%f1 l.6181
	flwi	f2, r3 8
	fdiv	f1, f1, f2
ble_cont.8878:
	flwl	%f2 l.6188
	fmul	f3, f1, f1
	flwl	%f4 l.6177
	fswi	f1, r3 16
	addi	r30 r31 0
	fadd	f1, f2 f0
	fadd	f2, f3 f0
	fadd	f3, f4 f0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	atan_sub.6088
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	flwl	%f2 l.6181
	fadd	f1, f2, f1
	flwi	f2, r3 16
	fdiv	f1, f2, f1
	lwi	r2, r3 0
	addi	r28, r0, 0
	sgt	r29, r2, r28
	seq	r29, r29, r0
	jeql	r29, r0, ble_else.8879
	addi	r28, r0, 0
	slt	r29, r2, r28
	seq	r29, r29, r0
	jeql	r29, r0, bge_else.8880
	jr	r31
bge_else.8880:
	flwl	%f2 l.6189
	fsub	f1, f2, f1
	jr	r31
ble_else.8879:
	flwl	%f2 l.6185
	fsub	f1, f2, f1
	jr	r31
xor.2552:
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8881
	addi	r2, r5 0
	jr	r31
beq_else.8881:
	addi	r28, r0, 0
	seq	r29, r5, r28
	jeql	r29, r0, beq_else.8882
	addi	r2, r0 1
	jr	r31
beq_else.8882:
	addi	r2, r0 0
	jr	r31
sgn.2555:
	fswi	f1, r3 0
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	fiszero.2507
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8883
	flwi	f1, r3 0
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	fispos.2505
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8884
	flwl	%f1 l.6186
	jr	r31
beq_else.8884:
	flwl	%f1 l.6181
	jr	r31
beq_else.8883:
	flwl	%f1 l.6177
	jr	r31
fneg_cond.2557:
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8885
	jl	fneg.2512
beq_else.8885:
	jr	r31
add_mod5.2560:
	add	r2, r2, r5
	addi	r28, r0, 5
	slt	r29, r2, r28
	seq	r29, r29, r0
	jeql	r29, r0, bge_else.8886
	addi	r2, r2, -5
	jr	r31
bge_else.8886:
	jr	r31
vecset.2563:
	fswi	f1, r2 0
	fswi	f2, r2 8
	fswi	f3, r2 16
	jr	r31
vecfill.2568:
	fswi	f1, r2 0
	fswi	f1, r2 8
	fswi	f1, r2 16
	jr	r31
vecbzero.2571:
	flwl	%f1 l.6177
	jl	vecfill.2568
veccpy.2573:
	flwi	f1, r5 0
	fswi	f1, r2 0
	flwi	f1, r5 8
	fswi	f1, r2 8
	flwi	f1, r5 16
	fswi	f1, r2 16
	jr	r31
vecunit_sgn.2581:
	flwi	f1, r2 0
	swi	r5, r3 0
	swi	r2, r3 4
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	fsqr.2514
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	lwi	r2, r3 4
	flwi	f2, r2 8
	fswi	f1, r3 8
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	fsqr.2514
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	flwi	f2, r3 8
	fadd	f1, f2, f1
	lwi	r2, r3 4
	flwi	f2, r2 16
	fswi	f1, r3 16
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	fsqr.2514
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	flwi	f2, r3 16
	fadd	f1, f2, f1
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	min_caml_sqrt
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	fswi	f1, r3 24
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	fiszero.2507
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8890
	lwi	r2, r3 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8892
	flwl	%f1 l.6181
	flwi	f2, r3 24
	fdiv	f1, f1, f2
	jl	beq_cont.8893
beq_else.8892:
	flwl	%f1 l.6186
	flwi	f2, r3 24
	fdiv	f1, f1, f2
beq_cont.8893:
	jl	beq_cont.8891
beq_else.8890:
	flwl	%f1 l.6181
beq_cont.8891:
	lwi	r2, r3 4
	flwi	f2, r2 0
	fmul	f2, f2, f1
	fswi	f2, r2 0
	flwi	f2, r2 8
	fmul	f2, f2, f1
	fswi	f2, r2 8
	flwi	f2, r2 16
	fmul	f1, f2, f1
	fswi	f1, r2 16
	jr	r31
veciprod.2584:
	flwi	f1, r2 0
	flwi	f2, r5 0
	fmul	f1, f1, f2
	flwi	f2, r2 8
	flwi	f3, r5 8
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	flwi	f2, r2 16
	flwi	f3, r5 16
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	jr	r31
veciprod2.2587:
	flwi	f4, r2 0
	fmul	f1, f4, f1
	flwi	f4, r2 8
	fmul	f2, f4, f2
	fadd	f1, f1, f2
	flwi	f2, r2 16
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	jr	r31
vecaccum.2592:
	flwi	f2, r2 0
	flwi	f3, r5 0
	fmul	f3, f1, f3
	fadd	f2, f2, f3
	fswi	f2, r2 0
	flwi	f2, r2 8
	flwi	f3, r5 8
	fmul	f3, f1, f3
	fadd	f2, f2, f3
	fswi	f2, r2 8
	flwi	f2, r2 16
	flwi	f3, r5 16
	fmul	f1, f1, f3
	fadd	f1, f2, f1
	fswi	f1, r2 16
	jr	r31
vecadd.2596:
	flwi	f1, r2 0
	flwi	f2, r5 0
	fadd	f1, f1, f2
	fswi	f1, r2 0
	flwi	f1, r2 8
	flwi	f2, r5 8
	fadd	f1, f1, f2
	fswi	f1, r2 8
	flwi	f1, r2 16
	flwi	f2, r5 16
	fadd	f1, f1, f2
	fswi	f1, r2 16
	jr	r31
vecscale.2602:
	flwi	f2, r2 0
	fmul	f2, f2, f1
	fswi	f2, r2 0
	flwi	f2, r2 8
	fmul	f2, f2, f1
	fswi	f2, r2 8
	flwi	f2, r2 16
	fmul	f1, f2, f1
	fswi	f1, r2 16
	jr	r31
vecaccumv.2605:
	flwi	f1, r2 0
	flwi	f2, r5 0
	flwi	f3, r6 0
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	fswi	f1, r2 0
	flwi	f1, r2 8
	flwi	f2, r5 8
	flwi	f3, r6 8
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	fswi	f1, r2 8
	flwi	f1, r2 16
	flwi	f2, r5 16
	flwi	f3, r6 16
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	fswi	f1, r2 16
	jr	r31
o_texturetype.2609:
	lwi	r2, r2 0
	jr	r31
o_form.2611:
	lwi	r2, r2 4
	jr	r31
o_reflectiontype.2613:
	lwi	r2, r2 8
	jr	r31
o_isinvert.2615:
	lwi	r2, r2 24
	jr	r31
o_isrot.2617:
	lwi	r2, r2 12
	jr	r31
o_param_a.2619:
	lwi	r2, r2 16
	flwi	f1, r2 0
	jr	r31
o_param_b.2621:
	lwi	r2, r2 16
	flwi	f1, r2 8
	jr	r31
o_param_c.2623:
	lwi	r2, r2 16
	flwi	f1, r2 16
	jr	r31
o_param_abc.2625:
	lwi	r2, r2 16
	jr	r31
o_param_x.2627:
	lwi	r2, r2 20
	flwi	f1, r2 0
	jr	r31
o_param_y.2629:
	lwi	r2, r2 20
	flwi	f1, r2 8
	jr	r31
o_param_z.2631:
	lwi	r2, r2 20
	flwi	f1, r2 16
	jr	r31
o_diffuse.2633:
	lwi	r2, r2 28
	flwi	f1, r2 0
	jr	r31
o_hilight.2635:
	lwi	r2, r2 28
	flwi	f1, r2 8
	jr	r31
o_color_red.2637:
	lwi	r2, r2 32
	flwi	f1, r2 0
	jr	r31
o_color_green.2639:
	lwi	r2, r2 32
	flwi	f1, r2 8
	jr	r31
o_color_blue.2641:
	lwi	r2, r2 32
	flwi	f1, r2 16
	jr	r31
o_param_r1.2643:
	lwi	r2, r2 36
	flwi	f1, r2 0
	jr	r31
o_param_r2.2645:
	lwi	r2, r2 36
	flwi	f1, r2 8
	jr	r31
o_param_r3.2647:
	lwi	r2, r2 36
	flwi	f1, r2 16
	jr	r31
o_param_ctbl.2649:
	lwi	r2, r2 40
	jr	r31
p_rgb.2651:
	lwi	r2, r2 0
	jr	r31
p_intersection_points.2653:
	lwi	r2, r2 4
	jr	r31
p_surface_ids.2655:
	lwi	r2, r2 8
	jr	r31
p_calc_diffuse.2657:
	lwi	r2, r2 12
	jr	r31
p_energy.2659:
	lwi	r2, r2 16
	jr	r31
p_received_ray_20percent.2661:
	lwi	r2, r2 20
	jr	r31
p_group_id.2663:
	lwi	r2, r2 24
	lwi	r2, r2 0
	jr	r31
p_set_group_id.2665:
	lwi	r2, r2 24
	swi	r5, r2 0
	jr	r31
p_nvectors.2668:
	lwi	r2, r2 28
	jr	r31
d_vec.2670:
	lwi	r2, r2 0
	jr	r31
d_const.2672:
	lwi	r2, r2 4
	jr	r31
r_surface_id.2674:
	lwi	r2, r2 0
	jr	r31
r_dvec.2676:
	lwi	r2, r2 4
	jr	r31
r_bright.2678:
	flwi	f1, r2 8
	jr	r31
rad.2680:
	flwl	%f2 l.6272
	fmul	f1, f1, f2
	jr	r31
read_screen_settings.2682:
	lwi	r2, r24 20
	lwi	r5, r24 16
	lwi	r6, r24 12
	lwi	r7, r24 8
	lwi	r8, r24 4
	swi	r2, r3 0
	swi	r6, r3 4
	swi	r7, r3 8
	swi	r5, r3 12
	swi	r8, r3 16
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	min_caml_read_float
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r2, r3 16
	fswi	f1, r2 0
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	min_caml_read_float
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r2, r3 16
	fswi	f1, r2 8
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	min_caml_read_float
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r2, r3 16
	fswi	f1, r2 16
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	min_caml_read_float
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	rad.2680
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	fswi	f1, r3 24
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	cos.2518
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	flwi	f2, r3 24
	fswi	f1, r3 32
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	sin.2516
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	fswi	f1, r3 40
	addi	r30 r31 0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	min_caml_read_float
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	rad.2680
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	fswi	f1, r3 48
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	cos.2518
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	flwi	f2, r3 48
	fswi	f1, r3 56
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	sin.2516
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	flwi	f2, r3 32
	fmul	f3, f2, f1
	flwl	%f4 l.6276
	fmul	f3, f3, f4
	lwi	r2, r3 12
	fswi	f3, r2 0
	flwl	%f3 l.6278
	flwi	f4, r3 40
	fmul	f3, f4, f3
	fswi	f3, r2 8
	flwi	f3, r3 56
	fmul	f5, f2, f3
	flwl	%f6 l.6276
	fmul	f5, f5, f6
	fswi	f5, r2 16
	lwi	r5, r3 8
	fswi	f3, r5 0
	flwl	%f5 l.6177
	fswi	f5, r5 8
	fswi	f1, r3 64
	addi	r30 r31 0
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	fneg.2512
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	lwi	r2, r3 8
	fswi	f1, r2 16
	flwi	f1, r3 40
	addi	r30 r31 0
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	fneg.2512
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	flwi	f2, r3 64
	fmul	f1, f1, f2
	lwi	r2, r3 4
	fswi	f1, r2 0
	flwi	f1, r3 32
	addi	r30 r31 0
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	fneg.2512
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	lwi	r2, r3 4
	fswi	f1, r2 8
	flwi	f1, r3 40
	addi	r30 r31 0
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	fneg.2512
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	flwi	f2, r3 56
	fmul	f1, f1, f2
	lwi	r2, r3 4
	fswi	f1, r2 16
	lwi	r2, r3 16
	flwi	f1, r2 0
	lwi	r5, r3 12
	flwi	f2, r5 0
	fsub	f1, f1, f2
	lwi	r6, r3 0
	fswi	f1, r6 0
	flwi	f1, r2 8
	flwi	f2, r5 8
	fsub	f1, f1, f2
	fswi	f1, r6 8
	flwi	f1, r2 16
	flwi	f2, r5 16
	fsub	f1, f1, f2
	fswi	f1, r6 16
	jr	r31
read_light.2684:
	lwi	r2, r24 8
	lwi	r5, r24 4
	swi	r5, r3 0
	swi	r2, r3 4
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	min_caml_read_int
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	min_caml_read_float
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	rad.2680
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	fswi	f1, r3 8
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	sin.2516
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	fneg.2512
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r2, r3 4
	fswi	f1, r2 8
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	min_caml_read_float
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	rad.2680
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	flwi	f2, r3 8
	fswi	f1, r3 16
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	cos.2518
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	flwi	f2, r3 16
	fswi	f1, r3 24
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	sin.2516
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	flwi	f2, r3 24
	fmul	f1, f2, f1
	lwi	r2, r3 4
	fswi	f1, r2 0
	flwi	f1, r3 16
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	cos.2518
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	flwi	f2, r3 24
	fmul	f1, f2, f1
	lwi	r2, r3 4
	fswi	f1, r2 16
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	min_caml_read_float
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	lwi	r2, r3 0
	fswi	f1, r2 0
	jr	r31
rotate_quadratic_matrix.2686:
	flwi	f1, r5 0
	swi	r2, r3 0
	swi	r5, r3 4
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	cos.2518
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	lwi	r2, r3 4
	flwi	f2, r2 0
	fswi	f1, r3 8
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	sin.2516
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r2, r3 4
	flwi	f2, r2 8
	fswi	f1, r3 16
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	cos.2518
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r2, r3 4
	flwi	f2, r2 8
	fswi	f1, r3 24
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	sin.2516
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	lwi	r2, r3 4
	flwi	f2, r2 16
	fswi	f1, r3 32
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	cos.2518
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	lwi	r2, r3 4
	flwi	f2, r2 16
	fswi	f1, r3 40
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	sin.2516
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	flwi	f2, r3 40
	flwi	f3, r3 24
	fmul	f4, f3, f2
	flwi	f5, r3 32
	flwi	f6, r3 16
	fmul	f7, f6, f5
	fmul	f7, f7, f2
	flwi	f8, r3 8
	fmul	f9, f8, f1
	fsub	f7, f7, f9
	fmul	f9, f8, f5
	fmul	f9, f9, f2
	fmul	f10, f6, f1
	fadd	f9, f9, f10
	fmul	f10, f3, f1
	fmul	f11, f6, f5
	fmul	f11, f11, f1
	fmul	f12, f8, f2
	fadd	f11, f11, f12
	fmul	f12, f8, f5
	fmul	f1, f12, f1
	fmul	f2, f6, f2
	fsub	f1, f1, f2
	fswi	f1, r3 48
	fswi	f9, r3 56
	fswi	f11, r3 64
	fswi	f7, r3 72
	fswi	f10, r3 80
	fswi	f4, r3 88
	addi	r30 r31 0
	fadd	f1, f5 f0
	swi	r30, r3 100
	addi	r3, r3, 104
	jal	fneg.2512
	addi	r3, r3, -104
	lwi	r30, r3 100
	addi	r31 r30 0
	flwi	f2, r3 24
	flwi	f3, r3 16
	fmul	f3, f3, f2
	flwi	f4, r3 8
	fmul	f2, f4, f2
	lwi	r2, r3 0
	flwi	f4, r2 0
	flwi	f5, r2 8
	flwi	f6, r2 16
	flwi	f7, r3 88
	fswi	f2, r3 96
	fswi	f3, r3 104
	fswi	f6, r3 112
	fswi	f1, r3 120
	fswi	f5, r3 128
	fswi	f4, r3 136
	addi	r30 r31 0
	fadd	f1, f7 f0
	swi	r30, r3 148
	addi	r3, r3, 152
	jal	fsqr.2514
	addi	r3, r3, -152
	lwi	r30, r3 148
	addi	r31 r30 0
	flwi	f2, r3 136
	fmul	f1, f2, f1
	flwi	f3, r3 80
	fswi	f1, r3 144
	addi	r30 r31 0
	fadd	f1, f3 f0
	swi	r30, r3 156
	addi	r3, r3, 160
	jal	fsqr.2514
	addi	r3, r3, -160
	lwi	r30, r3 156
	addi	r31 r30 0
	flwi	f2, r3 128
	fmul	f1, f2, f1
	flwi	f3, r3 144
	fadd	f1, f3, f1
	flwi	f3, r3 120
	fswi	f1, r3 152
	addi	r30 r31 0
	fadd	f1, f3 f0
	swi	r30, r3 164
	addi	r3, r3, 168
	jal	fsqr.2514
	addi	r3, r3, -168
	lwi	r30, r3 164
	addi	r31 r30 0
	flwi	f2, r3 112
	fmul	f1, f2, f1
	flwi	f3, r3 152
	fadd	f1, f3, f1
	lwi	r2, r3 0
	fswi	f1, r2 0
	flwi	f1, r3 72
	addi	r30 r31 0
	swi	r30, r3 164
	addi	r3, r3, 168
	jal	fsqr.2514
	addi	r3, r3, -168
	lwi	r30, r3 164
	addi	r31 r30 0
	flwi	f2, r3 136
	fmul	f1, f2, f1
	flwi	f3, r3 64
	fswi	f1, r3 160
	addi	r30 r31 0
	fadd	f1, f3 f0
	swi	r30, r3 172
	addi	r3, r3, 176
	jal	fsqr.2514
	addi	r3, r3, -176
	lwi	r30, r3 172
	addi	r31 r30 0
	flwi	f2, r3 128
	fmul	f1, f2, f1
	flwi	f3, r3 160
	fadd	f1, f3, f1
	flwi	f3, r3 104
	fswi	f1, r3 168
	addi	r30 r31 0
	fadd	f1, f3 f0
	swi	r30, r3 180
	addi	r3, r3, 184
	jal	fsqr.2514
	addi	r3, r3, -184
	lwi	r30, r3 180
	addi	r31 r30 0
	flwi	f2, r3 112
	fmul	f1, f2, f1
	flwi	f3, r3 168
	fadd	f1, f3, f1
	lwi	r2, r3 0
	fswi	f1, r2 8
	flwi	f1, r3 56
	addi	r30 r31 0
	swi	r30, r3 180
	addi	r3, r3, 184
	jal	fsqr.2514
	addi	r3, r3, -184
	lwi	r30, r3 180
	addi	r31 r30 0
	flwi	f2, r3 136
	fmul	f1, f2, f1
	flwi	f3, r3 48
	fswi	f1, r3 176
	addi	r30 r31 0
	fadd	f1, f3 f0
	swi	r30, r3 188
	addi	r3, r3, 192
	jal	fsqr.2514
	addi	r3, r3, -192
	lwi	r30, r3 188
	addi	r31 r30 0
	flwi	f2, r3 128
	fmul	f1, f2, f1
	flwi	f3, r3 176
	fadd	f1, f3, f1
	flwi	f3, r3 96
	fswi	f1, r3 184
	addi	r30 r31 0
	fadd	f1, f3 f0
	swi	r30, r3 196
	addi	r3, r3, 200
	jal	fsqr.2514
	addi	r3, r3, -200
	lwi	r30, r3 196
	addi	r31 r30 0
	flwi	f2, r3 112
	fmul	f1, f2, f1
	flwi	f3, r3 184
	fadd	f1, f3, f1
	lwi	r2, r3 0
	fswi	f1, r2 16
	flwl	%f1 l.6180
	flwi	f3, r3 72
	flwi	f4, r3 136
	fmul	f5, f4, f3
	flwi	f6, r3 56
	fmul	f5, f5, f6
	flwi	f7, r3 64
	flwi	f8, r3 128
	fmul	f9, f8, f7
	flwi	f10, r3 48
	fmul	f9, f9, f10
	fadd	f5, f5, f9
	flwi	f9, r3 104
	fmul	f11, f2, f9
	flwi	f12, r3 96
	fmul	f11, f11, f12
	fadd	f5, f5, f11
	fmul	f1, f1, f5
	lwi	r2, r3 4
	fswi	f1, r2 0
	flwl	%f1 l.6180
	flwi	f5, r3 88
	fmul	f11, f4, f5
	fmul	f6, f11, f6
	flwi	f11, r3 80
	fmul	f13, f8, f11
	fmul	f10, f13, f10
	fadd	f6, f6, f10
	flwi	f10, r3 120
	fmul	f13, f2, f10
	fmul	f12, f13, f12
	fadd	f6, f6, f12
	fmul	f1, f1, f6
	fswi	f1, r2 8
	flwl	%f1 l.6180
	fmul	f4, f4, f5
	fmul	f3, f4, f3
	fmul	f4, f8, f11
	fmul	f4, f4, f7
	fadd	f3, f3, f4
	fmul	f2, f2, f10
	fmul	f2, f2, f9
	fadd	f2, f3, f2
	fmul	f1, f1, f2
	fswi	f1, r2 16
	jr	r31
read_nth_object.2689:
	lwi	r5, r24 4
	swi	r5, r3 0
	swi	r2, r3 4
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	min_caml_read_int
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	addi	r28, r0, -1
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8904
	addi	r2, r0 0
	jr	r31
beq_else.8904:
	swi	r2, r3 8
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	min_caml_read_int
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	swi	r2, r3 12
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	min_caml_read_int
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	swi	r2, r3 16
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	min_caml_read_int
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	addi	r5, r0 3
	flwl	%f1 l.6177
	swi	r2, r3 20
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	min_caml_create_float_array
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	swi	r2, r3 24
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	min_caml_read_float
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r2, r3 24
	fswi	f1, r2 0
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	min_caml_read_float
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r2, r3 24
	fswi	f1, r2 8
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	min_caml_read_float
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r2, r3 24
	fswi	f1, r2 16
	addi	r5, r0 3
	flwl	%f1 l.6177
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	min_caml_create_float_array
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	swi	r2, r3 28
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	min_caml_read_float
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	lwi	r2, r3 28
	fswi	f1, r2 0
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	min_caml_read_float
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	lwi	r2, r3 28
	fswi	f1, r2 8
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	min_caml_read_float
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	lwi	r2, r3 28
	fswi	f1, r2 16
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	min_caml_read_float
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	fisneg.2503
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	addi	r5, r0 2
	flwl	%f1 l.6177
	swi	r2, r3 32
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	min_caml_create_float_array
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	swi	r2, r3 36
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	min_caml_read_float
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	lwi	r2, r3 36
	fswi	f1, r2 0
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	min_caml_read_float
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	lwi	r2, r3 36
	fswi	f1, r2 8
	addi	r5, r0 3
	flwl	%f1 l.6177
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	min_caml_create_float_array
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	swi	r2, r3 40
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	min_caml_read_float
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	lwi	r2, r3 40
	fswi	f1, r2 0
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	min_caml_read_float
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	lwi	r2, r3 40
	fswi	f1, r2 8
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	min_caml_read_float
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	lwi	r2, r3 40
	fswi	f1, r2 16
	addi	r5, r0 3
	flwl	%f1 l.6177
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	min_caml_create_float_array
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	lwi	r5, r3 20
	addi	r28, r0, 0
	seq	r29, r5, r28
	jeql	r29, r0, beq_else.8905
	jl	beq_cont.8906
beq_else.8905:
	swi	r2, r3 44
	addi	r30 r31 0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	min_caml_read_float
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	rad.2680
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	lwi	r2, r3 44
	fswi	f1, r2 0
	addi	r30 r31 0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	min_caml_read_float
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	rad.2680
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	lwi	r2, r3 44
	fswi	f1, r2 8
	addi	r30 r31 0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	min_caml_read_float
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	rad.2680
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	lwi	r2, r3 44
	fswi	f1, r2 16
beq_cont.8906:
	lwi	r5, r3 12
	addi	r28, r0, 2
	seq	r29, r5, r28
	jeql	r29, r0, beq_else.8907
	addi	r6, r0 1
	jl	beq_cont.8908
beq_else.8907:
	lwi	r6, r3 32
beq_cont.8908:
	addi	r7, r0 4
	flwl	%f1 l.6177
	swi	r6, r3 48
	swi	r2, r3 44
	addi	r30 r31 0
	addi	r2, r7 0 #
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	min_caml_create_float_array
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	addi	r5, r4 0
	addi	r4, r4, 48
	swi	r2, r5 40
	lwi	r2, r3 44
	swi	r2, r5 36
	lwi	r6, r3 40
	swi	r6, r5 32
	lwi	r6, r3 36
	swi	r6, r5 28
	lwi	r6, r3 48
	swi	r6, r5 24
	lwi	r6, r3 28
	swi	r6, r5 20
	lwi	r6, r3 24
	swi	r6, r5 16
	lwi	r7, r3 20
	swi	r7, r5 12
	lwi	r8, r3 16
	swi	r8, r5 8
	lwi	r8, r3 12
	swi	r8, r5 4
	lwi	r9, r3 8
	swi	r9, r5 0
	lwi	r9, r3 4
	sli	r9, r9, 2
	lwi	r10, r3 0
	add	r30 r10 r9
	swi	r5, r30 0
	addi	r28, r0, 3
	seq	r29, r8, r28
	jeql	r29, r0, beq_else.8909
	flwi	f1, r6 0
	fswi	f1, r3 56
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	fiszero.2507
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8912
	flwi	f1, r3 56
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	sgn.2555
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	flwi	f2, r3 56
	fswi	f1, r3 64
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	fsqr.2514
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	flwi	f2, r3 64
	fdiv	f1, f2, f1
	jl	beq_cont.8913
beq_else.8912:
	flwl	%f1 l.6177
beq_cont.8913:
	lwi	r2, r3 24
	fswi	f1, r2 0
	flwi	f1, r2 8
	fswi	f1, r3 72
	addi	r30 r31 0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	fiszero.2507
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8914
	flwi	f1, r3 72
	addi	r30 r31 0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	sgn.2555
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	flwi	f2, r3 72
	fswi	f1, r3 80
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	fsqr.2514
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	flwi	f2, r3 80
	fdiv	f1, f2, f1
	jl	beq_cont.8915
beq_else.8914:
	flwl	%f1 l.6177
beq_cont.8915:
	lwi	r2, r3 24
	fswi	f1, r2 8
	flwi	f1, r2 16
	fswi	f1, r3 88
	addi	r30 r31 0
	swi	r30, r3 100
	addi	r3, r3, 104
	jal	fiszero.2507
	addi	r3, r3, -104
	lwi	r30, r3 100
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8916
	flwi	f1, r3 88
	addi	r30 r31 0
	swi	r30, r3 100
	addi	r3, r3, 104
	jal	sgn.2555
	addi	r3, r3, -104
	lwi	r30, r3 100
	addi	r31 r30 0
	flwi	f2, r3 88
	fswi	f1, r3 96
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 108
	addi	r3, r3, 112
	jal	fsqr.2514
	addi	r3, r3, -112
	lwi	r30, r3 108
	addi	r31 r30 0
	flwi	f2, r3 96
	fdiv	f1, f2, f1
	jl	beq_cont.8917
beq_else.8916:
	flwl	%f1 l.6177
beq_cont.8917:
	lwi	r2, r3 24
	fswi	f1, r2 16
	jl	beq_cont.8910
beq_else.8909:
	addi	r28, r0, 2
	seq	r29, r8, r28
	jeql	r29, r0, beq_else.8918
	lwi	r5, r3 32
	addi	r28, r0, 0
	seq	r29, r5, r28
	jeql	r29, r0, beq_else.8920
	addi	r5, r0 1
	jl	beq_cont.8921
beq_else.8920:
	addi	r5, r0 0
beq_cont.8921:
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 108
	addi	r3, r3, 112
	jal	vecunit_sgn.2581
	addi	r3, r3, -112
	lwi	r30, r3 108
	addi	r31 r30 0
	jl	beq_cont.8919
beq_else.8918:
beq_cont.8919:
beq_cont.8910:
	lwi	r2, r3 20
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8922
	jl	beq_cont.8923
beq_else.8922:
	lwi	r2, r3 24
	lwi	r5, r3 44
	addi	r30 r31 0
	swi	r30, r3 108
	addi	r3, r3, 112
	jal	rotate_quadratic_matrix.2686
	addi	r3, r3, -112
	lwi	r30, r3 108
	addi	r31 r30 0
beq_cont.8923:
	addi	r2, r0 1
	jr	r31
read_object.2691:
	lwi	r5, r24 8
	lwi	r6, r24 4
	addi	r28, r0, 60
	slt	r29, r2, r28
	seq	r29, r29, r0
	jeql	r29, r0, bge_else.8924
	jr	r31
bge_else.8924:
	swi	r24, r3 0
	swi	r6, r3 4
	swi	r2, r3 8
	addi	r30 r31 0
	addi	r24, r5 0 #
	swi	r30, r3 12
	addi	r3, r3, 16
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8926
	lwi	r2, r3 4
	lwi	r5, r3 8
	swi	r5, r2 0
	jr	r31
beq_else.8926:
	lwi	r2, r3 8
	addi	r2, r2, 1
	lwi	r24, r3 0
	lwi	r23, r24 0
	jr	r23
read_all_object.2693:
	lwi	r24, r24 4
	addi	r2, r0 0
	lwi	r23, r24 0
	jr	r23
read_net_item.2695:
	swi	r2, r3 0
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_read_int
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	addi	r28, r0, -1
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8928
	lwi	r2, r3 0
	addi	r2, r2, 1
	addi	r5, r0 -1
	jl	min_caml_create_array
beq_else.8928:
	lwi	r5, r3 0
	addi	r6, r5, 1
	swi	r2, r3 4
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	read_net_item.2695
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	lwi	r5, r3 0
	sli	r5, r5, 2
	lwi	r6, r3 4
	add	r30 r2 r5
	swi	r6, r30 0
	jr	r31
read_or_network.2697:
	addi	r5, r0 0
	swi	r2, r3 0
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	read_net_item.2695
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r5, r2 0 #
	addi	r31 r30 0
	lwi	r2, r5 0
	addi	r28, r0, -1
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8929
	lwi	r2, r3 0
	addi	r2, r2, 1
	jl	min_caml_create_array
beq_else.8929:
	lwi	r2, r3 0
	addi	r6, r2, 1
	swi	r5, r3 4
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	read_or_network.2697
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	lwi	r5, r3 0
	sli	r5, r5, 2
	lwi	r6, r3 4
	add	r30 r2 r5
	swi	r6, r30 0
	jr	r31
read_and_network.2699:
	lwi	r5, r24 4
	addi	r6, r0 0
	swi	r24, r3 0
	swi	r5, r3 4
	swi	r2, r3 8
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	read_net_item.2695
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	lwi	r5, r2 0
	addi	r28, r0, -1
	seq	r29, r5, r28
	jeql	r29, r0, beq_else.8930
	jr	r31
beq_else.8930:
	lwi	r5, r3 8
	sli	r6, r5, 2
	lwi	r7, r3 4
	add	r30 r7 r6
	swi	r2, r30 0
	addi	r2, r5, 1
	lwi	r24, r3 0
	lwi	r23, r24 0
	jr	r23
read_parameter.2701:
	lwi	r2, r24 20
	lwi	r5, r24 16
	lwi	r6, r24 12
	lwi	r7, r24 8
	lwi	r8, r24 4
	swi	r8, r3 0
	swi	r6, r3 4
	swi	r7, r3 8
	swi	r5, r3 12
	addi	r30 r31 0
	addi	r24, r2 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r24, r3 12
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r24, r3 8
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	addi	r2, r0 0
	lwi	r24, r3 4
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	addi	r2, r0 0
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	read_or_network.2697
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r5, r3 0
	swi	r2, r5 0
	jr	r31
solver_rect_surface.2703:
	lwi	r9, r24 4
	sli	r10, r6, 3
	add	r30 r5 r10
	flwi	f4, r30 0
	swi	r9, r3 0
	fswi	f3, r3 8
	swi	r8, r3 16
	fswi	f2, r3 24
	swi	r7, r3 32
	fswi	f1, r3 40
	swi	r5, r3 48
	swi	r6, r3 52
	swi	r2, r3 56
	addi	r30 r31 0
	fadd	f1, f4 f0
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	fiszero.2507
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8936
	lwi	r2, r3 56
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	o_param_abc.2625
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	lwi	r5, r3 56
	swi	r2, r3 60
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	o_isinvert.2615
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	lwi	r5, r3 52
	sli	r6, r5, 3
	lwi	r7, r3 48
	add	r30 r7 r6
	flwi	f1, r30 0
	swi	r2, r3 64
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	fisneg.2503
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r5, r2 0 #
	addi	r31 r30 0
	lwi	r2, r3 64
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	xor.2552
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	lwi	r5, r3 52
	sli	r6, r5, 3
	lwi	r7, r3 60
	add	r30 r7 r6
	flwi	f1, r30 0
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	fneg_cond.2557
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	flwi	f2, r3 40
	fsub	f1, f1, f2
	lwi	r2, r3 52
	sli	r2, r2, 3
	lwi	r5, r3 48
	add	r30 r5 r2
	flwi	f2, r30 0
	fdiv	f1, f1, f2
	lwi	r2, r3 32
	sli	r6, r2, 3
	add	r30 r5 r6
	flwi	f2, r30 0
	fmul	f2, f1, f2
	flwi	f3, r3 24
	fadd	f2, f2, f3
	fswi	f1, r3 72
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	fabs.2499
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	lwi	r2, r3 32
	sli	r2, r2, 3
	lwi	r5, r3 60
	add	r30 r5 r2
	flwi	f2, r30 0
	addi	r30 r31 0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	fless.2509
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8938
	addi	r2, r0 0
	jr	r31
beq_else.8938:
	lwi	r2, r3 16
	sli	r5, r2, 3
	lwi	r6, r3 48
	add	r30 r6 r5
	flwi	f1, r30 0
	flwi	f2, r3 72
	fmul	f1, f2, f1
	flwi	f3, r3 8
	fadd	f1, f1, f3
	addi	r30 r31 0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	fabs.2499
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	lwi	r2, r3 16
	sli	r2, r2, 3
	lwi	r5, r3 60
	add	r30 r5 r2
	flwi	f2, r30 0
	addi	r30 r31 0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	fless.2509
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8939
	addi	r2, r0 0
	jr	r31
beq_else.8939:
	lwi	r2, r3 0
	flwi	f1, r3 72
	fswi	f1, r2 0
	addi	r2, r0 1
	jr	r31
beq_else.8936:
	addi	r2, r0 0
	jr	r31
solver_rect.2712:
	lwi	r24, r24 4
	addi	r6, r0 0
	addi	r7, r0 1
	addi	r8, r0 2
	fswi	f1, r3 0
	fswi	f3, r3 8
	fswi	f2, r3 16
	swi	r5, r3 24
	swi	r2, r3 28
	swi	r24, r3 32
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8940
	addi	r6, r0 1
	addi	r7, r0 2
	addi	r8, r0 0
	flwi	f1, r3 16
	flwi	f2, r3 8
	flwi	f3, r3 0
	lwi	r2, r3 28
	lwi	r5, r3 24
	lwi	r24, r3 32
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8941
	addi	r6, r0 2
	addi	r7, r0 0
	addi	r8, r0 1
	flwi	f1, r3 8
	flwi	f2, r3 0
	flwi	f3, r3 16
	lwi	r2, r3 28
	lwi	r5, r3 24
	lwi	r24, r3 32
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8942
	addi	r2, r0 0
	jr	r31
beq_else.8942:
	addi	r2, r0 3
	jr	r31
beq_else.8941:
	addi	r2, r0 2
	jr	r31
beq_else.8940:
	addi	r2, r0 1
	jr	r31
solver_surface.2718:
	lwi	r6, r24 4
	swi	r6, r3 0
	fswi	f3, r3 8
	fswi	f2, r3 16
	fswi	f1, r3 24
	swi	r5, r3 32
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	o_param_abc.2625
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r5, r2 0 #
	addi	r31 r30 0
	lwi	r2, r3 32
	swi	r5, r3 36
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	veciprod.2584
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	fswi	f1, r3 40
	addi	r30 r31 0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	fispos.2505
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8944
	addi	r2, r0 0
	jr	r31
beq_else.8944:
	flwi	f1, r3 24
	flwi	f2, r3 16
	flwi	f3, r3 8
	lwi	r2, r3 36
	addi	r30 r31 0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	veciprod2.2587
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	fneg.2512
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	flwi	f2, r3 40
	fdiv	f1, f1, f2
	lwi	r2, r3 0
	fswi	f1, r2 0
	addi	r2, r0 1
	jr	r31
quadratic.2724:
	fswi	f1, r3 0
	fswi	f3, r3 8
	fswi	f2, r3 16
	swi	r2, r3 24
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	fsqr.2514
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r2, r3 24
	fswi	f1, r3 32
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	o_param_a.2619
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	flwi	f2, r3 32
	fmul	f1, f2, f1
	flwi	f2, r3 16
	fswi	f1, r3 40
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	fsqr.2514
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	lwi	r2, r3 24
	fswi	f1, r3 48
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	o_param_b.2621
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	flwi	f2, r3 48
	fmul	f1, f2, f1
	flwi	f2, r3 40
	fadd	f1, f2, f1
	flwi	f2, r3 8
	fswi	f1, r3 56
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	fsqr.2514
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	lwi	r2, r3 24
	fswi	f1, r3 64
	addi	r30 r31 0
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	o_param_c.2623
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	flwi	f2, r3 64
	fmul	f1, f2, f1
	flwi	f2, r3 56
	fadd	f1, f2, f1
	lwi	r2, r3 24
	fswi	f1, r3 72
	addi	r30 r31 0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	o_isrot.2617
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8946
	flwi	f1, r3 72
	jr	r31
beq_else.8946:
	flwi	f1, r3 8
	flwi	f2, r3 16
	fmul	f3, f2, f1
	lwi	r2, r3 24
	fswi	f3, r3 80
	addi	r30 r31 0
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	o_param_r1.2643
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	flwi	f2, r3 80
	fmul	f1, f2, f1
	flwi	f2, r3 72
	fadd	f1, f2, f1
	flwi	f2, r3 0
	flwi	f3, r3 8
	fmul	f3, f3, f2
	lwi	r2, r3 24
	fswi	f1, r3 88
	fswi	f3, r3 96
	addi	r30 r31 0
	swi	r30, r3 108
	addi	r3, r3, 112
	jal	o_param_r2.2645
	addi	r3, r3, -112
	lwi	r30, r3 108
	addi	r31 r30 0
	flwi	f2, r3 96
	fmul	f1, f2, f1
	flwi	f2, r3 88
	fadd	f1, f2, f1
	flwi	f2, r3 16
	flwi	f3, r3 0
	fmul	f2, f3, f2
	lwi	r2, r3 24
	fswi	f1, r3 104
	fswi	f2, r3 112
	addi	r30 r31 0
	swi	r30, r3 124
	addi	r3, r3, 128
	jal	o_param_r3.2647
	addi	r3, r3, -128
	lwi	r30, r3 124
	addi	r31 r30 0
	flwi	f2, r3 112
	fmul	f1, f2, f1
	flwi	f2, r3 104
	fadd	f1, f2, f1
	jr	r31
bilinear.2729:
	fmul	f7, f1, f4
	fswi	f4, r3 0
	fswi	f1, r3 8
	fswi	f6, r3 16
	fswi	f3, r3 24
	swi	r2, r3 32
	fswi	f5, r3 40
	fswi	f2, r3 48
	fswi	f7, r3 56
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	o_param_a.2619
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	flwi	f2, r3 56
	fmul	f1, f2, f1
	flwi	f2, r3 40
	flwi	f3, r3 48
	fmul	f4, f3, f2
	lwi	r2, r3 32
	fswi	f1, r3 64
	fswi	f4, r3 72
	addi	r30 r31 0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	o_param_b.2621
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	flwi	f2, r3 72
	fmul	f1, f2, f1
	flwi	f2, r3 64
	fadd	f1, f2, f1
	flwi	f2, r3 16
	flwi	f3, r3 24
	fmul	f4, f3, f2
	lwi	r2, r3 32
	fswi	f1, r3 80
	fswi	f4, r3 88
	addi	r30 r31 0
	swi	r30, r3 100
	addi	r3, r3, 104
	jal	o_param_c.2623
	addi	r3, r3, -104
	lwi	r30, r3 100
	addi	r31 r30 0
	flwi	f2, r3 88
	fmul	f1, f2, f1
	flwi	f2, r3 80
	fadd	f1, f2, f1
	lwi	r2, r3 32
	fswi	f1, r3 96
	addi	r30 r31 0
	swi	r30, r3 108
	addi	r3, r3, 112
	jal	o_isrot.2617
	addi	r3, r3, -112
	lwi	r30, r3 108
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8948
	flwi	f1, r3 96
	jr	r31
beq_else.8948:
	flwi	f1, r3 40
	flwi	f2, r3 24
	fmul	f3, f2, f1
	flwi	f4, r3 16
	flwi	f5, r3 48
	fmul	f6, f5, f4
	fadd	f3, f3, f6
	lwi	r2, r3 32
	fswi	f3, r3 104
	addi	r30 r31 0
	swi	r30, r3 116
	addi	r3, r3, 120
	jal	o_param_r1.2643
	addi	r3, r3, -120
	lwi	r30, r3 116
	addi	r31 r30 0
	flwi	f2, r3 104
	fmul	f1, f2, f1
	flwi	f2, r3 16
	flwi	f3, r3 8
	fmul	f2, f3, f2
	flwi	f4, r3 0
	flwi	f5, r3 24
	fmul	f5, f5, f4
	fadd	f2, f2, f5
	lwi	r2, r3 32
	fswi	f1, r3 112
	fswi	f2, r3 120
	addi	r30 r31 0
	swi	r30, r3 132
	addi	r3, r3, 136
	jal	o_param_r2.2645
	addi	r3, r3, -136
	lwi	r30, r3 132
	addi	r31 r30 0
	flwi	f2, r3 120
	fmul	f1, f2, f1
	flwi	f2, r3 112
	fadd	f1, f2, f1
	flwi	f2, r3 40
	flwi	f3, r3 8
	fmul	f2, f3, f2
	flwi	f3, r3 0
	flwi	f4, r3 48
	fmul	f3, f4, f3
	fadd	f2, f2, f3
	lwi	r2, r3 32
	fswi	f1, r3 128
	fswi	f2, r3 136
	addi	r30 r31 0
	swi	r30, r3 148
	addi	r3, r3, 152
	jal	o_param_r3.2647
	addi	r3, r3, -152
	lwi	r30, r3 148
	addi	r31 r30 0
	flwi	f2, r3 136
	fmul	f1, f2, f1
	flwi	f2, r3 128
	fadd	f1, f2, f1
	addi	r30 r31 0
	swi	r30, r3 148
	addi	r3, r3, 152
	jal	fhalf.2501
	addi	r3, r3, -152
	lwi	r30, r3 148
	addi	r31 r30 0
	flwi	f2, r3 96
	fadd	f1, f2, f1
	jr	r31
solver_second.2737:
	lwi	r6, r24 4
	flwi	f4, r5 0
	flwi	f5, r5 8
	flwi	f6, r5 16
	swi	r6, r3 0
	fswi	f3, r3 8
	fswi	f2, r3 16
	fswi	f1, r3 24
	swi	r2, r3 32
	swi	r5, r3 36
	addi	r30 r31 0
	fadd	f3, f6 f0
	fadd	f2, f5 f0
	fadd	f1, f4 f0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	quadratic.2724
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	fswi	f1, r3 40
	addi	r30 r31 0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	fiszero.2507
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8950
	lwi	r2, r3 36
	flwi	f1, r2 0
	flwi	f2, r2 8
	flwi	f3, r2 16
	flwi	f4, r3 24
	flwi	f5, r3 16
	flwi	f6, r3 8
	lwi	r2, r3 32
	addi	r30 r31 0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	bilinear.2729
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	flwi	f2, r3 24
	flwi	f3, r3 16
	flwi	f4, r3 8
	lwi	r2, r3 32
	fswi	f1, r3 48
	addi	r30 r31 0
	fadd	f1, f2 f0
	fadd	f2, f3 f0
	fadd	f3, f4 f0
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	quadratic.2724
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	lwi	r2, r3 32
	fswi	f1, r3 56
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	o_form.2611
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	addi	r28, r0, 3
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8951
	flwl	%f1 l.6181
	flwi	f2, r3 56
	fsub	f1, f2, f1
	jl	beq_cont.8952
beq_else.8951:
	flwi	f1, r3 56
beq_cont.8952:
	flwi	f2, r3 48
	fswi	f1, r3 64
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	fsqr.2514
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	flwi	f2, r3 64
	flwi	f3, r3 40
	fmul	f2, f3, f2
	fsub	f1, f1, f2
	fswi	f1, r3 72
	addi	r30 r31 0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	fispos.2505
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8953
	addi	r2, r0 0
	jr	r31
beq_else.8953:
	flwi	f1, r3 72
	addi	r30 r31 0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	min_caml_sqrt
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	lwi	r2, r3 32
	fswi	f1, r3 80
	addi	r30 r31 0
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	o_isinvert.2615
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8954
	flwi	f1, r3 80
	addi	r30 r31 0
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	fneg.2512
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	jl	beq_cont.8955
beq_else.8954:
	flwi	f1, r3 80
beq_cont.8955:
	flwi	f2, r3 48
	fsub	f1, f1, f2
	flwi	f2, r3 40
	fdiv	f1, f1, f2
	lwi	r2, r3 0
	fswi	f1, r2 0
	addi	r2, r0 1
	jr	r31
beq_else.8950:
	addi	r2, r0 0
	jr	r31
solver.2743:
	lwi	r7, r24 16
	lwi	r8, r24 12
	lwi	r9, r24 8
	lwi	r10, r24 4
	sli	r2, r2, 2
	add	r30 r10 r2
	lwi	r2, r30 0
	flwi	f1, r6 0
	swi	r8, r3 0
	swi	r7, r3 4
	swi	r5, r3 8
	swi	r9, r3 12
	swi	r2, r3 16
	swi	r6, r3 20
	fswi	f1, r3 24
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	o_param_x.2627
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	flwi	f2, r3 24
	fsub	f1, f2, f1
	lwi	r2, r3 20
	flwi	f2, r2 8
	lwi	r5, r3 16
	fswi	f1, r3 32
	fswi	f2, r3 40
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	o_param_y.2629
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	flwi	f2, r3 40
	fsub	f1, f2, f1
	lwi	r2, r3 20
	flwi	f2, r2 16
	lwi	r2, r3 16
	fswi	f1, r3 48
	fswi	f2, r3 56
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	o_param_z.2631
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	flwi	f2, r3 56
	fsub	f1, f2, f1
	lwi	r2, r3 16
	fswi	f1, r3 64
	addi	r30 r31 0
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	o_form.2611
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	addi	r28, r0, 1
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8956
	flwi	f1, r3 32
	flwi	f2, r3 48
	flwi	f3, r3 64
	lwi	r2, r3 16
	lwi	r5, r3 8
	lwi	r24, r3 12
	lwi	r23, r24 0
	jr	r23
beq_else.8956:
	addi	r28, r0, 2
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8957
	flwi	f1, r3 32
	flwi	f2, r3 48
	flwi	f3, r3 64
	lwi	r2, r3 16
	lwi	r5, r3 8
	lwi	r24, r3 4
	lwi	r23, r24 0
	jr	r23
beq_else.8957:
	flwi	f1, r3 32
	flwi	f2, r3 48
	flwi	f3, r3 64
	lwi	r2, r3 16
	lwi	r5, r3 8
	lwi	r24, r3 0
	lwi	r23, r24 0
	jr	r23
solver_rect_fast.2747:
	lwi	r7, r24 4
	flwi	f4, r6 0
	fsub	f4, f4, f1
	flwi	f5, r6 8
	fmul	f4, f4, f5
	flwi	f5, r5 8
	fmul	f5, f4, f5
	fadd	f5, f5, f2
	swi	r7, r3 0
	fswi	f1, r3 8
	fswi	f2, r3 16
	swi	r6, r3 24
	fswi	f3, r3 32
	fswi	f4, r3 40
	swi	r5, r3 48
	swi	r2, r3 52
	addi	r30 r31 0
	fadd	f1, f5 f0
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	fabs.2499
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	lwi	r2, r3 52
	fswi	f1, r3 56
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	o_param_b.2621
	addi	r3, r3, -72
	lwi	r30, r3 68
	fadd	f2, f1 f0
	addi	r31 r30 0
	flwi	f1, r3 56
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	fless.2509
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8960
	addi	r2, r0 0
	jl	beq_cont.8961
beq_else.8960:
	lwi	r2, r3 48
	flwi	f1, r2 16
	flwi	f2, r3 40
	fmul	f1, f2, f1
	flwi	f3, r3 32
	fadd	f1, f1, f3
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	fabs.2499
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	lwi	r2, r3 52
	fswi	f1, r3 64
	addi	r30 r31 0
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	o_param_c.2623
	addi	r3, r3, -80
	lwi	r30, r3 76
	fadd	f2, f1 f0
	addi	r31 r30 0
	flwi	f1, r3 64
	addi	r30 r31 0
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	fless.2509
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8962
	addi	r2, r0 0
	jl	beq_cont.8963
beq_else.8962:
	lwi	r2, r3 24
	flwi	f1, r2 8
	addi	r30 r31 0
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	fiszero.2507
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8964
	addi	r2, r0 1
	jl	beq_cont.8965
beq_else.8964:
	addi	r2, r0 0
beq_cont.8965:
beq_cont.8963:
beq_cont.8961:
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8966
	lwi	r2, r3 24
	flwi	f1, r2 16
	flwi	f2, r3 16
	fsub	f1, f1, f2
	flwi	f3, r2 24
	fmul	f1, f1, f3
	lwi	r5, r3 48
	flwi	f3, r5 0
	fmul	f3, f1, f3
	flwi	f4, r3 8
	fadd	f3, f3, f4
	fswi	f1, r3 72
	addi	r30 r31 0
	fadd	f1, f3 f0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	fabs.2499
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	lwi	r2, r3 52
	fswi	f1, r3 80
	addi	r30 r31 0
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	o_param_a.2619
	addi	r3, r3, -96
	lwi	r30, r3 92
	fadd	f2, f1 f0
	addi	r31 r30 0
	flwi	f1, r3 80
	addi	r30 r31 0
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	fless.2509
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8967
	addi	r2, r0 0
	jl	beq_cont.8968
beq_else.8967:
	lwi	r2, r3 48
	flwi	f1, r2 16
	flwi	f2, r3 72
	fmul	f1, f2, f1
	flwi	f3, r3 32
	fadd	f1, f1, f3
	addi	r30 r31 0
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	fabs.2499
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	lwi	r2, r3 52
	fswi	f1, r3 88
	addi	r30 r31 0
	swi	r30, r3 100
	addi	r3, r3, 104
	jal	o_param_c.2623
	addi	r3, r3, -104
	lwi	r30, r3 100
	fadd	f2, f1 f0
	addi	r31 r30 0
	flwi	f1, r3 88
	addi	r30 r31 0
	swi	r30, r3 100
	addi	r3, r3, 104
	jal	fless.2509
	addi	r3, r3, -104
	lwi	r30, r3 100
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8969
	addi	r2, r0 0
	jl	beq_cont.8970
beq_else.8969:
	lwi	r2, r3 24
	flwi	f1, r2 24
	addi	r30 r31 0
	swi	r30, r3 100
	addi	r3, r3, 104
	jal	fiszero.2507
	addi	r3, r3, -104
	lwi	r30, r3 100
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8971
	addi	r2, r0 1
	jl	beq_cont.8972
beq_else.8971:
	addi	r2, r0 0
beq_cont.8972:
beq_cont.8970:
beq_cont.8968:
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8973
	lwi	r2, r3 24
	flwi	f1, r2 32
	flwi	f2, r3 32
	fsub	f1, f1, f2
	flwi	f2, r2 40
	fmul	f1, f1, f2
	lwi	r5, r3 48
	flwi	f2, r5 0
	fmul	f2, f1, f2
	flwi	f3, r3 8
	fadd	f2, f2, f3
	fswi	f1, r3 96
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 108
	addi	r3, r3, 112
	jal	fabs.2499
	addi	r3, r3, -112
	lwi	r30, r3 108
	addi	r31 r30 0
	lwi	r2, r3 52
	fswi	f1, r3 104
	addi	r30 r31 0
	swi	r30, r3 116
	addi	r3, r3, 120
	jal	o_param_a.2619
	addi	r3, r3, -120
	lwi	r30, r3 116
	fadd	f2, f1 f0
	addi	r31 r30 0
	flwi	f1, r3 104
	addi	r30 r31 0
	swi	r30, r3 116
	addi	r3, r3, 120
	jal	fless.2509
	addi	r3, r3, -120
	lwi	r30, r3 116
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8974
	addi	r2, r0 0
	jl	beq_cont.8975
beq_else.8974:
	lwi	r2, r3 48
	flwi	f1, r2 8
	flwi	f2, r3 96
	fmul	f1, f2, f1
	flwi	f3, r3 16
	fadd	f1, f1, f3
	addi	r30 r31 0
	swi	r30, r3 116
	addi	r3, r3, 120
	jal	fabs.2499
	addi	r3, r3, -120
	lwi	r30, r3 116
	addi	r31 r30 0
	lwi	r2, r3 52
	fswi	f1, r3 112
	addi	r30 r31 0
	swi	r30, r3 124
	addi	r3, r3, 128
	jal	o_param_b.2621
	addi	r3, r3, -128
	lwi	r30, r3 124
	fadd	f2, f1 f0
	addi	r31 r30 0
	flwi	f1, r3 112
	addi	r30 r31 0
	swi	r30, r3 124
	addi	r3, r3, 128
	jal	fless.2509
	addi	r3, r3, -128
	lwi	r30, r3 124
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8976
	addi	r2, r0 0
	jl	beq_cont.8977
beq_else.8976:
	lwi	r2, r3 24
	flwi	f1, r2 40
	addi	r30 r31 0
	swi	r30, r3 124
	addi	r3, r3, 128
	jal	fiszero.2507
	addi	r3, r3, -128
	lwi	r30, r3 124
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8978
	addi	r2, r0 1
	jl	beq_cont.8979
beq_else.8978:
	addi	r2, r0 0
beq_cont.8979:
beq_cont.8977:
beq_cont.8975:
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8980
	addi	r2, r0 0
	jr	r31
beq_else.8980:
	lwi	r2, r3 0
	flwi	f1, r3 96
	fswi	f1, r2 0
	addi	r2, r0 3
	jr	r31
beq_else.8973:
	lwi	r2, r3 0
	flwi	f1, r3 72
	fswi	f1, r2 0
	addi	r2, r0 2
	jr	r31
beq_else.8966:
	lwi	r2, r3 0
	flwi	f1, r3 40
	fswi	f1, r2 0
	addi	r2, r0 1
	jr	r31
solver_surface_fast.2754:
	lwi	r2, r24 4
	flwi	f4, r5 0
	swi	r2, r3 0
	fswi	f3, r3 8
	fswi	f2, r3 16
	fswi	f1, r3 24
	swi	r5, r3 32
	addi	r30 r31 0
	fadd	f1, f4 f0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	fisneg.2503
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8982
	addi	r2, r0 0
	jr	r31
beq_else.8982:
	lwi	r2, r3 32
	flwi	f1, r2 8
	flwi	f2, r3 24
	fmul	f1, f1, f2
	flwi	f2, r2 16
	flwi	f3, r3 16
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	flwi	f2, r2 24
	flwi	f3, r3 8
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	lwi	r2, r3 0
	fswi	f1, r2 0
	addi	r2, r0 1
	jr	r31
solver_second_fast.2760:
	lwi	r6, r24 4
	flwi	f4, r5 0
	swi	r6, r3 0
	fswi	f4, r3 8
	swi	r2, r3 16
	fswi	f3, r3 24
	fswi	f2, r3 32
	fswi	f1, r3 40
	swi	r5, r3 48
	addi	r30 r31 0
	fadd	f1, f4 f0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	fiszero.2507
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8985
	lwi	r2, r3 48
	flwi	f1, r2 8
	flwi	f2, r3 40
	fmul	f1, f1, f2
	flwi	f3, r2 16
	flwi	f4, r3 32
	fmul	f3, f3, f4
	fadd	f1, f1, f3
	flwi	f3, r2 24
	flwi	f5, r3 24
	fmul	f3, f3, f5
	fadd	f1, f1, f3
	lwi	r5, r3 16
	fswi	f1, r3 56
	addi	r30 r31 0
	addi	r2, r5 0 #
	fadd	f3, f5 f0
	fadd	f1, f2 f0
	fadd	f2, f4 f0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	quadratic.2724
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	lwi	r2, r3 16
	fswi	f1, r3 64
	addi	r30 r31 0
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	o_form.2611
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	addi	r28, r0, 3
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8987
	flwl	%f1 l.6181
	flwi	f2, r3 64
	fsub	f1, f2, f1
	jl	beq_cont.8988
beq_else.8987:
	flwi	f1, r3 64
beq_cont.8988:
	flwi	f2, r3 56
	fswi	f1, r3 72
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	fsqr.2514
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	flwi	f2, r3 72
	flwi	f3, r3 8
	fmul	f2, f3, f2
	fsub	f1, f1, f2
	fswi	f1, r3 80
	addi	r30 r31 0
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	fispos.2505
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8989
	addi	r2, r0 0
	jr	r31
beq_else.8989:
	lwi	r2, r3 16
	addi	r30 r31 0
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	o_isinvert.2615
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8990
	flwi	f1, r3 80
	addi	r30 r31 0
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	min_caml_sqrt
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	flwi	f2, r3 56
	fsub	f1, f2, f1
	lwi	r2, r3 48
	flwi	f2, r2 32
	fmul	f1, f1, f2
	lwi	r2, r3 0
	fswi	f1, r2 0
	jl	beq_cont.8991
beq_else.8990:
	flwi	f1, r3 80
	addi	r30 r31 0
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	min_caml_sqrt
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	flwi	f2, r3 56
	fadd	f1, f2, f1
	lwi	r2, r3 48
	flwi	f2, r2 32
	fmul	f1, f1, f2
	lwi	r2, r3 0
	fswi	f1, r2 0
beq_cont.8991:
	addi	r2, r0 1
	jr	r31
beq_else.8985:
	addi	r2, r0 0
	jr	r31
solver_fast.2766:
	lwi	r7, r24 16
	lwi	r8, r24 12
	lwi	r9, r24 8
	lwi	r10, r24 4
	sli	r11, r2, 2
	add	r30 r10 r11
	lwi	r10, r30 0
	flwi	f1, r6 0
	swi	r8, r3 0
	swi	r7, r3 4
	swi	r9, r3 8
	swi	r2, r3 12
	swi	r5, r3 16
	swi	r10, r3 20
	swi	r6, r3 24
	fswi	f1, r3 32
	addi	r30 r31 0
	addi	r2, r10 0 #
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	o_param_x.2627
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	flwi	f2, r3 32
	fsub	f1, f2, f1
	lwi	r2, r3 24
	flwi	f2, r2 8
	lwi	r5, r3 20
	fswi	f1, r3 40
	fswi	f2, r3 48
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	o_param_y.2629
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	flwi	f2, r3 48
	fsub	f1, f2, f1
	lwi	r2, r3 24
	flwi	f2, r2 16
	lwi	r2, r3 20
	fswi	f1, r3 56
	fswi	f2, r3 64
	addi	r30 r31 0
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	o_param_z.2631
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	flwi	f2, r3 64
	fsub	f1, f2, f1
	lwi	r2, r3 16
	fswi	f1, r3 72
	addi	r30 r31 0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	d_const.2672
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	lwi	r5, r3 12
	sli	r5, r5, 2
	add	r30 r2 r5
	lwi	r2, r30 0
	lwi	r5, r3 20
	swi	r2, r3 80
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	o_form.2611
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	addi	r28, r0, 1
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8993
	lwi	r2, r3 16
	addi	r30 r31 0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	d_vec.2670
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r5, r2 0 #
	addi	r31 r30 0
	flwi	f1, r3 40
	flwi	f2, r3 56
	flwi	f3, r3 72
	lwi	r2, r3 20
	lwi	r6, r3 80
	lwi	r24, r3 8
	lwi	r23, r24 0
	jr	r23
beq_else.8993:
	addi	r28, r0, 2
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8994
	flwi	f1, r3 40
	flwi	f2, r3 56
	flwi	f3, r3 72
	lwi	r2, r3 20
	lwi	r5, r3 80
	lwi	r24, r3 4
	lwi	r23, r24 0
	jr	r23
beq_else.8994:
	flwi	f1, r3 40
	flwi	f2, r3 56
	flwi	f3, r3 72
	lwi	r2, r3 20
	lwi	r5, r3 80
	lwi	r24, r3 0
	lwi	r23, r24 0
	jr	r23
solver_surface_fast2.2770:
	lwi	r2, r24 4
	flwi	f1, r5 0
	swi	r2, r3 0
	swi	r6, r3 4
	swi	r5, r3 8
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	fisneg.2503
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8995
	addi	r2, r0 0
	jr	r31
beq_else.8995:
	lwi	r2, r3 8
	flwi	f1, r2 0
	lwi	r2, r3 4
	flwi	f2, r2 24
	fmul	f1, f1, f2
	lwi	r2, r3 0
	fswi	f1, r2 0
	addi	r2, r0 1
	jr	r31
solver_second_fast2.2777:
	lwi	r7, r24 4
	flwi	f4, r5 0
	swi	r7, r3 0
	swi	r2, r3 4
	fswi	f4, r3 8
	swi	r6, r3 16
	fswi	f3, r3 24
	fswi	f2, r3 32
	fswi	f1, r3 40
	swi	r5, r3 48
	addi	r30 r31 0
	fadd	f1, f4 f0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	fiszero.2507
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8997
	lwi	r2, r3 48
	flwi	f1, r2 8
	flwi	f2, r3 40
	fmul	f1, f1, f2
	flwi	f2, r2 16
	flwi	f3, r3 32
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	flwi	f2, r2 24
	flwi	f3, r3 24
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	lwi	r5, r3 16
	flwi	f2, r5 24
	fswi	f1, r3 56
	fswi	f2, r3 64
	addi	r30 r31 0
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	fsqr.2514
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	flwi	f2, r3 64
	flwi	f3, r3 8
	fmul	f2, f3, f2
	fsub	f1, f1, f2
	fswi	f1, r3 72
	addi	r30 r31 0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	fispos.2505
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.8999
	addi	r2, r0 0
	jr	r31
beq_else.8999:
	lwi	r2, r3 4
	addi	r30 r31 0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	o_isinvert.2615
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9000
	flwi	f1, r3 72
	addi	r30 r31 0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	min_caml_sqrt
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	flwi	f2, r3 56
	fsub	f1, f2, f1
	lwi	r2, r3 48
	flwi	f2, r2 32
	fmul	f1, f1, f2
	lwi	r2, r3 0
	fswi	f1, r2 0
	jl	beq_cont.9001
beq_else.9000:
	flwi	f1, r3 72
	addi	r30 r31 0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	min_caml_sqrt
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	flwi	f2, r3 56
	fadd	f1, f2, f1
	lwi	r2, r3 48
	flwi	f2, r2 32
	fmul	f1, f1, f2
	lwi	r2, r3 0
	fswi	f1, r2 0
beq_cont.9001:
	addi	r2, r0 1
	jr	r31
beq_else.8997:
	addi	r2, r0 0
	jr	r31
solver_fast2.2784:
	lwi	r6, r24 16
	lwi	r7, r24 12
	lwi	r8, r24 8
	lwi	r9, r24 4
	sli	r10, r2, 2
	add	r30 r9 r10
	lwi	r9, r30 0
	swi	r7, r3 0
	swi	r6, r3 4
	swi	r8, r3 8
	swi	r9, r3 12
	swi	r2, r3 16
	swi	r5, r3 20
	addi	r30 r31 0
	addi	r2, r9 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	o_param_ctbl.2649
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	flwi	f1, r2 0
	flwi	f2, r2 8
	flwi	f3, r2 16
	lwi	r5, r3 20
	swi	r2, r3 24
	fswi	f3, r3 32
	fswi	f2, r3 40
	fswi	f1, r3 48
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	d_const.2672
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	lwi	r5, r3 16
	sli	r5, r5, 2
	add	r30 r2 r5
	lwi	r2, r30 0
	lwi	r5, r3 12
	swi	r2, r3 56
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	o_form.2611
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	addi	r28, r0, 1
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9003
	lwi	r2, r3 20
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	d_vec.2670
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r5, r2 0 #
	addi	r31 r30 0
	flwi	f1, r3 48
	flwi	f2, r3 40
	flwi	f3, r3 32
	lwi	r2, r3 12
	lwi	r6, r3 56
	lwi	r24, r3 8
	lwi	r23, r24 0
	jr	r23
beq_else.9003:
	addi	r28, r0, 2
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9004
	flwi	f1, r3 48
	flwi	f2, r3 40
	flwi	f3, r3 32
	lwi	r2, r3 12
	lwi	r5, r3 56
	lwi	r6, r3 24
	lwi	r24, r3 4
	lwi	r23, r24 0
	jr	r23
beq_else.9004:
	flwi	f1, r3 48
	flwi	f2, r3 40
	flwi	f3, r3 32
	lwi	r2, r3 12
	lwi	r5, r3 56
	lwi	r6, r3 24
	lwi	r24, r3 0
	lwi	r23, r24 0
	jr	r23
setup_rect_table.2787:
	addi	r6, r0 6
	flwl	%f1 l.6177
	swi	r5, r3 0
	swi	r2, r3 4
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	min_caml_create_float_array
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	lwi	r5, r3 4
	flwi	f1, r5 0
	swi	r2, r3 8
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	fiszero.2507
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9005
	lwi	r2, r3 0
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	o_isinvert.2615
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	lwi	r5, r3 4
	flwi	f1, r5 0
	swi	r2, r3 12
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	fisneg.2503
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r5, r2 0 #
	addi	r31 r30 0
	lwi	r2, r3 12
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	xor.2552
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r5, r3 0
	swi	r2, r3 16
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	o_param_a.2619
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r2, r3 16
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	fneg_cond.2557
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r2, r3 8
	fswi	f1, r2 0
	flwl	%f1 l.6181
	lwi	r5, r3 4
	flwi	f2, r5 0
	fdiv	f1, f1, f2
	fswi	f1, r2 8
	jl	beq_cont.9006
beq_else.9005:
	flwl	%f1 l.6177
	lwi	r2, r3 8
	fswi	f1, r2 8
beq_cont.9006:
	lwi	r5, r3 4
	flwi	f1, r5 8
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	fiszero.2507
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9007
	lwi	r2, r3 0
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	o_isinvert.2615
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r5, r3 4
	flwi	f1, r5 8
	swi	r2, r3 20
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	fisneg.2503
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r5, r2 0 #
	addi	r31 r30 0
	lwi	r2, r3 20
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	xor.2552
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r5, r3 0
	swi	r2, r3 24
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	o_param_b.2621
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r2, r3 24
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	fneg_cond.2557
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r2, r3 8
	fswi	f1, r2 16
	flwl	%f1 l.6181
	lwi	r5, r3 4
	flwi	f2, r5 8
	fdiv	f1, f1, f2
	fswi	f1, r2 24
	jl	beq_cont.9008
beq_else.9007:
	flwl	%f1 l.6177
	lwi	r2, r3 8
	fswi	f1, r2 24
beq_cont.9008:
	lwi	r5, r3 4
	flwi	f1, r5 16
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	fiszero.2507
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9009
	lwi	r2, r3 0
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	o_isinvert.2615
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r5, r3 4
	flwi	f1, r5 16
	swi	r2, r3 28
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	fisneg.2503
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r5, r2 0 #
	addi	r31 r30 0
	lwi	r2, r3 28
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	xor.2552
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	lwi	r5, r3 0
	swi	r2, r3 32
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	o_param_c.2623
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	lwi	r2, r3 32
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	fneg_cond.2557
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	lwi	r2, r3 8
	fswi	f1, r2 32
	flwl	%f1 l.6181
	lwi	r5, r3 4
	flwi	f2, r5 16
	fdiv	f1, f1, f2
	fswi	f1, r2 40
	jl	beq_cont.9010
beq_else.9009:
	flwl	%f1 l.6177
	lwi	r2, r3 8
	fswi	f1, r2 40
beq_cont.9010:
	jr	r31
setup_surface_table.2790:
	addi	r6, r0 4
	flwl	%f1 l.6177
	swi	r5, r3 0
	swi	r2, r3 4
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	min_caml_create_float_array
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	lwi	r5, r3 4
	flwi	f1, r5 0
	lwi	r6, r3 0
	swi	r2, r3 8
	fswi	f1, r3 16
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	o_param_a.2619
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	flwi	f2, r3 16
	fmul	f1, f2, f1
	lwi	r2, r3 4
	flwi	f2, r2 8
	lwi	r5, r3 0
	fswi	f1, r3 24
	fswi	f2, r3 32
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	o_param_b.2621
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	flwi	f2, r3 32
	fmul	f1, f2, f1
	flwi	f2, r3 24
	fadd	f1, f2, f1
	lwi	r2, r3 4
	flwi	f2, r2 16
	lwi	r2, r3 0
	fswi	f1, r3 40
	fswi	f2, r3 48
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	o_param_c.2623
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	flwi	f2, r3 48
	fmul	f1, f2, f1
	flwi	f2, r3 40
	fadd	f1, f2, f1
	fswi	f1, r3 56
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	fispos.2505
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9012
	flwl	%f1 l.6177
	lwi	r2, r3 8
	fswi	f1, r2 0
	jl	beq_cont.9013
beq_else.9012:
	flwl	%f1 l.6186
	flwi	f2, r3 56
	fdiv	f1, f1, f2
	lwi	r2, r3 8
	fswi	f1, r2 0
	lwi	r5, r3 0
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	o_param_a.2619
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	flwi	f2, r3 56
	fdiv	f1, f1, f2
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	fneg.2512
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	lwi	r2, r3 8
	fswi	f1, r2 8
	lwi	r5, r3 0
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	o_param_b.2621
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	flwi	f2, r3 56
	fdiv	f1, f1, f2
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	fneg.2512
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	lwi	r2, r3 8
	fswi	f1, r2 16
	lwi	r5, r3 0
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	o_param_c.2623
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	flwi	f2, r3 56
	fdiv	f1, f1, f2
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	fneg.2512
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	lwi	r2, r3 8
	fswi	f1, r2 24
beq_cont.9013:
	jr	r31
setup_second_table.2793:
	addi	r6, r0 5
	flwl	%f1 l.6177
	swi	r5, r3 0
	swi	r2, r3 4
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	min_caml_create_float_array
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	lwi	r5, r3 4
	flwi	f1, r5 0
	flwi	f2, r5 8
	flwi	f3, r5 16
	lwi	r6, r3 0
	swi	r2, r3 8
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	quadratic.2724
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	lwi	r2, r3 4
	flwi	f2, r2 0
	lwi	r5, r3 0
	fswi	f1, r3 16
	fswi	f2, r3 24
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	o_param_a.2619
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	flwi	f2, r3 24
	fmul	f1, f2, f1
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	fneg.2512
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	lwi	r2, r3 4
	flwi	f2, r2 8
	lwi	r5, r3 0
	fswi	f1, r3 32
	fswi	f2, r3 40
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	o_param_b.2621
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	flwi	f2, r3 40
	fmul	f1, f2, f1
	addi	r30 r31 0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	fneg.2512
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	lwi	r2, r3 4
	flwi	f2, r2 16
	lwi	r5, r3 0
	fswi	f1, r3 48
	fswi	f2, r3 56
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	o_param_c.2623
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	flwi	f2, r3 56
	fmul	f1, f2, f1
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	fneg.2512
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	lwi	r2, r3 8
	flwi	f2, r3 16
	fswi	f2, r2 0
	lwi	r5, r3 0
	fswi	f1, r3 64
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	o_isrot.2617
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9015
	lwi	r2, r3 8
	flwi	f1, r3 32
	fswi	f1, r2 8
	flwi	f1, r3 48
	fswi	f1, r2 16
	flwi	f1, r3 64
	fswi	f1, r2 24
	jl	beq_cont.9016
beq_else.9015:
	lwi	r2, r3 4
	flwi	f1, r2 16
	lwi	r5, r3 0
	fswi	f1, r3 72
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	o_param_r2.2645
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	flwi	f2, r3 72
	fmul	f1, f2, f1
	lwi	r2, r3 4
	flwi	f2, r2 8
	lwi	r5, r3 0
	fswi	f1, r3 80
	fswi	f2, r3 88
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 100
	addi	r3, r3, 104
	jal	o_param_r3.2647
	addi	r3, r3, -104
	lwi	r30, r3 100
	addi	r31 r30 0
	flwi	f2, r3 88
	fmul	f1, f2, f1
	flwi	f2, r3 80
	fadd	f1, f2, f1
	addi	r30 r31 0
	swi	r30, r3 100
	addi	r3, r3, 104
	jal	fhalf.2501
	addi	r3, r3, -104
	lwi	r30, r3 100
	addi	r31 r30 0
	flwi	f2, r3 32
	fsub	f1, f2, f1
	lwi	r2, r3 8
	fswi	f1, r2 8
	lwi	r5, r3 4
	flwi	f1, r5 16
	lwi	r6, r3 0
	fswi	f1, r3 96
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 108
	addi	r3, r3, 112
	jal	o_param_r1.2643
	addi	r3, r3, -112
	lwi	r30, r3 108
	addi	r31 r30 0
	flwi	f2, r3 96
	fmul	f1, f2, f1
	lwi	r2, r3 4
	flwi	f2, r2 0
	lwi	r5, r3 0
	fswi	f1, r3 104
	fswi	f2, r3 112
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 124
	addi	r3, r3, 128
	jal	o_param_r3.2647
	addi	r3, r3, -128
	lwi	r30, r3 124
	addi	r31 r30 0
	flwi	f2, r3 112
	fmul	f1, f2, f1
	flwi	f2, r3 104
	fadd	f1, f2, f1
	addi	r30 r31 0
	swi	r30, r3 124
	addi	r3, r3, 128
	jal	fhalf.2501
	addi	r3, r3, -128
	lwi	r30, r3 124
	addi	r31 r30 0
	flwi	f2, r3 48
	fsub	f1, f2, f1
	lwi	r2, r3 8
	fswi	f1, r2 16
	lwi	r5, r3 4
	flwi	f1, r5 8
	lwi	r6, r3 0
	fswi	f1, r3 120
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 132
	addi	r3, r3, 136
	jal	o_param_r1.2643
	addi	r3, r3, -136
	lwi	r30, r3 132
	addi	r31 r30 0
	flwi	f2, r3 120
	fmul	f1, f2, f1
	lwi	r2, r3 4
	flwi	f2, r2 0
	lwi	r2, r3 0
	fswi	f1, r3 128
	fswi	f2, r3 136
	addi	r30 r31 0
	swi	r30, r3 148
	addi	r3, r3, 152
	jal	o_param_r2.2645
	addi	r3, r3, -152
	lwi	r30, r3 148
	addi	r31 r30 0
	flwi	f2, r3 136
	fmul	f1, f2, f1
	flwi	f2, r3 128
	fadd	f1, f2, f1
	addi	r30 r31 0
	swi	r30, r3 148
	addi	r3, r3, 152
	jal	fhalf.2501
	addi	r3, r3, -152
	lwi	r30, r3 148
	addi	r31 r30 0
	flwi	f2, r3 64
	fsub	f1, f2, f1
	lwi	r2, r3 8
	fswi	f1, r2 24
beq_cont.9016:
	flwi	f1, r3 16
	addi	r30 r31 0
	swi	r30, r3 148
	addi	r3, r3, 152
	jal	fiszero.2507
	addi	r3, r3, -152
	lwi	r30, r3 148
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9017
	flwl	%f1 l.6181
	flwi	f2, r3 16
	fdiv	f1, f1, f2
	lwi	r2, r3 8
	fswi	f1, r2 32
	jl	beq_cont.9018
beq_else.9017:
beq_cont.9018:
	lwi	r2, r3 8
	jr	r31
iter_setup_dirvec_constants.2796:
	lwi	r6, r24 4
	addi	r28, r0, 0
	slt	r29, r5, r28
	seq	r29, r29, r0
	jeql	r29, r0, bge_else.9019
	sli	r7, r5, 2
	add	r30 r6 r7
	lwi	r6, r30 0
	swi	r24, r3 0
	swi	r5, r3 4
	swi	r6, r3 8
	swi	r2, r3 12
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	d_const.2672
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r5, r3 12
	swi	r2, r3 16
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	d_vec.2670
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r5, r3 8
	swi	r2, r3 20
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	o_form.2611
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	addi	r28, r0, 1
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9020
	lwi	r2, r3 20
	lwi	r5, r3 8
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	setup_rect_table.2787
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r5, r3 4
	sli	r6, r5, 2
	lwi	r7, r3 16
	add	r30 r7 r6
	swi	r2, r30 0
	jl	beq_cont.9021
beq_else.9020:
	addi	r28, r0, 2
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9022
	lwi	r2, r3 20
	lwi	r5, r3 8
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	setup_surface_table.2790
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r5, r3 4
	sli	r6, r5, 2
	lwi	r7, r3 16
	add	r30 r7 r6
	swi	r2, r30 0
	jl	beq_cont.9023
beq_else.9022:
	lwi	r2, r3 20
	lwi	r5, r3 8
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	setup_second_table.2793
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r5, r3 4
	sli	r6, r5, 2
	lwi	r7, r3 16
	add	r30 r7 r6
	swi	r2, r30 0
beq_cont.9023:
beq_cont.9021:
	addi	r5, r5, -1
	lwi	r2, r3 12
	lwi	r24, r3 0
	lwi	r23, r24 0
	jr	r23
bge_else.9019:
	jr	r31
setup_dirvec_constants.2799:
	lwi	r5, r24 8
	lwi	r24, r24 4
	lwi	r5, r5 0
	addi	r5, r5, -1
	lwi	r23, r24 0
	jr	r23
setup_startp_constants.2801:
	lwi	r6, r24 4
	addi	r28, r0, 0
	slt	r29, r5, r28
	seq	r29, r29, r0
	jeql	r29, r0, bge_else.9025
	sli	r7, r5, 2
	add	r30 r6 r7
	lwi	r6, r30 0
	swi	r24, r3 0
	swi	r5, r3 4
	swi	r2, r3 8
	swi	r6, r3 12
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	o_param_ctbl.2649
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r5, r3 12
	swi	r2, r3 16
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	o_form.2611
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r5, r3 8
	flwi	f1, r5 0
	lwi	r6, r3 12
	swi	r2, r3 20
	fswi	f1, r3 24
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	o_param_x.2627
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	flwi	f2, r3 24
	fsub	f1, f2, f1
	lwi	r2, r3 16
	fswi	f1, r2 0
	lwi	r5, r3 8
	flwi	f1, r5 8
	lwi	r6, r3 12
	fswi	f1, r3 32
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	o_param_y.2629
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	flwi	f2, r3 32
	fsub	f1, f2, f1
	lwi	r2, r3 16
	fswi	f1, r2 8
	lwi	r5, r3 8
	flwi	f1, r5 16
	lwi	r6, r3 12
	fswi	f1, r3 40
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	o_param_z.2631
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	flwi	f2, r3 40
	fsub	f1, f2, f1
	lwi	r2, r3 16
	fswi	f1, r2 16
	lwi	r5, r3 20
	addi	r28, r0, 2
	seq	r29, r5, r28
	jeql	r29, r0, beq_else.9026
	lwi	r5, r3 12
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	o_param_abc.2625
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	lwi	r5, r3 16
	flwi	f1, r5 0
	flwi	f2, r5 8
	flwi	f3, r5 16
	addi	r30 r31 0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	veciprod2.2587
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	lwi	r2, r3 16
	fswi	f1, r2 24
	jl	beq_cont.9027
beq_else.9026:
	addi	r28, r0, 2
	sgt	r29, r5, r28
	seq	r29, r29, r0
	jeql	r29, r0, ble_else.9028
	jl	ble_cont.9029
ble_else.9028:
	flwi	f1, r2 0
	flwi	f2, r2 8
	flwi	f3, r2 16
	lwi	r6, r3 12
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	quadratic.2724
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	lwi	r2, r3 20
	addi	r28, r0, 3
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9030
	flwl	%f2 l.6181
	fsub	f1, f1, f2
	jl	beq_cont.9031
beq_else.9030:
beq_cont.9031:
	lwi	r2, r3 16
	fswi	f1, r2 24
ble_cont.9029:
beq_cont.9027:
	lwi	r2, r3 4
	addi	r5, r2, -1
	lwi	r2, r3 8
	lwi	r24, r3 0
	lwi	r23, r24 0
	jr	r23
bge_else.9025:
	jr	r31
setup_startp.2804:
	lwi	r5, r24 12
	lwi	r6, r24 8
	lwi	r7, r24 4
	swi	r2, r3 0
	swi	r6, r3 4
	swi	r7, r3 8
	addi	r30 r31 0
	addi	r23, r5 0 #
	addi	r5, r2 0 #
	addi	r2, r23 0 #
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	veccpy.2573
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	lwi	r2, r3 8
	lwi	r2, r2 0
	addi	r5, r2, -1
	lwi	r2, r3 0
	lwi	r24, r3 4
	lwi	r23, r24 0
	jr	r23
is_rect_outside.2806:
	fswi	f3, r3 0
	fswi	f2, r3 8
	swi	r2, r3 16
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	fabs.2499
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r2, r3 16
	fswi	f1, r3 24
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	o_param_a.2619
	addi	r3, r3, -40
	lwi	r30, r3 36
	fadd	f2, f1 f0
	addi	r31 r30 0
	flwi	f1, r3 24
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	fless.2509
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9034
	addi	r2, r0 0
	jl	beq_cont.9035
beq_else.9034:
	flwi	f1, r3 8
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	fabs.2499
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	lwi	r2, r3 16
	fswi	f1, r3 32
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	o_param_b.2621
	addi	r3, r3, -48
	lwi	r30, r3 44
	fadd	f2, f1 f0
	addi	r31 r30 0
	flwi	f1, r3 32
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	fless.2509
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9036
	addi	r2, r0 0
	jl	beq_cont.9037
beq_else.9036:
	flwi	f1, r3 0
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	fabs.2499
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	lwi	r2, r3 16
	fswi	f1, r3 40
	addi	r30 r31 0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	o_param_c.2623
	addi	r3, r3, -56
	lwi	r30, r3 52
	fadd	f2, f1 f0
	addi	r31 r30 0
	flwi	f1, r3 40
	addi	r30 r31 0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	fless.2509
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
beq_cont.9037:
beq_cont.9035:
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9038
	lwi	r2, r3 16
	addi	r30 r31 0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	o_isinvert.2615
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9039
	addi	r2, r0 1
	jr	r31
beq_else.9039:
	addi	r2, r0 0
	jr	r31
beq_else.9038:
	lwi	r2, r3 16
	jl	o_isinvert.2615
is_plane_outside.2811:
	swi	r2, r3 0
	fswi	f3, r3 8
	fswi	f2, r3 16
	fswi	f1, r3 24
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	o_param_abc.2625
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	flwi	f1, r3 24
	flwi	f2, r3 16
	flwi	f3, r3 8
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	veciprod2.2587
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	lwi	r2, r3 0
	fswi	f1, r3 32
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	o_isinvert.2615
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	flwi	f1, r3 32
	swi	r2, r3 40
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	fisneg.2503
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r5, r2 0 #
	addi	r31 r30 0
	lwi	r2, r3 40
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	xor.2552
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9041
	addi	r2, r0 1
	jr	r31
beq_else.9041:
	addi	r2, r0 0
	jr	r31
is_second_outside.2816:
	swi	r2, r3 0
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	quadratic.2724
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	lwi	r2, r3 0
	fswi	f1, r3 8
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	o_form.2611
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	addi	r28, r0, 3
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9043
	flwl	%f1 l.6181
	flwi	f2, r3 8
	fsub	f1, f2, f1
	jl	beq_cont.9044
beq_else.9043:
	flwi	f1, r3 8
beq_cont.9044:
	lwi	r2, r3 0
	fswi	f1, r3 16
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	o_isinvert.2615
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	flwi	f1, r3 16
	swi	r2, r3 24
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	fisneg.2503
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r5, r2 0 #
	addi	r31 r30 0
	lwi	r2, r3 24
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	xor.2552
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9045
	addi	r2, r0 1
	jr	r31
beq_else.9045:
	addi	r2, r0 0
	jr	r31
is_outside.2821:
	fswi	f3, r3 0
	fswi	f2, r3 8
	swi	r2, r3 16
	fswi	f1, r3 24
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	o_param_x.2627
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	flwi	f2, r3 24
	fsub	f1, f2, f1
	lwi	r2, r3 16
	fswi	f1, r3 32
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	o_param_y.2629
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	flwi	f2, r3 8
	fsub	f1, f2, f1
	lwi	r2, r3 16
	fswi	f1, r3 40
	addi	r30 r31 0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	o_param_z.2631
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	flwi	f2, r3 0
	fsub	f1, f2, f1
	lwi	r2, r3 16
	fswi	f1, r3 48
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	o_form.2611
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	addi	r28, r0, 1
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9047
	flwi	f1, r3 32
	flwi	f2, r3 40
	flwi	f3, r3 48
	lwi	r2, r3 16
	jl	is_rect_outside.2806
beq_else.9047:
	addi	r28, r0, 2
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9048
	flwi	f1, r3 32
	flwi	f2, r3 40
	flwi	f3, r3 48
	lwi	r2, r3 16
	jl	is_plane_outside.2811
beq_else.9048:
	flwi	f1, r3 32
	flwi	f2, r3 40
	flwi	f3, r3 48
	lwi	r2, r3 16
	jl	is_second_outside.2816
check_all_inside.2826:
	lwi	r6, r24 4
	sli	r7, r2, 2
	add	r30 r5 r7
	lwi	r7, r30 0
	addi	r28, r0, -1
	seq	r29, r7, r28
	jeql	r29, r0, beq_else.9049
	addi	r2, r0 1
	jr	r31
beq_else.9049:
	sli	r7, r7, 2
	add	r30 r6 r7
	lwi	r6, r30 0
	fswi	f3, r3 0
	fswi	f2, r3 8
	fswi	f1, r3 16
	swi	r5, r3 24
	swi	r24, r3 28
	swi	r2, r3 32
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	is_outside.2821
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9050
	lwi	r2, r3 32
	addi	r2, r2, 1
	flwi	f1, r3 16
	flwi	f2, r3 8
	flwi	f3, r3 0
	lwi	r5, r3 24
	lwi	r24, r3 28
	lwi	r23, r24 0
	jr	r23
beq_else.9050:
	addi	r2, r0 0
	jr	r31
shadow_check_and_group.2832:
	lwi	r6, r24 28
	lwi	r7, r24 24
	lwi	r8, r24 20
	lwi	r9, r24 16
	lwi	r10, r24 12
	lwi	r11, r24 8
	lwi	r12, r24 4
	sli	r13, r2, 2
	add	r30 r5 r13
	lwi	r13, r30 0
	addi	r28, r0, -1
	seq	r29, r13, r28
	jeql	r29, r0, beq_else.9051
	addi	r2, r0 0
	jr	r31
beq_else.9051:
	sli	r13, r2, 2
	add	r30 r5 r13
	lwi	r13, r30 0
	swi	r12, r3 0
	swi	r11, r3 4
	swi	r10, r3 8
	swi	r5, r3 12
	swi	r24, r3 16
	swi	r2, r3 20
	swi	r8, r3 24
	swi	r13, r3 28
	swi	r7, r3 32
	addi	r30 r31 0
	addi	r5, r9 0 #
	addi	r2, r13 0 #
	addi	r24, r6 0 #
	addi	r6, r11 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	lwi	r5, r3 32
	flwi	f1, r5 0
	fswi	f1, r3 40
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9053
	addi	r2, r0 0
	jl	beq_cont.9054
beq_else.9053:
	flwl	%f2 l.6502
	addi	r30 r31 0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	fless.2509
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
beq_cont.9054:
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9055
	lwi	r2, r3 28
	sli	r2, r2, 2
	lwi	r5, r3 24
	add	r30 r5 r2
	lwi	r2, r30 0
	addi	r30 r31 0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	o_isinvert.2615
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9056
	addi	r2, r0 0
	jr	r31
beq_else.9056:
	lwi	r2, r3 20
	addi	r2, r2, 1
	lwi	r5, r3 12
	lwi	r24, r3 16
	lwi	r23, r24 0
	jr	r23
beq_else.9055:
	flwl	%f1 l.6503
	flwi	f2, r3 40
	fadd	f1, f2, f1
	lwi	r2, r3 8
	flwi	f2, r2 0
	fmul	f2, f2, f1
	lwi	r5, r3 4
	flwi	f3, r5 0
	fadd	f2, f2, f3
	flwi	f3, r2 8
	fmul	f3, f3, f1
	flwi	f4, r5 8
	fadd	f3, f3, f4
	flwi	f4, r2 16
	fmul	f1, f4, f1
	flwi	f4, r5 16
	fadd	f1, f1, f4
	addi	r2, r0 0
	lwi	r5, r3 12
	lwi	r24, r3 0
	addi	r30 r31 0
	fadd	f31, f3 f0
	fadd	f3, f1 f0
	fadd	f1, f2 f0
	fadd	f2, f31 f0
	swi	r30, r3 52
	addi	r3, r3, 56
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9057
	lwi	r2, r3 20
	addi	r2, r2, 1
	lwi	r5, r3 12
	lwi	r24, r3 16
	lwi	r23, r24 0
	jr	r23
beq_else.9057:
	addi	r2, r0 1
	jr	r31
shadow_check_one_or_group.2835:
	lwi	r6, r24 8
	lwi	r7, r24 4
	sli	r8, r2, 2
	add	r30 r5 r8
	lwi	r8, r30 0
	addi	r28, r0, -1
	seq	r29, r8, r28
	jeql	r29, r0, beq_else.9058
	addi	r2, r0 0
	jr	r31
beq_else.9058:
	sli	r8, r8, 2
	add	r30 r7 r8
	lwi	r7, r30 0
	addi	r8, r0 0
	swi	r5, r3 0
	swi	r24, r3 4
	swi	r2, r3 8
	addi	r30 r31 0
	addi	r5, r7 0 #
	addi	r2, r8 0 #
	addi	r24, r6 0 #
	swi	r30, r3 12
	addi	r3, r3, 16
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9059
	lwi	r2, r3 8
	addi	r2, r2, 1
	lwi	r5, r3 0
	lwi	r24, r3 4
	lwi	r23, r24 0
	jr	r23
beq_else.9059:
	addi	r2, r0 1
	jr	r31
shadow_check_one_or_matrix.2838:
	lwi	r6, r24 20
	lwi	r7, r24 16
	lwi	r8, r24 12
	lwi	r9, r24 8
	lwi	r10, r24 4
	sli	r11, r2, 2
	add	r30 r5 r11
	lwi	r11, r30 0
	lwi	r12, r11 0
	addi	r28, r0, -1
	seq	r29, r12, r28
	jeql	r29, r0, beq_else.9060
	addi	r2, r0 0
	jr	r31
beq_else.9060:
	swi	r11, r3 0
	swi	r8, r3 4
	swi	r5, r3 8
	swi	r24, r3 12
	swi	r2, r3 16
	addi	r28, r0, 99
	seq	r29, r12, r28
	jeql	r29, r0, beq_else.9061
	addi	r2, r0 1
	jl	beq_cont.9062
beq_else.9061:
	swi	r7, r3 20
	addi	r30 r31 0
	addi	r5, r9 0 #
	addi	r2, r12 0 #
	addi	r24, r6 0 #
	addi	r6, r10 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9063
	addi	r2, r0 0
	jl	beq_cont.9064
beq_else.9063:
	lwi	r2, r3 20
	flwi	f1, r2 0
	flwl	%f2 l.6516
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	fless.2509
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9065
	addi	r2, r0 0
	jl	beq_cont.9066
beq_else.9065:
	addi	r2, r0 1
	lwi	r5, r3 0
	lwi	r24, r3 4
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9067
	addi	r2, r0 0
	jl	beq_cont.9068
beq_else.9067:
	addi	r2, r0 1
beq_cont.9068:
beq_cont.9066:
beq_cont.9064:
beq_cont.9062:
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9069
	lwi	r2, r3 16
	addi	r2, r2, 1
	lwi	r5, r3 8
	lwi	r24, r3 12
	lwi	r23, r24 0
	jr	r23
beq_else.9069:
	addi	r2, r0 1
	lwi	r5, r3 0
	lwi	r24, r3 4
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9070
	lwi	r2, r3 16
	addi	r2, r2, 1
	lwi	r5, r3 8
	lwi	r24, r3 12
	lwi	r23, r24 0
	jr	r23
beq_else.9070:
	addi	r2, r0 1
	jr	r31
solve_each_element.2841:
	lwi	r7, r24 36
	lwi	r8, r24 32
	lwi	r9, r24 28
	lwi	r10, r24 24
	lwi	r11, r24 20
	lwi	r12, r24 16
	lwi	r13, r24 12
	lwi	r14, r24 8
	lwi	r15, r24 4
	sli	r16, r2, 2
	add	r30 r5 r16
	lwi	r16, r30 0
	addi	r28, r0, -1
	seq	r29, r16, r28
	jeql	r29, r0, beq_else.9071
	jr	r31
beq_else.9071:
	swi	r12, r3 0
	swi	r14, r3 4
	swi	r13, r3 8
	swi	r15, r3 12
	swi	r8, r3 16
	swi	r7, r3 20
	swi	r9, r3 24
	swi	r6, r3 28
	swi	r5, r3 32
	swi	r24, r3 36
	swi	r2, r3 40
	swi	r11, r3 44
	swi	r16, r3 48
	addi	r30 r31 0
	addi	r5, r6 0 #
	addi	r2, r16 0 #
	addi	r24, r10 0 #
	addi	r6, r8 0 #
	swi	r30, r3 52
	addi	r3, r3, 56
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9073
	lwi	r2, r3 48
	sli	r2, r2, 2
	lwi	r5, r3 44
	add	r30 r5 r2
	lwi	r2, r30 0
	addi	r30 r31 0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	o_isinvert.2615
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9074
	jr	r31
beq_else.9074:
	lwi	r2, r3 40
	addi	r2, r2, 1
	lwi	r5, r3 32
	lwi	r6, r3 28
	lwi	r24, r3 36
	lwi	r23, r24 0
	jr	r23
beq_else.9073:
	lwi	r5, r3 24
	flwi	f2, r5 0
	flwl	%f1 l.6177
	swi	r2, r3 52
	fswi	f2, r3 56
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	fless.2509
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9076
	jl	beq_cont.9077
beq_else.9076:
	lwi	r2, r3 20
	flwi	f2, r2 0
	flwi	f1, r3 56
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	fless.2509
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9078
	jl	beq_cont.9079
beq_else.9078:
	flwl	%f1 l.6503
	flwi	f2, r3 56
	fadd	f1, f2, f1
	lwi	r2, r3 28
	flwi	f2, r2 0
	fmul	f2, f2, f1
	lwi	r5, r3 16
	flwi	f3, r5 0
	fadd	f2, f2, f3
	flwi	f3, r2 8
	fmul	f3, f3, f1
	flwi	f4, r5 8
	fadd	f3, f3, f4
	flwi	f4, r2 16
	fmul	f4, f4, f1
	flwi	f5, r5 16
	fadd	f4, f4, f5
	addi	r5, r0 0
	lwi	r6, r3 32
	lwi	r24, r3 12
	fswi	f4, r3 64
	fswi	f3, r3 72
	fswi	f2, r3 80
	fswi	f1, r3 88
	addi	r30 r31 0
	addi	r2, r5 0 #
	addi	r5, r6 0 #
	fadd	f1, f2 f0
	fadd	f2, f3 f0
	fadd	f3, f4 f0
	swi	r30, r3 100
	addi	r3, r3, 104
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -104
	lwi	r30, r3 100
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9080
	jl	beq_cont.9081
beq_else.9080:
	lwi	r2, r3 20
	flwi	f1, r3 88
	fswi	f1, r2 0
	flwi	f1, r3 80
	flwi	f2, r3 72
	flwi	f3, r3 64
	lwi	r2, r3 8
	addi	r30 r31 0
	swi	r30, r3 100
	addi	r3, r3, 104
	jal	vecset.2563
	addi	r3, r3, -104
	lwi	r30, r3 100
	addi	r31 r30 0
	lwi	r2, r3 4
	lwi	r5, r3 48
	swi	r5, r2 0
	lwi	r2, r3 0
	lwi	r5, r3 52
	swi	r5, r2 0
beq_cont.9081:
beq_cont.9079:
beq_cont.9077:
	lwi	r2, r3 40
	addi	r2, r2, 1
	lwi	r5, r3 32
	lwi	r6, r3 28
	lwi	r24, r3 36
	lwi	r23, r24 0
	jr	r23
solve_one_or_network.2845:
	lwi	r7, r24 8
	lwi	r8, r24 4
	sli	r9, r2, 2
	add	r30 r5 r9
	lwi	r9, r30 0
	addi	r28, r0, -1
	seq	r29, r9, r28
	jeql	r29, r0, beq_else.9082
	jr	r31
beq_else.9082:
	sli	r9, r9, 2
	add	r30 r8 r9
	lwi	r8, r30 0
	addi	r9, r0 0
	swi	r6, r3 0
	swi	r5, r3 4
	swi	r24, r3 8
	swi	r2, r3 12
	addi	r30 r31 0
	addi	r5, r8 0 #
	addi	r2, r9 0 #
	addi	r24, r7 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r2, r3 12
	addi	r2, r2, 1
	lwi	r5, r3 4
	lwi	r6, r3 0
	lwi	r24, r3 8
	lwi	r23, r24 0
	jr	r23
trace_or_matrix.2849:
	lwi	r7, r24 20
	lwi	r8, r24 16
	lwi	r9, r24 12
	lwi	r10, r24 8
	lwi	r11, r24 4
	sli	r12, r2, 2
	add	r30 r5 r12
	lwi	r12, r30 0
	lwi	r13, r12 0
	addi	r28, r0, -1
	seq	r29, r13, r28
	jeql	r29, r0, beq_else.9084
	jr	r31
beq_else.9084:
	swi	r6, r3 0
	swi	r5, r3 4
	swi	r24, r3 8
	swi	r2, r3 12
	addi	r28, r0, 99
	seq	r29, r13, r28
	jeql	r29, r0, beq_else.9086
	addi	r7, r0 1
	addi	r30 r31 0
	addi	r5, r12 0 #
	addi	r2, r7 0 #
	addi	r24, r11 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	jl	beq_cont.9087
beq_else.9086:
	swi	r12, r3 16
	swi	r11, r3 20
	swi	r7, r3 24
	swi	r9, r3 28
	addi	r30 r31 0
	addi	r5, r6 0 #
	addi	r2, r13 0 #
	addi	r24, r10 0 #
	addi	r6, r8 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9088
	jl	beq_cont.9089
beq_else.9088:
	lwi	r2, r3 28
	flwi	f1, r2 0
	lwi	r2, r3 24
	flwi	f2, r2 0
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	fless.2509
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9090
	jl	beq_cont.9091
beq_else.9090:
	addi	r2, r0 1
	lwi	r5, r3 16
	lwi	r6, r3 0
	lwi	r24, r3 20
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
beq_cont.9091:
beq_cont.9089:
beq_cont.9087:
	lwi	r2, r3 12
	addi	r2, r2, 1
	lwi	r5, r3 4
	lwi	r6, r3 0
	lwi	r24, r3 8
	lwi	r23, r24 0
	jr	r23
judge_intersection.2853:
	lwi	r5, r24 12
	lwi	r6, r24 8
	lwi	r7, r24 4
	flwl	%f1 l.6536
	fswi	f1, r6 0
	addi	r8, r0 0
	lwi	r7, r7 0
	swi	r6, r3 0
	addi	r30 r31 0
	addi	r6, r2 0 #
	addi	r24, r5 0 #
	addi	r5, r7 0 #
	addi	r2, r8 0 #
	swi	r30, r3 4
	addi	r3, r3, 8
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	lwi	r2, r3 0
	flwi	f2, r2 0
	flwl	%f1 l.6516
	fswi	f2, r3 8
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	fless.2509
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9093
	addi	r2, r0 0
	jr	r31
beq_else.9093:
	flwl	%f2 l.6540
	flwi	f1, r3 8
	jl	fless.2509
solve_each_element_fast.2855:
	lwi	r7, r24 36
	lwi	r8, r24 32
	lwi	r9, r24 28
	lwi	r10, r24 24
	lwi	r11, r24 20
	lwi	r12, r24 16
	lwi	r13, r24 12
	lwi	r14, r24 8
	lwi	r15, r24 4
	swi	r12, r3 0
	swi	r14, r3 4
	swi	r13, r3 8
	swi	r15, r3 12
	swi	r8, r3 16
	swi	r7, r3 20
	swi	r10, r3 24
	swi	r24, r3 28
	swi	r11, r3 32
	swi	r6, r3 36
	swi	r9, r3 40
	swi	r5, r3 44
	swi	r2, r3 48
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	d_vec.2670
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	lwi	r5, r3 48
	sli	r6, r5, 2
	lwi	r7, r3 44
	add	r30 r7 r6
	lwi	r6, r30 0
	addi	r28, r0, -1
	seq	r29, r6, r28
	jeql	r29, r0, beq_else.9094
	jr	r31
beq_else.9094:
	lwi	r8, r3 36
	lwi	r24, r3 40
	swi	r2, r3 52
	swi	r6, r3 56
	addi	r30 r31 0
	addi	r5, r8 0 #
	addi	r2, r6 0 #
	swi	r30, r3 60
	addi	r3, r3, 64
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9096
	lwi	r2, r3 56
	sli	r2, r2, 2
	lwi	r5, r3 32
	add	r30 r5 r2
	lwi	r2, r30 0
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	o_isinvert.2615
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9097
	jr	r31
beq_else.9097:
	lwi	r2, r3 48
	addi	r2, r2, 1
	lwi	r5, r3 44
	lwi	r6, r3 36
	lwi	r24, r3 28
	lwi	r23, r24 0
	jr	r23
beq_else.9096:
	lwi	r5, r3 24
	flwi	f2, r5 0
	flwl	%f1 l.6177
	swi	r2, r3 60
	fswi	f2, r3 64
	addi	r30 r31 0
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	fless.2509
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9099
	jl	beq_cont.9100
beq_else.9099:
	lwi	r2, r3 20
	flwi	f2, r2 0
	flwi	f1, r3 64
	addi	r30 r31 0
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	fless.2509
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9101
	jl	beq_cont.9102
beq_else.9101:
	flwl	%f1 l.6503
	flwi	f2, r3 64
	fadd	f1, f2, f1
	lwi	r2, r3 52
	flwi	f2, r2 0
	fmul	f2, f2, f1
	lwi	r5, r3 16
	flwi	f3, r5 0
	fadd	f2, f2, f3
	flwi	f3, r2 8
	fmul	f3, f3, f1
	flwi	f4, r5 8
	fadd	f3, f3, f4
	flwi	f4, r2 16
	fmul	f4, f4, f1
	flwi	f5, r5 16
	fadd	f4, f4, f5
	addi	r2, r0 0
	lwi	r5, r3 44
	lwi	r24, r3 12
	fswi	f4, r3 72
	fswi	f3, r3 80
	fswi	f2, r3 88
	fswi	f1, r3 96
	addi	r30 r31 0
	fadd	f1, f2 f0
	fadd	f2, f3 f0
	fadd	f3, f4 f0
	swi	r30, r3 108
	addi	r3, r3, 112
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -112
	lwi	r30, r3 108
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9103
	jl	beq_cont.9104
beq_else.9103:
	lwi	r2, r3 20
	flwi	f1, r3 96
	fswi	f1, r2 0
	flwi	f1, r3 88
	flwi	f2, r3 80
	flwi	f3, r3 72
	lwi	r2, r3 8
	addi	r30 r31 0
	swi	r30, r3 108
	addi	r3, r3, 112
	jal	vecset.2563
	addi	r3, r3, -112
	lwi	r30, r3 108
	addi	r31 r30 0
	lwi	r2, r3 4
	lwi	r5, r3 56
	swi	r5, r2 0
	lwi	r2, r3 0
	lwi	r5, r3 60
	swi	r5, r2 0
beq_cont.9104:
beq_cont.9102:
beq_cont.9100:
	lwi	r2, r3 48
	addi	r2, r2, 1
	lwi	r5, r3 44
	lwi	r6, r3 36
	lwi	r24, r3 28
	lwi	r23, r24 0
	jr	r23
solve_one_or_network_fast.2859:
	lwi	r7, r24 8
	lwi	r8, r24 4
	sli	r9, r2, 2
	add	r30 r5 r9
	lwi	r9, r30 0
	addi	r28, r0, -1
	seq	r29, r9, r28
	jeql	r29, r0, beq_else.9105
	jr	r31
beq_else.9105:
	sli	r9, r9, 2
	add	r30 r8 r9
	lwi	r8, r30 0
	addi	r9, r0 0
	swi	r6, r3 0
	swi	r5, r3 4
	swi	r24, r3 8
	swi	r2, r3 12
	addi	r30 r31 0
	addi	r5, r8 0 #
	addi	r2, r9 0 #
	addi	r24, r7 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r2, r3 12
	addi	r2, r2, 1
	lwi	r5, r3 4
	lwi	r6, r3 0
	lwi	r24, r3 8
	lwi	r23, r24 0
	jr	r23
trace_or_matrix_fast.2863:
	lwi	r7, r24 16
	lwi	r8, r24 12
	lwi	r9, r24 8
	lwi	r10, r24 4
	sli	r11, r2, 2
	add	r30 r5 r11
	lwi	r11, r30 0
	lwi	r12, r11 0
	addi	r28, r0, -1
	seq	r29, r12, r28
	jeql	r29, r0, beq_else.9107
	jr	r31
beq_else.9107:
	swi	r6, r3 0
	swi	r5, r3 4
	swi	r24, r3 8
	swi	r2, r3 12
	addi	r28, r0, 99
	seq	r29, r12, r28
	jeql	r29, r0, beq_else.9109
	addi	r7, r0 1
	addi	r30 r31 0
	addi	r5, r11 0 #
	addi	r2, r7 0 #
	addi	r24, r10 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	jl	beq_cont.9110
beq_else.9109:
	swi	r11, r3 16
	swi	r10, r3 20
	swi	r7, r3 24
	swi	r9, r3 28
	addi	r30 r31 0
	addi	r5, r6 0 #
	addi	r2, r12 0 #
	addi	r24, r8 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9111
	jl	beq_cont.9112
beq_else.9111:
	lwi	r2, r3 28
	flwi	f1, r2 0
	lwi	r2, r3 24
	flwi	f2, r2 0
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	fless.2509
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9113
	jl	beq_cont.9114
beq_else.9113:
	addi	r2, r0 1
	lwi	r5, r3 16
	lwi	r6, r3 0
	lwi	r24, r3 20
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
beq_cont.9114:
beq_cont.9112:
beq_cont.9110:
	lwi	r2, r3 12
	addi	r2, r2, 1
	lwi	r5, r3 4
	lwi	r6, r3 0
	lwi	r24, r3 8
	lwi	r23, r24 0
	jr	r23
judge_intersection_fast.2867:
	lwi	r5, r24 12
	lwi	r6, r24 8
	lwi	r7, r24 4
	flwl	%f1 l.6536
	fswi	f1, r6 0
	addi	r8, r0 0
	lwi	r7, r7 0
	swi	r6, r3 0
	addi	r30 r31 0
	addi	r6, r2 0 #
	addi	r24, r5 0 #
	addi	r5, r7 0 #
	addi	r2, r8 0 #
	swi	r30, r3 4
	addi	r3, r3, 8
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	lwi	r2, r3 0
	flwi	f2, r2 0
	flwl	%f1 l.6516
	fswi	f2, r3 8
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	fless.2509
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9116
	addi	r2, r0 0
	jr	r31
beq_else.9116:
	flwl	%f2 l.6540
	flwi	f1, r3 8
	jl	fless.2509
get_nvector_rect.2869:
	lwi	r5, r24 8
	lwi	r6, r24 4
	lwi	r6, r6 0
	swi	r5, r3 0
	swi	r2, r3 4
	swi	r6, r3 8
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	vecbzero.2571
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	lwi	r2, r3 8
	addi	r5, r2, -1
	addi	r2, r2, -1
	sli	r2, r2, 3
	lwi	r6, r3 4
	add	r30 r6 r2
	flwi	f1, r30 0
	swi	r5, r3 12
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	sgn.2555
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	fneg.2512
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r2, r3 12
	sli	r2, r2, 3
	lwi	r5, r3 0
	add	f29 r5 r2
	fswi	f1, f29 0
	jr	r31
get_nvector_plane.2871:
	lwi	r5, r24 4
	swi	r2, r3 0
	swi	r5, r3 4
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	o_param_a.2619
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	fneg.2512
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	lwi	r2, r3 4
	fswi	f1, r2 0
	lwi	r5, r3 0
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	o_param_b.2621
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	fneg.2512
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	lwi	r2, r3 4
	fswi	f1, r2 8
	lwi	r5, r3 0
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	o_param_c.2623
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	fneg.2512
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	lwi	r2, r3 4
	fswi	f1, r2 16
	jr	r31
get_nvector_second.2873:
	lwi	r5, r24 8
	lwi	r6, r24 4
	flwi	f1, r6 0
	swi	r5, r3 0
	swi	r2, r3 4
	swi	r6, r3 8
	fswi	f1, r3 16
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	o_param_x.2627
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	flwi	f2, r3 16
	fsub	f1, f2, f1
	lwi	r2, r3 8
	flwi	f2, r2 8
	lwi	r5, r3 4
	fswi	f1, r3 24
	fswi	f2, r3 32
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	o_param_y.2629
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	flwi	f2, r3 32
	fsub	f1, f2, f1
	lwi	r2, r3 8
	flwi	f2, r2 16
	lwi	r2, r3 4
	fswi	f1, r3 40
	fswi	f2, r3 48
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	o_param_z.2631
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	flwi	f2, r3 48
	fsub	f1, f2, f1
	lwi	r2, r3 4
	fswi	f1, r3 56
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	o_param_a.2619
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	flwi	f2, r3 24
	fmul	f1, f2, f1
	lwi	r2, r3 4
	fswi	f1, r3 64
	addi	r30 r31 0
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	o_param_b.2621
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	flwi	f2, r3 40
	fmul	f1, f2, f1
	lwi	r2, r3 4
	fswi	f1, r3 72
	addi	r30 r31 0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	o_param_c.2623
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	flwi	f2, r3 56
	fmul	f1, f2, f1
	lwi	r2, r3 4
	fswi	f1, r3 80
	addi	r30 r31 0
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	o_isrot.2617
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9120
	lwi	r2, r3 0
	flwi	f1, r3 64
	fswi	f1, r2 0
	flwi	f1, r3 72
	fswi	f1, r2 8
	flwi	f1, r3 80
	fswi	f1, r2 16
	jl	beq_cont.9121
beq_else.9120:
	lwi	r2, r3 4
	addi	r30 r31 0
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	o_param_r3.2647
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	flwi	f2, r3 40
	fmul	f1, f2, f1
	lwi	r2, r3 4
	fswi	f1, r3 88
	addi	r30 r31 0
	swi	r30, r3 100
	addi	r3, r3, 104
	jal	o_param_r2.2645
	addi	r3, r3, -104
	lwi	r30, r3 100
	addi	r31 r30 0
	flwi	f2, r3 56
	fmul	f1, f2, f1
	flwi	f3, r3 88
	fadd	f1, f3, f1
	addi	r30 r31 0
	swi	r30, r3 100
	addi	r3, r3, 104
	jal	fhalf.2501
	addi	r3, r3, -104
	lwi	r30, r3 100
	addi	r31 r30 0
	flwi	f2, r3 64
	fadd	f1, f2, f1
	lwi	r2, r3 0
	fswi	f1, r2 0
	lwi	r5, r3 4
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 100
	addi	r3, r3, 104
	jal	o_param_r3.2647
	addi	r3, r3, -104
	lwi	r30, r3 100
	addi	r31 r30 0
	flwi	f2, r3 24
	fmul	f1, f2, f1
	lwi	r2, r3 4
	fswi	f1, r3 96
	addi	r30 r31 0
	swi	r30, r3 108
	addi	r3, r3, 112
	jal	o_param_r1.2643
	addi	r3, r3, -112
	lwi	r30, r3 108
	addi	r31 r30 0
	flwi	f2, r3 56
	fmul	f1, f2, f1
	flwi	f2, r3 96
	fadd	f1, f2, f1
	addi	r30 r31 0
	swi	r30, r3 108
	addi	r3, r3, 112
	jal	fhalf.2501
	addi	r3, r3, -112
	lwi	r30, r3 108
	addi	r31 r30 0
	flwi	f2, r3 72
	fadd	f1, f2, f1
	lwi	r2, r3 0
	fswi	f1, r2 8
	lwi	r5, r3 4
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 108
	addi	r3, r3, 112
	jal	o_param_r2.2645
	addi	r3, r3, -112
	lwi	r30, r3 108
	addi	r31 r30 0
	flwi	f2, r3 24
	fmul	f1, f2, f1
	lwi	r2, r3 4
	fswi	f1, r3 104
	addi	r30 r31 0
	swi	r30, r3 116
	addi	r3, r3, 120
	jal	o_param_r1.2643
	addi	r3, r3, -120
	lwi	r30, r3 116
	addi	r31 r30 0
	flwi	f2, r3 40
	fmul	f1, f2, f1
	flwi	f2, r3 104
	fadd	f1, f2, f1
	addi	r30 r31 0
	swi	r30, r3 116
	addi	r3, r3, 120
	jal	fhalf.2501
	addi	r3, r3, -120
	lwi	r30, r3 116
	addi	r31 r30 0
	flwi	f2, r3 80
	fadd	f1, f2, f1
	lwi	r2, r3 0
	fswi	f1, r2 16
beq_cont.9121:
	lwi	r5, r3 4
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 116
	addi	r3, r3, 120
	jal	o_isinvert.2615
	addi	r3, r3, -120
	lwi	r30, r3 116
	addi	r5, r2 0 #
	addi	r31 r30 0
	lwi	r2, r3 0
	jl	vecunit_sgn.2581
get_nvector.2875:
	lwi	r6, r24 12
	lwi	r7, r24 8
	lwi	r8, r24 4
	swi	r6, r3 0
	swi	r2, r3 4
	swi	r8, r3 8
	swi	r5, r3 12
	swi	r7, r3 16
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	o_form.2611
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	addi	r28, r0, 1
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9122
	lwi	r2, r3 12
	lwi	r24, r3 16
	lwi	r23, r24 0
	jr	r23
beq_else.9122:
	addi	r28, r0, 2
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9123
	lwi	r2, r3 4
	lwi	r24, r3 8
	lwi	r23, r24 0
	jr	r23
beq_else.9123:
	lwi	r2, r3 4
	lwi	r24, r3 0
	lwi	r23, r24 0
	jr	r23
utexture.2878:
	lwi	r6, r24 4
	swi	r5, r3 0
	swi	r6, r3 4
	swi	r2, r3 8
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	o_texturetype.2609
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	lwi	r5, r3 8
	swi	r2, r3 12
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	o_color_red.2637
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r2, r3 4
	fswi	f1, r2 0
	lwi	r5, r3 8
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	o_color_green.2639
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r2, r3 4
	fswi	f1, r2 8
	lwi	r5, r3 8
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	o_color_blue.2641
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r2, r3 4
	fswi	f1, r2 16
	lwi	r5, r3 12
	addi	r28, r0, 1
	seq	r29, r5, r28
	jeql	r29, r0, beq_else.9124
	lwi	r5, r3 0
	flwi	f1, r5 0
	lwi	r6, r3 8
	fswi	f1, r3 16
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	o_param_x.2627
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	flwi	f2, r3 16
	fsub	f1, f2, f1
	flwl	%f2 l.6602
	fmul	f2, f1, f2
	fswi	f1, r3 24
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	min_caml_floor
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	flwl	%f2 l.6603
	fmul	f1, f1, f2
	flwi	f2, r3 24
	fsub	f1, f2, f1
	flwl	%f2 l.6594
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	fless.2509
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	lwi	r5, r3 0
	flwi	f1, r5 16
	lwi	r5, r3 8
	swi	r2, r3 32
	fswi	f1, r3 40
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	o_param_z.2631
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	flwi	f2, r3 40
	fsub	f1, f2, f1
	flwl	%f2 l.6602
	fmul	f2, f1, f2
	fswi	f1, r3 48
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	min_caml_floor
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	flwl	%f2 l.6603
	fmul	f1, f1, f2
	flwi	f2, r3 48
	fsub	f1, f2, f1
	flwl	%f2 l.6594
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	fless.2509
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	lwi	r5, r3 32
	addi	r28, r0, 0
	seq	r29, r5, r28
	jeql	r29, r0, beq_else.9126
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9128
	flwl	%f1 l.6589
	jl	beq_cont.9129
beq_else.9128:
	flwl	%f1 l.6177
beq_cont.9129:
	jl	beq_cont.9127
beq_else.9126:
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9130
	flwl	%f1 l.6177
	jl	beq_cont.9131
beq_else.9130:
	flwl	%f1 l.6589
beq_cont.9131:
beq_cont.9127:
	lwi	r2, r3 4
	fswi	f1, r2 8
	jr	r31
beq_else.9124:
	addi	r28, r0, 2
	seq	r29, r5, r28
	jeql	r29, r0, beq_else.9133
	lwi	r5, r3 0
	flwi	f1, r5 8
	flwl	%f2 l.6598
	fmul	f1, f1, f2
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	sin.2516
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	fsqr.2514
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	flwl	%f2 l.6589
	fmul	f2, f2, f1
	lwi	r2, r3 4
	fswi	f2, r2 0
	flwl	%f2 l.6589
	flwl	%f3 l.6181
	fsub	f1, f3, f1
	fmul	f1, f2, f1
	fswi	f1, r2 8
	jr	r31
beq_else.9133:
	addi	r28, r0, 3
	seq	r29, r5, r28
	jeql	r29, r0, beq_else.9135
	lwi	r5, r3 0
	flwi	f1, r5 0
	lwi	r6, r3 8
	fswi	f1, r3 56
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	o_param_x.2627
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	flwi	f2, r3 56
	fsub	f1, f2, f1
	lwi	r2, r3 0
	flwi	f2, r2 16
	lwi	r2, r3 8
	fswi	f1, r3 64
	fswi	f2, r3 72
	addi	r30 r31 0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	o_param_z.2631
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	flwi	f2, r3 72
	fsub	f1, f2, f1
	flwi	f2, r3 64
	fswi	f1, r3 80
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	fsqr.2514
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	flwi	f2, r3 80
	fswi	f1, r3 88
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 100
	addi	r3, r3, 104
	jal	fsqr.2514
	addi	r3, r3, -104
	lwi	r30, r3 100
	addi	r31 r30 0
	flwi	f2, r3 88
	fadd	f1, f2, f1
	addi	r30 r31 0
	swi	r30, r3 100
	addi	r3, r3, 104
	jal	min_caml_sqrt
	addi	r3, r3, -104
	lwi	r30, r3 100
	addi	r31 r30 0
	flwl	%f2 l.6594
	fdiv	f1, f1, f2
	fswi	f1, r3 96
	addi	r30 r31 0
	swi	r30, r3 108
	addi	r3, r3, 112
	jal	min_caml_floor
	addi	r3, r3, -112
	lwi	r30, r3 108
	addi	r31 r30 0
	flwi	f2, r3 96
	fsub	f1, f2, f1
	flwl	%f2 l.6586
	fmul	f1, f1, f2
	addi	r30 r31 0
	swi	r30, r3 108
	addi	r3, r3, 112
	jal	cos.2518
	addi	r3, r3, -112
	lwi	r30, r3 108
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 108
	addi	r3, r3, 112
	jal	fsqr.2514
	addi	r3, r3, -112
	lwi	r30, r3 108
	addi	r31 r30 0
	flwl	%f2 l.6589
	fmul	f2, f1, f2
	lwi	r2, r3 4
	fswi	f2, r2 8
	flwl	%f2 l.6181
	fsub	f1, f2, f1
	flwl	%f2 l.6589
	fmul	f1, f1, f2
	fswi	f1, r2 16
	jr	r31
beq_else.9135:
	addi	r28, r0, 4
	seq	r29, r5, r28
	jeql	r29, r0, beq_else.9137
	lwi	r5, r3 0
	flwi	f1, r5 0
	lwi	r6, r3 8
	fswi	f1, r3 104
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 116
	addi	r3, r3, 120
	jal	o_param_x.2627
	addi	r3, r3, -120
	lwi	r30, r3 116
	addi	r31 r30 0
	flwi	f2, r3 104
	fsub	f1, f2, f1
	lwi	r2, r3 8
	fswi	f1, r3 112
	addi	r30 r31 0
	swi	r30, r3 124
	addi	r3, r3, 128
	jal	o_param_a.2619
	addi	r3, r3, -128
	lwi	r30, r3 124
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 124
	addi	r3, r3, 128
	jal	min_caml_sqrt
	addi	r3, r3, -128
	lwi	r30, r3 124
	addi	r31 r30 0
	flwi	f2, r3 112
	fmul	f1, f2, f1
	lwi	r2, r3 0
	flwi	f2, r2 16
	lwi	r5, r3 8
	fswi	f1, r3 120
	fswi	f2, r3 128
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 140
	addi	r3, r3, 144
	jal	o_param_z.2631
	addi	r3, r3, -144
	lwi	r30, r3 140
	addi	r31 r30 0
	flwi	f2, r3 128
	fsub	f1, f2, f1
	lwi	r2, r3 8
	fswi	f1, r3 136
	addi	r30 r31 0
	swi	r30, r3 148
	addi	r3, r3, 152
	jal	o_param_c.2623
	addi	r3, r3, -152
	lwi	r30, r3 148
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 148
	addi	r3, r3, 152
	jal	min_caml_sqrt
	addi	r3, r3, -152
	lwi	r30, r3 148
	addi	r31 r30 0
	flwi	f2, r3 136
	fmul	f1, f2, f1
	flwi	f2, r3 120
	fswi	f1, r3 144
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 156
	addi	r3, r3, 160
	jal	fsqr.2514
	addi	r3, r3, -160
	lwi	r30, r3 156
	addi	r31 r30 0
	flwi	f2, r3 144
	fswi	f1, r3 152
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 164
	addi	r3, r3, 168
	jal	fsqr.2514
	addi	r3, r3, -168
	lwi	r30, r3 164
	addi	r31 r30 0
	flwi	f2, r3 152
	fadd	f1, f2, f1
	flwi	f2, r3 120
	fswi	f1, r3 160
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 172
	addi	r3, r3, 176
	jal	fabs.2499
	addi	r3, r3, -176
	lwi	r30, r3 172
	addi	r31 r30 0
	flwl	%f2 l.6583
	addi	r30 r31 0
	swi	r30, r3 172
	addi	r3, r3, 176
	jal	fless.2509
	addi	r3, r3, -176
	lwi	r30, r3 172
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9138
	flwi	f1, r3 120
	flwi	f2, r3 144
	fdiv	f1, f2, f1
	addi	r30 r31 0
	swi	r30, r3 172
	addi	r3, r3, 176
	jal	fabs.2499
	addi	r3, r3, -176
	lwi	r30, r3 172
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 172
	addi	r3, r3, 176
	jal	atan.2520
	addi	r3, r3, -176
	lwi	r30, r3 172
	addi	r31 r30 0
	flwl	%f2 l.6585
	fmul	f1, f1, f2
	flwl	%f2 l.6586
	fdiv	f1, f1, f2
	jl	beq_cont.9139
beq_else.9138:
	flwl	%f1 l.6584
beq_cont.9139:
	fswi	f1, r3 168
	addi	r30 r31 0
	swi	r30, r3 180
	addi	r3, r3, 184
	jal	min_caml_floor
	addi	r3, r3, -184
	lwi	r30, r3 180
	addi	r31 r30 0
	flwi	f2, r3 168
	fsub	f1, f2, f1
	lwi	r2, r3 0
	flwi	f2, r2 8
	lwi	r2, r3 8
	fswi	f1, r3 176
	fswi	f2, r3 184
	addi	r30 r31 0
	swi	r30, r3 196
	addi	r3, r3, 200
	jal	o_param_y.2629
	addi	r3, r3, -200
	lwi	r30, r3 196
	addi	r31 r30 0
	flwi	f2, r3 184
	fsub	f1, f2, f1
	lwi	r2, r3 8
	fswi	f1, r3 192
	addi	r30 r31 0
	swi	r30, r3 204
	addi	r3, r3, 208
	jal	o_param_b.2621
	addi	r3, r3, -208
	lwi	r30, r3 204
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 204
	addi	r3, r3, 208
	jal	min_caml_sqrt
	addi	r3, r3, -208
	lwi	r30, r3 204
	addi	r31 r30 0
	flwi	f2, r3 192
	fmul	f1, f2, f1
	flwi	f2, r3 160
	fswi	f1, r3 200
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 212
	addi	r3, r3, 216
	jal	fabs.2499
	addi	r3, r3, -216
	lwi	r30, r3 212
	addi	r31 r30 0
	flwl	%f2 l.6583
	addi	r30 r31 0
	swi	r30, r3 212
	addi	r3, r3, 216
	jal	fless.2509
	addi	r3, r3, -216
	lwi	r30, r3 212
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9140
	flwi	f1, r3 160
	flwi	f2, r3 200
	fdiv	f1, f2, f1
	addi	r30 r31 0
	swi	r30, r3 212
	addi	r3, r3, 216
	jal	fabs.2499
	addi	r3, r3, -216
	lwi	r30, r3 212
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 212
	addi	r3, r3, 216
	jal	atan.2520
	addi	r3, r3, -216
	lwi	r30, r3 212
	addi	r31 r30 0
	flwl	%f2 l.6585
	fmul	f1, f1, f2
	flwl	%f2 l.6586
	fdiv	f1, f1, f2
	jl	beq_cont.9141
beq_else.9140:
	flwl	%f1 l.6584
beq_cont.9141:
	fswi	f1, r3 208
	addi	r30 r31 0
	swi	r30, r3 220
	addi	r3, r3, 224
	jal	min_caml_floor
	addi	r3, r3, -224
	lwi	r30, r3 220
	addi	r31 r30 0
	flwi	f2, r3 208
	fsub	f1, f2, f1
	flwl	%f2 l.6588
	flwl	%f3 l.6178
	flwi	f4, r3 176
	fsub	f3, f3, f4
	fswi	f1, r3 216
	fswi	f2, r3 224
	addi	r30 r31 0
	fadd	f1, f3 f0
	swi	r30, r3 236
	addi	r3, r3, 240
	jal	fsqr.2514
	addi	r3, r3, -240
	lwi	r30, r3 236
	addi	r31 r30 0
	flwi	f2, r3 224
	fsub	f1, f2, f1
	flwl	%f2 l.6178
	flwi	f3, r3 216
	fsub	f2, f2, f3
	fswi	f1, r3 232
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 244
	addi	r3, r3, 248
	jal	fsqr.2514
	addi	r3, r3, -248
	lwi	r30, r3 244
	addi	r31 r30 0
	flwi	f2, r3 232
	fsub	f1, f2, f1
	fswi	f1, r3 240
	addi	r30 r31 0
	swi	r30, r3 252
	addi	r3, r3, 256
	jal	fisneg.2503
	addi	r3, r3, -256
	lwi	r30, r3 252
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9142
	flwi	f1, r3 240
	jl	beq_cont.9143
beq_else.9142:
	flwl	%f1 l.6177
beq_cont.9143:
	flwl	%f2 l.6589
	fmul	f1, f2, f1
	flwl	%f2 l.6590
	fdiv	f1, f1, f2
	lwi	r2, r3 4
	fswi	f1, r2 16
	jr	r31
beq_else.9137:
	jr	r31
add_light.2881:
	lwi	r2, r24 8
	lwi	r5, r24 4
	fswi	f3, r3 0
	fswi	f2, r3 8
	fswi	f1, r3 16
	swi	r2, r3 24
	swi	r5, r3 28
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	fispos.2505
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9146
	jl	beq_cont.9147
beq_else.9146:
	flwi	f1, r3 16
	lwi	r2, r3 28
	lwi	r5, r3 24
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	vecaccum.2592
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
beq_cont.9147:
	flwi	f1, r3 8
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	fispos.2505
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9148
	jr	r31
beq_else.9148:
	flwi	f1, r3 8
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	fsqr.2514
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	fsqr.2514
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	flwi	f2, r3 0
	fmul	f1, f1, f2
	lwi	r2, r3 28
	flwi	f2, r2 0
	fadd	f2, f2, f1
	fswi	f2, r2 0
	flwi	f2, r2 8
	fadd	f2, f2, f1
	fswi	f2, r2 8
	flwi	f2, r2 16
	fadd	f1, f2, f1
	fswi	f1, r2 16
	jr	r31
trace_reflections.2885:
	lwi	r6, r24 32
	lwi	r7, r24 28
	lwi	r8, r24 24
	lwi	r9, r24 20
	lwi	r10, r24 16
	lwi	r11, r24 12
	lwi	r12, r24 8
	lwi	r13, r24 4
	addi	r28, r0, 0
	slt	r29, r2, r28
	seq	r29, r29, r0
	jeql	r29, r0, bge_else.9151
	sli	r14, r2, 2
	add	r30 r7 r14
	lwi	r7, r30 0
	swi	r24, r3 0
	swi	r2, r3 4
	fswi	f2, r3 8
	swi	r13, r3 16
	swi	r5, r3 20
	fswi	f1, r3 24
	swi	r9, r3 32
	swi	r6, r3 36
	swi	r8, r3 40
	swi	r7, r3 44
	swi	r11, r3 48
	swi	r12, r3 52
	swi	r10, r3 56
	addi	r30 r31 0
	addi	r2, r7 0 #
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	r_dvec.2676
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	lwi	r24, r3 56
	swi	r2, r3 60
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9152
	jl	beq_cont.9153
beq_else.9152:
	lwi	r2, r3 52
	lwi	r2, r2 0
	addi	r5, r0 4
	mul	r2, r2, r5
	lwi	r5, r3 48
	lwi	r5, r5 0
	add	r2, r2, r5
	lwi	r5, r3 44
	swi	r2, r3 64
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	r_surface_id.2674
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	lwi	r5, r3 64
	seq	r29, r5, r2
	jeql	r29, r0, beq_else.9154
	addi	r2, r0 0
	lwi	r5, r3 40
	lwi	r5, r5 0
	lwi	r24, r3 36
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9156
	lwi	r2, r3 60
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	d_vec.2670
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r5, r2 0 #
	addi	r31 r30 0
	lwi	r2, r3 32
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	veciprod.2584
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	lwi	r2, r3 44
	fswi	f1, r3 72
	addi	r30 r31 0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	r_bright.2678
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	flwi	f2, r3 24
	fmul	f3, f1, f2
	flwi	f4, r3 72
	fmul	f3, f3, f4
	lwi	r2, r3 60
	fswi	f3, r3 80
	fswi	f1, r3 88
	addi	r30 r31 0
	swi	r30, r3 100
	addi	r3, r3, 104
	jal	d_vec.2670
	addi	r3, r3, -104
	lwi	r30, r3 100
	addi	r5, r2 0 #
	addi	r31 r30 0
	lwi	r2, r3 20
	addi	r30 r31 0
	swi	r30, r3 100
	addi	r3, r3, 104
	jal	veciprod.2584
	addi	r3, r3, -104
	lwi	r30, r3 100
	addi	r31 r30 0
	flwi	f2, r3 88
	fmul	f2, f2, f1
	flwi	f1, r3 80
	flwi	f3, r3 8
	lwi	r24, r3 16
	addi	r30 r31 0
	swi	r30, r3 100
	addi	r3, r3, 104
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -104
	lwi	r30, r3 100
	addi	r31 r30 0
	jl	beq_cont.9157
beq_else.9156:
beq_cont.9157:
	jl	beq_cont.9155
beq_else.9154:
beq_cont.9155:
beq_cont.9153:
	lwi	r2, r3 4
	addi	r2, r2, -1
	flwi	f1, r3 24
	flwi	f2, r3 8
	lwi	r5, r3 20
	lwi	r24, r3 0
	lwi	r23, r24 0
	jr	r23
bge_else.9151:
	jr	r31
trace_ray.2890:
	lwi	r7, r24 80
	lwi	r8, r24 76
	lwi	r9, r24 72
	lwi	r10, r24 68
	lwi	r11, r24 64
	lwi	r12, r24 60
	lwi	r13, r24 56
	lwi	r14, r24 52
	lwi	r15, r24 48
	lwi	r16, r24 44
	lwi	r17, r24 40
	lwi	r18, r24 36
	lwi	r19, r24 32
	lwi	r20, r24 28
	lwi	r21, r24 24
	lwi	r22, r24 20
	lwi	r23, r24 16
	swi	r24, r3 0
	lwi	r24, r24 12
	swi	r9, r3 4
	lwi	r9, r24 8
	swi	r8, r3 8
	lwi	r8, r24 4
	addi	r28, r0, 4
	sgt	r29, r2, r28
	seq	r29, r29, r0
	jeql	r29, r0, ble_else.9160
	fswi	f2, r3 16
	swi	r18, r3 24
	swi	r13, r3 28
	swi	r8, r3 32
	swi	r12, r3 36
	swi	r15, r3 40
	swi	r17, r3 44
	swi	r10, r3 48
	swi	r6, r3 52
	swi	r21, r3 56
	swi	r7, r3 60
	swi	r22, r3 64
	swi	r11, r3 68
	swi	r24, r3 72
	swi	r16, r3 76
	swi	r23, r3 80
	swi	r14, r3 84
	swi	r9, r3 88
	fswi	f1, r3 96
	swi	r19, r3 104
	swi	r2, r3 108
	swi	r5, r3 112
	swi	r20, r3 116
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 124
	addi	r3, r3, 128
	jal	p_surface_ids.2655
	addi	r3, r3, -128
	lwi	r30, r3 124
	addi	r31 r30 0
	lwi	r5, r3 112
	lwi	r24, r3 116
	swi	r2, r3 120
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 124
	addi	r3, r3, 128
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -128
	lwi	r30, r3 124
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9163
	addi	r2, r0 -1
	lwi	r5, r3 108
	sli	r6, r5, 2
	lwi	r7, r3 120
	add	r30 r7 r6
	swi	r2, r30 0
	addi	r28, r0, 0
	seq	r29, r5, r28
	jeql	r29, r0, beq_else.9164
	jr	r31
beq_else.9164:
	lwi	r2, r3 112
	lwi	r5, r3 104
	addi	r30 r31 0
	swi	r30, r3 124
	addi	r3, r3, 128
	jal	veciprod.2584
	addi	r3, r3, -128
	lwi	r30, r3 124
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 124
	addi	r3, r3, 128
	jal	fneg.2512
	addi	r3, r3, -128
	lwi	r30, r3 124
	addi	r31 r30 0
	fswi	f1, r3 128
	addi	r30 r31 0
	swi	r30, r3 140
	addi	r3, r3, 144
	jal	fispos.2505
	addi	r3, r3, -144
	lwi	r30, r3 140
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9167
	jr	r31
beq_else.9167:
	flwi	f1, r3 128
	addi	r30 r31 0
	swi	r30, r3 140
	addi	r3, r3, 144
	jal	fsqr.2514
	addi	r3, r3, -144
	lwi	r30, r3 140
	addi	r31 r30 0
	flwi	f2, r3 128
	fmul	f1, f1, f2
	flwi	f2, r3 96
	fmul	f1, f1, f2
	lwi	r2, r3 88
	flwi	f2, r2 0
	fmul	f1, f1, f2
	lwi	r2, r3 84
	flwi	f2, r2 0
	fadd	f2, f2, f1
	fswi	f2, r2 0
	flwi	f2, r2 8
	fadd	f2, f2, f1
	fswi	f2, r2 8
	flwi	f2, r2 16
	fadd	f1, f2, f1
	fswi	f1, r2 16
	jr	r31
beq_else.9163:
	lwi	r2, r3 80
	lwi	r2, r2 0
	sli	r5, r2, 2
	lwi	r6, r3 76
	add	r30 r6 r5
	lwi	r5, r30 0
	swi	r2, r3 136
	swi	r5, r3 140
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 148
	addi	r3, r3, 152
	jal	o_reflectiontype.2613
	addi	r3, r3, -152
	lwi	r30, r3 148
	addi	r31 r30 0
	lwi	r5, r3 140
	swi	r2, r3 144
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 148
	addi	r3, r3, 152
	jal	o_diffuse.2633
	addi	r3, r3, -152
	lwi	r30, r3 148
	addi	r31 r30 0
	flwi	f2, r3 96
	fmul	f1, f1, f2
	lwi	r2, r3 140
	lwi	r5, r3 112
	lwi	r24, r3 72
	fswi	f1, r3 152
	addi	r30 r31 0
	swi	r30, r3 164
	addi	r3, r3, 168
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -168
	lwi	r30, r3 164
	addi	r31 r30 0
	lwi	r2, r3 68
	lwi	r5, r3 64
	addi	r30 r31 0
	swi	r30, r3 164
	addi	r3, r3, 168
	jal	veccpy.2573
	addi	r3, r3, -168
	lwi	r30, r3 164
	addi	r31 r30 0
	lwi	r2, r3 140
	lwi	r5, r3 64
	lwi	r24, r3 60
	addi	r30 r31 0
	swi	r30, r3 164
	addi	r3, r3, 168
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -168
	lwi	r30, r3 164
	addi	r31 r30 0
	addi	r2, r0 4
	lwi	r5, r3 136
	mul	r2, r5, r2
	lwi	r5, r3 56
	lwi	r5, r5 0
	add	r2, r2, r5
	lwi	r5, r3 108
	sli	r6, r5, 2
	lwi	r7, r3 120
	add	r30 r7 r6
	swi	r2, r30 0
	lwi	r2, r3 52
	addi	r30 r31 0
	swi	r30, r3 164
	addi	r3, r3, 168
	jal	p_intersection_points.2653
	addi	r3, r3, -168
	lwi	r30, r3 164
	addi	r31 r30 0
	lwi	r5, r3 108
	sli	r6, r5, 2
	add	r30 r2 r6
	lwi	r2, r30 0
	lwi	r6, r3 64
	addi	r30 r31 0
	addi	r5, r6 0 #
	swi	r30, r3 164
	addi	r3, r3, 168
	jal	veccpy.2573
	addi	r3, r3, -168
	lwi	r30, r3 164
	addi	r31 r30 0
	lwi	r2, r3 52
	addi	r30 r31 0
	swi	r30, r3 164
	addi	r3, r3, 168
	jal	p_calc_diffuse.2657
	addi	r3, r3, -168
	lwi	r30, r3 164
	addi	r31 r30 0
	lwi	r5, r3 140
	swi	r2, r3 160
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 164
	addi	r3, r3, 168
	jal	o_diffuse.2633
	addi	r3, r3, -168
	lwi	r30, r3 164
	addi	r31 r30 0
	flwl	%f2 l.6178
	addi	r30 r31 0
	swi	r30, r3 164
	addi	r3, r3, 168
	jal	fless.2509
	addi	r3, r3, -168
	lwi	r30, r3 164
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9171
	addi	r2, r0 1
	lwi	r5, r3 108
	sli	r6, r5, 2
	lwi	r7, r3 160
	add	r30 r7 r6
	swi	r2, r30 0
	lwi	r2, r3 52
	addi	r30 r31 0
	swi	r30, r3 164
	addi	r3, r3, 168
	jal	p_energy.2659
	addi	r3, r3, -168
	lwi	r30, r3 164
	addi	r31 r30 0
	lwi	r5, r3 108
	sli	r6, r5, 2
	add	r30 r2 r6
	lwi	r6, r30 0
	lwi	r7, r3 48
	swi	r2, r3 164
	addi	r30 r31 0
	addi	r5, r7 0 #
	addi	r2, r6 0 #
	swi	r30, r3 172
	addi	r3, r3, 176
	jal	veccpy.2573
	addi	r3, r3, -176
	lwi	r30, r3 172
	addi	r31 r30 0
	lwi	r2, r3 108
	sli	r5, r2, 2
	lwi	r6, r3 164
	add	r30 r6 r5
	lwi	r5, r30 0
	flwl	%f1 l.6625
	flwi	f2, r3 152
	fmul	f1, f1, f2
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 172
	addi	r3, r3, 176
	jal	vecscale.2602
	addi	r3, r3, -176
	lwi	r30, r3 172
	addi	r31 r30 0
	lwi	r2, r3 52
	addi	r30 r31 0
	swi	r30, r3 172
	addi	r3, r3, 176
	jal	p_nvectors.2668
	addi	r3, r3, -176
	lwi	r30, r3 172
	addi	r31 r30 0
	lwi	r5, r3 108
	sli	r6, r5, 2
	add	r30 r2 r6
	lwi	r2, r30 0
	lwi	r6, r3 44
	addi	r30 r31 0
	addi	r5, r6 0 #
	swi	r30, r3 172
	addi	r3, r3, 176
	jal	veccpy.2573
	addi	r3, r3, -176
	lwi	r30, r3 172
	addi	r31 r30 0
	jl	beq_cont.9172
beq_else.9171:
	addi	r2, r0 0
	lwi	r5, r3 108
	sli	r6, r5, 2
	lwi	r7, r3 160
	add	r30 r7 r6
	swi	r2, r30 0
beq_cont.9172:
	flwl	%f1 l.6627
	lwi	r2, r3 112
	lwi	r5, r3 44
	fswi	f1, r3 168
	addi	r30 r31 0
	swi	r30, r3 180
	addi	r3, r3, 184
	jal	veciprod.2584
	addi	r3, r3, -184
	lwi	r30, r3 180
	addi	r31 r30 0
	flwi	f2, r3 168
	fmul	f1, f2, f1
	lwi	r2, r3 112
	lwi	r5, r3 44
	addi	r30 r31 0
	swi	r30, r3 180
	addi	r3, r3, 184
	jal	vecaccum.2592
	addi	r3, r3, -184
	lwi	r30, r3 180
	addi	r31 r30 0
	lwi	r2, r3 140
	addi	r30 r31 0
	swi	r30, r3 180
	addi	r3, r3, 184
	jal	o_hilight.2635
	addi	r3, r3, -184
	lwi	r30, r3 180
	addi	r31 r30 0
	flwi	f2, r3 96
	fmul	f1, f2, f1
	addi	r2, r0 0
	lwi	r5, r3 40
	lwi	r5, r5 0
	lwi	r24, r3 36
	fswi	f1, r3 176
	addi	r30 r31 0
	swi	r30, r3 188
	addi	r3, r3, 192
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -192
	lwi	r30, r3 188
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9173
	lwi	r2, r3 44
	lwi	r5, r3 104
	addi	r30 r31 0
	swi	r30, r3 188
	addi	r3, r3, 192
	jal	veciprod.2584
	addi	r3, r3, -192
	lwi	r30, r3 188
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 188
	addi	r3, r3, 192
	jal	fneg.2512
	addi	r3, r3, -192
	lwi	r30, r3 188
	addi	r31 r30 0
	flwi	f2, r3 152
	fmul	f1, f1, f2
	lwi	r2, r3 112
	lwi	r5, r3 104
	fswi	f1, r3 184
	addi	r30 r31 0
	swi	r30, r3 196
	addi	r3, r3, 200
	jal	veciprod.2584
	addi	r3, r3, -200
	lwi	r30, r3 196
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 196
	addi	r3, r3, 200
	jal	fneg.2512
	addi	r3, r3, -200
	lwi	r30, r3 196
	fadd	f2, f1 f0
	addi	r31 r30 0
	flwi	f1, r3 184
	flwi	f3, r3 176
	lwi	r24, r3 32
	addi	r30 r31 0
	swi	r30, r3 196
	addi	r3, r3, 200
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -200
	lwi	r30, r3 196
	addi	r31 r30 0
	jl	beq_cont.9174
beq_else.9173:
beq_cont.9174:
	lwi	r2, r3 64
	lwi	r24, r3 28
	addi	r30 r31 0
	swi	r30, r3 196
	addi	r3, r3, 200
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -200
	lwi	r30, r3 196
	addi	r31 r30 0
	lwi	r2, r3 24
	lwi	r2, r2 0
	addi	r2, r2, -1
	flwi	f1, r3 152
	flwi	f2, r3 176
	lwi	r5, r3 112
	lwi	r24, r3 8
	addi	r30 r31 0
	swi	r30, r3 196
	addi	r3, r3, 200
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -200
	lwi	r30, r3 196
	addi	r31 r30 0
	flwl	%f1 l.6630
	flwi	f2, r3 96
	addi	r30 r31 0
	swi	r30, r3 196
	addi	r3, r3, 200
	jal	fless.2509
	addi	r3, r3, -200
	lwi	r30, r3 196
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9175
	jr	r31
beq_else.9175:
	lwi	r2, r3 108
	addi	r28, r0, 4
	slt	r29, r2, r28
	seq	r29, r29, r0
	jeql	r29, r0, bge_else.9177
	jl	bge_cont.9178
bge_else.9177:
	addi	r5, r2, 1
	addi	r6, r0 -1
	sli	r5, r5, 2
	lwi	r7, r3 120
	add	r30 r7 r5
	swi	r6, r30 0
bge_cont.9178:
	lwi	r5, r3 144
	addi	r28, r0, 2
	seq	r29, r5, r28
	jeql	r29, r0, beq_else.9179
	flwl	%f1 l.6181
	lwi	r5, r3 140
	fswi	f1, r3 192
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 204
	addi	r3, r3, 208
	jal	o_diffuse.2633
	addi	r3, r3, -208
	lwi	r30, r3 204
	addi	r31 r30 0
	flwi	f2, r3 192
	fsub	f1, f2, f1
	flwi	f2, r3 96
	fmul	f1, f2, f1
	lwi	r2, r3 108
	addi	r2, r2, 1
	lwi	r5, r3 4
	flwi	f2, r5 0
	flwi	f3, r3 16
	fadd	f2, f3, f2
	lwi	r5, r3 112
	lwi	r6, r3 52
	lwi	r24, r3 0
	lwi	r23, r24 0
	jr	r23
beq_else.9179:
	jr	r31
ble_else.9160:
	jr	r31
trace_diffuse_ray.2896:
	lwi	r5, r24 48
	lwi	r6, r24 44
	lwi	r7, r24 40
	lwi	r8, r24 36
	lwi	r9, r24 32
	lwi	r10, r24 28
	lwi	r11, r24 24
	lwi	r12, r24 20
	lwi	r13, r24 16
	lwi	r14, r24 12
	lwi	r15, r24 8
	lwi	r16, r24 4
	swi	r6, r3 0
	swi	r16, r3 4
	fswi	f1, r3 8
	swi	r11, r3 16
	swi	r10, r3 20
	swi	r7, r3 24
	swi	r8, r3 28
	swi	r13, r3 32
	swi	r5, r3 36
	swi	r15, r3 40
	swi	r2, r3 44
	swi	r9, r3 48
	swi	r14, r3 52
	addi	r30 r31 0
	addi	r24, r12 0 #
	swi	r30, r3 60
	addi	r3, r3, 64
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9182
	jr	r31
beq_else.9182:
	lwi	r2, r3 52
	lwi	r2, r2 0
	sli	r2, r2, 2
	lwi	r5, r3 48
	add	r30 r5 r2
	lwi	r2, r30 0
	lwi	r5, r3 44
	swi	r2, r3 56
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	d_vec.2670
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r5, r2 0 #
	addi	r31 r30 0
	lwi	r2, r3 56
	lwi	r24, r3 40
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	lwi	r2, r3 56
	lwi	r5, r3 32
	lwi	r24, r3 36
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	addi	r2, r0 0
	lwi	r5, r3 28
	lwi	r5, r5 0
	lwi	r24, r3 24
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9184
	lwi	r2, r3 20
	lwi	r5, r3 16
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	veciprod.2584
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	fneg.2512
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	fswi	f1, r3 64
	addi	r30 r31 0
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	fispos.2505
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9186
	flwl	%f1 l.6177
	jl	beq_cont.9187
beq_else.9186:
	flwi	f1, r3 64
beq_cont.9187:
	flwi	f2, r3 8
	fmul	f1, f2, f1
	lwi	r2, r3 56
	fswi	f1, r3 72
	addi	r30 r31 0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	o_diffuse.2633
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	flwi	f2, r3 72
	fmul	f1, f2, f1
	lwi	r2, r3 4
	lwi	r5, r3 0
	jl	vecaccum.2592
beq_else.9184:
	jr	r31
iter_trace_diffuse_rays.2899:
	lwi	r8, r24 4
	addi	r28, r0, 0
	slt	r29, r7, r28
	seq	r29, r29, r0
	jeql	r29, r0, bge_else.9189
	sli	r9, r7, 2
	add	r30 r2 r9
	lwi	r9, r30 0
	swi	r6, r3 0
	swi	r24, r3 4
	swi	r8, r3 8
	swi	r2, r3 12
	swi	r7, r3 16
	swi	r5, r3 20
	addi	r30 r31 0
	addi	r2, r9 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	d_vec.2670
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r5, r3 20
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	veciprod.2584
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	fswi	f1, r3 24
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	fisneg.2503
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9190
	lwi	r2, r3 16
	sli	r5, r2, 2
	lwi	r6, r3 12
	add	r30 r6 r5
	lwi	r5, r30 0
	flwl	%f1 l.6648
	flwi	f2, r3 24
	fdiv	f1, f2, f1
	lwi	r24, r3 8
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	jl	beq_cont.9191
beq_else.9190:
	lwi	r2, r3 16
	addi	r5, r2, 1
	sli	r5, r5, 2
	lwi	r6, r3 12
	add	r30 r6 r5
	lwi	r5, r30 0
	flwl	%f1 l.6646
	flwi	f2, r3 24
	fdiv	f1, f2, f1
	lwi	r24, r3 8
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
beq_cont.9191:
	lwi	r2, r3 16
	addi	r7, r2, -2
	lwi	r2, r3 12
	lwi	r5, r3 20
	lwi	r6, r3 0
	lwi	r24, r3 4
	lwi	r23, r24 0
	jr	r23
bge_else.9189:
	jr	r31
trace_diffuse_rays.2904:
	lwi	r7, r24 8
	lwi	r8, r24 4
	swi	r6, r3 0
	swi	r5, r3 4
	swi	r2, r3 8
	swi	r8, r3 12
	addi	r30 r31 0
	addi	r2, r6 0 #
	addi	r24, r7 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	addi	r7, r0 118
	lwi	r2, r3 8
	lwi	r5, r3 4
	lwi	r6, r3 0
	lwi	r24, r3 12
	lwi	r23, r24 0
	jr	r23
trace_diffuse_ray_80percent.2908:
	lwi	r7, r24 8
	lwi	r8, r24 4
	swi	r6, r3 0
	swi	r5, r3 4
	swi	r7, r3 8
	swi	r8, r3 12
	swi	r2, r3 16
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9193
	jl	beq_cont.9194
beq_else.9193:
	lwi	r9, r8 0
	addi	r30 r31 0
	addi	r2, r9 0 #
	addi	r24, r7 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
beq_cont.9194:
	lwi	r2, r3 16
	addi	r28, r0, 1
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9195
	jl	beq_cont.9196
beq_else.9195:
	lwi	r5, r3 12
	lwi	r6, r5 4
	lwi	r7, r3 4
	lwi	r8, r3 0
	lwi	r24, r3 8
	addi	r30 r31 0
	addi	r5, r7 0 #
	addi	r2, r6 0 #
	addi	r6, r8 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
beq_cont.9196:
	lwi	r2, r3 16
	addi	r28, r0, 2
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9197
	jl	beq_cont.9198
beq_else.9197:
	lwi	r5, r3 12
	lwi	r6, r5 8
	lwi	r7, r3 4
	lwi	r8, r3 0
	lwi	r24, r3 8
	addi	r30 r31 0
	addi	r5, r7 0 #
	addi	r2, r6 0 #
	addi	r6, r8 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
beq_cont.9198:
	lwi	r2, r3 16
	addi	r28, r0, 3
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9199
	jl	beq_cont.9200
beq_else.9199:
	lwi	r5, r3 12
	lwi	r6, r5 12
	lwi	r7, r3 4
	lwi	r8, r3 0
	lwi	r24, r3 8
	addi	r30 r31 0
	addi	r5, r7 0 #
	addi	r2, r6 0 #
	addi	r6, r8 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
beq_cont.9200:
	lwi	r2, r3 16
	addi	r28, r0, 4
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9201
	jr	r31
beq_else.9201:
	lwi	r2, r3 12
	lwi	r2, r2 16
	lwi	r5, r3 4
	lwi	r6, r3 0
	lwi	r24, r3 8
	lwi	r23, r24 0
	jr	r23
calc_diffuse_using_1point.2912:
	lwi	r6, r24 12
	lwi	r7, r24 8
	lwi	r8, r24 4
	swi	r7, r3 0
	swi	r6, r3 4
	swi	r8, r3 8
	swi	r5, r3 12
	swi	r2, r3 16
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	p_received_ray_20percent.2661
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r5, r3 16
	swi	r2, r3 20
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	p_nvectors.2668
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r5, r3 16
	swi	r2, r3 24
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	p_intersection_points.2653
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r5, r3 16
	swi	r2, r3 28
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	p_energy.2659
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	lwi	r5, r3 12
	sli	r6, r5, 2
	lwi	r7, r3 20
	add	r30 r7 r6
	lwi	r6, r30 0
	lwi	r7, r3 8
	swi	r2, r3 32
	addi	r30 r31 0
	addi	r5, r6 0 #
	addi	r2, r7 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	veccpy.2573
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	lwi	r2, r3 16
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	p_group_id.2663
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	lwi	r5, r3 12
	sli	r6, r5, 2
	lwi	r7, r3 24
	add	r30 r7 r6
	lwi	r6, r30 0
	sli	r7, r5, 2
	lwi	r8, r3 28
	add	r30 r8 r7
	lwi	r7, r30 0
	lwi	r24, r3 4
	addi	r30 r31 0
	addi	r5, r6 0 #
	addi	r6, r7 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	lwi	r2, r3 12
	sli	r2, r2, 2
	lwi	r5, r3 32
	add	r30 r5 r2
	lwi	r5, r30 0
	lwi	r2, r3 0
	lwi	r6, r3 8
	jl	vecaccumv.2605
calc_diffuse_using_5points.2915:
	lwi	r9, r24 8
	lwi	r10, r24 4
	sli	r11, r2, 2
	add	r30 r5 r11
	lwi	r5, r30 0
	swi	r9, r3 0
	swi	r10, r3 4
	swi	r8, r3 8
	swi	r7, r3 12
	swi	r6, r3 16
	swi	r2, r3 20
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	p_received_ray_20percent.2661
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r5, r3 20
	addi	r6, r5, -1
	sli	r6, r6, 2
	lwi	r7, r3 16
	add	r30 r7 r6
	lwi	r6, r30 0
	swi	r2, r3 24
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	p_received_ray_20percent.2661
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r5, r3 20
	sli	r6, r5, 2
	lwi	r7, r3 16
	add	r30 r7 r6
	lwi	r6, r30 0
	swi	r2, r3 28
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	p_received_ray_20percent.2661
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	lwi	r5, r3 20
	addi	r6, r5, 1
	sli	r6, r6, 2
	lwi	r7, r3 16
	add	r30 r7 r6
	lwi	r6, r30 0
	swi	r2, r3 32
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	p_received_ray_20percent.2661
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	lwi	r5, r3 20
	sli	r6, r5, 2
	lwi	r7, r3 12
	add	r30 r7 r6
	lwi	r6, r30 0
	swi	r2, r3 36
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	p_received_ray_20percent.2661
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	lwi	r5, r3 8
	sli	r6, r5, 2
	lwi	r7, r3 24
	add	r30 r7 r6
	lwi	r6, r30 0
	lwi	r7, r3 4
	swi	r2, r3 40
	addi	r30 r31 0
	addi	r5, r6 0 #
	addi	r2, r7 0 #
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	veccpy.2573
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	lwi	r2, r3 8
	sli	r5, r2, 2
	lwi	r6, r3 28
	add	r30 r6 r5
	lwi	r5, r30 0
	lwi	r6, r3 4
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	vecadd.2596
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	lwi	r2, r3 8
	sli	r5, r2, 2
	lwi	r6, r3 32
	add	r30 r6 r5
	lwi	r5, r30 0
	lwi	r6, r3 4
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	vecadd.2596
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	lwi	r2, r3 8
	sli	r5, r2, 2
	lwi	r6, r3 36
	add	r30 r6 r5
	lwi	r5, r30 0
	lwi	r6, r3 4
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	vecadd.2596
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	lwi	r2, r3 8
	sli	r5, r2, 2
	lwi	r6, r3 40
	add	r30 r6 r5
	lwi	r5, r30 0
	lwi	r6, r3 4
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	vecadd.2596
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	lwi	r2, r3 20
	sli	r2, r2, 2
	lwi	r5, r3 16
	add	r30 r5 r2
	lwi	r2, r30 0
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	p_energy.2659
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	lwi	r5, r3 8
	sli	r5, r5, 2
	add	r30 r2 r5
	lwi	r5, r30 0
	lwi	r2, r3 0
	lwi	r6, r3 4
	jl	vecaccumv.2605
do_without_neighbors.2921:
	lwi	r6, r24 4
	addi	r28, r0, 4
	sgt	r29, r5, r28
	seq	r29, r29, r0
	jeql	r29, r0, ble_else.9203
	swi	r24, r3 0
	swi	r6, r3 4
	swi	r2, r3 8
	swi	r5, r3 12
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	p_surface_ids.2655
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r5, r3 12
	sli	r6, r5, 2
	add	r30 r2 r6
	lwi	r2, r30 0
	addi	r28, r0, 0
	slt	r29, r2, r28
	seq	r29, r29, r0
	jeql	r29, r0, bge_else.9204
	lwi	r2, r3 8
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	p_calc_diffuse.2657
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r5, r3 12
	sli	r6, r5, 2
	add	r30 r2 r6
	lwi	r2, r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9205
	jl	beq_cont.9206
beq_else.9205:
	lwi	r2, r3 8
	lwi	r24, r3 4
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
beq_cont.9206:
	lwi	r2, r3 12
	addi	r5, r2, 1
	lwi	r2, r3 8
	lwi	r24, r3 0
	lwi	r23, r24 0
	jr	r23
bge_else.9204:
	jr	r31
ble_else.9203:
	jr	r31
neighbors_exist.2924:
	lwi	r6, r24 4
	lwi	r7, r6 4
	addi	r8, r5, 1
	sgt	r29, r7, r8
	seq	r29, r29, r0
	jeql	r29, r0, ble_else.9209
	addi	r2, r0 0
	jr	r31
ble_else.9209:
	addi	r28, r0, 0
	sgt	r29, r5, r28
	seq	r29, r29, r0
	jeql	r29, r0, ble_else.9210
	addi	r2, r0 0
	jr	r31
ble_else.9210:
	lwi	r5, r6 0
	addi	r6, r2, 1
	sgt	r29, r5, r6
	seq	r29, r29, r0
	jeql	r29, r0, ble_else.9211
	addi	r2, r0 0
	jr	r31
ble_else.9211:
	addi	r28, r0, 0
	sgt	r29, r2, r28
	seq	r29, r29, r0
	jeql	r29, r0, ble_else.9212
	addi	r2, r0 0
	jr	r31
ble_else.9212:
	addi	r2, r0 1
	jr	r31
get_surface_id.2928:
	swi	r5, r3 0
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	p_surface_ids.2655
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	lwi	r5, r3 0
	sli	r5, r5, 2
	add	r30 r2 r5
	lwi	r2, r30 0
	jr	r31
neighbors_are_available.2931:
	sli	r9, r2, 2
	add	r30 r6 r9
	lwi	r9, r30 0
	swi	r6, r3 0
	swi	r7, r3 4
	swi	r8, r3 8
	swi	r5, r3 12
	swi	r2, r3 16
	addi	r30 r31 0
	addi	r5, r8 0 #
	addi	r2, r9 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	get_surface_id.2928
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r5, r3 16
	sli	r6, r5, 2
	lwi	r7, r3 12
	add	r30 r7 r6
	lwi	r6, r30 0
	lwi	r7, r3 8
	swi	r2, r3 20
	addi	r30 r31 0
	addi	r5, r7 0 #
	addi	r2, r6 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	get_surface_id.2928
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r5, r3 20
	seq	r29, r2, r5
	jeql	r29, r0, beq_else.9213
	lwi	r2, r3 16
	sli	r6, r2, 2
	lwi	r7, r3 4
	add	r30 r7 r6
	lwi	r6, r30 0
	lwi	r7, r3 8
	addi	r30 r31 0
	addi	r5, r7 0 #
	addi	r2, r6 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	get_surface_id.2928
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r5, r3 20
	seq	r29, r2, r5
	jeql	r29, r0, beq_else.9214
	lwi	r2, r3 16
	addi	r6, r2, -1
	sli	r6, r6, 2
	lwi	r7, r3 0
	add	r30 r7 r6
	lwi	r6, r30 0
	lwi	r8, r3 8
	addi	r30 r31 0
	addi	r5, r8 0 #
	addi	r2, r6 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	get_surface_id.2928
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r5, r3 20
	seq	r29, r2, r5
	jeql	r29, r0, beq_else.9215
	lwi	r2, r3 16
	addi	r2, r2, 1
	sli	r2, r2, 2
	lwi	r6, r3 0
	add	r30 r6 r2
	lwi	r2, r30 0
	lwi	r6, r3 8
	addi	r30 r31 0
	addi	r5, r6 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	get_surface_id.2928
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r5, r3 20
	seq	r29, r2, r5
	jeql	r29, r0, beq_else.9216
	addi	r2, r0 1
	jr	r31
beq_else.9216:
	addi	r2, r0 0
	jr	r31
beq_else.9215:
	addi	r2, r0 0
	jr	r31
beq_else.9214:
	addi	r2, r0 0
	jr	r31
beq_else.9213:
	addi	r2, r0 0
	jr	r31
try_exploit_neighbors.2937:
	lwi	r10, r24 8
	lwi	r11, r24 4
	sli	r12, r2, 2
	add	r30 r7 r12
	lwi	r12, r30 0
	addi	r28, r0, 4
	sgt	r29, r9, r28
	seq	r29, r29, r0
	jeql	r29, r0, ble_else.9217
	swi	r5, r3 0
	swi	r24, r3 4
	swi	r11, r3 8
	swi	r12, r3 12
	swi	r10, r3 16
	swi	r9, r3 20
	swi	r8, r3 24
	swi	r7, r3 28
	swi	r6, r3 32
	swi	r2, r3 36
	addi	r30 r31 0
	addi	r5, r9 0 #
	addi	r2, r12 0 #
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	get_surface_id.2928
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	addi	r28, r0, 0
	slt	r29, r2, r28
	seq	r29, r29, r0
	jeql	r29, r0, bge_else.9218
	lwi	r2, r3 36
	lwi	r5, r3 32
	lwi	r6, r3 28
	lwi	r7, r3 24
	lwi	r8, r3 20
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	neighbors_are_available.2931
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9219
	lwi	r2, r3 36
	sli	r2, r2, 2
	lwi	r5, r3 28
	add	r30 r5 r2
	lwi	r2, r30 0
	lwi	r5, r3 20
	lwi	r24, r3 16
	lwi	r23, r24 0
	jr	r23
beq_else.9219:
	lwi	r2, r3 12
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	p_calc_diffuse.2657
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	lwi	r8, r3 20
	sli	r5, r8, 2
	add	r30 r2 r5
	lwi	r2, r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9220
	jl	beq_cont.9221
beq_else.9220:
	lwi	r2, r3 36
	lwi	r5, r3 32
	lwi	r6, r3 28
	lwi	r7, r3 24
	lwi	r24, r3 8
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
beq_cont.9221:
	lwi	r2, r3 20
	addi	r9, r2, 1
	lwi	r2, r3 36
	lwi	r5, r3 0
	lwi	r6, r3 32
	lwi	r7, r3 28
	lwi	r8, r3 24
	lwi	r24, r3 4
	lwi	r23, r24 0
	jr	r23
bge_else.9218:
	jr	r31
ble_else.9217:
	jr	r31
write_ppm_header.2944:
	lwi	r2, r24 4
	addi	r5, r0 80
	swi	r2, r3 0
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_print_char
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	addi	r2, r0 51
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_print_char
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	addi	r2, r0 10
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_print_char
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	lwi	r2, r3 0
	lwi	r5, r2 0
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_print_int
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	addi	r2, r0 32
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_print_char
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	lwi	r2, r3 0
	lwi	r2, r2 4
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_print_int
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	addi	r2, r0 32
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_print_char
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	addi	r2, r0 255
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_print_int
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	addi	r2, r0 10
	jl	min_caml_print_char
write_rgb_element.2946:
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_int_of_float
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	addi	r28, r0, 255
	sgt	r29, r2, r28
	seq	r29, r29, r0
	jeql	r29, r0, ble_else.9224
	addi	r28, r0, 0
	slt	r29, r2, r28
	seq	r29, r29, r0
	jeql	r29, r0, bge_else.9226
	jl	bge_cont.9227
bge_else.9226:
	addi	r2, r0 0
bge_cont.9227:
	jl	ble_cont.9225
ble_else.9224:
	addi	r2, r0 255
ble_cont.9225:
	jl	min_caml_print_int
write_rgb.2948:
	lwi	r2, r24 4
	flwi	f1, r2 0
	swi	r2, r3 0
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	write_rgb_element.2946
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	addi	r2, r0 32
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_print_char
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	lwi	r2, r3 0
	flwi	f1, r2 8
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	write_rgb_element.2946
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	addi	r2, r0 32
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_print_char
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	lwi	r2, r3 0
	flwi	f1, r2 16
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	write_rgb_element.2946
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	addi	r2, r0 10
	jl	min_caml_print_char
pretrace_diffuse_rays.2950:
	lwi	r6, r24 12
	lwi	r7, r24 8
	lwi	r8, r24 4
	addi	r28, r0, 4
	sgt	r29, r5, r28
	seq	r29, r29, r0
	jeql	r29, r0, ble_else.9228
	swi	r24, r3 0
	swi	r6, r3 4
	swi	r7, r3 8
	swi	r8, r3 12
	swi	r5, r3 16
	swi	r2, r3 20
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	get_surface_id.2928
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	addi	r28, r0, 0
	slt	r29, r2, r28
	seq	r29, r29, r0
	jeql	r29, r0, bge_else.9229
	lwi	r2, r3 20
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	p_calc_diffuse.2657
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r5, r3 16
	sli	r6, r5, 2
	add	r30 r2 r6
	lwi	r2, r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9230
	jl	beq_cont.9231
beq_else.9230:
	lwi	r2, r3 20
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	p_group_id.2663
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r5, r3 12
	swi	r2, r3 24
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	vecbzero.2571
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r2, r3 20
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	p_nvectors.2668
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r5, r3 20
	swi	r2, r3 28
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	p_intersection_points.2653
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	lwi	r5, r3 24
	sli	r5, r5, 2
	lwi	r6, r3 8
	add	r30 r6 r5
	lwi	r5, r30 0
	lwi	r6, r3 16
	sli	r7, r6, 2
	lwi	r8, r3 28
	add	r30 r8 r7
	lwi	r7, r30 0
	sli	r8, r6, 2
	add	r30 r2 r8
	lwi	r2, r30 0
	lwi	r24, r3 4
	addi	r30 r31 0
	addi	r6, r2 0 #
	addi	r2, r5 0 #
	addi	r5, r7 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	lwi	r2, r3 20
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	p_received_ray_20percent.2661
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	lwi	r5, r3 16
	sli	r6, r5, 2
	add	r30 r2 r6
	lwi	r2, r30 0
	lwi	r6, r3 12
	addi	r30 r31 0
	addi	r5, r6 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	veccpy.2573
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
beq_cont.9231:
	lwi	r2, r3 16
	addi	r5, r2, 1
	lwi	r2, r3 20
	lwi	r24, r3 0
	lwi	r23, r24 0
	jr	r23
bge_else.9229:
	jr	r31
ble_else.9228:
	jr	r31
pretrace_pixels.2953:
	lwi	r7, r24 36
	lwi	r8, r24 32
	lwi	r9, r24 28
	lwi	r10, r24 24
	lwi	r11, r24 20
	lwi	r12, r24 16
	lwi	r13, r24 12
	lwi	r14, r24 8
	lwi	r15, r24 4
	addi	r28, r0, 0
	slt	r29, r5, r28
	seq	r29, r29, r0
	jeql	r29, r0, bge_else.9234
	flwi	f4, r11 0
	lwi	r11, r15 0
	sub	r11, r5, r11
	swi	r24, r3 0
	swi	r14, r3 4
	swi	r6, r3 8
	swi	r8, r3 12
	swi	r2, r3 16
	swi	r5, r3 20
	swi	r7, r3 24
	swi	r9, r3 28
	swi	r12, r3 32
	fswi	f3, r3 40
	fswi	f2, r3 48
	swi	r13, r3 56
	fswi	f1, r3 64
	swi	r10, r3 72
	fswi	f4, r3 80
	addi	r30 r31 0
	addi	r2, r11 0 #
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	min_caml_float_of_int
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	flwi	f2, r3 80
	fmul	f1, f2, f1
	lwi	r2, r3 72
	flwi	f2, r2 0
	fmul	f2, f1, f2
	flwi	f3, r3 64
	fadd	f2, f2, f3
	lwi	r5, r3 56
	fswi	f2, r5 0
	flwi	f2, r2 8
	fmul	f2, f1, f2
	flwi	f4, r3 48
	fadd	f2, f2, f4
	fswi	f2, r5 8
	flwi	f2, r2 16
	fmul	f1, f1, f2
	flwi	f2, r3 40
	fadd	f1, f1, f2
	fswi	f1, r5 16
	addi	r2, r0 0
	addi	r30 r31 0
	addi	r23, r5 0 #
	addi	r5, r2 0 #
	addi	r2, r23 0 #
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	vecunit_sgn.2581
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	lwi	r2, r3 32
	addi	r30 r31 0
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	vecbzero.2571
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	lwi	r2, r3 28
	lwi	r5, r3 24
	addi	r30 r31 0
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	veccpy.2573
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	addi	r2, r0 0
	flwl	%f1 l.6181
	lwi	r5, r3 20
	sli	r6, r5, 2
	lwi	r7, r3 16
	add	r30 r7 r6
	lwi	r6, r30 0
	flwl	%f2 l.6177
	lwi	r8, r3 56
	lwi	r24, r3 12
	addi	r30 r31 0
	addi	r5, r8 0 #
	swi	r30, r3 92
	addi	r3, r3, 96
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	lwi	r2, r3 20
	sli	r5, r2, 2
	lwi	r6, r3 16
	add	r30 r6 r5
	lwi	r5, r30 0
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	p_rgb.2651
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	lwi	r5, r3 32
	addi	r30 r31 0
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	veccpy.2573
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	lwi	r2, r3 20
	sli	r5, r2, 2
	lwi	r6, r3 16
	add	r30 r6 r5
	lwi	r5, r30 0
	lwi	r7, r3 8
	addi	r30 r31 0
	addi	r2, r5 0 #
	addi	r5, r7 0 #
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	p_set_group_id.2665
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	lwi	r2, r3 20
	sli	r5, r2, 2
	lwi	r6, r3 16
	add	r30 r6 r5
	lwi	r5, r30 0
	addi	r7, r0 0
	lwi	r24, r3 4
	addi	r30 r31 0
	addi	r2, r5 0 #
	addi	r5, r7 0 #
	swi	r30, r3 92
	addi	r3, r3, 96
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	lwi	r2, r3 20
	addi	r2, r2, -1
	addi	r5, r0 1
	lwi	r6, r3 8
	swi	r2, r3 88
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	add_mod5.2560
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r6, r2 0 #
	addi	r31 r30 0
	flwi	f1, r3 64
	flwi	f2, r3 48
	flwi	f3, r3 40
	lwi	r2, r3 16
	lwi	r5, r3 88
	lwi	r24, r3 0
	lwi	r23, r24 0
	jr	r23
bge_else.9234:
	jr	r31
pretrace_line.2960:
	lwi	r7, r24 24
	lwi	r8, r24 20
	lwi	r9, r24 16
	lwi	r10, r24 12
	lwi	r11, r24 8
	lwi	r12, r24 4
	flwi	f1, r9 0
	lwi	r9, r12 4
	sub	r5, r5, r9
	swi	r6, r3 0
	swi	r2, r3 4
	swi	r10, r3 8
	swi	r11, r3 12
	swi	r7, r3 16
	swi	r8, r3 20
	fswi	f1, r3 24
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	min_caml_float_of_int
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	flwi	f2, r3 24
	fmul	f1, f2, f1
	lwi	r2, r3 20
	flwi	f2, r2 0
	fmul	f2, f1, f2
	lwi	r5, r3 16
	flwi	f3, r5 0
	fadd	f2, f2, f3
	flwi	f3, r2 8
	fmul	f3, f1, f3
	flwi	f4, r5 8
	fadd	f3, f3, f4
	flwi	f4, r2 16
	fmul	f1, f1, f4
	flwi	f4, r5 16
	fadd	f1, f1, f4
	lwi	r2, r3 12
	lwi	r2, r2 0
	addi	r5, r2, -1
	lwi	r2, r3 4
	lwi	r6, r3 0
	lwi	r24, r3 8
	fadd	f31, f3 f0
	fadd	f3, f1 f0
	fadd	f1, f2 f0
	fadd	f2, f31 f0
	lwi	r23, r24 0
	jr	r23
scan_pixel.2964:
	lwi	r9, r24 24
	lwi	r10, r24 20
	lwi	r11, r24 16
	lwi	r12, r24 12
	lwi	r13, r24 8
	lwi	r14, r24 4
	lwi	r13, r13 0
	sgt	r29, r13, r2
	seq	r29, r29, r0
	jeql	r29, r0, ble_else.9239
	jr	r31
ble_else.9239:
	sli	r13, r2, 2
	add	r30 r7 r13
	lwi	r13, r30 0
	swi	r24, r3 0
	swi	r9, r3 4
	swi	r6, r3 8
	swi	r10, r3 12
	swi	r14, r3 16
	swi	r7, r3 20
	swi	r8, r3 24
	swi	r5, r3 28
	swi	r2, r3 32
	swi	r12, r3 36
	swi	r11, r3 40
	addi	r30 r31 0
	addi	r2, r13 0 #
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	p_rgb.2651
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r5, r2 0 #
	addi	r31 r30 0
	lwi	r2, r3 40
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	veccpy.2573
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	lwi	r2, r3 32
	lwi	r5, r3 28
	lwi	r6, r3 24
	lwi	r24, r3 36
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9241
	lwi	r2, r3 32
	sli	r5, r2, 2
	lwi	r6, r3 20
	add	r30 r6 r5
	lwi	r5, r30 0
	addi	r7, r0 0
	lwi	r24, r3 16
	addi	r30 r31 0
	addi	r2, r5 0 #
	addi	r5, r7 0 #
	swi	r30, r3 44
	addi	r3, r3, 48
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	jl	beq_cont.9242
beq_else.9241:
	addi	r9, r0 0
	lwi	r2, r3 32
	lwi	r5, r3 28
	lwi	r6, r3 8
	lwi	r7, r3 20
	lwi	r8, r3 24
	lwi	r24, r3 12
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
beq_cont.9242:
	lwi	r24, r3 4
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	lwi	r2, r3 32
	addi	r2, r2, 1
	lwi	r5, r3 28
	lwi	r6, r3 8
	lwi	r7, r3 20
	lwi	r8, r3 24
	lwi	r24, r3 0
	lwi	r23, r24 0
	jr	r23
scan_line.2970:
	lwi	r9, r24 12
	lwi	r10, r24 8
	lwi	r11, r24 4
	lwi	r12, r11 4
	sgt	r29, r12, r2
	seq	r29, r29, r0
	jeql	r29, r0, ble_else.9243
	jr	r31
ble_else.9243:
	lwi	r11, r11 4
	addi	r11, r11, -1
	swi	r24, r3 0
	swi	r8, r3 4
	swi	r7, r3 8
	swi	r6, r3 12
	swi	r5, r3 16
	swi	r2, r3 20
	swi	r9, r3 24
	sgt	r29, r11, r2
	seq	r29, r29, r0
	jeql	r29, r0, ble_else.9245
	jl	ble_cont.9246
ble_else.9245:
	addi	r11, r2, 1
	addi	r30 r31 0
	addi	r6, r8 0 #
	addi	r5, r11 0 #
	addi	r2, r7 0 #
	addi	r24, r10 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
ble_cont.9246:
	addi	r2, r0 0
	lwi	r5, r3 20
	lwi	r6, r3 16
	lwi	r7, r3 12
	lwi	r8, r3 8
	lwi	r24, r3 24
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r2, r3 20
	addi	r2, r2, 1
	addi	r5, r0 2
	lwi	r6, r3 4
	swi	r2, r3 28
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	add_mod5.2560
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r8, r2 0 #
	addi	r31 r30 0
	lwi	r2, r3 28
	lwi	r5, r3 12
	lwi	r6, r3 8
	lwi	r7, r3 16
	lwi	r24, r3 0
	lwi	r23, r24 0
	jr	r23
create_float5x3array.2976:
	addi	r2, r0 3
	flwl	%f1 l.6177
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_create_float_array
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r5, r2 0 #
	addi	r31 r30 0
	addi	r2, r0 5
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_create_array
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	addi	r5, r0 3
	flwl	%f1 l.6177
	swi	r2, r3 0
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_create_float_array
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	lwi	r5, r3 0
	swi	r2, r5 4
	addi	r2, r0 3
	flwl	%f1 l.6177
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_create_float_array
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	lwi	r5, r3 0
	swi	r2, r5 8
	addi	r2, r0 3
	flwl	%f1 l.6177
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_create_float_array
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	lwi	r5, r3 0
	swi	r2, r5 12
	addi	r2, r0 3
	flwl	%f1 l.6177
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_create_float_array
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	lwi	r5, r3 0
	swi	r2, r5 16
	addi	r2, r5 0
	jr	r31
create_pixel.2978:
	addi	r2, r0 3
	flwl	%f1 l.6177
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_create_float_array
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	swi	r2, r3 0
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	create_float5x3array.2976
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	addi	r5, r0 5
	addi	r6, r0 0
	swi	r2, r3 4
	addi	r30 r31 0
	addi	r2, r5 0 #
	addi	r5, r6 0 #
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	min_caml_create_array
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	addi	r5, r0 5
	addi	r6, r0 0
	swi	r2, r3 8
	addi	r30 r31 0
	addi	r2, r5 0 #
	addi	r5, r6 0 #
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	min_caml_create_array
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	swi	r2, r3 12
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	create_float5x3array.2976
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	swi	r2, r3 16
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	create_float5x3array.2976
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	addi	r5, r0 1
	addi	r6, r0 0
	swi	r2, r3 20
	addi	r30 r31 0
	addi	r2, r5 0 #
	addi	r5, r6 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	min_caml_create_array
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	swi	r2, r3 24
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	create_float5x3array.2976
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	addi	r5, r4 0
	addi	r4, r4, 32
	swi	r2, r5 28
	lwi	r2, r3 24
	swi	r2, r5 24
	lwi	r2, r3 20
	swi	r2, r5 20
	lwi	r2, r3 16
	swi	r2, r5 16
	lwi	r2, r3 12
	swi	r2, r5 12
	lwi	r2, r3 8
	swi	r2, r5 8
	lwi	r2, r3 4
	swi	r2, r5 4
	lwi	r2, r3 0
	swi	r2, r5 0
	addi	r2, r5 0
	jr	r31
init_line_elements.2980:
	addi	r28, r0, 0
	slt	r29, r5, r28
	seq	r29, r29, r0
	jeql	r29, r0, bge_else.9247
	swi	r2, r3 0
	swi	r5, r3 4
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	create_pixel.2978
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	lwi	r5, r3 4
	sli	r6, r5, 2
	lwi	r7, r3 0
	add	r30 r7 r6
	swi	r2, r30 0
	addi	r5, r5, -1
	addi	r2, r7 0 #
	jl	init_line_elements.2980
bge_else.9247:
	jr	r31
create_pixelline.2983:
	lwi	r2, r24 4
	lwi	r5, r2 0
	swi	r2, r3 0
	swi	r5, r3 4
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	create_pixel.2978
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r5, r2 0 #
	addi	r31 r30 0
	lwi	r2, r3 4
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	min_caml_create_array
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	lwi	r5, r3 0
	lwi	r5, r5 0
	addi	r5, r5, -2
	jl	init_line_elements.2980
tan.2985:
	fswi	f1, r3 0
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	sin.2516
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	flwi	f2, r3 0
	fswi	f1, r3 8
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	cos.2518
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	flwi	f2, r3 8
	fdiv	f1, f2, f1
	jr	r31
adjust_position.2987:
	fmul	f1, f1, f1
	flwl	%f3 l.6630
	fadd	f1, f1, f3
	fswi	f2, r3 0
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	min_caml_sqrt
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	flwl	%f2 l.6181
	fdiv	f2, f2, f1
	fswi	f1, r3 8
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	atan.2520
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	flwi	f2, r3 0
	fmul	f1, f1, f2
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	tan.2985
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	flwi	f2, r3 8
	fmul	f1, f1, f2
	jr	r31
calc_dirvec.2990:
	lwi	r7, r24 4
	addi	r28, r0, 5
	slt	r29, r2, r28
	seq	r29, r29, r0
	jeql	r29, r0, bge_else.9248
	swi	r6, r3 0
	swi	r7, r3 4
	swi	r5, r3 8
	fswi	f1, r3 16
	fswi	f2, r3 24
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	fsqr.2514
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	flwi	f2, r3 24
	fswi	f1, r3 32
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	fsqr.2514
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	flwi	f2, r3 32
	fadd	f1, f2, f1
	flwl	%f2 l.6181
	fadd	f1, f1, f2
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	min_caml_sqrt
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	flwi	f2, r3 16
	fdiv	f2, f2, f1
	flwi	f3, r3 24
	fdiv	f3, f3, f1
	flwl	%f4 l.6181
	fdiv	f1, f4, f1
	lwi	r2, r3 8
	sli	r2, r2, 2
	lwi	r5, r3 4
	add	r30 r5 r2
	lwi	r2, r30 0
	lwi	r5, r3 0
	sli	r6, r5, 2
	add	r30 r2 r6
	lwi	r6, r30 0
	swi	r2, r3 40
	fswi	f1, r3 48
	fswi	f3, r3 56
	fswi	f2, r3 64
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	d_vec.2670
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	flwi	f1, r3 64
	flwi	f2, r3 56
	flwi	f3, r3 48
	addi	r30 r31 0
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	vecset.2563
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	lwi	r2, r3 0
	addi	r5, r2, 40
	sli	r5, r5, 2
	lwi	r6, r3 40
	add	r30 r6 r5
	lwi	r5, r30 0
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	d_vec.2670
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	flwi	f1, r3 56
	swi	r2, r3 72
	addi	r30 r31 0
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	fneg.2512
	addi	r3, r3, -80
	lwi	r30, r3 76
	fadd	f3, f1 f0
	addi	r31 r30 0
	flwi	f1, r3 64
	flwi	f2, r3 48
	lwi	r2, r3 72
	addi	r30 r31 0
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	vecset.2563
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	lwi	r2, r3 0
	addi	r5, r2, 80
	sli	r5, r5, 2
	lwi	r6, r3 40
	add	r30 r6 r5
	lwi	r5, r30 0
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	d_vec.2670
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	flwi	f1, r3 64
	swi	r2, r3 76
	addi	r30 r31 0
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	fneg.2512
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	flwi	f2, r3 56
	fswi	f1, r3 80
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	fneg.2512
	addi	r3, r3, -96
	lwi	r30, r3 92
	fadd	f3, f1 f0
	addi	r31 r30 0
	flwi	f1, r3 48
	flwi	f2, r3 80
	lwi	r2, r3 76
	addi	r30 r31 0
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	vecset.2563
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	lwi	r2, r3 0
	addi	r5, r2, 1
	sli	r5, r5, 2
	lwi	r6, r3 40
	add	r30 r6 r5
	lwi	r5, r30 0
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	d_vec.2670
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	flwi	f1, r3 64
	swi	r2, r3 88
	addi	r30 r31 0
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	fneg.2512
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	flwi	f2, r3 56
	fswi	f1, r3 96
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 108
	addi	r3, r3, 112
	jal	fneg.2512
	addi	r3, r3, -112
	lwi	r30, r3 108
	addi	r31 r30 0
	flwi	f2, r3 48
	fswi	f1, r3 104
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 116
	addi	r3, r3, 120
	jal	fneg.2512
	addi	r3, r3, -120
	lwi	r30, r3 116
	fadd	f3, f1 f0
	addi	r31 r30 0
	flwi	f1, r3 96
	flwi	f2, r3 104
	lwi	r2, r3 88
	addi	r30 r31 0
	swi	r30, r3 116
	addi	r3, r3, 120
	jal	vecset.2563
	addi	r3, r3, -120
	lwi	r30, r3 116
	addi	r31 r30 0
	lwi	r2, r3 0
	addi	r5, r2, 41
	sli	r5, r5, 2
	lwi	r6, r3 40
	add	r30 r6 r5
	lwi	r5, r30 0
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 116
	addi	r3, r3, 120
	jal	d_vec.2670
	addi	r3, r3, -120
	lwi	r30, r3 116
	addi	r31 r30 0
	flwi	f1, r3 64
	swi	r2, r3 112
	addi	r30 r31 0
	swi	r30, r3 116
	addi	r3, r3, 120
	jal	fneg.2512
	addi	r3, r3, -120
	lwi	r30, r3 116
	addi	r31 r30 0
	flwi	f2, r3 48
	fswi	f1, r3 120
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 132
	addi	r3, r3, 136
	jal	fneg.2512
	addi	r3, r3, -136
	lwi	r30, r3 132
	fadd	f2, f1 f0
	addi	r31 r30 0
	flwi	f1, r3 120
	flwi	f3, r3 56
	lwi	r2, r3 112
	addi	r30 r31 0
	swi	r30, r3 132
	addi	r3, r3, 136
	jal	vecset.2563
	addi	r3, r3, -136
	lwi	r30, r3 132
	addi	r31 r30 0
	lwi	r2, r3 0
	addi	r2, r2, 81
	sli	r2, r2, 2
	lwi	r5, r3 40
	add	r30 r5 r2
	lwi	r2, r30 0
	addi	r30 r31 0
	swi	r30, r3 132
	addi	r3, r3, 136
	jal	d_vec.2670
	addi	r3, r3, -136
	lwi	r30, r3 132
	addi	r31 r30 0
	flwi	f1, r3 48
	swi	r2, r3 128
	addi	r30 r31 0
	swi	r30, r3 132
	addi	r3, r3, 136
	jal	fneg.2512
	addi	r3, r3, -136
	lwi	r30, r3 132
	addi	r31 r30 0
	flwi	f2, r3 64
	flwi	f3, r3 56
	lwi	r2, r3 128
	jl	vecset.2563
bge_else.9248:
	fswi	f3, r3 136
	swi	r6, r3 0
	swi	r5, r3 8
	swi	r24, r3 144
	fswi	f4, r3 152
	swi	r2, r3 160
	addi	r30 r31 0
	fadd	f1, f2 f0
	fadd	f2, f3 f0
	swi	r30, r3 164
	addi	r3, r3, 168
	jal	adjust_position.2987
	addi	r3, r3, -168
	lwi	r30, r3 164
	addi	r31 r30 0
	lwi	r2, r3 160
	addi	r2, r2, 1
	flwi	f2, r3 152
	fswi	f1, r3 168
	swi	r2, r3 176
	addi	r30 r31 0
	swi	r30, r3 180
	addi	r3, r3, 184
	jal	adjust_position.2987
	addi	r3, r3, -184
	lwi	r30, r3 180
	fadd	f2, f1 f0
	addi	r31 r30 0
	flwi	f1, r3 168
	flwi	f3, r3 136
	flwi	f4, r3 152
	lwi	r2, r3 176
	lwi	r5, r3 8
	lwi	r6, r3 0
	lwi	r24, r3 144
	lwi	r23, r24 0
	jr	r23
calc_dirvecs.2998:
	lwi	r7, r24 4
	addi	r28, r0, 0
	slt	r29, r2, r28
	seq	r29, r29, r0
	jeql	r29, r0, bge_else.9256
	swi	r24, r3 0
	swi	r2, r3 4
	fswi	f1, r3 8
	swi	r6, r3 16
	swi	r5, r3 20
	swi	r7, r3 24
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	min_caml_float_of_int
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	flwl	%f2 l.6742
	fmul	f1, f1, f2
	flwl	%f2 l.6743
	fsub	f3, f1, f2
	addi	r2, r0 0
	flwl	%f1 l.6177
	flwl	%f2 l.6177
	flwi	f4, r3 8
	lwi	r5, r3 20
	lwi	r6, r3 16
	lwi	r24, r3 24
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r2, r3 4
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	min_caml_float_of_int
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	flwl	%f2 l.6742
	fmul	f1, f1, f2
	flwl	%f2 l.6630
	fadd	f3, f1, f2
	addi	r2, r0 0
	flwl	%f1 l.6177
	flwl	%f2 l.6177
	lwi	r5, r3 16
	addi	r6, r5, 2
	flwi	f4, r3 8
	lwi	r7, r3 20
	lwi	r24, r3 24
	addi	r30 r31 0
	addi	r5, r7 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	lwi	r2, r3 4
	addi	r2, r2, -1
	addi	r5, r0 1
	lwi	r6, r3 20
	swi	r2, r3 28
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	add_mod5.2560
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r5, r2 0 #
	addi	r31 r30 0
	flwi	f1, r3 8
	lwi	r2, r3 28
	lwi	r6, r3 16
	lwi	r24, r3 0
	lwi	r23, r24 0
	jr	r23
bge_else.9256:
	jr	r31
calc_dirvec_rows.3003:
	lwi	r7, r24 4
	addi	r28, r0, 0
	slt	r29, r2, r28
	seq	r29, r29, r0
	jeql	r29, r0, bge_else.9258
	swi	r24, r3 0
	swi	r2, r3 4
	swi	r6, r3 8
	swi	r5, r3 12
	swi	r7, r3 16
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	min_caml_float_of_int
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	flwl	%f2 l.6742
	fmul	f1, f1, f2
	flwl	%f2 l.6743
	fsub	f1, f1, f2
	addi	r2, r0 4
	lwi	r5, r3 12
	lwi	r6, r3 8
	lwi	r24, r3 16
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r2, r3 4
	addi	r2, r2, -1
	addi	r5, r0 2
	lwi	r6, r3 12
	swi	r2, r3 20
	addi	r30 r31 0
	addi	r2, r6 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	add_mod5.2560
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r5, r2 0 #
	addi	r31 r30 0
	lwi	r2, r3 8
	addi	r6, r2, 4
	lwi	r2, r3 20
	lwi	r24, r3 0
	lwi	r23, r24 0
	jr	r23
bge_else.9258:
	jr	r31
create_dirvec.3007:
	lwi	r2, r24 4
	addi	r5, r0 3
	flwl	%f1 l.6177
	swi	r2, r3 0
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_create_float_array
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r5, r2 0 #
	addi	r31 r30 0
	lwi	r2, r3 0
	lwi	r2, r2 0
	swi	r5, r3 4
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	min_caml_create_array
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	addi	r5, r4 0
	addi	r4, r4, 8
	swi	r2, r5 4
	lwi	r2, r3 4
	swi	r2, r5 0
	addi	r2, r5 0
	jr	r31
create_dirvec_elements.3009:
	lwi	r6, r24 4
	addi	r28, r0, 0
	slt	r29, r5, r28
	seq	r29, r29, r0
	jeql	r29, r0, bge_else.9260
	swi	r24, r3 0
	swi	r2, r3 4
	swi	r5, r3 8
	addi	r30 r31 0
	addi	r24, r6 0 #
	swi	r30, r3 12
	addi	r3, r3, 16
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	lwi	r5, r3 8
	sli	r6, r5, 2
	lwi	r7, r3 4
	add	r30 r7 r6
	swi	r2, r30 0
	addi	r5, r5, -1
	lwi	r24, r3 0
	addi	r2, r7 0 #
	lwi	r23, r24 0
	jr	r23
bge_else.9260:
	jr	r31
create_dirvecs.3012:
	lwi	r5, r24 12
	lwi	r6, r24 8
	lwi	r7, r24 4
	addi	r28, r0, 0
	slt	r29, r2, r28
	seq	r29, r29, r0
	jeql	r29, r0, bge_else.9262
	addi	r8, r0 120
	swi	r24, r3 0
	swi	r6, r3 4
	swi	r5, r3 8
	swi	r2, r3 12
	swi	r8, r3 16
	addi	r30 r31 0
	addi	r24, r7 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r5, r2 0 #
	addi	r31 r30 0
	lwi	r2, r3 16
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	min_caml_create_array
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r5, r3 12
	sli	r6, r5, 2
	lwi	r7, r3 8
	add	r30 r7 r6
	swi	r2, r30 0
	sli	r2, r5, 2
	add	r30 r7 r2
	lwi	r2, r30 0
	addi	r6, r0 118
	lwi	r24, r3 4
	addi	r30 r31 0
	addi	r5, r6 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	lwi	r2, r3 12
	addi	r2, r2, -1
	lwi	r24, r3 0
	lwi	r23, r24 0
	jr	r23
bge_else.9262:
	jr	r31
init_dirvec_constants.3014:
	lwi	r6, r24 4
	addi	r28, r0, 0
	slt	r29, r5, r28
	seq	r29, r29, r0
	jeql	r29, r0, bge_else.9264
	sli	r7, r5, 2
	add	r30 r2 r7
	lwi	r7, r30 0
	swi	r2, r3 0
	swi	r24, r3 4
	swi	r5, r3 8
	addi	r30 r31 0
	addi	r2, r7 0 #
	addi	r24, r6 0 #
	swi	r30, r3 12
	addi	r3, r3, 16
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	lwi	r2, r3 8
	addi	r5, r2, -1
	lwi	r2, r3 0
	lwi	r24, r3 4
	lwi	r23, r24 0
	jr	r23
bge_else.9264:
	jr	r31
init_vecset_constants.3017:
	lwi	r5, r24 8
	lwi	r6, r24 4
	addi	r28, r0, 0
	slt	r29, r2, r28
	seq	r29, r29, r0
	jeql	r29, r0, bge_else.9266
	sli	r7, r2, 2
	add	r30 r6 r7
	lwi	r6, r30 0
	addi	r7, r0 119
	swi	r24, r3 0
	swi	r2, r3 4
	addi	r30 r31 0
	addi	r2, r6 0 #
	addi	r24, r5 0 #
	addi	r5, r7 0 #
	swi	r30, r3 12
	addi	r3, r3, 16
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	lwi	r2, r3 4
	addi	r2, r2, -1
	lwi	r24, r3 0
	lwi	r23, r24 0
	jr	r23
bge_else.9266:
	jr	r31
init_dirvecs.3019:
	lwi	r2, r24 12
	lwi	r5, r24 8
	lwi	r6, r24 4
	addi	r7, r0 4
	swi	r2, r3 0
	swi	r6, r3 4
	addi	r30 r31 0
	addi	r2, r7 0 #
	addi	r24, r5 0 #
	swi	r30, r3 12
	addi	r3, r3, 16
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	addi	r2, r0 9
	addi	r5, r0 0
	addi	r6, r0 0
	lwi	r24, r3 4
	addi	r30 r31 0
	swi	r30, r3 12
	addi	r3, r3, 16
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	addi	r2, r0 4
	lwi	r24, r3 0
	lwi	r23, r24 0
	jr	r23
add_reflection.3021:
	lwi	r6, r24 12
	lwi	r7, r24 8
	lwi	r24, r24 4
	swi	r7, r3 0
	swi	r2, r3 4
	swi	r5, r3 8
	fswi	f1, r3 16
	swi	r6, r3 24
	fswi	f4, r3 32
	fswi	f3, r3 40
	fswi	f2, r3 48
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	swi	r2, r3 56
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	d_vec.2670
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	flwi	f1, r3 48
	flwi	f2, r3 40
	flwi	f3, r3 32
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	vecset.2563
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	lwi	r2, r3 56
	lwi	r24, r3 24
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	addi	r2, r4 0
	addi	r4, r4, 16
	flwi	f1, r3 16
	fswi	f1, r2 8
	lwi	r5, r3 56
	swi	r5, r2 4
	lwi	r5, r3 8
	swi	r5, r2 0
	lwi	r5, r3 4
	sli	r5, r5, 2
	lwi	r6, r3 0
	add	r30 r6 r5
	swi	r2, r30 0
	jr	r31
setup_rect_reflection.3028:
	lwi	r6, r24 12
	lwi	r7, r24 8
	lwi	r8, r24 4
	addi	r9, r0 4
	mul	r2, r2, r9
	lwi	r9, r6 0
	flwl	%f1 l.6181
	swi	r6, r3 0
	swi	r9, r3 4
	swi	r8, r3 8
	swi	r2, r3 12
	swi	r7, r3 16
	fswi	f1, r3 24
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	o_diffuse.2633
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	flwi	f2, r3 24
	fsub	f1, f2, f1
	lwi	r2, r3 16
	flwi	f2, r2 0
	fswi	f1, r3 32
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	fneg.2512
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	lwi	r2, r3 16
	flwi	f2, r2 8
	fswi	f1, r3 40
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	fneg.2512
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	lwi	r2, r3 16
	flwi	f2, r2 16
	fswi	f1, r3 48
	addi	r30 r31 0
	fadd	f1, f2 f0
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	fneg.2512
	addi	r3, r3, -64
	lwi	r30, r3 60
	fadd	f4, f1 f0
	addi	r31 r30 0
	lwi	r2, r3 12
	addi	r5, r2, 1
	lwi	r6, r3 16
	flwi	f2, r6 0
	flwi	f1, r3 32
	flwi	f3, r3 48
	lwi	r7, r3 4
	lwi	r24, r3 8
	fswi	f4, r3 56
	addi	r30 r31 0
	addi	r2, r7 0 #
	swi	r30, r3 68
	addi	r3, r3, 72
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	lwi	r2, r3 4
	addi	r5, r2, 1
	lwi	r6, r3 12
	addi	r7, r6, 2
	lwi	r8, r3 16
	flwi	f3, r8 8
	flwi	f1, r3 32
	flwi	f2, r3 40
	flwi	f4, r3 56
	lwi	r24, r3 8
	addi	r30 r31 0
	addi	r2, r5 0 #
	addi	r5, r7 0 #
	swi	r30, r3 68
	addi	r3, r3, 72
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	lwi	r2, r3 4
	addi	r5, r2, 2
	lwi	r6, r3 12
	addi	r6, r6, 3
	lwi	r7, r3 16
	flwi	f4, r7 16
	flwi	f1, r3 32
	flwi	f2, r3 40
	flwi	f3, r3 48
	lwi	r24, r3 8
	addi	r30 r31 0
	addi	r2, r5 0 #
	addi	r5, r6 0 #
	swi	r30, r3 68
	addi	r3, r3, 72
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	lwi	r2, r3 4
	addi	r2, r2, 3
	lwi	r5, r3 0
	swi	r2, r5 0
	jr	r31
setup_surface_reflection.3031:
	lwi	r6, r24 12
	lwi	r7, r24 8
	lwi	r8, r24 4
	addi	r9, r0 4
	mul	r2, r2, r9
	addi	r2, r2, 1
	lwi	r9, r6 0
	flwl	%f1 l.6181
	swi	r6, r3 0
	swi	r2, r3 4
	swi	r9, r3 8
	swi	r8, r3 12
	swi	r7, r3 16
	swi	r5, r3 20
	fswi	f1, r3 24
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	o_diffuse.2633
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	flwi	f2, r3 24
	fsub	f1, f2, f1
	lwi	r2, r3 20
	fswi	f1, r3 32
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	o_param_abc.2625
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r5, r2 0 #
	addi	r31 r30 0
	lwi	r2, r3 16
	addi	r30 r31 0
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	veciprod.2584
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	flwl	%f2 l.6180
	lwi	r2, r3 20
	fswi	f1, r3 40
	fswi	f2, r3 48
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	o_param_a.2619
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	flwi	f2, r3 48
	fmul	f1, f2, f1
	flwi	f2, r3 40
	fmul	f1, f1, f2
	lwi	r2, r3 16
	flwi	f3, r2 0
	fsub	f1, f1, f3
	flwl	%f3 l.6180
	lwi	r5, r3 20
	fswi	f1, r3 56
	fswi	f3, r3 64
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	o_param_b.2621
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	flwi	f2, r3 64
	fmul	f1, f2, f1
	flwi	f2, r3 40
	fmul	f1, f1, f2
	lwi	r2, r3 16
	flwi	f3, r2 8
	fsub	f1, f1, f3
	flwl	%f3 l.6180
	lwi	r5, r3 20
	fswi	f1, r3 72
	fswi	f3, r3 80
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	o_param_c.2623
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	flwi	f2, r3 80
	fmul	f1, f2, f1
	flwi	f2, r3 40
	fmul	f1, f1, f2
	lwi	r2, r3 16
	flwi	f2, r2 16
	fsub	f4, f1, f2
	flwi	f1, r3 32
	flwi	f2, r3 56
	flwi	f3, r3 72
	lwi	r2, r3 8
	lwi	r5, r3 4
	lwi	r24, r3 12
	addi	r30 r31 0
	swi	r30, r3 92
	addi	r3, r3, 96
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	lwi	r2, r3 8
	addi	r2, r2, 1
	lwi	r5, r3 0
	swi	r2, r5 0
	jr	r31
setup_reflections.3034:
	lwi	r5, r24 12
	lwi	r6, r24 8
	lwi	r7, r24 4
	addi	r28, r0, 0
	slt	r29, r2, r28
	seq	r29, r29, r0
	jeql	r29, r0, bge_else.9274
	sli	r8, r2, 2
	add	r30 r7 r8
	lwi	r7, r30 0
	swi	r5, r3 0
	swi	r2, r3 4
	swi	r6, r3 8
	swi	r7, r3 12
	addi	r30 r31 0
	addi	r2, r7 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	o_reflectiontype.2613
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	addi	r28, r0, 2
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9275
	lwi	r2, r3 12
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	o_diffuse.2633
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	flwl	%f2 l.6181
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	fless.2509
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	addi	r28, r0, 0
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9276
	jr	r31
beq_else.9276:
	lwi	r2, r3 12
	addi	r30 r31 0
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	o_form.2611
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	addi	r28, r0, 1
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9278
	lwi	r2, r3 4
	lwi	r5, r3 12
	lwi	r24, r3 8
	lwi	r23, r24 0
	jr	r23
beq_else.9278:
	addi	r28, r0, 2
	seq	r29, r2, r28
	jeql	r29, r0, beq_else.9279
	lwi	r2, r3 4
	lwi	r5, r3 12
	lwi	r24, r3 0
	lwi	r23, r24 0
	jr	r23
beq_else.9279:
	jr	r31
beq_else.9275:
	jr	r31
bge_else.9274:
	jr	r31
rt.3036:
	lwi	r6, r24 56
	lwi	r7, r24 52
	lwi	r8, r24 48
	lwi	r9, r24 44
	lwi	r10, r24 40
	lwi	r11, r24 36
	lwi	r12, r24 32
	lwi	r13, r24 28
	lwi	r14, r24 24
	lwi	r15, r24 20
	lwi	r16, r24 16
	lwi	r17, r24 12
	lwi	r18, r24 8
	lwi	r19, r24 4
	swi	r2, r17 0
	swi	r5, r17 4
	addi	r17, r0 2
	div	r17, r2, r17
	swi	r17, r18 0
	addi	r17, r0 2
	div	r5, r5, r17
	swi	r5, r18 4
	flwl	%f1 l.6776
	swi	r10, r3 0
	swi	r12, r3 4
	swi	r7, r3 8
	swi	r13, r3 12
	swi	r8, r3 16
	swi	r15, r3 20
	swi	r14, r3 24
	swi	r16, r3 28
	swi	r6, r3 32
	swi	r11, r3 36
	swi	r19, r3 40
	swi	r9, r3 44
	fswi	f1, r3 48
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	min_caml_float_of_int
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	flwi	f2, r3 48
	fdiv	f1, f2, f1
	lwi	r2, r3 44
	fswi	f1, r2 0
	lwi	r24, r3 40
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	lwi	r24, r3 40
	swi	r2, r3 56
	addi	r30 r31 0
	swi	r30, r3 60
	addi	r3, r3, 64
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	lwi	r24, r3 40
	swi	r2, r3 60
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	lwi	r24, r3 36
	swi	r2, r3 64
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	lwi	r24, r3 32
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	lwi	r24, r3 28
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	lwi	r2, r3 24
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	d_vec.2670
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	lwi	r5, r3 20
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	veccpy.2573
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	lwi	r2, r3 24
	lwi	r24, r3 16
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	lwi	r2, r3 12
	lwi	r2, r2 0
	addi	r2, r2, -1
	lwi	r24, r3 8
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	addi	r5, r0 0
	addi	r6, r0 0
	lwi	r2, r3 60
	lwi	r24, r3 4
	addi	r30 r31 0
	swi	r30, r3 68
	addi	r3, r3, 72
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	addi	r2, r0 0
	addi	r8, r0 2
	lwi	r5, r3 56
	lwi	r6, r3 60
	lwi	r7, r3 64
	lwi	r24, r3 0
	lwi	r23, r24 0
	jr	r23
_min_caml_start: # main entry point
   # main program start
	addi	r2, r0 1
	addi	r5, r0 0
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_create_array
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	addi	r5, r0 0
	flwl	%f1 l.6177
	swi	r2, r3 0
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_create_float_array
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	addi	r5, r0 60
	addi	r6, r0 0
	addi	r7, r0 0
	addi	r8, r0 0
	addi	r9, r0 0
	addi	r10, r0 0
	addi	r11, r4 0
	addi	r4, r4, 48
	swi	r2, r11 40
	swi	r2, r11 36
	swi	r2, r11 32
	swi	r2, r11 28
	swi	r10, r11 24
	swi	r2, r11 20
	swi	r2, r11 16
	swi	r9, r11 12
	swi	r8, r11 8
	swi	r7, r11 4
	swi	r6, r11 0
	addi	r2, r11 0
	addi	r30 r31 0
	addi	r23, r5 0 #
	addi	r5, r2 0 #
	addi	r2, r23 0 #
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_create_array
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	addi	r5, r0 3
	flwl	%f1 l.6177
	swi	r2, r3 4
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	min_caml_create_float_array
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	addi	r5, r0 3
	flwl	%f1 l.6177
	swi	r2, r3 8
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 12
	addi	r3, r3, 16
	jal	min_caml_create_float_array
	addi	r3, r3, -16
	lwi	r30, r3 12
	addi	r31 r30 0
	addi	r5, r0 3
	flwl	%f1 l.6177
	swi	r2, r3 12
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	min_caml_create_float_array
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	addi	r5, r0 1
	flwl	%f1 l.6589
	swi	r2, r3 16
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 20
	addi	r3, r3, 24
	jal	min_caml_create_float_array
	addi	r3, r3, -24
	lwi	r30, r3 20
	addi	r31 r30 0
	addi	r5, r0 50
	addi	r6, r0 1
	addi	r7, r0 -1
	swi	r2, r3 20
	swi	r5, r3 24
	addi	r30 r31 0
	addi	r5, r7 0 #
	addi	r2, r6 0 #
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	min_caml_create_array
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r5, r2 0 #
	addi	r31 r30 0
	lwi	r2, r3 24
	addi	r30 r31 0
	swi	r30, r3 28
	addi	r3, r3, 32
	jal	min_caml_create_array
	addi	r3, r3, -32
	lwi	r30, r3 28
	addi	r31 r30 0
	addi	r5, r0 1
	addi	r6, r0 1
	lwi	r7, r2 0
	swi	r2, r3 28
	swi	r5, r3 32
	addi	r30 r31 0
	addi	r5, r7 0 #
	addi	r2, r6 0 #
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	min_caml_create_array
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r5, r2 0 #
	addi	r31 r30 0
	lwi	r2, r3 32
	addi	r30 r31 0
	swi	r30, r3 36
	addi	r3, r3, 40
	jal	min_caml_create_array
	addi	r3, r3, -40
	lwi	r30, r3 36
	addi	r31 r30 0
	addi	r5, r0 1
	flwl	%f1 l.6177
	swi	r2, r3 36
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	min_caml_create_float_array
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	addi	r5, r0 1
	addi	r6, r0 0
	swi	r2, r3 40
	addi	r30 r31 0
	addi	r2, r5 0 #
	addi	r5, r6 0 #
	swi	r30, r3 44
	addi	r3, r3, 48
	jal	min_caml_create_array
	addi	r3, r3, -48
	lwi	r30, r3 44
	addi	r31 r30 0
	addi	r5, r0 1
	flwl	%f1 l.6536
	swi	r2, r3 44
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	min_caml_create_float_array
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	addi	r5, r0 3
	flwl	%f1 l.6177
	swi	r2, r3 48
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 52
	addi	r3, r3, 56
	jal	min_caml_create_float_array
	addi	r3, r3, -56
	lwi	r30, r3 52
	addi	r31 r30 0
	addi	r5, r0 1
	addi	r6, r0 0
	swi	r2, r3 52
	addi	r30 r31 0
	addi	r2, r5 0 #
	addi	r5, r6 0 #
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	min_caml_create_array
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	addi	r5, r0 3
	flwl	%f1 l.6177
	swi	r2, r3 56
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 60
	addi	r3, r3, 64
	jal	min_caml_create_float_array
	addi	r3, r3, -64
	lwi	r30, r3 60
	addi	r31 r30 0
	addi	r5, r0 3
	flwl	%f1 l.6177
	swi	r2, r3 60
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	min_caml_create_float_array
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	addi	r5, r0 3
	flwl	%f1 l.6177
	swi	r2, r3 64
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 68
	addi	r3, r3, 72
	jal	min_caml_create_float_array
	addi	r3, r3, -72
	lwi	r30, r3 68
	addi	r31 r30 0
	addi	r5, r0 3
	flwl	%f1 l.6177
	swi	r2, r3 68
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	min_caml_create_float_array
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	addi	r5, r0 2
	addi	r6, r0 0
	swi	r2, r3 72
	addi	r30 r31 0
	addi	r2, r5 0 #
	addi	r5, r6 0 #
	swi	r30, r3 76
	addi	r3, r3, 80
	jal	min_caml_create_array
	addi	r3, r3, -80
	lwi	r30, r3 76
	addi	r31 r30 0
	addi	r5, r0 2
	addi	r6, r0 0
	swi	r2, r3 76
	addi	r30 r31 0
	addi	r2, r5 0 #
	addi	r5, r6 0 #
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	min_caml_create_array
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	addi	r5, r0 1
	flwl	%f1 l.6177
	swi	r2, r3 80
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 84
	addi	r3, r3, 88
	jal	min_caml_create_float_array
	addi	r3, r3, -88
	lwi	r30, r3 84
	addi	r31 r30 0
	addi	r5, r0 3
	flwl	%f1 l.6177
	swi	r2, r3 84
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	min_caml_create_float_array
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	addi	r5, r0 3
	flwl	%f1 l.6177
	swi	r2, r3 88
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 92
	addi	r3, r3, 96
	jal	min_caml_create_float_array
	addi	r3, r3, -96
	lwi	r30, r3 92
	addi	r31 r30 0
	addi	r5, r0 3
	flwl	%f1 l.6177
	swi	r2, r3 92
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 100
	addi	r3, r3, 104
	jal	min_caml_create_float_array
	addi	r3, r3, -104
	lwi	r30, r3 100
	addi	r31 r30 0
	addi	r5, r0 3
	flwl	%f1 l.6177
	swi	r2, r3 96
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 100
	addi	r3, r3, 104
	jal	min_caml_create_float_array
	addi	r3, r3, -104
	lwi	r30, r3 100
	addi	r31 r30 0
	addi	r5, r0 3
	flwl	%f1 l.6177
	swi	r2, r3 100
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 108
	addi	r3, r3, 112
	jal	min_caml_create_float_array
	addi	r3, r3, -112
	lwi	r30, r3 108
	addi	r31 r30 0
	addi	r5, r0 3
	flwl	%f1 l.6177
	swi	r2, r3 104
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 108
	addi	r3, r3, 112
	jal	min_caml_create_float_array
	addi	r3, r3, -112
	lwi	r30, r3 108
	addi	r31 r30 0
	addi	r5, r0 0
	flwl	%f1 l.6177
	swi	r2, r3 108
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 116
	addi	r3, r3, 120
	jal	min_caml_create_float_array
	addi	r3, r3, -120
	lwi	r30, r3 116
	addi	r5, r2 0 #
	addi	r31 r30 0
	addi	r2, r0 0
	swi	r5, r3 112
	addi	r30 r31 0
	swi	r30, r3 116
	addi	r3, r3, 120
	jal	min_caml_create_array
	addi	r3, r3, -120
	lwi	r30, r3 116
	addi	r31 r30 0
	addi	r5, r0 0
	addi	r6, r4 0
	addi	r4, r4, 8
	swi	r2, r6 4
	lwi	r2, r3 112
	swi	r2, r6 0
	addi	r2, r6 0
	addi	r30 r31 0
	addi	r23, r5 0 #
	addi	r5, r2 0 #
	addi	r2, r23 0 #
	swi	r30, r3 116
	addi	r3, r3, 120
	jal	min_caml_create_array
	addi	r3, r3, -120
	lwi	r30, r3 116
	addi	r5, r2 0 #
	addi	r31 r30 0
	addi	r2, r0 5
	addi	r30 r31 0
	swi	r30, r3 116
	addi	r3, r3, 120
	jal	min_caml_create_array
	addi	r3, r3, -120
	lwi	r30, r3 116
	addi	r31 r30 0
	addi	r5, r0 0
	flwl	%f1 l.6177
	swi	r2, r3 116
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 124
	addi	r3, r3, 128
	jal	min_caml_create_float_array
	addi	r3, r3, -128
	lwi	r30, r3 124
	addi	r31 r30 0
	addi	r5, r0 3
	flwl	%f1 l.6177
	swi	r2, r3 120
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 124
	addi	r3, r3, 128
	jal	min_caml_create_float_array
	addi	r3, r3, -128
	lwi	r30, r3 124
	addi	r31 r30 0
	addi	r5, r0 60
	lwi	r6, r3 120
	swi	r2, r3 124
	addi	r30 r31 0
	addi	r2, r5 0 #
	addi	r5, r6 0 #
	swi	r30, r3 132
	addi	r3, r3, 136
	jal	min_caml_create_array
	addi	r3, r3, -136
	lwi	r30, r3 132
	addi	r31 r30 0
	addi	r5, r4 0
	addi	r4, r4, 8
	swi	r2, r5 4
	lwi	r2, r3 124
	swi	r2, r5 0
	addi	r2, r5 0
	addi	r5, r0 0
	flwl	%f1 l.6177
	swi	r2, r3 128
	addi	r30 r31 0
	addi	r2, r5 0 #
	swi	r30, r3 132
	addi	r3, r3, 136
	jal	min_caml_create_float_array
	addi	r3, r3, -136
	lwi	r30, r3 132
	addi	r5, r2 0 #
	addi	r31 r30 0
	addi	r2, r0 0
	swi	r5, r3 132
	addi	r30 r31 0
	swi	r30, r3 140
	addi	r3, r3, 144
	jal	min_caml_create_array
	addi	r3, r3, -144
	lwi	r30, r3 140
	addi	r31 r30 0
	addi	r5, r4 0
	addi	r4, r4, 8
	swi	r2, r5 4
	lwi	r2, r3 132
	swi	r2, r5 0
	addi	r2, r5 0
	addi	r5, r0 180
	addi	r6, r0 0
	flwl	%f1 l.6177
	addi	r7, r4 0
	addi	r4, r4, 16
	fswi	f1, r7 8
	swi	r2, r7 4
	swi	r6, r7 0
	addi	r2, r7 0
	addi	r30 r31 0
	addi	r23, r5 0 #
	addi	r5, r2 0 #
	addi	r2, r23 0 #
	swi	r30, r3 140
	addi	r3, r3, 144
	jal	min_caml_create_array
	addi	r3, r3, -144
	lwi	r30, r3 140
	addi	r31 r30 0
	addi	r5, r0 1
	addi	r6, r0 0
	swi	r2, r3 136
	addi	r30 r31 0
	addi	r2, r5 0 #
	addi	r5, r6 0 #
	swi	r30, r3 140
	addi	r3, r3, 144
	jal	min_caml_create_array
	addi	r3, r3, -144
	lwi	r30, r3 140
	addi	r31 r30 0
	addi	r5, r4 0
	addi	r4, r4, 24
	closure_address	%r6 read_screen_settings.2682
	swi	r6, r5 0
	lwi	r6, r3 12
	swi	r6, r5 20
	lwi	r7, r3 104
	swi	r7, r5 16
	lwi	r8, r3 100
	swi	r8, r5 12
	lwi	r9, r3 96
	swi	r9, r5 8
	lwi	r10, r3 8
	swi	r10, r5 4
	addi	r10, r4 0
	addi	r4, r4, 16
	closure_address	%r11 read_light.2684
	swi	r11, r10 0
	lwi	r11, r3 16
	swi	r11, r10 8
	lwi	r12, r3 20
	swi	r12, r10 4
	addi	r13, r4 0
	addi	r4, r4, 8
	closure_address	%r14 read_nth_object.2689
	swi	r14, r13 0
	lwi	r14, r3 4
	swi	r14, r13 4
	addi	r15, r4 0
	addi	r4, r4, 16
	closure_address	%r16 read_object.2691
	swi	r16, r15 0
	swi	r13, r15 8
	lwi	r13, r3 0
	swi	r13, r15 4
	addi	r16, r4 0
	addi	r4, r4, 8
	closure_address	%r17 read_all_object.2693
	swi	r17, r16 0
	swi	r15, r16 4
	addi	r15, r4 0
	addi	r4, r4, 8
	closure_address	%r17 read_and_network.2699
	swi	r17, r15 0
	lwi	r17, r3 28
	swi	r17, r15 4
	addi	r18, r4 0
	addi	r4, r4, 24
	closure_address	%r19 read_parameter.2701
	swi	r19, r18 0
	swi	r5, r18 20
	swi	r10, r18 16
	swi	r15, r18 12
	swi	r16, r18 8
	lwi	r5, r3 36
	swi	r5, r18 4
	addi	r10, r4 0
	addi	r4, r4, 8
	closure_address	%r15 solver_rect_surface.2703
	swi	r15, r10 0
	lwi	r15, r3 40
	swi	r15, r10 4
	addi	r16, r4 0
	addi	r4, r4, 8
	closure_address	%r19 solver_rect.2712
	swi	r19, r16 0
	swi	r10, r16 4
	addi	r10, r4 0
	addi	r4, r4, 8
	closure_address	%r19 solver_surface.2718
	swi	r19, r10 0
	swi	r15, r10 4
	addi	r19, r4 0
	addi	r4, r4, 8
	closure_address	%r20 solver_second.2737
	swi	r20, r19 0
	swi	r15, r19 4
	addi	r20, r4 0
	addi	r4, r4, 24
	closure_address	%r21 solver.2743
	swi	r21, r20 0
	swi	r10, r20 16
	swi	r19, r20 12
	swi	r16, r20 8
	swi	r14, r20 4
	addi	r10, r4 0
	addi	r4, r4, 8
	closure_address	%r16 solver_rect_fast.2747
	swi	r16, r10 0
	swi	r15, r10 4
	addi	r16, r4 0
	addi	r4, r4, 8
	closure_address	%r19 solver_surface_fast.2754
	swi	r19, r16 0
	swi	r15, r16 4
	addi	r19, r4 0
	addi	r4, r4, 8
	closure_address	%r21 solver_second_fast.2760
	swi	r21, r19 0
	swi	r15, r19 4
	addi	r21, r4 0
	addi	r4, r4, 24
	closure_address	%r22 solver_fast.2766
	swi	r22, r21 0
	swi	r16, r21 16
	swi	r19, r21 12
	swi	r10, r21 8
	swi	r14, r21 4
	addi	r16, r4 0
	addi	r4, r4, 8
	closure_address	%r19 solver_surface_fast2.2770
	swi	r19, r16 0
	swi	r15, r16 4
	addi	r19, r4 0
	addi	r4, r4, 8
	closure_address	%r22 solver_second_fast2.2777
	swi	r22, r19 0
	swi	r15, r19 4
	addi	r22, r4 0
	addi	r4, r4, 24
	closure_address	%r23 solver_fast2.2784
	swi	r23, r22 0
	swi	r16, r22 16
	swi	r19, r22 12
	swi	r10, r22 8
	swi	r14, r22 4
	addi	r10, r4 0
	addi	r4, r4, 8
	closure_address	%r16 iter_setup_dirvec_constants.2796
	swi	r16, r10 0
	swi	r14, r10 4
	addi	r16, r4 0
	addi	r4, r4, 16
	closure_address	%r19 setup_dirvec_constants.2799
	swi	r19, r16 0
	swi	r13, r16 8
	swi	r10, r16 4
	addi	r10, r4 0
	addi	r4, r4, 8
	closure_address	%r19 setup_startp_constants.2801
	swi	r19, r10 0
	swi	r14, r10 4
	addi	r19, r4 0
	addi	r4, r4, 16
	closure_address	%r23 setup_startp.2804
	swi	r23, r19 0
	lwi	r23, r3 92
	swi	r23, r19 12
	swi	r10, r19 8
	swi	r13, r19 4
	addi	r10, r4 0
	addi	r4, r4, 8
	closure_address	%r24 check_all_inside.2826
	swi	r24, r10 0
	swi	r14, r10 4
	addi	r24, r4 0
	addi	r4, r4, 32
	swi	r18, r3 140
	closure_address	%r18 shadow_check_and_group.2832
	swi	r18, r24 0
	swi	r21, r24 28
	swi	r15, r24 24
	swi	r14, r24 20
	lwi	r18, r3 128
	swi	r18, r24 16
	swi	r11, r24 12
	swi	r16, r3 144
	lwi	r16, r3 52
	swi	r16, r24 8
	swi	r10, r24 4
	addi	r13, r4 0
	addi	r4, r4, 16
	closure_address	%r8 shadow_check_one_or_group.2835
	swi	r8, r13 0
	swi	r24, r13 8
	swi	r17, r13 4
	addi	r8, r4 0
	addi	r4, r4, 24
	closure_address	%r24 shadow_check_one_or_matrix.2838
	swi	r24, r8 0
	swi	r21, r8 20
	swi	r15, r8 16
	swi	r13, r8 12
	swi	r18, r8 8
	swi	r16, r8 4
	addi	r13, r4 0
	addi	r4, r4, 40
	closure_address	%r21 solve_each_element.2841
	swi	r21, r13 0
	lwi	r21, r3 48
	swi	r21, r13 36
	lwi	r24, r3 88
	swi	r24, r13 32
	swi	r15, r13 28
	swi	r20, r13 24
	swi	r14, r13 20
	lwi	r18, r3 44
	swi	r18, r13 16
	swi	r16, r13 12
	lwi	r7, r3 56
	swi	r7, r13 8
	swi	r10, r13 4
	addi	r9, r4 0
	addi	r4, r4, 16
	closure_address	%r6 solve_one_or_network.2845
	swi	r6, r9 0
	swi	r13, r9 8
	swi	r17, r9 4
	addi	r6, r4 0
	addi	r4, r4, 24
	closure_address	%r13 trace_or_matrix.2849
	swi	r13, r6 0
	swi	r21, r6 20
	swi	r24, r6 16
	swi	r15, r6 12
	swi	r20, r6 8
	swi	r9, r6 4
	addi	r9, r4 0
	addi	r4, r4, 16
	closure_address	%r13 judge_intersection.2853
	swi	r13, r9 0
	swi	r6, r9 12
	swi	r21, r9 8
	swi	r5, r9 4
	addi	r6, r4 0
	addi	r4, r4, 40
	closure_address	%r13 solve_each_element_fast.2855
	swi	r13, r6 0
	swi	r21, r6 36
	swi	r23, r6 32
	swi	r22, r6 28
	swi	r15, r6 24
	swi	r14, r6 20
	swi	r18, r6 16
	swi	r16, r6 12
	swi	r7, r6 8
	swi	r10, r6 4
	addi	r10, r4 0
	addi	r4, r4, 16
	closure_address	%r13 solve_one_or_network_fast.2859
	swi	r13, r10 0
	swi	r6, r10 8
	swi	r17, r10 4
	addi	r6, r4 0
	addi	r4, r4, 24
	closure_address	%r13 trace_or_matrix_fast.2863
	swi	r13, r6 0
	swi	r21, r6 16
	swi	r22, r6 12
	swi	r15, r6 8
	swi	r10, r6 4
	addi	r10, r4 0
	addi	r4, r4, 16
	closure_address	%r13 judge_intersection_fast.2867
	swi	r13, r10 0
	swi	r6, r10 12
	swi	r21, r10 8
	swi	r5, r10 4
	addi	r6, r4 0
	addi	r4, r4, 16
	closure_address	%r13 get_nvector_rect.2869
	swi	r13, r6 0
	lwi	r13, r3 60
	swi	r13, r6 8
	swi	r18, r6 4
	addi	r15, r4 0
	addi	r4, r4, 8
	closure_address	%r17 get_nvector_plane.2871
	swi	r17, r15 0
	swi	r13, r15 4
	addi	r17, r4 0
	addi	r4, r4, 16
	closure_address	%r20 get_nvector_second.2873
	swi	r20, r17 0
	swi	r13, r17 8
	swi	r16, r17 4
	addi	r20, r4 0
	addi	r4, r4, 16
	closure_address	%r22 get_nvector.2875
	swi	r22, r20 0
	swi	r17, r20 12
	swi	r6, r20 8
	swi	r15, r20 4
	addi	r6, r4 0
	addi	r4, r4, 8
	closure_address	%r15 utexture.2878
	swi	r15, r6 0
	lwi	r15, r3 64
	swi	r15, r6 4
	addi	r17, r4 0
	addi	r4, r4, 16
	closure_address	%r22 add_light.2881
	swi	r22, r17 0
	swi	r15, r17 8
	lwi	r22, r3 72
	swi	r22, r17 4
	addi	r23, r4 0
	addi	r4, r4, 40
	closure_address	%r12 trace_reflections.2885
	swi	r12, r23 0
	swi	r8, r23 32
	lwi	r12, r3 136
	swi	r12, r23 28
	swi	r5, r23 24
	swi	r13, r23 20
	swi	r10, r23 16
	swi	r18, r23 12
	swi	r7, r23 8
	swi	r17, r23 4
	addi	r12, r4 0
	addi	r4, r4, 88
	swi	r10, r3 148
	closure_address	%r10 trace_ray.2890
	swi	r10, r12 0
	swi	r6, r12 80
	swi	r23, r12 76
	swi	r21, r12 72
	swi	r15, r12 68
	swi	r24, r12 64
	swi	r8, r12 60
	swi	r19, r12 56
	swi	r22, r12 52
	swi	r5, r12 48
	swi	r14, r12 44
	swi	r13, r12 40
	swi	r2, r12 36
	swi	r11, r12 32
	swi	r9, r12 28
	swi	r18, r12 24
	swi	r16, r12 20
	swi	r7, r12 16
	swi	r20, r12 12
	lwi	r9, r3 20
	swi	r9, r12 8
	swi	r17, r12 4
	addi	r9, r4 0
	addi	r4, r4, 56
	closure_address	%r10 trace_diffuse_ray.2896
	swi	r10, r9 0
	swi	r6, r9 48
	swi	r15, r9 44
	swi	r8, r9 40
	swi	r5, r9 36
	swi	r14, r9 32
	swi	r13, r9 28
	swi	r11, r9 24
	lwi	r5, r3 148
	swi	r5, r9 20
	swi	r16, r9 16
	swi	r7, r9 12
	swi	r20, r9 8
	lwi	r5, r3 68
	swi	r5, r9 4
	addi	r6, r4 0
	addi	r4, r4, 8
	closure_address	%r7 iter_trace_diffuse_rays.2899
	swi	r7, r6 0
	swi	r9, r6 4
	addi	r7, r4 0
	addi	r4, r4, 16
	closure_address	%r8 trace_diffuse_rays.2904
	swi	r8, r7 0
	swi	r19, r7 8
	swi	r6, r7 4
	addi	r6, r4 0
	addi	r4, r4, 16
	closure_address	%r8 trace_diffuse_ray_80percent.2908
	swi	r8, r6 0
	swi	r7, r6 8
	lwi	r8, r3 116
	swi	r8, r6 4
	addi	r9, r4 0
	addi	r4, r4, 16
	closure_address	%r10 calc_diffuse_using_1point.2912
	swi	r10, r9 0
	swi	r6, r9 12
	swi	r22, r9 8
	swi	r5, r9 4
	addi	r6, r4 0
	addi	r4, r4, 16
	closure_address	%r10 calc_diffuse_using_5points.2915
	swi	r10, r6 0
	swi	r22, r6 8
	swi	r5, r6 4
	addi	r10, r4 0
	addi	r4, r4, 8
	closure_address	%r13 do_without_neighbors.2921
	swi	r13, r10 0
	swi	r9, r10 4
	addi	r9, r4 0
	addi	r4, r4, 8
	closure_address	%r13 neighbors_exist.2924
	swi	r13, r9 0
	lwi	r13, r3 76
	swi	r13, r9 4
	addi	r15, r4 0
	addi	r4, r4, 16
	closure_address	%r16 try_exploit_neighbors.2937
	swi	r16, r15 0
	swi	r10, r15 8
	swi	r6, r15 4
	addi	r6, r4 0
	addi	r4, r4, 8
	closure_address	%r16 write_ppm_header.2944
	swi	r16, r6 0
	swi	r13, r6 4
	addi	r16, r4 0
	addi	r4, r4, 8
	closure_address	%r17 write_rgb.2948
	swi	r17, r16 0
	swi	r22, r16 4
	addi	r17, r4 0
	addi	r4, r4, 16
	closure_address	%r18 pretrace_diffuse_rays.2950
	swi	r18, r17 0
	swi	r7, r17 12
	swi	r8, r17 8
	swi	r5, r17 4
	addi	r5, r4 0
	addi	r4, r4, 40
	closure_address	%r7 pretrace_pixels.2953
	swi	r7, r5 0
	lwi	r7, r3 12
	swi	r7, r5 36
	swi	r12, r5 32
	swi	r24, r5 28
	lwi	r7, r3 96
	swi	r7, r5 24
	lwi	r7, r3 84
	swi	r7, r5 20
	swi	r22, r5 16
	lwi	r12, r3 108
	swi	r12, r5 12
	swi	r17, r5 8
	lwi	r12, r3 80
	swi	r12, r5 4
	addi	r17, r4 0
	addi	r4, r4, 32
	closure_address	%r18 pretrace_line.2960
	swi	r18, r17 0
	lwi	r18, r3 104
	swi	r18, r17 24
	lwi	r18, r3 100
	swi	r18, r17 20
	swi	r7, r17 16
	swi	r5, r17 12
	swi	r13, r17 8
	swi	r12, r17 4
	addi	r5, r4 0
	addi	r4, r4, 32
	closure_address	%r18 scan_pixel.2964
	swi	r18, r5 0
	swi	r16, r5 24
	swi	r15, r5 20
	swi	r22, r5 16
	swi	r9, r5 12
	swi	r13, r5 8
	swi	r10, r5 4
	addi	r9, r4 0
	addi	r4, r4, 16
	closure_address	%r10 scan_line.2970
	swi	r10, r9 0
	swi	r5, r9 12
	swi	r17, r9 8
	swi	r13, r9 4
	addi	r5, r4 0
	addi	r4, r4, 8
	closure_address	%r10 create_pixelline.2983
	swi	r10, r5 0
	swi	r13, r5 4
	addi	r10, r4 0
	addi	r4, r4, 8
	closure_address	%r15 calc_dirvec.2990
	swi	r15, r10 0
	swi	r8, r10 4
	addi	r15, r4 0
	addi	r4, r4, 8
	closure_address	%r16 calc_dirvecs.2998
	swi	r16, r15 0
	swi	r10, r15 4
	addi	r10, r4 0
	addi	r4, r4, 8
	closure_address	%r16 calc_dirvec_rows.3003
	swi	r16, r10 0
	swi	r15, r10 4
	addi	r15, r4 0
	addi	r4, r4, 8
	closure_address	%r16 create_dirvec.3007
	swi	r16, r15 0
	lwi	r16, r3 0
	swi	r16, r15 4
	addi	r18, r4 0
	addi	r4, r4, 8
	closure_address	%r19 create_dirvec_elements.3009
	swi	r19, r18 0
	swi	r15, r18 4
	addi	r19, r4 0
	addi	r4, r4, 16
	closure_address	%r20 create_dirvecs.3012
	swi	r20, r19 0
	swi	r8, r19 12
	swi	r18, r19 8
	swi	r15, r19 4
	addi	r18, r4 0
	addi	r4, r4, 8
	closure_address	%r20 init_dirvec_constants.3014
	swi	r20, r18 0
	lwi	r20, r3 144
	swi	r20, r18 4
	addi	r21, r4 0
	addi	r4, r4, 16
	closure_address	%r22 init_vecset_constants.3017
	swi	r22, r21 0
	swi	r18, r21 8
	swi	r8, r21 4
	addi	r8, r4 0
	addi	r4, r4, 16
	closure_address	%r18 init_dirvecs.3019
	swi	r18, r8 0
	swi	r21, r8 12
	swi	r19, r8 8
	swi	r10, r8 4
	addi	r10, r4 0
	addi	r4, r4, 16
	closure_address	%r18 add_reflection.3021
	swi	r18, r10 0
	swi	r20, r10 12
	lwi	r18, r3 136
	swi	r18, r10 8
	swi	r15, r10 4
	addi	r15, r4 0
	addi	r4, r4, 16
	closure_address	%r18 setup_rect_reflection.3028
	swi	r18, r15 0
	swi	r2, r15 12
	swi	r11, r15 8
	swi	r10, r15 4
	addi	r18, r4 0
	addi	r4, r4, 16
	closure_address	%r19 setup_surface_reflection.3031
	swi	r19, r18 0
	swi	r2, r18 12
	swi	r11, r18 8
	swi	r10, r18 4
	addi	r2, r4 0
	addi	r4, r4, 16
	closure_address	%r10 setup_reflections.3034
	swi	r10, r2 0
	swi	r18, r2 12
	swi	r15, r2 8
	swi	r14, r2 4
	addi	r24, r4 0
	addi	r4, r4, 64
	closure_address	%r10 rt.3036
	swi	r10, r24 0
	swi	r6, r24 56
	swi	r2, r24 52
	swi	r20, r24 48
	swi	r7, r24 44
	swi	r9, r24 40
	lwi	r2, r3 140
	swi	r2, r24 36
	swi	r17, r24 32
	swi	r16, r24 28
	lwi	r2, r3 128
	swi	r2, r24 24
	swi	r11, r24 20
	swi	r8, r24 16
	swi	r13, r24 12
	swi	r12, r24 8
	swi	r5, r24 4
	addi	r2, r0 128
	addi	r5, r0 128
	addi	r30 r31 0
	swi	r30, r3 156
	addi	r3, r3, 160
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -160
	lwi	r30, r3 156
	addi	r31 r30 0
	addi	r2, r0 0
	addi	r30 r31 0
	swi	r30, r3 156
	addi	r3, r3, 160
	jal	min_caml_print_int
	addi	r3, r3, -160
	lwi	r30, r3 156
	addi	r31 r30 0
   # main program end

#### f1 ####################################
#### r2 : loop counter , r5 base value  ####
#### r2 : previous heap pointer  ###########
#### use r28 ,r29               ############

 
min_caml_create_array:	
	addi	r29	r2	0
	addi	r2	r4	0
create_array_loop:
	seq		r28	r29	r0
	seq		r28 r28 r0
	jeql	r28 r0	create_array_exit
	swi		r5	r4	0
	addi	r29	r29	-1
	addi	r4	r4	4 #?
	jl		create_array_loop
create_array_exit:
	jr	r31



min_caml_create_float_array:
	addi	r29	r2	0
	addi	r2	r4	0
create_float_array_loop:
	seq		r28	r29	r0
	seq		r28	r28	r0
	jeql	r28	r0	create_float_array_exit
	fswi	f1  r4 0 # ?
	addi	r29	r29	-1
	addi	r4	r4	4 # ?
	jl	create_float_array_loop
create_float_array_exit:
	jr	r31
	
min_caml_sqrt:
	sqrt f1 f1
	jr r31





min_caml_read_int_sub:
	read r29
	addi r28 r0  32
	jeql r29 r28 min_caml_read_int_sub_exit
	addi r28 r0  10
	jeql r29 r28 min_caml_read_int_sub_exit
	jeql r29 r0  min_caml_read_int_sub_exit
	
	swi  r29 r3  0
	swi  r31 r3  4
	addi r3  r3  8
	jal  min_caml_read_int_sub
	addi r3  r3  -8
	lwi  r31 r3  4
	lwi  r29 r3  0
	
	addi r29 r29 -48
	mul  r29 r29 r27
	add  r2  r2  r29
	mul  r27 r27 r26
	jr   r31
min_caml_read_int_sub_exit:
	addi r2 r0 0
	addi r27 r0 1
	jr   r31
	
	
min_caml_read_int:
	addi  r26 r26 10
	jal   min_caml_read_int_sub
	jr    r31
	
	
	
	


min_caml_read_float_sub:
	read r29
	addi r28 r0 46
	jeql r28 r29 min_caml_read_float_comma
	
	addi r28 r0  32
	jeql r29 r28 min_caml_read_int_sub_exit
	addi r28 r0  10
	jeql r29 r28 min_caml_read_int_sub_exit
	jeql r29 r0  min_caml_read_int_sub_exit
	
	mul  r28 r28 r26
	swi  r31 r3  0
	swi  r29 r3  4
	addi r3  r3  8
	jal  min_caml_read_float_sub
	addi r3  r3  -8
	lwi  r29 r3  4
	lwi  r31 r3  0
	addi r29 r29 -48
	mul  r29 r29 r25
	add  r27 r27 r29
	addi r29 r0  10
	mul  r25 r25 r29
	jr r31
	
min_caml_read_float_comma:
	addi r26 r0 10
	jl min_caml_read_float_sub
min_caml_read_float_exit:
	addi r27 r27 0
	addi r25 r25 1
	jr   r31
	
	
min_caml_read_float:
	swi  r31 r3  0
	addi r3  r3  4
	
		jal min_caml_read_float_sub
		swi  r25 r3  0
		addi r3  r3  4
		
			addi r2  r27 0
			jal min_caml_float_of_int
		
		addi r3 r3 -4
		fswi f1  r3  0
		addi r3  r3  4

			jal min_caml_float_of_int
		
		addi r3  r3  -4
		flwi f29 r3  0
		fadd f1  f1  f29
	
	addi r3  r3  -4
	lwi  r31 r3  0
	jr   r31
	
	


min_caml_print_int_rec:
	jeql r2  r0  min_caml_print_int_exit
	div  r28 r2  r29
	mul  r28 r28 r29
	sub  r28 r2  r28
	addi r28 r28 48
	
	swi  r28 r3  0
	swi  r31 r3  4
	addi r3  r3  8
		
		div  r2  r2  r29
		jal  min_caml_print_int_rec
		
	addi r3  r3  -8
	lwi  r31 r3  4
	lwi  r28 r3  0
	write r28
	jr   r31
	
min_caml_print_int_exit:
	jr r31

min_caml_print_int:
	jeql r2  r0  min_caml_print_int_print_zero
	addi r29 r0  10
	jal min_caml_print_int_rec
	jr  r31
	
	
min_caml_print_int_print_zero:
	addi r2  r2  48
	write  r2
	jr   r31
	
	
	


min_caml_print_char:
	write r2
	jr    r31
	
	
	
