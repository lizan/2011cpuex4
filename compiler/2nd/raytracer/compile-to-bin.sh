#!/bin/sh


cd ../mincaml
make
cd ../raytracer


# compile to asm
cd output
rm -f min-rt.s debug2.dat debug16.dat min-rt.bin
cd ../


cd min-rt
cat min-rt-lib/Komaki/fabs.ml >> min-rt.ml
cat min-rt-lib/Komaki/fhalf.ml >> min-rt.ml
cat min-rt-lib/Komaki/fisneg.ml >> min-rt.ml
cat min-rt-lib/Komaki/fispos.ml >> min-rt.ml
cat min-rt-lib/Komaki/fiszero.ml >> min-rt.ml
cat min-rt-lib/Komaki/fless.ml >> min-rt.ml
cat min-rt-lib/Komaki/fneg.ml >> min-rt.ml
cat min-rt-lib/Komaki/fsqr.ml >> min-rt.ml
cat min-rt-lib/Komaki/sin.ml >> min-rt.ml
cat min-rt-lib/Komaki/cos.ml >> min-rt.ml
cat min-rt-lib/Komaki/atan.ml >> min-rt.ml
cat base.ml >> min-rt.ml
../../mincaml/./min-caml min-rt 
rm -f min-rt.ml

cat min-rt.s >> ../output/min-rt.s
cat min-rt-lib/Komaki/create_array.s >> ../output/min-rt.s
cat min-rt-lib/Komaki/sqrt.s >> ../output/min-rt.s
cat min-rt-lib/Komaki/read_int.s >> ../output/min-rt.s
cat min-rt-lib/Komaki/read_float.s >> ../output/min-rt.s
cat min-rt-lib/Komaki/print_int.s >> ../output/min-rt.s
cat min-rt-lib/Komaki/print_char.s >> ../output/min-rt.s
cat min-rt-lib/kozai/floor.s >> ../output/min-rt.s
cat min-rt-lib/kozai/ftoi.s >> ../output/min-rt.s
cat min-rt-lib/kozai/itf.s >> ../output/min-rt.s
rm -f min-rt.s kNormal_output.dat syntax_output.dat
cd ../

cd ../mincaml
make clean
cd ../raytracer




#asm to bin
cd ../../../asm2bin/2nd
make
cd ../../compiler/2nd/raytracer


cd output
../../../../asm2bin/2nd/./AsmToBin < min-rt.s > min-rt.bin
cd ../


cd ../../../asm2bin/2nd
make clean
cd ../../compiler/2nd/raytracer

