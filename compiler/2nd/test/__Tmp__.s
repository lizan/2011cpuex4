	jal __start__
	ASITIS 00000000000000010101111110010000
__start__: # hoge
	addi	r3 r3 300
	mul	r3 r3 r3
	addi	%r4 %r4 900
	mul	%r4 %r4 %r4
	INIT	1000000000.000000 l.13169
	INIT	128.000000 l.13041
	INIT	0.900000 l.12859
	INIT	150.000000 l.12536
	INIT	-150.000000 l.12534
	INIT	0.100000 l.12474
	INIT	-2.000000 l.12436
	INIT	0.003906 l.12422
	INIT	100000000.000000 l.12362
	INIT	255.000000 l.12341
	INIT	20.000000 l.12338
	INIT	0.050000 l.12337
	INIT	0.250000 l.12332
	INIT	10.000000 l.12328
	INIT	0.300000 l.12322
	INIT	0.150000 l.12321
	INIT	3.141593 l.12317
	INIT	30.000000 l.12316
	INIT	15.000000 l.12315
	INIT	0.000100 l.12314
	INIT	-0.100000 l.12184
	INIT	0.010000 l.12163
	INIT	-0.200000 l.12162
	INIT	-200.000000 l.11853
	INIT	200.000000 l.11851
	INIT	0.017453 l.11850
	INIT	-1.000000 l.11828
	INIT	2.437500 l.11811
	INIT	0.437500 l.11810
	INIT	0.060035 l.11809
	INIT	0.089764 l.11808
	INIT	0.111111 l.11807
	INIT	0.142857 l.11806
	INIT	0.200000 l.11805
	INIT	0.333333 l.11804
	INIT	0.000196 l.11797
	INIT	0.008333 l.11796
	INIT	0.166667 l.11795
	INIT	1.000000 l.11794
	INIT	0.001370 l.11793
	INIT	0.041664 l.11792
	INIT	804.247719 l.11791
	INIT	402.123860 l.11790
	INIT	201.061930 l.11789
	INIT	100.530965 l.11788
	INIT	50.265482 l.11787
	INIT	25.132741 l.11786
	INIT	12.566371 l.11785
	INIT	6.283185 l.11784
	INIT	0.000000 l.11783
	INIT	0.785398 l.11782
	INIT	1.570796 l.11781
	INIT	3.141593 l.11780
	INIT	0.500000 l.11779
	INIT	2.000000 l.11778
	jal	_min_caml_start
redp.6276:
	flwi	f2, r28 1 #
	fgt	r30 f1 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21041
	flwl	%f3 l.11778
	fmul	f1, f1, f3
	fgt	r30 f1 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21042
	fmul	f1, f1, f3
	fgt	r30 f1 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21043
	fmul	f1, f1, f3
	fgt	r30 f1 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21044
	fmul	f1, f1, f3
	fgt	r30 f1 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21045
	fmul	f1, f1, f3
	fgt	r30 f1 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21046
	fmul	f1, f1, f3
	fgt	r30 f1 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21047
	fmul	f1, f1, f3
	fgt	r30 f1 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21048
	fmul	f1, f1, f3
	lwi	r29, r28 0
	jr	r29
ble_else.21048:
	jr	r31
ble_else.21047:
	jr	r31
ble_else.21046:
	jr	r31
ble_else.21045:
	jr	r31
ble_else.21044:
	jr	r31
ble_else.21043:
	jr	r31
ble_else.21042:
	jr	r31
ble_else.21041:
	jr	r31
redx.6279:
	flwi	f3, r28 1 #
	fgt	r30 f3 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21049
	fgt	r30 f1 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21050
	flwl	%f4 l.11779
	fmul	f5, f1, f4
	fsub	f2, f2, f1
	fgt	r30 f3 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21051
	fgt	r30 f5 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21052
	fmul	f1, f5, f4
	fsub	f2, f2, f5
	lwi	r29, r28 0
	jr	r29
ble_else.21052:
	fmul	f1, f5, f4
	lwi	r29, r28 0
	jr	r29
ble_else.21051:
	fadd	f1, f2 f0
	jr	r31
ble_else.21050:
	flwl	%f4 l.11779
	fmul	f1, f1, f4
	fgt	r30 f3 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21053
	fgt	r30 f1 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21054
	fmul	f3, f1, f4
	fsub	f2, f2, f1
	fadd	f1, f3 f0
	lwi	r29, r28 0
	jr	r29
ble_else.21054:
	fmul	f1, f1, f4
	lwi	r29, r28 0
	jr	r29
ble_else.21053:
	fadd	f1, f2 f0
	jr	r31
ble_else.21049:
	fadd	f1, f2 f0
	jr	r31
sin.2572:
	flwl	%f2 l.11780
	flwl	%f3 l.11781
	flwl	%f4 l.11782
	flwl	%f5 l.11783
	fgt	r30 f1 f5
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21055
	addi	r1, r0 0
	jl	ble_cont.21056
ble_else.21055:
	addi	r1, r0 1
ble_cont.21056:
	fgt	r30 f5 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21057
	jl	ble_cont.21058
ble_else.21057:
	fsub	f1, f5, f1
ble_cont.21058:
	flwl	%f6 l.11784
	addi	r28, r4 0
	addi	r4, r4, 2
	closure_address	%r2 redp.6276
	swi	r2, r28 0
	fswi	f1, r28 1
	fswi	f5, r3 0
	fswi	f4, r3 2
	fswi	f3, r3 4
	swi	r1, r3 5
	fswi	f2, r3 6
	fswi	f1, r3 8
	fswi	f6, r3 10
	fgt	r30 f6 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21063
	flwl	%f7 l.11785
	fgt	r30 f7 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21065
	flwl	%f7 l.11786
	fgt	r30 f7 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21067
	flwl	%f7 l.11787
	fgt	r30 f7 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21069
	flwl	%f7 l.11788
	fgt	r30 f7 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21071
	flwl	%f7 l.11789
	fgt	r30 f7 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21073
	flwl	%f7 l.11790
	fgt	r30 f7 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21075
	flwl	%f7 l.11791
	fadd	f1, f7 f0
	swi	r31 r3 11
	addi	r3, r3, 12
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -12
	lwi	r31, r3 11
	jl	ble_cont.21076
ble_else.21075:
	fadd	f1, f7 f0
ble_cont.21076:
	jl	ble_cont.21074
ble_else.21073:
	fadd	f1, f7 f0
ble_cont.21074:
	jl	ble_cont.21072
ble_else.21071:
	fadd	f1, f7 f0
ble_cont.21072:
	jl	ble_cont.21070
ble_else.21069:
	fadd	f1, f7 f0
ble_cont.21070:
	jl	ble_cont.21068
ble_else.21067:
	fadd	f1, f7 f0
ble_cont.21068:
	jl	ble_cont.21066
ble_else.21065:
	fadd	f1, f7 f0
ble_cont.21066:
	jl	ble_cont.21064
ble_else.21063:
	fadd	f1, f6 f0
ble_cont.21064:
	addi	r28, r4 0
	addi	r4, r4, 2
	closure_address	%r1 redx.6279
	swi	r1, r28 0
	flwi	f2, r3 10 #off
	fswi	f2, r28 1
	flwi	f3, r3 8 #off
	fgt	r30 f2 f3
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21077
	fgt	r30 f1 f3
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21079
	flwl	%f2 l.11779
	fmul	f2, f1, f2
	fsub	f1, f3, f1
	fadd	f31, f2 f0
	fadd	f2, f1 f0
	fadd	f1, f31 f0
	swi	r31 r3 11
	addi	r3, r3, 12
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -12
	lwi	r31, r3 11
	jl	ble_cont.21080
ble_else.21079:
	flwl	%f2 l.11779
	fmul	f1, f1, f2
	fadd	f2, f3 f0
	swi	r31 r3 11
	addi	r3, r3, 12
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -12
	lwi	r31, r3 11
ble_cont.21080:
	jl	ble_cont.21078
ble_else.21077:
	fadd	f1, f3 f0
ble_cont.21078:
	flwi	f2, r3 6 #off
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21081
	fsub	f3, f1, f2
	jl	ble_cont.21082
ble_else.21081:
	fadd	f3, f1 f0
ble_cont.21082:
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21083
	lwi	r1, r3 5
	addi	r30, r0, 0
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21085
	addi	r1, r0 1
	jl	beq_cont.21086
beq_else.21085:
	addi	r1, r0 0
beq_cont.21086:
	jl	ble_cont.21084
ble_else.21083:
	lwi	r1, r3 5
ble_cont.21084:
	flwi	f1, r3 4 #off
	fgt	r30 f1 f3
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21087
	fsub	f2, f2, f3
	jl	ble_cont.21088
ble_else.21087:
	fadd	f2, f3 f0
ble_cont.21088:
	flwi	f3, r3 2 #off
	fgt	r30 f2 f3
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21089
	fmul	f1, f2, f2
	fmul	f3, f1, f1
	fmul	f4, f1, f3
	flwl	%f5 l.11795
	flwl	%f6 l.11796
	flwl	%f7 l.11797
	flwl	%f8 l.11794
	fmul	f1, f1, f5
	fsub	f1, f8, f1
	fmul	f3, f3, f6
	fadd	f1, f1, f3
	fmul	f3, f4, f7
	fsub	f1, f1, f3
	fmul	f1, f2, f1
	jl	ble_cont.21090
ble_else.21089:
	fsub	f1, f1, f2
	fmul	f1, f1, f1
	fmul	f2, f1, f1
	fmul	f3, f1, f2
	flwl	%f4 l.11779
	flwl	%f5 l.11792
	flwl	%f6 l.11793
	flwl	%f7 l.11794
	fmul	f1, f1, f4
	fsub	f1, f7, f1
	fmul	f2, f2, f5
	fadd	f1, f1, f2
	fmul	f2, f3, f6
	fsub	f1, f1, f2
ble_cont.21090:
	addi	r30, r0, 0
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21091
	flwi	f2, r3 0 #off
	fsub	f1, f2, f1
	jr	r31
beq_else.21091:
	jr	r31
k_atan.6231:
	fmul	f2, f1, f1
	flwl	%f3 l.11804
	flwl	%f4 l.11805
	flwl	%f5 l.11806
	flwl	%f6 l.11807
	flwl	%f7 l.11808
	flwl	%f8 l.11809
	flwl	%f9 l.11794
	fmul	f8, f2, f8
	fsub	f7, f7, f8
	fmul	f7, f2, f7
	fadd	f6, f6, f7
	fmul	f6, f2, f6
	fsub	f5, f5, f6
	fmul	f5, f2, f5
	fadd	f4, f4, f5
	fmul	f4, f2, f4
	fadd	f3, f3, f4
	fmul	f2, f2, f3
	fsub	f2, f9, f2
	fmul	f1, f1, f2
	jr	r31
atan.2576:
	flwl	%f2 l.11781
	flwl	%f3 l.11782
	flwl	%f4 l.11783
	fgt	r30 f1 f4
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21092
	addi	r1, r0 0
	jl	ble_cont.21093
ble_else.21092:
	addi	r1, r0 1
ble_cont.21093:
	fgt	r30 f4 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21094
	fadd	f5, f1 f0
	jl	ble_cont.21095
ble_else.21094:
	fsub	f5, f4, f1
ble_cont.21095:
	flwl	%f6 l.11810
	fswi	f4, r3 0
	swi	r1, r3 1
	fgt	r30 f6 f5
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21096
	flwl	%f1 l.11811
	fgt	r30 f1 f5
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21098
	flwl	%f1 l.11794
	fdiv	f1, f1, f5
	fswi	f2, r3 2
	swi	r31 r3 3
	addi	r3, r3, 4
	jal	k_atan.6231
	addi	r3, r3, -4
	lwi	r31 r3 3
	flwi	f2, r3 2 #off
	fsub	f1, f2, f1
	jl	ble_cont.21099
ble_else.21098:
	flwl	%f1 l.11794
	fsub	f2, f5, f1
	fadd	f1, f5, f1
	fdiv	f1, f2, f1
	fswi	f3, r3 4
	swi	r31 r3 5
	addi	r3, r3, 6
	jal	k_atan.6231
	addi	r3, r3, -6
	lwi	r31 r3 5
	flwi	f2, r3 4 #off
	fadd	f1, f2, f1
ble_cont.21099:
	jl	ble_cont.21097
ble_else.21096:
	swi	r31 r3 5
	addi	r3, r3, 6
	jal	k_atan.6231
	addi	r3, r3, -6
	lwi	r31 r3 5
ble_cont.21097:
	lwi	r1, r3 1
	addi	r30, r0, 0
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21101
	flwi	f2, r3 0 #off
	fsub	f1, f2, f1
	jr	r31
beq_else.21101:
	jr	r31
read_int_rec.2578:
	swi	r1, r3 0
	swi	r31 r3 1
	addi	r3, r3, 2
	jal	min_caml_read
	addi	r3, r3, -2
	lwi	r31 r3 1
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30, r30, r0
	jeql	r30, r0, bge_else.21102
	addi	r2, r0 10
	lwi	r5, r3 0
	mul	r5, r5, r2
	addi	r1, r1, -48
	add	r1, r5, r1
	swi	r2, r3 1
	swi	r1, r3 2
	swi	r31 r3 3
	addi	r3, r3, 4
	jal	min_caml_read
	addi	r3, r3, -4
	lwi	r31 r3 3
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30, r30, r0
	jeql	r30, r0, bge_else.21103
	lwi	r2, r3 1
	lwi	r5, r3 2
	mul	r5, r5, r2
	addi	r1, r1, -48
	add	r1, r5, r1
	swi	r1, r3 3
	swi	r31 r3 4
	addi	r3, r3, 5
	jal	min_caml_read
	addi	r3, r3, -5
	lwi	r31 r3 4
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30, r30, r0
	jeql	r30, r0, bge_else.21104
	lwi	r2, r3 1
	lwi	r5, r3 3
	mul	r5, r5, r2
	addi	r1, r1, -48
	add	r1, r5, r1
	swi	r1, r3 4
	swi	r31 r3 5
	addi	r3, r3, 6
	jal	min_caml_read
	addi	r3, r3, -6
	lwi	r31 r3 5
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30, r30, r0
	jeql	r30, r0, bge_else.21105
	lwi	r2, r3 1
	lwi	r5, r3 4
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r1, r2, r1
	jl	read_int_rec.2578
bge_else.21105:
	lwi	r1, r3 4
	jr	r31
bge_else.21104:
	lwi	r1, r3 3
	jr	r31
bge_else.21103:
	lwi	r1, r3 2
	jr	r31
bge_else.21102:
	lwi	r1, r3 0
	jr	r31
read_int.2580:
	swi	r31 r3 0
	addi	r3, r3, 1
	jal	min_caml_read
	addi	r3, r3, -1
	lwi	r31 r3 0
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21106
	addi	r1, r0 0
	swi	r1, r3 0
	swi	r31 r3 1
	addi	r3, r3, 2
	jal	min_caml_read
	addi	r3, r3, -2
	lwi	r31 r3 1
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21107
	swi	r31 r3 1
	addi	r3, r3, 2
	jal	read_int.2580
	addi	r3, r3, -2
	lwi	r31 r3 1
	lwi	r2, r3 0
	sub	r1, r2, r1
	jl	beq_cont.21108
beq_else.21107:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21109
	addi	r1, r1, -48
	swi	r1, r3 1
	swi	r31 r3 2
	addi	r3, r3, 3
	jal	min_caml_read
	addi	r3, r3, -3
	lwi	r31 r3 2
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21111
	addi	r2, r0 10
	lwi	r5, r3 1
	mul	r5, r5, r2
	addi	r1, r1, -48
	add	r1, r5, r1
	swi	r2, r3 2
	swi	r1, r3 3
	swi	r31 r3 4
	addi	r3, r3, 5
	jal	min_caml_read
	addi	r3, r3, -5
	lwi	r31 r3 4
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21113
	lwi	r2, r3 2
	lwi	r5, r3 3
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r1, r2, r1
	swi	r31 r3 4
	addi	r3, r3, 5
	jal	read_int_rec.2578
	addi	r3, r3, -5
	lwi	r31 r3 4
	jl	bge_cont.21114
bge_else.21113:
	lwi	r1, r3 3
bge_cont.21114:
	jl	bge_cont.21112
bge_else.21111:
	lwi	r1, r3 1
bge_cont.21112:
	jl	bge_cont.21110
bge_else.21109:
	swi	r31 r3 4
	addi	r3, r3, 5
	jal	read_int.2580
	addi	r3, r3, -5
	lwi	r31 r3 4
bge_cont.21110:
beq_cont.21108:
	lwi	r2, r3 0
	sub	r1, r2, r1
	jr	r31
beq_else.21106:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30, r30, r0
	jeql	r30, r0, bge_else.21115
	addi	r1, r1, -48
	swi	r1, r3 4
	swi	r31 r3 5
	addi	r3, r3, 6
	jal	min_caml_read
	addi	r3, r3, -6
	lwi	r31 r3 5
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30, r30, r0
	jeql	r30, r0, bge_else.21116
	addi	r2, r0 10
	lwi	r5, r3 4
	mul	r5, r5, r2
	addi	r1, r1, -48
	add	r1, r5, r1
	swi	r2, r3 5
	swi	r1, r3 6
	swi	r31 r3 7
	addi	r3, r3, 8
	jal	min_caml_read
	addi	r3, r3, -8
	lwi	r31 r3 7
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30, r30, r0
	jeql	r30, r0, bge_else.21117
	lwi	r2, r3 5
	lwi	r5, r3 6
	mul	r5, r5, r2
	addi	r1, r1, -48
	add	r1, r5, r1
	swi	r1, r3 7
	swi	r31 r3 8
	addi	r3, r3, 9
	jal	min_caml_read
	addi	r3, r3, -9
	lwi	r31 r3 8
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30, r30, r0
	jeql	r30, r0, bge_else.21118
	lwi	r2, r3 5
	lwi	r5, r3 7
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r1, r2, r1
	jl	read_int_rec.2578
bge_else.21118:
	lwi	r1, r3 7
	jr	r31
bge_else.21117:
	lwi	r1, r3 6
	jr	r31
bge_else.21116:
	lwi	r1, r3 4
	jr	r31
bge_else.21115:
	swi	r31 r3 8
	addi	r3, r3, 9
	jal	min_caml_read
	addi	r3, r3, -9
	lwi	r31 r3 8
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21119
	addi	r1, r0 0
	swi	r1, r3 8
	swi	r31 r3 9
	addi	r3, r3, 10
	jal	read_int.2580
	addi	r3, r3, -10
	lwi	r31 r3 9
	lwi	r2, r3 8
	sub	r1, r2, r1
	jr	r31
beq_else.21119:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30, r30, r0
	jeql	r30, r0, bge_else.21120
	addi	r1, r1, -48
	swi	r1, r3 9
	swi	r31 r3 10
	addi	r3, r3, 11
	jal	min_caml_read
	addi	r3, r3, -11
	lwi	r31 r3 10
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30, r30, r0
	jeql	r30, r0, bge_else.21121
	addi	r2, r0 10
	lwi	r5, r3 9
	mul	r5, r5, r2
	addi	r1, r1, -48
	add	r1, r5, r1
	swi	r2, r3 10
	swi	r1, r3 11
	swi	r31 r3 12
	addi	r3, r3, 13
	jal	min_caml_read
	addi	r3, r3, -13
	lwi	r31 r3 12
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30, r30, r0
	jeql	r30, r0, bge_else.21122
	lwi	r2, r3 10
	lwi	r5, r3 11
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r1, r2, r1
	jl	read_int_rec.2578
bge_else.21122:
	lwi	r1, r3 11
	jr	r31
bge_else.21121:
	lwi	r1, r3 9
	jr	r31
bge_else.21120:
	jl	read_int.2580
read_float_rec.2582:
	swi	r1, r3 0
	swi	r5, r3 1
	swi	r2, r3 2
	swi	r31 r3 3
	addi	r3, r3, 4
	jal	min_caml_read
	addi	r3, r3, -4
	lwi	r31 r3 3
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21123
	addi	r1, r0 10
	swi	r1, r3 3
	swi	r31 r3 4
	addi	r3, r3, 5
	jal	min_caml_read
	addi	r3, r3, -5
	lwi	r31 r3 4
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21124
	lwi	r1, r3 3
	lwi	r2, r3 2
	lwi	r5, r3 1
	jl	read_float_rec.2582
beq_else.21124:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30, r30, r0
	jeql	r30, r0, bge_else.21125
	lwi	r2, r3 3
	lwi	r5, r3 2
	mul	r5, r5, r2
	addi	r1, r1, -48
	add	r1, r5, r1
	lwi	r5, r3 1
	mul	r5, r5, r2
	addi	r29, r2 0 #
	addi	r2, r1 0 #
	addi	r1, r29 0 #
	jl	read_float_rec.2582
bge_else.21125:
	lwi	r1, r3 2
	swi	r31 r3 4
	addi	r3, r3, 5
	jal	min_caml_float_of_int
	addi	r3, r3, -5
	lwi	r31 r3 4
	lwi	r1, r3 1
	fswi	f1, r3 4
	swi	r31 r3 5
	addi	r3, r3, 6
	jal	min_caml_float_of_int
	addi	r3, r3, -6
	lwi	r31 r3 5
	flwi	f2, r3 4 #off
	fdiv	f1, f2, f1
	jr	r31
beq_else.21123:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30, r30, r0
	jeql	r30, r0, bge_else.21126
	addi	r2, r0 10
	lwi	r5, r3 2
	mul	r5, r5, r2
	addi	r1, r1, -48
	add	r1, r5, r1
	lwi	r5, r3 0
	lwi	r6, r3 1
	mul	r6, r6, r5
	swi	r6, r3 5
	swi	r1, r3 6
	swi	r2, r3 7
	swi	r31 r3 8
	addi	r3, r3, 9
	jal	min_caml_read
	addi	r3, r3, -9
	lwi	r31 r3 8
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21127
	lwi	r1, r3 7
	lwi	r2, r3 6
	lwi	r5, r3 5
	jl	read_float_rec.2582
beq_else.21127:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30, r30, r0
	jeql	r30, r0, bge_else.21128
	lwi	r2, r3 7
	lwi	r5, r3 6
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 0
	lwi	r5, r3 5
	mul	r5, r5, r1
	jl	read_float_rec.2582
bge_else.21128:
	lwi	r1, r3 6
	swi	r31 r3 8
	addi	r3, r3, 9
	jal	min_caml_float_of_int
	addi	r3, r3, -9
	lwi	r31 r3 8
	lwi	r1, r3 5
	fswi	f1, r3 8
	swi	r31 r3 9
	addi	r3, r3, 10
	jal	min_caml_float_of_int
	addi	r3, r3, -10
	lwi	r31 r3 9
	flwi	f2, r3 8 #off
	fdiv	f1, f2, f1
	jr	r31
bge_else.21126:
	lwi	r1, r3 2
	swi	r31 r3 9
	addi	r3, r3, 10
	jal	min_caml_float_of_int
	addi	r3, r3, -10
	lwi	r31 r3 9
	lwi	r1, r3 1
	fswi	f1, r3 10
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	min_caml_float_of_int
	addi	r3, r3, -12
	lwi	r31 r3 11
	flwi	f2, r3 10 #off
	fdiv	f1, f2, f1
	jr	r31
read_float.2586:
	swi	r31 r3 0
	addi	r3, r3, 1
	jal	min_caml_read
	addi	r3, r3, -1
	lwi	r31 r3 0
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21130
	flwl	%f1 l.11783
	fswi	f1, r3 0
	swi	r31 r3 1
	addi	r3, r3, 2
	jal	min_caml_read
	addi	r3, r3, -2
	lwi	r31 r3 1
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21131
	swi	r31 r3 1
	addi	r3, r3, 2
	jal	read_float.2586
	addi	r3, r3, -2
	lwi	r31 r3 1
	flwi	f2, r3 0 #off
	fsub	f1, f2, f1
	jl	beq_cont.21132
beq_else.21131:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21133
	addi	r2, r0 1
	addi	r1, r1, -48
	swi	r2, r3 1
	swi	r1, r3 2
	swi	r31 r3 3
	addi	r3, r3, 4
	jal	min_caml_read
	addi	r3, r3, -4
	lwi	r31 r3 3
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21135
	addi	r1, r0 10
	lwi	r2, r3 2
	lwi	r5, r3 1
	swi	r31 r3 3
	addi	r3, r3, 4
	jal	read_float_rec.2582
	addi	r3, r3, -4
	lwi	r31 r3 3
	jl	beq_cont.21136
beq_else.21135:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21137
	addi	r2, r0 10
	lwi	r5, r3 2
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 1
	addi	r5, r1 0 #
	swi	r31 r3 3
	addi	r3, r3, 4
	jal	read_float_rec.2582
	addi	r3, r3, -4
	lwi	r31 r3 3
	jl	bge_cont.21138
bge_else.21137:
	lwi	r1, r3 2
	swi	r31 r3 3
	addi	r3, r3, 4
	jal	min_caml_float_of_int
	addi	r3, r3, -4
	lwi	r31 r3 3
	lwi	r1, r3 1
	fswi	f1, r3 4
	swi	r31 r3 5
	addi	r3, r3, 6
	jal	min_caml_float_of_int
	addi	r3, r3, -6
	lwi	r31 r3 5
	flwi	f2, r3 4 #off
	fdiv	f1, f2, f1
bge_cont.21138:
beq_cont.21136:
	jl	bge_cont.21134
bge_else.21133:
	swi	r31 r3 5
	addi	r3, r3, 6
	jal	read_float.2586
	addi	r3, r3, -6
	lwi	r31 r3 5
bge_cont.21134:
beq_cont.21132:
	flwi	f2, r3 0 #off
	fsub	f1, f2, f1
	jr	r31
beq_else.21130:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30, r30, r0
	jeql	r30, r0, bge_else.21140
	addi	r2, r0 1
	addi	r1, r1, -48
	swi	r2, r3 5
	swi	r1, r3 6
	swi	r31 r3 7
	addi	r3, r3, 8
	jal	min_caml_read
	addi	r3, r3, -8
	lwi	r31 r3 7
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21141
	addi	r1, r0 10
	lwi	r2, r3 6
	lwi	r5, r3 5
	jl	read_float_rec.2582
beq_else.21141:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30, r30, r0
	jeql	r30, r0, bge_else.21142
	addi	r2, r0 10
	lwi	r5, r3 6
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 5
	addi	r5, r1 0 #
	jl	read_float_rec.2582
bge_else.21142:
	lwi	r1, r3 6
	swi	r31 r3 7
	addi	r3, r3, 8
	jal	min_caml_float_of_int
	addi	r3, r3, -8
	lwi	r31 r3 7
	lwi	r1, r3 5
	fswi	f1, r3 8
	swi	r31 r3 9
	addi	r3, r3, 10
	jal	min_caml_float_of_int
	addi	r3, r3, -10
	lwi	r31 r3 9
	flwi	f2, r3 8 #off
	fdiv	f1, f2, f1
	jr	r31
bge_else.21140:
	swi	r31 r3 9
	addi	r3, r3, 10
	jal	min_caml_read
	addi	r3, r3, -10
	lwi	r31 r3 9
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21144
	flwl	%f1 l.11783
	fswi	f1, r3 10
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	read_float.2586
	addi	r3, r3, -12
	lwi	r31 r3 11
	flwi	f2, r3 10 #off
	fsub	f1, f2, f1
	jr	r31
beq_else.21144:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30, r30, r0
	jeql	r30, r0, bge_else.21146
	addi	r2, r0 1
	addi	r1, r1, -48
	swi	r2, r3 11
	swi	r1, r3 12
	swi	r31 r3 13
	addi	r3, r3, 14
	jal	min_caml_read
	addi	r3, r3, -14
	lwi	r31 r3 13
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21147
	addi	r1, r0 10
	lwi	r2, r3 12
	lwi	r5, r3 11
	jl	read_float_rec.2582
beq_else.21147:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30, r30, r0
	jeql	r30, r0, bge_else.21148
	addi	r2, r0 10
	lwi	r5, r3 12
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 11
	addi	r5, r1 0 #
	jl	read_float_rec.2582
bge_else.21148:
	lwi	r1, r3 12
	swi	r31 r3 13
	addi	r3, r3, 14
	jal	min_caml_float_of_int
	addi	r3, r3, -14
	lwi	r31 r3 13
	lwi	r1, r3 11
	fswi	f1, r3 14
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	min_caml_float_of_int
	addi	r3, r3, -16
	lwi	r31 r3 15
	flwi	f2, r3 14 #off
	fdiv	f1, f2, f1
	jr	r31
bge_else.21146:
	jl	read_float.2586
my_div_sub.6173:
	lwi	r5, r28 2
	lwi	r6, r28 1
	addi	r7, r1, 1
	seq	r30, r7, r2
	jeql	r30, r0, beq_else.21150
	jr	r31
beq_else.21150:
	add	r8, r1, r2
	addi	r9, r0 2
	div	r8, r8, r9
	mul	r10, r6, r8
	sgt	r30, r10, r5
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.21151
	addi	r1, r8, 1
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21152
	addi	r1, r8 0
	jr	r31
beq_else.21152:
	add	r1, r8, r2
	div	r1, r1, r9
	mul	r6, r6, r1
	sgt	r30, r6, r5
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.21153
	lwi	r29, r28 0
	jr	r29
ble_else.21153:
	addi	r2, r1 0 #
	addi	r1, r8 0 #
	lwi	r29, r28 0
	jr	r29
ble_else.21151:
	seq	r30, r7, r8
	jeql	r30, r0, beq_else.21154
	jr	r31
beq_else.21154:
	add	r2, r1, r8
	div	r2, r2, r9
	mul	r6, r6, r2
	sgt	r30, r6, r5
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.21155
	addi	r1, r2 0 #
	addi	r2, r8 0 #
	lwi	r29, r28 0
	jr	r29
ble_else.21155:
	lwi	r29, r28 0
	jr	r29
my_div.2588:
	addi	r28, r4 0
	addi	r4, r4, 3
	closure_address	%r5 my_div_sub.6173
	swi	r5, r28 0
	swi	r1, r28 2
	swi	r2, r28 1
	addi	r5, r0 0
	sub	r5, r5, r1
	addi	r6, r5, 1
	seq	r30, r6, r1
	jeql	r30, r0, beq_else.21156
	addi	r1, r5 0
	jr	r31
beq_else.21156:
	add	r6, r5, r1
	addi	r7, r0 2
	div	r6, r6, r7
	mul	r2, r2, r6
	sgt	r30, r2, r1
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.21157
	addi	r2, r1 0 #
	addi	r1, r6 0 #
	lwi	r29, r28 0
	jr	r29
ble_else.21157:
	addi	r2, r6 0 #
	addi	r1, r5 0 #
	lwi	r29, r28 0
	jr	r29
my_div_sub.6173.9540:
	lwi	r5, r28 2
	lwi	r6, r28 1
	addi	r7, r1, 1
	seq	r30, r7, r2
	jeql	r30, r0, beq_else.21158
	jr	r31
beq_else.21158:
	add	r8, r1, r2
	addi	r9, r0 2
	div	r8, r8, r9
	mul	r10, r6, r8
	sgt	r30, r10, r5
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.21159
	addi	r1, r8, 1
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21160
	addi	r1, r8 0
	jr	r31
beq_else.21160:
	add	r1, r8, r2
	div	r1, r1, r9
	mul	r6, r6, r1
	sgt	r30, r6, r5
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.21161
	lwi	r29, r28 0
	jr	r29
ble_else.21161:
	addi	r2, r1 0 #
	addi	r1, r8 0 #
	lwi	r29, r28 0
	jr	r29
ble_else.21159:
	seq	r30, r7, r8
	jeql	r30, r0, beq_else.21162
	jr	r31
beq_else.21162:
	add	r2, r1, r8
	div	r2, r2, r9
	mul	r6, r6, r2
	sgt	r30, r6, r5
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.21163
	addi	r1, r2 0 #
	addi	r2, r8 0 #
	lwi	r29, r28 0
	jr	r29
ble_else.21163:
	lwi	r29, r28 0
	jr	r29
my_div_sub.6173.9520:
	lwi	r5, r28 3
	lwi	r6, r28 2
	lwi	r7, r28 1
	add	r8, r1, r6
	seq	r30, r8, r2
	jeql	r30, r0, beq_else.21164
	jr	r31
beq_else.21164:
	add	r9, r1, r2
	addi	r10, r0 2
	div	r9, r9, r10
	mul	r11, r7, r9
	sgt	r30, r11, r5
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.21165
	add	r1, r9, r6
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21166
	addi	r1, r9 0
	jr	r31
beq_else.21166:
	add	r1, r9, r2
	div	r1, r1, r10
	mul	r6, r7, r1
	sgt	r30, r6, r5
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.21167
	lwi	r29, r28 0
	jr	r29
ble_else.21167:
	addi	r2, r1 0 #
	addi	r1, r9 0 #
	lwi	r29, r28 0
	jr	r29
ble_else.21165:
	seq	r30, r8, r9
	jeql	r30, r0, beq_else.21168
	jr	r31
beq_else.21168:
	add	r2, r1, r9
	div	r2, r2, r10
	mul	r6, r7, r2
	sgt	r30, r6, r5
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.21169
	addi	r1, r2 0 #
	addi	r2, r9 0 #
	lwi	r29, r28 0
	jr	r29
ble_else.21169:
	lwi	r29, r28 0
	jr	r29
print_int_rec.2591:
	addi	r2, r0 0
	addi	r30, r0, 0
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21170
	jr	r31
beq_else.21170:
	addi	r5, r0 10
	addi	r28, r4 0
	addi	r4, r4, 3
	closure_address	%r6 my_div_sub.6173.9540
	swi	r6, r28 0
	swi	r1, r28 2
	swi	r5, r28 1
	sub	r2, r2, r1
	addi	r6, r0 1
	addi	r7, r2, 1
	swi	r2, r3 0
	swi	r7, r3 1
	swi	r6, r3 2
	swi	r1, r3 3
	swi	r5, r3 4
	seq	r30, r7, r1
	jeql	r30, r0, beq_else.21172
	addi	r1, r2 0
	jl	beq_cont.21173
beq_else.21172:
	add	r8, r2, r1
	addi	r9, r0 2
	div	r8, r8, r9
	mul	r9, r5, r8
	sgt	r30, r9, r1
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.21174
	addi	r2, r1 0 #
	addi	r1, r8 0 #
	swi	r31 r3 5
	addi	r3, r3, 6
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -6
	lwi	r31, r3 5
	jl	ble_cont.21175
ble_else.21174:
	addi	r1, r2 0 #
	addi	r2, r8 0 #
	swi	r31 r3 5
	addi	r3, r3, 6
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -6
	lwi	r31, r3 5
ble_cont.21175:
beq_cont.21173:
	addi	r30, r0, 0
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21176
	jl	beq_cont.21177
beq_else.21176:
	lwi	r2, r3 4
	swi	r1, r3 5
	swi	r31 r3 6
	addi	r3, r3, 7
	jal	my_div.2588
	addi	r3, r3, -7
	lwi	r31 r3 6
	swi	r31 r3 6
	addi	r3, r3, 7
	jal	print_int_rec.2591
	addi	r3, r3, -7
	lwi	r31 r3 6
	lwi	r1, r3 5
	lwi	r2, r3 4
	swi	r31 r3 6
	addi	r3, r3, 7
	jal	my_div.2588
	addi	r3, r3, -7
	lwi	r31 r3 6
	lwi	r2, r3 4
	mul	r1, r1, r2
	lwi	r5, r3 5
	sub	r1, r5, r1
	swi	r31 r3 6
	addi	r3, r3, 7
	jal	min_caml_print_int_sub
	addi	r3, r3, -7
	lwi	r31 r3 6
beq_cont.21177:
	addi	r28, r4 0
	addi	r4, r4, 4
	closure_address	%r1 my_div_sub.6173.9520
	swi	r1, r28 0
	lwi	r2, r3 3
	swi	r2, r28 3
	lwi	r1, r3 2
	swi	r1, r28 2
	lwi	r1, r3 4
	swi	r1, r28 1
	lwi	r5, r3 1
	seq	r30, r5, r2
	jeql	r30, r0, beq_else.21178
	lwi	r5, r3 0
	addi	r1, r5 0
	jl	beq_cont.21179
beq_else.21178:
	lwi	r5, r3 0
	add	r6, r5, r2
	addi	r7, r0 2
	div	r6, r6, r7
	mul	r7, r1, r6
	sgt	r30, r7, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.21180
	addi	r1, r6 0 #
	swi	r31 r3 6
	addi	r3, r3, 7
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -7
	lwi	r31, r3 6
	jl	ble_cont.21181
ble_else.21180:
	addi	r2, r6 0 #
	addi	r1, r5 0 #
	swi	r31 r3 6
	addi	r3, r3, 7
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -7
	lwi	r31, r3 6
ble_cont.21181:
beq_cont.21179:
	lwi	r2, r3 4
	mul	r1, r1, r2
	lwi	r2, r3 3
	sub	r1, r2, r1
	jl	min_caml_print_int_sub
vecunit_sgn.2654:
	lwi	r5, r28 4
	lwi	r6, r28 3
	lwi	r7, r28 2
	flwi	f1, r28 1 #
	sli	r8, r6, 0
	add	r0 r1 r8
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f2
	sli	r8, r7, 0
	add	r0 r1 r8
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f3, f3, f3
	fadd	f2, f2, f3
	sli	r8, r5, 0
	add	r0 r1 r8
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f3, f3, f3
	fadd	f2, f2, f3
	swi	r5, r3 0
	swi	r7, r3 1
	swi	r1, r3 2
	swi	r2, r3 3
	swi	r6, r3 4
	fswi	f1, r3 6
	fadd	f1, f2 f0
	swi	r31 r3 7
	addi	r3, r3, 8
	jal	min_caml_sqrt
	addi	r3, r3, -8
	lwi	r31 r3 7
	flwi	f2, r3 6 #off
	feq	r30, f1, f2
	jeql	r30, r0, beq_else.21183
	addi	r1, r0 1
	jl	beq_cont.21184
beq_else.21183:
	addi	r1, r0 0
beq_cont.21184:
	lwi	r2, r3 4
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21185
	lwi	r1, r3 3
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21187
	flwl	%f2 l.11794
	fdiv	f1, f2, f1
	jl	beq_cont.21188
beq_else.21187:
	flwl	%f2 l.11828
	fdiv	f1, f2, f1
beq_cont.21188:
	jl	beq_cont.21186
beq_else.21185:
	flwl	%f1 l.11794
beq_cont.21186:
	sli	r1, r2, 0
	lwi	r5, r3 2
	add	r0 r5 r1
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f1
	sli	r1, r2, 0
	add	r0 r5 r1
	fswi	f2, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 1
	sli	r2, r1, 0
	add	r0 r5 r2
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f1
	sli	r1, r1, 0
	add	r0 r5 r1
	fswi	f2, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 0
	sli	r2, r1, 0
	add	r0 r5 r2
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f1, f2, f1
	sli	r1, r1, 0
	add	r0 r5 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	jr	r31
vecaccumv.2678:
	lwi	r6, r28 3
	lwi	r7, r28 2
	lwi	r8, r28 1
	sli	r9, r7, 0
	add	r0 r1 r9
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r9, r7, 0
	add	r0 r2 r9
	flwi	f2, r0 0
	sub	r0 r0 r0
	sli	r9, r7, 0
	add	r0 r5 r9
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	sli	r7, r7, 0
	add	r0 r1 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r7, r8, 0
	add	r0 r1 r7
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r7, r8, 0
	add	r0 r2 r7
	flwi	f2, r0 0
	sub	r0 r0 r0
	sli	r7, r8, 0
	add	r0 r5 r7
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	sli	r7, r8, 0
	add	r0 r1 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r7, r6, 0
	add	r0 r1 r7
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r7, r6, 0
	add	r0 r2 r7
	flwi	f2, r0 0
	sub	r0 r0 r0
	sli	r2, r6, 0
	add	r0 r5 r2
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	sli	r2, r6, 0
	add	r0 r1 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	jr	r31
read_screen_settings.2755:
	lwi	r1, r28 9
	lwi	r2, r28 8
	lwi	r5, r28 7
	lwi	r6, r28 6
	lwi	r7, r28 5
	lwi	r8, r28 4
	lwi	r9, r28 3
	lwi	r10, r28 2
	flwi	f1, r28 1 #
	swi	r1, r3 0
	swi	r5, r3 1
	swi	r6, r3 2
	swi	r2, r3 3
	swi	r8, r3 4
	swi	r7, r3 5
	swi	r9, r3 6
	swi	r10, r3 7
	fswi	f1, r3 8
	swi	r31 r3 9
	addi	r3, r3, 10
	jal	min_caml_read
	addi	r3, r3, -10
	lwi	r31 r3 9
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21191
	swi	r31 r3 9
	addi	r3, r3, 10
	jal	read_float.2586
	addi	r3, r3, -10
	lwi	r31 r3 9
	flwi	f2, r3 8 #off
	fsub	f1, f2, f1
	jl	beq_cont.21192
beq_else.21191:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21193
	addi	r1, r1, -48
	swi	r1, r3 9
	swi	r31 r3 10
	addi	r3, r3, 11
	jal	min_caml_read
	addi	r3, r3, -11
	lwi	r31 r3 10
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21195
	addi	r1, r0 10
	lwi	r2, r3 9
	lwi	r5, r3 7
	swi	r31 r3 10
	addi	r3, r3, 11
	jal	read_float_rec.2582
	addi	r3, r3, -11
	lwi	r31 r3 10
	jl	beq_cont.21196
beq_else.21195:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21197
	addi	r2, r0 10
	lwi	r5, r3 9
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 7
	addi	r5, r1 0 #
	swi	r31 r3 10
	addi	r3, r3, 11
	jal	read_float_rec.2582
	addi	r3, r3, -11
	lwi	r31 r3 10
	jl	bge_cont.21198
bge_else.21197:
	lwi	r1, r3 9
	swi	r31 r3 10
	addi	r3, r3, 11
	jal	min_caml_float_of_int
	addi	r3, r3, -11
	lwi	r31 r3 10
	lwi	r1, r3 7
	fswi	f1, r3 10
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	min_caml_float_of_int
	addi	r3, r3, -12
	lwi	r31 r3 11
	flwi	f2, r3 10 #off
	fdiv	f1, f2, f1
bge_cont.21198:
beq_cont.21196:
	jl	bge_cont.21194
bge_else.21193:
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	read_float.2586
	addi	r3, r3, -12
	lwi	r31 r3 11
bge_cont.21194:
beq_cont.21192:
	lwi	r1, r3 6
	sli	r2, r1, 0
	lwi	r5, r3 5
	add	r0 r5 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	min_caml_read
	addi	r3, r3, -12
	lwi	r31 r3 11
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21199
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	read_float.2586
	addi	r3, r3, -12
	lwi	r31 r3 11
	flwi	f2, r3 8 #off
	fsub	f1, f2, f1
	jl	beq_cont.21200
beq_else.21199:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21201
	addi	r1, r1, -48
	swi	r1, r3 11
	swi	r31 r3 12
	addi	r3, r3, 13
	jal	min_caml_read
	addi	r3, r3, -13
	lwi	r31 r3 12
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21203
	addi	r1, r0 10
	lwi	r2, r3 11
	lwi	r5, r3 7
	swi	r31 r3 12
	addi	r3, r3, 13
	jal	read_float_rec.2582
	addi	r3, r3, -13
	lwi	r31 r3 12
	jl	beq_cont.21204
beq_else.21203:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21205
	addi	r2, r0 10
	lwi	r5, r3 11
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 7
	addi	r5, r1 0 #
	swi	r31 r3 12
	addi	r3, r3, 13
	jal	read_float_rec.2582
	addi	r3, r3, -13
	lwi	r31 r3 12
	jl	bge_cont.21206
bge_else.21205:
	lwi	r1, r3 11
	swi	r31 r3 12
	addi	r3, r3, 13
	jal	min_caml_float_of_int
	addi	r3, r3, -13
	lwi	r31 r3 12
	lwi	r1, r3 7
	fswi	f1, r3 12
	swi	r31 r3 13
	addi	r3, r3, 14
	jal	min_caml_float_of_int
	addi	r3, r3, -14
	lwi	r31 r3 13
	flwi	f2, r3 12 #off
	fdiv	f1, f2, f1
bge_cont.21206:
beq_cont.21204:
	jl	bge_cont.21202
bge_else.21201:
	swi	r31 r3 13
	addi	r3, r3, 14
	jal	read_float.2586
	addi	r3, r3, -14
	lwi	r31 r3 13
bge_cont.21202:
beq_cont.21200:
	lwi	r1, r3 7
	sli	r2, r1, 0
	lwi	r5, r3 5
	add	r0 r5 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	swi	r31 r3 13
	addi	r3, r3, 14
	jal	min_caml_read
	addi	r3, r3, -14
	lwi	r31 r3 13
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21207
	swi	r31 r3 13
	addi	r3, r3, 14
	jal	read_float.2586
	addi	r3, r3, -14
	lwi	r31 r3 13
	flwi	f2, r3 8 #off
	fsub	f1, f2, f1
	jl	beq_cont.21208
beq_else.21207:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21209
	addi	r1, r1, -48
	swi	r1, r3 13
	swi	r31 r3 14
	addi	r3, r3, 15
	jal	min_caml_read
	addi	r3, r3, -15
	lwi	r31 r3 14
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21211
	addi	r1, r0 10
	lwi	r2, r3 13
	lwi	r5, r3 7
	swi	r31 r3 14
	addi	r3, r3, 15
	jal	read_float_rec.2582
	addi	r3, r3, -15
	lwi	r31 r3 14
	jl	beq_cont.21212
beq_else.21211:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21213
	addi	r2, r0 10
	lwi	r5, r3 13
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 7
	addi	r5, r1 0 #
	swi	r31 r3 14
	addi	r3, r3, 15
	jal	read_float_rec.2582
	addi	r3, r3, -15
	lwi	r31 r3 14
	jl	bge_cont.21214
bge_else.21213:
	lwi	r1, r3 13
	swi	r31 r3 14
	addi	r3, r3, 15
	jal	min_caml_float_of_int
	addi	r3, r3, -15
	lwi	r31 r3 14
	lwi	r1, r3 7
	fswi	f1, r3 14
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	min_caml_float_of_int
	addi	r3, r3, -16
	lwi	r31 r3 15
	flwi	f2, r3 14 #off
	fdiv	f1, f2, f1
bge_cont.21214:
beq_cont.21212:
	jl	bge_cont.21210
bge_else.21209:
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	read_float.2586
	addi	r3, r3, -16
	lwi	r31 r3 15
bge_cont.21210:
beq_cont.21208:
	lwi	r1, r3 4
	sli	r2, r1, 0
	lwi	r5, r3 5
	add	r0 r5 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	min_caml_read
	addi	r3, r3, -16
	lwi	r31 r3 15
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21215
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	read_float.2586
	addi	r3, r3, -16
	lwi	r31 r3 15
	flwi	f2, r3 8 #off
	fsub	f1, f2, f1
	jl	beq_cont.21216
beq_else.21215:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21217
	addi	r1, r1, -48
	swi	r1, r3 15
	swi	r31 r3 16
	addi	r3, r3, 17
	jal	min_caml_read
	addi	r3, r3, -17
	lwi	r31 r3 16
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21219
	addi	r1, r0 10
	lwi	r2, r3 15
	lwi	r5, r3 7
	swi	r31 r3 16
	addi	r3, r3, 17
	jal	read_float_rec.2582
	addi	r3, r3, -17
	lwi	r31 r3 16
	jl	beq_cont.21220
beq_else.21219:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21221
	addi	r2, r0 10
	lwi	r5, r3 15
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 7
	addi	r5, r1 0 #
	swi	r31 r3 16
	addi	r3, r3, 17
	jal	read_float_rec.2582
	addi	r3, r3, -17
	lwi	r31 r3 16
	jl	bge_cont.21222
bge_else.21221:
	lwi	r1, r3 15
	swi	r31 r3 16
	addi	r3, r3, 17
	jal	min_caml_float_of_int
	addi	r3, r3, -17
	lwi	r31 r3 16
	lwi	r1, r3 7
	fswi	f1, r3 16
	swi	r31 r3 17
	addi	r3, r3, 18
	jal	min_caml_float_of_int
	addi	r3, r3, -18
	lwi	r31 r3 17
	flwi	f2, r3 16 #off
	fdiv	f1, f2, f1
bge_cont.21222:
beq_cont.21220:
	jl	bge_cont.21218
bge_else.21217:
	swi	r31 r3 17
	addi	r3, r3, 18
	jal	read_float.2586
	addi	r3, r3, -18
	lwi	r31 r3 17
bge_cont.21218:
beq_cont.21216:
	flwl	%f2 l.11850
	fmul	f1, f1, f2
	flwl	%f3 l.11781
	fsub	f4, f3, f1
	fswi	f3, r3 18
	fswi	f2, r3 20
	fswi	f1, r3 22
	fadd	f1, f4 f0
	swi	r31 r3 23
	addi	r3, r3, 24
	jal	sin.2572
	addi	r3, r3, -24
	lwi	r31 r3 23
	flwi	f2, r3 22 #off
	fswi	f1, r3 24
	fadd	f1, f2 f0
	swi	r31 r3 25
	addi	r3, r3, 26
	jal	sin.2572
	addi	r3, r3, -26
	lwi	r31 r3 25
	fswi	f1, r3 26
	swi	r31 r3 27
	addi	r3, r3, 28
	jal	min_caml_read
	addi	r3, r3, -28
	lwi	r31 r3 27
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21228
	swi	r31 r3 27
	addi	r3, r3, 28
	jal	read_float.2586
	addi	r3, r3, -28
	lwi	r31 r3 27
	flwi	f2, r3 8 #off
	fsub	f1, f2, f1
	jl	beq_cont.21229
beq_else.21228:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21230
	addi	r1, r1, -48
	swi	r1, r3 27
	swi	r31 r3 28
	addi	r3, r3, 29
	jal	min_caml_read
	addi	r3, r3, -29
	lwi	r31 r3 28
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21232
	addi	r1, r0 10
	lwi	r2, r3 27
	lwi	r5, r3 7
	swi	r31 r3 28
	addi	r3, r3, 29
	jal	read_float_rec.2582
	addi	r3, r3, -29
	lwi	r31 r3 28
	jl	beq_cont.21233
beq_else.21232:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21234
	addi	r2, r0 10
	lwi	r5, r3 27
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 7
	addi	r5, r1 0 #
	swi	r31 r3 28
	addi	r3, r3, 29
	jal	read_float_rec.2582
	addi	r3, r3, -29
	lwi	r31 r3 28
	jl	bge_cont.21235
bge_else.21234:
	lwi	r1, r3 27
	swi	r31 r3 28
	addi	r3, r3, 29
	jal	min_caml_float_of_int
	addi	r3, r3, -29
	lwi	r31 r3 28
	lwi	r1, r3 7
	fswi	f1, r3 28
	swi	r31 r3 29
	addi	r3, r3, 30
	jal	min_caml_float_of_int
	addi	r3, r3, -30
	lwi	r31 r3 29
	flwi	f2, r3 28 #off
	fdiv	f1, f2, f1
bge_cont.21235:
beq_cont.21233:
	jl	bge_cont.21231
bge_else.21230:
	swi	r31 r3 29
	addi	r3, r3, 30
	jal	read_float.2586
	addi	r3, r3, -30
	lwi	r31 r3 29
bge_cont.21231:
beq_cont.21229:
	flwi	f2, r3 20 #off
	fmul	f1, f1, f2
	flwi	f2, r3 18 #off
	fsub	f2, f2, f1
	fswi	f1, r3 30
	fadd	f1, f2 f0
	swi	r31 r3 31
	addi	r3, r3, 32
	jal	sin.2572
	addi	r3, r3, -32
	lwi	r31 r3 31
	flwi	f2, r3 30 #off
	fswi	f1, r3 32
	fadd	f1, f2 f0
	swi	r31 r3 33
	addi	r3, r3, 34
	jal	sin.2572
	addi	r3, r3, -34
	lwi	r31 r3 33
	flwi	f2, r3 24 #off
	fmul	f3, f2, f1
	flwl	%f4 l.11851
	fmul	f3, f3, f4
	lwi	r1, r3 6
	sli	r2, r1, 0
	lwi	r5, r3 3
	add	r0 r5 r2
	fswi	f3, r0 0
	sub	r0 r0 r0
	flwl	%f3 l.11853
	flwi	f5, r3 26 #off
	fmul	f3, f5, f3
	lwi	r2, r3 7
	sli	r6, r2, 0
	add	r0 r5 r6
	fswi	f3, r0 0
	sub	r0 r0 r0
	flwi	f3, r3 32 #off
	fmul	f6, f2, f3
	fmul	f4, f6, f4
	lwi	r6, r3 4
	sli	r7, r6, 0
	add	r0 r5 r7
	fswi	f4, r0 0
	sub	r0 r0 r0
	sli	r7, r1, 0
	lwi	r8, r3 2
	add	r0 r8 r7
	fswi	f3, r0 0
	sub	r0 r0 r0
	sli	r7, r2, 0
	flwi	f4, r3 8 #off
	add	r0 r8 r7
	fswi	f4, r0 0
	sub	r0 r0 r0
	fsub	f6, f4, f1
	sli	r7, r6, 0
	add	r0 r8 r7
	fswi	f6, r0 0
	sub	r0 r0 r0
	fsub	f5, f4, f5
	fmul	f1, f5, f1
	sli	r7, r1, 0
	lwi	r8, r3 1
	add	r0 r8 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	fsub	f1, f4, f2
	sli	r7, r2, 0
	add	r0 r8 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	fmul	f1, f5, f3
	sli	r7, r6, 0
	add	r0 r8 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r7, r1, 0
	lwi	r8, r3 5
	add	r0 r8 r7
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r7, r1, 0
	add	r0 r5 r7
	flwi	f2, r0 0
	sub	r0 r0 r0
	fsub	f1, f1, f2
	sli	r1, r1, 0
	lwi	r7, r3 0
	add	r0 r7 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r1, r2, 0
	add	r0 r8 r1
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r1, r2, 0
	add	r0 r5 r1
	flwi	f2, r0 0
	sub	r0 r0 r0
	fsub	f1, f1, f2
	sli	r1, r2, 0
	add	r0 r7 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r1, r6, 0
	add	r0 r8 r1
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r1, r6, 0
	add	r0 r5 r1
	flwi	f2, r0 0
	sub	r0 r0 r0
	fsub	f1, f1, f2
	sli	r1, r6, 0
	add	r0 r7 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	jr	r31
read_light.2757:
	lwi	r1, r28 6
	lwi	r2, r28 5
	lwi	r5, r28 4
	lwi	r6, r28 3
	lwi	r7, r28 2
	flwi	f1, r28 1 #
	swi	r2, r3 0
	swi	r5, r3 1
	swi	r1, r3 2
	swi	r7, r3 3
	fswi	f1, r3 4
	swi	r6, r3 5
	swi	r31 r3 6
	addi	r3, r3, 7
	jal	min_caml_read
	addi	r3, r3, -7
	lwi	r31 r3 6
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21239
	swi	r31 r3 6
	addi	r3, r3, 7
	jal	read_int.2580
	addi	r3, r3, -7
	lwi	r31 r3 6
	lwi	r2, r3 5
	sub	r1, r2, r1
	jl	beq_cont.21240
beq_else.21239:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21241
	addi	r1, r1, -48
	swi	r1, r3 6
	swi	r31 r3 7
	addi	r3, r3, 8
	jal	min_caml_read
	addi	r3, r3, -8
	lwi	r31 r3 7
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21243
	addi	r2, r0 10
	lwi	r5, r3 6
	mul	r5, r5, r2
	addi	r1, r1, -48
	add	r1, r5, r1
	swi	r2, r3 7
	swi	r1, r3 8
	swi	r31 r3 9
	addi	r3, r3, 10
	jal	min_caml_read
	addi	r3, r3, -10
	lwi	r31 r3 9
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21245
	lwi	r2, r3 7
	lwi	r5, r3 8
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r1, r2, r1
	swi	r31 r3 9
	addi	r3, r3, 10
	jal	read_int_rec.2578
	addi	r3, r3, -10
	lwi	r31 r3 9
	jl	bge_cont.21246
bge_else.21245:
	lwi	r1, r3 8
bge_cont.21246:
	jl	bge_cont.21244
bge_else.21243:
	lwi	r1, r3 6
bge_cont.21244:
	jl	bge_cont.21242
bge_else.21241:
	swi	r31 r3 9
	addi	r3, r3, 10
	jal	read_int.2580
	addi	r3, r3, -10
	lwi	r31 r3 9
bge_cont.21242:
beq_cont.21240:
	swi	r31 r3 9
	addi	r3, r3, 10
	jal	min_caml_read
	addi	r3, r3, -10
	lwi	r31 r3 9
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21247
	swi	r31 r3 9
	addi	r3, r3, 10
	jal	read_float.2586
	addi	r3, r3, -10
	lwi	r31 r3 9
	flwi	f2, r3 4 #off
	fsub	f1, f2, f1
	jl	beq_cont.21248
beq_else.21247:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21249
	addi	r1, r1, -48
	swi	r1, r3 9
	swi	r31 r3 10
	addi	r3, r3, 11
	jal	min_caml_read
	addi	r3, r3, -11
	lwi	r31 r3 10
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21251
	addi	r1, r0 10
	lwi	r2, r3 9
	lwi	r5, r3 3
	swi	r31 r3 10
	addi	r3, r3, 11
	jal	read_float_rec.2582
	addi	r3, r3, -11
	lwi	r31 r3 10
	jl	beq_cont.21252
beq_else.21251:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21253
	addi	r2, r0 10
	lwi	r5, r3 9
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 3
	addi	r5, r1 0 #
	swi	r31 r3 10
	addi	r3, r3, 11
	jal	read_float_rec.2582
	addi	r3, r3, -11
	lwi	r31 r3 10
	jl	bge_cont.21254
bge_else.21253:
	lwi	r1, r3 9
	swi	r31 r3 10
	addi	r3, r3, 11
	jal	min_caml_float_of_int
	addi	r3, r3, -11
	lwi	r31 r3 10
	lwi	r1, r3 3
	fswi	f1, r3 10
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	min_caml_float_of_int
	addi	r3, r3, -12
	lwi	r31 r3 11
	flwi	f2, r3 10 #off
	fdiv	f1, f2, f1
bge_cont.21254:
beq_cont.21252:
	jl	bge_cont.21250
bge_else.21249:
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	read_float.2586
	addi	r3, r3, -12
	lwi	r31 r3 11
bge_cont.21250:
beq_cont.21248:
	flwl	%f2 l.11850
	fmul	f1, f1, f2
	fswi	f1, r3 12
	fswi	f2, r3 14
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	sin.2572
	addi	r3, r3, -16
	lwi	r31 r3 15
	flwi	f2, r3 4 #off
	fsub	f1, f2, f1
	lwi	r1, r3 3
	sli	r2, r1, 0
	lwi	r5, r3 2
	add	r0 r5 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	min_caml_read
	addi	r3, r3, -16
	lwi	r31 r3 15
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21257
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	read_float.2586
	addi	r3, r3, -16
	lwi	r31 r3 15
	flwi	f2, r3 4 #off
	fsub	f1, f2, f1
	jl	beq_cont.21258
beq_else.21257:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21259
	addi	r1, r1, -48
	swi	r1, r3 15
	swi	r31 r3 16
	addi	r3, r3, 17
	jal	min_caml_read
	addi	r3, r3, -17
	lwi	r31 r3 16
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21261
	addi	r1, r0 10
	lwi	r2, r3 15
	lwi	r5, r3 3
	swi	r31 r3 16
	addi	r3, r3, 17
	jal	read_float_rec.2582
	addi	r3, r3, -17
	lwi	r31 r3 16
	jl	beq_cont.21262
beq_else.21261:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21263
	addi	r2, r0 10
	lwi	r5, r3 15
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 3
	addi	r5, r1 0 #
	swi	r31 r3 16
	addi	r3, r3, 17
	jal	read_float_rec.2582
	addi	r3, r3, -17
	lwi	r31 r3 16
	jl	bge_cont.21264
bge_else.21263:
	lwi	r1, r3 15
	swi	r31 r3 16
	addi	r3, r3, 17
	jal	min_caml_float_of_int
	addi	r3, r3, -17
	lwi	r31 r3 16
	lwi	r1, r3 3
	fswi	f1, r3 16
	swi	r31 r3 17
	addi	r3, r3, 18
	jal	min_caml_float_of_int
	addi	r3, r3, -18
	lwi	r31 r3 17
	flwi	f2, r3 16 #off
	fdiv	f1, f2, f1
bge_cont.21264:
beq_cont.21262:
	jl	bge_cont.21260
bge_else.21259:
	swi	r31 r3 17
	addi	r3, r3, 18
	jal	read_float.2586
	addi	r3, r3, -18
	lwi	r31 r3 17
bge_cont.21260:
beq_cont.21258:
	flwi	f2, r3 14 #off
	fmul	f1, f1, f2
	flwl	%f2 l.11781
	flwi	f3, r3 12 #off
	fsub	f3, f2, f3
	fswi	f2, r3 18
	fswi	f1, r3 20
	fadd	f1, f3 f0
	swi	r31 r3 21
	addi	r3, r3, 22
	jal	sin.2572
	addi	r3, r3, -22
	lwi	r31 r3 21
	flwi	f2, r3 20 #off
	fswi	f1, r3 22
	fadd	f1, f2 f0
	swi	r31 r3 23
	addi	r3, r3, 24
	jal	sin.2572
	addi	r3, r3, -24
	lwi	r31 r3 23
	flwi	f2, r3 22 #off
	fmul	f1, f2, f1
	lwi	r1, r3 5
	sli	r2, r1, 0
	lwi	r5, r3 2
	add	r0 r5 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 20 #off
	flwi	f3, r3 18 #off
	fsub	f1, f3, f1
	swi	r31 r3 23
	addi	r3, r3, 24
	jal	sin.2572
	addi	r3, r3, -24
	lwi	r31 r3 23
	flwi	f2, r3 22 #off
	fmul	f1, f2, f1
	lwi	r1, r3 1
	sli	r1, r1, 0
	lwi	r2, r3 2
	add	r0 r2 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	swi	r31 r3 23
	addi	r3, r3, 24
	jal	min_caml_read
	addi	r3, r3, -24
	lwi	r31 r3 23
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21268
	swi	r31 r3 23
	addi	r3, r3, 24
	jal	read_float.2586
	addi	r3, r3, -24
	lwi	r31 r3 23
	flwi	f2, r3 4 #off
	fsub	f1, f2, f1
	jl	beq_cont.21269
beq_else.21268:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21270
	addi	r1, r1, -48
	swi	r1, r3 23
	swi	r31 r3 24
	addi	r3, r3, 25
	jal	min_caml_read
	addi	r3, r3, -25
	lwi	r31 r3 24
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21272
	addi	r1, r0 10
	lwi	r2, r3 23
	lwi	r5, r3 3
	swi	r31 r3 24
	addi	r3, r3, 25
	jal	read_float_rec.2582
	addi	r3, r3, -25
	lwi	r31 r3 24
	jl	beq_cont.21273
beq_else.21272:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21274
	addi	r2, r0 10
	lwi	r5, r3 23
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 3
	addi	r5, r1 0 #
	swi	r31 r3 24
	addi	r3, r3, 25
	jal	read_float_rec.2582
	addi	r3, r3, -25
	lwi	r31 r3 24
	jl	bge_cont.21275
bge_else.21274:
	lwi	r1, r3 23
	swi	r31 r3 24
	addi	r3, r3, 25
	jal	min_caml_float_of_int
	addi	r3, r3, -25
	lwi	r31 r3 24
	lwi	r1, r3 3
	fswi	f1, r3 24
	swi	r31 r3 25
	addi	r3, r3, 26
	jal	min_caml_float_of_int
	addi	r3, r3, -26
	lwi	r31 r3 25
	flwi	f2, r3 24 #off
	fdiv	f1, f2, f1
bge_cont.21275:
beq_cont.21273:
	jl	bge_cont.21271
bge_else.21270:
	swi	r31 r3 25
	addi	r3, r3, 26
	jal	read_float.2586
	addi	r3, r3, -26
	lwi	r31 r3 25
bge_cont.21271:
beq_cont.21269:
	lwi	r1, r3 5
	sli	r1, r1, 0
	lwi	r2, r3 0
	add	r0 r2 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	jr	r31
rotate_quadratic_matrix.2759:
	lwi	r5, r28 4
	lwi	r6, r28 3
	lwi	r7, r28 2
	flwi	f1, r28 1 #
	sli	r8, r6, 0
	add	r0 r2 r8
	flwi	f2, r0 0
	sub	r0 r0 r0
	flwl	%f3 l.11781
	fsub	f2, f3, f2
	swi	r1, r3 0
	fswi	f1, r3 2
	swi	r5, r3 3
	fswi	f3, r3 4
	swi	r7, r3 5
	swi	r2, r3 6
	swi	r6, r3 7
	fadd	f1, f2 f0
	swi	r31 r3 8
	addi	r3, r3, 9
	jal	sin.2572
	addi	r3, r3, -9
	lwi	r31 r3 8
	lwi	r1, r3 7
	sli	r2, r1, 0
	lwi	r5, r3 6
	add	r0 r5 r2
	flwi	f2, r0 0
	sub	r0 r0 r0
	fswi	f1, r3 8
	fadd	f1, f2 f0
	swi	r31 r3 9
	addi	r3, r3, 10
	jal	sin.2572
	addi	r3, r3, -10
	lwi	r31 r3 9
	lwi	r1, r3 5
	sli	r2, r1, 0
	lwi	r5, r3 6
	add	r0 r5 r2
	flwi	f2, r0 0
	sub	r0 r0 r0
	flwi	f3, r3 4 #off
	fsub	f2, f3, f2
	fswi	f1, r3 10
	fadd	f1, f2 f0
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	sin.2572
	addi	r3, r3, -12
	lwi	r31 r3 11
	lwi	r1, r3 5
	sli	r2, r1, 0
	lwi	r5, r3 6
	add	r0 r5 r2
	flwi	f2, r0 0
	sub	r0 r0 r0
	fswi	f1, r3 12
	fadd	f1, f2 f0
	swi	r31 r3 13
	addi	r3, r3, 14
	jal	sin.2572
	addi	r3, r3, -14
	lwi	r31 r3 13
	lwi	r1, r3 3
	sli	r2, r1, 0
	lwi	r5, r3 6
	add	r0 r5 r2
	flwi	f2, r0 0
	sub	r0 r0 r0
	flwi	f3, r3 4 #off
	fsub	f2, f3, f2
	fswi	f1, r3 14
	fadd	f1, f2 f0
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	sin.2572
	addi	r3, r3, -16
	lwi	r31 r3 15
	lwi	r1, r3 3
	sli	r2, r1, 0
	lwi	r5, r3 6
	add	r0 r5 r2
	flwi	f2, r0 0
	sub	r0 r0 r0
	fswi	f1, r3 16
	fadd	f1, f2 f0
	swi	r31 r3 17
	addi	r3, r3, 18
	jal	sin.2572
	addi	r3, r3, -18
	lwi	r31 r3 17
	flwi	f2, r3 16 #off
	flwi	f3, r3 12 #off
	fmul	f4, f3, f2
	flwi	f5, r3 14 #off
	flwi	f6, r3 10 #off
	fmul	f7, f6, f5
	fmul	f8, f7, f2
	flwi	f9, r3 8 #off
	fmul	f10, f9, f1
	fsub	f8, f8, f10
	fmul	f10, f9, f5
	fmul	f11, f10, f2
	fmul	f12, f6, f1
	fadd	f11, f11, f12
	fmul	f12, f3, f1
	fmul	f7, f7, f1
	fmul	f13, f9, f2
	fadd	f7, f7, f13
	fmul	f1, f10, f1
	fmul	f2, f6, f2
	fsub	f1, f1, f2
	flwi	f2, r3 2 #off
	fsub	f2, f2, f5
	fmul	f5, f6, f3
	fmul	f3, f9, f3
	lwi	r1, r3 7
	sli	r2, r1, 0
	lwi	r5, r3 0
	add	r0 r5 r2
	flwi	f6, r0 0
	sub	r0 r0 r0
	lwi	r2, r3 5
	sli	r6, r2, 0
	add	r0 r5 r6
	flwi	f9, r0 0
	sub	r0 r0 r0
	lwi	r6, r3 3
	sli	r7, r6, 0
	add	r0 r5 r7
	flwi	f10, r0 0
	sub	r0 r0 r0
	fmul	f13, f4, f4
	fmul	f13, f6, f13
	fmul	f14, f12, f12
	fmul	f14, f9, f14
	fadd	f13, f13, f14
	fmul	f14, f2, f2
	fmul	f14, f10, f14
	fadd	f13, f13, f14
	sli	r7, r1, 0
	add	r0 r5 r7
	fswi	f13, r0 0
	sub	r0 r0 r0
	fmul	f13, f8, f8
	fmul	f13, f6, f13
	fmul	f14, f7, f7
	fmul	f14, f9, f14
	fadd	f13, f13, f14
	fmul	f14, f5, f5
	fmul	f14, f10, f14
	fadd	f13, f13, f14
	sli	r7, r2, 0
	add	r0 r5 r7
	fswi	f13, r0 0
	sub	r0 r0 r0
	fmul	f13, f11, f11
	fmul	f13, f6, f13
	fmul	f14, f1, f1
	fmul	f14, f9, f14
	fadd	f13, f13, f14
	fmul	f14, f3, f3
	fmul	f14, f10, f14
	fadd	f13, f13, f14
	sli	r7, r6, 0
	add	r0 r5 r7
	fswi	f13, r0 0
	sub	r0 r0 r0
	flwl	%f13 l.11778
	fmul	f14, f6, f8
	fmul	f14, f14, f11
	fmul	f15, f9, f7
	fmul	f15, f15, f1
	fadd	f14, f14, f15
	fmul	f15, f10, f5
	fmul	f15, f15, f3
	fadd	f14, f14, f15
	fmul	f14, f13, f14
	sli	r1, r1, 0
	lwi	r5, r3 6
	add	r0 r5 r1
	fswi	f14, r0 0
	sub	r0 r0 r0
	fmul	f4, f6, f4
	fmul	f6, f4, f11
	fmul	f9, f9, f12
	fmul	f1, f9, f1
	fadd	f1, f6, f1
	fmul	f2, f10, f2
	fmul	f3, f2, f3
	fadd	f1, f1, f3
	fmul	f1, f13, f1
	sli	r1, r2, 0
	add	r0 r5 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	fmul	f1, f4, f8
	fmul	f3, f9, f7
	fadd	f1, f1, f3
	fmul	f2, f2, f5
	fadd	f1, f1, f2
	fmul	f1, f13, f1
	sli	r1, r6, 0
	add	r0 r5 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	jr	r31
read_nth_object.2762:
	lwi	r2, r28 9
	lwi	r5, r28 8
	lwi	r6, r28 7
	lwi	r7, r28 6
	lwi	r8, r28 5
	lwi	r9, r28 4
	lwi	r10, r28 3
	lwi	r11, r28 2
	flwi	f1, r28 1 #
	swi	r5, r3 0
	swi	r2, r3 1
	swi	r6, r3 2
	swi	r1, r3 3
	swi	r7, r3 4
	swi	r11, r3 5
	fswi	f1, r3 6
	swi	r9, r3 7
	swi	r8, r3 8
	swi	r10, r3 9
	swi	r31 r3 10
	addi	r3, r3, 11
	jal	min_caml_read
	addi	r3, r3, -11
	lwi	r31 r3 10
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21283
	swi	r31 r3 10
	addi	r3, r3, 11
	jal	read_int.2580
	addi	r3, r3, -11
	lwi	r31 r3 10
	lwi	r2, r3 9
	sub	r1, r2, r1
	jl	beq_cont.21284
beq_else.21283:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21285
	addi	r1, r1, -48
	swi	r1, r3 10
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	min_caml_read
	addi	r3, r3, -12
	lwi	r31 r3 11
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21287
	addi	r2, r0 10
	lwi	r5, r3 10
	mul	r5, r5, r2
	addi	r1, r1, -48
	add	r1, r5, r1
	swi	r2, r3 11
	swi	r1, r3 12
	swi	r31 r3 13
	addi	r3, r3, 14
	jal	min_caml_read
	addi	r3, r3, -14
	lwi	r31 r3 13
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21289
	lwi	r2, r3 11
	lwi	r5, r3 12
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r1, r2, r1
	swi	r31 r3 13
	addi	r3, r3, 14
	jal	read_int_rec.2578
	addi	r3, r3, -14
	lwi	r31 r3 13
	jl	bge_cont.21290
bge_else.21289:
	lwi	r1, r3 12
bge_cont.21290:
	jl	bge_cont.21288
bge_else.21287:
	lwi	r1, r3 10
bge_cont.21288:
	jl	bge_cont.21286
bge_else.21285:
	swi	r31 r3 13
	addi	r3, r3, 14
	jal	read_int.2580
	addi	r3, r3, -14
	lwi	r31 r3 13
bge_cont.21286:
beq_cont.21284:
	lwi	r2, r3 8
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21291
	addi	r1, r0 0
	jr	r31
beq_else.21291:
	swi	r1, r3 13
	swi	r31 r3 14
	addi	r3, r3, 15
	jal	min_caml_read
	addi	r3, r3, -15
	lwi	r31 r3 14
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21292
	swi	r31 r3 14
	addi	r3, r3, 15
	jal	read_int.2580
	addi	r3, r3, -15
	lwi	r31 r3 14
	lwi	r2, r3 9
	sub	r1, r2, r1
	jl	beq_cont.21293
beq_else.21292:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21294
	addi	r1, r1, -48
	swi	r1, r3 14
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	min_caml_read
	addi	r3, r3, -16
	lwi	r31 r3 15
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21296
	addi	r2, r0 10
	lwi	r5, r3 14
	mul	r5, r5, r2
	addi	r1, r1, -48
	add	r1, r5, r1
	swi	r2, r3 15
	swi	r1, r3 16
	swi	r31 r3 17
	addi	r3, r3, 18
	jal	min_caml_read
	addi	r3, r3, -18
	lwi	r31 r3 17
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21298
	lwi	r2, r3 15
	lwi	r5, r3 16
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r1, r2, r1
	swi	r31 r3 17
	addi	r3, r3, 18
	jal	read_int_rec.2578
	addi	r3, r3, -18
	lwi	r31 r3 17
	jl	bge_cont.21299
bge_else.21298:
	lwi	r1, r3 16
bge_cont.21299:
	jl	bge_cont.21297
bge_else.21296:
	lwi	r1, r3 14
bge_cont.21297:
	jl	bge_cont.21295
bge_else.21294:
	swi	r31 r3 17
	addi	r3, r3, 18
	jal	read_int.2580
	addi	r3, r3, -18
	lwi	r31 r3 17
bge_cont.21295:
beq_cont.21293:
	swi	r1, r3 17
	swi	r31 r3 18
	addi	r3, r3, 19
	jal	min_caml_read
	addi	r3, r3, -19
	lwi	r31 r3 18
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21300
	swi	r31 r3 18
	addi	r3, r3, 19
	jal	read_int.2580
	addi	r3, r3, -19
	lwi	r31 r3 18
	lwi	r2, r3 9
	sub	r1, r2, r1
	jl	beq_cont.21301
beq_else.21300:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21302
	addi	r1, r1, -48
	swi	r1, r3 18
	swi	r31 r3 19
	addi	r3, r3, 20
	jal	min_caml_read
	addi	r3, r3, -20
	lwi	r31 r3 19
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21304
	addi	r2, r0 10
	lwi	r5, r3 18
	mul	r5, r5, r2
	addi	r1, r1, -48
	add	r1, r5, r1
	swi	r2, r3 19
	swi	r1, r3 20
	swi	r31 r3 21
	addi	r3, r3, 22
	jal	min_caml_read
	addi	r3, r3, -22
	lwi	r31 r3 21
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21306
	lwi	r2, r3 19
	lwi	r5, r3 20
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r1, r2, r1
	swi	r31 r3 21
	addi	r3, r3, 22
	jal	read_int_rec.2578
	addi	r3, r3, -22
	lwi	r31 r3 21
	jl	bge_cont.21307
bge_else.21306:
	lwi	r1, r3 20
bge_cont.21307:
	jl	bge_cont.21305
bge_else.21304:
	lwi	r1, r3 18
bge_cont.21305:
	jl	bge_cont.21303
bge_else.21302:
	swi	r31 r3 21
	addi	r3, r3, 22
	jal	read_int.2580
	addi	r3, r3, -22
	lwi	r31 r3 21
bge_cont.21303:
beq_cont.21301:
	swi	r1, r3 21
	swi	r31 r3 22
	addi	r3, r3, 23
	jal	min_caml_read
	addi	r3, r3, -23
	lwi	r31 r3 22
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21308
	swi	r31 r3 22
	addi	r3, r3, 23
	jal	read_int.2580
	addi	r3, r3, -23
	lwi	r31 r3 22
	lwi	r2, r3 9
	sub	r1, r2, r1
	jl	beq_cont.21309
beq_else.21308:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21310
	addi	r1, r1, -48
	swi	r1, r3 22
	swi	r31 r3 23
	addi	r3, r3, 24
	jal	min_caml_read
	addi	r3, r3, -24
	lwi	r31 r3 23
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21312
	addi	r2, r0 10
	lwi	r5, r3 22
	mul	r5, r5, r2
	addi	r1, r1, -48
	add	r1, r5, r1
	swi	r2, r3 23
	swi	r1, r3 24
	swi	r31 r3 25
	addi	r3, r3, 26
	jal	min_caml_read
	addi	r3, r3, -26
	lwi	r31 r3 25
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21314
	lwi	r2, r3 23
	lwi	r5, r3 24
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r1, r2, r1
	swi	r31 r3 25
	addi	r3, r3, 26
	jal	read_int_rec.2578
	addi	r3, r3, -26
	lwi	r31 r3 25
	jl	bge_cont.21315
bge_else.21314:
	lwi	r1, r3 24
bge_cont.21315:
	jl	bge_cont.21313
bge_else.21312:
	lwi	r1, r3 22
bge_cont.21313:
	jl	bge_cont.21311
bge_else.21310:
	swi	r31 r3 25
	addi	r3, r3, 26
	jal	read_int.2580
	addi	r3, r3, -26
	lwi	r31 r3 25
bge_cont.21311:
beq_cont.21309:
	flwi	f1, r3 6 #off
	lwi	r2, r3 7
	swi	r1, r3 25
	addi	r1, r2 0 #
	swi	r31 r3 26
	addi	r3, r3, 27
	jal	min_caml_create_float_array
	addi	r3, r3, -27
	lwi	r31 r3 26
	swi	r1, r3 26
	swi	r31 r3 27
	addi	r3, r3, 28
	jal	min_caml_read
	addi	r3, r3, -28
	lwi	r31 r3 27
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21316
	swi	r31 r3 27
	addi	r3, r3, 28
	jal	read_float.2586
	addi	r3, r3, -28
	lwi	r31 r3 27
	flwi	f2, r3 6 #off
	fsub	f1, f2, f1
	jl	beq_cont.21317
beq_else.21316:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21318
	addi	r1, r1, -48
	swi	r1, r3 27
	swi	r31 r3 28
	addi	r3, r3, 29
	jal	min_caml_read
	addi	r3, r3, -29
	lwi	r31 r3 28
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21320
	addi	r1, r0 10
	lwi	r2, r3 27
	lwi	r5, r3 5
	swi	r31 r3 28
	addi	r3, r3, 29
	jal	read_float_rec.2582
	addi	r3, r3, -29
	lwi	r31 r3 28
	jl	beq_cont.21321
beq_else.21320:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21322
	addi	r2, r0 10
	lwi	r5, r3 27
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 5
	addi	r5, r1 0 #
	swi	r31 r3 28
	addi	r3, r3, 29
	jal	read_float_rec.2582
	addi	r3, r3, -29
	lwi	r31 r3 28
	jl	bge_cont.21323
bge_else.21322:
	lwi	r1, r3 27
	swi	r31 r3 28
	addi	r3, r3, 29
	jal	min_caml_float_of_int
	addi	r3, r3, -29
	lwi	r31 r3 28
	lwi	r1, r3 5
	fswi	f1, r3 28
	swi	r31 r3 29
	addi	r3, r3, 30
	jal	min_caml_float_of_int
	addi	r3, r3, -30
	lwi	r31 r3 29
	flwi	f2, r3 28 #off
	fdiv	f1, f2, f1
bge_cont.21323:
beq_cont.21321:
	jl	bge_cont.21319
bge_else.21318:
	swi	r31 r3 29
	addi	r3, r3, 30
	jal	read_float.2586
	addi	r3, r3, -30
	lwi	r31 r3 29
bge_cont.21319:
beq_cont.21317:
	lwi	r1, r3 9
	sli	r2, r1, 0
	lwi	r5, r3 26
	add	r0 r5 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	swi	r31 r3 29
	addi	r3, r3, 30
	jal	min_caml_read
	addi	r3, r3, -30
	lwi	r31 r3 29
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21324
	swi	r31 r3 29
	addi	r3, r3, 30
	jal	read_float.2586
	addi	r3, r3, -30
	lwi	r31 r3 29
	flwi	f2, r3 6 #off
	fsub	f1, f2, f1
	jl	beq_cont.21325
beq_else.21324:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21326
	addi	r1, r1, -48
	swi	r1, r3 29
	swi	r31 r3 30
	addi	r3, r3, 31
	jal	min_caml_read
	addi	r3, r3, -31
	lwi	r31 r3 30
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21328
	addi	r1, r0 10
	lwi	r2, r3 29
	lwi	r5, r3 5
	swi	r31 r3 30
	addi	r3, r3, 31
	jal	read_float_rec.2582
	addi	r3, r3, -31
	lwi	r31 r3 30
	jl	beq_cont.21329
beq_else.21328:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21330
	addi	r2, r0 10
	lwi	r5, r3 29
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 5
	addi	r5, r1 0 #
	swi	r31 r3 30
	addi	r3, r3, 31
	jal	read_float_rec.2582
	addi	r3, r3, -31
	lwi	r31 r3 30
	jl	bge_cont.21331
bge_else.21330:
	lwi	r1, r3 29
	swi	r31 r3 30
	addi	r3, r3, 31
	jal	min_caml_float_of_int
	addi	r3, r3, -31
	lwi	r31 r3 30
	lwi	r1, r3 5
	fswi	f1, r3 30
	swi	r31 r3 31
	addi	r3, r3, 32
	jal	min_caml_float_of_int
	addi	r3, r3, -32
	lwi	r31 r3 31
	flwi	f2, r3 30 #off
	fdiv	f1, f2, f1
bge_cont.21331:
beq_cont.21329:
	jl	bge_cont.21327
bge_else.21326:
	swi	r31 r3 31
	addi	r3, r3, 32
	jal	read_float.2586
	addi	r3, r3, -32
	lwi	r31 r3 31
bge_cont.21327:
beq_cont.21325:
	lwi	r1, r3 5
	sli	r2, r1, 0
	lwi	r5, r3 26
	add	r0 r5 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	swi	r31 r3 31
	addi	r3, r3, 32
	jal	min_caml_read
	addi	r3, r3, -32
	lwi	r31 r3 31
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21332
	swi	r31 r3 31
	addi	r3, r3, 32
	jal	read_float.2586
	addi	r3, r3, -32
	lwi	r31 r3 31
	flwi	f2, r3 6 #off
	fsub	f1, f2, f1
	jl	beq_cont.21333
beq_else.21332:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21334
	addi	r1, r1, -48
	swi	r1, r3 31
	swi	r31 r3 32
	addi	r3, r3, 33
	jal	min_caml_read
	addi	r3, r3, -33
	lwi	r31 r3 32
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21336
	addi	r1, r0 10
	lwi	r2, r3 31
	lwi	r5, r3 5
	swi	r31 r3 32
	addi	r3, r3, 33
	jal	read_float_rec.2582
	addi	r3, r3, -33
	lwi	r31 r3 32
	jl	beq_cont.21337
beq_else.21336:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21338
	addi	r2, r0 10
	lwi	r5, r3 31
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 5
	addi	r5, r1 0 #
	swi	r31 r3 32
	addi	r3, r3, 33
	jal	read_float_rec.2582
	addi	r3, r3, -33
	lwi	r31 r3 32
	jl	bge_cont.21339
bge_else.21338:
	lwi	r1, r3 31
	swi	r31 r3 32
	addi	r3, r3, 33
	jal	min_caml_float_of_int
	addi	r3, r3, -33
	lwi	r31 r3 32
	lwi	r1, r3 5
	fswi	f1, r3 32
	swi	r31 r3 33
	addi	r3, r3, 34
	jal	min_caml_float_of_int
	addi	r3, r3, -34
	lwi	r31 r3 33
	flwi	f2, r3 32 #off
	fdiv	f1, f2, f1
bge_cont.21339:
beq_cont.21337:
	jl	bge_cont.21335
bge_else.21334:
	swi	r31 r3 33
	addi	r3, r3, 34
	jal	read_float.2586
	addi	r3, r3, -34
	lwi	r31 r3 33
bge_cont.21335:
beq_cont.21333:
	lwi	r1, r3 4
	sli	r2, r1, 0
	lwi	r5, r3 26
	add	r0 r5 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 6 #off
	lwi	r2, r3 7
	addi	r1, r2 0 #
	swi	r31 r3 33
	addi	r3, r3, 34
	jal	min_caml_create_float_array
	addi	r3, r3, -34
	lwi	r31 r3 33
	swi	r1, r3 33
	swi	r31 r3 34
	addi	r3, r3, 35
	jal	min_caml_read
	addi	r3, r3, -35
	lwi	r31 r3 34
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21340
	swi	r31 r3 34
	addi	r3, r3, 35
	jal	read_float.2586
	addi	r3, r3, -35
	lwi	r31 r3 34
	flwi	f2, r3 6 #off
	fsub	f1, f2, f1
	jl	beq_cont.21341
beq_else.21340:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21342
	addi	r1, r1, -48
	swi	r1, r3 34
	swi	r31 r3 35
	addi	r3, r3, 36
	jal	min_caml_read
	addi	r3, r3, -36
	lwi	r31 r3 35
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21344
	addi	r1, r0 10
	lwi	r2, r3 34
	lwi	r5, r3 5
	swi	r31 r3 35
	addi	r3, r3, 36
	jal	read_float_rec.2582
	addi	r3, r3, -36
	lwi	r31 r3 35
	jl	beq_cont.21345
beq_else.21344:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21346
	addi	r2, r0 10
	lwi	r5, r3 34
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 5
	addi	r5, r1 0 #
	swi	r31 r3 35
	addi	r3, r3, 36
	jal	read_float_rec.2582
	addi	r3, r3, -36
	lwi	r31 r3 35
	jl	bge_cont.21347
bge_else.21346:
	lwi	r1, r3 34
	swi	r31 r3 35
	addi	r3, r3, 36
	jal	min_caml_float_of_int
	addi	r3, r3, -36
	lwi	r31 r3 35
	lwi	r1, r3 5
	fswi	f1, r3 36
	swi	r31 r3 37
	addi	r3, r3, 38
	jal	min_caml_float_of_int
	addi	r3, r3, -38
	lwi	r31 r3 37
	flwi	f2, r3 36 #off
	fdiv	f1, f2, f1
bge_cont.21347:
beq_cont.21345:
	jl	bge_cont.21343
bge_else.21342:
	swi	r31 r3 37
	addi	r3, r3, 38
	jal	read_float.2586
	addi	r3, r3, -38
	lwi	r31 r3 37
bge_cont.21343:
beq_cont.21341:
	lwi	r1, r3 9
	sli	r2, r1, 0
	lwi	r5, r3 33
	add	r0 r5 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	swi	r31 r3 37
	addi	r3, r3, 38
	jal	min_caml_read
	addi	r3, r3, -38
	lwi	r31 r3 37
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21349
	swi	r31 r3 37
	addi	r3, r3, 38
	jal	read_float.2586
	addi	r3, r3, -38
	lwi	r31 r3 37
	flwi	f2, r3 6 #off
	fsub	f1, f2, f1
	jl	beq_cont.21350
beq_else.21349:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21351
	addi	r1, r1, -48
	swi	r1, r3 37
	swi	r31 r3 38
	addi	r3, r3, 39
	jal	min_caml_read
	addi	r3, r3, -39
	lwi	r31 r3 38
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21353
	addi	r1, r0 10
	lwi	r2, r3 37
	lwi	r5, r3 5
	swi	r31 r3 38
	addi	r3, r3, 39
	jal	read_float_rec.2582
	addi	r3, r3, -39
	lwi	r31 r3 38
	jl	beq_cont.21354
beq_else.21353:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21355
	addi	r2, r0 10
	lwi	r5, r3 37
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 5
	addi	r5, r1 0 #
	swi	r31 r3 38
	addi	r3, r3, 39
	jal	read_float_rec.2582
	addi	r3, r3, -39
	lwi	r31 r3 38
	jl	bge_cont.21356
bge_else.21355:
	lwi	r1, r3 37
	swi	r31 r3 38
	addi	r3, r3, 39
	jal	min_caml_float_of_int
	addi	r3, r3, -39
	lwi	r31 r3 38
	lwi	r1, r3 5
	fswi	f1, r3 38
	swi	r31 r3 39
	addi	r3, r3, 40
	jal	min_caml_float_of_int
	addi	r3, r3, -40
	lwi	r31 r3 39
	flwi	f2, r3 38 #off
	fdiv	f1, f2, f1
bge_cont.21356:
beq_cont.21354:
	jl	bge_cont.21352
bge_else.21351:
	swi	r31 r3 39
	addi	r3, r3, 40
	jal	read_float.2586
	addi	r3, r3, -40
	lwi	r31 r3 39
bge_cont.21352:
beq_cont.21350:
	lwi	r1, r3 5
	sli	r2, r1, 0
	lwi	r5, r3 33
	add	r0 r5 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	swi	r31 r3 39
	addi	r3, r3, 40
	jal	min_caml_read
	addi	r3, r3, -40
	lwi	r31 r3 39
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21357
	swi	r31 r3 39
	addi	r3, r3, 40
	jal	read_float.2586
	addi	r3, r3, -40
	lwi	r31 r3 39
	flwi	f2, r3 6 #off
	fsub	f1, f2, f1
	jl	beq_cont.21358
beq_else.21357:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21359
	addi	r1, r1, -48
	swi	r1, r3 39
	swi	r31 r3 40
	addi	r3, r3, 41
	jal	min_caml_read
	addi	r3, r3, -41
	lwi	r31 r3 40
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21361
	addi	r1, r0 10
	lwi	r2, r3 39
	lwi	r5, r3 5
	swi	r31 r3 40
	addi	r3, r3, 41
	jal	read_float_rec.2582
	addi	r3, r3, -41
	lwi	r31 r3 40
	jl	beq_cont.21362
beq_else.21361:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21363
	addi	r2, r0 10
	lwi	r5, r3 39
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 5
	addi	r5, r1 0 #
	swi	r31 r3 40
	addi	r3, r3, 41
	jal	read_float_rec.2582
	addi	r3, r3, -41
	lwi	r31 r3 40
	jl	bge_cont.21364
bge_else.21363:
	lwi	r1, r3 39
	swi	r31 r3 40
	addi	r3, r3, 41
	jal	min_caml_float_of_int
	addi	r3, r3, -41
	lwi	r31 r3 40
	lwi	r1, r3 5
	fswi	f1, r3 40
	swi	r31 r3 41
	addi	r3, r3, 42
	jal	min_caml_float_of_int
	addi	r3, r3, -42
	lwi	r31 r3 41
	flwi	f2, r3 40 #off
	fdiv	f1, f2, f1
bge_cont.21364:
beq_cont.21362:
	jl	bge_cont.21360
bge_else.21359:
	swi	r31 r3 41
	addi	r3, r3, 42
	jal	read_float.2586
	addi	r3, r3, -42
	lwi	r31 r3 41
bge_cont.21360:
beq_cont.21358:
	lwi	r1, r3 4
	sli	r2, r1, 0
	lwi	r5, r3 33
	add	r0 r5 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	swi	r31 r3 41
	addi	r3, r3, 42
	jal	min_caml_read
	addi	r3, r3, -42
	lwi	r31 r3 41
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21365
	swi	r31 r3 41
	addi	r3, r3, 42
	jal	read_float.2586
	addi	r3, r3, -42
	lwi	r31 r3 41
	flwi	f2, r3 6 #off
	fsub	f1, f2, f1
	jl	beq_cont.21366
beq_else.21365:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21367
	addi	r1, r1, -48
	swi	r1, r3 41
	swi	r31 r3 42
	addi	r3, r3, 43
	jal	min_caml_read
	addi	r3, r3, -43
	lwi	r31 r3 42
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21369
	addi	r1, r0 10
	lwi	r2, r3 41
	lwi	r5, r3 5
	swi	r31 r3 42
	addi	r3, r3, 43
	jal	read_float_rec.2582
	addi	r3, r3, -43
	lwi	r31 r3 42
	jl	beq_cont.21370
beq_else.21369:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21371
	addi	r2, r0 10
	lwi	r5, r3 41
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 5
	addi	r5, r1 0 #
	swi	r31 r3 42
	addi	r3, r3, 43
	jal	read_float_rec.2582
	addi	r3, r3, -43
	lwi	r31 r3 42
	jl	bge_cont.21372
bge_else.21371:
	lwi	r1, r3 41
	swi	r31 r3 42
	addi	r3, r3, 43
	jal	min_caml_float_of_int
	addi	r3, r3, -43
	lwi	r31 r3 42
	lwi	r1, r3 5
	fswi	f1, r3 42
	swi	r31 r3 43
	addi	r3, r3, 44
	jal	min_caml_float_of_int
	addi	r3, r3, -44
	lwi	r31 r3 43
	flwi	f2, r3 42 #off
	fdiv	f1, f2, f1
bge_cont.21372:
beq_cont.21370:
	jl	bge_cont.21368
bge_else.21367:
	swi	r31 r3 43
	addi	r3, r3, 44
	jal	read_float.2586
	addi	r3, r3, -44
	lwi	r31 r3 43
bge_cont.21368:
beq_cont.21366:
	flwi	f2, r3 6 #off
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21373
	addi	r1, r0 0
	jl	ble_cont.21374
ble_else.21373:
	addi	r1, r0 1
ble_cont.21374:
	lwi	r2, r3 4
	swi	r1, r3 43
	addi	r1, r2 0 #
	fadd	f1, f2 f0
	swi	r31 r3 44
	addi	r3, r3, 45
	jal	min_caml_create_float_array
	addi	r3, r3, -45
	lwi	r31 r3 44
	swi	r1, r3 44
	swi	r31 r3 45
	addi	r3, r3, 46
	jal	min_caml_read
	addi	r3, r3, -46
	lwi	r31 r3 45
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21375
	swi	r31 r3 45
	addi	r3, r3, 46
	jal	read_float.2586
	addi	r3, r3, -46
	lwi	r31 r3 45
	flwi	f2, r3 6 #off
	fsub	f1, f2, f1
	jl	beq_cont.21376
beq_else.21375:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21377
	addi	r1, r1, -48
	swi	r1, r3 45
	swi	r31 r3 46
	addi	r3, r3, 47
	jal	min_caml_read
	addi	r3, r3, -47
	lwi	r31 r3 46
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21379
	addi	r1, r0 10
	lwi	r2, r3 45
	lwi	r5, r3 5
	swi	r31 r3 46
	addi	r3, r3, 47
	jal	read_float_rec.2582
	addi	r3, r3, -47
	lwi	r31 r3 46
	jl	beq_cont.21380
beq_else.21379:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21381
	addi	r2, r0 10
	lwi	r5, r3 45
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 5
	addi	r5, r1 0 #
	swi	r31 r3 46
	addi	r3, r3, 47
	jal	read_float_rec.2582
	addi	r3, r3, -47
	lwi	r31 r3 46
	jl	bge_cont.21382
bge_else.21381:
	lwi	r1, r3 45
	swi	r31 r3 46
	addi	r3, r3, 47
	jal	min_caml_float_of_int
	addi	r3, r3, -47
	lwi	r31 r3 46
	lwi	r1, r3 5
	fswi	f1, r3 46
	swi	r31 r3 47
	addi	r3, r3, 48
	jal	min_caml_float_of_int
	addi	r3, r3, -48
	lwi	r31 r3 47
	flwi	f2, r3 46 #off
	fdiv	f1, f2, f1
bge_cont.21382:
beq_cont.21380:
	jl	bge_cont.21378
bge_else.21377:
	swi	r31 r3 47
	addi	r3, r3, 48
	jal	read_float.2586
	addi	r3, r3, -48
	lwi	r31 r3 47
bge_cont.21378:
beq_cont.21376:
	lwi	r1, r3 9
	sli	r2, r1, 0
	lwi	r5, r3 44
	add	r0 r5 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	swi	r31 r3 47
	addi	r3, r3, 48
	jal	min_caml_read
	addi	r3, r3, -48
	lwi	r31 r3 47
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21383
	swi	r31 r3 47
	addi	r3, r3, 48
	jal	read_float.2586
	addi	r3, r3, -48
	lwi	r31 r3 47
	flwi	f2, r3 6 #off
	fsub	f1, f2, f1
	jl	beq_cont.21384
beq_else.21383:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21385
	addi	r1, r1, -48
	swi	r1, r3 47
	swi	r31 r3 48
	addi	r3, r3, 49
	jal	min_caml_read
	addi	r3, r3, -49
	lwi	r31 r3 48
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21387
	addi	r1, r0 10
	lwi	r2, r3 47
	lwi	r5, r3 5
	swi	r31 r3 48
	addi	r3, r3, 49
	jal	read_float_rec.2582
	addi	r3, r3, -49
	lwi	r31 r3 48
	jl	beq_cont.21388
beq_else.21387:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21389
	addi	r2, r0 10
	lwi	r5, r3 47
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 5
	addi	r5, r1 0 #
	swi	r31 r3 48
	addi	r3, r3, 49
	jal	read_float_rec.2582
	addi	r3, r3, -49
	lwi	r31 r3 48
	jl	bge_cont.21390
bge_else.21389:
	lwi	r1, r3 47
	swi	r31 r3 48
	addi	r3, r3, 49
	jal	min_caml_float_of_int
	addi	r3, r3, -49
	lwi	r31 r3 48
	lwi	r1, r3 5
	fswi	f1, r3 48
	swi	r31 r3 49
	addi	r3, r3, 50
	jal	min_caml_float_of_int
	addi	r3, r3, -50
	lwi	r31 r3 49
	flwi	f2, r3 48 #off
	fdiv	f1, f2, f1
bge_cont.21390:
beq_cont.21388:
	jl	bge_cont.21386
bge_else.21385:
	swi	r31 r3 49
	addi	r3, r3, 50
	jal	read_float.2586
	addi	r3, r3, -50
	lwi	r31 r3 49
bge_cont.21386:
beq_cont.21384:
	lwi	r1, r3 5
	sli	r2, r1, 0
	lwi	r5, r3 44
	add	r0 r5 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 6 #off
	lwi	r2, r3 7
	addi	r1, r2 0 #
	swi	r31 r3 49
	addi	r3, r3, 50
	jal	min_caml_create_float_array
	addi	r3, r3, -50
	lwi	r31 r3 49
	swi	r1, r3 49
	swi	r31 r3 50
	addi	r3, r3, 51
	jal	min_caml_read
	addi	r3, r3, -51
	lwi	r31 r3 50
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21391
	swi	r31 r3 50
	addi	r3, r3, 51
	jal	read_float.2586
	addi	r3, r3, -51
	lwi	r31 r3 50
	flwi	f2, r3 6 #off
	fsub	f1, f2, f1
	jl	beq_cont.21392
beq_else.21391:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21393
	addi	r1, r1, -48
	swi	r1, r3 50
	swi	r31 r3 51
	addi	r3, r3, 52
	jal	min_caml_read
	addi	r3, r3, -52
	lwi	r31 r3 51
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21395
	addi	r1, r0 10
	lwi	r2, r3 50
	lwi	r5, r3 5
	swi	r31 r3 51
	addi	r3, r3, 52
	jal	read_float_rec.2582
	addi	r3, r3, -52
	lwi	r31 r3 51
	jl	beq_cont.21396
beq_else.21395:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21397
	addi	r2, r0 10
	lwi	r5, r3 50
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 5
	addi	r5, r1 0 #
	swi	r31 r3 51
	addi	r3, r3, 52
	jal	read_float_rec.2582
	addi	r3, r3, -52
	lwi	r31 r3 51
	jl	bge_cont.21398
bge_else.21397:
	lwi	r1, r3 50
	swi	r31 r3 51
	addi	r3, r3, 52
	jal	min_caml_float_of_int
	addi	r3, r3, -52
	lwi	r31 r3 51
	lwi	r1, r3 5
	fswi	f1, r3 52
	swi	r31 r3 53
	addi	r3, r3, 54
	jal	min_caml_float_of_int
	addi	r3, r3, -54
	lwi	r31 r3 53
	flwi	f2, r3 52 #off
	fdiv	f1, f2, f1
bge_cont.21398:
beq_cont.21396:
	jl	bge_cont.21394
bge_else.21393:
	swi	r31 r3 53
	addi	r3, r3, 54
	jal	read_float.2586
	addi	r3, r3, -54
	lwi	r31 r3 53
bge_cont.21394:
beq_cont.21392:
	lwi	r1, r3 9
	sli	r2, r1, 0
	lwi	r5, r3 49
	add	r0 r5 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	swi	r31 r3 53
	addi	r3, r3, 54
	jal	min_caml_read
	addi	r3, r3, -54
	lwi	r31 r3 53
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21400
	swi	r31 r3 53
	addi	r3, r3, 54
	jal	read_float.2586
	addi	r3, r3, -54
	lwi	r31 r3 53
	flwi	f2, r3 6 #off
	fsub	f1, f2, f1
	jl	beq_cont.21401
beq_else.21400:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21402
	addi	r1, r1, -48
	swi	r1, r3 53
	swi	r31 r3 54
	addi	r3, r3, 55
	jal	min_caml_read
	addi	r3, r3, -55
	lwi	r31 r3 54
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21404
	addi	r1, r0 10
	lwi	r2, r3 53
	lwi	r5, r3 5
	swi	r31 r3 54
	addi	r3, r3, 55
	jal	read_float_rec.2582
	addi	r3, r3, -55
	lwi	r31 r3 54
	jl	beq_cont.21405
beq_else.21404:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21406
	addi	r2, r0 10
	lwi	r5, r3 53
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 5
	addi	r5, r1 0 #
	swi	r31 r3 54
	addi	r3, r3, 55
	jal	read_float_rec.2582
	addi	r3, r3, -55
	lwi	r31 r3 54
	jl	bge_cont.21407
bge_else.21406:
	lwi	r1, r3 53
	swi	r31 r3 54
	addi	r3, r3, 55
	jal	min_caml_float_of_int
	addi	r3, r3, -55
	lwi	r31 r3 54
	lwi	r1, r3 5
	fswi	f1, r3 54
	swi	r31 r3 55
	addi	r3, r3, 56
	jal	min_caml_float_of_int
	addi	r3, r3, -56
	lwi	r31 r3 55
	flwi	f2, r3 54 #off
	fdiv	f1, f2, f1
bge_cont.21407:
beq_cont.21405:
	jl	bge_cont.21403
bge_else.21402:
	swi	r31 r3 55
	addi	r3, r3, 56
	jal	read_float.2586
	addi	r3, r3, -56
	lwi	r31 r3 55
bge_cont.21403:
beq_cont.21401:
	lwi	r1, r3 5
	sli	r2, r1, 0
	lwi	r5, r3 49
	add	r0 r5 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	swi	r31 r3 55
	addi	r3, r3, 56
	jal	min_caml_read
	addi	r3, r3, -56
	lwi	r31 r3 55
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21408
	swi	r31 r3 55
	addi	r3, r3, 56
	jal	read_float.2586
	addi	r3, r3, -56
	lwi	r31 r3 55
	flwi	f2, r3 6 #off
	fsub	f1, f2, f1
	jl	beq_cont.21409
beq_else.21408:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21410
	addi	r1, r1, -48
	swi	r1, r3 55
	swi	r31 r3 56
	addi	r3, r3, 57
	jal	min_caml_read
	addi	r3, r3, -57
	lwi	r31 r3 56
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21412
	addi	r1, r0 10
	lwi	r2, r3 55
	lwi	r5, r3 5
	swi	r31 r3 56
	addi	r3, r3, 57
	jal	read_float_rec.2582
	addi	r3, r3, -57
	lwi	r31 r3 56
	jl	beq_cont.21413
beq_else.21412:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21414
	addi	r2, r0 10
	lwi	r5, r3 55
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 5
	addi	r5, r1 0 #
	swi	r31 r3 56
	addi	r3, r3, 57
	jal	read_float_rec.2582
	addi	r3, r3, -57
	lwi	r31 r3 56
	jl	bge_cont.21415
bge_else.21414:
	lwi	r1, r3 55
	swi	r31 r3 56
	addi	r3, r3, 57
	jal	min_caml_float_of_int
	addi	r3, r3, -57
	lwi	r31 r3 56
	lwi	r1, r3 5
	fswi	f1, r3 56
	swi	r31 r3 57
	addi	r3, r3, 58
	jal	min_caml_float_of_int
	addi	r3, r3, -58
	lwi	r31 r3 57
	flwi	f2, r3 56 #off
	fdiv	f1, f2, f1
bge_cont.21415:
beq_cont.21413:
	jl	bge_cont.21411
bge_else.21410:
	swi	r31 r3 57
	addi	r3, r3, 58
	jal	read_float.2586
	addi	r3, r3, -58
	lwi	r31 r3 57
bge_cont.21411:
beq_cont.21409:
	lwi	r1, r3 4
	sli	r2, r1, 0
	lwi	r5, r3 49
	add	r0 r5 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 6 #off
	lwi	r2, r3 7
	addi	r1, r2 0 #
	swi	r31 r3 57
	addi	r3, r3, 58
	jal	min_caml_create_float_array
	addi	r3, r3, -58
	lwi	r31 r3 57
	lwi	r2, r3 9
	lwi	r5, r3 25
	swi	r1, r3 57
	seq	r30, r5, r2
	jeql	r30, r0, beq_else.21416
	jl	beq_cont.21417
beq_else.21416:
	swi	r31 r3 58
	addi	r3, r3, 59
	jal	min_caml_read
	addi	r3, r3, -59
	lwi	r31 r3 58
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21418
	swi	r31 r3 58
	addi	r3, r3, 59
	jal	read_float.2586
	addi	r3, r3, -59
	lwi	r31 r3 58
	flwi	f2, r3 6 #off
	fsub	f1, f2, f1
	jl	beq_cont.21419
beq_else.21418:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21420
	addi	r1, r1, -48
	swi	r1, r3 58
	swi	r31 r3 59
	addi	r3, r3, 60
	jal	min_caml_read
	addi	r3, r3, -60
	lwi	r31 r3 59
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21422
	addi	r1, r0 10
	lwi	r2, r3 58
	lwi	r5, r3 5
	swi	r31 r3 59
	addi	r3, r3, 60
	jal	read_float_rec.2582
	addi	r3, r3, -60
	lwi	r31 r3 59
	jl	beq_cont.21423
beq_else.21422:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21424
	addi	r2, r0 10
	lwi	r5, r3 58
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 5
	addi	r5, r1 0 #
	swi	r31 r3 59
	addi	r3, r3, 60
	jal	read_float_rec.2582
	addi	r3, r3, -60
	lwi	r31 r3 59
	jl	bge_cont.21425
bge_else.21424:
	lwi	r1, r3 58
	swi	r31 r3 59
	addi	r3, r3, 60
	jal	min_caml_float_of_int
	addi	r3, r3, -60
	lwi	r31 r3 59
	lwi	r1, r3 5
	fswi	f1, r3 60
	swi	r31 r3 61
	addi	r3, r3, 62
	jal	min_caml_float_of_int
	addi	r3, r3, -62
	lwi	r31 r3 61
	flwi	f2, r3 60 #off
	fdiv	f1, f2, f1
bge_cont.21425:
beq_cont.21423:
	jl	bge_cont.21421
bge_else.21420:
	swi	r31 r3 61
	addi	r3, r3, 62
	jal	read_float.2586
	addi	r3, r3, -62
	lwi	r31 r3 61
bge_cont.21421:
beq_cont.21419:
	flwl	%f2 l.11850
	fmul	f1, f1, f2
	lwi	r1, r3 9
	sli	r2, r1, 0
	lwi	r5, r3 57
	add	r0 r5 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	fswi	f2, r3 62
	swi	r31 r3 63
	addi	r3, r3, 64
	jal	min_caml_read
	addi	r3, r3, -64
	lwi	r31 r3 63
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21428
	swi	r31 r3 63
	addi	r3, r3, 64
	jal	read_float.2586
	addi	r3, r3, -64
	lwi	r31 r3 63
	flwi	f2, r3 6 #off
	fsub	f1, f2, f1
	jl	beq_cont.21429
beq_else.21428:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21430
	addi	r1, r1, -48
	swi	r1, r3 63
	swi	r31 r3 64
	addi	r3, r3, 65
	jal	min_caml_read
	addi	r3, r3, -65
	lwi	r31 r3 64
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21432
	addi	r1, r0 10
	lwi	r2, r3 63
	lwi	r5, r3 5
	swi	r31 r3 64
	addi	r3, r3, 65
	jal	read_float_rec.2582
	addi	r3, r3, -65
	lwi	r31 r3 64
	jl	beq_cont.21433
beq_else.21432:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21434
	addi	r2, r0 10
	lwi	r5, r3 63
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 5
	addi	r5, r1 0 #
	swi	r31 r3 64
	addi	r3, r3, 65
	jal	read_float_rec.2582
	addi	r3, r3, -65
	lwi	r31 r3 64
	jl	bge_cont.21435
bge_else.21434:
	lwi	r1, r3 63
	swi	r31 r3 64
	addi	r3, r3, 65
	jal	min_caml_float_of_int
	addi	r3, r3, -65
	lwi	r31 r3 64
	lwi	r1, r3 5
	fswi	f1, r3 64
	swi	r31 r3 65
	addi	r3, r3, 66
	jal	min_caml_float_of_int
	addi	r3, r3, -66
	lwi	r31 r3 65
	flwi	f2, r3 64 #off
	fdiv	f1, f2, f1
bge_cont.21435:
beq_cont.21433:
	jl	bge_cont.21431
bge_else.21430:
	swi	r31 r3 65
	addi	r3, r3, 66
	jal	read_float.2586
	addi	r3, r3, -66
	lwi	r31 r3 65
bge_cont.21431:
beq_cont.21429:
	flwi	f2, r3 62 #off
	fmul	f1, f1, f2
	lwi	r1, r3 5
	sli	r2, r1, 0
	lwi	r5, r3 57
	add	r0 r5 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	swi	r31 r3 65
	addi	r3, r3, 66
	jal	min_caml_read
	addi	r3, r3, -66
	lwi	r31 r3 65
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21436
	swi	r31 r3 65
	addi	r3, r3, 66
	jal	read_float.2586
	addi	r3, r3, -66
	lwi	r31 r3 65
	flwi	f2, r3 6 #off
	fsub	f1, f2, f1
	jl	beq_cont.21437
beq_else.21436:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21438
	addi	r1, r1, -48
	swi	r1, r3 65
	swi	r31 r3 66
	addi	r3, r3, 67
	jal	min_caml_read
	addi	r3, r3, -67
	lwi	r31 r3 66
	addi	r30, r0, 46
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21440
	addi	r1, r0 10
	lwi	r2, r3 65
	lwi	r5, r3 5
	swi	r31 r3 66
	addi	r3, r3, 67
	jal	read_float_rec.2582
	addi	r3, r3, -67
	lwi	r31 r3 66
	jl	beq_cont.21441
beq_else.21440:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21442
	addi	r2, r0 10
	lwi	r5, r3 65
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r2, r2, r1
	lwi	r1, r3 5
	addi	r5, r1 0 #
	swi	r31 r3 66
	addi	r3, r3, 67
	jal	read_float_rec.2582
	addi	r3, r3, -67
	lwi	r31 r3 66
	jl	bge_cont.21443
bge_else.21442:
	lwi	r1, r3 65
	swi	r31 r3 66
	addi	r3, r3, 67
	jal	min_caml_float_of_int
	addi	r3, r3, -67
	lwi	r31 r3 66
	lwi	r1, r3 5
	fswi	f1, r3 66
	swi	r31 r3 67
	addi	r3, r3, 68
	jal	min_caml_float_of_int
	addi	r3, r3, -68
	lwi	r31 r3 67
	flwi	f2, r3 66 #off
	fdiv	f1, f2, f1
bge_cont.21443:
beq_cont.21441:
	jl	bge_cont.21439
bge_else.21438:
	swi	r31 r3 67
	addi	r3, r3, 68
	jal	read_float.2586
	addi	r3, r3, -68
	lwi	r31 r3 67
bge_cont.21439:
beq_cont.21437:
	flwi	f2, r3 62 #off
	fmul	f1, f1, f2
	lwi	r1, r3 4
	sli	r2, r1, 0
	lwi	r5, r3 57
	add	r0 r5 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
beq_cont.21417:
	lwi	r1, r3 4
	lwi	r2, r3 17
	seq	r30, r2, r1
	jeql	r30, r0, beq_else.21444
	addi	r5, r0 1
	jl	beq_cont.21445
beq_else.21444:
	lwi	r5, r3 43
beq_cont.21445:
	addi	r6, r0 4
	flwi	f1, r3 6 #off
	swi	r5, r3 67
	addi	r1, r6 0 #
	swi	r31 r3 68
	addi	r3, r3, 69
	jal	min_caml_create_float_array
	addi	r3, r3, -69
	lwi	r31 r3 68
	addi	r2, r4 0
	addi	r4, r4, 11
	swi	r1, r2 10
	lwi	r1, r3 57
	swi	r1, r2 9
	lwi	r5, r3 49
	swi	r5, r2 8
	lwi	r5, r3 44
	swi	r5, r2 7
	lwi	r5, r3 67
	swi	r5, r2 6
	lwi	r5, r3 33
	swi	r5, r2 5
	lwi	r5, r3 26
	swi	r5, r2 4
	lwi	r6, r3 25
	swi	r6, r2 3
	lwi	r7, r3 21
	swi	r7, r2 2
	lwi	r7, r3 17
	swi	r7, r2 1
	lwi	r8, r3 13
	swi	r8, r2 0
	lwi	r8, r3 3
	sli	r8, r8, 0
	lwi	r9, r3 2
	add	r0 r9 r8
	swi	r2, r0 0
	sub	r0 r0 r0
	lwi	r2, r3 7
	seq	r30, r7, r2
	jeql	r30, r0, beq_else.21446
	lwi	r2, r3 9
	sli	r7, r2, 0
	add	r0 r5 r7
	flwi	f1, r0 0
	sub	r0 r0 r0
	flwi	f2, r3 6 #off
	feq	r30, f1, f2
	jeql	r30, r0, beq_else.21448
	addi	r7, r0 1
	jl	beq_cont.21449
beq_else.21448:
	addi	r7, r0 0
beq_cont.21449:
	seq	r30, r7, r2
	jeql	r30, r0, beq_else.21450
	seq	r30, r7, r2
	jeql	r30, r0, beq_else.21452
	fgt	r30 f1 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21454
	addi	r7, r0 0
	jl	ble_cont.21455
ble_else.21454:
	addi	r7, r0 1
ble_cont.21455:
	seq	r30, r7, r2
	jeql	r30, r0, beq_else.21456
	flwl	%f3 l.11828
	jl	beq_cont.21457
beq_else.21456:
	flwl	%f3 l.11794
beq_cont.21457:
	jl	beq_cont.21453
beq_else.21452:
	flwl	%f3 l.11783
beq_cont.21453:
	fmul	f1, f1, f1
	fdiv	f1, f3, f1
	jl	beq_cont.21451
beq_else.21450:
	flwl	%f1 l.11783
beq_cont.21451:
	sli	r7, r2, 0
	add	r0 r5 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r7, r3 5
	sli	r8, r7, 0
	add	r0 r5 r8
	flwi	f1, r0 0
	sub	r0 r0 r0
	feq	r30, f1, f2
	jeql	r30, r0, beq_else.21458
	addi	r8, r0 1
	jl	beq_cont.21459
beq_else.21458:
	addi	r8, r0 0
beq_cont.21459:
	seq	r30, r8, r2
	jeql	r30, r0, beq_else.21460
	seq	r30, r8, r2
	jeql	r30, r0, beq_else.21462
	fgt	r30 f1 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21464
	addi	r8, r0 0
	jl	ble_cont.21465
ble_else.21464:
	addi	r8, r0 1
ble_cont.21465:
	seq	r30, r8, r2
	jeql	r30, r0, beq_else.21466
	flwl	%f3 l.11828
	jl	beq_cont.21467
beq_else.21466:
	flwl	%f3 l.11794
beq_cont.21467:
	jl	beq_cont.21463
beq_else.21462:
	flwl	%f3 l.11783
beq_cont.21463:
	fmul	f1, f1, f1
	fdiv	f1, f3, f1
	jl	beq_cont.21461
beq_else.21460:
	flwl	%f1 l.11783
beq_cont.21461:
	sli	r7, r7, 0
	add	r0 r5 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r7, r3 4
	sli	r8, r7, 0
	add	r0 r5 r8
	flwi	f1, r0 0
	sub	r0 r0 r0
	feq	r30, f1, f2
	jeql	r30, r0, beq_else.21468
	addi	r8, r0 1
	jl	beq_cont.21469
beq_else.21468:
	addi	r8, r0 0
beq_cont.21469:
	seq	r30, r8, r2
	jeql	r30, r0, beq_else.21470
	seq	r30, r8, r2
	jeql	r30, r0, beq_else.21472
	fgt	r30 f1 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21474
	addi	r8, r0 0
	jl	ble_cont.21475
ble_else.21474:
	addi	r8, r0 1
ble_cont.21475:
	seq	r30, r8, r2
	jeql	r30, r0, beq_else.21476
	flwl	%f2 l.11828
	jl	beq_cont.21477
beq_else.21476:
	flwl	%f2 l.11794
beq_cont.21477:
	jl	beq_cont.21473
beq_else.21472:
	flwl	%f2 l.11783
beq_cont.21473:
	fmul	f1, f1, f1
	fdiv	f1, f2, f1
	jl	beq_cont.21471
beq_else.21470:
	flwl	%f1 l.11783
beq_cont.21471:
	sli	r7, r7, 0
	add	r0 r5 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.21447
beq_else.21446:
	lwi	r2, r3 4
	seq	r30, r7, r2
	jeql	r30, r0, beq_else.21478
	lwi	r2, r3 9
	lwi	r7, r3 43
	seq	r30, r7, r2
	jeql	r30, r0, beq_else.21480
	addi	r7, r0 1
	jl	beq_cont.21481
beq_else.21480:
	addi	r7, r0 0
beq_cont.21481:
	lwi	r28, r3 1
	addi	r2, r7 0 #
	addi	r1, r5 0 #
	swi	r31 r3 68
	addi	r3, r3, 69
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -69
	lwi	r31, r3 68
	jl	beq_cont.21479
beq_else.21478:
beq_cont.21479:
beq_cont.21447:
	lwi	r1, r3 9
	lwi	r2, r3 25
	seq	r30, r2, r1
	jeql	r30, r0, beq_else.21482
	jl	beq_cont.21483
beq_else.21482:
	lwi	r1, r3 26
	lwi	r2, r3 57
	lwi	r28, r3 0
	swi	r31 r3 68
	addi	r3, r3, 69
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -69
	lwi	r31, r3 68
beq_cont.21483:
	addi	r1, r0 1
	jr	r31
read_object.2764:
	lwi	r2, r28 5
	lwi	r5, r28 4
	lwi	r6, r28 3
	lwi	r7, r28 2
	lwi	r8, r28 1
	sgt	r30, r6, r1
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.21484
	jr	r31
ble_else.21484:
	swi	r28, r3 0
	swi	r2, r3 1
	swi	r6, r3 2
	swi	r8, r3 3
	swi	r5, r3 4
	swi	r1, r3 5
	swi	r7, r3 6
	addi	r28, r2 0 #
	swi	r31 r3 7
	addi	r3, r3, 8
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -8
	lwi	r31, r3 7
	lwi	r2, r3 6
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21486
	sli	r1, r2, 0
	lwi	r2, r3 4
	lwi	r5, r3 5
	add	r0 r2 r1
	swi	r5, r0 0
	sub	r0 r0 r0
	jr	r31
beq_else.21486:
	lwi	r1, r3 3
	lwi	r5, r3 5
	add	r5, r5, r1
	lwi	r6, r3 2
	sgt	r30, r6, r5
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.21488
	jr	r31
ble_else.21488:
	lwi	r28, r3 1
	swi	r5, r3 7
	addi	r1, r5 0 #
	swi	r31 r3 8
	addi	r3, r3, 9
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -9
	lwi	r31, r3 8
	lwi	r2, r3 6
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21490
	sli	r1, r2, 0
	lwi	r2, r3 4
	lwi	r5, r3 7
	add	r0 r2 r1
	swi	r5, r0 0
	sub	r0 r0 r0
	jr	r31
beq_else.21490:
	lwi	r1, r3 3
	lwi	r5, r3 7
	add	r5, r5, r1
	lwi	r6, r3 2
	sgt	r30, r6, r5
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.21492
	jr	r31
ble_else.21492:
	lwi	r28, r3 1
	swi	r5, r3 8
	addi	r1, r5 0 #
	swi	r31 r3 9
	addi	r3, r3, 10
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -10
	lwi	r31, r3 9
	lwi	r2, r3 6
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21494
	sli	r1, r2, 0
	lwi	r2, r3 4
	lwi	r5, r3 8
	add	r0 r2 r1
	swi	r5, r0 0
	sub	r0 r0 r0
	jr	r31
beq_else.21494:
	lwi	r1, r3 3
	lwi	r5, r3 8
	add	r5, r5, r1
	lwi	r6, r3 2
	sgt	r30, r6, r5
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.21496
	jr	r31
ble_else.21496:
	lwi	r28, r3 1
	swi	r5, r3 9
	addi	r1, r5 0 #
	swi	r31 r3 10
	addi	r3, r3, 11
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -11
	lwi	r31, r3 10
	lwi	r2, r3 6
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21498
	sli	r1, r2, 0
	lwi	r2, r3 4
	lwi	r5, r3 9
	add	r0 r2 r1
	swi	r5, r0 0
	sub	r0 r0 r0
	jr	r31
beq_else.21498:
	lwi	r1, r3 3
	lwi	r2, r3 9
	add	r1, r2, r1
	lwi	r28, r3 0
	lwi	r29, r28 0
	jr	r29
read_net_item.2768:
	lwi	r2, r28 3
	lwi	r5, r28 2
	lwi	r6, r28 1
	swi	r28, r3 0
	swi	r6, r3 1
	swi	r1, r3 2
	swi	r2, r3 3
	swi	r5, r3 4
	swi	r31 r3 5
	addi	r3, r3, 6
	jal	min_caml_read
	addi	r3, r3, -6
	lwi	r31 r3 5
	addi	r30, r0, 45
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.21500
	swi	r31 r3 5
	addi	r3, r3, 6
	jal	read_int.2580
	addi	r3, r3, -6
	lwi	r31 r3 5
	lwi	r2, r3 4
	sub	r1, r2, r1
	jl	beq_cont.21501
beq_else.21500:
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21502
	addi	r1, r1, -48
	swi	r1, r3 5
	swi	r31 r3 6
	addi	r3, r3, 7
	jal	min_caml_read
	addi	r3, r3, -7
	lwi	r31 r3 6
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21504
	addi	r2, r0 10
	lwi	r5, r3 5
	mul	r5, r5, r2
	addi	r1, r1, -48
	add	r1, r5, r1
	swi	r2, r3 6
	swi	r1, r3 7
	swi	r31 r3 8
	addi	r3, r3, 9
	jal	min_caml_read
	addi	r3, r3, -9
	lwi	r31 r3 8
	addi	r30, r0, 48
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.21506
	lwi	r2, r3 6
	lwi	r5, r3 7
	mul	r2, r5, r2
	addi	r1, r1, -48
	add	r1, r2, r1
	swi	r31 r3 8
	addi	r3, r3, 9
	jal	read_int_rec.2578
	addi	r3, r3, -9
	lwi	r31 r3 8
	jl	bge_cont.21507
bge_else.21506:
	lwi	r1, r3 7
bge_cont.21507:
	jl	bge_cont.21505
bge_else.21504:
	lwi	r1, r3 5
bge_cont.21505:
	jl	bge_cont.21503
bge_else.21502:
	swi	r31 r3 8
	addi	r3, r3, 9
	jal	read_int.2580
	addi	r3, r3, -9
	lwi	r31 r3 8
bge_cont.21503:
beq_cont.21501:
	lwi	r2, r3 3
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21508
	lwi	r1, r3 1
	lwi	r5, r3 2
	add	r1, r5, r1
	jl	min_caml_create_array
beq_else.21508:
	lwi	r5, r3 1
	lwi	r6, r3 2
	add	r7, r6, r5
	swi	r1, r3 8
	swi	r7, r3 9
	swi	r31 r3 10
	addi	r3, r3, 11
	jal	read_int.2580
	addi	r3, r3, -11
	lwi	r31 r3 10
	lwi	r2, r3 3
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21509
	lwi	r1, r3 1
	lwi	r5, r3 9
	add	r1, r5, r1
	swi	r31 r3 10
	addi	r3, r3, 11
	jal	min_caml_create_array
	addi	r3, r3, -11
	lwi	r31 r3 10
	jl	beq_cont.21510
beq_else.21509:
	lwi	r2, r3 1
	lwi	r5, r3 9
	add	r2, r5, r2
	lwi	r28, r3 0
	swi	r1, r3 10
	addi	r1, r2 0 #
	swi	r31 r3 11
	addi	r3, r3, 12
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -12
	lwi	r31, r3 11
	lwi	r2, r3 9
	sli	r2, r2, 0
	lwi	r5, r3 10
	add	r0 r1 r2
	swi	r5, r0 0
	sub	r0 r0 r0
beq_cont.21510:
	lwi	r2, r3 2
	sli	r2, r2, 0
	lwi	r5, r3 8
	add	r0 r1 r2
	swi	r5, r0 0
	sub	r0 r0 r0
	jr	r31
read_or_network.2770:
	lwi	r2, r28 4
	lwi	r5, r28 3
	lwi	r6, r28 2
	lwi	r7, r28 1
	swi	r28, r3 0
	swi	r1, r3 1
	swi	r6, r3 2
	swi	r2, r3 3
	swi	r7, r3 4
	swi	r5, r3 5
	swi	r31 r3 6
	addi	r3, r3, 7
	jal	read_int.2580
	addi	r3, r3, -7
	lwi	r31 r3 6
	lwi	r2, r3 5
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21511
	lwi	r1, r3 4
	swi	r31 r3 6
	addi	r3, r3, 7
	jal	min_caml_create_array
	addi	r3, r3, -7
	lwi	r31 r3 6
	addi	r2, r1 0 ##
	jl	beq_cont.21512
beq_else.21511:
	lwi	r5, r3 4
	lwi	r28, r3 3
	swi	r1, r3 6
	addi	r1, r5 0 #
	swi	r31 r3 7
	addi	r3, r3, 8
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -8
	lwi	r31, r3 7
	lwi	r2, r3 2
	sli	r5, r2, 0
	lwi	r6, r3 6
	add	r0 r1 r5
	swi	r6, r0 0
	sub	r0 r0 r0
	addi	r2, r1 0
beq_cont.21512:
	lwi	r1, r3 2
	sli	r5, r1, 0
	add	r5 r2 r5
	lwi	r5, r5 0
	lwi	r6, r3 5
	seq	r30, r5, r6
	jeql	r30, r0, beq_else.21513
	lwi	r1, r3 4
	lwi	r5, r3 1
	add	r1, r5, r1
	jl	min_caml_create_array
beq_else.21513:
	lwi	r5, r3 4
	lwi	r7, r3 1
	add	r8, r7, r5
	lwi	r28, r3 3
	swi	r2, r3 7
	swi	r8, r3 8
	swi	r31 r3 9
	addi	r3, r3, 10
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -10
	lwi	r31, r3 9
	addi	r2, r1 0 #
	lwi	r1, r3 2
	sli	r1, r1, 0
	add	r1 r2 r1
	lwi	r1, r1 0
	lwi	r5, r3 5
	seq	r30, r1, r5
	jeql	r30, r0, beq_else.21514
	lwi	r1, r3 4
	lwi	r5, r3 8
	add	r1, r5, r1
	swi	r31 r3 9
	addi	r3, r3, 10
	jal	min_caml_create_array
	addi	r3, r3, -10
	lwi	r31 r3 9
	jl	beq_cont.21515
beq_else.21514:
	lwi	r1, r3 4
	lwi	r5, r3 8
	add	r1, r5, r1
	lwi	r28, r3 0
	swi	r2, r3 9
	swi	r31 r3 10
	addi	r3, r3, 11
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -11
	lwi	r31, r3 10
	lwi	r2, r3 8
	sli	r2, r2, 0
	lwi	r5, r3 9
	add	r0 r1 r2
	swi	r5, r0 0
	sub	r0 r0 r0
beq_cont.21515:
	lwi	r2, r3 1
	sli	r2, r2, 0
	lwi	r5, r3 7
	add	r0 r1 r2
	swi	r5, r0 0
	sub	r0 r0 r0
	jr	r31
read_and_network.2772:
	lwi	r2, r28 5
	lwi	r5, r28 4
	lwi	r6, r28 3
	lwi	r7, r28 2
	lwi	r8, r28 1
	swi	r28, r3 0
	swi	r5, r3 1
	swi	r1, r3 2
	swi	r7, r3 3
	swi	r2, r3 4
	swi	r8, r3 5
	swi	r6, r3 6
	swi	r31 r3 7
	addi	r3, r3, 8
	jal	read_int.2580
	addi	r3, r3, -8
	lwi	r31 r3 7
	lwi	r2, r3 6
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21516
	lwi	r1, r3 5
	swi	r31 r3 7
	addi	r3, r3, 8
	jal	min_caml_create_array
	addi	r3, r3, -8
	lwi	r31 r3 7
	jl	beq_cont.21517
beq_else.21516:
	lwi	r5, r3 5
	lwi	r28, r3 4
	swi	r1, r3 7
	addi	r1, r5 0 #
	swi	r31 r3 8
	addi	r3, r3, 9
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -9
	lwi	r31, r3 8
	lwi	r2, r3 3
	sli	r5, r2, 0
	lwi	r6, r3 7
	add	r0 r1 r5
	swi	r6, r0 0
	sub	r0 r0 r0
beq_cont.21517:
	lwi	r2, r3 3
	sli	r5, r2, 0
	add	r5 r1 r5
	lwi	r5, r5 0
	lwi	r6, r3 6
	seq	r30, r5, r6
	jeql	r30, r0, beq_else.21518
	jr	r31
beq_else.21518:
	lwi	r5, r3 2
	sli	r7, r5, 0
	lwi	r8, r3 1
	add	r0 r8 r7
	swi	r1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 5
	add	r5, r5, r1
	lwi	r28, r3 4
	swi	r5, r3 8
	addi	r1, r2 0 #
	swi	r31 r3 9
	addi	r3, r3, 10
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -10
	lwi	r31, r3 9
	lwi	r2, r3 3
	sli	r2, r2, 0
	add	r2 r1 r2
	lwi	r2, r2 0
	lwi	r5, r3 6
	seq	r30, r2, r5
	jeql	r30, r0, beq_else.21520
	jr	r31
beq_else.21520:
	lwi	r2, r3 8
	sli	r5, r2, 0
	lwi	r6, r3 1
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 5
	add	r1, r2, r1
	lwi	r28, r3 0
	lwi	r29, r28 0
	jr	r29
solver_rect_surface.2776:
	lwi	r8, r28 3
	lwi	r9, r28 2
	flwi	f4, r28 1 #
	sli	r10, r5, 0
	add	r0 r2 r10
	flwi	f5, r0 0
	sub	r0 r0 r0
	feq	r30, f5, f4
	jeql	r30, r0, beq_else.21522
	addi	r10, r0 1
	jl	beq_cont.21523
beq_else.21522:
	addi	r10, r0 0
beq_cont.21523:
	seq	r30, r10, r9
	jeql	r30, r0, beq_else.21524
	lwi	r10, r1 4
	lwi	r1, r1 6
	sli	r11, r5, 0
	add	r0 r2 r11
	flwi	f5, r0 0
	sub	r0 r0 r0
	fgt	r30 f4 f5
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21525
	addi	r11, r0 0
	jl	ble_cont.21526
ble_else.21525:
	addi	r11, r0 1
ble_cont.21526:
	seq	r30, r1, r9
	jeql	r30, r0, beq_else.21527
	addi	r1, r11 0
	jl	beq_cont.21528
beq_else.21527:
	seq	r30, r11, r9
	jeql	r30, r0, beq_else.21529
	addi	r1, r0 1
	jl	beq_cont.21530
beq_else.21529:
	addi	r1, r0 0
beq_cont.21530:
beq_cont.21528:
	sli	r11, r5, 0
	add	r0 r10 r11
	flwi	f5, r0 0
	sub	r0 r0 r0
	seq	r30, r1, r9
	jeql	r30, r0, beq_else.21531
	flwl	%f6 l.11783
	fsub	f5, f6, f5
	jl	beq_cont.21532
beq_else.21531:
beq_cont.21532:
	fsub	f1, f5, f1
	sli	r1, r5, 0
	add	r0 r2 r1
	flwi	f5, r0 0
	sub	r0 r0 r0
	fdiv	f1, f1, f5
	sli	r1, r6, 0
	add	r0 r2 r1
	flwi	f5, r0 0
	sub	r0 r0 r0
	fmul	f5, f1, f5
	fadd	f2, f5, f2
	fgt	r30 f4 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21533
	jl	ble_cont.21534
ble_else.21533:
	fsub	f2, f4, f2
ble_cont.21534:
	sli	r1, r6, 0
	add	r0 r10 r1
	flwi	f5, r0 0
	sub	r0 r0 r0
	fgt	r30 f5 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21535
	addi	r1, r0 0
	jl	ble_cont.21536
ble_else.21535:
	addi	r1, r0 1
ble_cont.21536:
	seq	r30, r1, r9
	jeql	r30, r0, beq_else.21537
	addi	r1, r0 0
	jr	r31
beq_else.21537:
	sli	r1, r7, 0
	add	r0 r2 r1
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f2, f1, f2
	fadd	f2, f2, f3
	fgt	r30 f4 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21538
	jl	ble_cont.21539
ble_else.21538:
	fsub	f2, f4, f2
ble_cont.21539:
	sli	r1, r7, 0
	add	r0 r10 r1
	flwi	f3, r0 0
	sub	r0 r0 r0
	fgt	r30 f3 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21540
	addi	r1, r0 0
	jl	ble_cont.21541
ble_else.21540:
	addi	r1, r0 1
ble_cont.21541:
	seq	r30, r1, r9
	jeql	r30, r0, beq_else.21542
	addi	r1, r0 0
	jr	r31
beq_else.21542:
	sli	r1, r9, 0
	add	r0 r8 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	addi	r1, r0 1
	jr	r31
beq_else.21524:
	addi	r1, r0 0
	jr	r31
quadratic.2797:
	lwi	r2, r28 1
	fmul	f4, f1, f1
	lwi	r5, r1 4
	flwi	f5, r5 0 #
	fmul	f4, f4, f5
	fmul	f5, f2, f2
	lwi	r5, r1 4
	flwi	f6, r5 1 #
	fmul	f5, f5, f6
	fadd	f4, f4, f5
	fmul	f5, f3, f3
	lwi	r5, r1 4
	flwi	f6, r5 2 #
	fmul	f5, f5, f6
	fadd	f4, f4, f5
	lwi	r5, r1 3
	seq	r30, r5, r2
	jeql	r30, r0, beq_else.21543
	fadd	f1, f4 f0
	jr	r31
beq_else.21543:
	fmul	f5, f2, f3
	lwi	r2, r1 9
	flwi	f6, r2 0 #
	fmul	f5, f5, f6
	fadd	f4, f4, f5
	fmul	f3, f3, f1
	lwi	r2, r1 9
	flwi	f5, r2 1 #
	fmul	f3, f3, f5
	fadd	f3, f4, f3
	fmul	f1, f1, f2
	lwi	r1, r1 9
	flwi	f2, r1 2 #
	fmul	f1, f1, f2
	fadd	f1, f3, f1
	jr	r31
bilinear.2802:
	lwi	r2, r28 1
	fmul	f7, f1, f4
	lwi	r5, r1 4
	flwi	f8, r5 0 #
	fmul	f7, f7, f8
	fmul	f8, f2, f5
	lwi	r5, r1 4
	flwi	f9, r5 1 #
	fmul	f8, f8, f9
	fadd	f7, f7, f8
	fmul	f8, f3, f6
	lwi	r5, r1 4
	flwi	f9, r5 2 #
	fmul	f8, f8, f9
	fadd	f7, f7, f8
	lwi	r5, r1 3
	seq	r30, r5, r2
	jeql	r30, r0, beq_else.21544
	fadd	f1, f7 f0
	jr	r31
beq_else.21544:
	fmul	f8, f3, f5
	fmul	f9, f2, f6
	fadd	f8, f8, f9
	lwi	r2, r1 9
	flwi	f9, r2 0 #
	fmul	f8, f8, f9
	fmul	f6, f1, f6
	fmul	f3, f3, f4
	fadd	f3, f6, f3
	lwi	r2, r1 9
	flwi	f6, r2 1 #
	fmul	f3, f3, f6
	fadd	f3, f8, f3
	fmul	f1, f1, f5
	fmul	f2, f2, f4
	fadd	f1, f1, f2
	lwi	r1, r1 9
	flwi	f2, r1 2 #
	fmul	f1, f1, f2
	fadd	f1, f3, f1
	flwl	%f2 l.11779
	fmul	f1, f1, f2
	fadd	f1, f7, f1
	jr	r31
solver_second.2810:
	lwi	r5, r28 8
	lwi	r6, r28 7
	lwi	r7, r28 6
	lwi	r8, r28 5
	lwi	r9, r28 4
	lwi	r10, r28 3
	lwi	r11, r28 2
	flwi	f4, r28 1 #
	sli	r12, r10, 0
	add	r0 r2 r12
	flwi	f5, r0 0
	sub	r0 r0 r0
	sli	r12, r11, 0
	add	r0 r2 r12
	flwi	f6, r0 0
	sub	r0 r0 r0
	sli	r12, r8, 0
	add	r0 r2 r12
	flwi	f7, r0 0
	sub	r0 r0 r0
	swi	r5, r3 0
	swi	r9, r3 1
	swi	r6, r3 2
	fswi	f3, r3 4
	fswi	f2, r3 6
	fswi	f1, r3 8
	swi	r1, r3 9
	swi	r7, r3 10
	swi	r8, r3 11
	swi	r11, r3 12
	swi	r2, r3 13
	swi	r10, r3 14
	fswi	f4, r3 16
	addi	r28, r6 0 #
	fadd	f3, f7 f0
	fadd	f2, f6 f0
	fadd	f1, f5 f0
	swi	r31 r3 17
	addi	r3, r3, 18
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -18
	lwi	r31, r3 17
	flwi	f2, r3 16 #off
	feq	r30, f1, f2
	jeql	r30, r0, beq_else.21549
	addi	r1, r0 1
	jl	beq_cont.21550
beq_else.21549:
	addi	r1, r0 0
beq_cont.21550:
	lwi	r2, r3 14
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21551
	sli	r1, r2, 0
	lwi	r5, r3 13
	add	r0 r5 r1
	flwi	f3, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 12
	sli	r1, r1, 0
	add	r0 r5 r1
	flwi	f4, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 11
	sli	r1, r1, 0
	add	r0 r5 r1
	flwi	f5, r0 0
	sub	r0 r0 r0
	flwi	f6, r3 8 #off
	flwi	f7, r3 6 #off
	flwi	f8, r3 4 #off
	lwi	r1, r3 9
	lwi	r28, r3 10
	fswi	f1, r3 18
	fadd	f2, f4 f0
	fadd	f1, f3 f0
	fadd	f4, f6 f0
	fadd	f3, f5 f0
	fadd	f6, f8 f0
	fadd	f5, f7 f0
	swi	r31 r3 19
	addi	r3, r3, 20
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -20
	lwi	r31, r3 19
	flwi	f2, r3 8 #off
	flwi	f3, r3 6 #off
	flwi	f4, r3 4 #off
	lwi	r1, r3 9
	lwi	r28, r3 2
	fswi	f1, r3 20
	fadd	f1, f2 f0
	fadd	f2, f3 f0
	fadd	f3, f4 f0
	swi	r31 r3 21
	addi	r3, r3, 22
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -22
	lwi	r31, r3 21
	lwi	r1, r3 9
	lwi	r2, r1 1
	lwi	r5, r3 1
	seq	r30, r2, r5
	jeql	r30, r0, beq_else.21554
	flwl	%f2 l.11794
	fsub	f1, f1, f2
	jl	beq_cont.21555
beq_else.21554:
beq_cont.21555:
	flwi	f2, r3 20 #off
	fmul	f3, f2, f2
	flwi	f4, r3 18 #off
	fmul	f1, f4, f1
	fsub	f1, f3, f1
	flwi	f3, r3 16 #off
	fgt	r30 f1 f3
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21556
	addi	r2, r0 0
	jl	ble_cont.21557
ble_else.21556:
	addi	r2, r0 1
ble_cont.21557:
	lwi	r5, r3 14
	seq	r30, r2, r5
	jeql	r30, r0, beq_else.21558
	addi	r1, r0 0
	jr	r31
beq_else.21558:
	swi	r31 r3 21
	addi	r3, r3, 22
	jal	min_caml_sqrt
	addi	r3, r3, -22
	lwi	r31 r3 21
	lwi	r1, r3 9
	lwi	r1, r1 6
	lwi	r2, r3 14
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21559
	flwi	f2, r3 16 #off
	fsub	f1, f2, f1
	jl	beq_cont.21560
beq_else.21559:
beq_cont.21560:
	flwi	f2, r3 20 #off
	fsub	f1, f1, f2
	flwi	f2, r3 18 #off
	fdiv	f1, f1, f2
	sli	r1, r2, 0
	lwi	r2, r3 0
	add	r0 r2 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	addi	r1, r0 1
	jr	r31
beq_else.21551:
	addi	r1, r0 0
	jr	r31
solver.2816:
	lwi	r6, r28 7
	lwi	r7, r28 6
	lwi	r8, r28 5
	lwi	r9, r28 4
	lwi	r10, r28 3
	lwi	r11, r28 2
	lwi	r12, r28 1
	sli	r1, r1, 0
	add	r1 r9 r1
	lwi	r1, r1 0
	sli	r9, r11, 0
	add	r0 r5 r9
	flwi	f1, r0 0
	sub	r0 r0 r0
	lwi	r9, r1 5
	flwi	f2, r9 0 #
	fsub	f1, f1, f2
	sli	r9, r12, 0
	add	r0 r5 r9
	flwi	f2, r0 0
	sub	r0 r0 r0
	lwi	r9, r1 5
	flwi	f3, r9 1 #
	fsub	f2, f2, f3
	sli	r9, r10, 0
	add	r0 r5 r9
	flwi	f3, r0 0
	sub	r0 r0 r0
	lwi	r5, r1 5
	flwi	f4, r5 2 #
	fsub	f3, f3, f4
	lwi	r5, r1 1
	seq	r30, r5, r12
	jeql	r30, r0, beq_else.21561
	fswi	f1, r3 0
	fswi	f3, r3 2
	fswi	f2, r3 4
	swi	r10, r3 5
	swi	r12, r3 6
	swi	r2, r3 7
	swi	r1, r3 8
	swi	r7, r3 9
	swi	r11, r3 10
	addi	r6, r12 0 #
	addi	r5, r11 0 #
	addi	r28, r7 0 #
	addi	r7, r10 0 #
	swi	r31 r3 11
	addi	r3, r3, 12
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -12
	lwi	r31, r3 11
	lwi	r7, r3 10
	seq	r30, r1, r7
	jeql	r30, r0, beq_else.21564
	flwi	f1, r3 4 #off
	flwi	f2, r3 2 #off
	flwi	f3, r3 0 #off
	lwi	r1, r3 8
	lwi	r2, r3 7
	lwi	r5, r3 6
	lwi	r6, r3 5
	lwi	r28, r3 9
	swi	r31 r3 11
	addi	r3, r3, 12
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -12
	lwi	r31, r3 11
	lwi	r6, r3 10
	seq	r30, r1, r6
	jeql	r30, r0, beq_else.21565
	flwi	f1, r3 2 #off
	flwi	f2, r3 0 #off
	flwi	f3, r3 4 #off
	lwi	r1, r3 8
	lwi	r2, r3 7
	lwi	r5, r3 5
	lwi	r7, r3 6
	lwi	r28, r3 9
	swi	r31 r3 11
	addi	r3, r3, 12
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -12
	lwi	r31, r3 11
	lwi	r2, r3 10
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21566
	addi	r1, r0 0
	jr	r31
beq_else.21566:
	addi	r1, r0 3
	jr	r31
beq_else.21565:
	addi	r1, r0 2
	jr	r31
beq_else.21564:
	addi	r1, r0 1
	jr	r31
beq_else.21561:
	seq	r30, r5, r10
	jeql	r30, r0, beq_else.21567
	lwi	r1, r1 4
	sli	r5, r11, 0
	add	r0 r2 r5
	flwi	f4, r0 0
	sub	r0 r0 r0
	sli	r5, r11, 0
	add	r0 r1 r5
	flwi	f5, r0 0
	sub	r0 r0 r0
	fmul	f4, f4, f5
	sli	r5, r12, 0
	add	r0 r2 r5
	flwi	f5, r0 0
	sub	r0 r0 r0
	sli	r5, r12, 0
	add	r0 r1 r5
	flwi	f6, r0 0
	sub	r0 r0 r0
	fmul	f5, f5, f6
	fadd	f4, f4, f5
	sli	r5, r10, 0
	add	r0 r2 r5
	flwi	f5, r0 0
	sub	r0 r0 r0
	sli	r2, r10, 0
	add	r0 r1 r2
	flwi	f6, r0 0
	sub	r0 r0 r0
	fmul	f5, f5, f6
	fadd	f4, f4, f5
	flwl	%f5 l.11783
	fgt	r30 f4 f5
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21568
	addi	r2, r0 0
	jl	ble_cont.21569
ble_else.21568:
	addi	r2, r0 1
ble_cont.21569:
	seq	r30, r2, r11
	jeql	r30, r0, beq_else.21570
	addi	r1, r0 0
	jr	r31
beq_else.21570:
	sli	r2, r11, 0
	add	r0 r1 r2
	flwi	f5, r0 0
	sub	r0 r0 r0
	fmul	f1, f5, f1
	sli	r2, r12, 0
	add	r0 r1 r2
	flwi	f5, r0 0
	sub	r0 r0 r0
	fmul	f2, f5, f2
	fadd	f1, f1, f2
	sli	r2, r10, 0
	add	r0 r1 r2
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	flwl	%f2 l.11783
	fsub	f1, f2, f1
	fdiv	f1, f1, f4
	sli	r1, r11, 0
	add	r0 r8 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	addi	r1, r0 1
	jr	r31
beq_else.21567:
	addi	r28, r6 0 #
	lwi	r29, r28 0
	jr	r29
solver_rect_fast.2820:
	lwi	r6, r28 7
	lwi	r7, r28 6
	lwi	r8, r28 5
	lwi	r9, r28 4
	lwi	r10, r28 3
	lwi	r11, r28 2
	flwi	f4, r28 1 #
	sli	r12, r10, 0
	add	r0 r5 r12
	flwi	f5, r0 0
	sub	r0 r0 r0
	fsub	f5, f5, f1
	sli	r12, r11, 0
	add	r0 r5 r12
	flwi	f6, r0 0
	sub	r0 r0 r0
	fmul	f5, f5, f6
	sli	r12, r11, 0
	add	r0 r2 r12
	flwi	f6, r0 0
	sub	r0 r0 r0
	fmul	f6, f5, f6
	fadd	f6, f6, f2
	fgt	r30 f4 f6
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21571
	jl	ble_cont.21572
ble_else.21571:
	fsub	f6, f4, f6
ble_cont.21572:
	lwi	r12, r1 4
	flwi	f7, r12 1 #
	fgt	r30 f7 f6
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21573
	addi	r12, r0 0
	jl	ble_cont.21574
ble_else.21573:
	addi	r12, r0 1
ble_cont.21574:
	seq	r30, r12, r10
	jeql	r30, r0, beq_else.21575
	addi	r12, r0 0
	jl	beq_cont.21576
beq_else.21575:
	sli	r12, r8, 0
	add	r0 r2 r12
	flwi	f6, r0 0
	sub	r0 r0 r0
	fmul	f6, f5, f6
	fadd	f6, f6, f3
	fgt	r30 f4 f6
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21577
	jl	ble_cont.21578
ble_else.21577:
	fsub	f6, f4, f6
ble_cont.21578:
	lwi	r12, r1 4
	flwi	f7, r12 2 #
	fgt	r30 f7 f6
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21579
	addi	r12, r0 0
	jl	ble_cont.21580
ble_else.21579:
	addi	r12, r0 1
ble_cont.21580:
	seq	r30, r12, r10
	jeql	r30, r0, beq_else.21581
	addi	r12, r0 0
	jl	beq_cont.21582
beq_else.21581:
	sli	r12, r11, 0
	add	r0 r5 r12
	flwi	f6, r0 0
	sub	r0 r0 r0
	feq	r30, f6, f4
	jeql	r30, r0, beq_else.21583
	addi	r12, r0 1
	jl	beq_cont.21584
beq_else.21583:
	addi	r12, r0 0
beq_cont.21584:
	seq	r30, r12, r10
	jeql	r30, r0, beq_else.21585
	addi	r12, r0 1
	jl	beq_cont.21586
beq_else.21585:
	addi	r12, r0 0
beq_cont.21586:
beq_cont.21582:
beq_cont.21576:
	seq	r30, r12, r10
	jeql	r30, r0, beq_else.21587
	sli	r12, r8, 0
	add	r0 r5 r12
	flwi	f5, r0 0
	sub	r0 r0 r0
	fsub	f5, f5, f2
	sli	r12, r9, 0
	add	r0 r5 r12
	flwi	f6, r0 0
	sub	r0 r0 r0
	fmul	f5, f5, f6
	sli	r12, r10, 0
	add	r0 r2 r12
	flwi	f6, r0 0
	sub	r0 r0 r0
	fmul	f6, f5, f6
	fadd	f6, f6, f1
	fgt	r30 f4 f6
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21588
	jl	ble_cont.21589
ble_else.21588:
	fsub	f6, f4, f6
ble_cont.21589:
	lwi	r12, r1 4
	flwi	f7, r12 0 #
	fgt	r30 f7 f6
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21590
	addi	r12, r0 0
	jl	ble_cont.21591
ble_else.21590:
	addi	r12, r0 1
ble_cont.21591:
	seq	r30, r12, r10
	jeql	r30, r0, beq_else.21592
	addi	r8, r0 0
	jl	beq_cont.21593
beq_else.21592:
	sli	r8, r8, 0
	add	r0 r2 r8
	flwi	f6, r0 0
	sub	r0 r0 r0
	fmul	f6, f5, f6
	fadd	f6, f6, f3
	fgt	r30 f4 f6
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21594
	jl	ble_cont.21595
ble_else.21594:
	fsub	f6, f4, f6
ble_cont.21595:
	lwi	r8, r1 4
	flwi	f7, r8 2 #
	fgt	r30 f7 f6
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21596
	addi	r8, r0 0
	jl	ble_cont.21597
ble_else.21596:
	addi	r8, r0 1
ble_cont.21597:
	seq	r30, r8, r10
	jeql	r30, r0, beq_else.21598
	addi	r8, r0 0
	jl	beq_cont.21599
beq_else.21598:
	sli	r8, r9, 0
	add	r0 r5 r8
	flwi	f6, r0 0
	sub	r0 r0 r0
	feq	r30, f6, f4
	jeql	r30, r0, beq_else.21600
	addi	r8, r0 1
	jl	beq_cont.21601
beq_else.21600:
	addi	r8, r0 0
beq_cont.21601:
	seq	r30, r8, r10
	jeql	r30, r0, beq_else.21602
	addi	r8, r0 1
	jl	beq_cont.21603
beq_else.21602:
	addi	r8, r0 0
beq_cont.21603:
beq_cont.21599:
beq_cont.21593:
	seq	r30, r8, r10
	jeql	r30, r0, beq_else.21604
	flwi	f5, r5 4 #
	fsub	f3, f5, f3
	sli	r8, r7, 0
	add	r0 r5 r8
	flwi	f5, r0 0
	sub	r0 r0 r0
	fmul	f3, f3, f5
	sli	r8, r10, 0
	add	r0 r2 r8
	flwi	f5, r0 0
	sub	r0 r0 r0
	fmul	f5, f3, f5
	fadd	f1, f5, f1
	fgt	r30 f4 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21605
	jl	ble_cont.21606
ble_else.21605:
	fsub	f1, f4, f1
ble_cont.21606:
	lwi	r8, r1 4
	flwi	f5, r8 0 #
	fgt	r30 f5 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21607
	addi	r8, r0 0
	jl	ble_cont.21608
ble_else.21607:
	addi	r8, r0 1
ble_cont.21608:
	seq	r30, r8, r10
	jeql	r30, r0, beq_else.21609
	addi	r1, r0 0
	jl	beq_cont.21610
beq_else.21609:
	sli	r8, r11, 0
	add	r0 r2 r8
	flwi	f1, r0 0
	sub	r0 r0 r0
	fmul	f1, f3, f1
	fadd	f1, f1, f2
	fgt	r30 f4 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21611
	jl	ble_cont.21612
ble_else.21611:
	fsub	f1, f4, f1
ble_cont.21612:
	lwi	r1, r1 4
	flwi	f2, r1 1 #
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21613
	addi	r1, r0 0
	jl	ble_cont.21614
ble_else.21613:
	addi	r1, r0 1
ble_cont.21614:
	seq	r30, r1, r10
	jeql	r30, r0, beq_else.21615
	addi	r1, r0 0
	jl	beq_cont.21616
beq_else.21615:
	sli	r1, r7, 0
	add	r0 r5 r1
	flwi	f1, r0 0
	sub	r0 r0 r0
	feq	r30, f1, f4
	jeql	r30, r0, beq_else.21617
	addi	r1, r0 1
	jl	beq_cont.21618
beq_else.21617:
	addi	r1, r0 0
beq_cont.21618:
	seq	r30, r1, r10
	jeql	r30, r0, beq_else.21619
	addi	r1, r0 1
	jl	beq_cont.21620
beq_else.21619:
	addi	r1, r0 0
beq_cont.21620:
beq_cont.21616:
beq_cont.21610:
	seq	r30, r1, r10
	jeql	r30, r0, beq_else.21621
	addi	r1, r0 0
	jr	r31
beq_else.21621:
	sli	r1, r10, 0
	add	r0 r6 r1
	fswi	f3, r0 0
	sub	r0 r0 r0
	addi	r1, r0 3
	jr	r31
beq_else.21604:
	sli	r1, r10, 0
	add	r0 r6 r1
	fswi	f5, r0 0
	sub	r0 r0 r0
	addi	r1, r0 2
	jr	r31
beq_else.21587:
	sli	r1, r10, 0
	add	r0 r6 r1
	fswi	f5, r0 0
	sub	r0 r0 r0
	addi	r1, r0 1
	jr	r31
solver_second_fast.2833:
	lwi	r5, r28 7
	lwi	r6, r28 6
	lwi	r7, r28 5
	lwi	r8, r28 4
	lwi	r9, r28 3
	lwi	r10, r28 2
	flwi	f4, r28 1 #
	sli	r11, r9, 0
	add	r0 r2 r11
	flwi	f5, r0 0
	sub	r0 r0 r0
	feq	r30, f5, f4
	jeql	r30, r0, beq_else.21622
	addi	r11, r0 1
	jl	beq_cont.21623
beq_else.21622:
	addi	r11, r0 0
beq_cont.21623:
	seq	r30, r11, r9
	jeql	r30, r0, beq_else.21624
	sli	r10, r10, 0
	add	r0 r2 r10
	flwi	f6, r0 0
	sub	r0 r0 r0
	fmul	f6, f6, f1
	sli	r7, r7, 0
	add	r0 r2 r7
	flwi	f7, r0 0
	sub	r0 r0 r0
	fmul	f7, f7, f2
	fadd	f6, f6, f7
	sli	r7, r8, 0
	add	r0 r2 r7
	flwi	f7, r0 0
	sub	r0 r0 r0
	fmul	f7, f7, f3
	fadd	f6, f6, f7
	swi	r5, r3 0
	swi	r2, r3 1
	swi	r9, r3 2
	fswi	f4, r3 4
	fswi	f5, r3 6
	fswi	f6, r3 8
	swi	r8, r3 9
	swi	r1, r3 10
	addi	r28, r6 0 #
	swi	r31 r3 11
	addi	r3, r3, 12
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -12
	lwi	r31, r3 11
	lwi	r1, r3 10
	lwi	r2, r1 1
	lwi	r5, r3 9
	seq	r30, r2, r5
	jeql	r30, r0, beq_else.21628
	flwl	%f2 l.11794
	fsub	f1, f1, f2
	jl	beq_cont.21629
beq_else.21628:
beq_cont.21629:
	flwi	f2, r3 8 #off
	fmul	f3, f2, f2
	flwi	f4, r3 6 #off
	fmul	f1, f4, f1
	fsub	f1, f3, f1
	flwi	f3, r3 4 #off
	fgt	r30 f1 f3
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21630
	addi	r2, r0 0
	jl	ble_cont.21631
ble_else.21630:
	addi	r2, r0 1
ble_cont.21631:
	lwi	r5, r3 2
	seq	r30, r2, r5
	jeql	r30, r0, beq_else.21632
	addi	r1, r0 0
	jr	r31
beq_else.21632:
	lwi	r1, r1 6
	seq	r30, r1, r5
	jeql	r30, r0, beq_else.21633
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	min_caml_sqrt
	addi	r3, r3, -12
	lwi	r31 r3 11
	flwi	f2, r3 8 #off
	fsub	f1, f2, f1
	lwi	r1, r3 1
	flwi	f2, r1 4 #
	fmul	f1, f1, f2
	lwi	r1, r3 2
	sli	r1, r1, 0
	lwi	r2, r3 0
	add	r0 r2 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.21634
beq_else.21633:
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	min_caml_sqrt
	addi	r3, r3, -12
	lwi	r31 r3 11
	flwi	f2, r3 8 #off
	fadd	f1, f2, f1
	lwi	r1, r3 1
	flwi	f2, r1 4 #
	fmul	f1, f1, f2
	lwi	r1, r3 2
	sli	r1, r1, 0
	lwi	r2, r3 0
	add	r0 r2 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
beq_cont.21634:
	addi	r1, r0 1
	jr	r31
beq_else.21624:
	addi	r1, r0 0
	jr	r31
solver_fast.2839:
	lwi	r6, r28 9
	lwi	r7, r28 8
	lwi	r8, r28 7
	lwi	r9, r28 6
	lwi	r10, r28 5
	lwi	r11, r28 4
	lwi	r12, r28 3
	lwi	r13, r28 2
	flwi	f1, r28 1 #
	sli	r14, r1, 0
	add	r9 r9 r14
	lwi	r9, r9 0
	sli	r14, r12, 0
	add	r0 r5 r14
	flwi	f2, r0 0
	sub	r0 r0 r0
	lwi	r14, r9 5
	flwi	f3, r14 0 #
	fsub	f2, f2, f3
	sli	r14, r13, 0
	add	r0 r5 r14
	flwi	f3, r0 0
	sub	r0 r0 r0
	lwi	r14, r9 5
	flwi	f4, r14 1 #
	fsub	f3, f3, f4
	sli	r14, r10, 0
	add	r0 r5 r14
	flwi	f4, r0 0
	sub	r0 r0 r0
	lwi	r5, r9 5
	flwi	f5, r5 2 #
	fsub	f4, f4, f5
	lwi	r5, r2 1
	sli	r1, r1, 0
	add	r5 r5 r1
	lwi	r5, r5 0
	lwi	r1, r9 1
	seq	r30, r1, r13
	jeql	r30, r0, beq_else.21635
	lwi	r2, r2 0
	addi	r1, r9 0 #
	addi	r28, r7 0 #
	fadd	f1, f2 f0
	fadd	f2, f3 f0
	fadd	f3, f4 f0
	lwi	r29, r28 0
	jr	r29
beq_else.21635:
	seq	r30, r1, r10
	jeql	r30, r0, beq_else.21636
	sli	r1, r12, 0
	add	r0 r5 r1
	flwi	f5, r0 0
	sub	r0 r0 r0
	fgt	r30 f1 f5
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21637
	addi	r1, r0 0
	jl	ble_cont.21638
ble_else.21637:
	addi	r1, r0 1
ble_cont.21638:
	seq	r30, r1, r12
	jeql	r30, r0, beq_else.21639
	addi	r1, r0 0
	jr	r31
beq_else.21639:
	sli	r1, r13, 0
	add	r0 r5 r1
	flwi	f1, r0 0
	sub	r0 r0 r0
	fmul	f1, f1, f2
	sli	r1, r10, 0
	add	r0 r5 r1
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	sli	r1, r11, 0
	add	r0 r5 r1
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f4
	fadd	f1, f1, f2
	sli	r1, r12, 0
	add	r0 r8 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	addi	r1, r0 1
	jr	r31
beq_else.21636:
	addi	r2, r5 0 #
	addi	r1, r9 0 #
	addi	r28, r6 0 #
	fadd	f1, f2 f0
	fadd	f2, f3 f0
	fadd	f3, f4 f0
	lwi	r29, r28 0
	jr	r29
solver_second_fast2.2850:
	lwi	r6, r28 6
	lwi	r7, r28 5
	lwi	r8, r28 4
	lwi	r9, r28 3
	lwi	r10, r28 2
	flwi	f4, r28 1 #
	sli	r11, r9, 0
	add	r0 r2 r11
	flwi	f5, r0 0
	sub	r0 r0 r0
	feq	r30, f5, f4
	jeql	r30, r0, beq_else.21640
	addi	r11, r0 1
	jl	beq_cont.21641
beq_else.21640:
	addi	r11, r0 0
beq_cont.21641:
	seq	r30, r11, r9
	jeql	r30, r0, beq_else.21642
	sli	r10, r10, 0
	add	r0 r2 r10
	flwi	f6, r0 0
	sub	r0 r0 r0
	fmul	f1, f6, f1
	sli	r7, r7, 0
	add	r0 r2 r7
	flwi	f6, r0 0
	sub	r0 r0 r0
	fmul	f2, f6, f2
	fadd	f1, f1, f2
	sli	r7, r8, 0
	add	r0 r2 r7
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	sli	r7, r8, 0
	add	r0 r5 r7
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f3, f1, f1
	fmul	f2, f5, f2
	fsub	f2, f3, f2
	fgt	r30 f2 f4
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21643
	addi	r5, r0 0
	jl	ble_cont.21644
ble_else.21643:
	addi	r5, r0 1
ble_cont.21644:
	seq	r30, r5, r9
	jeql	r30, r0, beq_else.21645
	addi	r1, r0 0
	jr	r31
beq_else.21645:
	lwi	r1, r1 6
	seq	r30, r1, r9
	jeql	r30, r0, beq_else.21646
	swi	r6, r3 0
	swi	r9, r3 1
	swi	r2, r3 2
	fswi	f1, r3 4
	fadd	f1, f2 f0
	swi	r31 r3 5
	addi	r3, r3, 6
	jal	min_caml_sqrt
	addi	r3, r3, -6
	lwi	r31 r3 5
	flwi	f2, r3 4 #off
	fsub	f1, f2, f1
	lwi	r1, r3 2
	flwi	f2, r1 4 #
	fmul	f1, f1, f2
	lwi	r1, r3 1
	sli	r1, r1, 0
	lwi	r2, r3 0
	add	r0 r2 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.21647
beq_else.21646:
	swi	r6, r3 0
	swi	r9, r3 1
	swi	r2, r3 2
	fswi	f1, r3 4
	fadd	f1, f2 f0
	swi	r31 r3 7
	addi	r3, r3, 8
	jal	min_caml_sqrt
	addi	r3, r3, -8
	lwi	r31 r3 7
	flwi	f2, r3 4 #off
	fadd	f1, f2, f1
	lwi	r1, r3 2
	flwi	f2, r1 4 #
	fmul	f1, f1, f2
	lwi	r1, r3 1
	sli	r1, r1, 0
	lwi	r2, r3 0
	add	r0 r2 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
beq_cont.21647:
	addi	r1, r0 1
	jr	r31
beq_else.21642:
	addi	r1, r0 0
	jr	r31
solver_fast2.2857:
	lwi	r5, r28 8
	lwi	r6, r28 7
	lwi	r7, r28 6
	lwi	r8, r28 5
	lwi	r9, r28 4
	lwi	r10, r28 3
	lwi	r11, r28 2
	lwi	r12, r28 1
	sli	r13, r1, 0
	add	r8 r8 r13
	lwi	r8, r8 0
	lwi	r13, r8 10
	sli	r14, r11, 0
	add	r0 r13 r14
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r14, r12, 0
	add	r0 r13 r14
	flwi	f2, r0 0
	sub	r0 r0 r0
	sli	r14, r9, 0
	add	r0 r13 r14
	flwi	f3, r0 0
	sub	r0 r0 r0
	lwi	r14, r2 1
	sli	r1, r1, 0
	add	r1 r14 r1
	lwi	r1, r1 0
	lwi	r14, r8 1
	seq	r30, r14, r12
	jeql	r30, r0, beq_else.21650
	lwi	r2, r2 0
	addi	r5, r1 0 #
	addi	r28, r6 0 #
	addi	r1, r8 0 #
	lwi	r29, r28 0
	jr	r29
beq_else.21650:
	seq	r30, r14, r9
	jeql	r30, r0, beq_else.21651
	sli	r2, r11, 0
	add	r0 r1 r2
	flwi	f1, r0 0
	sub	r0 r0 r0
	flwl	%f2 l.11783
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21652
	addi	r2, r0 0
	jl	ble_cont.21653
ble_else.21652:
	addi	r2, r0 1
ble_cont.21653:
	seq	r30, r2, r11
	jeql	r30, r0, beq_else.21654
	addi	r1, r0 0
	jr	r31
beq_else.21654:
	sli	r2, r11, 0
	add	r0 r1 r2
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r1, r10, 0
	add	r0 r13 r1
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f1, f1, f2
	sli	r1, r11, 0
	add	r0 r7 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	addi	r1, r0 1
	jr	r31
beq_else.21651:
	addi	r2, r1 0 #
	addi	r28, r5 0 #
	addi	r5, r13 0 #
	addi	r1, r8 0 #
	lwi	r29, r28 0
	jr	r29
setup_rect_table.2860:
	lwi	r5, r28 6
	lwi	r6, r28 5
	lwi	r7, r28 4
	lwi	r8, r28 3
	lwi	r9, r28 2
	flwi	f1, r28 1 #
	addi	r10, r0 6
	swi	r5, r3 0
	swi	r7, r3 1
	swi	r6, r3 2
	swi	r9, r3 3
	swi	r2, r3 4
	fswi	f1, r3 6
	swi	r1, r3 7
	swi	r8, r3 8
	addi	r1, r10 0 #
	swi	r31 r3 9
	addi	r3, r3, 10
	jal	min_caml_create_float_array
	addi	r3, r3, -10
	lwi	r31 r3 9
	lwi	r2, r3 8
	sli	r5, r2, 0
	lwi	r6, r3 7
	add	r0 r6 r5
	flwi	f1, r0 0
	sub	r0 r0 r0
	flwi	f2, r3 6 #off
	feq	r30, f1, f2
	jeql	r30, r0, beq_else.21656
	addi	r5, r0 1
	jl	beq_cont.21657
beq_else.21656:
	addi	r5, r0 0
beq_cont.21657:
	seq	r30, r5, r2
	jeql	r30, r0, beq_else.21658
	lwi	r5, r3 4
	lwi	r7, r5 6
	sli	r8, r2, 0
	add	r0 r6 r8
	flwi	f1, r0 0
	sub	r0 r0 r0
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21660
	addi	r8, r0 0
	jl	ble_cont.21661
ble_else.21660:
	addi	r8, r0 1
ble_cont.21661:
	seq	r30, r7, r2
	jeql	r30, r0, beq_else.21662
	addi	r7, r8 0
	jl	beq_cont.21663
beq_else.21662:
	seq	r30, r8, r2
	jeql	r30, r0, beq_else.21664
	addi	r7, r0 1
	jl	beq_cont.21665
beq_else.21664:
	addi	r7, r0 0
beq_cont.21665:
beq_cont.21663:
	lwi	r8, r5 4
	flwi	f1, r8 0 #
	seq	r30, r7, r2
	jeql	r30, r0, beq_else.21666
	flwl	%f3 l.11783
	fsub	f1, f3, f1
	jl	beq_cont.21667
beq_else.21666:
beq_cont.21667:
	sli	r7, r2, 0
	add	r0 r1 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	flwl	%f1 l.11794
	sli	r7, r2, 0
	add	r0 r6 r7
	flwi	f3, r0 0
	sub	r0 r0 r0
	fdiv	f1, f1, f3
	lwi	r7, r3 3
	sli	r8, r7, 0
	add	r0 r1 r8
	fswi	f1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.21659
beq_else.21658:
	lwi	r5, r3 3
	sli	r7, r5, 0
	add	r0 r1 r7
	fswi	f2, r0 0
	sub	r0 r0 r0
beq_cont.21659:
	lwi	r5, r3 3
	sli	r7, r5, 0
	add	r0 r6 r7
	flwi	f1, r0 0
	sub	r0 r0 r0
	feq	r30, f1, f2
	jeql	r30, r0, beq_else.21668
	addi	r7, r0 1
	jl	beq_cont.21669
beq_else.21668:
	addi	r7, r0 0
beq_cont.21669:
	seq	r30, r7, r2
	jeql	r30, r0, beq_else.21670
	lwi	r7, r3 4
	lwi	r8, r7 6
	sli	r9, r5, 0
	add	r0 r6 r9
	flwi	f1, r0 0
	sub	r0 r0 r0
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21672
	addi	r9, r0 0
	jl	ble_cont.21673
ble_else.21672:
	addi	r9, r0 1
ble_cont.21673:
	seq	r30, r8, r2
	jeql	r30, r0, beq_else.21674
	addi	r8, r9 0
	jl	beq_cont.21675
beq_else.21674:
	seq	r30, r9, r2
	jeql	r30, r0, beq_else.21676
	addi	r8, r0 1
	jl	beq_cont.21677
beq_else.21676:
	addi	r8, r0 0
beq_cont.21677:
beq_cont.21675:
	lwi	r9, r7 4
	flwi	f1, r9 1 #
	seq	r30, r8, r2
	jeql	r30, r0, beq_else.21678
	flwl	%f3 l.11783
	fsub	f1, f3, f1
	jl	beq_cont.21679
beq_else.21678:
beq_cont.21679:
	lwi	r8, r3 2
	sli	r9, r8, 0
	add	r0 r1 r9
	fswi	f1, r0 0
	sub	r0 r0 r0
	flwl	%f1 l.11794
	sli	r5, r5, 0
	add	r0 r6 r5
	flwi	f3, r0 0
	sub	r0 r0 r0
	fdiv	f1, f1, f3
	lwi	r5, r3 1
	sli	r5, r5, 0
	add	r0 r1 r5
	fswi	f1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.21671
beq_else.21670:
	lwi	r5, r3 1
	sli	r5, r5, 0
	add	r0 r1 r5
	fswi	f2, r0 0
	sub	r0 r0 r0
beq_cont.21671:
	lwi	r5, r3 2
	sli	r7, r5, 0
	add	r0 r6 r7
	flwi	f1, r0 0
	sub	r0 r0 r0
	feq	r30, f1, f2
	jeql	r30, r0, beq_else.21680
	addi	r7, r0 1
	jl	beq_cont.21681
beq_else.21680:
	addi	r7, r0 0
beq_cont.21681:
	seq	r30, r7, r2
	jeql	r30, r0, beq_else.21682
	lwi	r7, r3 4
	lwi	r8, r7 6
	sli	r9, r5, 0
	add	r0 r6 r9
	flwi	f1, r0 0
	sub	r0 r0 r0
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21684
	addi	r9, r0 0
	jl	ble_cont.21685
ble_else.21684:
	addi	r9, r0 1
ble_cont.21685:
	seq	r30, r8, r2
	jeql	r30, r0, beq_else.21686
	addi	r8, r9 0
	jl	beq_cont.21687
beq_else.21686:
	seq	r30, r9, r2
	jeql	r30, r0, beq_else.21688
	addi	r8, r0 1
	jl	beq_cont.21689
beq_else.21688:
	addi	r8, r0 0
beq_cont.21689:
beq_cont.21687:
	lwi	r7, r7 4
	flwi	f1, r7 2 #
	seq	r30, r8, r2
	jeql	r30, r0, beq_else.21690
	flwl	%f2 l.11783
	fsub	f1, f2, f1
	jl	beq_cont.21691
beq_else.21690:
beq_cont.21691:
	fswi	f1, r1 4
	flwl	%f1 l.11794
	sli	r2, r5, 0
	add	r0 r6 r2
	flwi	f2, r0 0
	sub	r0 r0 r0
	fdiv	f1, f1, f2
	lwi	r2, r3 0
	sli	r2, r2, 0
	add	r0 r1 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.21683
beq_else.21682:
	lwi	r2, r3 0
	sli	r2, r2, 0
	add	r0 r1 r2
	fswi	f2, r0 0
	sub	r0 r0 r0
beq_cont.21683:
	jr	r31
setup_surface_table.2863:
	lwi	r5, r28 5
	lwi	r6, r28 4
	lwi	r7, r28 3
	lwi	r8, r28 2
	flwi	f1, r28 1 #
	addi	r9, r0 4
	swi	r6, r3 0
	fswi	f1, r3 2
	swi	r5, r3 3
	swi	r8, r3 4
	swi	r2, r3 5
	swi	r1, r3 6
	swi	r7, r3 7
	addi	r1, r9 0 #
	swi	r31 r3 8
	addi	r3, r3, 9
	jal	min_caml_create_float_array
	addi	r3, r3, -9
	lwi	r31 r3 8
	lwi	r2, r3 7
	sli	r5, r2, 0
	lwi	r6, r3 6
	add	r0 r6 r5
	flwi	f1, r0 0
	sub	r0 r0 r0
	lwi	r5, r3 5
	lwi	r7, r5 4
	flwi	f2, r7 0 #
	fmul	f1, f1, f2
	lwi	r7, r3 4
	sli	r8, r7, 0
	add	r0 r6 r8
	flwi	f2, r0 0
	sub	r0 r0 r0
	lwi	r8, r5 4
	flwi	f3, r8 1 #
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	lwi	r8, r3 3
	sli	r9, r8, 0
	add	r0 r6 r9
	flwi	f2, r0 0
	sub	r0 r0 r0
	lwi	r6, r5 4
	flwi	f3, r6 2 #
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	flwi	f2, r3 2 #off
	fgt	r30 f1 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21693
	addi	r6, r0 0
	jl	ble_cont.21694
ble_else.21693:
	addi	r6, r0 1
ble_cont.21694:
	seq	r30, r6, r2
	jeql	r30, r0, beq_else.21695
	sli	r2, r2, 0
	add	r0 r1 r2
	fswi	f2, r0 0
	sub	r0 r0 r0
	jl	beq_cont.21696
beq_else.21695:
	flwl	%f3 l.11828
	fdiv	f3, f3, f1
	sli	r2, r2, 0
	add	r0 r1 r2
	fswi	f3, r0 0
	sub	r0 r0 r0
	lwi	r2, r5 4
	flwi	f3, r2 0 #
	fdiv	f3, f3, f1
	fsub	f3, f2, f3
	sli	r2, r7, 0
	add	r0 r1 r2
	fswi	f3, r0 0
	sub	r0 r0 r0
	lwi	r2, r5 4
	flwi	f3, r2 1 #
	fdiv	f3, f3, f1
	fsub	f3, f2, f3
	sli	r2, r8, 0
	add	r0 r1 r2
	fswi	f3, r0 0
	sub	r0 r0 r0
	lwi	r2, r5 4
	flwi	f3, r2 2 #
	fdiv	f1, f3, f1
	fsub	f1, f2, f1
	lwi	r2, r3 0
	sli	r2, r2, 0
	add	r0 r1 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
beq_cont.21696:
	jr	r31
setup_second_table.2866:
	lwi	r5, r28 7
	lwi	r6, r28 6
	lwi	r7, r28 5
	lwi	r8, r28 4
	lwi	r9, r28 3
	lwi	r10, r28 2
	flwi	f1, r28 1 #
	swi	r8, r3 0
	fswi	f1, r3 2
	swi	r2, r3 3
	swi	r5, r3 4
	swi	r7, r3 5
	swi	r10, r3 6
	swi	r1, r3 7
	swi	r9, r3 8
	addi	r1, r6 0 #
	swi	r31 r3 9
	addi	r3, r3, 10
	jal	min_caml_create_float_array
	addi	r3, r3, -10
	lwi	r31 r3 9
	lwi	r2, r3 8
	sli	r5, r2, 0
	lwi	r6, r3 7
	add	r0 r6 r5
	flwi	f1, r0 0
	sub	r0 r0 r0
	lwi	r5, r3 6
	sli	r7, r5, 0
	add	r0 r6 r7
	flwi	f2, r0 0
	sub	r0 r0 r0
	lwi	r7, r3 5
	sli	r8, r7, 0
	add	r0 r6 r8
	flwi	f3, r0 0
	sub	r0 r0 r0
	lwi	r8, r3 3
	lwi	r28, r3 4
	swi	r1, r3 9
	addi	r1, r8 0 #
	swi	r31 r3 10
	addi	r3, r3, 11
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -11
	lwi	r31, r3 10
	lwi	r1, r3 8
	sli	r2, r1, 0
	lwi	r5, r3 7
	add	r0 r5 r2
	flwi	f2, r0 0
	sub	r0 r0 r0
	lwi	r2, r3 3
	lwi	r6, r2 4
	flwi	f3, r6 0 #
	fmul	f2, f2, f3
	flwi	f3, r3 2 #off
	fsub	f2, f3, f2
	lwi	r6, r3 6
	sli	r7, r6, 0
	add	r0 r5 r7
	flwi	f4, r0 0
	sub	r0 r0 r0
	lwi	r7, r2 4
	flwi	f5, r7 1 #
	fmul	f4, f4, f5
	fsub	f4, f3, f4
	lwi	r7, r3 5
	sli	r8, r7, 0
	add	r0 r5 r8
	flwi	f5, r0 0
	sub	r0 r0 r0
	lwi	r8, r2 4
	flwi	f6, r8 2 #
	fmul	f5, f5, f6
	fsub	f5, f3, f5
	sli	r8, r1, 0
	lwi	r9, r3 9
	add	r0 r9 r8
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r8, r2 3
	seq	r30, r8, r1
	jeql	r30, r0, beq_else.21698
	sli	r2, r6, 0
	add	r0 r9 r2
	fswi	f2, r0 0
	sub	r0 r0 r0
	sli	r2, r7, 0
	add	r0 r9 r2
	fswi	f4, r0 0
	sub	r0 r0 r0
	lwi	r2, r3 0
	sli	r2, r2, 0
	add	r0 r9 r2
	fswi	f5, r0 0
	sub	r0 r0 r0
	jl	beq_cont.21699
beq_else.21698:
	sli	r8, r7, 0
	add	r0 r5 r8
	flwi	f6, r0 0
	sub	r0 r0 r0
	lwi	r8, r2 9
	flwi	f7, r8 1 #
	fmul	f6, f6, f7
	sli	r8, r6, 0
	add	r0 r5 r8
	flwi	f7, r0 0
	sub	r0 r0 r0
	lwi	r8, r2 9
	flwi	f8, r8 2 #
	fmul	f7, f7, f8
	fadd	f6, f6, f7
	flwl	%f7 l.11779
	fmul	f6, f6, f7
	fsub	f2, f2, f6
	sli	r8, r6, 0
	add	r0 r9 r8
	fswi	f2, r0 0
	sub	r0 r0 r0
	sli	r8, r7, 0
	add	r0 r5 r8
	flwi	f2, r0 0
	sub	r0 r0 r0
	lwi	r8, r2 9
	flwi	f6, r8 0 #
	fmul	f2, f2, f6
	sli	r8, r1, 0
	add	r0 r5 r8
	flwi	f6, r0 0
	sub	r0 r0 r0
	lwi	r8, r2 9
	flwi	f7, r8 2 #
	fmul	f6, f6, f7
	fadd	f2, f2, f6
	flwl	%f6 l.11779
	fmul	f2, f2, f6
	fsub	f2, f4, f2
	sli	r7, r7, 0
	add	r0 r9 r7
	fswi	f2, r0 0
	sub	r0 r0 r0
	sli	r6, r6, 0
	add	r0 r5 r6
	flwi	f2, r0 0
	sub	r0 r0 r0
	lwi	r6, r2 9
	flwi	f4, r6 0 #
	fmul	f2, f2, f4
	sli	r6, r1, 0
	add	r0 r5 r6
	flwi	f4, r0 0
	sub	r0 r0 r0
	lwi	r2, r2 9
	flwi	f6, r2 1 #
	fmul	f4, f4, f6
	fadd	f2, f2, f4
	flwl	%f4 l.11779
	fmul	f2, f2, f4
	fsub	f2, f5, f2
	lwi	r2, r3 0
	sli	r2, r2, 0
	add	r0 r9 r2
	fswi	f2, r0 0
	sub	r0 r0 r0
beq_cont.21699:
	feq	r30, f1, f3
	jeql	r30, r0, beq_else.21700
	addi	r2, r0 1
	jl	beq_cont.21701
beq_else.21700:
	addi	r2, r0 0
beq_cont.21701:
	seq	r30, r2, r1
	jeql	r30, r0, beq_else.21702
	flwl	%f2 l.11794
	fdiv	f1, f2, f1
	fswi	f1, r9 4
	jl	beq_cont.21703
beq_else.21702:
beq_cont.21703:
	addi	r1, r9 0
	jr	r31
iter_setup_dirvec_constants.2869:
	lwi	r5, r28 7
	lwi	r6, r28 6
	lwi	r7, r28 5
	lwi	r8, r28 4
	lwi	r9, r28 3
	lwi	r10, r28 2
	lwi	r11, r28 1
	sgt	r30, r10, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.21704
	sli	r12, r2, 0
	add	r12 r8 r12
	lwi	r12, r12 0
	lwi	r13, r1 1
	lwi	r14, r1 0
	lwi	r15, r12 1
	swi	r28, r3 0
	swi	r6, r3 1
	swi	r5, r3 2
	swi	r9, r3 3
	swi	r7, r3 4
	swi	r1, r3 5
	swi	r8, r3 6
	swi	r10, r3 7
	swi	r11, r3 8
	seq	r30, r15, r11
	jeql	r30, r0, beq_else.21705
	swi	r13, r3 9
	swi	r2, r3 10
	addi	r2, r12 0 #
	addi	r1, r14 0 #
	addi	r28, r7 0 #
	swi	r31 r3 11
	addi	r3, r3, 12
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -12
	lwi	r31, r3 11
	lwi	r2, r3 10
	sli	r5, r2, 0
	lwi	r6, r3 9
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.21706
beq_else.21705:
	seq	r30, r15, r9
	jeql	r30, r0, beq_else.21707
	swi	r13, r3 9
	swi	r2, r3 10
	addi	r2, r12 0 #
	addi	r1, r14 0 #
	addi	r28, r5 0 #
	swi	r31 r3 11
	addi	r3, r3, 12
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -12
	lwi	r31, r3 11
	lwi	r2, r3 10
	sli	r5, r2, 0
	lwi	r6, r3 9
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.21708
beq_else.21707:
	swi	r13, r3 9
	swi	r2, r3 10
	addi	r2, r12 0 #
	addi	r1, r14 0 #
	addi	r28, r6 0 #
	swi	r31 r3 11
	addi	r3, r3, 12
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -12
	lwi	r31, r3 11
	lwi	r2, r3 10
	sli	r5, r2, 0
	lwi	r6, r3 9
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
beq_cont.21708:
beq_cont.21706:
	lwi	r1, r3 8
	sub	r2, r2, r1
	lwi	r5, r3 7
	sgt	r30, r5, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.21709
	sli	r5, r2, 0
	lwi	r6, r3 6
	add	r5 r6 r5
	lwi	r5, r5 0
	lwi	r6, r3 5
	lwi	r7, r6 1
	lwi	r8, r6 0
	lwi	r9, r5 1
	seq	r30, r9, r1
	jeql	r30, r0, beq_else.21710
	lwi	r28, r3 4
	swi	r7, r3 11
	swi	r2, r3 12
	addi	r2, r5 0 #
	addi	r1, r8 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
	lwi	r2, r3 12
	sli	r5, r2, 0
	lwi	r6, r3 11
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.21711
beq_else.21710:
	lwi	r10, r3 3
	seq	r30, r9, r10
	jeql	r30, r0, beq_else.21712
	lwi	r28, r3 2
	swi	r7, r3 11
	swi	r2, r3 12
	addi	r2, r5 0 #
	addi	r1, r8 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
	lwi	r2, r3 12
	sli	r5, r2, 0
	lwi	r6, r3 11
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.21713
beq_else.21712:
	lwi	r28, r3 1
	swi	r7, r3 11
	swi	r2, r3 12
	addi	r2, r5 0 #
	addi	r1, r8 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
	lwi	r2, r3 12
	sli	r5, r2, 0
	lwi	r6, r3 11
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
beq_cont.21713:
beq_cont.21711:
	lwi	r1, r3 8
	sub	r2, r2, r1
	lwi	r1, r3 5
	lwi	r28, r3 0
	lwi	r29, r28 0
	jr	r29
ble_else.21709:
	jr	r31
ble_else.21704:
	jr	r31
setup_startp_constants.2874:
	lwi	r5, r28 6
	lwi	r6, r28 5
	lwi	r7, r28 4
	lwi	r8, r28 3
	lwi	r9, r28 2
	lwi	r10, r28 1
	sgt	r30, r9, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.21716
	sli	r11, r2, 0
	add	r6 r6 r11
	lwi	r6, r6 0
	lwi	r11, r6 10
	lwi	r12, r6 1
	sli	r13, r9, 0
	add	r0 r1 r13
	flwi	f1, r0 0
	sub	r0 r0 r0
	lwi	r13, r6 5
	flwi	f2, r13 0 #
	fsub	f1, f1, f2
	sli	r13, r9, 0
	add	r0 r11 r13
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r13, r10, 0
	add	r0 r1 r13
	flwi	f1, r0 0
	sub	r0 r0 r0
	lwi	r13, r6 5
	flwi	f2, r13 1 #
	fsub	f1, f1, f2
	sli	r13, r10, 0
	add	r0 r11 r13
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r13, r7, 0
	add	r0 r1 r13
	flwi	f1, r0 0
	sub	r0 r0 r0
	lwi	r13, r6 5
	flwi	f2, r13 2 #
	fsub	f1, f1, f2
	sli	r13, r7, 0
	add	r0 r11 r13
	fswi	f1, r0 0
	sub	r0 r0 r0
	swi	r1, r3 0
	swi	r28, r3 1
	swi	r10, r3 2
	swi	r2, r3 3
	seq	r30, r12, r7
	jeql	r30, r0, beq_else.21717
	lwi	r5, r6 4
	sli	r6, r9, 0
	add	r0 r11 r6
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r6, r10, 0
	add	r0 r11 r6
	flwi	f2, r0 0
	sub	r0 r0 r0
	sli	r6, r7, 0
	add	r0 r11 r6
	flwi	f3, r0 0
	sub	r0 r0 r0
	sli	r6, r9, 0
	add	r0 r5 r6
	flwi	f4, r0 0
	sub	r0 r0 r0
	fmul	f1, f4, f1
	sli	r6, r10, 0
	add	r0 r5 r6
	flwi	f4, r0 0
	sub	r0 r0 r0
	fmul	f2, f4, f2
	fadd	f1, f1, f2
	sli	r6, r7, 0
	add	r0 r5 r6
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	sli	r5, r8, 0
	add	r0 r11 r5
	fswi	f1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.21718
beq_else.21717:
	sgt	r30, r12, r7
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.21719
	jl	ble_cont.21720
ble_else.21719:
	sli	r9, r9, 0
	add	r0 r11 r9
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r9, r10, 0
	add	r0 r11 r9
	flwi	f2, r0 0
	sub	r0 r0 r0
	sli	r7, r7, 0
	add	r0 r11 r7
	flwi	f3, r0 0
	sub	r0 r0 r0
	swi	r11, r3 4
	swi	r8, r3 5
	swi	r12, r3 6
	addi	r1, r6 0 #
	addi	r28, r5 0 #
	swi	r31 r3 7
	addi	r3, r3, 8
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -8
	lwi	r31, r3 7
	lwi	r1, r3 5
	lwi	r2, r3 6
	seq	r30, r2, r1
	jeql	r30, r0, beq_else.21721
	flwl	%f2 l.11794
	fsub	f1, f1, f2
	jl	beq_cont.21722
beq_else.21721:
beq_cont.21722:
	sli	r1, r1, 0
	lwi	r2, r3 4
	add	r0 r2 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
ble_cont.21720:
beq_cont.21718:
	lwi	r1, r3 2
	lwi	r2, r3 3
	sub	r2, r2, r1
	lwi	r1, r3 0
	lwi	r28, r3 1
	lwi	r29, r28 0
	jr	r29
ble_else.21716:
	jr	r31
is_rect_outside.2879:
	lwi	r2, r28 2
	flwi	f4, r28 1 #
	fgt	r30 f4 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21724
	jl	ble_cont.21725
ble_else.21724:
	fsub	f1, f4, f1
ble_cont.21725:
	lwi	r5, r1 4
	flwi	f5, r5 0 #
	fgt	r30 f5 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21726
	addi	r5, r0 0
	jl	ble_cont.21727
ble_else.21726:
	addi	r5, r0 1
ble_cont.21727:
	seq	r30, r5, r2
	jeql	r30, r0, beq_else.21728
	addi	r5, r0 0
	jl	beq_cont.21729
beq_else.21728:
	fgt	r30 f4 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21730
	fadd	f1, f2 f0
	jl	ble_cont.21731
ble_else.21730:
	fsub	f1, f4, f2
ble_cont.21731:
	lwi	r5, r1 4
	flwi	f2, r5 1 #
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21732
	addi	r5, r0 0
	jl	ble_cont.21733
ble_else.21732:
	addi	r5, r0 1
ble_cont.21733:
	seq	r30, r5, r2
	jeql	r30, r0, beq_else.21734
	addi	r5, r0 0
	jl	beq_cont.21735
beq_else.21734:
	fgt	r30 f4 f3
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21736
	fadd	f1, f3 f0
	jl	ble_cont.21737
ble_else.21736:
	fsub	f1, f4, f3
ble_cont.21737:
	lwi	r5, r1 4
	flwi	f2, r5 2 #
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21738
	addi	r5, r0 0
	jl	ble_cont.21739
ble_else.21738:
	addi	r5, r0 1
ble_cont.21739:
beq_cont.21735:
beq_cont.21729:
	seq	r30, r5, r2
	jeql	r30, r0, beq_else.21740
	lwi	r1, r1 6
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21741
	addi	r1, r0 1
	jr	r31
beq_else.21741:
	addi	r1, r0 0
	jr	r31
beq_else.21740:
	lwi	r1, r1 6
	jr	r31
is_plane_outside.2884:
	lwi	r2, r28 4
	lwi	r5, r28 3
	lwi	r6, r28 2
	flwi	f4, r28 1 #
	lwi	r7, r1 4
	sli	r8, r5, 0
	add	r0 r7 r8
	flwi	f5, r0 0
	sub	r0 r0 r0
	fmul	f1, f5, f1
	sli	r6, r6, 0
	add	r0 r7 r6
	flwi	f5, r0 0
	sub	r0 r0 r0
	fmul	f2, f5, f2
	fadd	f1, f1, f2
	sli	r2, r2, 0
	add	r0 r7 r2
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	lwi	r1, r1 6
	fgt	r30 f4 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21742
	addi	r2, r0 0
	jl	ble_cont.21743
ble_else.21742:
	addi	r2, r0 1
ble_cont.21743:
	seq	r30, r1, r5
	jeql	r30, r0, beq_else.21744
	addi	r1, r2 0
	jl	beq_cont.21745
beq_else.21744:
	seq	r30, r2, r5
	jeql	r30, r0, beq_else.21746
	addi	r1, r0 1
	jl	beq_cont.21747
beq_else.21746:
	addi	r1, r0 0
beq_cont.21747:
beq_cont.21745:
	seq	r30, r1, r5
	jeql	r30, r0, beq_else.21748
	addi	r1, r0 1
	jr	r31
beq_else.21748:
	addi	r1, r0 0
	jr	r31
is_outside.2894:
	lwi	r2, r28 6
	lwi	r5, r28 5
	lwi	r6, r28 4
	lwi	r7, r28 3
	lwi	r8, r28 2
	lwi	r9, r28 1
	lwi	r10, r1 5
	flwi	f4, r10 0 #
	fsub	f1, f1, f4
	lwi	r10, r1 5
	flwi	f4, r10 1 #
	fsub	f2, f2, f4
	lwi	r10, r1 5
	flwi	f4, r10 2 #
	fsub	f3, f3, f4
	lwi	r10, r1 1
	seq	r30, r10, r9
	jeql	r30, r0, beq_else.21749
	addi	r28, r5 0 #
	lwi	r29, r28 0
	jr	r29
beq_else.21749:
	seq	r30, r10, r6
	jeql	r30, r0, beq_else.21750
	lwi	r2, r1 4
	sli	r5, r8, 0
	add	r0 r2 r5
	flwi	f4, r0 0
	sub	r0 r0 r0
	fmul	f1, f4, f1
	sli	r5, r9, 0
	add	r0 r2 r5
	flwi	f4, r0 0
	sub	r0 r0 r0
	fmul	f2, f4, f2
	fadd	f1, f1, f2
	sli	r5, r6, 0
	add	r0 r2 r5
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	lwi	r1, r1 6
	flwl	%f2 l.11783
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21751
	addi	r2, r0 0
	jl	ble_cont.21752
ble_else.21751:
	addi	r2, r0 1
ble_cont.21752:
	seq	r30, r1, r8
	jeql	r30, r0, beq_else.21753
	addi	r1, r2 0
	jl	beq_cont.21754
beq_else.21753:
	seq	r30, r2, r8
	jeql	r30, r0, beq_else.21755
	addi	r1, r0 1
	jl	beq_cont.21756
beq_else.21755:
	addi	r1, r0 0
beq_cont.21756:
beq_cont.21754:
	seq	r30, r1, r8
	jeql	r30, r0, beq_else.21757
	addi	r1, r0 1
	jr	r31
beq_else.21757:
	addi	r1, r0 0
	jr	r31
beq_else.21750:
	swi	r8, r3 0
	swi	r7, r3 1
	swi	r1, r3 2
	addi	r28, r2 0 #
	swi	r31 r3 3
	addi	r3, r3, 4
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -4
	lwi	r31, r3 3
	lwi	r1, r3 2
	lwi	r2, r1 1
	lwi	r5, r3 1
	seq	r30, r2, r5
	jeql	r30, r0, beq_else.21758
	flwl	%f2 l.11794
	fsub	f1, f1, f2
	jl	beq_cont.21759
beq_else.21758:
beq_cont.21759:
	lwi	r1, r1 6
	flwl	%f2 l.11783
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21760
	addi	r2, r0 0
	jl	ble_cont.21761
ble_else.21760:
	addi	r2, r0 1
ble_cont.21761:
	lwi	r5, r3 0
	seq	r30, r1, r5
	jeql	r30, r0, beq_else.21762
	addi	r1, r2 0
	jl	beq_cont.21763
beq_else.21762:
	seq	r30, r2, r5
	jeql	r30, r0, beq_else.21764
	addi	r1, r0 1
	jl	beq_cont.21765
beq_else.21764:
	addi	r1, r0 0
beq_cont.21765:
beq_cont.21763:
	seq	r30, r1, r5
	jeql	r30, r0, beq_else.21766
	addi	r1, r0 1
	jr	r31
beq_else.21766:
	addi	r1, r0 0
	jr	r31
check_all_inside.2899:
	lwi	r5, r28 11
	lwi	r6, r28 10
	lwi	r7, r28 9
	lwi	r8, r28 8
	lwi	r9, r28 7
	lwi	r10, r28 6
	lwi	r11, r28 5
	lwi	r12, r28 4
	lwi	r13, r28 3
	lwi	r14, r28 2
	flwi	f4, r28 1 #
	sli	r15, r1, 0
	add	r15 r2 r15
	lwi	r15, r15 0
	seq	r30, r15, r11
	jeql	r30, r0, beq_else.21767
	addi	r1, r0 1
	jr	r31
beq_else.21767:
	sli	r15, r15, 0
	add	r15 r6 r15
	lwi	r15, r15 0
	lwi	r16, r15 5
	flwi	f5, r16 0 #
	fsub	f5, f1, f5
	lwi	r16, r15 5
	flwi	f6, r16 1 #
	fsub	f6, f2, f6
	lwi	r16, r15 5
	flwi	f7, r16 2 #
	fsub	f7, f3, f7
	lwi	r16, r15 1
	swi	r28, r3 0
	fswi	f3, r3 2
	fswi	f2, r3 4
	fswi	f1, r3 6
	swi	r9, r3 7
	swi	r6, r3 8
	swi	r11, r3 9
	swi	r2, r3 10
	swi	r14, r3 11
	swi	r1, r3 12
	swi	r13, r3 13
	seq	r30, r16, r14
	jeql	r30, r0, beq_else.21771
	addi	r1, r15 0 #
	addi	r28, r7 0 #
	fadd	f3, f7 f0
	fadd	f2, f6 f0
	fadd	f1, f5 f0
	swi	r31 r3 14
	addi	r3, r3, 15
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -15
	lwi	r31, r3 14
	jl	beq_cont.21772
beq_else.21771:
	seq	r30, r16, r10
	jeql	r30, r0, beq_else.21773
	addi	r1, r15 0 #
	addi	r28, r8 0 #
	fadd	f3, f7 f0
	fadd	f2, f6 f0
	fadd	f1, f5 f0
	swi	r31 r3 14
	addi	r3, r3, 15
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -15
	lwi	r31, r3 14
	jl	beq_cont.21774
beq_else.21773:
	fswi	f4, r3 14
	swi	r12, r3 15
	swi	r15, r3 16
	addi	r1, r15 0 #
	addi	r28, r5 0 #
	fadd	f3, f7 f0
	fadd	f2, f6 f0
	fadd	f1, f5 f0
	swi	r31 r3 17
	addi	r3, r3, 18
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -18
	lwi	r31, r3 17
	lwi	r1, r3 16
	lwi	r2, r1 1
	lwi	r5, r3 15
	seq	r30, r2, r5
	jeql	r30, r0, beq_else.21775
	flwl	%f2 l.11794
	fsub	f1, f1, f2
	jl	beq_cont.21776
beq_else.21775:
beq_cont.21776:
	lwi	r1, r1 6
	flwi	f2, r3 14 #off
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21777
	addi	r2, r0 0
	jl	ble_cont.21778
ble_else.21777:
	addi	r2, r0 1
ble_cont.21778:
	lwi	r5, r3 13
	seq	r30, r1, r5
	jeql	r30, r0, beq_else.21779
	addi	r1, r2 0
	jl	beq_cont.21780
beq_else.21779:
	seq	r30, r2, r5
	jeql	r30, r0, beq_else.21781
	addi	r1, r0 1
	jl	beq_cont.21782
beq_else.21781:
	addi	r1, r0 0
beq_cont.21782:
beq_cont.21780:
	seq	r30, r1, r5
	jeql	r30, r0, beq_else.21783
	addi	r1, r0 1
	jl	beq_cont.21784
beq_else.21783:
	addi	r1, r0 0
beq_cont.21784:
beq_cont.21774:
beq_cont.21772:
	lwi	r2, r3 13
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21785
	lwi	r1, r3 11
	lwi	r5, r3 12
	add	r5, r5, r1
	sli	r6, r5, 0
	lwi	r7, r3 10
	add	r6 r7 r6
	lwi	r6, r6 0
	lwi	r8, r3 9
	seq	r30, r6, r8
	jeql	r30, r0, beq_else.21786
	addi	r1, r0 1
	jr	r31
beq_else.21786:
	sli	r6, r6, 0
	lwi	r8, r3 8
	add	r6 r8 r6
	lwi	r6, r6 0
	flwi	f1, r3 6 #off
	flwi	f2, r3 4 #off
	flwi	f3, r3 2 #off
	lwi	r28, r3 7
	swi	r5, r3 17
	addi	r1, r6 0 #
	swi	r31 r3 18
	addi	r3, r3, 19
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -19
	lwi	r31, r3 18
	lwi	r2, r3 13
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21787
	lwi	r1, r3 11
	lwi	r2, r3 17
	add	r1, r2, r1
	flwi	f1, r3 6 #off
	flwi	f2, r3 4 #off
	flwi	f3, r3 2 #off
	lwi	r2, r3 10
	lwi	r28, r3 0
	lwi	r29, r28 0
	jr	r29
beq_else.21787:
	addi	r1, r0 0
	jr	r31
beq_else.21785:
	addi	r1, r0 0
	jr	r31
shadow_check_and_group.2905:
	lwi	r5, r28 12
	lwi	r6, r28 11
	lwi	r7, r28 10
	lwi	r8, r28 9
	lwi	r9, r28 8
	lwi	r10, r28 7
	lwi	r11, r28 6
	lwi	r12, r28 5
	lwi	r13, r28 4
	lwi	r14, r28 3
	lwi	r15, r28 2
	lwi	r16, r28 1
	sli	r17, r1, 0
	add	r17 r2 r17
	lwi	r17, r17 0
	seq	r30, r17, r14
	jeql	r30, r0, beq_else.21788
	addi	r1, r0 0
	jr	r31
beq_else.21788:
	sli	r17, r1, 0
	add	r17 r2 r17
	lwi	r17, r17 0
	swi	r12, r3 0
	swi	r10, r3 1
	swi	r14, r3 2
	swi	r13, r3 3
	swi	r11, r3 4
	swi	r9, r3 5
	swi	r2, r3 6
	swi	r28, r3 7
	swi	r16, r3 8
	swi	r1, r3 9
	swi	r7, r3 10
	swi	r17, r3 11
	swi	r6, r3 12
	swi	r15, r3 13
	addi	r2, r8 0 #
	addi	r1, r17 0 #
	addi	r28, r5 0 #
	addi	r5, r11 0 #
	swi	r31 r3 14
	addi	r3, r3, 15
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -15
	lwi	r31, r3 14
	lwi	r2, r3 13
	sli	r5, r2, 0
	lwi	r6, r3 12
	add	r0 r6 r5
	flwi	f1, r0 0
	sub	r0 r0 r0
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21789
	addi	r1, r0 0
	jl	beq_cont.21790
beq_else.21789:
	flwl	%f2 l.12162
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21791
	addi	r1, r0 0
	jl	ble_cont.21792
ble_else.21791:
	addi	r1, r0 1
ble_cont.21792:
beq_cont.21790:
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21793
	lwi	r1, r3 11
	sli	r1, r1, 0
	lwi	r5, r3 10
	add	r1 r5 r1
	lwi	r1, r1 0
	lwi	r1, r1 6
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21794
	addi	r1, r0 0
	jr	r31
beq_else.21794:
	lwi	r1, r3 8
	lwi	r2, r3 9
	add	r1, r2, r1
	lwi	r2, r3 6
	lwi	r28, r3 7
	lwi	r29, r28 0
	jr	r29
beq_else.21793:
	flwl	%f2 l.12163
	fadd	f1, f1, f2
	sli	r1, r2, 0
	lwi	r5, r3 5
	add	r0 r5 r1
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f1
	sli	r1, r2, 0
	lwi	r6, r3 4
	add	r0 r6 r1
	flwi	f3, r0 0
	sub	r0 r0 r0
	fadd	f2, f2, f3
	lwi	r1, r3 8
	sli	r7, r1, 0
	add	r0 r5 r7
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f3, f3, f1
	sli	r7, r1, 0
	add	r0 r6 r7
	flwi	f4, r0 0
	sub	r0 r0 r0
	fadd	f3, f3, f4
	lwi	r7, r3 3
	sli	r8, r7, 0
	add	r0 r5 r8
	flwi	f4, r0 0
	sub	r0 r0 r0
	fmul	f1, f4, f1
	sli	r5, r7, 0
	add	r0 r6 r5
	flwi	f4, r0 0
	sub	r0 r0 r0
	fadd	f1, f1, f4
	sli	r5, r2, 0
	lwi	r6, r3 6
	add	r5 r6 r5
	lwi	r5, r5 0
	lwi	r7, r3 2
	seq	r30, r5, r7
	jeql	r30, r0, beq_else.21795
	addi	r1, r0 1
	jl	beq_cont.21796
beq_else.21795:
	sli	r5, r5, 0
	lwi	r7, r3 10
	add	r5 r7 r5
	lwi	r5, r5 0
	lwi	r28, r3 1
	fswi	f1, r3 14
	fswi	f3, r3 16
	fswi	f2, r3 18
	addi	r1, r5 0 #
	fadd	f31, f3 f0
	fadd	f3, f1 f0
	fadd	f1, f2 f0
	fadd	f2, f31 f0
	swi	r31 r3 19
	addi	r3, r3, 20
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -20
	lwi	r31, r3 19
	lwi	r2, r3 13
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21799
	flwi	f1, r3 18 #off
	flwi	f2, r3 16 #off
	flwi	f3, r3 14 #off
	lwi	r1, r3 8
	lwi	r5, r3 6
	lwi	r28, r3 0
	addi	r2, r5 0 #
	swi	r31 r3 19
	addi	r3, r3, 20
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -20
	lwi	r31, r3 19
	jl	beq_cont.21800
beq_else.21799:
	addi	r1, r0 0
beq_cont.21800:
beq_cont.21796:
	lwi	r2, r3 13
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21801
	lwi	r1, r3 8
	lwi	r2, r3 9
	add	r1, r2, r1
	lwi	r2, r3 6
	lwi	r28, r3 7
	lwi	r29, r28 0
	jr	r29
beq_else.21801:
	addi	r1, r0 1
	jr	r31
shadow_check_one_or_group.2908:
	lwi	r5, r28 5
	lwi	r6, r28 4
	lwi	r7, r28 3
	lwi	r8, r28 2
	lwi	r9, r28 1
	sli	r10, r1, 0
	add	r10 r2 r10
	lwi	r10, r10 0
	seq	r30, r10, r7
	jeql	r30, r0, beq_else.21802
	addi	r1, r0 0
	jr	r31
beq_else.21802:
	sli	r10, r10, 0
	add	r10 r6 r10
	lwi	r10, r10 0
	swi	r28, r3 0
	swi	r5, r3 1
	swi	r6, r3 2
	swi	r7, r3 3
	swi	r2, r3 4
	swi	r9, r3 5
	swi	r1, r3 6
	swi	r8, r3 7
	addi	r2, r10 0 #
	addi	r1, r8 0 #
	addi	r28, r5 0 #
	swi	r31 r3 8
	addi	r3, r3, 9
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -9
	lwi	r31, r3 8
	lwi	r2, r3 7
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21803
	lwi	r1, r3 5
	lwi	r5, r3 6
	add	r5, r5, r1
	sli	r6, r5, 0
	lwi	r7, r3 4
	add	r6 r7 r6
	lwi	r6, r6 0
	lwi	r8, r3 3
	seq	r30, r6, r8
	jeql	r30, r0, beq_else.21804
	addi	r1, r0 0
	jr	r31
beq_else.21804:
	sli	r6, r6, 0
	lwi	r9, r3 2
	add	r6 r9 r6
	lwi	r6, r6 0
	lwi	r28, r3 1
	swi	r5, r3 8
	addi	r1, r2 0 #
	addi	r2, r6 0 #
	swi	r31 r3 9
	addi	r3, r3, 10
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -10
	lwi	r31, r3 9
	lwi	r2, r3 7
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21805
	lwi	r1, r3 5
	lwi	r5, r3 8
	add	r5, r5, r1
	sli	r6, r5, 0
	lwi	r7, r3 4
	add	r6 r7 r6
	lwi	r6, r6 0
	lwi	r8, r3 3
	seq	r30, r6, r8
	jeql	r30, r0, beq_else.21806
	addi	r1, r0 0
	jr	r31
beq_else.21806:
	sli	r6, r6, 0
	lwi	r9, r3 2
	add	r6 r9 r6
	lwi	r6, r6 0
	lwi	r28, r3 1
	swi	r5, r3 9
	addi	r1, r2 0 #
	addi	r2, r6 0 #
	swi	r31 r3 10
	addi	r3, r3, 11
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -11
	lwi	r31, r3 10
	lwi	r2, r3 7
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21807
	lwi	r1, r3 5
	lwi	r5, r3 9
	add	r5, r5, r1
	sli	r6, r5, 0
	lwi	r7, r3 4
	add	r6 r7 r6
	lwi	r6, r6 0
	lwi	r8, r3 3
	seq	r30, r6, r8
	jeql	r30, r0, beq_else.21808
	addi	r1, r0 0
	jr	r31
beq_else.21808:
	sli	r6, r6, 0
	lwi	r8, r3 2
	add	r6 r8 r6
	lwi	r6, r6 0
	lwi	r28, r3 1
	swi	r5, r3 10
	addi	r1, r2 0 #
	addi	r2, r6 0 #
	swi	r31 r3 11
	addi	r3, r3, 12
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -12
	lwi	r31, r3 11
	lwi	r2, r3 7
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21809
	lwi	r1, r3 5
	lwi	r2, r3 10
	add	r1, r2, r1
	lwi	r2, r3 4
	lwi	r28, r3 0
	lwi	r29, r28 0
	jr	r29
beq_else.21809:
	addi	r1, r0 1
	jr	r31
beq_else.21807:
	addi	r1, r0 1
	jr	r31
beq_else.21805:
	addi	r1, r0 1
	jr	r31
beq_else.21803:
	addi	r1, r0 1
	jr	r31
shadow_check_one_or_matrix.2911:
	lwi	r5, r28 12
	lwi	r6, r28 11
	lwi	r7, r28 10
	lwi	r8, r28 9
	lwi	r9, r28 8
	lwi	r10, r28 7
	lwi	r11, r28 6
	lwi	r12, r28 5
	lwi	r13, r28 4
	lwi	r14, r28 3
	lwi	r15, r28 2
	lwi	r16, r28 1
	sli	r17, r1, 0
	add	r17 r2 r17
	lwi	r17, r17 0
	sli	r18, r15, 0
	add	r18 r17 r18
	lwi	r18, r18 0
	seq	r30, r18, r13
	jeql	r30, r0, beq_else.21810
	addi	r1, r0 0
	jr	r31
beq_else.21810:
	swi	r7, r3 0
	swi	r14, r3 1
	swi	r12, r3 2
	swi	r8, r3 3
	swi	r11, r3 4
	swi	r13, r3 5
	swi	r17, r3 6
	swi	r2, r3 7
	swi	r28, r3 8
	swi	r16, r3 9
	swi	r1, r3 10
	swi	r15, r3 11
	addi	r30, r0, 99
	seq	r30, r18, r30
	jeql	r30, r0, beq_else.21811
	addi	r1, r0 1
	jl	beq_cont.21812
beq_else.21811:
	swi	r6, r3 12
	addi	r2, r9 0 #
	addi	r1, r18 0 #
	addi	r28, r5 0 #
	addi	r5, r10 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
	lwi	r2, r3 11
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21813
	addi	r1, r0 0
	jl	beq_cont.21814
beq_else.21813:
	sli	r1, r2, 0
	lwi	r5, r3 12
	add	r0 r5 r1
	flwi	f1, r0 0
	sub	r0 r0 r0
	flwl	%f2 l.12184
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21815
	addi	r1, r0 0
	jl	ble_cont.21816
ble_else.21815:
	addi	r1, r0 1
ble_cont.21816:
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21817
	addi	r1, r0 0
	jl	beq_cont.21818
beq_else.21817:
	lwi	r1, r3 9
	sli	r5, r1, 0
	lwi	r6, r3 6
	add	r5 r6 r5
	lwi	r5, r5 0
	lwi	r7, r3 5
	seq	r30, r5, r7
	jeql	r30, r0, beq_else.21819
	addi	r1, r0 0
	jl	beq_cont.21820
beq_else.21819:
	sli	r5, r5, 0
	lwi	r8, r3 4
	add	r5 r8 r5
	lwi	r5, r5 0
	lwi	r28, r3 3
	addi	r1, r2 0 #
	addi	r2, r5 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
	lwi	r2, r3 11
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21821
	lwi	r1, r3 2
	sli	r5, r1, 0
	lwi	r6, r3 6
	add	r5 r6 r5
	lwi	r5, r5 0
	lwi	r7, r3 5
	seq	r30, r5, r7
	jeql	r30, r0, beq_else.21823
	addi	r1, r0 0
	jl	beq_cont.21824
beq_else.21823:
	sli	r5, r5, 0
	lwi	r8, r3 4
	add	r5 r8 r5
	lwi	r5, r5 0
	lwi	r28, r3 3
	addi	r1, r2 0 #
	addi	r2, r5 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
	lwi	r2, r3 11
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21825
	lwi	r1, r3 1
	sli	r5, r1, 0
	lwi	r6, r3 6
	add	r5 r6 r5
	lwi	r5, r5 0
	lwi	r7, r3 5
	seq	r30, r5, r7
	jeql	r30, r0, beq_else.21827
	addi	r1, r0 0
	jl	beq_cont.21828
beq_else.21827:
	sli	r5, r5, 0
	lwi	r8, r3 4
	add	r5 r8 r5
	lwi	r5, r5 0
	lwi	r28, r3 3
	addi	r1, r2 0 #
	addi	r2, r5 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
	lwi	r2, r3 11
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21829
	addi	r1, r0 4
	lwi	r5, r3 6
	lwi	r28, r3 0
	addi	r2, r5 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
	jl	beq_cont.21830
beq_else.21829:
	addi	r1, r0 1
beq_cont.21830:
beq_cont.21828:
	jl	beq_cont.21826
beq_else.21825:
	addi	r1, r0 1
beq_cont.21826:
beq_cont.21824:
	jl	beq_cont.21822
beq_else.21821:
	addi	r1, r0 1
beq_cont.21822:
beq_cont.21820:
	lwi	r2, r3 11
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21831
	addi	r1, r0 0
	jl	beq_cont.21832
beq_else.21831:
	addi	r1, r0 1
beq_cont.21832:
beq_cont.21818:
beq_cont.21814:
beq_cont.21812:
	lwi	r2, r3 11
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21833
	lwi	r1, r3 9
	lwi	r2, r3 10
	add	r1, r2, r1
	lwi	r2, r3 7
	lwi	r28, r3 8
	lwi	r29, r28 0
	jr	r29
beq_else.21833:
	lwi	r1, r3 9
	sli	r5, r1, 0
	lwi	r6, r3 6
	add	r5 r6 r5
	lwi	r5, r5 0
	lwi	r7, r3 5
	seq	r30, r5, r7
	jeql	r30, r0, beq_else.21834
	addi	r1, r0 0
	jl	beq_cont.21835
beq_else.21834:
	sli	r5, r5, 0
	lwi	r8, r3 4
	add	r5 r8 r5
	lwi	r5, r5 0
	lwi	r28, r3 3
	addi	r1, r2 0 #
	addi	r2, r5 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
	lwi	r2, r3 11
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21836
	lwi	r1, r3 2
	sli	r1, r1, 0
	lwi	r5, r3 6
	add	r1 r5 r1
	lwi	r1, r1 0
	lwi	r6, r3 5
	seq	r30, r1, r6
	jeql	r30, r0, beq_else.21838
	addi	r1, r0 0
	jl	beq_cont.21839
beq_else.21838:
	sli	r1, r1, 0
	lwi	r7, r3 4
	add	r1 r7 r1
	lwi	r1, r1 0
	lwi	r28, r3 3
	addi	r29, r2 0 #
	addi	r2, r1 0 #
	addi	r1, r29 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
	lwi	r2, r3 11
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21840
	lwi	r1, r3 1
	sli	r1, r1, 0
	lwi	r5, r3 6
	add	r1 r5 r1
	lwi	r1, r1 0
	lwi	r6, r3 5
	seq	r30, r1, r6
	jeql	r30, r0, beq_else.21842
	addi	r1, r0 0
	jl	beq_cont.21843
beq_else.21842:
	sli	r1, r1, 0
	lwi	r6, r3 4
	add	r1 r6 r1
	lwi	r1, r1 0
	lwi	r28, r3 3
	addi	r29, r2 0 #
	addi	r2, r1 0 #
	addi	r1, r29 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
	lwi	r2, r3 11
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21844
	addi	r1, r0 4
	lwi	r5, r3 6
	lwi	r28, r3 0
	addi	r2, r5 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
	jl	beq_cont.21845
beq_else.21844:
	addi	r1, r0 1
beq_cont.21845:
beq_cont.21843:
	jl	beq_cont.21841
beq_else.21840:
	addi	r1, r0 1
beq_cont.21841:
beq_cont.21839:
	jl	beq_cont.21837
beq_else.21836:
	addi	r1, r0 1
beq_cont.21837:
beq_cont.21835:
	lwi	r2, r3 11
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21846
	lwi	r1, r3 9
	lwi	r2, r3 10
	add	r1, r2, r1
	lwi	r2, r3 7
	lwi	r28, r3 8
	lwi	r29, r28 0
	jr	r29
beq_else.21846:
	addi	r1, r0 1
	jr	r31
solve_each_element.2914:
	lwi	r6, r28 15
	lwi	r7, r28 14
	lwi	r8, r28 13
	lwi	r9, r28 12
	lwi	r10, r28 11
	lwi	r11, r28 10
	lwi	r12, r28 9
	lwi	r13, r28 8
	lwi	r14, r28 7
	lwi	r15, r28 6
	lwi	r16, r28 5
	lwi	r17, r28 4
	lwi	r18, r28 3
	lwi	r19, r28 2
	flwi	f1, r28 1 #
	sli	r20, r1, 0
	add	r20 r2 r20
	lwi	r20, r20 0
	seq	r30, r20, r17
	jeql	r30, r0, beq_else.21847
	jr	r31
beq_else.21847:
	swi	r12, r3 0
	swi	r14, r3 1
	swi	r13, r3 2
	swi	r15, r3 3
	swi	r11, r3 4
	swi	r17, r3 5
	swi	r16, r3 6
	swi	r7, r3 7
	swi	r6, r3 8
	fswi	f1, r3 10
	swi	r8, r3 11
	swi	r5, r3 12
	swi	r2, r3 13
	swi	r28, r3 14
	swi	r19, r3 15
	swi	r1, r3 16
	swi	r10, r3 17
	swi	r20, r3 18
	swi	r18, r3 19
	addi	r2, r5 0 #
	addi	r1, r20 0 #
	addi	r28, r9 0 #
	addi	r5, r7 0 #
	swi	r31 r3 20
	addi	r3, r3, 21
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -21
	lwi	r31, r3 20
	lwi	r2, r3 19
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21850
	lwi	r1, r3 18
	sli	r1, r1, 0
	lwi	r5, r3 17
	add	r1 r5 r1
	lwi	r1, r1 0
	lwi	r1, r1 6
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21851
	jr	r31
beq_else.21851:
	lwi	r1, r3 15
	lwi	r2, r3 16
	add	r1, r2, r1
	lwi	r2, r3 13
	lwi	r5, r3 12
	lwi	r28, r3 14
	lwi	r29, r28 0
	jr	r29
beq_else.21850:
	sli	r5, r2, 0
	lwi	r6, r3 11
	add	r0 r6 r5
	flwi	f1, r0 0
	sub	r0 r0 r0
	flwi	f2, r3 10 #off
	fgt	r30 f1 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21853
	addi	r5, r0 0
	jl	ble_cont.21854
ble_else.21853:
	addi	r5, r0 1
ble_cont.21854:
	seq	r30, r5, r2
	jeql	r30, r0, beq_else.21855
	jl	beq_cont.21856
beq_else.21855:
	sli	r5, r2, 0
	lwi	r6, r3 8
	add	r0 r6 r5
	flwi	f2, r0 0
	sub	r0 r0 r0
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21857
	addi	r5, r0 0
	jl	ble_cont.21858
ble_else.21857:
	addi	r5, r0 1
ble_cont.21858:
	seq	r30, r5, r2
	jeql	r30, r0, beq_else.21859
	jl	beq_cont.21860
beq_else.21859:
	flwl	%f2 l.12163
	fadd	f1, f1, f2
	sli	r5, r2, 0
	lwi	r7, r3 12
	add	r0 r7 r5
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f1
	sli	r5, r2, 0
	lwi	r8, r3 7
	add	r0 r8 r5
	flwi	f3, r0 0
	sub	r0 r0 r0
	fadd	f2, f2, f3
	lwi	r5, r3 15
	sli	r9, r5, 0
	add	r0 r7 r9
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f3, f3, f1
	sli	r9, r5, 0
	add	r0 r8 r9
	flwi	f4, r0 0
	sub	r0 r0 r0
	fadd	f3, f3, f4
	lwi	r9, r3 6
	sli	r10, r9, 0
	add	r0 r7 r10
	flwi	f4, r0 0
	sub	r0 r0 r0
	fmul	f4, f4, f1
	sli	r10, r9, 0
	add	r0 r8 r10
	flwi	f5, r0 0
	sub	r0 r0 r0
	fadd	f4, f4, f5
	sli	r8, r2, 0
	lwi	r10, r3 13
	add	r8 r10 r8
	lwi	r8, r8 0
	lwi	r11, r3 5
	swi	r1, r3 20
	fswi	f4, r3 22
	fswi	f3, r3 24
	fswi	f2, r3 26
	fswi	f1, r3 28
	seq	r30, r8, r11
	jeql	r30, r0, beq_else.21865
	addi	r1, r0 1
	jl	beq_cont.21866
beq_else.21865:
	sli	r8, r8, 0
	lwi	r11, r3 17
	add	r8 r11 r8
	lwi	r8, r8 0
	lwi	r28, r3 4
	addi	r1, r8 0 #
	fadd	f1, f2 f0
	fadd	f2, f3 f0
	fadd	f3, f4 f0
	swi	r31 r3 29
	addi	r3, r3, 30
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -30
	lwi	r31, r3 29
	lwi	r2, r3 19
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21867
	flwi	f1, r3 26 #off
	flwi	f2, r3 24 #off
	flwi	f3, r3 22 #off
	lwi	r1, r3 15
	lwi	r5, r3 13
	lwi	r28, r3 3
	addi	r2, r5 0 #
	swi	r31 r3 29
	addi	r3, r3, 30
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -30
	lwi	r31, r3 29
	jl	beq_cont.21868
beq_else.21867:
	addi	r1, r0 0
beq_cont.21868:
beq_cont.21866:
	lwi	r2, r3 19
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21869
	jl	beq_cont.21870
beq_else.21869:
	sli	r1, r2, 0
	lwi	r5, r3 8
	flwi	f1, r3 28 #off
	add	r0 r5 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r1, r2, 0
	lwi	r5, r3 2
	flwi	f1, r3 26 #off
	add	r0 r5 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 15
	sli	r6, r1, 0
	flwi	f1, r3 24 #off
	add	r0 r5 r6
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r6, r3 6
	sli	r6, r6, 0
	flwi	f1, r3 22 #off
	add	r0 r5 r6
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r5, r2, 0
	lwi	r6, r3 1
	lwi	r7, r3 18
	add	r0 r6 r5
	swi	r7, r0 0
	sub	r0 r0 r0
	sli	r2, r2, 0
	lwi	r5, r3 0
	lwi	r6, r3 20
	add	r0 r5 r2
	swi	r6, r0 0
	sub	r0 r0 r0
beq_cont.21870:
beq_cont.21860:
beq_cont.21856:
	lwi	r1, r3 15
	lwi	r2, r3 16
	add	r1, r2, r1
	lwi	r2, r3 13
	lwi	r5, r3 12
	lwi	r28, r3 14
	lwi	r29, r28 0
	jr	r29
solve_one_or_network.2918:
	lwi	r6, r28 5
	lwi	r7, r28 4
	lwi	r8, r28 3
	lwi	r9, r28 2
	lwi	r10, r28 1
	sli	r11, r1, 0
	add	r11 r2 r11
	lwi	r11, r11 0
	seq	r30, r11, r8
	jeql	r30, r0, beq_else.21871
	jr	r31
beq_else.21871:
	sli	r11, r11, 0
	add	r11 r7 r11
	lwi	r11, r11 0
	swi	r28, r3 0
	swi	r5, r3 1
	swi	r9, r3 2
	swi	r6, r3 3
	swi	r7, r3 4
	swi	r8, r3 5
	swi	r2, r3 6
	swi	r10, r3 7
	swi	r1, r3 8
	addi	r2, r11 0 #
	addi	r1, r9 0 #
	addi	r28, r6 0 #
	swi	r31 r3 9
	addi	r3, r3, 10
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -10
	lwi	r31, r3 9
	lwi	r1, r3 7
	lwi	r2, r3 8
	add	r2, r2, r1
	sli	r5, r2, 0
	lwi	r6, r3 6
	add	r5 r6 r5
	lwi	r5, r5 0
	lwi	r7, r3 5
	seq	r30, r5, r7
	jeql	r30, r0, beq_else.21873
	jr	r31
beq_else.21873:
	sli	r5, r5, 0
	lwi	r8, r3 4
	add	r5 r8 r5
	lwi	r5, r5 0
	lwi	r9, r3 2
	lwi	r10, r3 1
	lwi	r28, r3 3
	swi	r2, r3 9
	addi	r2, r5 0 #
	addi	r1, r9 0 #
	addi	r5, r10 0 #
	swi	r31 r3 10
	addi	r3, r3, 11
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -11
	lwi	r31, r3 10
	lwi	r1, r3 7
	lwi	r2, r3 9
	add	r2, r2, r1
	sli	r5, r2, 0
	lwi	r6, r3 6
	add	r5 r6 r5
	lwi	r5, r5 0
	lwi	r7, r3 5
	seq	r30, r5, r7
	jeql	r30, r0, beq_else.21875
	jr	r31
beq_else.21875:
	sli	r5, r5, 0
	lwi	r8, r3 4
	add	r5 r8 r5
	lwi	r5, r5 0
	lwi	r9, r3 2
	lwi	r10, r3 1
	lwi	r28, r3 3
	swi	r2, r3 10
	addi	r2, r5 0 #
	addi	r1, r9 0 #
	addi	r5, r10 0 #
	swi	r31 r3 11
	addi	r3, r3, 12
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -12
	lwi	r31, r3 11
	lwi	r1, r3 7
	lwi	r2, r3 10
	add	r2, r2, r1
	sli	r5, r2, 0
	lwi	r6, r3 6
	add	r5 r6 r5
	lwi	r5, r5 0
	lwi	r7, r3 5
	seq	r30, r5, r7
	jeql	r30, r0, beq_else.21877
	jr	r31
beq_else.21877:
	sli	r5, r5, 0
	lwi	r7, r3 4
	add	r5 r7 r5
	lwi	r5, r5 0
	lwi	r7, r3 2
	lwi	r8, r3 1
	lwi	r28, r3 3
	swi	r2, r3 11
	addi	r2, r5 0 #
	addi	r1, r7 0 #
	addi	r5, r8 0 #
	swi	r31 r3 12
	addi	r3, r3, 13
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -13
	lwi	r31, r3 12
	lwi	r1, r3 7
	lwi	r2, r3 11
	add	r1, r2, r1
	lwi	r2, r3 6
	lwi	r5, r3 1
	lwi	r28, r3 0
	lwi	r29, r28 0
	jr	r29
trace_or_matrix.2922:
	lwi	r6, r28 12
	lwi	r7, r28 11
	lwi	r8, r28 10
	lwi	r9, r28 9
	lwi	r10, r28 8
	lwi	r11, r28 7
	lwi	r12, r28 6
	lwi	r13, r28 5
	lwi	r14, r28 4
	lwi	r15, r28 3
	lwi	r16, r28 2
	lwi	r17, r28 1
	sli	r18, r1, 0
	add	r18 r2 r18
	lwi	r18, r18 0
	sli	r19, r16, 0
	add	r19 r18 r19
	lwi	r19, r19 0
	seq	r30, r19, r14
	jeql	r30, r0, beq_else.21879
	jr	r31
beq_else.21879:
	swi	r5, r3 0
	swi	r2, r3 1
	swi	r28, r3 2
	swi	r17, r3 3
	swi	r1, r3 4
	addi	r30, r0, 99
	seq	r30, r19, r30
	jeql	r30, r0, beq_else.21881
	sli	r6, r17, 0
	add	r6 r18 r6
	lwi	r6, r6 0
	seq	r30, r6, r14
	jeql	r30, r0, beq_else.21883
	jl	beq_cont.21884
beq_else.21883:
	sli	r6, r6, 0
	add	r6 r12 r6
	lwi	r6, r6 0
	swi	r10, r3 5
	swi	r15, r3 6
	swi	r16, r3 7
	swi	r11, r3 8
	swi	r12, r3 9
	swi	r14, r3 10
	swi	r18, r3 11
	swi	r13, r3 12
	addi	r2, r6 0 #
	addi	r1, r16 0 #
	addi	r28, r11 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
	lwi	r1, r3 12
	sli	r1, r1, 0
	lwi	r2, r3 11
	add	r1 r2 r1
	lwi	r1, r1 0
	lwi	r5, r3 10
	seq	r30, r1, r5
	jeql	r30, r0, beq_else.21885
	jl	beq_cont.21886
beq_else.21885:
	sli	r1, r1, 0
	lwi	r6, r3 9
	add	r1 r6 r1
	lwi	r1, r1 0
	lwi	r7, r3 7
	lwi	r8, r3 0
	lwi	r28, r3 8
	addi	r5, r8 0 #
	addi	r2, r1 0 #
	addi	r1, r7 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
	lwi	r1, r3 6
	sli	r1, r1, 0
	lwi	r2, r3 11
	add	r1 r2 r1
	lwi	r1, r1 0
	lwi	r5, r3 10
	seq	r30, r1, r5
	jeql	r30, r0, beq_else.21887
	jl	beq_cont.21888
beq_else.21887:
	sli	r1, r1, 0
	lwi	r5, r3 9
	add	r1 r5 r1
	lwi	r1, r1 0
	lwi	r5, r3 7
	lwi	r6, r3 0
	lwi	r28, r3 8
	addi	r2, r1 0 #
	addi	r1, r5 0 #
	addi	r5, r6 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
	addi	r1, r0 4
	lwi	r2, r3 11
	lwi	r5, r3 0
	lwi	r28, r3 5
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
beq_cont.21888:
beq_cont.21886:
beq_cont.21884:
	jl	beq_cont.21882
beq_else.21881:
	swi	r10, r3 5
	swi	r15, r3 6
	swi	r13, r3 12
	swi	r11, r3 8
	swi	r12, r3 9
	swi	r14, r3 10
	swi	r18, r3 11
	swi	r6, r3 13
	swi	r8, r3 14
	swi	r16, r3 7
	addi	r2, r5 0 #
	addi	r1, r19 0 #
	addi	r28, r9 0 #
	addi	r5, r7 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -16
	lwi	r31, r3 15
	lwi	r2, r3 7
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21889
	jl	beq_cont.21890
beq_else.21889:
	sli	r1, r2, 0
	lwi	r5, r3 14
	add	r0 r5 r1
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r1, r2, 0
	lwi	r5, r3 13
	add	r0 r5 r1
	flwi	f2, r0 0
	sub	r0 r0 r0
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21891
	addi	r1, r0 0
	jl	ble_cont.21892
ble_else.21891:
	addi	r1, r0 1
ble_cont.21892:
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21893
	jl	beq_cont.21894
beq_else.21893:
	lwi	r1, r3 3
	sli	r5, r1, 0
	lwi	r6, r3 11
	add	r5 r6 r5
	lwi	r5, r5 0
	lwi	r7, r3 10
	seq	r30, r5, r7
	jeql	r30, r0, beq_else.21895
	jl	beq_cont.21896
beq_else.21895:
	sli	r5, r5, 0
	lwi	r8, r3 9
	add	r5 r8 r5
	lwi	r5, r5 0
	lwi	r9, r3 0
	lwi	r28, r3 8
	addi	r1, r2 0 #
	addi	r2, r5 0 #
	addi	r5, r9 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -16
	lwi	r31, r3 15
	lwi	r1, r3 12
	sli	r1, r1, 0
	lwi	r2, r3 11
	add	r1 r2 r1
	lwi	r1, r1 0
	lwi	r5, r3 10
	seq	r30, r1, r5
	jeql	r30, r0, beq_else.21897
	jl	beq_cont.21898
beq_else.21897:
	sli	r1, r1, 0
	lwi	r6, r3 9
	add	r1 r6 r1
	lwi	r1, r1 0
	lwi	r7, r3 7
	lwi	r8, r3 0
	lwi	r28, r3 8
	addi	r5, r8 0 #
	addi	r2, r1 0 #
	addi	r1, r7 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -16
	lwi	r31, r3 15
	lwi	r1, r3 6
	sli	r1, r1, 0
	lwi	r2, r3 11
	add	r1 r2 r1
	lwi	r1, r1 0
	lwi	r5, r3 10
	seq	r30, r1, r5
	jeql	r30, r0, beq_else.21899
	jl	beq_cont.21900
beq_else.21899:
	sli	r1, r1, 0
	lwi	r5, r3 9
	add	r1 r5 r1
	lwi	r1, r1 0
	lwi	r5, r3 7
	lwi	r6, r3 0
	lwi	r28, r3 8
	addi	r2, r1 0 #
	addi	r1, r5 0 #
	addi	r5, r6 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -16
	lwi	r31, r3 15
	addi	r1, r0 4
	lwi	r2, r3 11
	lwi	r5, r3 0
	lwi	r28, r3 5
	swi	r31 r3 15
	addi	r3, r3, 16
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -16
	lwi	r31, r3 15
beq_cont.21900:
beq_cont.21898:
beq_cont.21896:
beq_cont.21894:
beq_cont.21890:
beq_cont.21882:
	lwi	r1, r3 3
	lwi	r2, r3 4
	add	r1, r2, r1
	lwi	r2, r3 1
	lwi	r5, r3 0
	lwi	r28, r3 2
	lwi	r29, r28 0
	jr	r29
solve_each_element_fast.2928:
	lwi	r6, r28 15
	lwi	r7, r28 14
	lwi	r8, r28 13
	lwi	r9, r28 12
	lwi	r10, r28 11
	lwi	r11, r28 10
	lwi	r12, r28 9
	lwi	r13, r28 8
	lwi	r14, r28 7
	lwi	r15, r28 6
	lwi	r16, r28 5
	lwi	r17, r28 4
	lwi	r18, r28 3
	lwi	r19, r28 2
	flwi	f1, r28 1 #
	lwi	r20, r5 0
	sli	r21, r1, 0
	add	r21 r2 r21
	lwi	r21, r21 0
	seq	r30, r21, r17
	jeql	r30, r0, beq_else.21901
	jr	r31
beq_else.21901:
	swi	r12, r3 0
	swi	r14, r3 1
	swi	r13, r3 2
	swi	r15, r3 3
	swi	r11, r3 4
	swi	r17, r3 5
	swi	r16, r3 6
	swi	r7, r3 7
	swi	r20, r3 8
	swi	r6, r3 9
	fswi	f1, r3 10
	swi	r9, r3 11
	swi	r5, r3 12
	swi	r2, r3 13
	swi	r28, r3 14
	swi	r19, r3 15
	swi	r1, r3 16
	swi	r10, r3 17
	swi	r21, r3 18
	swi	r18, r3 19
	addi	r2, r5 0 #
	addi	r1, r21 0 #
	addi	r28, r8 0 #
	swi	r31 r3 20
	addi	r3, r3, 21
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -21
	lwi	r31, r3 20
	lwi	r2, r3 19
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21903
	lwi	r1, r3 18
	sli	r1, r1, 0
	lwi	r5, r3 17
	add	r1 r5 r1
	lwi	r1, r1 0
	lwi	r1, r1 6
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21904
	jr	r31
beq_else.21904:
	lwi	r1, r3 15
	lwi	r2, r3 16
	add	r1, r2, r1
	lwi	r2, r3 13
	lwi	r5, r3 12
	lwi	r28, r3 14
	lwi	r29, r28 0
	jr	r29
beq_else.21903:
	sli	r5, r2, 0
	lwi	r6, r3 11
	add	r0 r6 r5
	flwi	f1, r0 0
	sub	r0 r0 r0
	flwi	f2, r3 10 #off
	fgt	r30 f1 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21906
	addi	r5, r0 0
	jl	ble_cont.21907
ble_else.21906:
	addi	r5, r0 1
ble_cont.21907:
	seq	r30, r5, r2
	jeql	r30, r0, beq_else.21908
	jl	beq_cont.21909
beq_else.21908:
	sli	r5, r2, 0
	lwi	r6, r3 9
	add	r0 r6 r5
	flwi	f2, r0 0
	sub	r0 r0 r0
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21910
	addi	r5, r0 0
	jl	ble_cont.21911
ble_else.21910:
	addi	r5, r0 1
ble_cont.21911:
	seq	r30, r5, r2
	jeql	r30, r0, beq_else.21912
	jl	beq_cont.21913
beq_else.21912:
	flwl	%f2 l.12163
	fadd	f1, f1, f2
	sli	r5, r2, 0
	lwi	r7, r3 8
	add	r0 r7 r5
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f1
	sli	r5, r2, 0
	lwi	r8, r3 7
	add	r0 r8 r5
	flwi	f3, r0 0
	sub	r0 r0 r0
	fadd	f2, f2, f3
	lwi	r5, r3 15
	sli	r9, r5, 0
	add	r0 r7 r9
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f3, f3, f1
	sli	r9, r5, 0
	add	r0 r8 r9
	flwi	f4, r0 0
	sub	r0 r0 r0
	fadd	f3, f3, f4
	lwi	r9, r3 6
	sli	r10, r9, 0
	add	r0 r7 r10
	flwi	f4, r0 0
	sub	r0 r0 r0
	fmul	f4, f4, f1
	sli	r7, r9, 0
	add	r0 r8 r7
	flwi	f5, r0 0
	sub	r0 r0 r0
	fadd	f4, f4, f5
	sli	r7, r2, 0
	lwi	r8, r3 13
	add	r7 r8 r7
	lwi	r7, r7 0
	lwi	r10, r3 5
	swi	r1, r3 20
	fswi	f4, r3 22
	fswi	f3, r3 24
	fswi	f2, r3 26
	fswi	f1, r3 28
	seq	r30, r7, r10
	jeql	r30, r0, beq_else.21918
	addi	r1, r0 1
	jl	beq_cont.21919
beq_else.21918:
	sli	r7, r7, 0
	lwi	r10, r3 17
	add	r7 r10 r7
	lwi	r7, r7 0
	lwi	r28, r3 4
	addi	r1, r7 0 #
	fadd	f1, f2 f0
	fadd	f2, f3 f0
	fadd	f3, f4 f0
	swi	r31 r3 29
	addi	r3, r3, 30
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -30
	lwi	r31, r3 29
	lwi	r2, r3 19
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21920
	flwi	f1, r3 26 #off
	flwi	f2, r3 24 #off
	flwi	f3, r3 22 #off
	lwi	r1, r3 15
	lwi	r5, r3 13
	lwi	r28, r3 3
	addi	r2, r5 0 #
	swi	r31 r3 29
	addi	r3, r3, 30
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -30
	lwi	r31, r3 29
	jl	beq_cont.21921
beq_else.21920:
	addi	r1, r0 0
beq_cont.21921:
beq_cont.21919:
	lwi	r2, r3 19
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21922
	jl	beq_cont.21923
beq_else.21922:
	sli	r1, r2, 0
	lwi	r5, r3 9
	flwi	f1, r3 28 #off
	add	r0 r5 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r1, r2, 0
	lwi	r5, r3 2
	flwi	f1, r3 26 #off
	add	r0 r5 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 15
	sli	r6, r1, 0
	flwi	f1, r3 24 #off
	add	r0 r5 r6
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r6, r3 6
	sli	r6, r6, 0
	flwi	f1, r3 22 #off
	add	r0 r5 r6
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r5, r2, 0
	lwi	r6, r3 1
	lwi	r7, r3 18
	add	r0 r6 r5
	swi	r7, r0 0
	sub	r0 r0 r0
	sli	r2, r2, 0
	lwi	r5, r3 0
	lwi	r6, r3 20
	add	r0 r5 r2
	swi	r6, r0 0
	sub	r0 r0 r0
beq_cont.21923:
beq_cont.21913:
beq_cont.21909:
	lwi	r1, r3 15
	lwi	r2, r3 16
	add	r1, r2, r1
	lwi	r2, r3 13
	lwi	r5, r3 12
	lwi	r28, r3 14
	lwi	r29, r28 0
	jr	r29
solve_one_or_network_fast.2932:
	lwi	r6, r28 5
	lwi	r7, r28 4
	lwi	r8, r28 3
	lwi	r9, r28 2
	lwi	r10, r28 1
	sli	r11, r1, 0
	add	r11 r2 r11
	lwi	r11, r11 0
	seq	r30, r11, r8
	jeql	r30, r0, beq_else.21924
	jr	r31
beq_else.21924:
	sli	r11, r11, 0
	add	r11 r7 r11
	lwi	r11, r11 0
	swi	r28, r3 0
	swi	r5, r3 1
	swi	r9, r3 2
	swi	r6, r3 3
	swi	r7, r3 4
	swi	r8, r3 5
	swi	r2, r3 6
	swi	r10, r3 7
	swi	r1, r3 8
	addi	r2, r11 0 #
	addi	r1, r9 0 #
	addi	r28, r6 0 #
	swi	r31 r3 9
	addi	r3, r3, 10
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -10
	lwi	r31, r3 9
	lwi	r1, r3 7
	lwi	r2, r3 8
	add	r2, r2, r1
	sli	r5, r2, 0
	lwi	r6, r3 6
	add	r5 r6 r5
	lwi	r5, r5 0
	lwi	r7, r3 5
	seq	r30, r5, r7
	jeql	r30, r0, beq_else.21926
	jr	r31
beq_else.21926:
	sli	r5, r5, 0
	lwi	r8, r3 4
	add	r5 r8 r5
	lwi	r5, r5 0
	lwi	r9, r3 2
	lwi	r10, r3 1
	lwi	r28, r3 3
	swi	r2, r3 9
	addi	r2, r5 0 #
	addi	r1, r9 0 #
	addi	r5, r10 0 #
	swi	r31 r3 10
	addi	r3, r3, 11
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -11
	lwi	r31, r3 10
	lwi	r1, r3 7
	lwi	r2, r3 9
	add	r2, r2, r1
	sli	r5, r2, 0
	lwi	r6, r3 6
	add	r5 r6 r5
	lwi	r5, r5 0
	lwi	r7, r3 5
	seq	r30, r5, r7
	jeql	r30, r0, beq_else.21928
	jr	r31
beq_else.21928:
	sli	r5, r5, 0
	lwi	r8, r3 4
	add	r5 r8 r5
	lwi	r5, r5 0
	lwi	r9, r3 2
	lwi	r10, r3 1
	lwi	r28, r3 3
	swi	r2, r3 10
	addi	r2, r5 0 #
	addi	r1, r9 0 #
	addi	r5, r10 0 #
	swi	r31 r3 11
	addi	r3, r3, 12
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -12
	lwi	r31, r3 11
	lwi	r1, r3 7
	lwi	r2, r3 10
	add	r2, r2, r1
	sli	r5, r2, 0
	lwi	r6, r3 6
	add	r5 r6 r5
	lwi	r5, r5 0
	lwi	r7, r3 5
	seq	r30, r5, r7
	jeql	r30, r0, beq_else.21930
	jr	r31
beq_else.21930:
	sli	r5, r5, 0
	lwi	r7, r3 4
	add	r5 r7 r5
	lwi	r5, r5 0
	lwi	r7, r3 2
	lwi	r8, r3 1
	lwi	r28, r3 3
	swi	r2, r3 11
	addi	r2, r5 0 #
	addi	r1, r7 0 #
	addi	r5, r8 0 #
	swi	r31 r3 12
	addi	r3, r3, 13
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -13
	lwi	r31, r3 12
	lwi	r1, r3 7
	lwi	r2, r3 11
	add	r1, r2, r1
	lwi	r2, r3 6
	lwi	r5, r3 1
	lwi	r28, r3 0
	lwi	r29, r28 0
	jr	r29
trace_or_matrix_fast.2936:
	lwi	r6, r28 11
	lwi	r7, r28 10
	lwi	r8, r28 9
	lwi	r9, r28 8
	lwi	r10, r28 7
	lwi	r11, r28 6
	lwi	r12, r28 5
	lwi	r13, r28 4
	lwi	r14, r28 3
	lwi	r15, r28 2
	lwi	r16, r28 1
	sli	r17, r1, 0
	add	r17 r2 r17
	lwi	r17, r17 0
	sli	r18, r15, 0
	add	r18 r17 r18
	lwi	r18, r18 0
	seq	r30, r18, r13
	jeql	r30, r0, beq_else.21932
	jr	r31
beq_else.21932:
	swi	r5, r3 0
	swi	r2, r3 1
	swi	r28, r3 2
	swi	r16, r3 3
	swi	r1, r3 4
	addi	r30, r0, 99
	seq	r30, r18, r30
	jeql	r30, r0, beq_else.21934
	sli	r6, r16, 0
	add	r6 r17 r6
	lwi	r6, r6 0
	seq	r30, r6, r13
	jeql	r30, r0, beq_else.21936
	jl	beq_cont.21937
beq_else.21936:
	sli	r6, r6, 0
	add	r6 r11 r6
	lwi	r6, r6 0
	swi	r9, r3 5
	swi	r14, r3 6
	swi	r15, r3 7
	swi	r10, r3 8
	swi	r11, r3 9
	swi	r13, r3 10
	swi	r17, r3 11
	swi	r12, r3 12
	addi	r2, r6 0 #
	addi	r1, r15 0 #
	addi	r28, r10 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
	lwi	r1, r3 12
	sli	r1, r1, 0
	lwi	r2, r3 11
	add	r1 r2 r1
	lwi	r1, r1 0
	lwi	r5, r3 10
	seq	r30, r1, r5
	jeql	r30, r0, beq_else.21938
	jl	beq_cont.21939
beq_else.21938:
	sli	r1, r1, 0
	lwi	r6, r3 9
	add	r1 r6 r1
	lwi	r1, r1 0
	lwi	r7, r3 7
	lwi	r8, r3 0
	lwi	r28, r3 8
	addi	r5, r8 0 #
	addi	r2, r1 0 #
	addi	r1, r7 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
	lwi	r1, r3 6
	sli	r1, r1, 0
	lwi	r2, r3 11
	add	r1 r2 r1
	lwi	r1, r1 0
	lwi	r5, r3 10
	seq	r30, r1, r5
	jeql	r30, r0, beq_else.21940
	jl	beq_cont.21941
beq_else.21940:
	sli	r1, r1, 0
	lwi	r5, r3 9
	add	r1 r5 r1
	lwi	r1, r1 0
	lwi	r5, r3 7
	lwi	r6, r3 0
	lwi	r28, r3 8
	addi	r2, r1 0 #
	addi	r1, r5 0 #
	addi	r5, r6 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
	addi	r1, r0 4
	lwi	r2, r3 11
	lwi	r5, r3 0
	lwi	r28, r3 5
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
beq_cont.21941:
beq_cont.21939:
beq_cont.21937:
	jl	beq_cont.21935
beq_else.21934:
	swi	r9, r3 5
	swi	r14, r3 6
	swi	r12, r3 12
	swi	r10, r3 8
	swi	r11, r3 9
	swi	r13, r3 10
	swi	r17, r3 11
	swi	r6, r3 13
	swi	r8, r3 14
	swi	r15, r3 7
	addi	r2, r5 0 #
	addi	r1, r18 0 #
	addi	r28, r7 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -16
	lwi	r31, r3 15
	lwi	r2, r3 7
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21942
	jl	beq_cont.21943
beq_else.21942:
	sli	r1, r2, 0
	lwi	r5, r3 14
	add	r0 r5 r1
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r1, r2, 0
	lwi	r5, r3 13
	add	r0 r5 r1
	flwi	f2, r0 0
	sub	r0 r0 r0
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21944
	addi	r1, r0 0
	jl	ble_cont.21945
ble_else.21944:
	addi	r1, r0 1
ble_cont.21945:
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21946
	jl	beq_cont.21947
beq_else.21946:
	lwi	r1, r3 3
	sli	r5, r1, 0
	lwi	r6, r3 11
	add	r5 r6 r5
	lwi	r5, r5 0
	lwi	r7, r3 10
	seq	r30, r5, r7
	jeql	r30, r0, beq_else.21948
	jl	beq_cont.21949
beq_else.21948:
	sli	r5, r5, 0
	lwi	r8, r3 9
	add	r5 r8 r5
	lwi	r5, r5 0
	lwi	r9, r3 0
	lwi	r28, r3 8
	addi	r1, r2 0 #
	addi	r2, r5 0 #
	addi	r5, r9 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -16
	lwi	r31, r3 15
	lwi	r1, r3 12
	sli	r1, r1, 0
	lwi	r2, r3 11
	add	r1 r2 r1
	lwi	r1, r1 0
	lwi	r5, r3 10
	seq	r30, r1, r5
	jeql	r30, r0, beq_else.21950
	jl	beq_cont.21951
beq_else.21950:
	sli	r1, r1, 0
	lwi	r6, r3 9
	add	r1 r6 r1
	lwi	r1, r1 0
	lwi	r7, r3 7
	lwi	r8, r3 0
	lwi	r28, r3 8
	addi	r5, r8 0 #
	addi	r2, r1 0 #
	addi	r1, r7 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -16
	lwi	r31, r3 15
	lwi	r1, r3 6
	sli	r1, r1, 0
	lwi	r2, r3 11
	add	r1 r2 r1
	lwi	r1, r1 0
	lwi	r5, r3 10
	seq	r30, r1, r5
	jeql	r30, r0, beq_else.21952
	jl	beq_cont.21953
beq_else.21952:
	sli	r1, r1, 0
	lwi	r5, r3 9
	add	r1 r5 r1
	lwi	r1, r1 0
	lwi	r5, r3 7
	lwi	r6, r3 0
	lwi	r28, r3 8
	addi	r2, r1 0 #
	addi	r1, r5 0 #
	addi	r5, r6 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -16
	lwi	r31, r3 15
	addi	r1, r0 4
	lwi	r2, r3 11
	lwi	r5, r3 0
	lwi	r28, r3 5
	swi	r31 r3 15
	addi	r3, r3, 16
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -16
	lwi	r31, r3 15
beq_cont.21953:
beq_cont.21951:
beq_cont.21949:
beq_cont.21947:
beq_cont.21943:
beq_cont.21935:
	lwi	r1, r3 3
	lwi	r2, r3 4
	add	r1, r2, r1
	lwi	r2, r3 1
	lwi	r5, r3 0
	lwi	r28, r3 2
	lwi	r29, r28 0
	jr	r29
get_nvector_second.2946:
	lwi	r2, r28 6
	lwi	r5, r28 5
	lwi	r6, r28 4
	lwi	r7, r28 3
	lwi	r8, r28 2
	lwi	r9, r28 1
	sli	r10, r8, 0
	add	r0 r6 r10
	flwi	f1, r0 0
	sub	r0 r0 r0
	lwi	r10, r1 5
	flwi	f2, r10 0 #
	fsub	f1, f1, f2
	sli	r10, r9, 0
	add	r0 r6 r10
	flwi	f2, r0 0
	sub	r0 r0 r0
	lwi	r10, r1 5
	flwi	f3, r10 1 #
	fsub	f2, f2, f3
	sli	r10, r7, 0
	add	r0 r6 r10
	flwi	f3, r0 0
	sub	r0 r0 r0
	lwi	r6, r1 5
	flwi	f4, r6 2 #
	fsub	f3, f3, f4
	lwi	r6, r1 4
	flwi	f4, r6 0 #
	fmul	f4, f1, f4
	lwi	r6, r1 4
	flwi	f5, r6 1 #
	fmul	f5, f2, f5
	lwi	r6, r1 4
	flwi	f6, r6 2 #
	fmul	f6, f3, f6
	lwi	r6, r1 3
	seq	r30, r6, r8
	jeql	r30, r0, beq_else.21954
	sli	r6, r8, 0
	add	r0 r5 r6
	fswi	f4, r0 0
	sub	r0 r0 r0
	sli	r6, r9, 0
	add	r0 r5 r6
	fswi	f5, r0 0
	sub	r0 r0 r0
	sli	r6, r7, 0
	add	r0 r5 r6
	fswi	f6, r0 0
	sub	r0 r0 r0
	jl	beq_cont.21955
beq_else.21954:
	lwi	r6, r1 9
	flwi	f7, r6 2 #
	fmul	f7, f2, f7
	lwi	r6, r1 9
	flwi	f8, r6 1 #
	fmul	f8, f3, f8
	fadd	f7, f7, f8
	flwl	%f8 l.11779
	fmul	f7, f7, f8
	fadd	f4, f4, f7
	sli	r6, r8, 0
	add	r0 r5 r6
	fswi	f4, r0 0
	sub	r0 r0 r0
	lwi	r6, r1 9
	flwi	f4, r6 2 #
	fmul	f4, f1, f4
	lwi	r6, r1 9
	flwi	f7, r6 0 #
	fmul	f3, f3, f7
	fadd	f3, f4, f3
	flwl	%f4 l.11779
	fmul	f3, f3, f4
	fadd	f3, f5, f3
	sli	r6, r9, 0
	add	r0 r5 r6
	fswi	f3, r0 0
	sub	r0 r0 r0
	lwi	r6, r1 9
	flwi	f3, r6 1 #
	fmul	f1, f1, f3
	lwi	r6, r1 9
	flwi	f3, r6 0 #
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	flwl	%f2 l.11779
	fmul	f1, f1, f2
	fadd	f1, f6, f1
	sli	r6, r7, 0
	add	r0 r5 r6
	fswi	f1, r0 0
	sub	r0 r0 r0
beq_cont.21955:
	lwi	r1, r1 6
	addi	r28, r2 0 #
	addi	r2, r1 0 #
	addi	r1, r5 0 #
	lwi	r29, r28 0
	jr	r29
utexture.2951:
	lwi	r5, r28 7
	lwi	r6, r28 6
	lwi	r7, r28 5
	lwi	r8, r28 4
	lwi	r9, r28 3
	flwi	f1, r28 2 #
	flwi	f2, r28 1 #
	lwi	r10, r1 0
	lwi	r11, r1 8
	flwi	f3, r11 0 #
	sli	r11, r8, 0
	add	r0 r5 r11
	fswi	f3, r0 0
	sub	r0 r0 r0
	lwi	r11, r1 8
	flwi	f3, r11 1 #
	sli	r11, r9, 0
	add	r0 r5 r11
	fswi	f3, r0 0
	sub	r0 r0 r0
	lwi	r11, r1 8
	flwi	f3, r11 2 #
	sli	r11, r6, 0
	add	r0 r5 r11
	fswi	f3, r0 0
	sub	r0 r0 r0
	seq	r30, r10, r9
	jeql	r30, r0, beq_else.21956
	sli	r7, r8, 0
	add	r0 r2 r7
	flwi	f1, r0 0
	sub	r0 r0 r0
	lwi	r7, r1 5
	flwi	f2, r7 0 #
	fsub	f1, f1, f2
	flwl	%f2 l.12337
	fmul	f3, f1, f2
	swi	r5, r3 0
	swi	r9, r3 1
	swi	r8, r3 2
	fswi	f2, r3 4
	swi	r1, r3 5
	swi	r2, r3 6
	swi	r6, r3 7
	fswi	f1, r3 8
	fadd	f1, f3 f0
	swi	r31 r3 9
	addi	r3, r3, 10
	jal	min_caml_floor
	addi	r3, r3, -10
	lwi	r31 r3 9
	flwl	%f2 l.12338
	fmul	f1, f1, f2
	flwi	f3, r3 8 #off
	fsub	f1, f3, f1
	flwl	%f3 l.12328
	fgt	r30 f3 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21958
	addi	r1, r0 0
	jl	ble_cont.21959
ble_else.21958:
	addi	r1, r0 1
ble_cont.21959:
	lwi	r2, r3 7
	sli	r2, r2, 0
	lwi	r5, r3 6
	add	r0 r5 r2
	flwi	f1, r0 0
	sub	r0 r0 r0
	lwi	r2, r3 5
	lwi	r2, r2 5
	flwi	f4, r2 2 #
	fsub	f1, f1, f4
	flwi	f4, r3 4 #off
	fmul	f4, f1, f4
	swi	r1, r3 9
	fswi	f3, r3 10
	fswi	f1, r3 12
	fswi	f2, r3 14
	fadd	f1, f4 f0
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	min_caml_floor
	addi	r3, r3, -16
	lwi	r31 r3 15
	flwi	f2, r3 14 #off
	fmul	f1, f1, f2
	flwi	f2, r3 12 #off
	fsub	f1, f2, f1
	flwi	f2, r3 10 #off
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21962
	addi	r1, r0 0
	jl	ble_cont.21963
ble_else.21962:
	addi	r1, r0 1
ble_cont.21963:
	lwi	r2, r3 2
	lwi	r5, r3 9
	seq	r30, r5, r2
	jeql	r30, r0, beq_else.21964
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21966
	flwl	%f1 l.12341
	jl	beq_cont.21967
beq_else.21966:
	flwl	%f1 l.11783
beq_cont.21967:
	jl	beq_cont.21965
beq_else.21964:
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21968
	flwl	%f1 l.11783
	jl	beq_cont.21969
beq_else.21968:
	flwl	%f1 l.12341
beq_cont.21969:
beq_cont.21965:
	lwi	r1, r3 1
	sli	r1, r1, 0
	lwi	r2, r3 0
	add	r0 r2 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	jr	r31
beq_else.21956:
	seq	r30, r10, r6
	jeql	r30, r0, beq_else.21971
	sli	r1, r9, 0
	add	r0 r2 r1
	flwi	f2, r0 0
	sub	r0 r0 r0
	flwl	%f3 l.12332
	fmul	f2, f2, f3
	swi	r9, r3 1
	swi	r5, r3 0
	swi	r8, r3 2
	fswi	f1, r3 16
	fadd	f1, f2 f0
	swi	r31 r3 17
	addi	r3, r3, 18
	jal	sin.2572
	addi	r3, r3, -18
	lwi	r31 r3 17
	fmul	f1, f1, f1
	flwi	f2, r3 16 #off
	fmul	f3, f2, f1
	lwi	r1, r3 2
	sli	r1, r1, 0
	lwi	r2, r3 0
	add	r0 r2 r1
	fswi	f3, r0 0
	sub	r0 r0 r0
	flwl	%f3 l.11794
	fsub	f1, f3, f1
	fmul	f1, f2, f1
	lwi	r1, r3 1
	sli	r1, r1, 0
	add	r0 r2 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	jr	r31
beq_else.21971:
	seq	r30, r10, r7
	jeql	r30, r0, beq_else.21974
	sli	r7, r8, 0
	add	r0 r2 r7
	flwi	f2, r0 0
	sub	r0 r0 r0
	lwi	r7, r1 5
	flwi	f3, r7 0 #
	fsub	f2, f2, f3
	sli	r7, r6, 0
	add	r0 r2 r7
	flwi	f3, r0 0
	sub	r0 r0 r0
	lwi	r1, r1 5
	flwi	f4, r1 2 #
	fsub	f3, f3, f4
	fmul	f2, f2, f2
	fmul	f3, f3, f3
	fadd	f2, f2, f3
	swi	r6, r3 7
	swi	r5, r3 0
	swi	r9, r3 1
	fswi	f1, r3 16
	fadd	f1, f2 f0
	swi	r31 r3 19
	addi	r3, r3, 20
	jal	min_caml_sqrt
	addi	r3, r3, -20
	lwi	r31 r3 19
	flwl	%f2 l.12328
	fdiv	f1, f1, f2
	fswi	f1, r3 20
	swi	r31 r3 21
	addi	r3, r3, 22
	jal	min_caml_floor
	addi	r3, r3, -22
	lwi	r31 r3 21
	flwi	f2, r3 20 #off
	fsub	f1, f2, f1
	flwl	%f2 l.12317
	fmul	f1, f1, f2
	flwl	%f2 l.11781
	fsub	f1, f2, f1
	swi	r31 r3 21
	addi	r3, r3, 22
	jal	sin.2572
	addi	r3, r3, -22
	lwi	r31 r3 21
	fmul	f1, f1, f1
	flwi	f2, r3 16 #off
	fmul	f3, f1, f2
	lwi	r1, r3 1
	sli	r1, r1, 0
	lwi	r2, r3 0
	add	r0 r2 r1
	fswi	f3, r0 0
	sub	r0 r0 r0
	flwl	%f3 l.11794
	fsub	f1, f3, f1
	fmul	f1, f1, f2
	lwi	r1, r3 7
	sli	r1, r1, 0
	add	r0 r2 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	jr	r31
beq_else.21974:
	addi	r30, r0, 4
	seq	r30, r10, r30
	jeql	r30, r0, beq_else.21978
	sli	r7, r8, 0
	add	r0 r2 r7
	flwi	f3, r0 0
	sub	r0 r0 r0
	lwi	r7, r1 5
	flwi	f4, r7 0 #
	fsub	f3, f3, f4
	lwi	r7, r1 4
	flwi	f4, r7 0 #
	swi	r5, r3 0
	fswi	f1, r3 16
	swi	r9, r3 1
	swi	r8, r3 2
	fswi	f2, r3 24
	swi	r1, r3 5
	swi	r2, r3 6
	swi	r6, r3 7
	fswi	f3, r3 26
	fadd	f1, f4 f0
	swi	r31 r3 27
	addi	r3, r3, 28
	jal	min_caml_sqrt
	addi	r3, r3, -28
	lwi	r31 r3 27
	flwi	f2, r3 26 #off
	fmul	f1, f2, f1
	lwi	r1, r3 7
	sli	r2, r1, 0
	lwi	r5, r3 6
	add	r0 r5 r2
	flwi	f2, r0 0
	sub	r0 r0 r0
	lwi	r2, r3 5
	lwi	r6, r2 5
	flwi	f3, r6 2 #
	fsub	f2, f2, f3
	lwi	r6, r2 4
	flwi	f3, r6 2 #
	fswi	f1, r3 28
	fswi	f2, r3 30
	fadd	f1, f3 f0
	swi	r31 r3 31
	addi	r3, r3, 32
	jal	min_caml_sqrt
	addi	r3, r3, -32
	lwi	r31 r3 31
	flwi	f2, r3 30 #off
	fmul	f1, f2, f1
	flwi	f2, r3 28 #off
	fmul	f3, f2, f2
	fmul	f4, f1, f1
	fadd	f3, f3, f4
	flwi	f4, r3 24 #off
	fgt	r30 f4 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21984
	fadd	f5, f2 f0
	jl	ble_cont.21985
ble_else.21984:
	fsub	f5, f4, f2
ble_cont.21985:
	flwl	%f6 l.12314
	fgt	r30 f6 f5
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21986
	addi	r1, r0 0
	jl	ble_cont.21987
ble_else.21986:
	addi	r1, r0 1
ble_cont.21987:
	lwi	r2, r3 2
	fswi	f6, r3 32
	fswi	f3, r3 34
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.21990
	fdiv	f1, f1, f2
	fgt	r30 f4 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21992
	jl	ble_cont.21993
ble_else.21992:
	fsub	f1, f4, f1
ble_cont.21993:
	swi	r31 r3 35
	addi	r3, r3, 36
	jal	atan.2576
	addi	r3, r3, -36
	lwi	r31 r3 35
	flwl	%f2 l.12316
	fmul	f1, f1, f2
	flwl	%f2 l.12317
	fdiv	f1, f1, f2
	jl	beq_cont.21991
beq_else.21990:
	flwl	%f1 l.12315
beq_cont.21991:
	fswi	f1, r3 36
	swi	r31 r3 37
	addi	r3, r3, 38
	jal	min_caml_floor
	addi	r3, r3, -38
	lwi	r31 r3 37
	flwi	f2, r3 36 #off
	fsub	f1, f2, f1
	lwi	r1, r3 1
	sli	r1, r1, 0
	lwi	r2, r3 6
	add	r0 r2 r1
	flwi	f2, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 5
	lwi	r2, r1 5
	flwi	f3, r2 1 #
	fsub	f2, f2, f3
	lwi	r1, r1 4
	flwi	f3, r1 1 #
	fswi	f1, r3 38
	fswi	f2, r3 40
	fadd	f1, f3 f0
	swi	r31 r3 41
	addi	r3, r3, 42
	jal	min_caml_sqrt
	addi	r3, r3, -42
	lwi	r31 r3 41
	flwi	f2, r3 40 #off
	fmul	f1, f2, f1
	flwi	f2, r3 34 #off
	flwi	f3, r3 24 #off
	fgt	r30 f3 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21997
	fadd	f4, f2 f0
	jl	ble_cont.21998
ble_else.21997:
	fsub	f4, f3, f2
ble_cont.21998:
	flwi	f5, r3 32 #off
	fgt	r30 f5 f4
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.21999
	addi	r1, r0 0
	jl	ble_cont.22000
ble_else.21999:
	addi	r1, r0 1
ble_cont.22000:
	lwi	r2, r3 2
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.22001
	fdiv	f1, f1, f2
	fgt	r30 f3 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.22003
	jl	ble_cont.22004
ble_else.22003:
	fsub	f1, f3, f1
ble_cont.22004:
	swi	r31 r3 41
	addi	r3, r3, 42
	jal	atan.2576
	addi	r3, r3, -42
	lwi	r31 r3 41
	flwl	%f2 l.12316
	fmul	f1, f1, f2
	flwl	%f2 l.12317
	fdiv	f1, f1, f2
	jl	beq_cont.22002
beq_else.22001:
	flwl	%f1 l.12315
beq_cont.22002:
	fswi	f1, r3 42
	swi	r31 r3 43
	addi	r3, r3, 44
	jal	min_caml_floor
	addi	r3, r3, -44
	lwi	r31 r3 43
	flwi	f2, r3 42 #off
	fsub	f1, f2, f1
	flwl	%f2 l.12321
	flwl	%f3 l.11779
	flwi	f4, r3 38 #off
	fsub	f4, f3, f4
	fmul	f4, f4, f4
	fsub	f2, f2, f4
	fsub	f1, f3, f1
	fmul	f1, f1, f1
	fsub	f1, f2, f1
	flwi	f2, r3 24 #off
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.22006
	addi	r1, r0 0
	jl	ble_cont.22007
ble_else.22006:
	addi	r1, r0 1
ble_cont.22007:
	lwi	r2, r3 2
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.22008
	jl	beq_cont.22009
beq_else.22008:
	flwl	%f1 l.11783
beq_cont.22009:
	flwi	f2, r3 16 #off
	fmul	f1, f2, f1
	flwl	%f2 l.12322
	fdiv	f1, f1, f2
	lwi	r1, r3 7
	sli	r1, r1, 0
	lwi	r2, r3 0
	add	r0 r2 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	jr	r31
beq_else.21978:
	jr	r31
add_light.2954:
	lwi	r1, r28 6
	lwi	r2, r28 5
	lwi	r5, r28 4
	lwi	r6, r28 3
	lwi	r7, r28 2
	flwi	f4, r28 1 #
	fgt	r30 f1 f4
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.22012
	addi	r8, r0 0
	jl	ble_cont.22013
ble_else.22012:
	addi	r8, r0 1
ble_cont.22013:
	seq	r30, r8, r6
	jeql	r30, r0, beq_else.22014
	jl	beq_cont.22015
beq_else.22014:
	sli	r8, r6, 0
	add	r0 r2 r8
	flwi	f5, r0 0
	sub	r0 r0 r0
	sli	r8, r6, 0
	add	r0 r1 r8
	flwi	f6, r0 0
	sub	r0 r0 r0
	fmul	f6, f1, f6
	fadd	f5, f5, f6
	sli	r8, r6, 0
	add	r0 r2 r8
	fswi	f5, r0 0
	sub	r0 r0 r0
	sli	r8, r7, 0
	add	r0 r2 r8
	flwi	f5, r0 0
	sub	r0 r0 r0
	sli	r8, r7, 0
	add	r0 r1 r8
	flwi	f6, r0 0
	sub	r0 r0 r0
	fmul	f6, f1, f6
	fadd	f5, f5, f6
	sli	r8, r7, 0
	add	r0 r2 r8
	fswi	f5, r0 0
	sub	r0 r0 r0
	sli	r8, r5, 0
	add	r0 r2 r8
	flwi	f5, r0 0
	sub	r0 r0 r0
	sli	r8, r5, 0
	add	r0 r1 r8
	flwi	f6, r0 0
	sub	r0 r0 r0
	fmul	f1, f1, f6
	fadd	f1, f5, f1
	sli	r1, r5, 0
	add	r0 r2 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
beq_cont.22015:
	fgt	r30 f2 f4
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.22016
	addi	r1, r0 0
	jl	ble_cont.22017
ble_else.22016:
	addi	r1, r0 1
ble_cont.22017:
	seq	r30, r1, r6
	jeql	r30, r0, beq_else.22018
	jr	r31
beq_else.22018:
	fmul	f1, f2, f2
	fmul	f1, f1, f1
	fmul	f1, f1, f3
	sli	r1, r6, 0
	add	r0 r2 r1
	flwi	f2, r0 0
	sub	r0 r0 r0
	fadd	f2, f2, f1
	sli	r1, r6, 0
	add	r0 r2 r1
	fswi	f2, r0 0
	sub	r0 r0 r0
	sli	r1, r7, 0
	add	r0 r2 r1
	flwi	f2, r0 0
	sub	r0 r0 r0
	fadd	f2, f2, f1
	sli	r1, r7, 0
	add	r0 r2 r1
	fswi	f2, r0 0
	sub	r0 r0 r0
	sli	r1, r5, 0
	add	r0 r2 r1
	flwi	f2, r0 0
	sub	r0 r0 r0
	fadd	f1, f2, f1
	sli	r1, r5, 0
	add	r0 r2 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	jr	r31
trace_reflections.2958:
	lwi	r5, r28 13
	lwi	r6, r28 12
	lwi	r7, r28 11
	lwi	r8, r28 10
	lwi	r9, r28 9
	lwi	r10, r28 8
	lwi	r11, r28 7
	lwi	r12, r28 6
	lwi	r13, r28 5
	lwi	r14, r28 4
	lwi	r15, r28 3
	lwi	r16, r28 2
	flwi	f3, r28 1 #
	sgt	r30, r15, r1
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22021
	sli	r17, r1, 0
	add	r8 r8 r17
	lwi	r8, r8 0
	lwi	r17, r8 1
	sli	r18, r15, 0
	add	r0 r6 r18
	fswi	f3, r0 0
	sub	r0 r0 r0
	sli	r18, r15, 0
	add	r18 r9 r18
	lwi	r18, r18 0
	swi	r28, r3 0
	swi	r1, r3 1
	fswi	f2, r3 2
	swi	r13, r3 3
	swi	r2, r3 4
	fswi	f1, r3 6
	swi	r14, r3 7
	swi	r16, r3 8
	swi	r10, r3 9
	swi	r17, r3 10
	swi	r7, r3 11
	swi	r9, r3 12
	swi	r8, r3 13
	swi	r11, r3 14
	swi	r12, r3 15
	swi	r6, r3 16
	swi	r15, r3 17
	addi	r2, r18 0 #
	addi	r1, r15 0 #
	addi	r28, r5 0 #
	addi	r5, r17 0 #
	swi	r31 r3 18
	addi	r3, r3, 19
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -19
	lwi	r31, r3 18
	lwi	r1, r3 17
	sli	r2, r1, 0
	lwi	r5, r3 16
	add	r0 r5 r2
	flwi	f1, r0 0
	sub	r0 r0 r0
	flwl	%f2 l.12184
	fgt	r30 f1 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.22023
	addi	r2, r0 0
	jl	ble_cont.22024
ble_else.22023:
	addi	r2, r0 1
ble_cont.22024:
	seq	r30, r2, r1
	jeql	r30, r0, beq_else.22025
	addi	r2, r0 0
	jl	beq_cont.22026
beq_else.22025:
	flwl	%f2 l.12362
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.22027
	addi	r2, r0 0
	jl	ble_cont.22028
ble_else.22027:
	addi	r2, r0 1
ble_cont.22028:
beq_cont.22026:
	seq	r30, r2, r1
	jeql	r30, r0, beq_else.22029
	jl	beq_cont.22030
beq_else.22029:
	sli	r2, r1, 0
	lwi	r5, r3 15
	add	r2 r5 r2
	lwi	r2, r2 0
	addi	r5, r0 4
	mul	r2, r2, r5
	sli	r5, r1, 0
	lwi	r6, r3 14
	add	r5 r6 r5
	lwi	r5, r5 0
	add	r2, r2, r5
	lwi	r5, r3 13
	lwi	r6, r5 0
	seq	r30, r2, r6
	jeql	r30, r0, beq_else.22031
	sli	r2, r1, 0
	lwi	r6, r3 12
	add	r2 r6 r2
	lwi	r2, r2 0
	lwi	r28, r3 11
	swi	r31 r3 18
	addi	r3, r3, 19
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -19
	lwi	r31, r3 18
	lwi	r2, r3 17
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.22033
	lwi	r1, r3 10
	lwi	r5, r1 0
	sli	r6, r2, 0
	lwi	r7, r3 9
	add	r0 r7 r6
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r6, r2, 0
	add	r0 r5 r6
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f1, f1, f2
	lwi	r6, r3 8
	sli	r8, r6, 0
	add	r0 r7 r8
	flwi	f2, r0 0
	sub	r0 r0 r0
	sli	r8, r6, 0
	add	r0 r5 r8
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	lwi	r8, r3 7
	sli	r9, r8, 0
	add	r0 r7 r9
	flwi	f2, r0 0
	sub	r0 r0 r0
	sli	r7, r8, 0
	add	r0 r5 r7
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	lwi	r5, r3 13
	flwi	f2, r5 2 #
	flwi	f3, r3 6 #off
	fmul	f4, f2, f3
	fmul	f1, f4, f1
	lwi	r1, r1 0
	sli	r5, r2, 0
	lwi	r7, r3 4
	add	r0 r7 r5
	flwi	f4, r0 0
	sub	r0 r0 r0
	sli	r2, r2, 0
	add	r0 r1 r2
	flwi	f5, r0 0
	sub	r0 r0 r0
	fmul	f4, f4, f5
	sli	r2, r6, 0
	add	r0 r7 r2
	flwi	f5, r0 0
	sub	r0 r0 r0
	sli	r2, r6, 0
	add	r0 r1 r2
	flwi	f6, r0 0
	sub	r0 r0 r0
	fmul	f5, f5, f6
	fadd	f4, f4, f5
	sli	r2, r8, 0
	add	r0 r7 r2
	flwi	f5, r0 0
	sub	r0 r0 r0
	sli	r2, r8, 0
	add	r0 r1 r2
	flwi	f6, r0 0
	sub	r0 r0 r0
	fmul	f5, f5, f6
	fadd	f4, f4, f5
	fmul	f2, f2, f4
	flwi	f4, r3 2 #off
	lwi	r28, r3 3
	fadd	f3, f4 f0
	swi	r31 r3 18
	addi	r3, r3, 19
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -19
	lwi	r31, r3 18
	jl	beq_cont.22034
beq_else.22033:
beq_cont.22034:
	jl	beq_cont.22032
beq_else.22031:
beq_cont.22032:
beq_cont.22030:
	lwi	r1, r3 8
	lwi	r2, r3 1
	sub	r1, r2, r1
	flwi	f1, r3 6 #off
	flwi	f2, r3 2 #off
	lwi	r2, r3 4
	lwi	r28, r3 0
	lwi	r29, r28 0
	jr	r29
ble_else.22021:
	jr	r31
trace_ray.2963:
	lwi	r6, r28 28
	lwi	r7, r28 27
	lwi	r8, r28 26
	lwi	r9, r28 25
	lwi	r10, r28 24
	lwi	r11, r28 23
	lwi	r12, r28 22
	lwi	r13, r28 21
	lwi	r14, r28 20
	lwi	r15, r28 19
	lwi	r16, r28 18
	lwi	r17, r28 17
	lwi	r18, r28 16
	lwi	r19, r28 15
	lwi	r20, r28 14
	lwi	r21, r28 13
	lwi	r22, r28 12
	lwi	r23, r28 11
	lwi	r24, r28 10
	lwi	r25, r28 9
	lwi	r26, r28 8
	lwi	r27, r28 7
	swi	r7, r3 0
	lwi	r7, r28 6
	swi	r19, r3 1
	lwi	r19, r28 5
	swi	r14, r3 2
	lwi	r14, r28 4
	swi	r20, r3 3
	lwi	r20, r28 3
	flwi	f3, r28 2 #
	flwi	f4, r28 1 #
	swi	r11, r3 4
	addi	r11, r0 4
	addi	r30, r0, 4
	sgt	r30, r1, r30
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22036
	swi	r27, r3 5
	lwi	r27, r5 2
	swi	r13, r3 6
	sli	r13, r14, 0
	add	r0 r9 r13
	fswi	f3, r0 0
	sub	r0 r0 r0
	sli	r13, r14, 0
	add	r13 r16 r13
	lwi	r13, r13 0
	swi	r28, r3 7
	fswi	f2, r3 8
	swi	r16, r3 9
	swi	r10, r3 10
	swi	r5, r3 11
	swi	r11, r3 12
	swi	r6, r3 13
	swi	r12, r3 14
	swi	r23, r3 15
	swi	r25, r3 16
	swi	r18, r3 17
	swi	r22, r3 18
	swi	r17, r3 19
	swi	r24, r3 20
	swi	r15, r3 21
	swi	r26, r3 22
	fswi	f1, r3 24
	fswi	f4, r3 26
	swi	r7, r3 27
	swi	r20, r3 28
	swi	r21, r3 29
	swi	r2, r3 30
	swi	r27, r3 31
	swi	r19, r3 32
	swi	r1, r3 33
	swi	r9, r3 34
	swi	r14, r3 35
	addi	r5, r2 0 #
	addi	r1, r14 0 #
	addi	r28, r8 0 #
	addi	r2, r13 0 #
	swi	r31 r3 36
	addi	r3, r3, 37
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -37
	lwi	r31, r3 36
	lwi	r1, r3 35
	sli	r2, r1, 0
	lwi	r5, r3 34
	add	r0 r5 r2
	flwi	f1, r0 0
	sub	r0 r0 r0
	flwl	%f2 l.12184
	fgt	r30 f1 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.22039
	addi	r2, r0 0
	jl	ble_cont.22040
ble_else.22039:
	addi	r2, r0 1
ble_cont.22040:
	seq	r30, r2, r1
	jeql	r30, r0, beq_else.22041
	addi	r2, r0 0
	jl	beq_cont.22042
beq_else.22041:
	flwl	%f2 l.12362
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.22043
	addi	r2, r0 0
	jl	ble_cont.22044
ble_else.22043:
	addi	r2, r0 1
ble_cont.22044:
beq_cont.22042:
	seq	r30, r2, r1
	jeql	r30, r0, beq_else.22045
	lwi	r2, r3 33
	sli	r5, r2, 0
	lwi	r6, r3 31
	lwi	r7, r3 32
	add	r0 r6 r5
	swi	r7, r0 0
	sub	r0 r0 r0
	seq	r30, r2, r1
	jeql	r30, r0, beq_else.22046
	jr	r31
beq_else.22046:
	sli	r2, r1, 0
	lwi	r5, r3 30
	add	r0 r5 r2
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r2, r1, 0
	lwi	r6, r3 29
	add	r0 r6 r2
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f1, f1, f2
	lwi	r2, r3 28
	sli	r7, r2, 0
	add	r0 r5 r7
	flwi	f2, r0 0
	sub	r0 r0 r0
	sli	r7, r2, 0
	add	r0 r6 r7
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	lwi	r7, r3 27
	sli	r8, r7, 0
	add	r0 r5 r8
	flwi	f2, r0 0
	sub	r0 r0 r0
	sli	r5, r7, 0
	add	r0 r6 r5
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	flwi	f2, r3 26 #off
	fsub	f1, f2, f1
	fgt	r30 f1 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.22048
	addi	r5, r0 0
	jl	ble_cont.22049
ble_else.22048:
	addi	r5, r0 1
ble_cont.22049:
	seq	r30, r5, r1
	jeql	r30, r0, beq_else.22050
	jr	r31
beq_else.22050:
	fmul	f2, f1, f1
	fmul	f1, f2, f1
	flwi	f2, r3 24 #off
	fmul	f1, f1, f2
	sli	r5, r1, 0
	lwi	r6, r3 22
	add	r0 r6 r5
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f1, f1, f2
	sli	r5, r1, 0
	lwi	r6, r3 21
	add	r0 r6 r5
	flwi	f2, r0 0
	sub	r0 r0 r0
	fadd	f2, f2, f1
	sli	r1, r1, 0
	add	r0 r6 r1
	fswi	f2, r0 0
	sub	r0 r0 r0
	sli	r1, r2, 0
	add	r0 r6 r1
	flwi	f2, r0 0
	sub	r0 r0 r0
	fadd	f2, f2, f1
	sli	r1, r2, 0
	add	r0 r6 r1
	fswi	f2, r0 0
	sub	r0 r0 r0
	sli	r1, r7, 0
	add	r0 r6 r1
	flwi	f2, r0 0
	sub	r0 r0 r0
	fadd	f1, f2, f1
	sli	r1, r7, 0
	add	r0 r6 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	jr	r31
beq_else.22045:
	sli	r2, r1, 0
	lwi	r6, r3 20
	add	r2 r6 r2
	lwi	r2, r2 0
	sli	r6, r2, 0
	lwi	r7, r3 19
	add	r6 r7 r6
	lwi	r6, r6 0
	lwi	r7, r6 2
	lwi	r8, r6 7
	flwi	f1, r8 0 #
	flwi	f2, r3 24 #off
	fmul	f1, f1, f2
	lwi	r8, r6 1
	lwi	r9, r3 28
	swi	r7, r3 36
	fswi	f1, r3 38
	swi	r2, r3 39
	swi	r6, r3 40
	seq	r30, r8, r9
	jeql	r30, r0, beq_else.22054
	sli	r8, r1, 0
	lwi	r10, r3 18
	add	r8 r10 r8
	lwi	r8, r8 0
	sli	r11, r1, 0
	lwi	r12, r3 17
	flwi	f3, r3 26 #off
	add	r0 r12 r11
	fswi	f3, r0 0
	sub	r0 r0 r0
	sli	r11, r9, 0
	add	r0 r12 r11
	fswi	f3, r0 0
	sub	r0 r0 r0
	lwi	r11, r3 27
	sli	r13, r11, 0
	add	r0 r12 r13
	fswi	f3, r0 0
	sub	r0 r0 r0
	sub	r13, r8, r9
	sub	r8, r8, r9
	sli	r8, r8, 0
	lwi	r14, r3 30
	add	r0 r14 r8
	flwi	f4, r0 0
	sub	r0 r0 r0
	flwl	%f5 l.11783
	feq	r30, f4, f5
	jeql	r30, r0, beq_else.22056
	addi	r8, r0 1
	jl	beq_cont.22057
beq_else.22056:
	addi	r8, r0 0
beq_cont.22057:
	seq	r30, r8, r1
	jeql	r30, r0, beq_else.22058
	flwl	%f5 l.11783
	fgt	r30 f4 f5
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.22060
	addi	r8, r0 0
	jl	ble_cont.22061
ble_else.22060:
	addi	r8, r0 1
ble_cont.22061:
	seq	r30, r8, r1
	jeql	r30, r0, beq_else.22062
	flwl	%f4 l.11828
	jl	beq_cont.22063
beq_else.22062:
	flwl	%f4 l.11794
beq_cont.22063:
	jl	beq_cont.22059
beq_else.22058:
	flwl	%f4 l.11783
beq_cont.22059:
	fsub	f4, f3, f4
	sli	r8, r13, 0
	add	r0 r12 r8
	fswi	f4, r0 0
	sub	r0 r0 r0
	jl	beq_cont.22055
beq_else.22054:
	lwi	r10, r3 27
	seq	r30, r8, r10
	jeql	r30, r0, beq_else.22064
	lwi	r8, r6 4
	flwi	f3, r8 0 #
	flwi	f4, r3 26 #off
	fsub	f3, f4, f3
	sli	r8, r1, 0
	lwi	r11, r3 17
	add	r0 r11 r8
	fswi	f3, r0 0
	sub	r0 r0 r0
	lwi	r8, r6 4
	flwi	f3, r8 1 #
	fsub	f3, f4, f3
	sli	r8, r9, 0
	add	r0 r11 r8
	fswi	f3, r0 0
	sub	r0 r0 r0
	lwi	r8, r6 4
	flwi	f3, r8 2 #
	fsub	f3, f4, f3
	sli	r8, r10, 0
	add	r0 r11 r8
	fswi	f3, r0 0
	sub	r0 r0 r0
	jl	beq_cont.22065
beq_else.22064:
	lwi	r28, r3 16
	addi	r1, r6 0 #
	swi	r31 r3 41
	addi	r3, r3, 42
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -42
	lwi	r31, r3 41
beq_cont.22065:
beq_cont.22055:
	lwi	r1, r3 35
	sli	r2, r1, 0
	lwi	r5, r3 15
	add	r0 r5 r2
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r2, r1, 0
	lwi	r6, r3 14
	add	r0 r6 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r2, r3 28
	sli	r7, r2, 0
	add	r0 r5 r7
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r7, r2, 0
	add	r0 r6 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r7, r3 27
	sli	r8, r7, 0
	add	r0 r5 r8
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r8, r7, 0
	add	r0 r6 r8
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r6, r3 40
	lwi	r28, r3 13
	addi	r2, r5 0 #
	addi	r1, r6 0 #
	swi	r31 r3 41
	addi	r3, r3, 42
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -42
	lwi	r31, r3 41
	lwi	r1, r3 12
	lwi	r2, r3 39
	mul	r1, r2, r1
	lwi	r2, r3 35
	sli	r5, r2, 0
	lwi	r6, r3 18
	add	r5 r6 r5
	lwi	r5, r5 0
	add	r1, r1, r5
	lwi	r5, r3 33
	sli	r6, r5, 0
	lwi	r7, r3 31
	add	r0 r7 r6
	swi	r1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 11
	lwi	r6, r1 1
	sli	r8, r5, 0
	add	r6 r6 r8
	lwi	r6, r6 0
	sli	r8, r2, 0
	lwi	r9, r3 15
	add	r0 r9 r8
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r8, r2, 0
	add	r0 r6 r8
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r8, r3 28
	sli	r10, r8, 0
	add	r0 r9 r10
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r10, r8, 0
	add	r0 r6 r10
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r10, r3 27
	sli	r11, r10, 0
	add	r0 r9 r11
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r11, r10, 0
	add	r0 r6 r11
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r6, r1 3
	lwi	r11, r3 40
	lwi	r12, r11 7
	flwi	f1, r12 0 #
	flwl	%f2 l.11779
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.22066
	addi	r12, r0 0
	jl	ble_cont.22067
ble_else.22066:
	addi	r12, r0 1
ble_cont.22067:
	seq	r30, r12, r2
	jeql	r30, r0, beq_else.22068
	sli	r12, r5, 0
	add	r0 r6 r12
	swi	r8, r0 0
	sub	r0 r0 r0
	lwi	r6, r1 4
	sli	r12, r5, 0
	add	r12 r6 r12
	lwi	r12, r12 0
	sli	r13, r2, 0
	lwi	r14, r3 10
	add	r0 r14 r13
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r13, r2, 0
	add	r0 r12 r13
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r13, r8, 0
	add	r0 r14 r13
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r13, r8, 0
	add	r0 r12 r13
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r13, r10, 0
	add	r0 r14 r13
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r13, r10, 0
	add	r0 r12 r13
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r12, r5, 0
	add	r6 r6 r12
	lwi	r6, r6 0
	flwl	%f1 l.12422
	flwi	f2, r3 38 #off
	fmul	f1, f1, f2
	sli	r12, r2, 0
	add	r0 r6 r12
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f3, f3, f1
	sli	r12, r2, 0
	add	r0 r6 r12
	fswi	f3, r0 0
	sub	r0 r0 r0
	sli	r12, r8, 0
	add	r0 r6 r12
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f3, f3, f1
	sli	r12, r8, 0
	add	r0 r6 r12
	fswi	f3, r0 0
	sub	r0 r0 r0
	sli	r12, r10, 0
	add	r0 r6 r12
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f1, f3, f1
	sli	r12, r10, 0
	add	r0 r6 r12
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r6, r1 7
	sli	r12, r5, 0
	add	r6 r6 r12
	lwi	r6, r6 0
	sli	r12, r2, 0
	lwi	r13, r3 17
	add	r0 r13 r12
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r12, r2, 0
	add	r0 r6 r12
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r12, r8, 0
	add	r0 r13 r12
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r12, r8, 0
	add	r0 r6 r12
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r12, r10, 0
	add	r0 r13 r12
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r12, r10, 0
	add	r0 r6 r12
	fswi	f1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.22069
beq_else.22068:
	sli	r12, r5, 0
	add	r0 r6 r12
	swi	r2, r0 0
	sub	r0 r0 r0
beq_cont.22069:
	flwl	%f1 l.12436
	sli	r6, r2, 0
	lwi	r12, r3 30
	add	r0 r12 r6
	flwi	f2, r0 0
	sub	r0 r0 r0
	sli	r6, r2, 0
	lwi	r13, r3 17
	add	r0 r13 r6
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f3
	sli	r6, r8, 0
	add	r0 r12 r6
	flwi	f3, r0 0
	sub	r0 r0 r0
	sli	r6, r8, 0
	add	r0 r13 r6
	flwi	f4, r0 0
	sub	r0 r0 r0
	fmul	f3, f3, f4
	fadd	f2, f2, f3
	sli	r6, r10, 0
	add	r0 r12 r6
	flwi	f3, r0 0
	sub	r0 r0 r0
	sli	r6, r10, 0
	add	r0 r13 r6
	flwi	f4, r0 0
	sub	r0 r0 r0
	fmul	f3, f3, f4
	fadd	f2, f2, f3
	fmul	f1, f1, f2
	sli	r6, r2, 0
	add	r0 r12 r6
	flwi	f2, r0 0
	sub	r0 r0 r0
	sli	r6, r2, 0
	add	r0 r13 r6
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f3, f1, f3
	fadd	f2, f2, f3
	sli	r6, r2, 0
	add	r0 r12 r6
	fswi	f2, r0 0
	sub	r0 r0 r0
	sli	r6, r8, 0
	add	r0 r12 r6
	flwi	f2, r0 0
	sub	r0 r0 r0
	sli	r6, r8, 0
	add	r0 r13 r6
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f3, f1, f3
	fadd	f2, f2, f3
	sli	r6, r8, 0
	add	r0 r12 r6
	fswi	f2, r0 0
	sub	r0 r0 r0
	sli	r6, r10, 0
	add	r0 r12 r6
	flwi	f2, r0 0
	sub	r0 r0 r0
	sli	r6, r10, 0
	add	r0 r13 r6
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f1, f1, f3
	fadd	f1, f2, f1
	sli	r6, r10, 0
	add	r0 r12 r6
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r6, r11 7
	flwi	f1, r6 1 #
	flwi	f2, r3 24 #off
	fmul	f1, f2, f1
	sli	r6, r2, 0
	lwi	r14, r3 9
	add	r6 r14 r6
	lwi	r6, r6 0
	lwi	r28, r3 6
	fswi	f1, r3 42
	addi	r1, r2 0 #
	addi	r2, r6 0 #
	swi	r31 r3 43
	addi	r3, r3, 44
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -44
	lwi	r31, r3 43
	lwi	r2, r3 35
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.22071
	sli	r1, r2, 0
	lwi	r5, r3 17
	add	r0 r5 r1
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r1, r2, 0
	lwi	r6, r3 29
	add	r0 r6 r1
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f1, f1, f2
	lwi	r1, r3 28
	sli	r7, r1, 0
	add	r0 r5 r7
	flwi	f2, r0 0
	sub	r0 r0 r0
	sli	r7, r1, 0
	add	r0 r6 r7
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	lwi	r7, r3 27
	sli	r8, r7, 0
	add	r0 r5 r8
	flwi	f2, r0 0
	sub	r0 r0 r0
	sli	r5, r7, 0
	add	r0 r6 r5
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	flwi	f2, r3 26 #off
	fsub	f1, f2, f1
	flwi	f3, r3 38 #off
	fmul	f1, f1, f3
	sli	r5, r2, 0
	lwi	r8, r3 30
	add	r0 r8 r5
	flwi	f4, r0 0
	sub	r0 r0 r0
	sli	r5, r2, 0
	add	r0 r6 r5
	flwi	f5, r0 0
	sub	r0 r0 r0
	fmul	f4, f4, f5
	sli	r5, r1, 0
	add	r0 r8 r5
	flwi	f5, r0 0
	sub	r0 r0 r0
	sli	r5, r1, 0
	add	r0 r6 r5
	flwi	f6, r0 0
	sub	r0 r0 r0
	fmul	f5, f5, f6
	fadd	f4, f4, f5
	sli	r5, r7, 0
	add	r0 r8 r5
	flwi	f5, r0 0
	sub	r0 r0 r0
	sli	r5, r7, 0
	add	r0 r6 r5
	flwi	f6, r0 0
	sub	r0 r0 r0
	fmul	f5, f5, f6
	fadd	f4, f4, f5
	fsub	f2, f2, f4
	flwi	f4, r3 42 #off
	lwi	r28, r3 5
	fadd	f3, f4 f0
	swi	r31 r3 43
	addi	r3, r3, 44
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -44
	lwi	r31, r3 43
	jl	beq_cont.22072
beq_else.22071:
beq_cont.22072:
	lwi	r1, r3 35
	sli	r2, r1, 0
	lwi	r5, r3 15
	add	r0 r5 r2
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r2, r1, 0
	lwi	r6, r3 4
	add	r0 r6 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r2, r3 28
	sli	r7, r2, 0
	add	r0 r5 r7
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r7, r2, 0
	add	r0 r6 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r7, r3 27
	sli	r8, r7, 0
	add	r0 r5 r8
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r8, r7, 0
	add	r0 r6 r8
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r6, r1, 0
	lwi	r8, r3 3
	add	r6 r8 r6
	lwi	r6, r6 0
	sub	r6, r6, r2
	lwi	r28, r3 2
	addi	r2, r6 0 #
	addi	r1, r5 0 #
	swi	r31 r3 43
	addi	r3, r3, 44
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -44
	lwi	r31, r3 43
	lwi	r1, r3 35
	sli	r2, r1, 0
	lwi	r5, r3 1
	add	r2 r5 r2
	lwi	r2, r2 0
	lwi	r5, r3 28
	sub	r2, r2, r5
	flwi	f1, r3 38 #off
	flwi	f2, r3 42 #off
	lwi	r6, r3 30
	lwi	r28, r3 0
	addi	r1, r2 0 #
	addi	r2, r6 0 #
	swi	r31 r3 43
	addi	r3, r3, 44
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -44
	lwi	r31, r3 43
	flwl	%f1 l.12474
	flwi	f2, r3 24 #off
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.22073
	addi	r1, r0 0
	jl	ble_cont.22074
ble_else.22073:
	addi	r1, r0 1
ble_cont.22074:
	lwi	r2, r3 35
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.22075
	jr	r31
beq_else.22075:
	lwi	r1, r3 33
	addi	r30, r0, 4
	slt	r30, r1, r30
	seq	r30 r30, r0
	jeql	r30, r0, bge_else.22077
	jl	bge_cont.22078
bge_else.22077:
	lwi	r5, r3 28
	add	r6, r1, r5
	sli	r6, r6, 0
	lwi	r7, r3 31
	lwi	r8, r3 32
	add	r0 r7 r6
	swi	r8, r0 0
	sub	r0 r0 r0
bge_cont.22078:
	lwi	r5, r3 27
	lwi	r6, r3 36
	seq	r30, r6, r5
	jeql	r30, r0, beq_else.22079
	flwl	%f1 l.11794
	lwi	r5, r3 40
	lwi	r5, r5 7
	flwi	f3, r5 0 #
	fsub	f1, f1, f3
	fmul	f1, f2, f1
	lwi	r5, r3 28
	add	r1, r1, r5
	sli	r2, r2, 0
	lwi	r5, r3 34
	add	r0 r5 r2
	flwi	f2, r0 0
	sub	r0 r0 r0
	flwi	f3, r3 8 #off
	fadd	f2, f3, f2
	lwi	r2, r3 30
	lwi	r5, r3 11
	lwi	r28, r3 7
	lwi	r29, r28 0
	jr	r29
beq_else.22079:
	jr	r31
ble_else.22036:
	jr	r31
trace_diffuse_ray.2969:
	lwi	r2, r28 19
	lwi	r5, r28 18
	lwi	r6, r28 17
	lwi	r7, r28 16
	lwi	r8, r28 15
	lwi	r9, r28 14
	lwi	r10, r28 13
	lwi	r11, r28 12
	lwi	r12, r28 11
	lwi	r13, r28 10
	lwi	r14, r28 9
	lwi	r15, r28 8
	lwi	r16, r28 7
	lwi	r17, r28 6
	lwi	r18, r28 5
	lwi	r19, r28 4
	lwi	r20, r28 3
	flwi	f2, r28 2 #
	flwi	f3, r28 1 #
	sli	r21, r19, 0
	add	r0 r6 r21
	fswi	f2, r0 0
	sub	r0 r0 r0
	sli	r21, r19, 0
	add	r21 r9 r21
	lwi	r21, r21 0
	swi	r7, r3 0
	swi	r17, r3 1
	fswi	f1, r3 2
	swi	r12, r3 3
	swi	r8, r3 4
	swi	r9, r3 5
	swi	r14, r3 6
	swi	r2, r3 7
	swi	r16, r3 8
	swi	r18, r3 9
	swi	r11, r3 10
	fswi	f3, r3 12
	swi	r13, r3 13
	swi	r20, r3 14
	swi	r1, r3 15
	swi	r10, r3 16
	swi	r15, r3 17
	swi	r6, r3 18
	swi	r19, r3 19
	addi	r2, r21 0 #
	addi	r28, r5 0 #
	addi	r5, r1 0 #
	addi	r1, r19 0 #
	swi	r31 r3 20
	addi	r3, r3, 21
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -21
	lwi	r31, r3 20
	lwi	r1, r3 19
	sli	r2, r1, 0
	lwi	r5, r3 18
	add	r0 r5 r2
	flwi	f1, r0 0
	sub	r0 r0 r0
	flwl	%f2 l.12184
	fgt	r30 f1 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.22083
	addi	r2, r0 0
	jl	ble_cont.22084
ble_else.22083:
	addi	r2, r0 1
ble_cont.22084:
	seq	r30, r2, r1
	jeql	r30, r0, beq_else.22085
	addi	r2, r0 0
	jl	beq_cont.22086
beq_else.22085:
	flwl	%f2 l.12362
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.22087
	addi	r2, r0 0
	jl	ble_cont.22088
ble_else.22087:
	addi	r2, r0 1
ble_cont.22088:
beq_cont.22086:
	seq	r30, r2, r1
	jeql	r30, r0, beq_else.22089
	jr	r31
beq_else.22089:
	sli	r2, r1, 0
	lwi	r5, r3 17
	add	r2 r5 r2
	lwi	r2, r2 0
	sli	r2, r2, 0
	lwi	r5, r3 16
	add	r2 r5 r2
	lwi	r2, r2 0
	lwi	r5, r3 15
	lwi	r5, r5 0
	lwi	r6, r2 1
	lwi	r7, r3 14
	swi	r2, r3 20
	seq	r30, r6, r7
	jeql	r30, r0, beq_else.22091
	sli	r6, r1, 0
	lwi	r8, r3 13
	add	r6 r8 r6
	lwi	r6, r6 0
	sli	r8, r1, 0
	lwi	r9, r3 10
	flwi	f1, r3 12 #off
	add	r0 r9 r8
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r8, r7, 0
	add	r0 r9 r8
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r8, r3 9
	sli	r10, r8, 0
	add	r0 r9 r10
	fswi	f1, r0 0
	sub	r0 r0 r0
	sub	r10, r6, r7
	sub	r6, r6, r7
	sli	r6, r6, 0
	add	r0 r5 r6
	flwi	f2, r0 0
	sub	r0 r0 r0
	flwl	%f3 l.11783
	feq	r30, f2, f3
	jeql	r30, r0, beq_else.22093
	addi	r5, r0 1
	jl	beq_cont.22094
beq_else.22093:
	addi	r5, r0 0
beq_cont.22094:
	seq	r30, r5, r1
	jeql	r30, r0, beq_else.22095
	flwl	%f3 l.11783
	fgt	r30 f2 f3
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.22097
	addi	r5, r0 0
	jl	ble_cont.22098
ble_else.22097:
	addi	r5, r0 1
ble_cont.22098:
	seq	r30, r5, r1
	jeql	r30, r0, beq_else.22099
	flwl	%f2 l.11828
	jl	beq_cont.22100
beq_else.22099:
	flwl	%f2 l.11794
beq_cont.22100:
	jl	beq_cont.22096
beq_else.22095:
	flwl	%f2 l.11783
beq_cont.22096:
	fsub	f2, f1, f2
	sli	r5, r10, 0
	add	r0 r9 r5
	fswi	f2, r0 0
	sub	r0 r0 r0
	jl	beq_cont.22092
beq_else.22091:
	lwi	r5, r3 9
	seq	r30, r6, r5
	jeql	r30, r0, beq_else.22101
	lwi	r6, r2 4
	flwi	f1, r6 0 #
	flwi	f2, r3 12 #off
	fsub	f1, f2, f1
	sli	r6, r1, 0
	lwi	r8, r3 10
	add	r0 r8 r6
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r6, r2 4
	flwi	f1, r6 1 #
	fsub	f1, f2, f1
	sli	r6, r7, 0
	add	r0 r8 r6
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r6, r2 4
	flwi	f1, r6 2 #
	fsub	f1, f2, f1
	sli	r6, r5, 0
	add	r0 r8 r6
	fswi	f1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.22102
beq_else.22101:
	lwi	r28, r3 8
	addi	r1, r2 0 #
	swi	r31 r3 21
	addi	r3, r3, 22
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -22
	lwi	r31, r3 21
beq_cont.22102:
beq_cont.22092:
	lwi	r1, r3 20
	lwi	r2, r3 6
	lwi	r28, r3 7
	swi	r31 r3 21
	addi	r3, r3, 22
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -22
	lwi	r31, r3 21
	lwi	r1, r3 19
	sli	r2, r1, 0
	lwi	r5, r3 5
	add	r2 r5 r2
	lwi	r2, r2 0
	lwi	r28, r3 4
	swi	r31 r3 21
	addi	r3, r3, 22
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -22
	lwi	r31, r3 21
	lwi	r2, r3 19
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.22103
	sli	r1, r2, 0
	lwi	r5, r3 10
	add	r0 r5 r1
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r1, r2, 0
	lwi	r6, r3 3
	add	r0 r6 r1
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f1, f1, f2
	lwi	r1, r3 14
	sli	r7, r1, 0
	add	r0 r5 r7
	flwi	f2, r0 0
	sub	r0 r0 r0
	sli	r7, r1, 0
	add	r0 r6 r7
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	lwi	r7, r3 9
	sli	r8, r7, 0
	add	r0 r5 r8
	flwi	f2, r0 0
	sub	r0 r0 r0
	sli	r5, r7, 0
	add	r0 r6 r5
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f3
	fadd	f1, f1, f2
	flwi	f2, r3 12 #off
	fsub	f1, f2, f1
	fgt	r30 f1 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.22104
	addi	r5, r0 0
	jl	ble_cont.22105
ble_else.22104:
	addi	r5, r0 1
ble_cont.22105:
	seq	r30, r5, r2
	jeql	r30, r0, beq_else.22106
	flwl	%f1 l.11783
	jl	beq_cont.22107
beq_else.22106:
beq_cont.22107:
	flwi	f2, r3 2 #off
	fmul	f1, f2, f1
	lwi	r5, r3 20
	lwi	r5, r5 7
	flwi	f2, r5 0 #
	fmul	f1, f1, f2
	sli	r5, r2, 0
	lwi	r6, r3 1
	add	r0 r6 r5
	flwi	f2, r0 0
	sub	r0 r0 r0
	sli	r5, r2, 0
	lwi	r8, r3 0
	add	r0 r8 r5
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f3, f1, f3
	fadd	f2, f2, f3
	sli	r2, r2, 0
	add	r0 r6 r2
	fswi	f2, r0 0
	sub	r0 r0 r0
	sli	r2, r1, 0
	add	r0 r6 r2
	flwi	f2, r0 0
	sub	r0 r0 r0
	sli	r2, r1, 0
	add	r0 r8 r2
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f3, f1, f3
	fadd	f2, f2, f3
	sli	r1, r1, 0
	add	r0 r6 r1
	fswi	f2, r0 0
	sub	r0 r0 r0
	sli	r1, r7, 0
	add	r0 r6 r1
	flwi	f2, r0 0
	sub	r0 r0 r0
	sli	r1, r7, 0
	add	r0 r8 r1
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f1, f1, f3
	fadd	f1, f2, f1
	sli	r1, r7, 0
	add	r0 r6 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	jr	r31
beq_else.22103:
	jr	r31
iter_trace_diffuse_rays.2972:
	lwi	r7, r28 5
	lwi	r8, r28 4
	lwi	r9, r28 3
	lwi	r10, r28 2
	flwi	f1, r28 1 #
	sgt	r30, r9, r6
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22110
	sli	r11, r6, 0
	add	r11 r1 r11
	lwi	r11, r11 0
	lwi	r11, r11 0
	sli	r12, r9, 0
	add	r0 r11 r12
	flwi	f2, r0 0
	sub	r0 r0 r0
	sli	r12, r9, 0
	add	r0 r2 r12
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f2, f2, f3
	sli	r12, r10, 0
	add	r0 r11 r12
	flwi	f3, r0 0
	sub	r0 r0 r0
	sli	r12, r10, 0
	add	r0 r2 r12
	flwi	f4, r0 0
	sub	r0 r0 r0
	fmul	f3, f3, f4
	fadd	f2, f2, f3
	sli	r12, r8, 0
	add	r0 r11 r12
	flwi	f3, r0 0
	sub	r0 r0 r0
	sli	r11, r8, 0
	add	r0 r2 r11
	flwi	f4, r0 0
	sub	r0 r0 r0
	fmul	f3, f3, f4
	fadd	f2, f2, f3
	fgt	r30 f1 f2
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.22111
	addi	r11, r0 0
	jl	ble_cont.22112
ble_else.22111:
	addi	r11, r0 1
ble_cont.22112:
	swi	r5, r3 0
	swi	r2, r3 1
	swi	r1, r3 2
	swi	r28, r3 3
	swi	r8, r3 4
	swi	r6, r3 5
	seq	r30, r11, r9
	jeql	r30, r0, beq_else.22113
	sli	r9, r6, 0
	add	r9 r1 r9
	lwi	r9, r9 0
	flwl	%f1 l.12536
	fdiv	f1, f2, f1
	addi	r1, r9 0 #
	addi	r28, r7 0 #
	swi	r31 r3 6
	addi	r3, r3, 7
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -7
	lwi	r31, r3 6
	jl	beq_cont.22114
beq_else.22113:
	add	r9, r6, r10
	sli	r9, r9, 0
	add	r9 r1 r9
	lwi	r9, r9 0
	flwl	%f1 l.12534
	fdiv	f1, f2, f1
	addi	r1, r9 0 #
	addi	r28, r7 0 #
	swi	r31 r3 6
	addi	r3, r3, 7
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -7
	lwi	r31, r3 6
beq_cont.22114:
	lwi	r1, r3 4
	lwi	r2, r3 5
	sub	r6, r2, r1
	lwi	r1, r3 2
	lwi	r2, r3 1
	lwi	r5, r3 0
	lwi	r28, r3 3
	lwi	r29, r28 0
	jr	r29
ble_else.22110:
	jr	r31
trace_diffuse_ray_80percent.2981:
	lwi	r6, r28 9
	lwi	r7, r28 8
	lwi	r8, r28 7
	lwi	r9, r28 6
	lwi	r10, r28 5
	lwi	r11, r28 4
	lwi	r12, r28 3
	lwi	r13, r28 2
	lwi	r14, r28 1
	swi	r12, r3 0
	swi	r2, r3 1
	swi	r9, r3 2
	swi	r7, r3 3
	swi	r8, r3 4
	swi	r11, r3 5
	swi	r6, r3 6
	swi	r5, r3 7
	swi	r13, r3 8
	swi	r10, r3 9
	swi	r14, r3 10
	swi	r1, r3 11
	seq	r30, r1, r13
	jeql	r30, r0, beq_else.22116
	jl	beq_cont.22117
beq_else.22116:
	sli	r15, r13, 0
	add	r15 r10 r15
	lwi	r15, r15 0
	sli	r16, r13, 0
	add	r0 r5 r16
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r16, r13, 0
	add	r0 r6 r16
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r16, r14, 0
	add	r0 r5 r16
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r16, r14, 0
	add	r0 r6 r16
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r16, r11, 0
	add	r0 r5 r16
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r16, r11, 0
	add	r0 r6 r16
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r16, r13, 0
	add	r16 r8 r16
	lwi	r16, r16 0
	sub	r16, r16, r14
	swi	r15, r3 12
	addi	r2, r16 0 #
	addi	r1, r5 0 #
	addi	r28, r7 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
	addi	r6, r0 118
	lwi	r1, r3 12
	lwi	r2, r3 1
	lwi	r5, r3 7
	lwi	r28, r3 2
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
beq_cont.22117:
	lwi	r1, r3 10
	lwi	r2, r3 11
	seq	r30, r2, r1
	jeql	r30, r0, beq_else.22118
	jl	beq_cont.22119
beq_else.22118:
	sli	r5, r1, 0
	lwi	r6, r3 9
	add	r5 r6 r5
	lwi	r5, r5 0
	lwi	r7, r3 8
	sli	r8, r7, 0
	lwi	r9, r3 7
	add	r0 r9 r8
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r8, r7, 0
	lwi	r10, r3 6
	add	r0 r10 r8
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r8, r1, 0
	add	r0 r9 r8
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r8, r1, 0
	add	r0 r10 r8
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r8, r3 5
	sli	r11, r8, 0
	add	r0 r9 r11
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r11, r8, 0
	add	r0 r10 r11
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r11, r7, 0
	lwi	r12, r3 4
	add	r11 r12 r11
	lwi	r11, r11 0
	sub	r11, r11, r1
	lwi	r28, r3 3
	swi	r5, r3 13
	addi	r2, r11 0 #
	addi	r1, r9 0 #
	swi	r31 r3 14
	addi	r3, r3, 15
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -15
	lwi	r31, r3 14
	addi	r6, r0 118
	lwi	r1, r3 13
	lwi	r2, r3 1
	lwi	r5, r3 7
	lwi	r28, r3 2
	swi	r31 r3 14
	addi	r3, r3, 15
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -15
	lwi	r31, r3 14
beq_cont.22119:
	lwi	r1, r3 5
	lwi	r2, r3 11
	seq	r30, r2, r1
	jeql	r30, r0, beq_else.22120
	jl	beq_cont.22121
beq_else.22120:
	sli	r5, r1, 0
	lwi	r6, r3 9
	add	r5 r6 r5
	lwi	r5, r5 0
	lwi	r7, r3 8
	sli	r8, r7, 0
	lwi	r9, r3 7
	add	r0 r9 r8
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r8, r7, 0
	lwi	r10, r3 6
	add	r0 r10 r8
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r8, r3 10
	sli	r11, r8, 0
	add	r0 r9 r11
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r11, r8, 0
	add	r0 r10 r11
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r11, r1, 0
	add	r0 r9 r11
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r11, r1, 0
	add	r0 r10 r11
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r11, r7, 0
	lwi	r12, r3 4
	add	r11 r12 r11
	lwi	r11, r11 0
	sub	r11, r11, r8
	lwi	r28, r3 3
	swi	r5, r3 14
	addi	r2, r11 0 #
	addi	r1, r9 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -16
	lwi	r31, r3 15
	addi	r6, r0 118
	lwi	r1, r3 14
	lwi	r2, r3 1
	lwi	r5, r3 7
	lwi	r28, r3 2
	swi	r31 r3 15
	addi	r3, r3, 16
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -16
	lwi	r31, r3 15
beq_cont.22121:
	lwi	r1, r3 0
	lwi	r2, r3 11
	seq	r30, r2, r1
	jeql	r30, r0, beq_else.22122
	jl	beq_cont.22123
beq_else.22122:
	sli	r1, r1, 0
	lwi	r5, r3 9
	add	r1 r5 r1
	lwi	r1, r1 0
	lwi	r6, r3 8
	sli	r7, r6, 0
	lwi	r8, r3 7
	add	r0 r8 r7
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r7, r6, 0
	lwi	r9, r3 6
	add	r0 r9 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r7, r3 10
	sli	r10, r7, 0
	add	r0 r8 r10
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r10, r7, 0
	add	r0 r9 r10
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r10, r3 5
	sli	r11, r10, 0
	add	r0 r8 r11
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r11, r10, 0
	add	r0 r9 r11
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r11, r6, 0
	lwi	r12, r3 4
	add	r11 r12 r11
	lwi	r11, r11 0
	sub	r11, r11, r7
	lwi	r28, r3 3
	swi	r1, r3 15
	addi	r2, r11 0 #
	addi	r1, r8 0 #
	swi	r31 r3 16
	addi	r3, r3, 17
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -17
	lwi	r31, r3 16
	addi	r6, r0 118
	lwi	r1, r3 15
	lwi	r2, r3 1
	lwi	r5, r3 7
	lwi	r28, r3 2
	swi	r31 r3 16
	addi	r3, r3, 17
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -17
	lwi	r31, r3 16
beq_cont.22123:
	lwi	r1, r3 11
	addi	r30, r0, 4
	seq	r30, r1, r30
	jeql	r30, r0, beq_else.22124
	jr	r31
beq_else.22124:
	lwi	r1, r3 9
	lwi	r1, r1 4
	lwi	r2, r3 8
	sli	r5, r2, 0
	lwi	r6, r3 7
	add	r0 r6 r5
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r5, r2, 0
	lwi	r7, r3 6
	add	r0 r7 r5
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r5, r3 10
	sli	r8, r5, 0
	add	r0 r6 r8
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r8, r5, 0
	add	r0 r7 r8
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r8, r3 5
	sli	r9, r8, 0
	add	r0 r6 r9
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r8, r8, 0
	add	r0 r7 r8
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r2, r2, 0
	lwi	r7, r3 4
	add	r2 r7 r2
	lwi	r2, r2 0
	sub	r2, r2, r5
	lwi	r28, r3 3
	swi	r1, r3 16
	addi	r1, r6 0 #
	swi	r31 r3 17
	addi	r3, r3, 18
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -18
	lwi	r31, r3 17
	addi	r6, r0 118
	lwi	r1, r3 16
	lwi	r2, r3 1
	lwi	r5, r3 7
	lwi	r28, r3 2
	lwi	r29, r28 0
	jr	r29
calc_diffuse_using_1point.2985:
	lwi	r5, r28 7
	lwi	r6, r28 6
	lwi	r7, r28 5
	lwi	r8, r28 4
	lwi	r9, r28 3
	lwi	r10, r28 2
	lwi	r11, r28 1
	lwi	r12, r1 5
	lwi	r13, r1 7
	lwi	r14, r1 1
	lwi	r15, r1 4
	sli	r16, r2, 0
	add	r12 r12 r16
	lwi	r12, r12 0
	sli	r16, r10, 0
	add	r0 r12 r16
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r10, r10, 0
	add	r0 r8 r10
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r10, r11, 0
	add	r0 r12 r10
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r10, r11, 0
	add	r0 r8 r10
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r10, r9, 0
	add	r0 r12 r10
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r9, r9, 0
	add	r0 r8 r9
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r1, r1 6
	lwi	r1, r1 0
	sli	r9, r2, 0
	add	r9 r13 r9
	lwi	r9, r9 0
	sli	r10, r2, 0
	add	r10 r14 r10
	lwi	r10, r10 0
	swi	r8, r3 0
	swi	r7, r3 1
	swi	r5, r3 2
	swi	r15, r3 3
	swi	r2, r3 4
	addi	r5, r10 0 #
	addi	r2, r9 0 #
	addi	r28, r6 0 #
	swi	r31 r3 5
	addi	r3, r3, 6
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -6
	lwi	r31, r3 5
	lwi	r1, r3 4
	sli	r1, r1, 0
	lwi	r2, r3 3
	add	r2 r2 r1
	lwi	r2, r2 0
	lwi	r1, r3 1
	lwi	r5, r3 0
	lwi	r28, r3 2
	lwi	r29, r28 0
	jr	r29
calc_diffuse_using_5points.2988:
	lwi	r8, r28 6
	lwi	r9, r28 5
	lwi	r10, r28 4
	lwi	r11, r28 3
	lwi	r12, r28 2
	lwi	r13, r28 1
	sli	r14, r1, 0
	add	r2 r2 r14
	lwi	r2, r2 0
	lwi	r2, r2 5
	sub	r14, r1, r13
	sli	r14, r14, 0
	add	r14 r5 r14
	lwi	r14, r14 0
	lwi	r14, r14 5
	sli	r15, r1, 0
	add	r15 r5 r15
	lwi	r15, r15 0
	lwi	r15, r15 5
	add	r16, r1, r13
	sli	r16, r16, 0
	add	r16 r5 r16
	lwi	r16, r16 0
	lwi	r16, r16 5
	sli	r17, r1, 0
	add	r6 r6 r17
	lwi	r6, r6 0
	lwi	r6, r6 5
	sli	r17, r7, 0
	add	r2 r2 r17
	lwi	r2, r2 0
	sli	r17, r12, 0
	add	r0 r2 r17
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r17, r12, 0
	add	r0 r10 r17
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r17, r13, 0
	add	r0 r2 r17
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r17, r13, 0
	add	r0 r10 r17
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r17, r11, 0
	add	r0 r2 r17
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r2, r11, 0
	add	r0 r10 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r2, r7, 0
	add	r2 r14 r2
	lwi	r2, r2 0
	sli	r14, r12, 0
	add	r0 r10 r14
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r14, r12, 0
	add	r0 r2 r14
	flwi	f2, r0 0
	sub	r0 r0 r0
	fadd	f1, f1, f2
	sli	r14, r12, 0
	add	r0 r10 r14
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r14, r13, 0
	add	r0 r10 r14
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r14, r13, 0
	add	r0 r2 r14
	flwi	f2, r0 0
	sub	r0 r0 r0
	fadd	f1, f1, f2
	sli	r14, r13, 0
	add	r0 r10 r14
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r14, r11, 0
	add	r0 r10 r14
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r14, r11, 0
	add	r0 r2 r14
	flwi	f2, r0 0
	sub	r0 r0 r0
	fadd	f1, f1, f2
	sli	r2, r11, 0
	add	r0 r10 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r2, r7, 0
	add	r2 r15 r2
	lwi	r2, r2 0
	sli	r14, r12, 0
	add	r0 r10 r14
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r14, r12, 0
	add	r0 r2 r14
	flwi	f2, r0 0
	sub	r0 r0 r0
	fadd	f1, f1, f2
	sli	r14, r12, 0
	add	r0 r10 r14
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r14, r13, 0
	add	r0 r10 r14
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r14, r13, 0
	add	r0 r2 r14
	flwi	f2, r0 0
	sub	r0 r0 r0
	fadd	f1, f1, f2
	sli	r14, r13, 0
	add	r0 r10 r14
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r14, r11, 0
	add	r0 r10 r14
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r14, r11, 0
	add	r0 r2 r14
	flwi	f2, r0 0
	sub	r0 r0 r0
	fadd	f1, f1, f2
	sli	r2, r11, 0
	add	r0 r10 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r2, r7, 0
	add	r2 r16 r2
	lwi	r2, r2 0
	sli	r14, r12, 0
	add	r0 r10 r14
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r14, r12, 0
	add	r0 r2 r14
	flwi	f2, r0 0
	sub	r0 r0 r0
	fadd	f1, f1, f2
	sli	r14, r12, 0
	add	r0 r10 r14
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r14, r13, 0
	add	r0 r10 r14
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r14, r13, 0
	add	r0 r2 r14
	flwi	f2, r0 0
	sub	r0 r0 r0
	fadd	f1, f1, f2
	sli	r14, r13, 0
	add	r0 r10 r14
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r14, r11, 0
	add	r0 r10 r14
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r14, r11, 0
	add	r0 r2 r14
	flwi	f2, r0 0
	sub	r0 r0 r0
	fadd	f1, f1, f2
	sli	r2, r11, 0
	add	r0 r10 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r2, r7, 0
	add	r2 r6 r2
	lwi	r2, r2 0
	sli	r6, r12, 0
	add	r0 r10 r6
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r6, r12, 0
	add	r0 r2 r6
	flwi	f2, r0 0
	sub	r0 r0 r0
	fadd	f1, f1, f2
	sli	r6, r12, 0
	add	r0 r10 r6
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r6, r13, 0
	add	r0 r10 r6
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r6, r13, 0
	add	r0 r2 r6
	flwi	f2, r0 0
	sub	r0 r0 r0
	fadd	f1, f1, f2
	sli	r6, r13, 0
	add	r0 r10 r6
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r6, r11, 0
	add	r0 r10 r6
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r6, r11, 0
	add	r0 r2 r6
	flwi	f2, r0 0
	sub	r0 r0 r0
	fadd	f1, f1, f2
	sli	r2, r11, 0
	add	r0 r10 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r1, r1, 0
	add	r1 r5 r1
	lwi	r1, r1 0
	lwi	r1, r1 4
	sli	r2, r7, 0
	add	r2 r1 r2
	lwi	r2, r2 0
	addi	r5, r10 0 #
	addi	r1, r9 0 #
	addi	r28, r8 0 #
	lwi	r29, r28 0
	jr	r29
do_without_neighbors.2994:
	lwi	r5, r28 8
	lwi	r6, r28 7
	lwi	r7, r28 6
	lwi	r8, r28 5
	lwi	r9, r28 4
	lwi	r10, r28 3
	lwi	r11, r28 2
	lwi	r12, r28 1
	addi	r30, r0, 4
	sgt	r30, r2, r30
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22126
	lwi	r13, r1 2
	sli	r14, r2, 0
	add	r13 r13 r14
	lwi	r13, r13 0
	sgt	r30, r11, r13
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22127
	lwi	r13, r1 3
	sli	r14, r2, 0
	add	r13 r13 r14
	lwi	r13, r13 0
	swi	r28, r3 0
	swi	r9, r3 1
	swi	r11, r3 2
	swi	r1, r3 3
	swi	r12, r3 4
	swi	r2, r3 5
	seq	r30, r13, r11
	jeql	r30, r0, beq_else.22128
	jl	beq_cont.22129
beq_else.22128:
	lwi	r13, r1 5
	lwi	r14, r1 7
	lwi	r15, r1 1
	lwi	r16, r1 4
	sli	r17, r2, 0
	add	r13 r13 r17
	lwi	r13, r13 0
	sli	r17, r11, 0
	add	r0 r13 r17
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r17, r11, 0
	add	r0 r8 r17
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r17, r12, 0
	add	r0 r13 r17
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r17, r12, 0
	add	r0 r8 r17
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r17, r10, 0
	add	r0 r13 r17
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r10, r10, 0
	add	r0 r8 r10
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r10, r1 6
	lwi	r10, r10 0
	sli	r13, r2, 0
	add	r13 r14 r13
	lwi	r13, r13 0
	sli	r14, r2, 0
	add	r14 r15 r14
	lwi	r14, r14 0
	swi	r8, r3 6
	swi	r7, r3 7
	swi	r5, r3 8
	swi	r16, r3 9
	addi	r5, r14 0 #
	addi	r2, r13 0 #
	addi	r1, r10 0 #
	addi	r28, r6 0 #
	swi	r31 r3 10
	addi	r3, r3, 11
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -11
	lwi	r31, r3 10
	lwi	r1, r3 5
	sli	r2, r1, 0
	lwi	r5, r3 9
	add	r2 r5 r2
	lwi	r2, r2 0
	lwi	r5, r3 7
	lwi	r6, r3 6
	lwi	r28, r3 8
	addi	r1, r5 0 #
	addi	r5, r6 0 #
	swi	r31 r3 10
	addi	r3, r3, 11
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -11
	lwi	r31, r3 10
beq_cont.22129:
	lwi	r1, r3 4
	lwi	r2, r3 5
	add	r2, r2, r1
	addi	r30, r0, 4
	sgt	r30, r2, r30
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22130
	lwi	r5, r3 3
	lwi	r6, r5 2
	sli	r7, r2, 0
	add	r6 r6 r7
	lwi	r6, r6 0
	lwi	r7, r3 2
	sgt	r30, r7, r6
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22131
	lwi	r6, r5 3
	sli	r8, r2, 0
	add	r6 r6 r8
	lwi	r6, r6 0
	swi	r2, r3 10
	seq	r30, r6, r7
	jeql	r30, r0, beq_else.22132
	jl	beq_cont.22133
beq_else.22132:
	lwi	r28, r3 1
	addi	r1, r5 0 #
	swi	r31 r3 11
	addi	r3, r3, 12
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -12
	lwi	r31, r3 11
beq_cont.22133:
	lwi	r1, r3 4
	lwi	r2, r3 10
	add	r2, r2, r1
	lwi	r1, r3 3
	lwi	r28, r3 0
	lwi	r29, r28 0
	jr	r29
ble_else.22131:
	jr	r31
ble_else.22130:
	jr	r31
ble_else.22127:
	jr	r31
ble_else.22126:
	jr	r31
neighbors_are_available.3004:
	lwi	r8, r28 1
	sli	r9, r1, 0
	add	r9 r5 r9
	lwi	r9, r9 0
	lwi	r9, r9 2
	sli	r10, r7, 0
	add	r9 r9 r10
	lwi	r9, r9 0
	sli	r10, r1, 0
	add	r2 r2 r10
	lwi	r2, r2 0
	lwi	r2, r2 2
	sli	r10, r7, 0
	add	r2 r2 r10
	lwi	r2, r2 0
	seq	r30, r2, r9
	jeql	r30, r0, beq_else.22138
	sli	r2, r1, 0
	add	r2 r6 r2
	lwi	r2, r2 0
	lwi	r2, r2 2
	sli	r6, r7, 0
	add	r2 r2 r6
	lwi	r2, r2 0
	seq	r30, r2, r9
	jeql	r30, r0, beq_else.22139
	sub	r2, r1, r8
	sli	r2, r2, 0
	add	r2 r5 r2
	lwi	r2, r2 0
	lwi	r2, r2 2
	sli	r6, r7, 0
	add	r2 r2 r6
	lwi	r2, r2 0
	seq	r30, r2, r9
	jeql	r30, r0, beq_else.22140
	add	r1, r1, r8
	sli	r1, r1, 0
	add	r1 r5 r1
	lwi	r1, r1 0
	lwi	r1, r1 2
	sli	r2, r7, 0
	add	r1 r1 r2
	lwi	r1, r1 0
	seq	r30, r1, r9
	jeql	r30, r0, beq_else.22141
	addi	r1, r0 1
	jr	r31
beq_else.22141:
	addi	r1, r0 0
	jr	r31
beq_else.22140:
	addi	r1, r0 0
	jr	r31
beq_else.22139:
	addi	r1, r0 0
	jr	r31
beq_else.22138:
	addi	r1, r0 0
	jr	r31
try_exploit_neighbors.3010:
	lwi	r9, r28 6
	lwi	r10, r28 5
	lwi	r11, r28 4
	lwi	r12, r28 3
	lwi	r13, r28 2
	lwi	r14, r28 1
	sli	r15, r1, 0
	add	r15 r6 r15
	lwi	r15, r15 0
	addi	r30, r0, 4
	sgt	r30, r8, r30
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22142
	lwi	r16, r15 2
	sli	r17, r8, 0
	add	r16 r16 r17
	lwi	r16, r16 0
	sgt	r30, r13, r16
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22143
	swi	r2, r3 0
	swi	r28, r3 1
	swi	r7, r3 2
	swi	r5, r3 3
	swi	r11, r3 4
	swi	r15, r3 5
	swi	r10, r3 6
	swi	r14, r3 7
	swi	r12, r3 8
	swi	r8, r3 9
	swi	r6, r3 10
	swi	r1, r3 11
	swi	r13, r3 12
	addi	r2, r5 0 #
	addi	r28, r9 0 #
	addi	r5, r6 0 #
	addi	r6, r7 0 #
	addi	r7, r8 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
	lwi	r2, r3 12
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.22144
	lwi	r1, r3 11
	sli	r1, r1, 0
	lwi	r5, r3 10
	add	r1 r5 r1
	lwi	r1, r1 0
	lwi	r5, r3 9
	addi	r30, r0, 4
	sgt	r30, r5, r30
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22145
	lwi	r6, r1 2
	sli	r7, r5, 0
	add	r6 r6 r7
	lwi	r6, r6 0
	sgt	r30, r2, r6
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22146
	lwi	r6, r1 3
	sli	r7, r5, 0
	add	r6 r6 r7
	lwi	r6, r6 0
	swi	r1, r3 13
	seq	r30, r6, r2
	jeql	r30, r0, beq_else.22147
	jl	beq_cont.22148
beq_else.22147:
	lwi	r28, r3 8
	addi	r2, r5 0 #
	swi	r31 r3 14
	addi	r3, r3, 15
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -15
	lwi	r31, r3 14
beq_cont.22148:
	lwi	r1, r3 7
	lwi	r2, r3 9
	add	r2, r2, r1
	lwi	r1, r3 13
	lwi	r28, r3 6
	lwi	r29, r28 0
	jr	r29
ble_else.22146:
	jr	r31
ble_else.22145:
	jr	r31
beq_else.22144:
	lwi	r1, r3 5
	lwi	r1, r1 3
	lwi	r7, r3 9
	sli	r5, r7, 0
	add	r1 r1 r5
	lwi	r1, r1 0
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.22151
	jl	beq_cont.22152
beq_else.22151:
	lwi	r1, r3 11
	lwi	r2, r3 3
	lwi	r5, r3 10
	lwi	r6, r3 2
	lwi	r28, r3 4
	swi	r31 r3 14
	addi	r3, r3, 15
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -15
	lwi	r31, r3 14
beq_cont.22152:
	lwi	r1, r3 7
	lwi	r2, r3 9
	add	r8, r2, r1
	lwi	r1, r3 11
	lwi	r2, r3 0
	lwi	r5, r3 3
	lwi	r6, r3 10
	lwi	r7, r3 2
	lwi	r28, r3 1
	lwi	r29, r28 0
	jr	r29
ble_else.22143:
	jr	r31
ble_else.22142:
	jr	r31
write_ppm_header.3017:
	lwi	r1, r28 3
	lwi	r2, r28 2
	lwi	r5, r28 1
	addi	r6, r0 80
	swi	r5, r3 0
	swi	r1, r3 1
	swi	r2, r3 2
	addi	r1, r6 0 #
	swi	r31 r3 3
	addi	r3, r3, 4
	jal	min_caml_print_char
	addi	r3, r3, -4
	lwi	r31 r3 3
	addi	r1, r0 51
	swi	r31 r3 3
	addi	r3, r3, 4
	jal	min_caml_print_char
	addi	r3, r3, -4
	lwi	r31 r3 3
	addi	r1, r0 10
	swi	r1, r3 3
	swi	r31 r3 4
	addi	r3, r3, 5
	jal	min_caml_print_char
	addi	r3, r3, -5
	lwi	r31 r3 4
	lwi	r1, r3 2
	sli	r2, r1, 0
	lwi	r5, r3 1
	add	r2 r5 r2
	lwi	r2, r2 0
	seq	r30, r2, r1
	jeql	r30, r0, beq_else.22155
	swi	r31 r3 4
	addi	r3, r3, 5
	jal	min_caml_print_int_sub
	addi	r3, r3, -5
	lwi	r31 r3 4
	jl	beq_cont.22156
beq_else.22155:
	sgt	r30, r1, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22157
	addi	r1, r2 0 #
	swi	r31 r3 4
	addi	r3, r3, 5
	jal	print_int_rec.2591
	addi	r3, r3, -5
	lwi	r31 r3 4
	jl	ble_cont.22158
ble_else.22157:
	addi	r6, r0 45
	swi	r2, r3 4
	addi	r1, r6 0 #
	swi	r31 r3 5
	addi	r3, r3, 6
	jal	min_caml_print_char
	addi	r3, r3, -6
	lwi	r31 r3 5
	lwi	r1, r3 4
	lwi	r2, r3 2
	sub	r1, r2, r1
	swi	r31 r3 5
	addi	r3, r3, 6
	jal	print_int_rec.2591
	addi	r3, r3, -6
	lwi	r31 r3 5
ble_cont.22158:
beq_cont.22156:
	addi	r1, r0 32
	swi	r1, r3 5
	swi	r31 r3 6
	addi	r3, r3, 7
	jal	min_caml_print_char
	addi	r3, r3, -7
	lwi	r31 r3 6
	lwi	r1, r3 0
	sli	r1, r1, 0
	lwi	r2, r3 1
	add	r1 r2 r1
	lwi	r1, r1 0
	lwi	r2, r3 2
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.22159
	addi	r1, r2 0 #
	swi	r31 r3 6
	addi	r3, r3, 7
	jal	min_caml_print_int_sub
	addi	r3, r3, -7
	lwi	r31 r3 6
	jl	beq_cont.22160
beq_else.22159:
	sgt	r30, r2, r1
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22161
	swi	r31 r3 6
	addi	r3, r3, 7
	jal	print_int_rec.2591
	addi	r3, r3, -7
	lwi	r31 r3 6
	jl	ble_cont.22162
ble_else.22161:
	addi	r5, r0 45
	swi	r1, r3 6
	addi	r1, r5 0 #
	swi	r31 r3 7
	addi	r3, r3, 8
	jal	min_caml_print_char
	addi	r3, r3, -8
	lwi	r31 r3 7
	lwi	r1, r3 6
	lwi	r2, r3 2
	sub	r1, r2, r1
	swi	r31 r3 7
	addi	r3, r3, 8
	jal	print_int_rec.2591
	addi	r3, r3, -8
	lwi	r31 r3 7
ble_cont.22162:
beq_cont.22160:
	lwi	r1, r3 5
	swi	r31 r3 7
	addi	r3, r3, 8
	jal	min_caml_print_char
	addi	r3, r3, -8
	lwi	r31 r3 7
	addi	r1, r0 255
	swi	r31 r3 7
	addi	r3, r3, 8
	jal	print_int_rec.2591
	addi	r3, r3, -8
	lwi	r31 r3 7
	lwi	r1, r3 3
	jl	min_caml_print_char
pretrace_diffuse_rays.3023:
	lwi	r5, r28 10
	lwi	r6, r28 9
	lwi	r7, r28 8
	lwi	r8, r28 7
	lwi	r9, r28 6
	lwi	r10, r28 5
	lwi	r11, r28 4
	lwi	r12, r28 3
	lwi	r13, r28 2
	flwi	f1, r28 1 #
	addi	r30, r0, 4
	sgt	r30, r2, r30
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22163
	lwi	r14, r1 2
	sli	r15, r2, 0
	add	r14 r14 r15
	lwi	r14, r14 0
	sgt	r30, r12, r14
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22164
	lwi	r14, r1 3
	sli	r15, r2, 0
	add	r14 r14 r15
	lwi	r14, r14 0
	swi	r28, r3 0
	swi	r13, r3 1
	swi	r2, r3 2
	seq	r30, r14, r12
	jeql	r30, r0, beq_else.22165
	jl	beq_cont.22166
beq_else.22165:
	lwi	r14, r1 6
	lwi	r14, r14 0
	sli	r15, r12, 0
	add	r0 r10 r15
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r15, r13, 0
	add	r0 r10 r15
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r15, r11, 0
	add	r0 r10 r15
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r15, r1 7
	lwi	r16, r1 1
	sli	r14, r14, 0
	add	r9 r9 r14
	lwi	r9, r9 0
	sli	r14, r2, 0
	add	r14 r15 r14
	lwi	r14, r14 0
	sli	r15, r2, 0
	add	r15 r16 r15
	lwi	r15, r15 0
	sli	r16, r12, 0
	add	r0 r15 r16
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r16, r12, 0
	add	r0 r5 r16
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r16, r13, 0
	add	r0 r15 r16
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r16, r13, 0
	add	r0 r5 r16
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r16, r11, 0
	add	r0 r15 r16
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r16, r11, 0
	add	r0 r5 r16
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r5, r12, 0
	add	r5 r7 r5
	lwi	r5, r5 0
	sub	r5, r5, r13
	swi	r11, r3 3
	swi	r10, r3 4
	swi	r12, r3 5
	swi	r1, r3 6
	swi	r15, r3 7
	swi	r14, r3 8
	swi	r9, r3 9
	swi	r8, r3 10
	addi	r2, r5 0 #
	addi	r1, r15 0 #
	addi	r28, r6 0 #
	swi	r31 r3 11
	addi	r3, r3, 12
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -12
	lwi	r31, r3 11
	addi	r6, r0 118
	lwi	r1, r3 9
	lwi	r2, r3 8
	lwi	r5, r3 7
	lwi	r28, r3 10
	swi	r31 r3 11
	addi	r3, r3, 12
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -12
	lwi	r31, r3 11
	lwi	r1, r3 6
	lwi	r2, r1 5
	lwi	r5, r3 2
	sli	r6, r5, 0
	add	r2 r2 r6
	lwi	r2, r2 0
	lwi	r6, r3 5
	sli	r7, r6, 0
	lwi	r8, r3 4
	add	r0 r8 r7
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r6, r6, 0
	add	r0 r2 r6
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r6, r3 1
	sli	r7, r6, 0
	add	r0 r8 r7
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r7, r6, 0
	add	r0 r2 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r7, r3 3
	sli	r9, r7, 0
	add	r0 r8 r9
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r7, r7, 0
	add	r0 r2 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
beq_cont.22166:
	lwi	r2, r3 1
	lwi	r5, r3 2
	add	r2, r5, r2
	lwi	r28, r3 0
	lwi	r29, r28 0
	jr	r29
ble_else.22164:
	jr	r31
ble_else.22163:
	jr	r31
pretrace_pixels.3026:
	lwi	r6, r28 15
	lwi	r7, r28 14
	lwi	r8, r28 13
	lwi	r9, r28 12
	lwi	r10, r28 11
	lwi	r11, r28 10
	lwi	r12, r28 9
	lwi	r13, r28 8
	lwi	r14, r28 7
	lwi	r15, r28 6
	lwi	r16, r28 5
	lwi	r17, r28 4
	lwi	r18, r28 3
	lwi	r19, r28 2
	flwi	f4, r28 1 #
	sgt	r30, r18, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22169
	sli	r20, r18, 0
	add	r0 r11 r20
	flwi	f5, r0 0
	sub	r0 r0 r0
	sli	r11, r18, 0
	add	r11 r15 r11
	lwi	r11, r11 0
	sub	r11, r2, r11
	swi	r28, r3 0
	swi	r16, r3 1
	swi	r14, r3 2
	swi	r5, r3 3
	swi	r8, r3 4
	swi	r1, r3 5
	swi	r2, r3 6
	swi	r9, r3 7
	swi	r6, r3 8
	swi	r12, r3 9
	fswi	f4, r3 10
	swi	r7, r3 11
	fswi	f3, r3 12
	swi	r17, r3 13
	fswi	f2, r3 14
	swi	r19, r3 15
	swi	r13, r3 16
	fswi	f1, r3 18
	swi	r10, r3 19
	swi	r18, r3 20
	fswi	f5, r3 22
	addi	r1, r11 0 #
	swi	r31 r3 23
	addi	r3, r3, 24
	jal	min_caml_float_of_int
	addi	r3, r3, -24
	lwi	r31 r3 23
	flwi	f2, r3 22 #off
	fmul	f1, f2, f1
	lwi	r2, r3 20
	sli	r1, r2, 0
	lwi	r5, r3 19
	add	r0 r5 r1
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f2, f1, f2
	flwi	f3, r3 18 #off
	fadd	f2, f2, f3
	sli	r1, r2, 0
	lwi	r6, r3 16
	add	r0 r6 r1
	fswi	f2, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 15
	sli	r7, r1, 0
	add	r0 r5 r7
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f2, f1, f2
	flwi	f4, r3 14 #off
	fadd	f2, f2, f4
	sli	r7, r1, 0
	add	r0 r6 r7
	fswi	f2, r0 0
	sub	r0 r0 r0
	lwi	r7, r3 13
	sli	r8, r7, 0
	add	r0 r5 r8
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f1, f1, f2
	flwi	f2, r3 12 #off
	fadd	f1, f1, f2
	sli	r5, r7, 0
	add	r0 r6 r5
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r28, r3 11
	addi	r1, r6 0 #
	swi	r31 r3 23
	addi	r3, r3, 24
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -24
	lwi	r31, r3 23
	lwi	r1, r3 20
	sli	r2, r1, 0
	lwi	r5, r3 9
	flwi	f2, r3 10 #off
	add	r0 r5 r2
	fswi	f2, r0 0
	sub	r0 r0 r0
	lwi	r2, r3 15
	sli	r6, r2, 0
	add	r0 r5 r6
	fswi	f2, r0 0
	sub	r0 r0 r0
	lwi	r6, r3 13
	sli	r7, r6, 0
	add	r0 r5 r7
	fswi	f2, r0 0
	sub	r0 r0 r0
	sli	r7, r1, 0
	lwi	r8, r3 8
	add	r0 r8 r7
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r7, r1, 0
	lwi	r9, r3 7
	add	r0 r9 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r7, r2, 0
	add	r0 r8 r7
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r7, r2, 0
	add	r0 r9 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r7, r6, 0
	add	r0 r8 r7
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r7, r6, 0
	add	r0 r9 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	flwl	%f1 l.11794
	lwi	r7, r3 6
	sli	r8, r7, 0
	lwi	r9, r3 5
	add	r8 r9 r8
	lwi	r8, r8 0
	lwi	r10, r3 16
	lwi	r28, r3 4
	addi	r5, r8 0 #
	addi	r2, r10 0 #
	swi	r31 r3 23
	addi	r3, r3, 24
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -24
	lwi	r31, r3 23
	lwi	r1, r3 6
	sli	r2, r1, 0
	lwi	r5, r3 5
	add	r2 r5 r2
	lwi	r2, r2 0
	lwi	r2, r2 0
	lwi	r6, r3 20
	sli	r7, r6, 0
	lwi	r8, r3 9
	add	r0 r8 r7
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r7, r6, 0
	add	r0 r2 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r7, r3 15
	sli	r9, r7, 0
	add	r0 r8 r9
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r9, r7, 0
	add	r0 r2 r9
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r9, r3 13
	sli	r10, r9, 0
	add	r0 r8 r10
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r8, r9, 0
	add	r0 r2 r8
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r2, r1, 0
	add	r2 r5 r2
	lwi	r2, r2 0
	lwi	r2, r2 6
	lwi	r8, r3 3
	swi	r8, r2 0
	sli	r2, r1, 0
	add	r2 r5 r2
	lwi	r2, r2 0
	lwi	r28, r3 2
	addi	r1, r2 0 #
	addi	r2, r6 0 #
	swi	r31 r3 23
	addi	r3, r3, 24
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -24
	lwi	r31, r3 23
	lwi	r1, r3 15
	lwi	r2, r3 6
	sub	r2, r2, r1
	lwi	r5, r3 3
	add	r1, r5, r1
	lwi	r5, r3 1
	sgt	r30, r5, r1
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22172
	sub	r5, r1, r5
	jl	ble_cont.22173
ble_else.22172:
	addi	r5, r1 0
ble_cont.22173:
	flwi	f1, r3 18 #off
	flwi	f2, r3 14 #off
	flwi	f3, r3 12 #off
	lwi	r1, r3 5
	lwi	r28, r3 0
	lwi	r29, r28 0
	jr	r29
ble_else.22169:
	jr	r31
pretrace_line.3033:
	lwi	r6, r28 9
	lwi	r7, r28 8
	lwi	r8, r28 7
	lwi	r9, r28 6
	lwi	r10, r28 5
	lwi	r11, r28 4
	lwi	r12, r28 3
	lwi	r13, r28 2
	lwi	r14, r28 1
	sli	r15, r13, 0
	add	r0 r8 r15
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r8, r14, 0
	add	r8 r11 r8
	lwi	r8, r8 0
	sub	r2, r2, r8
	swi	r5, r3 0
	swi	r1, r3 1
	swi	r9, r3 2
	swi	r10, r3 3
	swi	r12, r3 4
	swi	r14, r3 5
	swi	r6, r3 6
	swi	r7, r3 7
	swi	r13, r3 8
	fswi	f1, r3 10
	addi	r1, r2 0 #
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	min_caml_float_of_int
	addi	r3, r3, -12
	lwi	r31 r3 11
	flwi	f2, r3 10 #off
	fmul	f1, f2, f1
	lwi	r1, r3 8
	sli	r2, r1, 0
	lwi	r5, r3 7
	add	r0 r5 r2
	flwi	f2, r0 0
	sub	r0 r0 r0
	fmul	f2, f1, f2
	sli	r2, r1, 0
	lwi	r6, r3 6
	add	r0 r6 r2
	flwi	f3, r0 0
	sub	r0 r0 r0
	fadd	f2, f2, f3
	lwi	r2, r3 5
	sli	r7, r2, 0
	add	r0 r5 r7
	flwi	f3, r0 0
	sub	r0 r0 r0
	fmul	f3, f1, f3
	sli	r7, r2, 0
	add	r0 r6 r7
	flwi	f4, r0 0
	sub	r0 r0 r0
	fadd	f3, f3, f4
	lwi	r7, r3 4
	sli	r8, r7, 0
	add	r0 r5 r8
	flwi	f4, r0 0
	sub	r0 r0 r0
	fmul	f1, f1, f4
	sli	r5, r7, 0
	add	r0 r6 r5
	flwi	f4, r0 0
	sub	r0 r0 r0
	fadd	f1, f1, f4
	sli	r1, r1, 0
	lwi	r5, r3 3
	add	r1 r5 r1
	lwi	r1, r1 0
	sub	r2, r1, r2
	lwi	r1, r3 1
	lwi	r5, r3 0
	lwi	r28, r3 2
	fadd	f31, f3 f0
	fadd	f3, f1 f0
	fadd	f1, f2 f0
	fadd	f2, f31 f0
	lwi	r29, r28 0
	jr	r29
scan_pixel.3037:
	lwi	r8, r28 8
	lwi	r9, r28 7
	lwi	r10, r28 6
	lwi	r11, r28 5
	lwi	r12, r28 4
	lwi	r13, r28 3
	lwi	r14, r28 2
	lwi	r15, r28 1
	sli	r16, r14, 0
	add	r16 r10 r16
	lwi	r16, r16 0
	sgt	r30, r16, r1
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22176
	jr	r31
ble_else.22176:
	sli	r16, r1, 0
	add	r16 r6 r16
	lwi	r16, r16 0
	lwi	r16, r16 0
	sli	r17, r14, 0
	add	r0 r16 r17
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r17, r14, 0
	add	r0 r9 r17
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r17, r15, 0
	add	r0 r16 r17
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r17, r15, 0
	add	r0 r9 r17
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r17, r13, 0
	add	r0 r16 r17
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r16, r13, 0
	add	r0 r9 r16
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r16, r15, 0
	add	r16 r10 r16
	lwi	r16, r16 0
	add	r17, r2, r15
	sgt	r30, r16, r17
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22178
	addi	r10, r0 0
	jl	ble_cont.22179
ble_else.22178:
	sgt	r30, r2, r14
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22180
	addi	r10, r0 0
	jl	ble_cont.22181
ble_else.22180:
	sli	r16, r14, 0
	add	r10 r10 r16
	lwi	r10, r10 0
	add	r16, r1, r15
	sgt	r30, r10, r16
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22182
	addi	r10, r0 0
	jl	ble_cont.22183
ble_else.22182:
	sgt	r30, r1, r14
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22184
	addi	r10, r0 0
	jl	ble_cont.22185
ble_else.22184:
	addi	r10, r0 1
ble_cont.22185:
ble_cont.22183:
ble_cont.22181:
ble_cont.22179:
	swi	r7, r3 0
	swi	r6, r3 1
	swi	r5, r3 2
	swi	r2, r3 3
	swi	r28, r3 4
	swi	r1, r3 5
	swi	r13, r3 6
	swi	r15, r3 7
	swi	r9, r3 8
	swi	r14, r3 9
	seq	r30, r10, r14
	jeql	r30, r0, beq_else.22186
	sli	r8, r1, 0
	add	r8 r6 r8
	lwi	r8, r8 0
	lwi	r10, r8 2
	sli	r16, r14, 0
	add	r10 r10 r16
	lwi	r10, r10 0
	sgt	r30, r14, r10
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22188
	lwi	r10, r8 3
	sli	r16, r14, 0
	add	r10 r10 r16
	lwi	r10, r10 0
	swi	r8, r3 10
	swi	r11, r3 11
	seq	r30, r10, r14
	jeql	r30, r0, beq_else.22190
	jl	beq_cont.22191
beq_else.22190:
	addi	r2, r14 0 #
	addi	r1, r8 0 #
	addi	r28, r12 0 #
	swi	r31 r3 12
	addi	r3, r3, 13
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -13
	lwi	r31, r3 12
beq_cont.22191:
	lwi	r1, r3 10
	lwi	r2, r3 7
	lwi	r28, r3 11
	swi	r31 r3 12
	addi	r3, r3, 13
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -13
	lwi	r31, r3 12
	jl	ble_cont.22189
ble_else.22188:
ble_cont.22189:
	jl	beq_cont.22187
beq_else.22186:
	addi	r28, r8 0 #
	addi	r8, r14 0 #
	swi	r31 r3 12
	addi	r3, r3, 13
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -13
	lwi	r31, r3 12
beq_cont.22187:
	lwi	r1, r3 9
	sli	r2, r1, 0
	lwi	r5, r3 8
	add	r0 r5 r2
	flwi	f1, r0 0
	sub	r0 r0 r0
	swi	r31 r3 12
	addi	r3, r3, 13
	jal	min_caml_int_of_float
	addi	r3, r3, -13
	lwi	r31 r3 12
	addi	r30, r0, 255
	sgt	r30, r1, r30
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22192
	lwi	r2, r3 9
	sgt	r30, r2, r1
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22194
	jl	ble_cont.22195
ble_else.22194:
	addi	r1, r0 0
ble_cont.22195:
	jl	ble_cont.22193
ble_else.22192:
	addi	r1, r0 255
ble_cont.22193:
	lwi	r2, r3 9
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.22196
	addi	r1, r2 0 #
	swi	r31 r3 12
	addi	r3, r3, 13
	jal	min_caml_print_int_sub
	addi	r3, r3, -13
	lwi	r31 r3 12
	jl	beq_cont.22197
beq_else.22196:
	sgt	r30, r2, r1
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22198
	swi	r31 r3 12
	addi	r3, r3, 13
	jal	print_int_rec.2591
	addi	r3, r3, -13
	lwi	r31 r3 12
	jl	ble_cont.22199
ble_else.22198:
	addi	r5, r0 45
	swi	r1, r3 12
	addi	r1, r5 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	jal	min_caml_print_char
	addi	r3, r3, -14
	lwi	r31 r3 13
	lwi	r1, r3 12
	lwi	r2, r3 9
	sub	r1, r2, r1
	swi	r31 r3 13
	addi	r3, r3, 14
	jal	print_int_rec.2591
	addi	r3, r3, -14
	lwi	r31 r3 13
ble_cont.22199:
beq_cont.22197:
	addi	r1, r0 32
	swi	r1, r3 13
	swi	r31 r3 14
	addi	r3, r3, 15
	jal	min_caml_print_char
	addi	r3, r3, -15
	lwi	r31 r3 14
	lwi	r1, r3 7
	sli	r2, r1, 0
	lwi	r5, r3 8
	add	r0 r5 r2
	flwi	f1, r0 0
	sub	r0 r0 r0
	swi	r31 r3 14
	addi	r3, r3, 15
	jal	min_caml_int_of_float
	addi	r3, r3, -15
	lwi	r31 r3 14
	addi	r30, r0, 255
	sgt	r30, r1, r30
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22200
	lwi	r2, r3 9
	sgt	r30, r2, r1
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22202
	jl	ble_cont.22203
ble_else.22202:
	addi	r1, r0 0
ble_cont.22203:
	jl	ble_cont.22201
ble_else.22200:
	addi	r1, r0 255
ble_cont.22201:
	lwi	r2, r3 9
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.22204
	addi	r1, r2 0 #
	swi	r31 r3 14
	addi	r3, r3, 15
	jal	min_caml_print_int_sub
	addi	r3, r3, -15
	lwi	r31 r3 14
	jl	beq_cont.22205
beq_else.22204:
	sgt	r30, r2, r1
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22206
	swi	r31 r3 14
	addi	r3, r3, 15
	jal	print_int_rec.2591
	addi	r3, r3, -15
	lwi	r31 r3 14
	jl	ble_cont.22207
ble_else.22206:
	addi	r5, r0 45
	swi	r1, r3 14
	addi	r1, r5 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	min_caml_print_char
	addi	r3, r3, -16
	lwi	r31 r3 15
	lwi	r1, r3 14
	lwi	r2, r3 9
	sub	r1, r2, r1
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	print_int_rec.2591
	addi	r3, r3, -16
	lwi	r31 r3 15
ble_cont.22207:
beq_cont.22205:
	lwi	r1, r3 13
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	min_caml_print_char
	addi	r3, r3, -16
	lwi	r31 r3 15
	lwi	r1, r3 6
	sli	r1, r1, 0
	lwi	r2, r3 8
	add	r0 r2 r1
	flwi	f1, r0 0
	sub	r0 r0 r0
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	min_caml_int_of_float
	addi	r3, r3, -16
	lwi	r31 r3 15
	addi	r30, r0, 255
	sgt	r30, r1, r30
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22208
	lwi	r2, r3 9
	sgt	r30, r2, r1
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22210
	jl	ble_cont.22211
ble_else.22210:
	addi	r1, r0 0
ble_cont.22211:
	jl	ble_cont.22209
ble_else.22208:
	addi	r1, r0 255
ble_cont.22209:
	lwi	r2, r3 9
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.22212
	addi	r1, r2 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	min_caml_print_int_sub
	addi	r3, r3, -16
	lwi	r31 r3 15
	jl	beq_cont.22213
beq_else.22212:
	sgt	r30, r2, r1
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22214
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	print_int_rec.2591
	addi	r3, r3, -16
	lwi	r31 r3 15
	jl	ble_cont.22215
ble_else.22214:
	addi	r5, r0 45
	swi	r1, r3 15
	addi	r1, r5 0 #
	swi	r31 r3 16
	addi	r3, r3, 17
	jal	min_caml_print_char
	addi	r3, r3, -17
	lwi	r31 r3 16
	lwi	r1, r3 15
	lwi	r2, r3 9
	sub	r1, r2, r1
	swi	r31 r3 16
	addi	r3, r3, 17
	jal	print_int_rec.2591
	addi	r3, r3, -17
	lwi	r31 r3 16
ble_cont.22215:
beq_cont.22213:
	addi	r1, r0 10
	swi	r31 r3 16
	addi	r3, r3, 17
	jal	min_caml_print_char
	addi	r3, r3, -17
	lwi	r31 r3 16
	lwi	r1, r3 7
	lwi	r2, r3 5
	add	r1, r2, r1
	lwi	r2, r3 3
	lwi	r5, r3 2
	lwi	r6, r3 1
	lwi	r7, r3 0
	lwi	r28, r3 4
	lwi	r29, r28 0
	jr	r29
scan_line.3043:
	lwi	r8, r28 7
	lwi	r9, r28 6
	lwi	r10, r28 5
	lwi	r11, r28 4
	lwi	r12, r28 3
	lwi	r13, r28 2
	lwi	r14, r28 1
	sli	r15, r14, 0
	add	r15 r10 r15
	lwi	r15, r15 0
	sgt	r30, r15, r1
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22216
	jr	r31
ble_else.22216:
	sli	r15, r14, 0
	add	r15 r10 r15
	lwi	r15, r15 0
	sub	r15, r15, r14
	swi	r28, r3 0
	swi	r9, r3 1
	swi	r10, r3 2
	swi	r11, r3 3
	swi	r12, r3 4
	swi	r7, r3 5
	swi	r14, r3 6
	swi	r6, r3 7
	swi	r5, r3 8
	swi	r2, r3 9
	swi	r1, r3 10
	swi	r13, r3 11
	swi	r8, r3 12
	sgt	r30, r15, r1
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22218
	jl	ble_cont.22219
ble_else.22218:
	add	r15, r1, r14
	addi	r5, r7 0 #
	addi	r2, r15 0 #
	addi	r1, r6 0 #
	addi	r28, r9 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
ble_cont.22219:
	lwi	r1, r3 11
	lwi	r2, r3 10
	lwi	r5, r3 9
	lwi	r6, r3 8
	lwi	r7, r3 7
	lwi	r28, r3 12
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
	lwi	r1, r3 6
	lwi	r2, r3 10
	add	r2, r2, r1
	lwi	r5, r3 4
	lwi	r6, r3 5
	add	r6, r6, r5
	lwi	r7, r3 3
	sgt	r30, r7, r6
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22220
	sub	r6, r6, r7
	jl	ble_cont.22221
ble_else.22220:
ble_cont.22221:
	sli	r8, r1, 0
	lwi	r9, r3 2
	add	r8 r9 r8
	lwi	r8, r8 0
	sgt	r30, r8, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22222
	jr	r31
ble_else.22222:
	sli	r8, r1, 0
	add	r8 r9 r8
	lwi	r8, r8 0
	sub	r8, r8, r1
	swi	r6, r3 13
	swi	r2, r3 14
	sgt	r30, r8, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22224
	jl	ble_cont.22225
ble_else.22224:
	add	r8, r2, r1
	lwi	r9, r3 9
	lwi	r28, r3 1
	addi	r5, r6 0 #
	addi	r2, r8 0 #
	addi	r1, r9 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -16
	lwi	r31, r3 15
ble_cont.22225:
	lwi	r1, r3 11
	lwi	r2, r3 14
	lwi	r5, r3 8
	lwi	r6, r3 7
	lwi	r7, r3 9
	lwi	r28, r3 12
	swi	r31 r3 15
	addi	r3, r3, 16
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -16
	lwi	r31, r3 15
	lwi	r1, r3 6
	lwi	r2, r3 14
	add	r1, r2, r1
	lwi	r2, r3 4
	lwi	r5, r3 13
	add	r2, r5, r2
	lwi	r5, r3 3
	sgt	r30, r5, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22226
	sub	r7, r2, r5
	jl	ble_cont.22227
ble_else.22226:
	addi	r7, r2 0
ble_cont.22227:
	lwi	r2, r3 7
	lwi	r5, r3 9
	lwi	r6, r3 8
	lwi	r28, r3 0
	lwi	r29, r28 0
	jr	r29
create_pixel.3051:
	lwi	r1, r28 6
	lwi	r2, r28 5
	lwi	r5, r28 4
	lwi	r6, r28 3
	lwi	r7, r28 2
	flwi	f1, r28 1 #
	swi	r6, r3 0
	swi	r2, r3 1
	swi	r7, r3 2
	swi	r1, r3 3
	fswi	f1, r3 4
	swi	r5, r3 5
	addi	r1, r5 0 #
	swi	r31 r3 6
	addi	r3, r3, 7
	jal	min_caml_create_float_array
	addi	r3, r3, -7
	lwi	r31 r3 6
	flwi	f1, r3 4 #off
	lwi	r2, r3 5
	swi	r1, r3 6
	addi	r1, r2 0 #
	swi	r31 r3 7
	addi	r3, r3, 8
	jal	min_caml_create_float_array
	addi	r3, r3, -8
	lwi	r31 r3 7
	addi	r2, r1 0 ##
	lwi	r1, r3 3
	swi	r31 r3 7
	addi	r3, r3, 8
	jal	min_caml_create_array
	addi	r3, r3, -8
	lwi	r31 r3 7
	flwi	f1, r3 4 #off
	lwi	r2, r3 5
	swi	r1, r3 7
	addi	r1, r2 0 #
	swi	r31 r3 8
	addi	r3, r3, 9
	jal	min_caml_create_float_array
	addi	r3, r3, -9
	lwi	r31 r3 8
	lwi	r2, r3 2
	sli	r5, r2, 0
	lwi	r6, r3 7
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 4 #off
	lwi	r1, r3 5
	swi	r31 r3 8
	addi	r3, r3, 9
	jal	min_caml_create_float_array
	addi	r3, r3, -9
	lwi	r31 r3 8
	lwi	r2, r3 1
	sli	r5, r2, 0
	lwi	r6, r3 7
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 4 #off
	lwi	r1, r3 5
	swi	r31 r3 8
	addi	r3, r3, 9
	jal	min_caml_create_float_array
	addi	r3, r3, -9
	lwi	r31 r3 8
	lwi	r2, r3 5
	sli	r5, r2, 0
	lwi	r6, r3 7
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 4 #off
	addi	r1, r2 0 #
	swi	r31 r3 8
	addi	r3, r3, 9
	jal	min_caml_create_float_array
	addi	r3, r3, -9
	lwi	r31 r3 8
	lwi	r2, r3 7
	swi	r1, r2 4
	lwi	r1, r3 3
	lwi	r5, r3 0
	addi	r2, r5 0 #
	swi	r31 r3 8
	addi	r3, r3, 9
	jal	min_caml_create_array
	addi	r3, r3, -9
	lwi	r31 r3 8
	lwi	r2, r3 3
	lwi	r5, r3 0
	swi	r1, r3 8
	addi	r1, r2 0 #
	addi	r2, r5 0 #
	swi	r31 r3 9
	addi	r3, r3, 10
	jal	min_caml_create_array
	addi	r3, r3, -10
	lwi	r31 r3 9
	flwi	f1, r3 4 #off
	lwi	r2, r3 5
	swi	r1, r3 9
	addi	r1, r2 0 #
	swi	r31 r3 10
	addi	r3, r3, 11
	jal	min_caml_create_float_array
	addi	r3, r3, -11
	lwi	r31 r3 10
	addi	r2, r1 0 ##
	lwi	r1, r3 3
	swi	r31 r3 10
	addi	r3, r3, 11
	jal	min_caml_create_array
	addi	r3, r3, -11
	lwi	r31 r3 10
	flwi	f1, r3 4 #off
	lwi	r2, r3 5
	swi	r1, r3 10
	addi	r1, r2 0 #
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	min_caml_create_float_array
	addi	r3, r3, -12
	lwi	r31 r3 11
	lwi	r2, r3 2
	sli	r5, r2, 0
	lwi	r6, r3 10
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 4 #off
	lwi	r1, r3 5
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	min_caml_create_float_array
	addi	r3, r3, -12
	lwi	r31 r3 11
	lwi	r2, r3 1
	sli	r5, r2, 0
	lwi	r6, r3 10
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 4 #off
	lwi	r1, r3 5
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	min_caml_create_float_array
	addi	r3, r3, -12
	lwi	r31 r3 11
	lwi	r2, r3 5
	sli	r5, r2, 0
	lwi	r6, r3 10
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 4 #off
	addi	r1, r2 0 #
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	min_caml_create_float_array
	addi	r3, r3, -12
	lwi	r31 r3 11
	lwi	r2, r3 10
	swi	r1, r2 4
	flwi	f1, r3 4 #off
	lwi	r1, r3 5
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	min_caml_create_float_array
	addi	r3, r3, -12
	lwi	r31 r3 11
	addi	r2, r1 0 ##
	lwi	r1, r3 3
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	min_caml_create_array
	addi	r3, r3, -12
	lwi	r31 r3 11
	flwi	f1, r3 4 #off
	lwi	r2, r3 5
	swi	r1, r3 11
	addi	r1, r2 0 #
	swi	r31 r3 12
	addi	r3, r3, 13
	jal	min_caml_create_float_array
	addi	r3, r3, -13
	lwi	r31 r3 12
	lwi	r2, r3 2
	sli	r5, r2, 0
	lwi	r6, r3 11
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 4 #off
	lwi	r1, r3 5
	swi	r31 r3 12
	addi	r3, r3, 13
	jal	min_caml_create_float_array
	addi	r3, r3, -13
	lwi	r31 r3 12
	lwi	r2, r3 1
	sli	r5, r2, 0
	lwi	r6, r3 11
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 4 #off
	lwi	r1, r3 5
	swi	r31 r3 12
	addi	r3, r3, 13
	jal	min_caml_create_float_array
	addi	r3, r3, -13
	lwi	r31 r3 12
	lwi	r2, r3 5
	sli	r5, r2, 0
	lwi	r6, r3 11
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 4 #off
	addi	r1, r2 0 #
	swi	r31 r3 12
	addi	r3, r3, 13
	jal	min_caml_create_float_array
	addi	r3, r3, -13
	lwi	r31 r3 12
	lwi	r2, r3 11
	swi	r1, r2 4
	lwi	r1, r3 2
	lwi	r5, r3 0
	addi	r2, r5 0 #
	swi	r31 r3 12
	addi	r3, r3, 13
	jal	min_caml_create_array
	addi	r3, r3, -13
	lwi	r31 r3 12
	flwi	f1, r3 4 #off
	lwi	r2, r3 5
	swi	r1, r3 12
	addi	r1, r2 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	jal	min_caml_create_float_array
	addi	r3, r3, -14
	lwi	r31 r3 13
	addi	r2, r1 0 ##
	lwi	r1, r3 3
	swi	r31 r3 13
	addi	r3, r3, 14
	jal	min_caml_create_array
	addi	r3, r3, -14
	lwi	r31 r3 13
	flwi	f1, r3 4 #off
	lwi	r2, r3 5
	swi	r1, r3 13
	addi	r1, r2 0 #
	swi	r31 r3 14
	addi	r3, r3, 15
	jal	min_caml_create_float_array
	addi	r3, r3, -15
	lwi	r31 r3 14
	lwi	r2, r3 2
	sli	r2, r2, 0
	lwi	r5, r3 13
	add	r0 r5 r2
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 4 #off
	lwi	r1, r3 5
	swi	r31 r3 14
	addi	r3, r3, 15
	jal	min_caml_create_float_array
	addi	r3, r3, -15
	lwi	r31 r3 14
	lwi	r2, r3 1
	sli	r2, r2, 0
	lwi	r5, r3 13
	add	r0 r5 r2
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 4 #off
	lwi	r1, r3 5
	swi	r31 r3 14
	addi	r3, r3, 15
	jal	min_caml_create_float_array
	addi	r3, r3, -15
	lwi	r31 r3 14
	lwi	r2, r3 5
	sli	r5, r2, 0
	lwi	r6, r3 13
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 4 #off
	addi	r1, r2 0 #
	swi	r31 r3 14
	addi	r3, r3, 15
	jal	min_caml_create_float_array
	addi	r3, r3, -15
	lwi	r31 r3 14
	lwi	r2, r3 13
	swi	r1, r2 4
	addi	r1, r4 0
	addi	r4, r4, 8
	swi	r2, r1 7
	lwi	r2, r3 12
	swi	r2, r1 6
	lwi	r2, r3 11
	swi	r2, r1 5
	lwi	r2, r3 10
	swi	r2, r1 4
	lwi	r2, r3 9
	swi	r2, r1 3
	lwi	r2, r3 8
	swi	r2, r1 2
	lwi	r2, r3 7
	swi	r2, r1 1
	lwi	r2, r3 6
	swi	r2, r1 0
	jr	r31
init_line_elements.3053:
	lwi	r5, r28 7
	lwi	r6, r28 6
	lwi	r7, r28 5
	lwi	r8, r28 4
	lwi	r9, r28 3
	lwi	r10, r28 2
	flwi	f1, r28 1 #
	sgt	r30, r9, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22228
	swi	r28, r3 0
	swi	r5, r3 1
	swi	r1, r3 2
	swi	r2, r3 3
	swi	r9, r3 4
	swi	r7, r3 5
	swi	r10, r3 6
	swi	r6, r3 7
	fswi	f1, r3 8
	swi	r8, r3 9
	addi	r1, r8 0 #
	swi	r31 r3 10
	addi	r3, r3, 11
	jal	min_caml_create_float_array
	addi	r3, r3, -11
	lwi	r31 r3 10
	flwi	f1, r3 8 #off
	lwi	r2, r3 9
	swi	r1, r3 10
	addi	r1, r2 0 #
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	min_caml_create_float_array
	addi	r3, r3, -12
	lwi	r31 r3 11
	addi	r2, r1 0 ##
	lwi	r1, r3 7
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	min_caml_create_array
	addi	r3, r3, -12
	lwi	r31 r3 11
	flwi	f1, r3 8 #off
	lwi	r2, r3 9
	swi	r1, r3 11
	addi	r1, r2 0 #
	swi	r31 r3 12
	addi	r3, r3, 13
	jal	min_caml_create_float_array
	addi	r3, r3, -13
	lwi	r31 r3 12
	lwi	r2, r3 6
	sli	r5, r2, 0
	lwi	r6, r3 11
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	lwi	r1, r3 9
	swi	r31 r3 12
	addi	r3, r3, 13
	jal	min_caml_create_float_array
	addi	r3, r3, -13
	lwi	r31 r3 12
	lwi	r2, r3 5
	sli	r5, r2, 0
	lwi	r6, r3 11
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	lwi	r1, r3 9
	swi	r31 r3 12
	addi	r3, r3, 13
	jal	min_caml_create_float_array
	addi	r3, r3, -13
	lwi	r31 r3 12
	lwi	r2, r3 9
	sli	r5, r2, 0
	lwi	r6, r3 11
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	addi	r1, r2 0 #
	swi	r31 r3 12
	addi	r3, r3, 13
	jal	min_caml_create_float_array
	addi	r3, r3, -13
	lwi	r31 r3 12
	lwi	r2, r3 11
	swi	r1, r2 4
	lwi	r1, r3 7
	lwi	r5, r3 4
	addi	r2, r5 0 #
	swi	r31 r3 12
	addi	r3, r3, 13
	jal	min_caml_create_array
	addi	r3, r3, -13
	lwi	r31 r3 12
	lwi	r2, r3 7
	lwi	r5, r3 4
	swi	r1, r3 12
	addi	r1, r2 0 #
	addi	r2, r5 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	jal	min_caml_create_array
	addi	r3, r3, -14
	lwi	r31 r3 13
	flwi	f1, r3 8 #off
	lwi	r2, r3 9
	swi	r1, r3 13
	addi	r1, r2 0 #
	swi	r31 r3 14
	addi	r3, r3, 15
	jal	min_caml_create_float_array
	addi	r3, r3, -15
	lwi	r31 r3 14
	addi	r2, r1 0 ##
	lwi	r1, r3 7
	swi	r31 r3 14
	addi	r3, r3, 15
	jal	min_caml_create_array
	addi	r3, r3, -15
	lwi	r31 r3 14
	flwi	f1, r3 8 #off
	lwi	r2, r3 9
	swi	r1, r3 14
	addi	r1, r2 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	min_caml_create_float_array
	addi	r3, r3, -16
	lwi	r31 r3 15
	lwi	r2, r3 6
	sli	r5, r2, 0
	lwi	r6, r3 14
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	lwi	r1, r3 9
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	min_caml_create_float_array
	addi	r3, r3, -16
	lwi	r31 r3 15
	lwi	r2, r3 5
	sli	r5, r2, 0
	lwi	r6, r3 14
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	lwi	r1, r3 9
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	min_caml_create_float_array
	addi	r3, r3, -16
	lwi	r31 r3 15
	lwi	r2, r3 9
	sli	r5, r2, 0
	lwi	r6, r3 14
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	addi	r1, r2 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	min_caml_create_float_array
	addi	r3, r3, -16
	lwi	r31 r3 15
	lwi	r2, r3 14
	swi	r1, r2 4
	flwi	f1, r3 8 #off
	lwi	r1, r3 9
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	min_caml_create_float_array
	addi	r3, r3, -16
	lwi	r31 r3 15
	addi	r2, r1 0 ##
	lwi	r1, r3 7
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	min_caml_create_array
	addi	r3, r3, -16
	lwi	r31 r3 15
	flwi	f1, r3 8 #off
	lwi	r2, r3 9
	swi	r1, r3 15
	addi	r1, r2 0 #
	swi	r31 r3 16
	addi	r3, r3, 17
	jal	min_caml_create_float_array
	addi	r3, r3, -17
	lwi	r31 r3 16
	lwi	r2, r3 6
	sli	r5, r2, 0
	lwi	r6, r3 15
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	lwi	r1, r3 9
	swi	r31 r3 16
	addi	r3, r3, 17
	jal	min_caml_create_float_array
	addi	r3, r3, -17
	lwi	r31 r3 16
	lwi	r2, r3 5
	sli	r5, r2, 0
	lwi	r6, r3 15
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	lwi	r1, r3 9
	swi	r31 r3 16
	addi	r3, r3, 17
	jal	min_caml_create_float_array
	addi	r3, r3, -17
	lwi	r31 r3 16
	lwi	r2, r3 9
	sli	r5, r2, 0
	lwi	r6, r3 15
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	addi	r1, r2 0 #
	swi	r31 r3 16
	addi	r3, r3, 17
	jal	min_caml_create_float_array
	addi	r3, r3, -17
	lwi	r31 r3 16
	lwi	r2, r3 15
	swi	r1, r2 4
	lwi	r1, r3 6
	lwi	r5, r3 4
	addi	r2, r5 0 #
	swi	r31 r3 16
	addi	r3, r3, 17
	jal	min_caml_create_array
	addi	r3, r3, -17
	lwi	r31 r3 16
	flwi	f1, r3 8 #off
	lwi	r2, r3 9
	swi	r1, r3 16
	addi	r1, r2 0 #
	swi	r31 r3 17
	addi	r3, r3, 18
	jal	min_caml_create_float_array
	addi	r3, r3, -18
	lwi	r31 r3 17
	addi	r2, r1 0 ##
	lwi	r1, r3 7
	swi	r31 r3 17
	addi	r3, r3, 18
	jal	min_caml_create_array
	addi	r3, r3, -18
	lwi	r31 r3 17
	flwi	f1, r3 8 #off
	lwi	r2, r3 9
	swi	r1, r3 17
	addi	r1, r2 0 #
	swi	r31 r3 18
	addi	r3, r3, 19
	jal	min_caml_create_float_array
	addi	r3, r3, -19
	lwi	r31 r3 18
	lwi	r2, r3 6
	sli	r5, r2, 0
	lwi	r6, r3 17
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	lwi	r1, r3 9
	swi	r31 r3 18
	addi	r3, r3, 19
	jal	min_caml_create_float_array
	addi	r3, r3, -19
	lwi	r31 r3 18
	lwi	r2, r3 5
	sli	r5, r2, 0
	lwi	r6, r3 17
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	lwi	r1, r3 9
	swi	r31 r3 18
	addi	r3, r3, 19
	jal	min_caml_create_float_array
	addi	r3, r3, -19
	lwi	r31 r3 18
	lwi	r2, r3 9
	sli	r5, r2, 0
	lwi	r6, r3 17
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	addi	r1, r2 0 #
	swi	r31 r3 18
	addi	r3, r3, 19
	jal	min_caml_create_float_array
	addi	r3, r3, -19
	lwi	r31 r3 18
	lwi	r2, r3 17
	swi	r1, r2 4
	addi	r1, r4 0
	addi	r4, r4, 8
	swi	r2, r1 7
	lwi	r2, r3 16
	swi	r2, r1 6
	lwi	r2, r3 15
	swi	r2, r1 5
	lwi	r2, r3 14
	swi	r2, r1 4
	lwi	r2, r3 13
	swi	r2, r1 3
	lwi	r2, r3 12
	swi	r2, r1 2
	lwi	r2, r3 11
	swi	r2, r1 1
	lwi	r2, r3 10
	swi	r2, r1 0
	lwi	r2, r3 3
	sli	r5, r2, 0
	lwi	r6, r3 2
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 6
	sub	r2, r2, r1
	lwi	r5, r3 4
	sgt	r30, r5, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22229
	flwi	f1, r3 8 #off
	lwi	r7, r3 9
	swi	r2, r3 18
	addi	r1, r7 0 #
	swi	r31 r3 19
	addi	r3, r3, 20
	jal	min_caml_create_float_array
	addi	r3, r3, -20
	lwi	r31 r3 19
	flwi	f1, r3 8 #off
	lwi	r2, r3 9
	swi	r1, r3 19
	addi	r1, r2 0 #
	swi	r31 r3 20
	addi	r3, r3, 21
	jal	min_caml_create_float_array
	addi	r3, r3, -21
	lwi	r31 r3 20
	addi	r2, r1 0 ##
	lwi	r1, r3 7
	swi	r31 r3 20
	addi	r3, r3, 21
	jal	min_caml_create_array
	addi	r3, r3, -21
	lwi	r31 r3 20
	flwi	f1, r3 8 #off
	lwi	r2, r3 9
	swi	r1, r3 20
	addi	r1, r2 0 #
	swi	r31 r3 21
	addi	r3, r3, 22
	jal	min_caml_create_float_array
	addi	r3, r3, -22
	lwi	r31 r3 21
	lwi	r2, r3 6
	sli	r5, r2, 0
	lwi	r6, r3 20
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	lwi	r1, r3 9
	swi	r31 r3 21
	addi	r3, r3, 22
	jal	min_caml_create_float_array
	addi	r3, r3, -22
	lwi	r31 r3 21
	lwi	r2, r3 5
	sli	r5, r2, 0
	lwi	r6, r3 20
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	lwi	r1, r3 9
	swi	r31 r3 21
	addi	r3, r3, 22
	jal	min_caml_create_float_array
	addi	r3, r3, -22
	lwi	r31 r3 21
	lwi	r2, r3 9
	sli	r5, r2, 0
	lwi	r6, r3 20
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	addi	r1, r2 0 #
	swi	r31 r3 21
	addi	r3, r3, 22
	jal	min_caml_create_float_array
	addi	r3, r3, -22
	lwi	r31 r3 21
	lwi	r2, r3 20
	swi	r1, r2 4
	lwi	r1, r3 7
	lwi	r5, r3 4
	addi	r2, r5 0 #
	swi	r31 r3 21
	addi	r3, r3, 22
	jal	min_caml_create_array
	addi	r3, r3, -22
	lwi	r31 r3 21
	lwi	r2, r3 7
	lwi	r5, r3 4
	swi	r1, r3 21
	addi	r1, r2 0 #
	addi	r2, r5 0 #
	swi	r31 r3 22
	addi	r3, r3, 23
	jal	min_caml_create_array
	addi	r3, r3, -23
	lwi	r31 r3 22
	flwi	f1, r3 8 #off
	lwi	r2, r3 9
	swi	r1, r3 22
	addi	r1, r2 0 #
	swi	r31 r3 23
	addi	r3, r3, 24
	jal	min_caml_create_float_array
	addi	r3, r3, -24
	lwi	r31 r3 23
	addi	r2, r1 0 ##
	lwi	r1, r3 7
	swi	r31 r3 23
	addi	r3, r3, 24
	jal	min_caml_create_array
	addi	r3, r3, -24
	lwi	r31 r3 23
	flwi	f1, r3 8 #off
	lwi	r2, r3 9
	swi	r1, r3 23
	addi	r1, r2 0 #
	swi	r31 r3 24
	addi	r3, r3, 25
	jal	min_caml_create_float_array
	addi	r3, r3, -25
	lwi	r31 r3 24
	lwi	r2, r3 6
	sli	r5, r2, 0
	lwi	r6, r3 23
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	lwi	r1, r3 9
	swi	r31 r3 24
	addi	r3, r3, 25
	jal	min_caml_create_float_array
	addi	r3, r3, -25
	lwi	r31 r3 24
	lwi	r2, r3 5
	sli	r5, r2, 0
	lwi	r6, r3 23
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	lwi	r1, r3 9
	swi	r31 r3 24
	addi	r3, r3, 25
	jal	min_caml_create_float_array
	addi	r3, r3, -25
	lwi	r31 r3 24
	lwi	r2, r3 9
	sli	r5, r2, 0
	lwi	r6, r3 23
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	addi	r1, r2 0 #
	swi	r31 r3 24
	addi	r3, r3, 25
	jal	min_caml_create_float_array
	addi	r3, r3, -25
	lwi	r31 r3 24
	lwi	r2, r3 23
	swi	r1, r2 4
	flwi	f1, r3 8 #off
	lwi	r1, r3 9
	swi	r31 r3 24
	addi	r3, r3, 25
	jal	min_caml_create_float_array
	addi	r3, r3, -25
	lwi	r31 r3 24
	addi	r2, r1 0 ##
	lwi	r1, r3 7
	swi	r31 r3 24
	addi	r3, r3, 25
	jal	min_caml_create_array
	addi	r3, r3, -25
	lwi	r31 r3 24
	flwi	f1, r3 8 #off
	lwi	r2, r3 9
	swi	r1, r3 24
	addi	r1, r2 0 #
	swi	r31 r3 25
	addi	r3, r3, 26
	jal	min_caml_create_float_array
	addi	r3, r3, -26
	lwi	r31 r3 25
	lwi	r2, r3 6
	sli	r5, r2, 0
	lwi	r6, r3 24
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	lwi	r1, r3 9
	swi	r31 r3 25
	addi	r3, r3, 26
	jal	min_caml_create_float_array
	addi	r3, r3, -26
	lwi	r31 r3 25
	lwi	r2, r3 5
	sli	r5, r2, 0
	lwi	r6, r3 24
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	lwi	r1, r3 9
	swi	r31 r3 25
	addi	r3, r3, 26
	jal	min_caml_create_float_array
	addi	r3, r3, -26
	lwi	r31 r3 25
	lwi	r2, r3 9
	sli	r5, r2, 0
	lwi	r6, r3 24
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	addi	r1, r2 0 #
	swi	r31 r3 25
	addi	r3, r3, 26
	jal	min_caml_create_float_array
	addi	r3, r3, -26
	lwi	r31 r3 25
	lwi	r2, r3 24
	swi	r1, r2 4
	lwi	r1, r3 6
	lwi	r5, r3 4
	addi	r2, r5 0 #
	swi	r31 r3 25
	addi	r3, r3, 26
	jal	min_caml_create_array
	addi	r3, r3, -26
	lwi	r31 r3 25
	flwi	f1, r3 8 #off
	lwi	r2, r3 9
	swi	r1, r3 25
	addi	r1, r2 0 #
	swi	r31 r3 26
	addi	r3, r3, 27
	jal	min_caml_create_float_array
	addi	r3, r3, -27
	lwi	r31 r3 26
	addi	r2, r1 0 ##
	lwi	r1, r3 7
	swi	r31 r3 26
	addi	r3, r3, 27
	jal	min_caml_create_array
	addi	r3, r3, -27
	lwi	r31 r3 26
	flwi	f1, r3 8 #off
	lwi	r2, r3 9
	swi	r1, r3 26
	addi	r1, r2 0 #
	swi	r31 r3 27
	addi	r3, r3, 28
	jal	min_caml_create_float_array
	addi	r3, r3, -28
	lwi	r31 r3 27
	lwi	r2, r3 6
	sli	r5, r2, 0
	lwi	r6, r3 26
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	lwi	r1, r3 9
	swi	r31 r3 27
	addi	r3, r3, 28
	jal	min_caml_create_float_array
	addi	r3, r3, -28
	lwi	r31 r3 27
	lwi	r2, r3 5
	sli	r2, r2, 0
	lwi	r5, r3 26
	add	r0 r5 r2
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	lwi	r1, r3 9
	swi	r31 r3 27
	addi	r3, r3, 28
	jal	min_caml_create_float_array
	addi	r3, r3, -28
	lwi	r31 r3 27
	lwi	r2, r3 9
	sli	r5, r2, 0
	lwi	r6, r3 26
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 8 #off
	addi	r1, r2 0 #
	swi	r31 r3 27
	addi	r3, r3, 28
	jal	min_caml_create_float_array
	addi	r3, r3, -28
	lwi	r31 r3 27
	lwi	r2, r3 26
	swi	r1, r2 4
	addi	r1, r4 0
	addi	r4, r4, 8
	swi	r2, r1 7
	lwi	r2, r3 25
	swi	r2, r1 6
	lwi	r2, r3 24
	swi	r2, r1 5
	lwi	r2, r3 23
	swi	r2, r1 4
	lwi	r2, r3 22
	swi	r2, r1 3
	lwi	r2, r3 21
	swi	r2, r1 2
	lwi	r2, r3 20
	swi	r2, r1 1
	lwi	r2, r3 19
	swi	r2, r1 0
	lwi	r2, r3 18
	sli	r5, r2, 0
	lwi	r6, r3 2
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 6
	sub	r2, r2, r1
	lwi	r5, r3 4
	sgt	r30, r5, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22230
	lwi	r28, r3 1
	swi	r2, r3 27
	swi	r31 r3 28
	addi	r3, r3, 29
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -29
	lwi	r31, r3 28
	lwi	r2, r3 27
	sli	r5, r2, 0
	lwi	r6, r3 2
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 6
	sub	r2, r2, r1
	lwi	r5, r3 4
	sgt	r30, r5, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22231
	lwi	r28, r3 1
	swi	r2, r3 28
	swi	r31 r3 29
	addi	r3, r3, 30
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -30
	lwi	r31, r3 29
	lwi	r2, r3 28
	sli	r5, r2, 0
	lwi	r6, r3 2
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 6
	sub	r2, r2, r1
	lwi	r28, r3 0
	addi	r1, r6 0 #
	lwi	r29, r28 0
	jr	r29
ble_else.22231:
	addi	r1, r6 0
	jr	r31
ble_else.22230:
	addi	r1, r6 0
	jr	r31
ble_else.22229:
	addi	r1, r6 0
	jr	r31
ble_else.22228:
	jr	r31
calc_dirvec.3063:
	lwi	r6, r28 6
	lwi	r7, r28 5
	lwi	r8, r28 4
	lwi	r9, r28 3
	lwi	r10, r28 2
	flwi	f5, r28 1 #
	sgt	r30, r7, r1
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22232
	fmul	f3, f1, f1
	fmul	f4, f2, f2
	fadd	f3, f3, f4
	flwl	%f4 l.11794
	fadd	f3, f3, f4
	fswi	f5, r3 0
	swi	r8, r3 1
	swi	r10, r3 2
	swi	r9, r3 3
	swi	r5, r3 4
	swi	r6, r3 5
	swi	r2, r3 6
	fswi	f4, r3 8
	fswi	f2, r3 10
	fswi	f1, r3 12
	fadd	f1, f3 f0
	swi	r31 r3 13
	addi	r3, r3, 14
	jal	min_caml_sqrt
	addi	r3, r3, -14
	lwi	r31 r3 13
	flwi	f2, r3 12 #off
	fdiv	f2, f2, f1
	flwi	f3, r3 10 #off
	fdiv	f3, f3, f1
	flwi	f4, r3 8 #off
	fdiv	f1, f4, f1
	lwi	r1, r3 6
	sli	r1, r1, 0
	lwi	r2, r3 5
	add	r1 r2 r1
	lwi	r1, r1 0
	lwi	r2, r3 4
	sli	r5, r2, 0
	add	r5 r1 r5
	lwi	r5, r5 0
	lwi	r5, r5 0
	lwi	r6, r3 3
	sli	r7, r6, 0
	add	r0 r5 r7
	fswi	f2, r0 0
	sub	r0 r0 r0
	lwi	r7, r3 2
	sli	r8, r7, 0
	add	r0 r5 r8
	fswi	f3, r0 0
	sub	r0 r0 r0
	lwi	r8, r3 1
	sli	r9, r8, 0
	add	r0 r5 r9
	fswi	f1, r0 0
	sub	r0 r0 r0
	addi	r5, r2, 40
	sli	r5, r5, 0
	add	r5 r1 r5
	lwi	r5, r5 0
	lwi	r5, r5 0
	flwi	f4, r3 0 #off
	fsub	f5, f4, f3
	sli	r9, r6, 0
	add	r0 r5 r9
	fswi	f2, r0 0
	sub	r0 r0 r0
	sli	r9, r7, 0
	add	r0 r5 r9
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r9, r8, 0
	add	r0 r5 r9
	fswi	f5, r0 0
	sub	r0 r0 r0
	addi	r5, r2, 80
	sli	r5, r5, 0
	add	r5 r1 r5
	lwi	r5, r5 0
	lwi	r5, r5 0
	fsub	f5, f4, f2
	fsub	f6, f4, f3
	sli	r9, r6, 0
	add	r0 r5 r9
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r9, r7, 0
	add	r0 r5 r9
	fswi	f5, r0 0
	sub	r0 r0 r0
	sli	r9, r8, 0
	add	r0 r5 r9
	fswi	f6, r0 0
	sub	r0 r0 r0
	add	r5, r2, r7
	sli	r5, r5, 0
	add	r5 r1 r5
	lwi	r5, r5 0
	lwi	r5, r5 0
	fsub	f5, f4, f2
	fsub	f6, f4, f3
	fsub	f7, f4, f1
	sli	r9, r6, 0
	add	r0 r5 r9
	fswi	f5, r0 0
	sub	r0 r0 r0
	sli	r9, r7, 0
	add	r0 r5 r9
	fswi	f6, r0 0
	sub	r0 r0 r0
	sli	r9, r8, 0
	add	r0 r5 r9
	fswi	f7, r0 0
	sub	r0 r0 r0
	addi	r5, r2, 41
	sli	r5, r5, 0
	add	r5 r1 r5
	lwi	r5, r5 0
	lwi	r5, r5 0
	fsub	f5, f4, f2
	fsub	f6, f4, f1
	sli	r9, r6, 0
	add	r0 r5 r9
	fswi	f5, r0 0
	sub	r0 r0 r0
	sli	r9, r7, 0
	add	r0 r5 r9
	fswi	f6, r0 0
	sub	r0 r0 r0
	sli	r9, r8, 0
	add	r0 r5 r9
	fswi	f3, r0 0
	sub	r0 r0 r0
	addi	r2, r2, 81
	sli	r2, r2, 0
	add	r1 r1 r2
	lwi	r1, r1 0
	lwi	r1, r1 0
	fsub	f1, f4, f1
	sli	r2, r6, 0
	add	r0 r1 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r2, r7, 0
	add	r0 r1 r2
	fswi	f2, r0 0
	sub	r0 r0 r0
	sli	r2, r8, 0
	add	r0 r1 r2
	fswi	f3, r0 0
	sub	r0 r0 r0
	jr	r31
ble_else.22232:
	fmul	f1, f2, f2
	flwl	%f2 l.12474
	fadd	f1, f1, f2
	swi	r5, r3 4
	swi	r2, r3 6
	swi	r28, r3 13
	fswi	f4, r3 14
	fswi	f2, r3 16
	swi	r10, r3 2
	swi	r1, r3 17
	fswi	f3, r3 18
	swi	r31 r3 19
	addi	r3, r3, 20
	jal	min_caml_sqrt
	addi	r3, r3, -20
	lwi	r31 r3 19
	flwl	%f2 l.11794
	fdiv	f3, f2, f1
	fswi	f2, r3 20
	fswi	f1, r3 22
	fadd	f1, f3 f0
	swi	r31 r3 23
	addi	r3, r3, 24
	jal	atan.2576
	addi	r3, r3, -24
	lwi	r31 r3 23
	flwi	f2, r3 18 #off
	fmul	f1, f1, f2
	fswi	f1, r3 24
	swi	r31 r3 25
	addi	r3, r3, 26
	jal	sin.2572
	addi	r3, r3, -26
	lwi	r31 r3 25
	flwl	%f2 l.11781
	flwi	f3, r3 24 #off
	fsub	f3, f2, f3
	fswi	f2, r3 26
	fswi	f1, r3 28
	fadd	f1, f3 f0
	swi	r31 r3 29
	addi	r3, r3, 30
	jal	sin.2572
	addi	r3, r3, -30
	lwi	r31 r3 29
	flwi	f2, r3 28 #off
	fdiv	f1, f2, f1
	flwi	f2, r3 22 #off
	fmul	f1, f1, f2
	lwi	r1, r3 2
	lwi	r2, r3 17
	add	r1, r2, r1
	fmul	f2, f1, f1
	flwi	f3, r3 16 #off
	fadd	f2, f2, f3
	fswi	f1, r3 30
	swi	r1, r3 31
	fadd	f1, f2 f0
	swi	r31 r3 32
	addi	r3, r3, 33
	jal	min_caml_sqrt
	addi	r3, r3, -33
	lwi	r31 r3 32
	flwi	f2, r3 20 #off
	fdiv	f2, f2, f1
	fswi	f1, r3 32
	fadd	f1, f2 f0
	swi	r31 r3 33
	addi	r3, r3, 34
	jal	atan.2576
	addi	r3, r3, -34
	lwi	r31 r3 33
	flwi	f2, r3 14 #off
	fmul	f1, f1, f2
	fswi	f1, r3 34
	swi	r31 r3 35
	addi	r3, r3, 36
	jal	sin.2572
	addi	r3, r3, -36
	lwi	r31 r3 35
	flwi	f2, r3 34 #off
	flwi	f3, r3 26 #off
	fsub	f2, f3, f2
	fswi	f1, r3 36
	fadd	f1, f2 f0
	swi	r31 r3 37
	addi	r3, r3, 38
	jal	sin.2572
	addi	r3, r3, -38
	lwi	r31 r3 37
	flwi	f2, r3 36 #off
	fdiv	f1, f2, f1
	flwi	f2, r3 32 #off
	fmul	f2, f1, f2
	flwi	f1, r3 30 #off
	flwi	f3, r3 18 #off
	flwi	f4, r3 14 #off
	lwi	r1, r3 31
	lwi	r2, r3 6
	lwi	r5, r3 4
	lwi	r28, r3 13
	lwi	r29, r28 0
	jr	r29
calc_dirvecs.3071:
	lwi	r6, r28 6
	lwi	r7, r28 5
	lwi	r8, r28 4
	lwi	r9, r28 3
	lwi	r10, r28 2
	flwi	f2, r28 1 #
	sgt	r30, r9, r1
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22246
	swi	r28, r3 0
	swi	r7, r3 1
	swi	r10, r3 2
	swi	r8, r3 3
	swi	r1, r3 4
	fswi	f1, r3 6
	fswi	f2, r3 8
	swi	r5, r3 9
	swi	r2, r3 10
	swi	r9, r3 11
	swi	r6, r3 12
	swi	r31 r3 13
	addi	r3, r3, 14
	jal	min_caml_float_of_int
	addi	r3, r3, -14
	lwi	r31 r3 13
	flwl	%f2 l.11805
	fmul	f1, f1, f2
	flwl	%f3 l.12859
	fsub	f3, f1, f3
	flwi	f1, r3 8 #off
	flwi	f4, r3 6 #off
	lwi	r1, r3 11
	lwi	r2, r3 10
	lwi	r5, r3 9
	lwi	r28, r3 12
	fswi	f2, r3 14
	fadd	f2, f1 f0
	swi	r31 r3 15
	addi	r3, r3, 16
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -16
	lwi	r31, r3 15
	lwi	r1, r3 4
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	min_caml_float_of_int
	addi	r3, r3, -16
	lwi	r31 r3 15
	flwi	f2, r3 14 #off
	fmul	f1, f1, f2
	flwl	%f2 l.12474
	fadd	f3, f1, f2
	lwi	r1, r3 3
	lwi	r2, r3 9
	add	r5, r2, r1
	flwi	f1, r3 8 #off
	flwi	f4, r3 6 #off
	lwi	r1, r3 11
	lwi	r6, r3 10
	lwi	r28, r3 12
	addi	r2, r6 0 #
	fadd	f2, f1 f0
	swi	r31 r3 15
	addi	r3, r3, 16
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -16
	lwi	r31, r3 15
	lwi	r1, r3 2
	lwi	r2, r3 4
	sub	r2, r2, r1
	lwi	r5, r3 10
	add	r1, r5, r1
	lwi	r5, r3 1
	sgt	r30, r5, r1
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22250
	sub	r1, r1, r5
	jl	ble_cont.22251
ble_else.22250:
ble_cont.22251:
	flwi	f1, r3 6 #off
	lwi	r5, r3 9
	lwi	r28, r3 0
	addi	r29, r2 0 #
	addi	r2, r1 0 #
	addi	r1, r29 0 #
	lwi	r29, r28 0
	jr	r29
ble_else.22246:
	jr	r31
calc_dirvec_rows.3076:
	lwi	r6, r28 5
	lwi	r7, r28 4
	lwi	r8, r28 3
	lwi	r9, r28 2
	lwi	r10, r28 1
	sgt	r30, r9, r1
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22253
	swi	r28, r3 0
	swi	r9, r3 1
	swi	r7, r3 2
	swi	r8, r3 3
	swi	r10, r3 4
	swi	r1, r3 5
	swi	r5, r3 6
	swi	r2, r3 7
	swi	r6, r3 8
	swi	r31 r3 9
	addi	r3, r3, 10
	jal	min_caml_float_of_int
	addi	r3, r3, -10
	lwi	r31 r3 9
	flwl	%f2 l.11805
	fmul	f1, f1, f2
	flwl	%f3 l.12859
	fsub	f1, f1, f3
	addi	r1, r0 4
	lwi	r2, r3 7
	lwi	r5, r3 6
	lwi	r28, r3 8
	swi	r1, r3 9
	fswi	f3, r3 10
	fswi	f2, r3 12
	swi	r31 r3 13
	addi	r3, r3, 14
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -14
	lwi	r31, r3 13
	lwi	r1, r3 4
	lwi	r2, r3 5
	sub	r2, r2, r1
	lwi	r5, r3 3
	lwi	r6, r3 7
	add	r6, r6, r5
	lwi	r7, r3 2
	sgt	r30, r7, r6
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22255
	sub	r6, r6, r7
	jl	ble_cont.22256
ble_else.22255:
ble_cont.22256:
	lwi	r8, r3 6
	addi	r8, r8, 4
	lwi	r9, r3 1
	sgt	r30, r9, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22257
	swi	r2, r3 13
	swi	r8, r3 14
	swi	r6, r3 15
	addi	r1, r2 0 #
	swi	r31 r3 16
	addi	r3, r3, 17
	jal	min_caml_float_of_int
	addi	r3, r3, -17
	lwi	r31 r3 16
	flwi	f2, r3 12 #off
	fmul	f1, f1, f2
	flwi	f2, r3 10 #off
	fsub	f1, f1, f2
	lwi	r1, r3 9
	lwi	r2, r3 15
	lwi	r5, r3 14
	lwi	r28, r3 8
	swi	r31 r3 16
	addi	r3, r3, 17
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -17
	lwi	r31, r3 16
	lwi	r1, r3 4
	lwi	r2, r3 13
	sub	r1, r2, r1
	lwi	r2, r3 3
	lwi	r5, r3 15
	add	r2, r5, r2
	lwi	r5, r3 2
	sgt	r30, r5, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22258
	sub	r2, r2, r5
	jl	ble_cont.22259
ble_else.22258:
ble_cont.22259:
	lwi	r5, r3 14
	addi	r5, r5, 4
	lwi	r28, r3 0
	lwi	r29, r28 0
	jr	r29
ble_else.22257:
	jr	r31
ble_else.22253:
	jr	r31
create_dirvec_elements.3082:
	lwi	r5, r28 5
	lwi	r6, r28 4
	lwi	r7, r28 3
	lwi	r8, r28 2
	flwi	f1, r28 1 #
	sgt	r30, r7, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22262
	swi	r28, r3 0
	fswi	f1, r3 2
	swi	r6, r3 3
	swi	r8, r3 4
	swi	r1, r3 5
	swi	r2, r3 6
	swi	r5, r3 7
	swi	r7, r3 8
	addi	r1, r6 0 #
	swi	r31 r3 9
	addi	r3, r3, 10
	jal	min_caml_create_float_array
	addi	r3, r3, -10
	lwi	r31 r3 9
	addi	r2, r1 0 ##
	lwi	r1, r3 8
	sli	r5, r1, 0
	lwi	r6, r3 7
	add	r5 r6 r5
	lwi	r5, r5 0
	swi	r2, r3 9
	addi	r1, r5 0 #
	swi	r31 r3 10
	addi	r3, r3, 11
	jal	min_caml_create_array
	addi	r3, r3, -11
	lwi	r31 r3 10
	addi	r2, r4 0
	addi	r4, r4, 2
	swi	r1, r2 1
	lwi	r1, r3 9
	swi	r1, r2 0
	addi	r1, r2 0
	lwi	r2, r3 6
	sli	r5, r2, 0
	lwi	r6, r3 5
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 4
	sub	r2, r2, r1
	lwi	r5, r3 8
	sgt	r30, r5, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22264
	flwi	f1, r3 2 #off
	lwi	r7, r3 3
	swi	r2, r3 10
	addi	r1, r7 0 #
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	min_caml_create_float_array
	addi	r3, r3, -12
	lwi	r31 r3 11
	addi	r2, r1 0 ##
	lwi	r1, r3 8
	sli	r5, r1, 0
	lwi	r6, r3 7
	add	r5 r6 r5
	lwi	r5, r5 0
	swi	r2, r3 11
	addi	r1, r5 0 #
	swi	r31 r3 12
	addi	r3, r3, 13
	jal	min_caml_create_array
	addi	r3, r3, -13
	lwi	r31 r3 12
	addi	r2, r4 0
	addi	r4, r4, 2
	swi	r1, r2 1
	lwi	r1, r3 11
	swi	r1, r2 0
	addi	r1, r2 0
	lwi	r2, r3 10
	sli	r5, r2, 0
	lwi	r6, r3 5
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 4
	sub	r2, r2, r1
	lwi	r5, r3 8
	sgt	r30, r5, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22265
	flwi	f1, r3 2 #off
	lwi	r7, r3 3
	swi	r2, r3 12
	addi	r1, r7 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	jal	min_caml_create_float_array
	addi	r3, r3, -14
	lwi	r31 r3 13
	addi	r2, r1 0 ##
	lwi	r1, r3 8
	sli	r5, r1, 0
	lwi	r6, r3 7
	add	r5 r6 r5
	lwi	r5, r5 0
	swi	r2, r3 13
	addi	r1, r5 0 #
	swi	r31 r3 14
	addi	r3, r3, 15
	jal	min_caml_create_array
	addi	r3, r3, -15
	lwi	r31 r3 14
	addi	r2, r4 0
	addi	r4, r4, 2
	swi	r1, r2 1
	lwi	r1, r3 13
	swi	r1, r2 0
	addi	r1, r2 0
	lwi	r2, r3 12
	sli	r5, r2, 0
	lwi	r6, r3 5
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 4
	sub	r2, r2, r1
	lwi	r5, r3 8
	sgt	r30, r5, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22266
	flwi	f1, r3 2 #off
	lwi	r7, r3 3
	swi	r2, r3 14
	addi	r1, r7 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	min_caml_create_float_array
	addi	r3, r3, -16
	lwi	r31 r3 15
	addi	r2, r1 0 ##
	lwi	r1, r3 8
	sli	r1, r1, 0
	lwi	r5, r3 7
	add	r1 r5 r1
	lwi	r1, r1 0
	swi	r2, r3 15
	swi	r31 r3 16
	addi	r3, r3, 17
	jal	min_caml_create_array
	addi	r3, r3, -17
	lwi	r31 r3 16
	addi	r2, r4 0
	addi	r4, r4, 2
	swi	r1, r2 1
	lwi	r1, r3 15
	swi	r1, r2 0
	addi	r1, r2 0
	lwi	r2, r3 14
	sli	r5, r2, 0
	lwi	r6, r3 5
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 4
	sub	r2, r2, r1
	lwi	r28, r3 0
	addi	r1, r6 0 #
	lwi	r29, r28 0
	jr	r29
ble_else.22266:
	jr	r31
ble_else.22265:
	jr	r31
ble_else.22264:
	jr	r31
ble_else.22262:
	jr	r31
create_dirvecs.3085:
	lwi	r2, r28 7
	lwi	r5, r28 6
	lwi	r6, r28 5
	lwi	r7, r28 4
	lwi	r8, r28 3
	lwi	r9, r28 2
	flwi	f1, r28 1 #
	sgt	r30, r8, r1
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22271
	addi	r10, r0 120
	swi	r28, r3 0
	swi	r9, r3 1
	swi	r6, r3 2
	fswi	f1, r3 4
	swi	r7, r3 5
	swi	r5, r3 6
	swi	r1, r3 7
	swi	r10, r3 8
	swi	r2, r3 9
	swi	r8, r3 10
	addi	r1, r7 0 #
	swi	r31 r3 11
	addi	r3, r3, 12
	jal	min_caml_create_float_array
	addi	r3, r3, -12
	lwi	r31 r3 11
	addi	r2, r1 0 ##
	lwi	r1, r3 10
	sli	r5, r1, 0
	lwi	r6, r3 9
	add	r5 r6 r5
	lwi	r5, r5 0
	swi	r2, r3 11
	addi	r1, r5 0 #
	swi	r31 r3 12
	addi	r3, r3, 13
	jal	min_caml_create_array
	addi	r3, r3, -13
	lwi	r31 r3 12
	addi	r2, r4 0
	addi	r4, r4, 2
	swi	r1, r2 1
	lwi	r1, r3 11
	swi	r1, r2 0
	lwi	r1, r3 8
	swi	r31 r3 12
	addi	r3, r3, 13
	jal	min_caml_create_array
	addi	r3, r3, -13
	lwi	r31 r3 12
	lwi	r2, r3 7
	sli	r5, r2, 0
	lwi	r6, r3 6
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	sli	r1, r2, 0
	add	r1 r6 r1
	lwi	r1, r1 0
	flwi	f1, r3 4 #off
	lwi	r5, r3 5
	swi	r1, r3 12
	addi	r1, r5 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	jal	min_caml_create_float_array
	addi	r3, r3, -14
	lwi	r31 r3 13
	addi	r2, r1 0 ##
	lwi	r1, r3 10
	sli	r5, r1, 0
	lwi	r6, r3 9
	add	r5 r6 r5
	lwi	r5, r5 0
	swi	r2, r3 13
	addi	r1, r5 0 #
	swi	r31 r3 14
	addi	r3, r3, 15
	jal	min_caml_create_array
	addi	r3, r3, -15
	lwi	r31 r3 14
	addi	r2, r4 0
	addi	r4, r4, 2
	swi	r1, r2 1
	lwi	r1, r3 13
	swi	r1, r2 0
	addi	r1, r2 0
	lwi	r2, r3 12
	swi	r1, r2 118
	flwi	f1, r3 4 #off
	lwi	r1, r3 5
	swi	r31 r3 14
	addi	r3, r3, 15
	jal	min_caml_create_float_array
	addi	r3, r3, -15
	lwi	r31 r3 14
	addi	r2, r1 0 ##
	lwi	r1, r3 10
	sli	r5, r1, 0
	lwi	r6, r3 9
	add	r5 r6 r5
	lwi	r5, r5 0
	swi	r2, r3 14
	addi	r1, r5 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	min_caml_create_array
	addi	r3, r3, -16
	lwi	r31 r3 15
	addi	r2, r4 0
	addi	r4, r4, 2
	swi	r1, r2 1
	lwi	r1, r3 14
	swi	r1, r2 0
	addi	r1, r2 0
	lwi	r2, r3 12
	swi	r1, r2 117
	addi	r1, r0 116
	flwi	f1, r3 4 #off
	lwi	r5, r3 5
	swi	r1, r3 15
	addi	r1, r5 0 #
	swi	r31 r3 16
	addi	r3, r3, 17
	jal	min_caml_create_float_array
	addi	r3, r3, -17
	lwi	r31 r3 16
	addi	r2, r1 0 ##
	lwi	r1, r3 10
	sli	r5, r1, 0
	lwi	r6, r3 9
	add	r5 r6 r5
	lwi	r5, r5 0
	swi	r2, r3 16
	addi	r1, r5 0 #
	swi	r31 r3 17
	addi	r3, r3, 18
	jal	min_caml_create_array
	addi	r3, r3, -18
	lwi	r31 r3 17
	addi	r2, r4 0
	addi	r4, r4, 2
	swi	r1, r2 1
	lwi	r1, r3 16
	swi	r1, r2 0
	addi	r1, r2 0
	lwi	r2, r3 12
	swi	r1, r2 116
	addi	r1, r0 115
	lwi	r28, r3 2
	addi	r29, r2 0 #
	addi	r2, r1 0 #
	addi	r1, r29 0 #
	swi	r31 r3 17
	addi	r3, r3, 18
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -18
	lwi	r31, r3 17
	lwi	r1, r3 1
	lwi	r2, r3 7
	sub	r2, r2, r1
	lwi	r5, r3 10
	sgt	r30, r5, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22273
	flwi	f1, r3 4 #off
	lwi	r6, r3 5
	swi	r2, r3 17
	addi	r1, r6 0 #
	swi	r31 r3 18
	addi	r3, r3, 19
	jal	min_caml_create_float_array
	addi	r3, r3, -19
	lwi	r31 r3 18
	addi	r2, r1 0 ##
	lwi	r1, r3 10
	sli	r5, r1, 0
	lwi	r6, r3 9
	add	r5 r6 r5
	lwi	r5, r5 0
	swi	r2, r3 18
	addi	r1, r5 0 #
	swi	r31 r3 19
	addi	r3, r3, 20
	jal	min_caml_create_array
	addi	r3, r3, -20
	lwi	r31 r3 19
	addi	r2, r4 0
	addi	r4, r4, 2
	swi	r1, r2 1
	lwi	r1, r3 18
	swi	r1, r2 0
	lwi	r1, r3 8
	swi	r31 r3 19
	addi	r3, r3, 20
	jal	min_caml_create_array
	addi	r3, r3, -20
	lwi	r31 r3 19
	lwi	r2, r3 17
	sli	r5, r2, 0
	lwi	r6, r3 6
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	sli	r1, r2, 0
	add	r1 r6 r1
	lwi	r1, r1 0
	flwi	f1, r3 4 #off
	lwi	r5, r3 5
	swi	r1, r3 19
	addi	r1, r5 0 #
	swi	r31 r3 20
	addi	r3, r3, 21
	jal	min_caml_create_float_array
	addi	r3, r3, -21
	lwi	r31 r3 20
	addi	r2, r1 0 ##
	lwi	r1, r3 10
	sli	r5, r1, 0
	lwi	r6, r3 9
	add	r5 r6 r5
	lwi	r5, r5 0
	swi	r2, r3 20
	addi	r1, r5 0 #
	swi	r31 r3 21
	addi	r3, r3, 22
	jal	min_caml_create_array
	addi	r3, r3, -22
	lwi	r31 r3 21
	addi	r2, r4 0
	addi	r4, r4, 2
	swi	r1, r2 1
	lwi	r1, r3 20
	swi	r1, r2 0
	addi	r1, r2 0
	lwi	r2, r3 19
	swi	r1, r2 118
	flwi	f1, r3 4 #off
	lwi	r1, r3 5
	swi	r31 r3 21
	addi	r3, r3, 22
	jal	min_caml_create_float_array
	addi	r3, r3, -22
	lwi	r31 r3 21
	addi	r2, r1 0 ##
	lwi	r1, r3 10
	sli	r1, r1, 0
	lwi	r5, r3 9
	add	r1 r5 r1
	lwi	r1, r1 0
	swi	r2, r3 21
	swi	r31 r3 22
	addi	r3, r3, 23
	jal	min_caml_create_array
	addi	r3, r3, -23
	lwi	r31 r3 22
	addi	r2, r4 0
	addi	r4, r4, 2
	swi	r1, r2 1
	lwi	r1, r3 21
	swi	r1, r2 0
	addi	r1, r2 0
	lwi	r2, r3 19
	swi	r1, r2 117
	lwi	r1, r3 15
	lwi	r28, r3 2
	addi	r29, r2 0 #
	addi	r2, r1 0 #
	addi	r1, r29 0 #
	swi	r31 r3 22
	addi	r3, r3, 23
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -23
	lwi	r31, r3 22
	lwi	r1, r3 1
	lwi	r2, r3 17
	sub	r1, r2, r1
	lwi	r28, r3 0
	lwi	r29, r28 0
	jr	r29
ble_else.22273:
	jr	r31
ble_else.22271:
	jr	r31
init_dirvec_constants.3087:
	lwi	r5, r28 9
	lwi	r6, r28 8
	lwi	r7, r28 7
	lwi	r8, r28 6
	lwi	r9, r28 5
	lwi	r10, r28 4
	lwi	r11, r28 3
	lwi	r12, r28 2
	lwi	r13, r28 1
	sgt	r30, r12, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22276
	sli	r14, r2, 0
	add	r14 r1 r14
	lwi	r14, r14 0
	sli	r15, r12, 0
	add	r15 r9 r15
	lwi	r15, r15 0
	sub	r15, r15, r13
	swi	r28, r3 0
	swi	r6, r3 1
	swi	r5, r3 2
	swi	r11, r3 3
	swi	r7, r3 4
	swi	r8, r3 5
	swi	r10, r3 6
	swi	r9, r3 7
	swi	r1, r3 8
	swi	r12, r3 9
	swi	r13, r3 10
	swi	r2, r3 11
	sgt	r30, r12, r15
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22277
	sli	r16, r15, 0
	add	r16 r8 r16
	lwi	r16, r16 0
	lwi	r17, r14 1
	lwi	r18, r14 0
	lwi	r19, r16 1
	swi	r14, r3 12
	seq	r30, r19, r13
	jeql	r30, r0, beq_else.22279
	swi	r17, r3 13
	swi	r15, r3 14
	addi	r2, r16 0 #
	addi	r1, r18 0 #
	addi	r28, r7 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -16
	lwi	r31, r3 15
	lwi	r2, r3 14
	sli	r5, r2, 0
	lwi	r6, r3 13
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.22280
beq_else.22279:
	seq	r30, r19, r11
	jeql	r30, r0, beq_else.22281
	swi	r17, r3 13
	swi	r15, r3 14
	addi	r2, r16 0 #
	addi	r1, r18 0 #
	addi	r28, r5 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -16
	lwi	r31, r3 15
	lwi	r2, r3 14
	sli	r5, r2, 0
	lwi	r6, r3 13
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.22282
beq_else.22281:
	swi	r17, r3 13
	swi	r15, r3 14
	addi	r2, r16 0 #
	addi	r1, r18 0 #
	addi	r28, r6 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -16
	lwi	r31, r3 15
	lwi	r2, r3 14
	sli	r5, r2, 0
	lwi	r6, r3 13
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
beq_cont.22282:
beq_cont.22280:
	lwi	r1, r3 10
	sub	r2, r2, r1
	lwi	r5, r3 12
	lwi	r28, r3 6
	addi	r1, r5 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -16
	lwi	r31, r3 15
	jl	ble_cont.22278
ble_else.22277:
ble_cont.22278:
	lwi	r1, r3 10
	lwi	r2, r3 11
	sub	r2, r2, r1
	lwi	r5, r3 9
	sgt	r30, r5, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22283
	sli	r6, r2, 0
	lwi	r7, r3 8
	add	r6 r7 r6
	lwi	r6, r6 0
	sli	r8, r5, 0
	lwi	r9, r3 7
	add	r8 r9 r8
	lwi	r8, r8 0
	sub	r8, r8, r1
	lwi	r28, r3 6
	swi	r2, r3 15
	addi	r2, r8 0 #
	addi	r1, r6 0 #
	swi	r31 r3 16
	addi	r3, r3, 17
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -17
	lwi	r31, r3 16
	lwi	r1, r3 10
	lwi	r2, r3 15
	sub	r2, r2, r1
	lwi	r5, r3 9
	sgt	r30, r5, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22284
	sli	r6, r2, 0
	lwi	r7, r3 8
	add	r6 r7 r6
	lwi	r6, r6 0
	sli	r8, r5, 0
	lwi	r9, r3 7
	add	r8 r9 r8
	lwi	r8, r8 0
	sub	r8, r8, r1
	lwi	r28, r3 6
	swi	r2, r3 16
	addi	r2, r8 0 #
	addi	r1, r6 0 #
	swi	r31 r3 17
	addi	r3, r3, 18
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -18
	lwi	r31, r3 17
	lwi	r1, r3 10
	lwi	r2, r3 16
	sub	r2, r2, r1
	lwi	r5, r3 9
	sgt	r30, r5, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22285
	sli	r6, r2, 0
	lwi	r7, r3 8
	add	r6 r7 r6
	lwi	r6, r6 0
	sli	r8, r5, 0
	lwi	r9, r3 7
	add	r8 r9 r8
	lwi	r8, r8 0
	sub	r8, r8, r1
	swi	r2, r3 17
	sgt	r30, r5, r8
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22286
	sli	r5, r8, 0
	lwi	r9, r3 5
	add	r5 r9 r5
	lwi	r5, r5 0
	lwi	r9, r6 1
	lwi	r10, r6 0
	lwi	r11, r5 1
	swi	r6, r3 18
	seq	r30, r11, r1
	jeql	r30, r0, beq_else.22288
	lwi	r28, r3 4
	swi	r9, r3 19
	swi	r8, r3 20
	addi	r2, r5 0 #
	addi	r1, r10 0 #
	swi	r31 r3 21
	addi	r3, r3, 22
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -22
	lwi	r31, r3 21
	lwi	r2, r3 20
	sli	r5, r2, 0
	lwi	r6, r3 19
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.22289
beq_else.22288:
	lwi	r12, r3 3
	seq	r30, r11, r12
	jeql	r30, r0, beq_else.22290
	lwi	r28, r3 2
	swi	r9, r3 19
	swi	r8, r3 20
	addi	r2, r5 0 #
	addi	r1, r10 0 #
	swi	r31 r3 21
	addi	r3, r3, 22
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -22
	lwi	r31, r3 21
	lwi	r2, r3 20
	sli	r5, r2, 0
	lwi	r6, r3 19
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.22291
beq_else.22290:
	lwi	r28, r3 1
	swi	r9, r3 19
	swi	r8, r3 20
	addi	r2, r5 0 #
	addi	r1, r10 0 #
	swi	r31 r3 21
	addi	r3, r3, 22
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -22
	lwi	r31, r3 21
	lwi	r2, r3 20
	sli	r5, r2, 0
	lwi	r6, r3 19
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
beq_cont.22291:
beq_cont.22289:
	lwi	r1, r3 10
	sub	r2, r2, r1
	lwi	r5, r3 18
	lwi	r28, r3 6
	addi	r1, r5 0 #
	swi	r31 r3 21
	addi	r3, r3, 22
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -22
	lwi	r31, r3 21
	jl	ble_cont.22287
ble_else.22286:
ble_cont.22287:
	lwi	r1, r3 10
	lwi	r2, r3 17
	sub	r2, r2, r1
	lwi	r1, r3 8
	lwi	r28, r3 0
	lwi	r29, r28 0
	jr	r29
ble_else.22285:
	jr	r31
ble_else.22284:
	jr	r31
ble_else.22283:
	jr	r31
ble_else.22276:
	jr	r31
init_vecset_constants.3090:
	lwi	r2, r28 11
	lwi	r5, r28 10
	lwi	r6, r28 9
	lwi	r7, r28 8
	lwi	r8, r28 7
	lwi	r9, r28 6
	lwi	r10, r28 5
	lwi	r11, r28 4
	lwi	r12, r28 3
	lwi	r13, r28 2
	lwi	r14, r28 1
	sgt	r30, r13, r1
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22296
	sli	r15, r1, 0
	add	r15 r11 r15
	lwi	r15, r15 0
	addi	r16, r0 119
	lwi	r17, r15 119
	sli	r18, r13, 0
	add	r18 r8 r18
	lwi	r18, r18 0
	sub	r18, r18, r14
	swi	r28, r3 0
	swi	r16, r3 1
	swi	r11, r3 2
	swi	r1, r3 3
	swi	r10, r3 4
	swi	r5, r3 5
	swi	r2, r3 6
	swi	r12, r3 7
	swi	r6, r3 8
	swi	r7, r3 9
	swi	r9, r3 10
	swi	r14, r3 11
	swi	r8, r3 12
	swi	r13, r3 13
	swi	r15, r3 14
	addi	r2, r18 0 #
	addi	r1, r17 0 #
	addi	r28, r9 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -16
	lwi	r31, r3 15
	addi	r1, r0 118
	lwi	r2, r3 14
	lwi	r5, r2 118
	lwi	r6, r3 13
	sli	r7, r6, 0
	lwi	r8, r3 12
	add	r7 r8 r7
	lwi	r7, r7 0
	lwi	r9, r3 11
	sub	r7, r7, r9
	lwi	r28, r3 10
	swi	r1, r3 15
	addi	r2, r7 0 #
	addi	r1, r5 0 #
	swi	r31 r3 16
	addi	r3, r3, 17
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -17
	lwi	r31, r3 16
	addi	r1, r0 117
	lwi	r2, r3 14
	lwi	r5, r2 117
	lwi	r6, r3 13
	sli	r7, r6, 0
	lwi	r8, r3 12
	add	r7 r8 r7
	lwi	r7, r7 0
	lwi	r9, r3 11
	sub	r7, r7, r9
	swi	r1, r3 16
	sgt	r30, r6, r7
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22297
	sli	r10, r7, 0
	lwi	r11, r3 9
	add	r10 r11 r10
	lwi	r10, r10 0
	lwi	r12, r5 1
	lwi	r13, r5 0
	lwi	r14, r10 1
	swi	r5, r3 17
	seq	r30, r14, r9
	jeql	r30, r0, beq_else.22299
	lwi	r28, r3 8
	swi	r12, r3 18
	swi	r7, r3 19
	addi	r2, r10 0 #
	addi	r1, r13 0 #
	swi	r31 r3 20
	addi	r3, r3, 21
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -21
	lwi	r31, r3 20
	lwi	r2, r3 19
	sli	r5, r2, 0
	lwi	r6, r3 18
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.22300
beq_else.22299:
	lwi	r15, r3 7
	seq	r30, r14, r15
	jeql	r30, r0, beq_else.22301
	lwi	r28, r3 6
	swi	r12, r3 18
	swi	r7, r3 19
	addi	r2, r10 0 #
	addi	r1, r13 0 #
	swi	r31 r3 20
	addi	r3, r3, 21
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -21
	lwi	r31, r3 20
	lwi	r2, r3 19
	sli	r5, r2, 0
	lwi	r6, r3 18
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.22302
beq_else.22301:
	lwi	r28, r3 5
	swi	r12, r3 18
	swi	r7, r3 19
	addi	r2, r10 0 #
	addi	r1, r13 0 #
	swi	r31 r3 20
	addi	r3, r3, 21
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -21
	lwi	r31, r3 20
	lwi	r2, r3 19
	sli	r5, r2, 0
	lwi	r6, r3 18
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
beq_cont.22302:
beq_cont.22300:
	lwi	r1, r3 11
	sub	r2, r2, r1
	lwi	r5, r3 17
	lwi	r28, r3 10
	addi	r1, r5 0 #
	swi	r31 r3 20
	addi	r3, r3, 21
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -21
	lwi	r31, r3 20
	jl	ble_cont.22298
ble_else.22297:
ble_cont.22298:
	addi	r2, r0 116
	lwi	r1, r3 14
	lwi	r28, r3 4
	swi	r31 r3 20
	addi	r3, r3, 21
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -21
	lwi	r31, r3 20
	lwi	r1, r3 11
	lwi	r2, r3 3
	sub	r2, r2, r1
	lwi	r5, r3 13
	sgt	r30, r5, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22303
	sli	r6, r2, 0
	lwi	r7, r3 2
	add	r6 r7 r6
	lwi	r6, r6 0
	lwi	r8, r6 119
	sli	r9, r5, 0
	lwi	r10, r3 12
	add	r9 r10 r9
	lwi	r9, r9 0
	sub	r9, r9, r1
	lwi	r28, r3 10
	swi	r2, r3 20
	swi	r6, r3 21
	addi	r2, r9 0 #
	addi	r1, r8 0 #
	swi	r31 r3 22
	addi	r3, r3, 23
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -23
	lwi	r31, r3 22
	lwi	r1, r3 21
	lwi	r2, r1 118
	lwi	r5, r3 13
	sli	r6, r5, 0
	lwi	r7, r3 12
	add	r6 r7 r6
	lwi	r6, r6 0
	lwi	r8, r3 11
	sub	r6, r6, r8
	sgt	r30, r5, r6
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22304
	sli	r9, r6, 0
	lwi	r10, r3 9
	add	r9 r10 r9
	lwi	r9, r9 0
	lwi	r11, r2 1
	lwi	r12, r2 0
	lwi	r13, r9 1
	swi	r2, r3 22
	seq	r30, r13, r8
	jeql	r30, r0, beq_else.22306
	lwi	r28, r3 8
	swi	r11, r3 23
	swi	r6, r3 24
	addi	r2, r9 0 #
	addi	r1, r12 0 #
	swi	r31 r3 25
	addi	r3, r3, 26
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -26
	lwi	r31, r3 25
	lwi	r2, r3 24
	sli	r5, r2, 0
	lwi	r6, r3 23
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.22307
beq_else.22306:
	lwi	r14, r3 7
	seq	r30, r13, r14
	jeql	r30, r0, beq_else.22308
	lwi	r28, r3 6
	swi	r11, r3 23
	swi	r6, r3 24
	addi	r2, r9 0 #
	addi	r1, r12 0 #
	swi	r31 r3 25
	addi	r3, r3, 26
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -26
	lwi	r31, r3 25
	lwi	r2, r3 24
	sli	r5, r2, 0
	lwi	r6, r3 23
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.22309
beq_else.22308:
	lwi	r28, r3 5
	swi	r11, r3 23
	swi	r6, r3 24
	addi	r2, r9 0 #
	addi	r1, r12 0 #
	swi	r31 r3 25
	addi	r3, r3, 26
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -26
	lwi	r31, r3 25
	lwi	r2, r3 24
	sli	r5, r2, 0
	lwi	r6, r3 23
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
beq_cont.22309:
beq_cont.22307:
	lwi	r1, r3 11
	sub	r2, r2, r1
	lwi	r5, r3 22
	lwi	r28, r3 10
	addi	r1, r5 0 #
	swi	r31 r3 25
	addi	r3, r3, 26
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -26
	lwi	r31, r3 25
	jl	ble_cont.22305
ble_else.22304:
ble_cont.22305:
	lwi	r1, r3 21
	lwi	r2, r3 16
	lwi	r28, r3 4
	swi	r31 r3 25
	addi	r3, r3, 26
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -26
	lwi	r31, r3 25
	lwi	r1, r3 11
	lwi	r2, r3 20
	sub	r2, r2, r1
	lwi	r5, r3 13
	sgt	r30, r5, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22310
	sli	r6, r2, 0
	lwi	r7, r3 2
	add	r6 r7 r6
	lwi	r6, r6 0
	lwi	r8, r6 119
	sli	r9, r5, 0
	lwi	r10, r3 12
	add	r9 r10 r9
	lwi	r9, r9 0
	sub	r9, r9, r1
	swi	r2, r3 25
	swi	r6, r3 26
	sgt	r30, r5, r9
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22311
	sli	r10, r9, 0
	lwi	r11, r3 9
	add	r10 r11 r10
	lwi	r10, r10 0
	lwi	r11, r8 1
	lwi	r12, r8 0
	lwi	r13, r10 1
	swi	r8, r3 27
	seq	r30, r13, r1
	jeql	r30, r0, beq_else.22313
	lwi	r28, r3 8
	swi	r11, r3 28
	swi	r9, r3 29
	addi	r2, r10 0 #
	addi	r1, r12 0 #
	swi	r31 r3 30
	addi	r3, r3, 31
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -31
	lwi	r31, r3 30
	lwi	r2, r3 29
	sli	r5, r2, 0
	lwi	r6, r3 28
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.22314
beq_else.22313:
	lwi	r14, r3 7
	seq	r30, r13, r14
	jeql	r30, r0, beq_else.22315
	lwi	r28, r3 6
	swi	r11, r3 28
	swi	r9, r3 29
	addi	r2, r10 0 #
	addi	r1, r12 0 #
	swi	r31 r3 30
	addi	r3, r3, 31
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -31
	lwi	r31, r3 30
	lwi	r2, r3 29
	sli	r5, r2, 0
	lwi	r6, r3 28
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.22316
beq_else.22315:
	lwi	r28, r3 5
	swi	r11, r3 28
	swi	r9, r3 29
	addi	r2, r10 0 #
	addi	r1, r12 0 #
	swi	r31 r3 30
	addi	r3, r3, 31
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -31
	lwi	r31, r3 30
	lwi	r2, r3 29
	sli	r5, r2, 0
	lwi	r6, r3 28
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
beq_cont.22316:
beq_cont.22314:
	lwi	r1, r3 11
	sub	r2, r2, r1
	lwi	r5, r3 27
	lwi	r28, r3 10
	addi	r1, r5 0 #
	swi	r31 r3 30
	addi	r3, r3, 31
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -31
	lwi	r31, r3 30
	jl	ble_cont.22312
ble_else.22311:
ble_cont.22312:
	lwi	r1, r3 26
	lwi	r2, r3 15
	lwi	r28, r3 4
	swi	r31 r3 30
	addi	r3, r3, 31
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -31
	lwi	r31, r3 30
	lwi	r1, r3 11
	lwi	r2, r3 25
	sub	r2, r2, r1
	lwi	r5, r3 13
	sgt	r30, r5, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22317
	sli	r5, r2, 0
	lwi	r6, r3 2
	add	r5 r6 r5
	lwi	r5, r5 0
	lwi	r6, r3 1
	lwi	r28, r3 4
	swi	r2, r3 30
	addi	r2, r6 0 #
	addi	r1, r5 0 #
	swi	r31 r3 31
	addi	r3, r3, 32
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -32
	lwi	r31, r3 31
	lwi	r1, r3 11
	lwi	r2, r3 30
	sub	r1, r2, r1
	lwi	r28, r3 0
	lwi	r29, r28 0
	jr	r29
ble_else.22317:
	jr	r31
ble_else.22310:
	jr	r31
ble_else.22303:
	jr	r31
ble_else.22296:
	jr	r31
setup_rect_reflection.3101:
	lwi	r5, r28 10
	lwi	r6, r28 9
	lwi	r7, r28 8
	lwi	r8, r28 7
	lwi	r9, r28 6
	lwi	r10, r28 5
	lwi	r11, r28 4
	lwi	r12, r28 3
	lwi	r13, r28 2
	flwi	f1, r28 1 #
	addi	r14, r0 4
	mul	r1, r1, r14
	sli	r14, r12, 0
	add	r14 r6 r14
	lwi	r14, r14 0
	flwl	%f2 l.11794
	lwi	r2, r2 7
	flwi	f3, r2 0 #
	fsub	f2, f2, f3
	sli	r2, r12, 0
	add	r0 r8 r2
	flwi	f3, r0 0
	sub	r0 r0 r0
	fsub	f3, f1, f3
	sli	r2, r13, 0
	add	r0 r8 r2
	flwi	f4, r0 0
	sub	r0 r0 r0
	fsub	f4, f1, f4
	sli	r2, r10, 0
	add	r0 r8 r2
	flwi	f5, r0 0
	sub	r0 r0 r0
	fsub	f5, f1, f5
	add	r2, r1, r13
	sli	r15, r12, 0
	add	r0 r8 r15
	flwi	f6, r0 0
	sub	r0 r0 r0
	swi	r6, r3 0
	fswi	f3, r3 2
	fswi	f1, r3 4
	swi	r11, r3 5
	swi	r8, r3 6
	swi	r1, r3 7
	swi	r5, r3 8
	swi	r14, r3 9
	swi	r2, r3 10
	fswi	f2, r3 12
	swi	r9, r3 13
	fswi	f5, r3 14
	swi	r10, r3 15
	fswi	f4, r3 16
	swi	r13, r3 17
	fswi	f6, r3 18
	swi	r7, r3 19
	swi	r12, r3 20
	addi	r1, r11 0 #
	swi	r31 r3 21
	addi	r3, r3, 22
	jal	min_caml_create_float_array
	addi	r3, r3, -22
	lwi	r31 r3 21
	addi	r2, r1 0 ##
	lwi	r1, r3 20
	sli	r5, r1, 0
	lwi	r6, r3 19
	add	r5 r6 r5
	lwi	r5, r5 0
	swi	r2, r3 21
	addi	r1, r5 0 #
	swi	r31 r3 22
	addi	r3, r3, 23
	jal	min_caml_create_array
	addi	r3, r3, -23
	lwi	r31 r3 22
	addi	r2, r4 0
	addi	r4, r4, 2
	swi	r1, r2 1
	lwi	r1, r3 21
	swi	r1, r2 0
	lwi	r5, r3 20
	sli	r6, r5, 0
	flwi	f1, r3 18 #off
	add	r0 r1 r6
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r6, r3 17
	sli	r7, r6, 0
	flwi	f1, r3 16 #off
	add	r0 r1 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r7, r3 15
	sli	r8, r7, 0
	flwi	f2, r3 14 #off
	add	r0 r1 r8
	fswi	f2, r0 0
	sub	r0 r0 r0
	sli	r1, r5, 0
	lwi	r8, r3 19
	add	r1 r8 r1
	lwi	r1, r1 0
	sub	r1, r1, r6
	lwi	r28, r3 13
	swi	r2, r3 22
	addi	r29, r2 0 #
	addi	r2, r1 0 #
	addi	r1, r29 0 #
	swi	r31 r3 23
	addi	r3, r3, 24
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -24
	lwi	r31, r3 23
	addi	r1, r4 0
	addi	r4, r4, 3
	flwi	f1, r3 12 #off
	fswi	f1, r1 2
	lwi	r2, r3 22
	swi	r2, r1 1
	lwi	r2, r3 10
	swi	r2, r1 0
	lwi	r2, r3 9
	sli	r5, r2, 0
	lwi	r6, r3 8
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 17
	add	r5, r2, r1
	lwi	r7, r3 15
	lwi	r8, r3 7
	add	r9, r8, r7
	sli	r10, r1, 0
	lwi	r11, r3 6
	add	r0 r11 r10
	flwi	f2, r0 0
	sub	r0 r0 r0
	flwi	f3, r3 4 #off
	lwi	r10, r3 5
	swi	r5, r3 23
	swi	r9, r3 24
	fswi	f2, r3 26
	addi	r1, r10 0 #
	fadd	f1, f3 f0
	swi	r31 r3 27
	addi	r3, r3, 28
	jal	min_caml_create_float_array
	addi	r3, r3, -28
	lwi	r31 r3 27
	addi	r2, r1 0 ##
	lwi	r1, r3 20
	sli	r5, r1, 0
	lwi	r6, r3 19
	add	r5 r6 r5
	lwi	r5, r5 0
	swi	r2, r3 27
	addi	r1, r5 0 #
	swi	r31 r3 28
	addi	r3, r3, 29
	jal	min_caml_create_array
	addi	r3, r3, -29
	lwi	r31 r3 28
	addi	r2, r4 0
	addi	r4, r4, 2
	swi	r1, r2 1
	lwi	r1, r3 27
	swi	r1, r2 0
	lwi	r5, r3 20
	sli	r6, r5, 0
	flwi	f1, r3 2 #off
	add	r0 r1 r6
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r6, r3 17
	sli	r7, r6, 0
	flwi	f2, r3 26 #off
	add	r0 r1 r7
	fswi	f2, r0 0
	sub	r0 r0 r0
	lwi	r7, r3 15
	sli	r8, r7, 0
	flwi	f2, r3 14 #off
	add	r0 r1 r8
	fswi	f2, r0 0
	sub	r0 r0 r0
	sli	r1, r5, 0
	lwi	r8, r3 19
	add	r1 r8 r1
	lwi	r1, r1 0
	sub	r1, r1, r6
	lwi	r28, r3 13
	swi	r2, r3 28
	addi	r29, r2 0 #
	addi	r2, r1 0 #
	addi	r1, r29 0 #
	swi	r31 r3 29
	addi	r3, r3, 30
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -30
	lwi	r31, r3 29
	addi	r1, r4 0
	addi	r4, r4, 3
	flwi	f1, r3 12 #off
	fswi	f1, r1 2
	lwi	r2, r3 28
	swi	r2, r1 1
	lwi	r2, r3 24
	swi	r2, r1 0
	lwi	r2, r3 23
	sli	r2, r2, 0
	lwi	r5, r3 8
	add	r0 r5 r2
	swi	r1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 15
	lwi	r2, r3 9
	add	r6, r2, r1
	lwi	r7, r3 5
	lwi	r8, r3 7
	add	r8, r8, r7
	sli	r9, r1, 0
	lwi	r10, r3 6
	add	r0 r10 r9
	flwi	f2, r0 0
	sub	r0 r0 r0
	flwi	f3, r3 4 #off
	swi	r6, r3 29
	swi	r8, r3 30
	fswi	f2, r3 32
	addi	r1, r7 0 #
	fadd	f1, f3 f0
	swi	r31 r3 33
	addi	r3, r3, 34
	jal	min_caml_create_float_array
	addi	r3, r3, -34
	lwi	r31 r3 33
	addi	r2, r1 0 ##
	lwi	r1, r3 20
	sli	r5, r1, 0
	lwi	r6, r3 19
	add	r5 r6 r5
	lwi	r5, r5 0
	swi	r2, r3 33
	addi	r1, r5 0 #
	swi	r31 r3 34
	addi	r3, r3, 35
	jal	min_caml_create_array
	addi	r3, r3, -35
	lwi	r31 r3 34
	addi	r2, r4 0
	addi	r4, r4, 2
	swi	r1, r2 1
	lwi	r1, r3 33
	swi	r1, r2 0
	lwi	r5, r3 20
	sli	r6, r5, 0
	flwi	f1, r3 2 #off
	add	r0 r1 r6
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r6, r3 17
	sli	r7, r6, 0
	flwi	f1, r3 16 #off
	add	r0 r1 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r7, r3 15
	sli	r7, r7, 0
	flwi	f1, r3 32 #off
	add	r0 r1 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r1, r5, 0
	lwi	r7, r3 19
	add	r1 r7 r1
	lwi	r1, r1 0
	sub	r1, r1, r6
	lwi	r28, r3 13
	swi	r2, r3 34
	addi	r29, r2 0 #
	addi	r2, r1 0 #
	addi	r1, r29 0 #
	swi	r31 r3 35
	addi	r3, r3, 36
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -36
	lwi	r31, r3 35
	addi	r1, r4 0
	addi	r4, r4, 3
	flwi	f1, r3 12 #off
	fswi	f1, r1 2
	lwi	r2, r3 34
	swi	r2, r1 1
	lwi	r2, r3 30
	swi	r2, r1 0
	lwi	r2, r3 29
	sli	r2, r2, 0
	lwi	r5, r3 8
	add	r0 r5 r2
	swi	r1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 5
	lwi	r2, r3 9
	add	r1, r2, r1
	lwi	r2, r3 20
	sli	r2, r2, 0
	lwi	r5, r3 0
	add	r0 r5 r2
	swi	r1, r0 0
	sub	r0 r0 r0
	jr	r31
setup_surface_reflection.3104:
	lwi	r5, r28 10
	lwi	r6, r28 9
	lwi	r7, r28 8
	lwi	r8, r28 7
	lwi	r9, r28 6
	lwi	r10, r28 5
	lwi	r11, r28 4
	lwi	r12, r28 3
	lwi	r13, r28 2
	flwi	f1, r28 1 #
	addi	r14, r0 4
	mul	r1, r1, r14
	add	r1, r1, r13
	sli	r14, r12, 0
	add	r14 r6 r14
	lwi	r14, r14 0
	flwl	%f2 l.11794
	lwi	r15, r2 7
	flwi	f3, r15 0 #
	fsub	f2, f2, f3
	lwi	r15, r2 4
	sli	r16, r12, 0
	add	r0 r8 r16
	flwi	f3, r0 0
	sub	r0 r0 r0
	sli	r16, r12, 0
	add	r0 r15 r16
	flwi	f4, r0 0
	sub	r0 r0 r0
	fmul	f3, f3, f4
	sli	r16, r13, 0
	add	r0 r8 r16
	flwi	f4, r0 0
	sub	r0 r0 r0
	sli	r16, r13, 0
	add	r0 r15 r16
	flwi	f5, r0 0
	sub	r0 r0 r0
	fmul	f4, f4, f5
	fadd	f3, f3, f4
	sli	r16, r10, 0
	add	r0 r8 r16
	flwi	f4, r0 0
	sub	r0 r0 r0
	sli	r16, r10, 0
	add	r0 r15 r16
	flwi	f5, r0 0
	sub	r0 r0 r0
	fmul	f4, f4, f5
	fadd	f3, f3, f4
	flwl	%f4 l.11778
	lwi	r15, r2 4
	flwi	f5, r15 0 #
	fmul	f5, f4, f5
	fmul	f5, f5, f3
	sli	r15, r12, 0
	add	r0 r8 r15
	flwi	f6, r0 0
	sub	r0 r0 r0
	fsub	f5, f5, f6
	lwi	r15, r2 4
	flwi	f6, r15 1 #
	fmul	f6, f4, f6
	fmul	f6, f6, f3
	sli	r15, r13, 0
	add	r0 r8 r15
	flwi	f7, r0 0
	sub	r0 r0 r0
	fsub	f6, f6, f7
	lwi	r2, r2 4
	flwi	f7, r2 2 #
	fmul	f4, f4, f7
	fmul	f3, f4, f3
	sli	r2, r10, 0
	add	r0 r8 r2
	flwi	f4, r0 0
	sub	r0 r0 r0
	fsub	f3, f3, f4
	swi	r6, r3 0
	swi	r5, r3 1
	swi	r14, r3 2
	swi	r1, r3 3
	fswi	f2, r3 4
	swi	r9, r3 5
	fswi	f3, r3 6
	swi	r10, r3 7
	fswi	f6, r3 8
	swi	r13, r3 9
	fswi	f5, r3 10
	swi	r7, r3 11
	swi	r12, r3 12
	addi	r1, r11 0 #
	swi	r31 r3 13
	addi	r3, r3, 14
	jal	min_caml_create_float_array
	addi	r3, r3, -14
	lwi	r31 r3 13
	addi	r2, r1 0 ##
	lwi	r1, r3 12
	sli	r5, r1, 0
	lwi	r6, r3 11
	add	r5 r6 r5
	lwi	r5, r5 0
	swi	r2, r3 13
	addi	r1, r5 0 #
	swi	r31 r3 14
	addi	r3, r3, 15
	jal	min_caml_create_array
	addi	r3, r3, -15
	lwi	r31 r3 14
	addi	r2, r4 0
	addi	r4, r4, 2
	swi	r1, r2 1
	lwi	r1, r3 13
	swi	r1, r2 0
	lwi	r5, r3 12
	sli	r6, r5, 0
	flwi	f1, r3 10 #off
	add	r0 r1 r6
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r6, r3 9
	sli	r7, r6, 0
	flwi	f1, r3 8 #off
	add	r0 r1 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r7, r3 7
	sli	r7, r7, 0
	flwi	f1, r3 6 #off
	add	r0 r1 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r1, r5, 0
	lwi	r7, r3 11
	add	r1 r7 r1
	lwi	r1, r1 0
	sub	r1, r1, r6
	lwi	r28, r3 5
	swi	r2, r3 14
	addi	r29, r2 0 #
	addi	r2, r1 0 #
	addi	r1, r29 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -16
	lwi	r31, r3 15
	addi	r1, r4 0
	addi	r4, r4, 3
	flwi	f1, r3 4 #off
	fswi	f1, r1 2
	lwi	r2, r3 14
	swi	r2, r1 1
	lwi	r2, r3 3
	swi	r2, r1 0
	lwi	r2, r3 2
	sli	r5, r2, 0
	lwi	r6, r3 1
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 9
	add	r1, r2, r1
	lwi	r2, r3 12
	sli	r2, r2, 0
	lwi	r5, r3 0
	add	r0 r5 r2
	swi	r1, r0 0
	sub	r0 r0 r0
	jr	r31
rt.3109:
	lwi	r5, r28 40
	lwi	r6, r28 39
	lwi	r7, r28 38
	lwi	r8, r28 37
	lwi	r9, r28 36
	lwi	r10, r28 35
	lwi	r11, r28 34
	lwi	r12, r28 33
	lwi	r13, r28 32
	lwi	r14, r28 31
	lwi	r15, r28 30
	lwi	r16, r28 29
	lwi	r17, r28 28
	lwi	r18, r28 27
	lwi	r19, r28 26
	lwi	r20, r28 25
	lwi	r21, r28 24
	lwi	r22, r28 23
	lwi	r23, r28 22
	lwi	r24, r28 21
	lwi	r25, r28 20
	lwi	r26, r28 19
	lwi	r27, r28 18
	swi	r14, r3 0
	lwi	r14, r28 17
	swi	r12, r3 1
	lwi	r12, r28 16
	swi	r21, r3 2
	lwi	r21, r28 15
	swi	r8, r3 3
	lwi	r8, r28 14
	swi	r11, r3 4
	lwi	r11, r28 13
	swi	r25, r3 5
	lwi	r25, r28 12
	swi	r6, r3 6
	lwi	r6, r28 11
	swi	r26, r3 7
	lwi	r26, r28 10
	swi	r14, r3 8
	lwi	r14, r28 9
	swi	r14, r3 9
	lwi	r14, r28 8
	swi	r21, r3 10
	lwi	r21, r28 7
	swi	r27, r3 11
	lwi	r27, r28 6
	swi	r9, r3 12
	lwi	r9, r28 5
	swi	r7, r3 13
	lwi	r7, r28 4
	swi	r10, r3 14
	lwi	r10, r28 3
	swi	r23, r3 15
	lwi	r23, r28 2
	flwi	f1, r28 1 #
	swi	r25, r3 16
	sli	r25, r10, 0
	add	r0 r8 r25
	swi	r1, r0 0
	sub	r0 r0 r0
	sli	r25, r23, 0
	add	r0 r8 r25
	swi	r2, r0 0
	sub	r0 r0 r0
	div	r25, r1, r9
	swi	r21, r3 17
	sli	r21, r10, 0
	add	r0 r11 r21
	swi	r25, r0 0
	sub	r0 r0 r0
	div	r2, r2, r9
	sli	r21, r23, 0
	add	r0 r11 r21
	swi	r2, r0 0
	sub	r0 r0 r0
	flwl	%f2 l.13041
	swi	r14, r3 18
	swi	r26, r3 19
	swi	r5, r3 20
	swi	r22, r3 21
	swi	r16, r3 22
	swi	r20, r3 23
	swi	r17, r3 24
	swi	r24, r3 25
	swi	r18, r3 26
	swi	r19, r3 27
	swi	r15, r3 28
	swi	r12, r3 29
	swi	r6, r3 30
	swi	r9, r3 31
	swi	r23, r3 32
	swi	r27, r3 33
	fswi	f1, r3 34
	swi	r7, r3 35
	swi	r8, r3 36
	swi	r13, r3 37
	swi	r10, r3 38
	fswi	f2, r3 40
	swi	r31 r3 41
	addi	r3, r3, 42
	jal	min_caml_float_of_int
	addi	r3, r3, -42
	lwi	r31 r3 41
	flwi	f2, r3 40 #off
	fdiv	f1, f2, f1
	lwi	r1, r3 38
	sli	r2, r1, 0
	lwi	r5, r3 37
	add	r0 r5 r2
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r2, r1, 0
	lwi	r5, r3 36
	add	r2 r5 r2
	lwi	r2, r2 0
	flwi	f1, r3 34 #off
	lwi	r6, r3 35
	swi	r2, r3 41
	addi	r1, r6 0 #
	swi	r31 r3 42
	addi	r3, r3, 43
	jal	min_caml_create_float_array
	addi	r3, r3, -43
	lwi	r31 r3 42
	flwi	f1, r3 34 #off
	lwi	r2, r3 35
	swi	r1, r3 42
	addi	r1, r2 0 #
	swi	r31 r3 43
	addi	r3, r3, 44
	jal	min_caml_create_float_array
	addi	r3, r3, -44
	lwi	r31 r3 43
	addi	r2, r1 0 ##
	lwi	r1, r3 33
	swi	r31 r3 43
	addi	r3, r3, 44
	jal	min_caml_create_array
	addi	r3, r3, -44
	lwi	r31 r3 43
	flwi	f1, r3 34 #off
	lwi	r2, r3 35
	swi	r1, r3 43
	addi	r1, r2 0 #
	swi	r31 r3 44
	addi	r3, r3, 45
	jal	min_caml_create_float_array
	addi	r3, r3, -45
	lwi	r31 r3 44
	lwi	r2, r3 32
	sli	r5, r2, 0
	lwi	r6, r3 43
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 44
	addi	r3, r3, 45
	jal	min_caml_create_float_array
	addi	r3, r3, -45
	lwi	r31 r3 44
	lwi	r2, r3 31
	sli	r5, r2, 0
	lwi	r6, r3 43
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 44
	addi	r3, r3, 45
	jal	min_caml_create_float_array
	addi	r3, r3, -45
	lwi	r31 r3 44
	lwi	r2, r3 35
	sli	r5, r2, 0
	lwi	r6, r3 43
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	addi	r1, r0 4
	flwi	f1, r3 34 #off
	swi	r1, r3 44
	addi	r1, r2 0 #
	swi	r31 r3 45
	addi	r3, r3, 46
	jal	min_caml_create_float_array
	addi	r3, r3, -46
	lwi	r31 r3 45
	lwi	r2, r3 43
	swi	r1, r2 4
	lwi	r1, r3 33
	lwi	r5, r3 38
	addi	r2, r5 0 #
	swi	r31 r3 45
	addi	r3, r3, 46
	jal	min_caml_create_array
	addi	r3, r3, -46
	lwi	r31 r3 45
	lwi	r2, r3 33
	lwi	r5, r3 38
	swi	r1, r3 45
	addi	r1, r2 0 #
	addi	r2, r5 0 #
	swi	r31 r3 46
	addi	r3, r3, 47
	jal	min_caml_create_array
	addi	r3, r3, -47
	lwi	r31 r3 46
	flwi	f1, r3 34 #off
	lwi	r2, r3 35
	swi	r1, r3 46
	addi	r1, r2 0 #
	swi	r31 r3 47
	addi	r3, r3, 48
	jal	min_caml_create_float_array
	addi	r3, r3, -48
	lwi	r31 r3 47
	addi	r2, r1 0 ##
	lwi	r1, r3 33
	swi	r31 r3 47
	addi	r3, r3, 48
	jal	min_caml_create_array
	addi	r3, r3, -48
	lwi	r31 r3 47
	flwi	f1, r3 34 #off
	lwi	r2, r3 35
	swi	r1, r3 47
	addi	r1, r2 0 #
	swi	r31 r3 48
	addi	r3, r3, 49
	jal	min_caml_create_float_array
	addi	r3, r3, -49
	lwi	r31 r3 48
	lwi	r2, r3 32
	sli	r5, r2, 0
	lwi	r6, r3 47
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 48
	addi	r3, r3, 49
	jal	min_caml_create_float_array
	addi	r3, r3, -49
	lwi	r31 r3 48
	lwi	r2, r3 31
	sli	r5, r2, 0
	lwi	r6, r3 47
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 48
	addi	r3, r3, 49
	jal	min_caml_create_float_array
	addi	r3, r3, -49
	lwi	r31 r3 48
	lwi	r2, r3 35
	sli	r5, r2, 0
	lwi	r6, r3 47
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	addi	r1, r2 0 #
	swi	r31 r3 48
	addi	r3, r3, 49
	jal	min_caml_create_float_array
	addi	r3, r3, -49
	lwi	r31 r3 48
	lwi	r2, r3 47
	swi	r1, r2 4
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 48
	addi	r3, r3, 49
	jal	min_caml_create_float_array
	addi	r3, r3, -49
	lwi	r31 r3 48
	addi	r2, r1 0 ##
	lwi	r1, r3 33
	swi	r31 r3 48
	addi	r3, r3, 49
	jal	min_caml_create_array
	addi	r3, r3, -49
	lwi	r31 r3 48
	flwi	f1, r3 34 #off
	lwi	r2, r3 35
	swi	r1, r3 48
	addi	r1, r2 0 #
	swi	r31 r3 49
	addi	r3, r3, 50
	jal	min_caml_create_float_array
	addi	r3, r3, -50
	lwi	r31 r3 49
	lwi	r2, r3 32
	sli	r5, r2, 0
	lwi	r6, r3 48
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 49
	addi	r3, r3, 50
	jal	min_caml_create_float_array
	addi	r3, r3, -50
	lwi	r31 r3 49
	lwi	r2, r3 31
	sli	r5, r2, 0
	lwi	r6, r3 48
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 49
	addi	r3, r3, 50
	jal	min_caml_create_float_array
	addi	r3, r3, -50
	lwi	r31 r3 49
	lwi	r2, r3 35
	sli	r5, r2, 0
	lwi	r6, r3 48
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	addi	r1, r2 0 #
	swi	r31 r3 49
	addi	r3, r3, 50
	jal	min_caml_create_float_array
	addi	r3, r3, -50
	lwi	r31 r3 49
	lwi	r2, r3 48
	swi	r1, r2 4
	lwi	r1, r3 32
	lwi	r5, r3 38
	addi	r2, r5 0 #
	swi	r31 r3 49
	addi	r3, r3, 50
	jal	min_caml_create_array
	addi	r3, r3, -50
	lwi	r31 r3 49
	flwi	f1, r3 34 #off
	lwi	r2, r3 35
	swi	r1, r3 49
	addi	r1, r2 0 #
	swi	r31 r3 50
	addi	r3, r3, 51
	jal	min_caml_create_float_array
	addi	r3, r3, -51
	lwi	r31 r3 50
	addi	r2, r1 0 ##
	lwi	r1, r3 33
	swi	r31 r3 50
	addi	r3, r3, 51
	jal	min_caml_create_array
	addi	r3, r3, -51
	lwi	r31 r3 50
	flwi	f1, r3 34 #off
	lwi	r2, r3 35
	swi	r1, r3 50
	addi	r1, r2 0 #
	swi	r31 r3 51
	addi	r3, r3, 52
	jal	min_caml_create_float_array
	addi	r3, r3, -52
	lwi	r31 r3 51
	lwi	r2, r3 32
	sli	r5, r2, 0
	lwi	r6, r3 50
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 51
	addi	r3, r3, 52
	jal	min_caml_create_float_array
	addi	r3, r3, -52
	lwi	r31 r3 51
	lwi	r2, r3 31
	sli	r5, r2, 0
	lwi	r6, r3 50
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 51
	addi	r3, r3, 52
	jal	min_caml_create_float_array
	addi	r3, r3, -52
	lwi	r31 r3 51
	lwi	r2, r3 35
	sli	r5, r2, 0
	lwi	r6, r3 50
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	addi	r1, r2 0 #
	swi	r31 r3 51
	addi	r3, r3, 52
	jal	min_caml_create_float_array
	addi	r3, r3, -52
	lwi	r31 r3 51
	lwi	r2, r3 50
	swi	r1, r2 4
	addi	r1, r4 0
	addi	r4, r4, 8
	swi	r2, r1 7
	lwi	r2, r3 49
	swi	r2, r1 6
	lwi	r2, r3 48
	swi	r2, r1 5
	lwi	r2, r3 47
	swi	r2, r1 4
	lwi	r2, r3 46
	swi	r2, r1 3
	lwi	r2, r3 45
	swi	r2, r1 2
	lwi	r2, r3 43
	swi	r2, r1 1
	lwi	r2, r3 42
	swi	r2, r1 0
	addi	r2, r1 0
	lwi	r1, r3 41
	swi	r31 r3 51
	addi	r3, r3, 52
	jal	min_caml_create_array
	addi	r3, r3, -52
	lwi	r31 r3 51
	lwi	r2, r3 38
	sli	r5, r2, 0
	lwi	r6, r3 36
	add	r5 r6 r5
	lwi	r5, r5 0
	lwi	r7, r3 31
	sub	r5, r5, r7
	sgt	r30, r2, r5
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22330
	lwi	r28, r3 30
	swi	r1, r3 51
	swi	r5, r3 52
	swi	r31 r3 53
	addi	r3, r3, 54
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -54
	lwi	r31, r3 53
	lwi	r2, r3 52
	sli	r5, r2, 0
	lwi	r6, r3 51
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 32
	sub	r2, r2, r1
	lwi	r5, r3 38
	sgt	r30, r5, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22332
	lwi	r28, r3 30
	swi	r2, r3 53
	swi	r31 r3 54
	addi	r3, r3, 55
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -55
	lwi	r31, r3 54
	lwi	r2, r3 53
	sli	r5, r2, 0
	lwi	r6, r3 51
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 32
	sub	r2, r2, r1
	lwi	r28, r3 29
	addi	r1, r6 0 #
	swi	r31 r3 54
	addi	r3, r3, 55
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -55
	lwi	r31, r3 54
	jl	ble_cont.22333
ble_else.22332:
	addi	r1, r6 0
ble_cont.22333:
	jl	ble_cont.22331
ble_else.22330:
ble_cont.22331:
	lwi	r2, r3 38
	sli	r5, r2, 0
	lwi	r6, r3 36
	add	r5 r6 r5
	lwi	r5, r5 0
	flwi	f1, r3 34 #off
	lwi	r7, r3 35
	swi	r1, r3 54
	swi	r5, r3 55
	addi	r1, r7 0 #
	swi	r31 r3 56
	addi	r3, r3, 57
	jal	min_caml_create_float_array
	addi	r3, r3, -57
	lwi	r31 r3 56
	flwi	f1, r3 34 #off
	lwi	r2, r3 35
	swi	r1, r3 56
	addi	r1, r2 0 #
	swi	r31 r3 57
	addi	r3, r3, 58
	jal	min_caml_create_float_array
	addi	r3, r3, -58
	lwi	r31 r3 57
	addi	r2, r1 0 ##
	lwi	r1, r3 33
	swi	r31 r3 57
	addi	r3, r3, 58
	jal	min_caml_create_array
	addi	r3, r3, -58
	lwi	r31 r3 57
	flwi	f1, r3 34 #off
	lwi	r2, r3 35
	swi	r1, r3 57
	addi	r1, r2 0 #
	swi	r31 r3 58
	addi	r3, r3, 59
	jal	min_caml_create_float_array
	addi	r3, r3, -59
	lwi	r31 r3 58
	lwi	r2, r3 32
	sli	r5, r2, 0
	lwi	r6, r3 57
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 58
	addi	r3, r3, 59
	jal	min_caml_create_float_array
	addi	r3, r3, -59
	lwi	r31 r3 58
	lwi	r2, r3 31
	sli	r5, r2, 0
	lwi	r6, r3 57
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 58
	addi	r3, r3, 59
	jal	min_caml_create_float_array
	addi	r3, r3, -59
	lwi	r31 r3 58
	lwi	r2, r3 35
	sli	r5, r2, 0
	lwi	r6, r3 57
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	addi	r1, r2 0 #
	swi	r31 r3 58
	addi	r3, r3, 59
	jal	min_caml_create_float_array
	addi	r3, r3, -59
	lwi	r31 r3 58
	lwi	r2, r3 57
	swi	r1, r2 4
	lwi	r1, r3 33
	lwi	r5, r3 38
	addi	r2, r5 0 #
	swi	r31 r3 58
	addi	r3, r3, 59
	jal	min_caml_create_array
	addi	r3, r3, -59
	lwi	r31 r3 58
	lwi	r2, r3 33
	lwi	r5, r3 38
	swi	r1, r3 58
	addi	r1, r2 0 #
	addi	r2, r5 0 #
	swi	r31 r3 59
	addi	r3, r3, 60
	jal	min_caml_create_array
	addi	r3, r3, -60
	lwi	r31 r3 59
	flwi	f1, r3 34 #off
	lwi	r2, r3 35
	swi	r1, r3 59
	addi	r1, r2 0 #
	swi	r31 r3 60
	addi	r3, r3, 61
	jal	min_caml_create_float_array
	addi	r3, r3, -61
	lwi	r31 r3 60
	addi	r2, r1 0 ##
	lwi	r1, r3 33
	swi	r31 r3 60
	addi	r3, r3, 61
	jal	min_caml_create_array
	addi	r3, r3, -61
	lwi	r31 r3 60
	flwi	f1, r3 34 #off
	lwi	r2, r3 35
	swi	r1, r3 60
	addi	r1, r2 0 #
	swi	r31 r3 61
	addi	r3, r3, 62
	jal	min_caml_create_float_array
	addi	r3, r3, -62
	lwi	r31 r3 61
	lwi	r2, r3 32
	sli	r5, r2, 0
	lwi	r6, r3 60
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 61
	addi	r3, r3, 62
	jal	min_caml_create_float_array
	addi	r3, r3, -62
	lwi	r31 r3 61
	lwi	r2, r3 31
	sli	r5, r2, 0
	lwi	r6, r3 60
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 61
	addi	r3, r3, 62
	jal	min_caml_create_float_array
	addi	r3, r3, -62
	lwi	r31 r3 61
	lwi	r2, r3 35
	sli	r5, r2, 0
	lwi	r6, r3 60
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	addi	r1, r2 0 #
	swi	r31 r3 61
	addi	r3, r3, 62
	jal	min_caml_create_float_array
	addi	r3, r3, -62
	lwi	r31 r3 61
	lwi	r2, r3 60
	swi	r1, r2 4
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 61
	addi	r3, r3, 62
	jal	min_caml_create_float_array
	addi	r3, r3, -62
	lwi	r31 r3 61
	addi	r2, r1 0 ##
	lwi	r1, r3 33
	swi	r31 r3 61
	addi	r3, r3, 62
	jal	min_caml_create_array
	addi	r3, r3, -62
	lwi	r31 r3 61
	flwi	f1, r3 34 #off
	lwi	r2, r3 35
	swi	r1, r3 61
	addi	r1, r2 0 #
	swi	r31 r3 62
	addi	r3, r3, 63
	jal	min_caml_create_float_array
	addi	r3, r3, -63
	lwi	r31 r3 62
	lwi	r2, r3 32
	sli	r5, r2, 0
	lwi	r6, r3 61
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 62
	addi	r3, r3, 63
	jal	min_caml_create_float_array
	addi	r3, r3, -63
	lwi	r31 r3 62
	lwi	r2, r3 31
	sli	r5, r2, 0
	lwi	r6, r3 61
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 62
	addi	r3, r3, 63
	jal	min_caml_create_float_array
	addi	r3, r3, -63
	lwi	r31 r3 62
	lwi	r2, r3 35
	sli	r5, r2, 0
	lwi	r6, r3 61
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	addi	r1, r2 0 #
	swi	r31 r3 62
	addi	r3, r3, 63
	jal	min_caml_create_float_array
	addi	r3, r3, -63
	lwi	r31 r3 62
	lwi	r2, r3 61
	swi	r1, r2 4
	lwi	r1, r3 32
	lwi	r5, r3 38
	addi	r2, r5 0 #
	swi	r31 r3 62
	addi	r3, r3, 63
	jal	min_caml_create_array
	addi	r3, r3, -63
	lwi	r31 r3 62
	flwi	f1, r3 34 #off
	lwi	r2, r3 35
	swi	r1, r3 62
	addi	r1, r2 0 #
	swi	r31 r3 63
	addi	r3, r3, 64
	jal	min_caml_create_float_array
	addi	r3, r3, -64
	lwi	r31 r3 63
	addi	r2, r1 0 ##
	lwi	r1, r3 33
	swi	r31 r3 63
	addi	r3, r3, 64
	jal	min_caml_create_array
	addi	r3, r3, -64
	lwi	r31 r3 63
	flwi	f1, r3 34 #off
	lwi	r2, r3 35
	swi	r1, r3 63
	addi	r1, r2 0 #
	swi	r31 r3 64
	addi	r3, r3, 65
	jal	min_caml_create_float_array
	addi	r3, r3, -65
	lwi	r31 r3 64
	lwi	r2, r3 32
	sli	r5, r2, 0
	lwi	r6, r3 63
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 64
	addi	r3, r3, 65
	jal	min_caml_create_float_array
	addi	r3, r3, -65
	lwi	r31 r3 64
	lwi	r2, r3 31
	sli	r5, r2, 0
	lwi	r6, r3 63
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 64
	addi	r3, r3, 65
	jal	min_caml_create_float_array
	addi	r3, r3, -65
	lwi	r31 r3 64
	lwi	r2, r3 35
	sli	r5, r2, 0
	lwi	r6, r3 63
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	addi	r1, r2 0 #
	swi	r31 r3 64
	addi	r3, r3, 65
	jal	min_caml_create_float_array
	addi	r3, r3, -65
	lwi	r31 r3 64
	lwi	r2, r3 63
	swi	r1, r2 4
	addi	r1, r4 0
	addi	r4, r4, 8
	swi	r2, r1 7
	lwi	r2, r3 62
	swi	r2, r1 6
	lwi	r2, r3 61
	swi	r2, r1 5
	lwi	r2, r3 60
	swi	r2, r1 4
	lwi	r2, r3 59
	swi	r2, r1 3
	lwi	r2, r3 58
	swi	r2, r1 2
	lwi	r2, r3 57
	swi	r2, r1 1
	lwi	r2, r3 56
	swi	r2, r1 0
	addi	r2, r1 0
	lwi	r1, r3 55
	swi	r31 r3 64
	addi	r3, r3, 65
	jal	min_caml_create_array
	addi	r3, r3, -65
	lwi	r31 r3 64
	lwi	r2, r3 38
	sli	r5, r2, 0
	lwi	r6, r3 36
	add	r5 r6 r5
	lwi	r5, r5 0
	lwi	r7, r3 31
	sub	r5, r5, r7
	sgt	r30, r2, r5
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22334
	lwi	r28, r3 30
	swi	r1, r3 64
	swi	r5, r3 65
	swi	r31 r3 66
	addi	r3, r3, 67
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -67
	lwi	r31, r3 66
	lwi	r2, r3 65
	sli	r5, r2, 0
	lwi	r6, r3 64
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 32
	sub	r2, r2, r1
	lwi	r5, r3 38
	sgt	r30, r5, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22336
	lwi	r28, r3 30
	swi	r2, r3 66
	swi	r31 r3 67
	addi	r3, r3, 68
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -68
	lwi	r31, r3 67
	lwi	r2, r3 66
	sli	r5, r2, 0
	lwi	r6, r3 64
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 32
	sub	r2, r2, r1
	lwi	r28, r3 29
	addi	r1, r6 0 #
	swi	r31 r3 67
	addi	r3, r3, 68
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -68
	lwi	r31, r3 67
	jl	ble_cont.22337
ble_else.22336:
	addi	r1, r6 0
ble_cont.22337:
	jl	ble_cont.22335
ble_else.22334:
ble_cont.22335:
	lwi	r2, r3 38
	sli	r5, r2, 0
	lwi	r6, r3 36
	add	r5 r6 r5
	lwi	r5, r5 0
	flwi	f1, r3 34 #off
	lwi	r7, r3 35
	swi	r1, r3 67
	swi	r5, r3 68
	addi	r1, r7 0 #
	swi	r31 r3 69
	addi	r3, r3, 70
	jal	min_caml_create_float_array
	addi	r3, r3, -70
	lwi	r31 r3 69
	flwi	f1, r3 34 #off
	lwi	r2, r3 35
	swi	r1, r3 69
	addi	r1, r2 0 #
	swi	r31 r3 70
	addi	r3, r3, 71
	jal	min_caml_create_float_array
	addi	r3, r3, -71
	lwi	r31 r3 70
	addi	r2, r1 0 ##
	lwi	r1, r3 33
	swi	r31 r3 70
	addi	r3, r3, 71
	jal	min_caml_create_array
	addi	r3, r3, -71
	lwi	r31 r3 70
	flwi	f1, r3 34 #off
	lwi	r2, r3 35
	swi	r1, r3 70
	addi	r1, r2 0 #
	swi	r31 r3 71
	addi	r3, r3, 72
	jal	min_caml_create_float_array
	addi	r3, r3, -72
	lwi	r31 r3 71
	lwi	r2, r3 32
	sli	r5, r2, 0
	lwi	r6, r3 70
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 71
	addi	r3, r3, 72
	jal	min_caml_create_float_array
	addi	r3, r3, -72
	lwi	r31 r3 71
	lwi	r2, r3 31
	sli	r5, r2, 0
	lwi	r6, r3 70
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 71
	addi	r3, r3, 72
	jal	min_caml_create_float_array
	addi	r3, r3, -72
	lwi	r31 r3 71
	lwi	r2, r3 35
	sli	r5, r2, 0
	lwi	r6, r3 70
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	addi	r1, r2 0 #
	swi	r31 r3 71
	addi	r3, r3, 72
	jal	min_caml_create_float_array
	addi	r3, r3, -72
	lwi	r31 r3 71
	lwi	r2, r3 70
	swi	r1, r2 4
	lwi	r1, r3 33
	lwi	r5, r3 38
	addi	r2, r5 0 #
	swi	r31 r3 71
	addi	r3, r3, 72
	jal	min_caml_create_array
	addi	r3, r3, -72
	lwi	r31 r3 71
	lwi	r2, r3 33
	lwi	r5, r3 38
	swi	r1, r3 71
	addi	r1, r2 0 #
	addi	r2, r5 0 #
	swi	r31 r3 72
	addi	r3, r3, 73
	jal	min_caml_create_array
	addi	r3, r3, -73
	lwi	r31 r3 72
	flwi	f1, r3 34 #off
	lwi	r2, r3 35
	swi	r1, r3 72
	addi	r1, r2 0 #
	swi	r31 r3 73
	addi	r3, r3, 74
	jal	min_caml_create_float_array
	addi	r3, r3, -74
	lwi	r31 r3 73
	addi	r2, r1 0 ##
	lwi	r1, r3 33
	swi	r31 r3 73
	addi	r3, r3, 74
	jal	min_caml_create_array
	addi	r3, r3, -74
	lwi	r31 r3 73
	flwi	f1, r3 34 #off
	lwi	r2, r3 35
	swi	r1, r3 73
	addi	r1, r2 0 #
	swi	r31 r3 74
	addi	r3, r3, 75
	jal	min_caml_create_float_array
	addi	r3, r3, -75
	lwi	r31 r3 74
	lwi	r2, r3 32
	sli	r5, r2, 0
	lwi	r6, r3 73
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 74
	addi	r3, r3, 75
	jal	min_caml_create_float_array
	addi	r3, r3, -75
	lwi	r31 r3 74
	lwi	r2, r3 31
	sli	r5, r2, 0
	lwi	r6, r3 73
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 74
	addi	r3, r3, 75
	jal	min_caml_create_float_array
	addi	r3, r3, -75
	lwi	r31 r3 74
	lwi	r2, r3 35
	sli	r5, r2, 0
	lwi	r6, r3 73
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	addi	r1, r2 0 #
	swi	r31 r3 74
	addi	r3, r3, 75
	jal	min_caml_create_float_array
	addi	r3, r3, -75
	lwi	r31 r3 74
	lwi	r2, r3 73
	swi	r1, r2 4
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 74
	addi	r3, r3, 75
	jal	min_caml_create_float_array
	addi	r3, r3, -75
	lwi	r31 r3 74
	addi	r2, r1 0 ##
	lwi	r1, r3 33
	swi	r31 r3 74
	addi	r3, r3, 75
	jal	min_caml_create_array
	addi	r3, r3, -75
	lwi	r31 r3 74
	flwi	f1, r3 34 #off
	lwi	r2, r3 35
	swi	r1, r3 74
	addi	r1, r2 0 #
	swi	r31 r3 75
	addi	r3, r3, 76
	jal	min_caml_create_float_array
	addi	r3, r3, -76
	lwi	r31 r3 75
	lwi	r2, r3 32
	sli	r5, r2, 0
	lwi	r6, r3 74
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 75
	addi	r3, r3, 76
	jal	min_caml_create_float_array
	addi	r3, r3, -76
	lwi	r31 r3 75
	lwi	r2, r3 31
	sli	r5, r2, 0
	lwi	r6, r3 74
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 75
	addi	r3, r3, 76
	jal	min_caml_create_float_array
	addi	r3, r3, -76
	lwi	r31 r3 75
	lwi	r2, r3 35
	sli	r5, r2, 0
	lwi	r6, r3 74
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	addi	r1, r2 0 #
	swi	r31 r3 75
	addi	r3, r3, 76
	jal	min_caml_create_float_array
	addi	r3, r3, -76
	lwi	r31 r3 75
	lwi	r2, r3 74
	swi	r1, r2 4
	lwi	r1, r3 32
	lwi	r5, r3 38
	addi	r2, r5 0 #
	swi	r31 r3 75
	addi	r3, r3, 76
	jal	min_caml_create_array
	addi	r3, r3, -76
	lwi	r31 r3 75
	flwi	f1, r3 34 #off
	lwi	r2, r3 35
	swi	r1, r3 75
	addi	r1, r2 0 #
	swi	r31 r3 76
	addi	r3, r3, 77
	jal	min_caml_create_float_array
	addi	r3, r3, -77
	lwi	r31 r3 76
	addi	r2, r1 0 ##
	lwi	r1, r3 33
	swi	r31 r3 76
	addi	r3, r3, 77
	jal	min_caml_create_array
	addi	r3, r3, -77
	lwi	r31 r3 76
	flwi	f1, r3 34 #off
	lwi	r2, r3 35
	swi	r1, r3 76
	addi	r1, r2 0 #
	swi	r31 r3 77
	addi	r3, r3, 78
	jal	min_caml_create_float_array
	addi	r3, r3, -78
	lwi	r31 r3 77
	lwi	r2, r3 32
	sli	r5, r2, 0
	lwi	r6, r3 76
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 77
	addi	r3, r3, 78
	jal	min_caml_create_float_array
	addi	r3, r3, -78
	lwi	r31 r3 77
	lwi	r2, r3 31
	sli	r5, r2, 0
	lwi	r6, r3 76
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	lwi	r1, r3 35
	swi	r31 r3 77
	addi	r3, r3, 78
	jal	min_caml_create_float_array
	addi	r3, r3, -78
	lwi	r31 r3 77
	lwi	r2, r3 35
	sli	r5, r2, 0
	lwi	r6, r3 76
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	flwi	f1, r3 34 #off
	addi	r1, r2 0 #
	swi	r31 r3 77
	addi	r3, r3, 78
	jal	min_caml_create_float_array
	addi	r3, r3, -78
	lwi	r31 r3 77
	lwi	r2, r3 76
	swi	r1, r2 4
	addi	r1, r4 0
	addi	r4, r4, 8
	swi	r2, r1 7
	lwi	r2, r3 75
	swi	r2, r1 6
	lwi	r2, r3 74
	swi	r2, r1 5
	lwi	r2, r3 73
	swi	r2, r1 4
	lwi	r2, r3 72
	swi	r2, r1 3
	lwi	r2, r3 71
	swi	r2, r1 2
	lwi	r2, r3 70
	swi	r2, r1 1
	lwi	r2, r3 69
	swi	r2, r1 0
	addi	r2, r1 0
	lwi	r1, r3 68
	swi	r31 r3 77
	addi	r3, r3, 78
	jal	min_caml_create_array
	addi	r3, r3, -78
	lwi	r31 r3 77
	lwi	r2, r3 38
	sli	r5, r2, 0
	lwi	r6, r3 36
	add	r5 r6 r5
	lwi	r5, r5 0
	lwi	r7, r3 31
	sub	r5, r5, r7
	sgt	r30, r2, r5
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22338
	lwi	r28, r3 30
	swi	r1, r3 77
	swi	r5, r3 78
	swi	r31 r3 79
	addi	r3, r3, 80
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -80
	lwi	r31, r3 79
	lwi	r2, r3 78
	sli	r5, r2, 0
	lwi	r6, r3 77
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 32
	sub	r2, r2, r1
	lwi	r5, r3 38
	sgt	r30, r5, r2
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22340
	lwi	r28, r3 30
	swi	r2, r3 79
	swi	r31 r3 80
	addi	r3, r3, 81
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -81
	lwi	r31, r3 80
	lwi	r2, r3 79
	sli	r5, r2, 0
	lwi	r6, r3 77
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 32
	sub	r2, r2, r1
	lwi	r28, r3 29
	addi	r1, r6 0 #
	swi	r31 r3 80
	addi	r3, r3, 81
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -81
	lwi	r31, r3 80
	jl	ble_cont.22341
ble_else.22340:
	addi	r1, r6 0
ble_cont.22341:
	jl	ble_cont.22339
ble_else.22338:
ble_cont.22339:
	lwi	r28, r3 28
	swi	r1, r3 80
	swi	r31 r3 81
	addi	r3, r3, 82
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -82
	lwi	r31, r3 81
	lwi	r28, r3 27
	swi	r31 r3 81
	addi	r3, r3, 82
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -82
	lwi	r31, r3 81
	lwi	r1, r3 38
	lwi	r28, r3 26
	swi	r31 r3 81
	addi	r3, r3, 82
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -82
	lwi	r31, r3 81
	lwi	r2, r3 38
	seq	r30, r1, r2
	jeql	r30, r0, beq_else.22342
	sli	r1, r2, 0
	lwi	r5, r3 25
	add	r0 r5 r1
	swi	r2, r0 0
	sub	r0 r0 r0
	jl	beq_cont.22343
beq_else.22342:
	lwi	r1, r3 32
	lwi	r28, r3 24
	swi	r31 r3 81
	addi	r3, r3, 82
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -82
	lwi	r31, r3 81
beq_cont.22343:
	lwi	r1, r3 38
	lwi	r28, r3 23
	swi	r31 r3 81
	addi	r3, r3, 82
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -82
	lwi	r31, r3 81
	lwi	r1, r3 38
	lwi	r28, r3 22
	swi	r31 r3 81
	addi	r3, r3, 82
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -82
	lwi	r31, r3 81
	lwi	r2, r3 38
	sli	r5, r2, 0
	lwi	r6, r3 21
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	lwi	r28, r3 20
	swi	r31 r3 81
	addi	r3, r3, 82
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -82
	lwi	r31, r3 81
	lwi	r1, r3 44
	lwi	r28, r3 19
	swi	r31 r3 81
	addi	r3, r3, 82
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -82
	lwi	r31, r3 81
	addi	r1, r0 9
	swi	r31 r3 81
	addi	r3, r3, 82
	jal	min_caml_float_of_int
	addi	r3, r3, -82
	lwi	r31 r3 81
	flwl	%f2 l.11805
	fmul	f1, f1, f2
	flwl	%f2 l.12859
	fsub	f1, f1, f2
	lwi	r1, r3 44
	lwi	r2, r3 38
	lwi	r28, r3 18
	addi	r5, r2 0 #
	swi	r31 r3 81
	addi	r3, r3, 82
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -82
	lwi	r31, r3 81
	addi	r1, r0 8
	lwi	r2, r3 31
	lwi	r5, r3 44
	lwi	r28, r3 17
	swi	r31 r3 81
	addi	r3, r3, 82
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -82
	lwi	r31, r3 81
	lwi	r1, r3 16
	lwi	r2, r1 4
	addi	r5, r0 119
	lwi	r6, r2 119
	lwi	r7, r3 38
	sli	r8, r7, 0
	lwi	r9, r3 25
	add	r8 r9 r8
	lwi	r8, r8 0
	lwi	r10, r3 32
	sub	r8, r8, r10
	swi	r5, r3 81
	swi	r2, r3 82
	sgt	r30, r7, r8
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22344
	sli	r11, r8, 0
	lwi	r12, r3 15
	add	r11 r12 r11
	lwi	r11, r11 0
	lwi	r13, r6 1
	lwi	r14, r6 0
	lwi	r15, r11 1
	swi	r6, r3 83
	seq	r30, r15, r10
	jeql	r30, r0, beq_else.22346
	lwi	r28, r3 14
	swi	r13, r3 84
	swi	r8, r3 85
	addi	r2, r11 0 #
	addi	r1, r14 0 #
	swi	r31 r3 86
	addi	r3, r3, 87
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -87
	lwi	r31, r3 86
	lwi	r2, r3 85
	sli	r5, r2, 0
	lwi	r6, r3 84
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.22347
beq_else.22346:
	lwi	r16, r3 31
	seq	r30, r15, r16
	jeql	r30, r0, beq_else.22348
	lwi	r28, r3 13
	swi	r13, r3 84
	swi	r8, r3 85
	addi	r2, r11 0 #
	addi	r1, r14 0 #
	swi	r31 r3 86
	addi	r3, r3, 87
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -87
	lwi	r31, r3 86
	lwi	r2, r3 85
	sli	r5, r2, 0
	lwi	r6, r3 84
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.22349
beq_else.22348:
	lwi	r28, r3 12
	swi	r13, r3 84
	swi	r8, r3 85
	addi	r2, r11 0 #
	addi	r1, r14 0 #
	swi	r31 r3 86
	addi	r3, r3, 87
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -87
	lwi	r31, r3 86
	lwi	r2, r3 85
	sli	r5, r2, 0
	lwi	r6, r3 84
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
beq_cont.22349:
beq_cont.22347:
	lwi	r1, r3 32
	sub	r2, r2, r1
	lwi	r5, r3 83
	lwi	r28, r3 11
	addi	r1, r5 0 #
	swi	r31 r3 86
	addi	r3, r3, 87
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -87
	lwi	r31, r3 86
	jl	ble_cont.22345
ble_else.22344:
ble_cont.22345:
	addi	r2, r0 118
	lwi	r1, r3 82
	lwi	r28, r3 10
	swi	r31 r3 86
	addi	r3, r3, 87
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -87
	lwi	r31, r3 86
	lwi	r1, r3 35
	sli	r1, r1, 0
	lwi	r2, r3 16
	add	r1 r2 r1
	lwi	r1, r1 0
	lwi	r2, r3 81
	lwi	r28, r3 10
	swi	r31 r3 86
	addi	r3, r3, 87
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -87
	lwi	r31, r3 86
	lwi	r1, r3 31
	lwi	r28, r3 8
	swi	r31 r3 86
	addi	r3, r3, 87
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -87
	lwi	r31, r3 86
	lwi	r2, r3 38
	sli	r1, r2, 0
	lwi	r5, r3 7
	add	r0 r5 r1
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r1, r2, 0
	lwi	r6, r3 6
	add	r0 r6 r1
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r1, r3 32
	sli	r7, r1, 0
	add	r0 r5 r7
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r7, r1, 0
	add	r0 r6 r7
	fswi	f1, r0 0
	sub	r0 r0 r0
	lwi	r7, r3 31
	sli	r8, r7, 0
	add	r0 r5 r8
	flwi	f1, r0 0
	sub	r0 r0 r0
	sli	r5, r7, 0
	add	r0 r6 r5
	fswi	f1, r0 0
	sub	r0 r0 r0
	sli	r5, r2, 0
	lwi	r8, r3 25
	add	r5 r8 r5
	lwi	r5, r5 0
	sub	r5, r5, r1
	sgt	r30, r2, r5
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22350
	sli	r9, r5, 0
	lwi	r10, r3 15
	add	r9 r10 r9
	lwi	r9, r9 0
	lwi	r11, r9 1
	seq	r30, r11, r1
	jeql	r30, r0, beq_else.22352
	lwi	r28, r3 14
	swi	r5, r3 86
	addi	r2, r9 0 #
	addi	r1, r6 0 #
	swi	r31 r3 87
	addi	r3, r3, 88
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -88
	lwi	r31, r3 87
	lwi	r2, r3 86
	sli	r5, r2, 0
	lwi	r6, r3 9
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.22353
beq_else.22352:
	seq	r30, r11, r7
	jeql	r30, r0, beq_else.22354
	lwi	r28, r3 13
	swi	r5, r3 86
	addi	r2, r9 0 #
	addi	r1, r6 0 #
	swi	r31 r3 87
	addi	r3, r3, 88
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -88
	lwi	r31, r3 87
	lwi	r2, r3 86
	sli	r5, r2, 0
	lwi	r6, r3 9
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
	jl	beq_cont.22355
beq_else.22354:
	lwi	r28, r3 12
	swi	r5, r3 86
	addi	r2, r9 0 #
	addi	r1, r6 0 #
	swi	r31 r3 87
	addi	r3, r3, 88
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -88
	lwi	r31, r3 87
	lwi	r2, r3 86
	sli	r5, r2, 0
	lwi	r6, r3 9
	add	r0 r6 r5
	swi	r1, r0 0
	sub	r0 r0 r0
beq_cont.22355:
beq_cont.22353:
	lwi	r1, r3 32
	sub	r2, r2, r1
	lwi	r5, r3 5
	lwi	r28, r3 11
	addi	r1, r5 0 #
	swi	r31 r3 87
	addi	r3, r3, 88
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -88
	lwi	r31, r3 87
	jl	ble_cont.22351
ble_else.22350:
ble_cont.22351:
	lwi	r2, r3 38
	sli	r1, r2, 0
	lwi	r5, r3 25
	add	r1 r5 r1
	lwi	r1, r1 0
	lwi	r5, r3 32
	sub	r1, r1, r5
	sgt	r30, r2, r1
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22356
	sli	r6, r1, 0
	lwi	r7, r3 15
	add	r6 r7 r6
	lwi	r6, r6 0
	lwi	r7, r6 2
	lwi	r8, r3 31
	seq	r30, r7, r8
	jeql	r30, r0, beq_else.22358
	lwi	r7, r6 7
	flwi	f1, r7 0 #
	flwl	%f2 l.11794
	fgt	r30 f2 f1
	seq	r30 r30 r0
	jeql	r30, r0, ble_else.22360
	addi	r7, r0 0
	jl	ble_cont.22361
ble_else.22360:
	addi	r7, r0 1
ble_cont.22361:
	seq	r30, r7, r2
	jeql	r30, r0, beq_else.22362
	jl	beq_cont.22363
beq_else.22362:
	lwi	r7, r6 1
	seq	r30, r7, r5
	jeql	r30, r0, beq_else.22364
	lwi	r28, r3 4
	addi	r2, r6 0 #
	swi	r31 r3 87
	addi	r3, r3, 88
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -88
	lwi	r31, r3 87
	jl	beq_cont.22365
beq_else.22364:
	seq	r30, r7, r8
	jeql	r30, r0, beq_else.22366
	lwi	r28, r3 3
	addi	r2, r6 0 #
	swi	r31 r3 87
	addi	r3, r3, 88
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -88
	lwi	r31, r3 87
	jl	beq_cont.22367
beq_else.22366:
beq_cont.22367:
beq_cont.22365:
beq_cont.22363:
	jl	beq_cont.22359
beq_else.22358:
beq_cont.22359:
	jl	ble_cont.22357
ble_else.22356:
ble_cont.22357:
	lwi	r1, r3 67
	lwi	r2, r3 38
	lwi	r28, r3 2
	addi	r5, r2 0 #
	swi	r31 r3 87
	addi	r3, r3, 88
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -88
	lwi	r31, r3 87
	lwi	r2, r3 32
	sli	r1, r2, 0
	lwi	r5, r3 36
	add	r1 r5 r1
	lwi	r1, r1 0
	lwi	r6, r3 38
	sgt	r30, r1, r6
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22368
	jr	r31
ble_else.22368:
	sli	r1, r2, 0
	add	r1 r5 r1
	lwi	r1, r1 0
	sub	r1, r1, r2
	sgt	r30, r1, r6
	seq	r30, r30, r0
	jeql	r30, r0, ble_else.22370
	jl	ble_cont.22371
ble_else.22370:
	lwi	r1, r3 80
	lwi	r5, r3 31
	lwi	r28, r3 2
	swi	r31 r3 87
	addi	r3, r3, 88
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -88
	lwi	r31, r3 87
ble_cont.22371:
	lwi	r1, r3 38
	lwi	r5, r3 54
	lwi	r6, r3 67
	lwi	r7, r3 80
	lwi	r28, r3 1
	addi	r2, r1 0 #
	swi	r31 r3 87
	addi	r3, r3, 88
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -88
	lwi	r31, r3 87
	lwi	r1, r3 32
	lwi	r2, r3 67
	lwi	r5, r3 80
	lwi	r6, r3 54
	lwi	r7, r3 44
	lwi	r28, r3 0
	lwi	r29, r28 0
	jr	r29
_min_caml_start: # main entry point
   # main program start
	addi	r1, r0 1
	addi	r2, r0 0
	swi	r1, r3 0
	swi	r2, r3 1
	swi	r31 r3 2
	addi	r3, r3, 3
	jal	min_caml_create_array
	addi	r3, r3, -3
	lwi	r31 r3 2
	flwl	%f1 l.11783
	lwi	r2, r3 1
	swi	r1, r3 2
	fswi	f1, r3 4
	addi	r1, r2 0 #
	swi	r31 r3 5
	addi	r3, r3, 6
	jal	min_caml_create_float_array
	addi	r3, r3, -6
	lwi	r31 r3 5
	addi	r2, r0 60
	addi	r5, r4 0
	addi	r4, r4, 11
	swi	r1, r5 10
	swi	r1, r5 9
	swi	r1, r5 8
	swi	r1, r5 7
	lwi	r6, r3 1
	swi	r6, r5 6
	swi	r1, r5 5
	swi	r1, r5 4
	swi	r6, r5 3
	swi	r6, r5 2
	swi	r6, r5 1
	swi	r6, r5 0
	addi	r1, r5 0
	swi	r2, r3 5
	addi	r29, r2 0 #
	addi	r2, r1 0 #
	addi	r1, r29 0 #
	swi	r31 r3 6
	addi	r3, r3, 7
	jal	min_caml_create_array
	addi	r3, r3, -7
	lwi	r31 r3 6
	addi	r2, r0 3
	flwi	f1, r3 4 #off
	swi	r1, r3 6
	swi	r2, r3 7
	addi	r1, r2 0 #
	swi	r31 r3 8
	addi	r3, r3, 9
	jal	min_caml_create_float_array
	addi	r3, r3, -9
	lwi	r31 r3 8
	flwi	f1, r3 4 #off
	lwi	r2, r3 7
	swi	r1, r3 8
	addi	r1, r2 0 #
	swi	r31 r3 9
	addi	r3, r3, 10
	jal	min_caml_create_float_array
	addi	r3, r3, -10
	lwi	r31 r3 9
	flwi	f1, r3 4 #off
	lwi	r2, r3 7
	swi	r1, r3 9
	addi	r1, r2 0 #
	swi	r31 r3 10
	addi	r3, r3, 11
	jal	min_caml_create_float_array
	addi	r3, r3, -11
	lwi	r31 r3 10
	flwl	%f1 l.12341
	lwi	r2, r3 0
	fswi	f1, r3 10
	swi	r1, r3 11
	addi	r1, r2 0 #
	swi	r31 r3 12
	addi	r3, r3, 13
	jal	min_caml_create_float_array
	addi	r3, r3, -13
	lwi	r31 r3 12
	addi	r2, r0 50
	addi	r5, r0 -1
	lwi	r6, r3 0
	swi	r5, r3 12
	swi	r1, r3 13
	swi	r2, r3 14
	addi	r2, r5 0 #
	addi	r1, r6 0 #
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	min_caml_create_array
	addi	r3, r3, -16
	lwi	r31 r3 15
	addi	r2, r1 0 ##
	lwi	r1, r3 14
	swi	r31 r3 15
	addi	r3, r3, 16
	jal	min_caml_create_array
	addi	r3, r3, -16
	lwi	r31 r3 15
	lwi	r2, r1 0
	lwi	r5, r3 0
	swi	r1, r3 15
	addi	r1, r5 0 #
	swi	r31 r3 16
	addi	r3, r3, 17
	jal	min_caml_create_array
	addi	r3, r3, -17
	lwi	r31 r3 16
	addi	r2, r1 0 ##
	lwi	r1, r3 0
	swi	r31 r3 16
	addi	r3, r3, 17
	jal	min_caml_create_array
	addi	r3, r3, -17
	lwi	r31 r3 16
	flwi	f1, r3 4 #off
	lwi	r2, r3 0
	swi	r1, r3 16
	addi	r1, r2 0 #
	swi	r31 r3 17
	addi	r3, r3, 18
	jal	min_caml_create_float_array
	addi	r3, r3, -18
	lwi	r31 r3 17
	lwi	r2, r3 0
	lwi	r5, r3 1
	swi	r1, r3 17
	addi	r1, r2 0 #
	addi	r2, r5 0 #
	swi	r31 r3 18
	addi	r3, r3, 19
	jal	min_caml_create_array
	addi	r3, r3, -19
	lwi	r31 r3 18
	flwl	%f1 l.13169
	lwi	r2, r3 0
	fswi	f1, r3 18
	swi	r1, r3 19
	addi	r1, r2 0 #
	swi	r31 r3 20
	addi	r3, r3, 21
	jal	min_caml_create_float_array
	addi	r3, r3, -21
	lwi	r31 r3 20
	flwi	f1, r3 4 #off
	lwi	r2, r3 7
	swi	r1, r3 20
	addi	r1, r2 0 #
	swi	r31 r3 21
	addi	r3, r3, 22
	jal	min_caml_create_float_array
	addi	r3, r3, -22
	lwi	r31 r3 21
	lwi	r2, r3 0
	lwi	r5, r3 1
	swi	r1, r3 21
	addi	r1, r2 0 #
	addi	r2, r5 0 #
	swi	r31 r3 22
	addi	r3, r3, 23
	jal	min_caml_create_array
	addi	r3, r3, -23
	lwi	r31 r3 22
	flwi	f1, r3 4 #off
	lwi	r2, r3 7
	swi	r1, r3 22
	addi	r1, r2 0 #
	swi	r31 r3 23
	addi	r3, r3, 24
	jal	min_caml_create_float_array
	addi	r3, r3, -24
	lwi	r31 r3 23
	flwi	f1, r3 4 #off
	lwi	r2, r3 7
	swi	r1, r3 23
	addi	r1, r2 0 #
	swi	r31 r3 24
	addi	r3, r3, 25
	jal	min_caml_create_float_array
	addi	r3, r3, -25
	lwi	r31 r3 24
	flwi	f1, r3 4 #off
	lwi	r2, r3 7
	swi	r1, r3 24
	addi	r1, r2 0 #
	swi	r31 r3 25
	addi	r3, r3, 26
	jal	min_caml_create_float_array
	addi	r3, r3, -26
	lwi	r31 r3 25
	flwi	f1, r3 4 #off
	lwi	r2, r3 7
	swi	r1, r3 25
	addi	r1, r2 0 #
	swi	r31 r3 26
	addi	r3, r3, 27
	jal	min_caml_create_float_array
	addi	r3, r3, -27
	lwi	r31 r3 26
	addi	r2, r0 2
	lwi	r5, r3 1
	swi	r1, r3 26
	swi	r2, r3 27
	addi	r1, r2 0 #
	addi	r2, r5 0 #
	swi	r31 r3 28
	addi	r3, r3, 29
	jal	min_caml_create_array
	addi	r3, r3, -29
	lwi	r31 r3 28
	lwi	r2, r3 27
	lwi	r5, r3 1
	swi	r1, r3 28
	addi	r1, r2 0 #
	addi	r2, r5 0 #
	swi	r31 r3 29
	addi	r3, r3, 30
	jal	min_caml_create_array
	addi	r3, r3, -30
	lwi	r31 r3 29
	flwi	f1, r3 4 #off
	lwi	r2, r3 0
	swi	r1, r3 29
	addi	r1, r2 0 #
	swi	r31 r3 30
	addi	r3, r3, 31
	jal	min_caml_create_float_array
	addi	r3, r3, -31
	lwi	r31 r3 30
	flwi	f1, r3 4 #off
	lwi	r2, r3 7
	swi	r1, r3 30
	addi	r1, r2 0 #
	swi	r31 r3 31
	addi	r3, r3, 32
	jal	min_caml_create_float_array
	addi	r3, r3, -32
	lwi	r31 r3 31
	flwi	f1, r3 4 #off
	lwi	r2, r3 7
	swi	r1, r3 31
	addi	r1, r2 0 #
	swi	r31 r3 32
	addi	r3, r3, 33
	jal	min_caml_create_float_array
	addi	r3, r3, -33
	lwi	r31 r3 32
	flwi	f1, r3 4 #off
	lwi	r2, r3 7
	swi	r1, r3 32
	addi	r1, r2 0 #
	swi	r31 r3 33
	addi	r3, r3, 34
	jal	min_caml_create_float_array
	addi	r3, r3, -34
	lwi	r31 r3 33
	flwi	f1, r3 4 #off
	lwi	r2, r3 7
	swi	r1, r3 33
	addi	r1, r2 0 #
	swi	r31 r3 34
	addi	r3, r3, 35
	jal	min_caml_create_float_array
	addi	r3, r3, -35
	lwi	r31 r3 34
	flwi	f1, r3 4 #off
	lwi	r2, r3 7
	swi	r1, r3 34
	addi	r1, r2 0 #
	swi	r31 r3 35
	addi	r3, r3, 36
	jal	min_caml_create_float_array
	addi	r3, r3, -36
	lwi	r31 r3 35
	flwi	f1, r3 4 #off
	lwi	r2, r3 7
	swi	r1, r3 35
	addi	r1, r2 0 #
	swi	r31 r3 36
	addi	r3, r3, 37
	jal	min_caml_create_float_array
	addi	r3, r3, -37
	lwi	r31 r3 36
	flwi	f1, r3 4 #off
	lwi	r2, r3 1
	swi	r1, r3 36
	addi	r1, r2 0 #
	swi	r31 r3 37
	addi	r3, r3, 38
	jal	min_caml_create_float_array
	addi	r3, r3, -38
	lwi	r31 r3 37
	addi	r2, r1 0 ##
	lwi	r1, r3 1
	swi	r2, r3 37
	swi	r31 r3 38
	addi	r3, r3, 39
	jal	min_caml_create_array
	addi	r3, r3, -39
	lwi	r31 r3 38
	addi	r2, r4 0
	addi	r4, r4, 2
	swi	r1, r2 1
	lwi	r1, r3 37
	swi	r1, r2 0
	lwi	r1, r3 1
	swi	r31 r3 38
	addi	r3, r3, 39
	jal	min_caml_create_array
	addi	r3, r3, -39
	lwi	r31 r3 38
	addi	r2, r1 0 ##
	addi	r1, r0 5
	swi	r1, r3 38
	swi	r31 r3 39
	addi	r3, r3, 40
	jal	min_caml_create_array
	addi	r3, r3, -40
	lwi	r31 r3 39
	flwi	f1, r3 4 #off
	lwi	r2, r3 1
	swi	r1, r3 39
	addi	r1, r2 0 #
	swi	r31 r3 40
	addi	r3, r3, 41
	jal	min_caml_create_float_array
	addi	r3, r3, -41
	lwi	r31 r3 40
	flwi	f1, r3 4 #off
	lwi	r2, r3 7
	swi	r1, r3 40
	addi	r1, r2 0 #
	swi	r31 r3 41
	addi	r3, r3, 42
	jal	min_caml_create_float_array
	addi	r3, r3, -42
	lwi	r31 r3 41
	lwi	r2, r3 5
	lwi	r5, r3 40
	swi	r1, r3 41
	addi	r1, r2 0 #
	addi	r2, r5 0 #
	swi	r31 r3 42
	addi	r3, r3, 43
	jal	min_caml_create_array
	addi	r3, r3, -43
	lwi	r31 r3 42
	addi	r2, r4 0
	addi	r4, r4, 2
	swi	r1, r2 1
	lwi	r5, r3 41
	swi	r5, r2 0
	flwi	f1, r3 4 #off
	lwi	r6, r3 1
	swi	r1, r3 42
	swi	r2, r3 43
	addi	r1, r6 0 #
	swi	r31 r3 44
	addi	r3, r3, 45
	jal	min_caml_create_float_array
	addi	r3, r3, -45
	lwi	r31 r3 44
	addi	r2, r1 0 ##
	lwi	r1, r3 1
	swi	r2, r3 44
	swi	r31 r3 45
	addi	r3, r3, 46
	jal	min_caml_create_array
	addi	r3, r3, -46
	lwi	r31 r3 45
	addi	r2, r4 0
	addi	r4, r4, 2
	swi	r1, r2 1
	lwi	r1, r3 44
	swi	r1, r2 0
	addi	r1, r2 0
	addi	r2, r0 180
	addi	r5, r4 0
	addi	r4, r4, 3
	flwi	f1, r3 4 #off
	fswi	f1, r5 2
	swi	r1, r5 1
	lwi	r1, r3 1
	swi	r1, r5 0
	addi	r1, r2 0 #
	addi	r2, r5 0 #
	swi	r31 r3 45
	addi	r3, r3, 46
	jal	min_caml_create_array
	addi	r3, r3, -46
	lwi	r31 r3 45
	lwi	r2, r3 0
	lwi	r5, r3 1
	swi	r1, r3 45
	addi	r1, r2 0 #
	addi	r2, r5 0 #
	swi	r31 r3 46
	addi	r3, r3, 47
	jal	min_caml_create_array
	addi	r3, r3, -47
	lwi	r31 r3 46
	addi	r2, r4 0
	addi	r4, r4, 5
	closure_address	%r5 vecunit_sgn.2654
	swi	r5, r2 0
	lwi	r5, r3 27
	swi	r5, r2 4
	lwi	r6, r3 1
	swi	r6, r2 3
	lwi	r7, r3 0
	swi	r7, r2 2
	flwi	f1, r3 4 #off
	fswi	f1, r2 1
	addi	r8, r4 0
	addi	r4, r4, 4
	closure_address	%r9 vecaccumv.2678
	swi	r9, r8 0
	swi	r5, r8 3
	swi	r6, r8 2
	swi	r7, r8 1
	addi	r9, r4 0
	addi	r4, r4, 10
	closure_address	%r10 read_screen_settings.2755
	swi	r10, r9 0
	lwi	r10, r3 9
	swi	r10, r9 9
	lwi	r11, r3 35
	swi	r11, r9 8
	lwi	r12, r3 34
	swi	r12, r9 7
	lwi	r13, r3 33
	swi	r13, r9 6
	lwi	r14, r3 8
	swi	r14, r9 5
	swi	r5, r9 4
	swi	r6, r9 3
	swi	r7, r9 2
	fswi	f1, r9 1
	addi	r14, r4 0
	addi	r4, r4, 7
	closure_address	%r15 read_light.2757
	swi	r15, r14 0
	lwi	r15, r3 11
	swi	r15, r14 6
	lwi	r16, r3 13
	swi	r16, r14 5
	swi	r5, r14 4
	swi	r6, r14 3
	swi	r7, r14 2
	fswi	f1, r14 1
	addi	r17, r4 0
	addi	r4, r4, 5
	closure_address	%r18 rotate_quadratic_matrix.2759
	swi	r18, r17 0
	swi	r5, r17 4
	swi	r6, r17 3
	swi	r7, r17 2
	fswi	f1, r17 1
	addi	r18, r4 0
	addi	r4, r4, 10
	closure_address	%r19 read_nth_object.2762
	swi	r19, r18 0
	swi	r2, r18 9
	swi	r17, r18 8
	lwi	r17, r3 6
	swi	r17, r18 7
	swi	r5, r18 6
	lwi	r19, r3 12
	swi	r19, r18 5
	lwi	r20, r3 7
	swi	r20, r18 4
	swi	r6, r18 3
	swi	r7, r18 2
	fswi	f1, r18 1
	addi	r21, r4 0
	addi	r4, r4, 6
	closure_address	%r22 read_object.2764
	swi	r22, r21 0
	swi	r18, r21 5
	lwi	r22, r3 2
	swi	r22, r21 4
	lwi	r23, r3 5
	swi	r23, r21 3
	swi	r6, r21 2
	swi	r7, r21 1
	addi	r23, r4 0
	addi	r4, r4, 4
	closure_address	%r24 read_net_item.2768
	swi	r24, r23 0
	swi	r19, r23 3
	swi	r6, r23 2
	swi	r7, r23 1
	addi	r24, r4 0
	addi	r4, r4, 5
	closure_address	%r25 read_or_network.2770
	swi	r25, r24 0
	swi	r23, r24 4
	swi	r19, r24 3
	swi	r6, r24 2
	swi	r7, r24 1
	addi	r25, r4 0
	addi	r4, r4, 6
	closure_address	%r26 read_and_network.2772
	swi	r26, r25 0
	swi	r23, r25 5
	lwi	r23, r3 15
	swi	r23, r25 4
	swi	r19, r25 3
	swi	r6, r25 2
	swi	r7, r25 1
	addi	r26, r4 0
	addi	r4, r4, 4
	closure_address	%r27 solver_rect_surface.2776
	swi	r27, r26 0
	lwi	r27, r3 17
	swi	r27, r26 3
	swi	r6, r26 2
	fswi	f1, r26 1
	swi	r25, r3 46
	addi	r25, r4 0
	addi	r4, r4, 2
	swi	r14, r3 47
	closure_address	%r14 quadratic.2797
	swi	r14, r25 0
	swi	r6, r25 1
	addi	r14, r4 0
	addi	r4, r4, 2
	swi	r18, r3 48
	closure_address	%r18 bilinear.2802
	swi	r18, r14 0
	swi	r6, r14 1
	addi	r18, r4 0
	addi	r4, r4, 9
	swi	r21, r3 49
	closure_address	%r21 solver_second.2810
	swi	r21, r18 0
	swi	r27, r18 8
	swi	r25, r18 7
	swi	r14, r18 6
	swi	r5, r18 5
	swi	r20, r18 4
	swi	r6, r18 3
	swi	r7, r18 2
	fswi	f1, r18 1
	addi	r14, r4 0
	addi	r4, r4, 8
	closure_address	%r21 solver.2816
	swi	r21, r14 0
	swi	r18, r14 7
	swi	r26, r14 6
	swi	r27, r14 5
	swi	r17, r14 4
	swi	r5, r14 3
	swi	r6, r14 2
	swi	r7, r14 1
	addi	r18, r4 0
	addi	r4, r4, 8
	closure_address	%r21 solver_rect_fast.2820
	swi	r21, r18 0
	swi	r27, r18 7
	lwi	r21, r3 38
	swi	r21, r18 6
	swi	r5, r18 5
	swi	r20, r18 4
	swi	r6, r18 3
	swi	r7, r18 2
	fswi	f1, r18 1
	addi	r26, r4 0
	addi	r4, r4, 8
	swi	r24, r3 50
	closure_address	%r24 solver_second_fast.2833
	swi	r24, r26 0
	swi	r27, r26 7
	swi	r25, r26 6
	swi	r5, r26 5
	swi	r20, r26 4
	swi	r6, r26 3
	swi	r7, r26 2
	fswi	f1, r26 1
	addi	r24, r4 0
	addi	r4, r4, 10
	swi	r9, r3 51
	closure_address	%r9 solver_fast.2839
	swi	r9, r24 0
	swi	r26, r24 9
	swi	r18, r24 8
	swi	r27, r24 7
	swi	r17, r24 6
	swi	r5, r24 5
	swi	r20, r24 4
	swi	r6, r24 3
	swi	r7, r24 2
	fswi	f1, r24 1
	addi	r9, r4 0
	addi	r4, r4, 7
	closure_address	%r26 solver_second_fast2.2850
	swi	r26, r9 0
	swi	r27, r9 6
	swi	r5, r9 5
	swi	r20, r9 4
	swi	r6, r9 3
	swi	r7, r9 2
	fswi	f1, r9 1
	addi	r26, r4 0
	addi	r4, r4, 9
	closure_address	%r12 solver_fast2.2857
	swi	r12, r26 0
	swi	r9, r26 8
	swi	r18, r26 7
	swi	r27, r26 6
	swi	r17, r26 5
	swi	r5, r26 4
	swi	r20, r26 3
	swi	r6, r26 2
	swi	r7, r26 1
	addi	r9, r4 0
	addi	r4, r4, 7
	closure_address	%r12 setup_rect_table.2860
	swi	r12, r9 0
	swi	r21, r9 6
	swi	r5, r9 5
	swi	r20, r9 4
	swi	r6, r9 3
	swi	r7, r9 2
	fswi	f1, r9 1
	addi	r12, r4 0
	addi	r4, r4, 6
	closure_address	%r18 setup_surface_table.2863
	swi	r18, r12 0
	swi	r5, r12 5
	swi	r20, r12 4
	swi	r6, r12 3
	swi	r7, r12 2
	fswi	f1, r12 1
	addi	r18, r4 0
	addi	r4, r4, 8
	closure_address	%r11 setup_second_table.2866
	swi	r11, r18 0
	swi	r25, r18 7
	swi	r21, r18 6
	swi	r5, r18 5
	swi	r20, r18 4
	swi	r6, r18 3
	swi	r7, r18 2
	fswi	f1, r18 1
	addi	r11, r4 0
	addi	r4, r4, 8
	closure_address	%r21 iter_setup_dirvec_constants.2869
	swi	r21, r11 0
	swi	r12, r11 7
	swi	r18, r11 6
	swi	r9, r11 5
	swi	r17, r11 4
	swi	r5, r11 3
	swi	r6, r11 2
	swi	r7, r11 1
	addi	r21, r4 0
	addi	r4, r4, 7
	swi	r11, r3 52
	closure_address	%r11 setup_startp_constants.2874
	swi	r11, r21 0
	swi	r25, r21 6
	swi	r17, r21 5
	swi	r5, r21 4
	swi	r20, r21 3
	swi	r6, r21 2
	swi	r7, r21 1
	addi	r11, r4 0
	addi	r4, r4, 3
	swi	r9, r3 53
	closure_address	%r9 is_rect_outside.2879
	swi	r9, r11 0
	swi	r6, r11 2
	fswi	f1, r11 1
	addi	r9, r4 0
	addi	r4, r4, 5
	swi	r18, r3 54
	closure_address	%r18 is_plane_outside.2884
	swi	r18, r9 0
	swi	r5, r9 4
	swi	r6, r9 3
	swi	r7, r9 2
	fswi	f1, r9 1
	addi	r18, r4 0
	addi	r4, r4, 7
	swi	r12, r3 55
	closure_address	%r12 is_outside.2894
	swi	r12, r18 0
	swi	r25, r18 6
	swi	r11, r18 5
	swi	r5, r18 4
	swi	r20, r18 3
	swi	r6, r18 2
	swi	r7, r18 1
	addi	r12, r4 0
	addi	r4, r4, 12
	closure_address	%r13 check_all_inside.2899
	swi	r13, r12 0
	swi	r25, r12 11
	swi	r17, r12 10
	swi	r11, r12 9
	swi	r9, r12 8
	swi	r18, r12 7
	swi	r5, r12 6
	swi	r19, r12 5
	swi	r20, r12 4
	swi	r6, r12 3
	swi	r7, r12 2
	fswi	f1, r12 1
	addi	r9, r4 0
	addi	r4, r4, 13
	closure_address	%r11 shadow_check_and_group.2905
	swi	r11, r9 0
	swi	r24, r9 12
	swi	r27, r9 11
	swi	r17, r9 10
	lwi	r11, r3 43
	swi	r11, r9 9
	swi	r15, r9 8
	swi	r18, r9 7
	lwi	r13, r3 21
	swi	r13, r9 6
	swi	r12, r9 5
	swi	r5, r9 4
	swi	r19, r9 3
	swi	r6, r9 2
	swi	r7, r9 1
	addi	r25, r4 0
	addi	r4, r4, 6
	closure_address	%r10 shadow_check_one_or_group.2908
	swi	r10, r25 0
	swi	r9, r25 5
	swi	r23, r25 4
	swi	r19, r25 3
	swi	r6, r25 2
	swi	r7, r25 1
	addi	r10, r4 0
	addi	r4, r4, 13
	swi	r8, r3 56
	closure_address	%r8 shadow_check_one_or_matrix.2911
	swi	r8, r10 0
	swi	r24, r10 12
	swi	r27, r10 11
	swi	r25, r10 10
	swi	r9, r10 9
	swi	r11, r10 8
	swi	r13, r10 7
	swi	r23, r10 6
	swi	r5, r10 5
	swi	r19, r10 4
	swi	r20, r10 3
	swi	r6, r10 2
	swi	r7, r10 1
	addi	r8, r4 0
	addi	r4, r4, 16
	closure_address	%r9 solve_each_element.2914
	swi	r9, r8 0
	lwi	r9, r3 20
	swi	r9, r8 15
	lwi	r24, r3 31
	swi	r24, r8 14
	swi	r27, r8 13
	swi	r14, r8 12
	swi	r17, r8 11
	swi	r18, r8 10
	lwi	r25, r3 19
	swi	r25, r8 9
	swi	r13, r8 8
	lwi	r11, r3 22
	swi	r11, r8 7
	swi	r12, r8 6
	swi	r5, r8 5
	swi	r19, r8 4
	swi	r6, r8 3
	swi	r7, r8 2
	fswi	f1, r8 1
	addi	r16, r4 0
	addi	r4, r4, 6
	closure_address	%r15 solve_one_or_network.2918
	swi	r15, r16 0
	swi	r8, r16 5
	swi	r23, r16 4
	swi	r19, r16 3
	swi	r6, r16 2
	swi	r7, r16 1
	addi	r15, r4 0
	addi	r4, r4, 13
	closure_address	%r22 trace_or_matrix.2922
	swi	r22, r15 0
	swi	r9, r15 12
	swi	r24, r15 11
	swi	r27, r15 10
	swi	r14, r15 9
	swi	r16, r15 8
	swi	r8, r15 7
	swi	r23, r15 6
	swi	r5, r15 5
	swi	r19, r15 4
	swi	r20, r15 3
	swi	r6, r15 2
	swi	r7, r15 1
	addi	r8, r4 0
	addi	r4, r4, 16
	closure_address	%r14 solve_each_element_fast.2928
	swi	r14, r8 0
	swi	r9, r8 15
	lwi	r14, r3 32
	swi	r14, r8 14
	swi	r26, r8 13
	swi	r27, r8 12
	swi	r17, r8 11
	swi	r18, r8 10
	swi	r25, r8 9
	swi	r13, r8 8
	swi	r11, r8 7
	swi	r12, r8 6
	swi	r5, r8 5
	swi	r19, r8 4
	swi	r6, r8 3
	swi	r7, r8 2
	fswi	f1, r8 1
	addi	r12, r4 0
	addi	r4, r4, 6
	closure_address	%r16 solve_one_or_network_fast.2932
	swi	r16, r12 0
	swi	r8, r12 5
	swi	r23, r12 4
	swi	r19, r12 3
	swi	r6, r12 2
	swi	r7, r12 1
	addi	r16, r4 0
	addi	r4, r4, 12
	closure_address	%r18 trace_or_matrix_fast.2936
	swi	r18, r16 0
	swi	r9, r16 11
	swi	r26, r16 10
	swi	r27, r16 9
	swi	r12, r16 8
	swi	r8, r16 7
	swi	r23, r16 6
	swi	r5, r16 5
	swi	r19, r16 4
	swi	r20, r16 3
	swi	r6, r16 2
	swi	r7, r16 1
	addi	r8, r4 0
	addi	r4, r4, 7
	closure_address	%r12 get_nvector_second.2946
	swi	r12, r8 0
	swi	r2, r8 6
	lwi	r12, r3 23
	swi	r12, r8 5
	swi	r13, r8 4
	swi	r5, r8 3
	swi	r6, r8 2
	swi	r7, r8 1
	addi	r18, r4 0
	addi	r4, r4, 8
	closure_address	%r22 utexture.2951
	swi	r22, r18 0
	lwi	r22, r3 24
	swi	r22, r18 7
	swi	r5, r18 6
	swi	r20, r18 5
	swi	r6, r18 4
	swi	r7, r18 3
	flwi	f2, r3 10 #off
	fswi	f2, r18 2
	fswi	f1, r18 1
	addi	r23, r4 0
	addi	r4, r4, 7
	closure_address	%r26 add_light.2954
	swi	r26, r23 0
	swi	r22, r23 6
	lwi	r26, r3 26
	swi	r26, r23 5
	swi	r5, r23 4
	swi	r6, r23 3
	swi	r7, r23 2
	fswi	f1, r23 1
	addi	r27, r4 0
	addi	r4, r4, 14
	swi	r2, r3 57
	closure_address	%r2 trace_reflections.2958
	swi	r2, r27 0
	swi	r16, r27 13
	swi	r9, r27 12
	swi	r10, r27 11
	lwi	r2, r3 45
	swi	r2, r27 10
	lwi	r2, r3 16
	swi	r2, r27 9
	swi	r12, r27 8
	swi	r25, r27 7
	swi	r11, r27 6
	swi	r23, r27 5
	swi	r5, r27 4
	swi	r6, r27 3
	swi	r7, r27 2
	flwi	f2, r3 18 #off
	fswi	f2, r27 1
	addi	r20, r4 0
	addi	r4, r4, 29
	swi	r16, r3 58
	closure_address	%r16 trace_ray.2963
	swi	r16, r20 0
	swi	r18, r20 28
	swi	r27, r20 27
	swi	r15, r20 26
	swi	r9, r20 25
	swi	r22, r20 24
	swi	r14, r20 23
	swi	r24, r20 22
	swi	r10, r20 21
	swi	r21, r20 20
	swi	r26, r20 19
	swi	r2, r20 18
	swi	r17, r20 17
	swi	r12, r20 16
	swi	r1, r20 15
	lwi	r15, r3 2
	swi	r15, r20 14
	lwi	r16, r3 11
	swi	r16, r20 13
	swi	r25, r20 12
	swi	r13, r20 11
	swi	r11, r20 10
	swi	r8, r20 9
	lwi	r27, r3 13
	swi	r27, r20 8
	swi	r23, r20 7
	swi	r5, r20 6
	swi	r19, r20 5
	swi	r6, r20 4
	swi	r7, r20 3
	fswi	f2, r20 2
	fswi	f1, r20 1
	addi	r19, r4 0
	addi	r4, r4, 20
	closure_address	%r23 trace_diffuse_ray.2969
	swi	r23, r19 0
	swi	r18, r19 19
	lwi	r18, r3 58
	swi	r18, r19 18
	swi	r9, r19 17
	swi	r22, r19 16
	swi	r10, r19 15
	swi	r2, r19 14
	swi	r17, r19 13
	swi	r12, r19 12
	swi	r16, r19 11
	swi	r25, r19 10
	swi	r13, r19 9
	swi	r11, r19 8
	swi	r8, r19 7
	lwi	r8, r3 25
	swi	r8, r19 6
	swi	r5, r19 5
	swi	r6, r19 4
	swi	r7, r19 3
	fswi	f2, r19 2
	fswi	f1, r19 1
	addi	r9, r4 0
	addi	r4, r4, 6
	closure_address	%r10 iter_trace_diffuse_rays.2972
	swi	r10, r9 0
	swi	r19, r9 5
	swi	r5, r9 4
	swi	r6, r9 3
	swi	r7, r9 2
	fswi	f1, r9 1
	addi	r10, r4 0
	addi	r4, r4, 10
	closure_address	%r11 trace_diffuse_ray_80percent.2981
	swi	r11, r10 0
	swi	r14, r10 9
	swi	r21, r10 8
	swi	r15, r10 7
	swi	r9, r10 6
	lwi	r11, r3 39
	swi	r11, r10 5
	swi	r5, r10 4
	lwi	r12, r3 7
	swi	r12, r10 3
	swi	r6, r10 2
	swi	r7, r10 1
	addi	r13, r4 0
	addi	r4, r4, 8
	closure_address	%r18 calc_diffuse_using_1point.2985
	swi	r18, r13 0
	lwi	r18, r3 56
	swi	r18, r13 7
	swi	r10, r13 6
	swi	r26, r13 5
	swi	r8, r13 4
	swi	r5, r13 3
	swi	r6, r13 2
	swi	r7, r13 1
	addi	r19, r4 0
	addi	r4, r4, 7
	closure_address	%r22 calc_diffuse_using_5points.2988
	swi	r22, r19 0
	swi	r18, r19 6
	swi	r26, r19 5
	swi	r8, r19 4
	swi	r5, r19 3
	swi	r6, r19 2
	swi	r7, r19 1
	addi	r22, r4 0
	addi	r4, r4, 9
	closure_address	%r23 do_without_neighbors.2994
	swi	r23, r22 0
	swi	r18, r22 8
	swi	r10, r22 7
	swi	r26, r22 6
	swi	r8, r22 5
	swi	r13, r22 4
	swi	r5, r22 3
	swi	r6, r22 2
	swi	r7, r22 1
	addi	r10, r4 0
	addi	r4, r4, 2
	closure_address	%r18 neighbors_are_available.3004
	swi	r18, r10 0
	swi	r7, r10 1
	addi	r18, r4 0
	addi	r4, r4, 7
	closure_address	%r23 try_exploit_neighbors.3010
	swi	r23, r18 0
	swi	r10, r18 6
	swi	r22, r18 5
	swi	r19, r18 4
	swi	r13, r18 3
	swi	r6, r18 2
	swi	r7, r18 1
	addi	r10, r4 0
	addi	r4, r4, 4
	closure_address	%r19 write_ppm_header.3017
	swi	r19, r10 0
	lwi	r19, r3 28
	swi	r19, r10 3
	swi	r6, r10 2
	swi	r7, r10 1
	addi	r23, r4 0
	addi	r4, r4, 11
	closure_address	%r25 pretrace_diffuse_rays.3023
	swi	r25, r23 0
	swi	r14, r23 10
	swi	r21, r23 9
	swi	r15, r23 8
	swi	r9, r23 7
	swi	r11, r23 6
	swi	r8, r23 5
	swi	r5, r23 4
	swi	r6, r23 3
	swi	r7, r23 2
	fswi	f1, r23 1
	addi	r8, r4 0
	addi	r4, r4, 16
	closure_address	%r9 pretrace_pixels.3026
	swi	r9, r8 0
	lwi	r9, r3 9
	swi	r9, r8 15
	lwi	r9, r3 57
	swi	r9, r8 14
	swi	r20, r8 13
	swi	r24, r8 12
	lwi	r9, r3 33
	swi	r9, r8 11
	lwi	r9, r3 30
	swi	r9, r8 10
	swi	r26, r8 9
	lwi	r14, r3 36
	swi	r14, r8 8
	swi	r23, r8 7
	lwi	r14, r3 29
	swi	r14, r8 6
	lwi	r20, r3 38
	swi	r20, r8 5
	swi	r5, r8 4
	swi	r6, r8 3
	swi	r7, r8 2
	fswi	f1, r8 1
	addi	r21, r4 0
	addi	r4, r4, 10
	closure_address	%r23 pretrace_line.3033
	swi	r23, r21 0
	lwi	r23, r3 35
	swi	r23, r21 9
	lwi	r23, r3 34
	swi	r23, r21 8
	swi	r9, r21 7
	swi	r8, r21 6
	swi	r19, r21 5
	swi	r14, r21 4
	swi	r5, r21 3
	swi	r6, r21 2
	swi	r7, r21 1
	addi	r8, r4 0
	addi	r4, r4, 9
	closure_address	%r23 scan_pixel.3037
	swi	r23, r8 0
	swi	r18, r8 8
	swi	r26, r8 7
	swi	r19, r8 6
	swi	r22, r8 5
	swi	r13, r8 4
	swi	r5, r8 3
	swi	r6, r8 2
	swi	r7, r8 1
	addi	r13, r4 0
	addi	r4, r4, 8
	closure_address	%r18 scan_line.3043
	swi	r18, r13 0
	swi	r8, r13 7
	swi	r21, r13 6
	swi	r19, r13 5
	swi	r20, r13 4
	swi	r5, r13 3
	swi	r6, r13 2
	swi	r7, r13 1
	addi	r18, r4 0
	addi	r4, r4, 7
	closure_address	%r22 create_pixel.3051
	swi	r22, r18 0
	swi	r20, r18 6
	swi	r5, r18 5
	swi	r12, r18 4
	swi	r6, r18 3
	swi	r7, r18 2
	fswi	f1, r18 1
	addi	r22, r4 0
	addi	r4, r4, 8
	closure_address	%r23 init_line_elements.3053
	swi	r23, r22 0
	swi	r18, r22 7
	swi	r20, r22 6
	swi	r5, r22 5
	swi	r12, r22 4
	swi	r6, r22 3
	swi	r7, r22 2
	fswi	f1, r22 1
	addi	r23, r4 0
	addi	r4, r4, 7
	closure_address	%r24 calc_dirvec.3063
	swi	r24, r23 0
	swi	r11, r23 6
	swi	r20, r23 5
	swi	r5, r23 4
	swi	r6, r23 3
	swi	r7, r23 2
	fswi	f1, r23 1
	addi	r24, r4 0
	addi	r4, r4, 7
	closure_address	%r25 calc_dirvecs.3071
	swi	r25, r24 0
	swi	r23, r24 6
	swi	r20, r24 5
	swi	r5, r24 4
	swi	r6, r24 3
	swi	r7, r24 2
	fswi	f1, r24 1
	addi	r23, r4 0
	addi	r4, r4, 6
	closure_address	%r25 calc_dirvec_rows.3076
	swi	r25, r23 0
	swi	r24, r23 5
	swi	r20, r23 4
	swi	r5, r23 3
	swi	r6, r23 2
	swi	r7, r23 1
	addi	r25, r4 0
	addi	r4, r4, 6
	closure_address	%r26 create_dirvec_elements.3082
	swi	r26, r25 0
	swi	r15, r25 5
	swi	r12, r25 4
	swi	r6, r25 3
	swi	r7, r25 2
	fswi	f1, r25 1
	addi	r26, r4 0
	addi	r4, r4, 8
	closure_address	%r27 create_dirvecs.3085
	swi	r27, r26 0
	swi	r15, r26 7
	swi	r11, r26 6
	swi	r25, r26 5
	swi	r12, r26 4
	swi	r6, r26 3
	swi	r7, r26 2
	fswi	f1, r26 1
	addi	r25, r4 0
	addi	r4, r4, 10
	closure_address	%r27 init_dirvec_constants.3087
	swi	r27, r25 0
	lwi	r27, r3 55
	swi	r27, r25 9
	lwi	r20, r3 54
	swi	r20, r25 8
	swi	r23, r3 59
	lwi	r23, r3 53
	swi	r23, r25 7
	swi	r17, r25 6
	swi	r15, r25 5
	swi	r24, r3 60
	lwi	r24, r3 52
	swi	r24, r25 4
	swi	r5, r25 3
	swi	r6, r25 2
	swi	r7, r25 1
	swi	r26, r3 61
	addi	r26, r4 0
	addi	r4, r4, 12
	swi	r18, r3 62
	closure_address	%r18 init_vecset_constants.3090
	swi	r18, r26 0
	swi	r27, r26 11
	swi	r20, r26 10
	swi	r23, r26 9
	swi	r17, r26 8
	swi	r15, r26 7
	swi	r24, r26 6
	swi	r25, r26 5
	swi	r11, r26 4
	swi	r5, r26 3
	swi	r6, r26 2
	swi	r7, r26 1
	addi	r18, r4 0
	addi	r4, r4, 11
	closure_address	%r11 setup_rect_reflection.3101
	swi	r11, r18 0
	lwi	r11, r3 45
	swi	r11, r18 10
	swi	r1, r18 9
	swi	r15, r18 8
	swi	r16, r18 7
	swi	r24, r18 6
	swi	r5, r18 5
	swi	r12, r18 4
	swi	r6, r18 3
	swi	r7, r18 2
	fswi	f1, r18 1
	addi	r14, r4 0
	addi	r4, r4, 11
	closure_address	%r19 setup_surface_reflection.3104
	swi	r19, r14 0
	swi	r11, r14 10
	swi	r1, r14 9
	swi	r15, r14 8
	swi	r16, r14 7
	swi	r24, r14 6
	swi	r5, r14 5
	swi	r12, r14 4
	swi	r6, r14 3
	swi	r7, r14 2
	fswi	f1, r14 1
	addi	r28, r4 0
	addi	r4, r4, 41
	closure_address	%r1 rt.3109
	swi	r1, r28 0
	swi	r10, r28 40
	lwi	r1, r3 41
	swi	r1, r28 39
	swi	r27, r28 38
	swi	r14, r28 37
	swi	r20, r28 36
	swi	r23, r28 35
	swi	r18, r28 34
	swi	r8, r28 33
	swi	r9, r28 32
	swi	r13, r28 31
	lwi	r1, r3 51
	swi	r1, r28 30
	lwi	r1, r3 50
	swi	r1, r28 29
	lwi	r1, r3 49
	swi	r1, r28 28
	lwi	r1, r3 48
	swi	r1, r28 27
	lwi	r1, r3 47
	swi	r1, r28 26
	lwi	r1, r3 46
	swi	r1, r28 25
	swi	r21, r28 24
	swi	r2, r28 23
	swi	r17, r28 22
	swi	r15, r28 21
	lwi	r1, r3 43
	swi	r1, r28 20
	swi	r16, r28 19
	swi	r24, r28 18
	swi	r26, r28 17
	swi	r22, r28 16
	swi	r25, r28 15
	lwi	r1, r3 28
	swi	r1, r28 14
	lwi	r1, r3 29
	swi	r1, r28 13
	lwi	r1, r3 39
	swi	r1, r28 12
	lwi	r1, r3 62
	swi	r1, r28 11
	lwi	r1, r3 61
	swi	r1, r28 10
	lwi	r1, r3 42
	swi	r1, r28 9
	lwi	r1, r3 60
	swi	r1, r28 8
	lwi	r1, r3 59
	swi	r1, r28 7
	lwi	r1, r3 38
	swi	r1, r28 6
	swi	r5, r28 5
	swi	r12, r28 4
	swi	r6, r28 3
	swi	r7, r28 2
	fswi	f1, r28 1
	addi	r1, r0 128
	addi	r2, r1 0 #
	swi	r31 r3 63
	addi	r3, r3, 64
	lwi	%r29, r28 0
	closure_jump	%r29
	addi	r3, r3, -64
	lwi	r31, r3 63
	jl	AsmToBinStopLabel
   # main program end

min_caml_read:
	read r1
	jr   r31
	
	

#### f1 ####################################
#### r1 : loop counter , r2 base value  ####
#### r1 : previous heap pointer  ###########
#### use r28 ,r29               ############

 
min_caml_create_array:	
	addi	r29	r1	0
	addi	r1	r4	0
create_array_loop:
	seq		r28	r29	r0
	seq		r28 r28 r0
	jeql	r28 r0	create_array_exit
	swi		r2	r4	0
	addi	r29	r29	-1
	addi	r4	r4	1 #?
	jl		create_array_loop
create_array_exit:
	jr	r31



min_caml_create_float_array:
	addi	r29	r1	0
	addi	r1	r4	0
create_float_array_loop:
	seq		r28	r29	r0
	seq		r28	r28	r0
	jeql	r28	r0	create_float_array_exit
	fswi	f1  r4 0 # ?
	addi	r29	r29	-1
	addi	r4	r4	1 # ?
	jl	create_float_array_loop
create_float_array_exit:
	jr	r31
	


min_caml_print_int_sub:
	addi  r1 r1 48
	write r1
	jr r31
	


min_caml_floor:
	swi	r1, r3, 24
	flt	r26, f1, f0
	swi	r26, r3, 8
	jeql	r26, r0, min_caml_floor_plus
	fsub	f1, f0, f1	
min_caml_floor_plus:
	addi	r26, r0, 16384
	addi	r27, r0, 512
	mul	r26, r26, r27		#0x00800000	
	addi	r27, r0, 150
	mul	r27, r26, r27		#0x4b000000
	swi	r27, r3, 12
	flwi	f27, r3, 12
	fswi	f1, r3, 16
	flt	r29, f1, f27
	jeql	r29, r0, min_caml_floor_jump
	swi	r31, r3, 4
	addi	r3, r3, 28
	jal	min_caml_ftoi
	jal	min_caml_itof
	addi	r3, r3, -28
	lwi	r31, r3, 4
	flwi	f26, r3, 16
	flt	r29, f26, f1	#if(元のf < floor(f)) then r29 = 1 else 0
min_caml_floor_jump:
	addi	r26, r0, 1016
	addi	r27, r0, 4096
	mul	r26, r26, r27
	addi	r27, r0, 256
	mul	r26, r26, r27		#0x3f800000(1.0)
	swi	r26, r3, 20
	flwi	f27, r3, 20
	jeql	r29, r0, min_caml_floor_end
	fsub	f1, f1, f27	
min_caml_floor_end:
	lwi	r26, r3, 8
	jeql	r26, r0, min_caml_floor_plus_end
	flwi	f26, r3, 16
	feq	r29, f1, f26
	addi	r28, r0, 1
	jeql	r29, r28, min_caml_floor_minus_end
	fadd	f1, f1, f27
min_caml_floor_minus_end:	
	fsub	f1, f0, f1
min_caml_floor_plus_end:
	lwi	r1, r3, 24	
	jr	r31
	
	


min_caml_ftoi_sub:
	fswi	f2, r3, 4
min_caml_ftoi_large:	
	flt	r27, f2, f1
	jeql	r27, r0, min_caml_ftoi_small
	fsub	f1, f1, f2
	addi	r26, r26, 1
	jl	min_caml_ftoi_large
min_caml_ftoi_small:
	fadd	f26, f1, f2
	addi	r2, r26, 0
	fswi	f26, r3, 8
	lwi	r28, r3, 8
	lwi	r27, r3, 4
	sub	r1, r28, r27
	jr	r31
min_caml_ftoi_zero_end:
	flwi	f1, r3, 8
	lwi	r2, r3, 24
	flwi	f2, r3, 28
	addi	r1, r0, 0
	jr	r31
min_caml_ftoi:
min_caml_int_of_float:
	swi	r2, r3, 24
	fswi	f2, r3, 28
	fswi	f1, r3, 8
	lwi	r29, r3, 8
	slt	r26, r29, r0		#if(r26 == 0) then + else -
	swi	r26, r3, 12
	jeql	r26, r0, min_caml_ftoi_plus
	fsub	f1, f0, f1
	sub	r29, r0, r29
min_caml_ftoi_plus:	
	jeql	r29, r0, min_caml_ftoi_zero_end	#if(f == 0)
	addi	r26, r0, 16384
	addi	r27, r0, 512
	mul	r26, r26, r27	#0x00800000
	swi	r26, r3, 16
	addi	r27, r0, 150
	mul	r27, r26, r27	#0x4b000000(8388608.0)
	swi	r27, r3, 20
	addi	r26, r0, 20224		##0x00004f00
	addi	r27, r0, 256
	mul	r26, r26, r27		##0x004f0000
	mul	r26, r26, r27		# 0x4f000000
	slt	r28, r29, r26
	jeql	r28, r0, min_caml_ftoi_zero_end	#if(i >= 0x4f000000)
	flwi	f2, r3, 20		#0x4b000000
	swi	r31, r3, 4
	addi	r3, r3, 32
	jal	min_caml_ftoi_sub	#(f1,f2)=(f,8388608.0)を渡し,(r1,r2)=(ftoi(fn),m)を返す.  #f=8388608.0*m+fn
	addi	r3, r3, -32
	lwi	r31, r3, 4
	lwi	r26, r3, 16
	mul	r27, r26, r2
	add	r1, r1, r27
	lwi	r28, r3, 12
	jeql	r28, r0, min_caml_ftoi_end
	sub	r1, r0, r1
min_caml_ftoi_end:
	flwi	f1, r3, 8
	lwi	r2, r3, 24
	flwi	f2, r3, 28
	jr	r31
	
	
	


min_caml_print_char:
	write r1
	jr    r31
	
	
	


min_caml_itof_small:
	add	r26, r1, r2
	swi	r2, r3, 4
	swi	r26, r3, 8
	flwi	f26, r3, 4
	flwi	f27, r3, 8
	fsub	f1, f27, f26
	jr	r31
min_caml_itof_large:
	div	r26, r1, r2	#m
	mul	r27, r26, r2
	sub	r27, r1, r27	#n
	swi	r5, r3, 4
	flwi	f26, r3, 4
min_caml_itof_large_sub:
	jeql	r26, r0, min_caml_itof_large_exit
	fadd	f27, f27, f26
	addi	r26, r26, -1
	jl	min_caml_itof_large_sub	
min_caml_itof_large_exit:
	fswi	f27, r3, 8 
	addi	r1, r27, 0
	lwi	r2, r3, 4
	swi	r31, r3, 0
	addi	r3, r3, 12
	jal	min_caml_itof_small
	addi	r3, r3, -12
	lwi	r31, r3, 0
	flwi	f27, r3, 8
	fadd	f1, f1, f27
	jr 	r31
min_caml_itof_zero:
	fadd	f1, f0, f0
	jr 	r31
	
min_caml_itof:	
min_caml_float_of_int:
	swi	r1, r3, 8
	swi	r2, r3, 12
	swi	r5, r3, 16
	jeql	r1, r0, min_caml_itof_zero
	slt	r26, r1, r0	#if(r26 == 0) then + else -
	swi	r26, r3, 20
	jeql	r26, r0, min_caml_itof_plus
	sub	r1, r0, r1
min_caml_itof_plus:	
	addi	r26, r0, 16384	
	addi	r27, r0, 512
	mul	r26, r26, r27	#8388608(0x00800000)	
	addi	r27, r0, 150
	mul	r27, r26, r27	#0x4b000000(8388608.0)
	slt	r28, r26, r1
	addi	r29, r0, 1
	addi	r2, r27, 0
	jeql	r28, r29, min_caml_itof_golarge
	swi	r31, r3, 4
	addi	r3, r3, 24
	jal	min_caml_itof_small	#if(i < 8388608)((r1,r2)=(i,0x4b000000))
	addi	r3, r3, -24
	lwi	r31, r3, 4
	jl	min_caml_itof_last
min_caml_itof_golarge:
	addi	r2, r26, 0
	addi	r5, r27, 0
	swi	r31, r3, 4
	addi	r3, r3, 24
	jal	min_caml_itof_large	#else((r1,r2,r5)=(i,0x00800000,0x4b000000))
	addi	r3, r3, -24
	lwi	r31, r3, 4
min_caml_itof_last:
	lwi	r26, r3, 20
	jeql	r26, r0, min_caml_itof_end
	fsub	f1, f0, f1
min_caml_itof_end:
	lwi	r1, r3, 8
	lwi	r2, r3, 12
	lwi	r5, r3, 16
	jr 	r31
	
	


min_caml_sqrt:
	sqrt f1 f1
	jr r31
