(* 自由変数のある再帰関数 *)
let x = 10 in
let rec f y = x + y in
print_int (f 1)
