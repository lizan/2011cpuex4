	addi	%r4 %r4 10000
	jl	_min_caml_start
f.4:
	lwi	r5, r24 4
	add	r2, r5, r2
	jr	r31
_min_caml_start: # main entry point
   # main program start
	addi	r2, r0 10
	addi	r24, r4 0
	addi	r4, r4, 8
	closure_address	%r5 f.4
	swi	r5, r24 0
	swi	r2, r24 4
	addi	r2, r0 1
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	lwi	r30, r24 0
	closure_jump	r30
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_print_int
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
   # main program end
