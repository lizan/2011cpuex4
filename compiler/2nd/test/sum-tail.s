	addi	%r4 %r4 10000
	jl	_min_caml_start
sum.8:
	addi	r28, r0, 0
	sgt	r29, r5, r28
	seq	r29, r29, r0
	jeql	r29, r0, ble_else.19
	jr	r31
ble_else.19:
	add	r2, r2, r5
	addi	r5, r5, -1
	jl	sum.8
_min_caml_start: # main entry point
   # main program start
	addi	r2, r0 0
	addi	r5, r0 10000
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	sum.8
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
	addi	r30 r31 0
	swi	r30, r3 4
	addi	r3, r3, 8
	jal	min_caml_print_int
	addi	r3, r3, -8
	lwi	r30, r3 4
	addi	r31 r30 0
   # main program end
