#include <map>
#include <set>
#include <cmath>
#include <stack>
#include <queue>
#include <string>
#include <vector>
#include <bitset>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <iostream>
#include <algorithm>
using namespace std;
#define li        long long int
#define rep(i,to) for(li i=0;i<((li)(to));++i)
#define pb        push_back
#define sz(v)     ((li)(v).size())
#define bit(n)    (1ll<<(li)(n))
#define all(vec)  (vec).begin(),(vec).end()
#define each(i,c) for(__typeof((c).begin()) i=(c).begin();i!=(c).end();i++)
#define MP        make_pair
#define F         first
#define S         second



int main(){
	li res=0;
	rep(i,10){
		double d0=((double)rand()/1000.0-rand()/1000.0);
		double d1=((double)rand()/1000.0-rand()/1000.0);
		if(d0>d1) res++;
		printf("%0.5lf %0.5lf\n",d0,d1);
	}
	cerr<<res<<endl;
}
