#!/bin/sh



cd ../mincaml
make clean
make
cd ../test


rm -f __Tmp__.ml

cat ../raytracer/min-rt/min-rt-lib/Komaki/fabs.ml >> __Tmp__.ml
cat ../raytracer/min-rt/min-rt-lib/Komaki/fhalf.ml >> __Tmp__.ml
cat ../raytracer/min-rt/min-rt-lib/Komaki/fisneg.ml >> __Tmp__.ml
cat ../raytracer/min-rt/min-rt-lib/Komaki/fispos.ml >> __Tmp__.ml
cat ../raytracer/min-rt/min-rt-lib/Komaki/fiszero.ml >> __Tmp__.ml
cat ../raytracer/min-rt/min-rt-lib/Komaki/fless.ml >> __Tmp__.ml
cat ../raytracer/min-rt/min-rt-lib/Komaki/fneg.ml >> __Tmp__.ml
cat ../raytracer/min-rt/min-rt-lib/Komaki/fsqr.ml >> __Tmp__.ml
cat ../raytracer/min-rt/min-rt-lib/kozai/sin.ml >> __Tmp__.ml
cat ../raytracer/min-rt/min-rt-lib/kozai/cos.ml >> __Tmp__.ml
cat ../raytracer/min-rt/min-rt-lib/kozai/atan.ml >> __Tmp__.ml

cat ../raytracer/min-rt/min-rt-lib/Komaki/read_int.ml >> __Tmp__.ml
cat ../raytracer/min-rt/min-rt-lib/Komaki/read_float.ml >> __Tmp__.ml
cat ../raytracer/min-rt/min-rt-lib/Komaki/print_int.ml >> __Tmp__.ml


cat $1.ml >> __Tmp__.ml


../mincaml/./min-caml __Tmp__

rm -f kNormal_output.dat syntax_output.dat

cd ../mincaml
#make clean
cd ../test


cd ../../../asm2bin/2nd
make
cd ../../compiler/2nd/test

cat ../raytracer/min-rt/min-rt-lib/Komaki/read.s >> __Tmp__.s
cat ../raytracer/min-rt/min-rt-lib/Komaki/create_array.s >> __Tmp__.s
cat ../raytracer/min-rt/min-rt-lib/Komaki/print_int.s >> __Tmp__.s
cat ../raytracer/min-rt/min-rt-lib/kozai/floor.s >> __Tmp__.s
cat ../raytracer/min-rt/min-rt-lib/kozai/ftoi.s >> __Tmp__.s
cat ../raytracer/min-rt/min-rt-lib/Komaki/print_char.s >> __Tmp__.s
cat ../raytracer/min-rt/min-rt-lib/kozai/itof.s >> __Tmp__.s
cat ../raytracer/min-rt/min-rt-lib/Komaki/sqrt.s >> __Tmp__.s


../../../asm2bin/2nd/./AsmToBin < __Tmp__.s > __Tmp__.bin


cd ../../../asm2bin/2nd
make clean
cd ../../compiler/2nd/test



