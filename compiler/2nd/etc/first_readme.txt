Main.ml:
	最適化の回数を設定
	コマンドのパースをする。
	

実行される順番。
Lexer, Parser, Typing, kNormal, Alpha, mainの最適化, Closure, Virtual, Simm, RegAlloc, Emit




何かをするモジュール

	Lexer:
		Lexer.mllに従って各単語を分類。　
	
	Parser:
		単語列をParser.mlyに従って構文に変換。
		変換後はSyntax.ml形式に変更される。
	
	Typing.ml: Syntax型　-> Syntax型
		型チェックと型変数への代入を行うだけ。
		deref_termは何もしてない感じ。
		gで型チェックしつつ変数があればunifyに送って統合。
		unifyは型が同じかどうかを確かめつつ、変数なら代入をする。
		occurはt2がt1を含んでいるかを確かめる。
		extenvには型変数が保存されている。使われていない気がする。
		envにも型変数が保存されている。使われていない。

	kNormal.ml:　Syntax型 -> kNormal型
		syntaxの型からkNormalの型に変更。
		K正規化をする。
		kNormal型はだいたい文字列とintとfloatからなる。
		typing.extenvは外部変数としてここで使う。
		
	alpha.ml: kNormal型 -> kNormal型
		K正規化されているのでlet系で変数名を変更するだけ。
		
		
	最適化のところは後で。	
		
		
	closure.ml: kNormal型 -> closure型
		クロージャー変換する
		
	virtual.ml: closure型 -> asm型
		
	
	simm.ml: asm型 -> asm型
		16bit最適化。
		
	regAlloc.ml: asm型 -> asm型
		謎
		
	

型を規定するモジュール

	Syntax.ml:
		一番初めの変換形式。
		ほとんどmincamlのまま。
		内部でType.mlとId.mlを使っている。
		関数は{ name: 名前と型　; args: 名前と型のリスト ; body:返り値の型 }
		変数はVar(名前)
		letは{ (名前,タイプ), 代入される物, その後 }
		関数適用はappで{ 適用する物 , 適用されるもののリスト }
	
	Type.ml:
		一番元となる要素を規定している。
		addとかがないということ。
	
	Id.ml:
		Id.tはstring。
		文字列用のモジュール。
		Type.mlのをstringに変換したりする。
	
	M.ml:
		ほとんどmap
	
	S.ml:
		ほとんどmap
		
	Asm.ml:
		レジスタの内訳とか。
