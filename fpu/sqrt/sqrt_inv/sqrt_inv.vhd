library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity SQRT_INV is  
  port (I : in  std_logic_vector(31 downto 0);
        ANS : out std_logic_vector(31 downto 0));
end SQRT_INV;

architecture SQRT_INV_LOGIC of SQRT_INV is
  alias s : std_logic is I(31);
  alias e : std_logic_vector(7 downto 0) is I(30 downto 23);
  alias f : std_logic_vector(22 downto 0) is I(22 downto 0);

begin  -- FNV_LOGIC
  process (I)
    variable f1  : std_logic_vector(23 downto 0);
    variable f2  : std_logic_vector(24 downto 0);
    variable fp1  : std_logic_vector(6 downto 0);
    variable fp2 : std_logic_vector(13 downto 0);
    variable fp3 : std_logic_vector(23 downto 0);
    variable fp13  : std_logic_vector(8 downto 0);
    variable fp23 : std_logic_vector(15 downto 0);
    variable fi  : std_logic_vector(23 downto 0);
    variable fs1 : std_logic_vector(21 downto 0);
    variable fs2 : std_logic_vector(21 downto 0);
    variable fs3 : std_logic_vector(21 downto 0);
    variable fs4 : std_logic_vector(23 downto 0);
    variable fs43 : std_logic_vector(23 downto 0);
    variable fs5 : std_logic_vector(37 downto 0);
    variable fs6 : std_logic_vector(37 downto 0);
    variable fs7 : std_logic_vector(38 downto 0);
    variable fs8 : std_logic_vector(39 downto 0);
    variable fs87 : std_logic_vector(39 downto 0);

    variable fk1 : std_logic_vector(21 downto 0);
    variable fk2 : std_logic_vector(21 downto 0);
    variable fk3 : std_logic_vector(22 downto 0);
    variable fk4 : std_logic_vector(23 downto 0);
    variable fk43 : std_logic_vector(23 downto 0);
    variable fk5 : std_logic_vector(37 downto 0);
    variable fk6 : std_logic_vector(38 downto 0);
    variable fk7 : std_logic_vector(38 downto 0);
    variable fk8 : std_logic_vector(39 downto 0);
    variable fk87 : std_logic_vector(39 downto 0);
    variable em  : std_logic_vector(7 downto 0);
    variable ep  : std_logic_vector(7 downto 0);
  begin  -- process
    
    if e(0) = '1' then
      ep := "10111101" - ('0' & e(7 downto 1));  --189-e/2
    else
      ep := "10111110" - ('0' & e(7 downto 1));  --190-e/2
    end if;

    f1 := '1' & f;
    if e(0) = '1' then
      f2 := f1 & '0';
    else
      f2 := '0' & f1;
    end if;
    if f2>"1000110011100110010101111" then if f2>"1011101010100101010100101" then  if f2>"1101101001011100110001001" then if f2>"1110110101010111100000001" then if f2>"1111011111000101111011101" then if f2>"1111110100111111101010001" then fp1:="1011010"; else fp1:="1011011"; end if; else if f2>"1111001001111001001011101" then fp1:="1011100"; else fp1:="1011101"; end if; end if; else if f2>"1110001110001110001110101" then if f2>"1110100001011111000101101" then fp1:="1011110"; else fp1:="1011111"; end if; else if f2>"1101111011100011010011001" then fp1:="1100000"; else fp1:="1100001"; end if; end if; end if; else if f2>"1100100110010010010111001" then if f2>"1101000110110111000110001" then if f2>"1101010111111001001010011" then fp1:="1100010"; else fp1:="1100011"; end if; else if f2>"1100110110010101001111111" then fp1:="1100100"; else fp1:="1100101"; end if; end if; else if f2>"1100000111100100101111001" then if f2>"1100010110101101001111001" then fp1:="1100110"; else fp1:="1100111"; end if; else if f2>"1011111000110111110001111" then fp1:="1101000"; else fp1:="1101001"; end if; end if; end if; end if; else if f2>"1010000101011110011110011" then if f2>"1010110101010001011111111" then if f2>"1011001111001100000001111" then if f2>"1011011100101100011000111" then fp1:="1101010"; else fp1:="1101011"; end if; else if f2>"1011000010000011010110101" then fp1:="1101100"; else fp1:="1101101"; end if; end if; else if f2>"1010011100101111000001011" then if f2>"1010101000110101101001101" then fp1:="1101110"; else fp1:="1101111"; end if; else if f2>"1010010000111100110111101" then fp1:="1110000"; else fp1:="1110001"; end if; end if; end if; else if f2>"1001011010011101001111001" then if f2>"1001101111011010010000011" then if f2>"1001111010010011001010001" then fp1:="1110010"; else fp1:="1110011"; end if; else if f2>"1001100100110011001001101" then fp1:="1110100"; else fp1:="1110101"; end if; end if; else if f2>"1001000110100010101101001" then if f2>"1001010000010111111011111" then fp1:="1110110"; else fp1:="1110111"; end if; else if f2>"1000111100111101000000101" then fp1:="1111000"; else fp1:="1111001"; end if; end if; end if; end if; end if; else if f2>"0101111110111110001011011" then if f2>"0111100001011100001001110" then if f2>"1000010000011000100000111" then if f2>"1000100001100100001010101" then if f2>"1000101010011110001101111" then fp1:="1111010"; else fp1:="1111011"; end if; else if f2>"1000011000110111101111011" then fp1:="1111100"; else fp1:="1111101"; end if; end if; else if f2>"1000000000000000000000001" then if f2>"1000001000000110000100001" then fp1:="1111110"; else fp1:="1111111"; end if; else if f2>"0111110000010111100000110" then fp1:="1000000"; else fp1:="1000001"; end if; end if; end if; else if f2>"0110101011111111011000000" then if f2>"0111000101100010010101000" then if f2>"0111010011001011010000001" then fp1:="1000010"; else fp1:="1000011"; end if; else if f2>"0110111000011111000101010" then fp1:="1000100"; else fp1:="1000101"; end if; end if; else if f2>"0110010100100010110001001" then if f2>"0110100000000001001110001" then fp1:="1000110"; else fp1:="1000111"; end if; else if f2>"0110001001100010010010100" then fp1:="1001000"; else fp1:="1001001";  end if; end if; end if; end if; else if f2>"0100110111111000111111110" then if f2>"0101011000101100110001011" then if f2>"0101101011000101001001001" then if f2>"0101110100110100111011101" then fp1:="1001010"; else fp1:="1001011"; end if; else if f2>"0101100001101101100000000" then fp1:="1001100"; else fp1:="1001101"; end if; end if; else if f2>"0101000111101011100001011" then if f2>"0101010000000001110011101" then fp1:="1001110"; else fp1:="1001111"; end if; else if f2>"0100111111101000111001110" then fp1:="1010000"; else fp1:="1010001"; end if; end if; end if; else if f2>"0100011011100011010101110" then if f2>"0100101001001101110010011" then if f2>"0100110000011010111001111" then fp1:="1010010"; else fp1:="1010011"; end if; else if f2>"0100100010010000110110011" then fp1:="1010100"; else fp1:="1010101"; end if; end if; else if f2>"0100001110110011110101100" then if f2>"0100010101000100100011110" then fp1:="1010110"; else fp1:="1010111"; end if; else if f2>"0100001000110000100010110" then fp1:="1011000"; elsif f2>"0100000010111010000101110" then fp1:="1011001"; else fp1:="1011010"; end if; end if; end if; end if; end if; end if;  

    if e(0) = '1' then
      fs1 := f1(23 downto 9) * fp1;
      fs2 := fs1(21 downto 7) * fp1;
      fs3 := fs2(20 downto 6) * fp1;
      fp13 := fp1 * "11";
      fs4 := fp13 & "000000000000000";
      fs43 := fs4-fs3;
      fp2 := fs43(22 downto 9);

      fs5 := f1 * fp2;
      fs6 := fs5(37 downto 14) * fp2;
      fs7 := fs6(37 downto 13) * fp2;
      fp23 := fp2 * "11";
      fs8 := fp23 & "000000000000000000000000";
      fs87 := fs8-fs7;
      if f1 = x"800000" then
        fp3 := x"800000";
      else
        fp3 := fs87(38 downto 15);        
      end if;
    else
      fk1 := f1(23 downto 9) * fp1;
      fk2 := fk1(20 downto 6) * fp1;
      fk3 := fk2(21 downto 6) * fp1;
      fp13 := fp1 * "11";
      fk4 := fp13 & "000000000000000";
      fk43 := fk4-fk3;
      fp2 := fk43(22 downto 9);

      fk5 := f1 * fp2;
      fk6 := fk5(37 downto 13) * fp2;
      fk7 := fk6(37 downto 13) * fp2;
      fp23 := fp2 * "11";
      fk8 := fp23 & "000000000000000000000000";
      fk87 := fk8-fk7;
      fp3 := fk87(38 downto 15)+x"0001";     
    end if;
    fi := fp3(23 downto 0);
    
    if e = x"00" or ep = x"00" or s = '1' or e = x"ff" or ep = x"ff"then
      ANS <= (others => '0');
    elsif fi = x"800000" then
      ANS <= s & (ep + 1) & "00000000000000000000000";
    else
      ANS <= s & ep & fi(22 downto 0);      
    end if;
  end process;

end SQRT_INV_LOGIC;
