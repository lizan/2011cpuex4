//1024bitのROMを生成する.±1のパラメータ調整を行ってる.
#include <string>
#include <queue>
#include <vector>
#include <fstream>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <iomanip>
using namespace std;

union Xp{
  u_int32_t ii;
  float ff;
};

int main(){
  u_int64_t i,a0=0,x0,x1,rom1,rom2,p0,p1;
  ofstream rom_v1("rom_vhdl1.txt");
  ofstream rom_v2("rom_vhdl2.txt");
  ofstream rom_c1("rom_cpp1.txt");
  ofstream rom_c2("rom_cpp2.txt");
  union Xp xpp;
  rom_c1 << "u_int32_t rom_sqrt1[1025] = {";
  rom_c2 << "u_int32_t rom_sqrt2[1025] = {";
  rom_v1 << "constant rom_sqrt1 : data_sqrt1 :=\n(";
  rom_v2 << "constant rom_sqrt2 : data_sqrt2 :=\n(";
  for(a0 = 0; a0 < 0x400; a0++){
    p1 = 1;
    p0 = a0 | 0x200;
    if((a0 & 0x200) == 0){
      xpp.ff = (sqrt(p0) + sqrt(p0+1));
      x0 = (0x00800000 | (xpp.ii & 0x007fffff));
      rom1 = ((x0 + (((p0 << 46)/x0) >> 8))>>1)+2;
      rom2 = ((p1 << 46) / x0) >> 11;
    }
    else{
      xpp.ff = (sqrt(p0<<1) + sqrt((p0+1)<<1));
      x0 = (0x00800000 | (xpp.ii & 0x007fffff));
      rom1 = ((x0 + (((p0 << 46)/x0) >> 9))>>1)+2;
      rom2 = ((p1 << 45) / x0) >> 11;
    }

    rom_c1 << "0x" << hex << rom1 << ',';
    rom_c2 << "0x" << hex << rom2 << ',';
    rom_v1 << "x\"" << hex << rom1 << "\",";
    rom_v2 << "x\"" << setw(3) << setfill('0') << hex << rom2 << "\",";
  }
  rom_c1 << "0};" << endl;
  rom_c2 << "0};" << endl;
  rom_v1 << "x\"000000\");" << endl;
  rom_v2 << "x\"000\");" << endl;

  return 0;
}
