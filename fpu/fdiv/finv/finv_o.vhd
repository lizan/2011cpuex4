library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity FINV is  
  port (I : in  std_logic_vector(31 downto 0);
        ANS : out std_logic_vector(31 downto 0));
end FINV;

architecture FINV_LOGIC of FINV is
  alias s : std_logic is I(31);
  alias e : std_logic_vector(7 downto 0) is I(30 downto 23);
  alias f : std_logic_vector(22 downto 0) is I(22 downto 0);

begin  -- FNV_LOGIC
  process (I)
    variable f1  : std_logic_vector(23 downto 0);
    variable fp1  : std_logic_vector(6 downto 0);
    variable fp2 : std_logic_vector(13 downto 0);
    variable fi  : std_logic_vector(23 downto 0);
    variable fs1 : std_logic_vector(21 downto 0);
    variable fs2 : std_logic_vector(14 downto 0);
    variable fs3 : std_logic_vector(21 downto 0);
    variable fs4 : std_logic_vector(37 downto 0);
    variable fs5 : std_logic_vector(23 downto 0);
    variable fs6 : std_logic_vector(37 downto 0);
    variable ep  : std_logic_vector(7 downto 0);
  begin  -- process
    f1 := '1' & f;
    if f1>"101010101010101010101011" then if f1>"110011001100110011001101" then if f1>"111000111000111000111001" then if f1>"111100001111000011110001" then if f1>"111110000011111000010000" then if f1>"111111000000111111000001" then fp1 := "1000000"; else fp1 := "1000001"; end if; else if f1>"111101001000100110001110" then fp1 := "1000010"; else fp1 := "1000011"; end if; end if; else if f1>"111010100000111010100001" then if f1>"111011010111001100000100" then fp1 := "1000100"; else fp1 := "1000101"; end if; else if f1>"111001101100001010110101" then fp1 := "1000110"; else fp1 := "1000111"; end if; end if; end if; else	 if f1>"110101111001010000110110" then if f1>"110111010110011111001001" then if f1>"111000000111000000111000" then fp1 := "1001000"; else fp1 := "1001001"; end if; else if f1>"110110100111010000001110" then fp1 := "1001010"; else fp1 := "1001011"; end if; end if; else if f1>"110100100000110100100001" then if f1>"110101001100011101111011" then fp1 := "1001100"; else fp1 := "1001101"; end if; else if f1>"110011110110010001110101" then fp1 := "1001110"; else fp1 := "1001111"; end if; end if; end if; end if; else if f1>"101110100010111010001100" then if f1>"110000110000110000110001" then if f1>"110001111100111000001101" then if f1>"110010100100010110001000" then fp1 := "1010000"; else fp1 := "1010001"; end if; else if f1>"110001010110010111001001" then fp1 := "1010010"; else fp1 := "1010011"; end if; end if; else if f1>"101111101000001011111010" then if f1>"110000001100000011000001" then fp1 := "1010100"; else fp1 := "1010101"; end if; else  if f1>"101111000101001001100100" then fp1 := "1010110"; else fp1 := "1010111"; end if; end if; end if; else if f1>"101100100001011001000011" then if f1>"101101100000101101100001" then if f1>"101110000001011100000011" then fp1 := "1011000"; else fp1 := "1011001"; end if; else if f1>"101101000000101101000001" then fp1 := "1011010"; else fp1 := "1011011"; end if; end if; else if f1>"101011100100110001000001" then if f1>"101100000010110000001011" then fp1 := "1011100"; else fp1 := "1011101"; end if; else if f1>"101011000111011010010001" then fp1 := "1011110"; else fp1 := "1011111"; end if; end if; end if; end if; end if; else if f1>"100100100100100100100100" then if f1>"100111011000100111011000" then if f1>"101000111101011100001010" then if f1>"101001110010111100000101" then if f1>"101010001110100000111111" then fp1 := "1100000"; else fp1 := "1100001"; end if; else if f1>"101001010111111010110101" then fp1 := "1100010"; else fp1 := "1100011"; end if; end if; else if f1>"101000001010000010100001" then if f1>"101000100011011111000011" then fp1 := "1100100"; else fp1 := "1100101"; end if; else if f1>"100111110001000101100110" then fp1 := "1100110"; else fp1 := "1100111"; end if; end if; end if; else if f1>"100101111011010000100110" then if f1>"100110101001000011101000" then if f1>"100111000000100111000000" then fp1 := "1101000"; else fp1 := "1101001"; end if; else if f1>"100110010001111100011010" then fp1 := "1101010"; else fp1 := "1101011"; end if; end if; else if f1>"100101001111001000001001" then if f1>"100101100100111111011010" then fp1 := "1101100"; else fp1 := "1101101"; end if; else if f1>"100100111001101010000110" then fp1 := "1101110"; else fp1 := "1101111"; end if; end if; end if; end if; else if f1>"100010001000100010001000" then if f1>"100011010011110111001011" then if f1>"100011111011100000100100" then if f1>"100100001111110110111100" then fp1 := "1110000"; else fp1 := "1110001"; end if; else if f1>"100011100111100000110101" then fp1 := "1110010"; else fp1 := "1110011"; end if; end if; else if f1>"100010101101100011110011" then if f1>"100011000000100011000000" then fp1 := "1110100"; else fp1 := "1110101"; end if; else if f1>"100010011010111001000000" then fp1 := "1110110"; else fp1 := "1110111"; end if; end if; end if; else if f1>"100001000010000100001000" then if f1>"100001100100101110001010" then if f1>"100001110110011110101011" then fp1 := "1111000"; else fp1 := "1111001"; end if; else if f1>"100001010011010000001000" then fp1 := "1111010"; else fp1 := "1111011"; end if; end if; else if f1>"100000100000100000100000" then if f1>"100000110001001001101110" then fp1 := "1111100"; else fp1 := "1111101"; end if; else if f1>"100000010000001000000100" then fp1 := "1111110"; else fp1 := "1111111"; end if; end if; end if; end if; end if; end if; 

    
    fs1 := f1(23 downto 9) * fp1;
    fs2 := not(fs1(21 downto 7));
    fs3 := fs2 * fp1;
    fp2 := fs3(20 downto 7);

    fs4 := f1 * fp2;
    fs5 := not(fs4(37 downto 14));
    fs6 := fs5 * fp2;
    fi := fs6(36 downto 13)+"00000000000000000000001";

    if e >= "11111101" then
      ep := (others => '0');
    else
      ep := "11111101" - e;      
    end if;

    if e = x"00" or ep = x"00" then
      ANS <= (others => '0');
    elsif f1 = "100000000000000000000000" then
      ANS <= s & (ep + 1) & "00000000000000000000000";
    else
      ANS <= s & ep & fi(22 downto 0);      
    end if;
  end process;

end FINV_LOGIC;
