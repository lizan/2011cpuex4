typedef unsigned int u_int32_t;

class Cfadd{
 public:
  u_int32_t operator()(const u_int32_t x1, const u_int32_t x2);
};

class Cfsub{
 public:
  u_int32_t operator()(const u_int32_t x1, const u_int32_t x2);
};

class Cfmul{
 public:
  u_int32_t operator()(const u_int32_t x1, const u_int32_t x2);
};

class Cfdiv{
 public:
  u_int32_t operator()(const u_int32_t x1, const u_int32_t x2);
};

class Csqrt{
 public:
  u_int32_t operator()(const u_int32_t x);
  u_int32_t operator()(const u_int32_t x, const u_int32_t _) { // dummy function to make sqrt fit format
    return operator()(x);
  }
};

class Cfslt{
 public:
  int operator()(const u_int32_t x1, const u_int32_t x2);
};

class Cfeq{
 public:
  int operator()(const u_int32_t x1, const u_int32_t x2);
};
