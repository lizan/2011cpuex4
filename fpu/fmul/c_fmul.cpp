#include <string>
#include <queue>
#include <vector>
#include <fstream>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#define INPUTOPTION 2
//1の時はrandによる乱数,2の時はindata_sの値をとりだす
#define NODATA 0
//1の時はindata,outdataの出力を行わない
#define ONTEST 1
//1の時はデバッグ用関数を呼ぶ。indata_sの値を取り出すときは常に呼ぶ
using namespace std;
ofstream missdata("missdata");
ofstream m_indata("mindata");
ofstream m_outdata("moutdata");

//デバッグ用共用体
union Ap{
  u_int32_t ii;
  float ff;
};
union Bp{
  u_int32_t ii;
  float ff;
};
union Xp{
  u_int32_t ii;
  float ff;
};
union Ep{
  u_int32_t ii;
  float ff;
};
union Apb{
  u_int32_t ii;
  float ff;
}; 

//u_int32_tをk文字の文字列に変換
string to_string(long l){
  string x;
  for(int i = 0; i < 32; i++){
    if(l % 2 == 1)
      x = "1" + x;
    else
      x = "0" + x;
    l /= 2;
  }
  return x;
}

/*
  デバッグ用関数
  異なる結果ををmissdataに出力
 */
void t_or_f(u_int32_t a, u_int32_t b, u_int32_t x, string i1,string i2, string o, int i){
  float max,diff;
  u_int32_t ae,be,abe;
  union Ap app;
  union Bp bpp;
  union Xp xpp;
  union Ep epp;
  union Apb ab;

  ae = a & 0x7f800000;
  be = b & 0x7f800000;
  if(ae == 0x7f800000 || ae == 0)
    app.ii = 0;
  else
    app.ii = a;

  if(be == 0x7f800000 || be == 0)
    bpp.ii = 0;
  else
    bpp.ii = b;
  
  xpp.ii = x;
  
  ab.ff = app.ff * bpp.ff;

  abe = ab.ii & 0x7f800000;
  if(abe == 0x7f800000 || abe == 0)
    ab.ii = 0;
  
  epp.ii = 0x0b800000;//2^-104
  max = epp.ff;
  if(max < fabs(ab.ff))
    max = fabs(ab.ff);

  max /= 4194304;//pow(2,22);
  
  diff = fabs(xpp.ff - ab.ff);
  if(diff >= max/4){
    missdata << i << "番目\n" << 
      i1 << "\t" << app.ff << "\tA\n" <<
      i2 << "\t" << bpp.ff << "\tB\n" <<
      o << "\t" << xpp.ff << "\tFMUL(A,B)\n" <<
      to_string(ab.ii) << "\t" << ab.ff << "\t(A*B)\n"<<
      diff << "\t" << max <<  "\t" << epp.ff << "\n";

    m_indata << i1 << " " << i2 << endl;
    m_outdata << o << endl;
  }
}

/*関数本体   
  u_int32_t型の2数を浮動小数点で足したときの和をu_int32_t型で出力する
  e = 255 は0
 */
u_int32_t fmul(u_int32_t x1, u_int32_t x2){
  u_int32_t s1 = x1 >> 31;
  u_int32_t s2 = x2 >> 31;
  u_int32_t sp;
  u_int32_t e1 = (x1 & 0x7f800000) >> 23;
  u_int32_t e2 = (x2 & 0x7f800000) >> 23;
  u_int32_t ep;
  u_int32_t fu1 = (0x00800000 | (x1 & 0x007ff800)) >> 11;
  u_int32_t fu2 = (0x00800000 | (x2 & 0x007ff800)) >> 11;
  u_int32_t fd1 = (x1 & 0x000007ff);
  u_int32_t fd2 = (x2 & 0x000007ff);
  u_int32_t hh, hl, lh, f3;
  
  if(e1 == 0 || e2 == 0 || e1 == 255 || e2 == 255)
    return 0;
    
  hh = fu1 * fu2;
  hl = fu1 * fd2;
  lh = fd1 * fu2;
  f3 = hh + (hl >> 11) + (lh >> 11) +2;
  
  ep = e1 + e2 + 129;
  sp = s1 ^ s2; // xor
  if(f3 >= 0x04000000){
    f3 = (f3 >> 2);
    ep+=2;
  }
  else if(f3 >= 0x02000000){
    f3 = (f3 >> 1);
    ep++;
  }
    
  if(ep > 0x000001fe || ep <= 0x00000100)
    return 0;
  else
    return (sp << 31) | ((0x000000ff & ep) << 23) | ((f3 >> 1)& 0x007fffff); 
} 

//ランダムに値を生成し演算を実行する。
void randin(){
  long int k,i,s;
  ofstream indata("indata");
  ofstream e_outdata("e_outdata");
  string i1,i2, o;
  u_int32_t a,b,x;

  cout << "回数　srand定数\n";
  cin >> k >> s;  
  srand(s);

  for(i = 0;i < k;i++){
    a = rand() + rand();
    b = rand() + rand();

    x = fmul(a, b);

    i1 = to_string(a);
    i2 = to_string(b);
    o = to_string(x);

#if NODATA != 1
    indata << i1 << " " << i2 << "\n";
    e_outdata << o << "\n";    
#endif

#if ONTEST == 1
    t_or_f(a,b,x,i1,i2,o,i);//デバッグ
#endif

    if((i*100) % k == 0)
      cout << i*100/k << '\n';
  }
}

//01文字列をu_intに変換
u_int32_t to_int(string x){
  int k = x.size();
  u_int32_t ans = 0;
  for(int i = 0; i<k;i++){
    ans <<= 1;
    if(x.at(i) == '1'){
      ans++;
    }
  }
  return ans;
}

/*
  indata_sに入っているデータについて演算を行う
  indata_sには間違いが起こりやすそうなものが入っている。
*/
void org_in(){
  int i = 0;
  ifstream indata_s("indata_s");
  ofstream e_outdata("e_outdata");
  u_int32_t a,b,x;
  string o,as,bs;
  char c;

  while(indata_s >> as >> bs){
    i++;
    a = to_int(as);
    b = to_int(bs);

    x = fmul(a, b);

    o = to_string(x);
    
#if NODATA != 1
    e_outdata << o << "\n";    
#endif
    
    
    t_or_f(a,b,x,to_string(a),to_string(b),o,i);//デバッグ
    indata_s.get(c);
  }
}

int main(){

#if INPUTOPTION == 1
  randin();
#elif INPUTOPTION == 2
  org_in();
#endif
    
return 0;
}
  

