library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_textio.all;

library STD;
use STD.textio.all;
entity pfmul_test is
end pfmul_test;

architecture SIM_PFMUL of pfmul_test is
  component PFMUL
    port (CLK    : in std_logic
          FA, FB : in  std_logic_vector(31 downto 0);
          Q      : out std_logic_vector(31 downto 0));
  end component;
  
begin  -- SIM_PFMUL
  
  

end SIM_PFMUL;
