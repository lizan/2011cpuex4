#include <string>
#include <queue>
#include <vector>
#include <fstream>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#define INPUTOPTION 1
//1の時はrandによる乱数,2の時はindata_sの値をとりだす
#define ONTEST 1
//1の時はデバッグ用関数を呼ぶ。indata_sの値を取り出すときは常に呼ぶ
using namespace std;
ofstream missdata("missdata");
ofstream m_indata("mindata");
ofstream m_outdata("moutdata");

//デバッグ用共用体
union Ap{
  u_int32_t ii;
  float ff;
};
union Bp{
  u_int32_t ii;
  float ff;
};
union Xp{
  u_int32_t ii;
  float ff;
};
union Ep{
  u_int32_t ii;
  float ff;
};
union Apb{
  u_int32_t ii;
  float ff;
};

//u_int32_tをk文字の文字列に変換
string to_string(u_int32_t l){
  string x;
  for(int i = 0; i < 32; i++){
    if(l & 1U == 1U)
      x = "1" + x;
    else
      x = "0" + x;
    l >>= 1;
  }
  return x;
}

/*
  デバッグ用関数
  異なる結果ををmissdataに出力
 */
void t_or_f(u_int32_t a, u_int32_t b, u_int32_t x, string i1,string i2, string o, int i){
  float max,diff;
  union Ap app;
  union Bp bpp;
  union Xp xpp;
  union Ep epp;
  union Apb ab;

  if((a & 0x7f800000) == 0 || (a & 0x7f800000) == 0x07f800000)
    app.ii = 0;
  else
    app.ii = a;

  if((b & 0x7f800000) == 0 || (b & 0x7f800000) == 0x07f800000)
    bpp.ii = 0;
  else
    bpp.ii = b;
  
  xpp.ii = x;
  
  ab.ff = app.ff + bpp.ff;  

  if((ab.ii & 0x7f800000) == 0 || (ab.ii & 0x7f800000) == 0x07f800000)
    ab.ii = 0;

  epp.ii = 0x0c000000;//2^-103
  max = epp.ff;
  if(max < fabs(app.ff))
    max = fabs(app.ff);
  if(max < fabs(bpp.ff))
    max = fabs(bpp.ff);
  if(max < fabs(ab.ff))
    max = fabs(ab.ff);

  max /= 8388608;//pow(2,23);
  
  diff = fabs(xpp.ff - ab.ff);
  if(diff >= max/2){
    missdata << i << "番目\n" << 
      i1 << "\t" << app.ff << "\tA\n" <<
      i2 << "\t" << bpp.ff << "\tB\n" <<
      o << "\t" << xpp.ff << "\tFADD(A,B)\n" <<
      to_string(ab.ii) << "\t" << ab.ff << "\t(A+B)\n"<<
      diff << "\t" << max <<  "\t" << epp.ff << "\n";
    m_indata << i1 << " " << i2 << endl;
    m_outdata << o << endl;
  }
}

//x(a downto b)に相当
u_int32_t subu(u_int32_t x, int a, int b){//x(a downto b)
  u_int32_t y = (x << (32 - a -1)) >> (32 - a + b - 1);
  return y;
}

/*
  vhdl実装と同じもの
 */
u_int32_t fadd_vhd(u_int32_t x1, u_int32_t x2){
  u_int32_t s1 = x1 >> 31;
  u_int32_t s2 = x2 >> 31;
  u_int32_t sp;
  u_int32_t e1 = (x1 & 0x7f800000) >> 23;
  u_int32_t e2 = (x2 & 0x7f800000) >> 23;
  u_int32_t ep,epx;
  u_int32_t f1 = (x1 & 0x7fffffff);//23bit
  u_int32_t f2 = (x2 & 0x7fffffff);
  u_int32_t fla,fsm,f3,f4,fcal,n,ftt,ett,i,mar,ans;

  if((e1 == 255 || e1 == 0) && (e2 == 255 || e2 == 0))
    fla = 0;
  else if(e1 == 255)
    fla = ((x2 & 0x007fffff) | 0x00800000) << 2;
  else if(e2 == 255)
    fla = ((x1 & 0x007fffff) | 0x00800000) << 2;
  else if(e1 > e2)
    fla = ((x1 & 0x007fffff) | 0x00800000) << 2;
  else if(e2 > e1 )
    fla = ((x2 & 0x007fffff) | 0x00800000) << 2;
  else if(f1 > f2)
    fla = ((x1 & 0x007fffff) | 0x00800000) << 2;
  else
    fla = ((x2 & 0x007fffff) | 0x00800000) << 2;


  if(e1 == 0U || e1 == 255U || e2 == 0U || e2 == 255U)
    fsm = 0;
  else if(e1 > e2 && (e1-e2 > 26U))
    fsm = 0;
  else if(e2 > e1 && (e2-e1 > 26U))
    fsm = 0;
  else if(e1 - e2 == 25U)
    fsm = 1;
  else if(e2 - e1 == 25U)
    fsm = 1;
  else if(e1 > e2)
    fsm = (((x2 & 0x007fffff) | 0x00800000) << 2) >> (e1-e2);
  else if(e2 > e1)
    fsm = (((x1 & 0x007fffff) | 0x00800000) << 2) >> (e2-e1);
  else if(f1 > f2)
    fsm = ((x2 & 0x007fffff) | 0x00800000) << 2;
  else
    fsm = ((x1 & 0x007fffff) | 0x00800000) << 2;
  
  if((e1 == 0U || e1 == 255U) && (e2 == 0U || e2 == 255U)){
    ep = 0;
    sp = 0;
  }
  else if(e1 == 255){
    ep = e2;
    sp = s2;
  }
  else if(e2 == 255){
    ep = e1;
    sp = s1;
  }
  else if(e1 > e2){
    ep = e1;
    sp = s1;
  }
  else if (e2 > e1){
    ep = e2;
    sp = s2;
  }
  else if(f1 > f2){
    ep = e1;
    sp = s1;
  }
  else{
    ep = e2;
    sp = s2;
  }

  if(s1 == s2)
    fcal = fla + fsm;
  else
    fcal = fla - fsm;
    
  if(fcal >> 26 == 1)
    n=0;
  else if(subu(fcal,25,14) == 0)
    if(subu(fcal,13,8) == 0)
      if(subu(fcal,7,5) == 0)
	if(subu(fcal,4,2) == 0)
	  n=25;
	else if(subu(fcal,4,3) == 0)
	  n=24;
	else if(subu(fcal,4,4) == 0)
	  n=23;
	else
	  n=22;
      else
	if(subu(fcal,7,6) == 0)
	  n=21;
	else if(subu(fcal,7,7) == 0)
	  n=20;
	else
	  n=19;
    else
      if(subu(fcal,13,11) == 0)
	if(subu(fcal,10,9) == 0)
	  n=18;
	else if(subu(fcal,10,10) == 0)
	  n=17;
	else
	  n=16;
      else
	if(subu(fcal,13,12) == 0)
	  n=15;
	else if(subu(fcal,13,13) == 0)
	  n=14;
	else
	  n=13;
  else
    if(subu(fcal,25,20) == 0)
      if(subu(fcal,19,17) == 0)
	if(subu(fcal,16,15) == 0)
	  n=12;
	else if(subu(fcal,16,16) == 0)
	  n=11;
	else
	  n=10;
      else
	if(subu(fcal,19,18) == 0)
	  n=9;
	else if(subu(fcal,19,19) == 0)
	  n=8;
	else
	  n=7;
    else
      if(subu(fcal,25,23) == 0)
	if(subu(fcal,22,21) == 0)
	  n=6;
	else if(subu(fcal,22,22) == 0)
	  n=5;
	else
	  n=4;
      else
	if(subu(fcal,25,24) == 0)
	  n=3;
	else if(subu(fcal,25,25) == 0)
	  n=2;
	else
	  n=1;
  
  if(subu(fcal,26,2) == 0x01ffffff)
    i = 2;
  else if(subu(fcal,25,1) == 0x01ffffff)
    i = 1;
  else if(subu(fcal,24,0) == 0x01ffffff)
    i = 1;
  else 
    i = 0;

  switch (subu(n,1,0)){
  case 0 : f3 = fcal; break;
  case 1 : f3 = (subu(fcal,25,0) << 1); break;
  case 2 : f3 = (subu(fcal,24,0) << 2); break;
  default: f3 = (subu(fcal,23,0) << 3); break;
  }

  switch (subu(n,1,0)){
  case 3 : mar = 0; break;
  default: mar = 4; break;
  }

  epx = ep+i;
  
  switch (subu(n,4,2)){
  case 0 : ftt = f3 + mar; break;
  case 1 : ftt = (subu(f3,22,0) << 4); break;
  case 2 : ftt = (subu(f3,18,0) << 8); break;
  case 3 : ftt = (subu(f3,14,0) << 12); break;
  case 4 : ftt = (subu(f3,10,0) << 16); break;
  case 5 : ftt = (subu(f3,6,0) << 20); break;
  case 6 : ftt = (subu(f3,2,0) << 24); break;
  default: ftt = 0; break;
  }
  
  if(n == 26 || epx < n)
    epx = 0;
  else
    epx = epx - n + 1;
  
  if(ep == 0)
    ans = (sp << 31) + subu(ftt,25,3);
  else
    ans = (sp << 31) + (epx << 23) + subu(ftt,25,3);
  
  return ans;
}





/*
  関数本体   
  u_int32_t型の2数を浮動小数点で足したときの和をu_int32_t型で出力する
 */
u_int32_t fadd(u_int32_t x1, u_int32_t x2){
  u_int32_t s1 = x1 >> 31;
  u_int32_t s2 = x2 >> 31;
  u_int32_t sp;
  u_int32_t e1 = (x1 & 0x7f800000) >> 23;
  u_int32_t e2 = (x2 & 0x7f800000) >> 23;
  u_int32_t ep;
  u_int32_t f1 = (x1 & 0x7fffffff);//23bit
  u_int32_t f2 = (x2 & 0x7fffffff);
  u_int32_t fp1 = ((x1 & 0x007fffff) | 0x00800000) << 3;//シフトしても3桁は保持 //27(1+23+3)
  u_int32_t fp2 = ((x2 & 0x007fffff) | 0x00800000) << 3;//ALL1 -> 0x07ffffff
  u_int32_t f3,f4;
  int i=0;

  if(e1 == 0 || e1 == 255){//指数部0,255は0
    return x2;
  }
  if(e2 == 0 || e2 == 255){
    return x1;
  }    
  if(f1 > f2){//指数部の差だけ仮数部をシフト
    if(e1 - e2 >= 27)
      fp2 = 0;
    else
      fp2 >>= (e1 - e2);
    sp = s1;
    ep = e1;
  }
  else{
    if(e2 - e1 >= 27)
      fp1 = 0;
    else
      fp1 >>= (e2 - e1);
    sp = s2;
    ep = e2;
  }

  if(s1 == s2){//符号一致(+)
    f3 = fp1 + fp2;

    if(f3 >= 0x08000000){
      f3 >>= 1;
      ep++;
    }
  }
  else{//符号不一致(-)
    if(sp == s1)
      f3 = fp1 - fp2;
    else 
      f3 = fp2 - fp1;

    while(f3 < 0x04000000){
      f3 <<= 1;
      ep--;
      i++;
    }
  }

  if(i < 3)//丸め(下から3桁目を繰り上げ)
      f3 += 4U;

  if(f3 >= 0x08000000){//繰り上がりあり
    f3 >>= 1;
    ep++;
  }

  if((ep & 0x000000ff) == 0U || ep <= 0 || ep >= 0x00000100)
    return 0U;
  else
    return (sp << 31) | (ep << 23) | ((f3 >> 3) & 0x007fffff);
}

//ランダムに値を生成し演算を実行する。
void randin(){
  long int k,i,s;
  ofstream indata("indata");
  ofstream e_outdata("e_outdata");
  string i1,i2, o;
  u_int32_t a,b,x;

  cout << "回数　srand定数\n";
  cin >> k >> s;  
  srand(s);

  for(i = 0;i < k;i++){
    a = rand() + rand();
    b = rand() + rand();

    x = fadd_vhd(a, b);

    i1 = to_string(a);
    i2 = to_string(b);
    o = to_string(x);

    indata << i1 << " " << i2<< "\n";
    e_outdata << o << "\n";    

    t_or_f(a, b, x, i1, i2, o, i);//デバッグ

    if((i*100) % k == 0)
      cout << i*100/k << '\n';
  }
}

//01文字列をu_intに変換
u_int32_t to_int(string x){
  int k = x.size();
  u_int32_t ans = 0;
  for(int i = 0; i<k;i++){
    ans <<= 1;
    if(x.at(i) == '1'){
      ans++;
    }
  }
  return ans;
}

/*
  indata_sに入っているデータについて演算を行う
  indata_sには間違いが起こりやすそうなものが入っている。
*/
void org_in(){
  int i = 0;
  ifstream indata_s("indata_s");
  ofstream e_outdata("e_outdata");
  u_int32_t a,b,x;
  string o,as,bs;
  char c;

  while(indata_s >> as >> bs){
    i++;
    a = to_int(as);
    b = to_int(bs);

    x = fadd_vhd(a, b);

    o = to_string(x);
    
    e_outdata << o << "\n";    
    
    t_or_f(a,b,x,to_string(a),to_string(b),o,i);//デバッグ
    indata_s.get(c);
  }
}
    

//main
int main(){

#if INPUTOPTION == 1
  randin();
#elif INPUTOPTION == 2
  org_in();
#endif
    
  return 0;
}
  


