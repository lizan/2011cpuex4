#include <map>
#include <set>
#include <cmath>
#include <stack>
#include <queue>
#include <string>
#include <vector>
#include <bitset>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <iostream>
#include <algorithm>
using namespace std;
#define li       	long long int
#define rep(i,to)	for(li i=0;i<((li)(to));++i)
#define pb       	push_back
#define sz(v)    	((li)(v).size())
#define bit(n)   	(1ll<<(li)(n))


/////////////////////////////////////////

#define IN_FILE "fib.s"
#define OUT_FILE_TXT "fib.txt"
#define OUT_FILE_BIN "fib.bin"

/////////////////////////////////////////

#define MAX_LINE_CHAR 1024
#define REG_BIT 5
#define INST_LEN 1
FILE *in,*out0,*out1;
vector<string> inst;
vector<string> buf;
vector<li> jump_pos;
vector<pair<li,string> > JL;
vector<pair<li,string> > JEQ;
vector<li> SWPC;
map<string,li> label;
li LINE=0;

void init(){
	in=fopen(IN_FILE,"r");
	out0=fopen(OUT_FILE_TXT,"w");
	out1=fopen(OUT_FILE_BIN,"w");
}

void finish(){
	fclose(in);
	fclose(out0);
	fclose(out1);
}

void parse(char *c){
	inst.clear();
	stringstream ss;
	string str;
	ss<<c;
	while(ss>>str && str[0]!='#') inst.pb(str);
}

string li2str(li t0,li digit){
	string res=string(digit,'0');
	rep(i,digit)if(t0&bit(i)) res[digit-i-1]='1';
	return res;
}

string str2bin(string str,li digit){
	stringstream ss;
	string res=string(digit,'0');
	li tmp;
	ss<<str;
	ss>>tmp;
	rep(i,digit)if(tmp&bit(i)) res[digit-i-1]='1';
	return res;
}
#define str2bin_reg(reg,digit) (str2bin(reg.substr(1),digit))	


void generate_R(string opecode){
	string res=string(6,'0');
	res+=str2bin_reg(inst[2],5);
	res+=str2bin_reg(inst[3],5);
	res+=str2bin_reg(inst[1],5);
	res+=string(5,'0');
	res+=opecode;
	buf.pb(res);
}

void generate_I(string opecode){
	string res=opecode;
	res+=str2bin_reg(inst[2],5);
	res+=str2bin_reg(inst[1],5);
	res+=str2bin(inst[3],16);
	buf.pb(res);
}

void generate_JR(){
	buf.pb("000001"+str2bin_reg(inst[1],5)+string(21,'0'));
}

void generate_JL(){
	if(inst[1]=="min_caml_print_int") return ;
	JL.pb(make_pair(sz(buf),inst[1]));
	buf.pb("000010");
}
void replace_JL(){
	rep(i,sz(JL)) buf[JL[i].first]+=li2str(label[JL[i].second],26);
}

void generate_JEQ(){
	if(inst[3]=="min_caml_print_int") return ;
	JEQ.pb(make_pair(sz(buf),inst[3]));
	string res="000100";
	res+=str2bin_reg(inst[1],5);
	res+=str2bin_reg(inst[2],5);
	buf.pb(res);
}
void replace_JEQ(){
	rep(i,sz(JEQ)){
		li dif=label[JEQ[i].second]-1-JEQ[i].first;
		string res=li2str(abs(dif),16);
		if(dif<0) res[0]='1';
		buf[JEQ[i].first]+=res;
	}
}

void generate_SWPC(){
	SWPC.pb(sz(buf));
	string r30="11110";
	buf.pb("00100000000"+r30); // ADDI r30 r0 PC 
	buf.pb("101011"+str2bin_reg(inst[1],5)+"11110"+str2bin(inst[2],16));
}

void replace_SWPC(){
rep(i,sz(jump_pos)) cout<<"jump_pos:"<<jump_pos[i]<<endl;
rep(i,sz(SWPC)) cout<<"SWPC:"<<SWPC[i]<<endl;
	rep(i,sz(SWPC)){
		cout<<"  "<<*upper_bound(jump_pos.begin(),jump_pos.end(),SWPC[i])<<endl;
		buf[SWPC[i]]+=li2str(*upper_bound(jump_pos.begin(),jump_pos.end(),SWPC[i])*1+1,16);
	}
}

void replace(){
	replace_JL();
	replace_JEQ();
	replace_SWPC();
}

void generate(){
	if(inst[0]=="JL" || inst[0]=="JEQ") jump_pos.pb(sz(buf));
	if(inst[0]=="ADD") generate_R("100000");
	else if(inst[0]=="SUB") generate_R("100010");
	else if(inst[0]=="AND") generate_R("100100");
	else if(inst[0]=="OR") generate_R("100101");
	else if(inst[0]=="SLT") generate_R("101010");
	else if(inst[0]=="LWI") generate_I("100011");
	else if(inst[0]=="SWI") generate_I("101011");
	else if(inst[0]=="ADDI") generate_I("001000");
	else if(inst[0]=="BEQ") generate_I("000100");
	else if(inst[0]=="JL") generate_JL();
	else if(inst[0]=="JEQ") generate_JEQ();
	else if(inst[0]=="JR") generate_JR();
	else if(inst[0]=="SWPC") generate_SWPC();
	else{ cout<<"Cannot generate :"; rep(i,sz(inst)) cout<<inst[i]<<" "; cout<<endl;  }
}

void print(){
	rep(i,sz(buf)){
		rep(j,sz(buf[i])) fprintf(out0,"%c",buf[i][j]);
		fprintf(out0,"\n");
	}
	rep(i,sz(buf))rep(j,4){
		string str=buf[i].substr(j*8,8);
		char res=(char)0;
		rep(k,8)if(str[k]=='1') res+=bit(7-k);
		fprintf(out1,"%c",res);
	}
}

int main(){
	init();
	char c[MAX_LINE_CHAR];
	while(fgets(c,MAX_LINE_CHAR,in)){
		parse(c);
		if(sz(inst)==0) continue;
		if(inst[0][0]=='.') continue;
		if(sz(inst)==1 && inst[0][sz(inst[0])-1]==':'){
			label[inst[0].substr(0,sz(inst[0])-1)]=sz(buf)*INST_LEN;			
			continue;
		}
		generate();
	}
	replace();
	print();
	finish();
}
