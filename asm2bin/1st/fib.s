JL __main__
fib.37:
# [ Tail IfEq ] start 
	ADDI	r27 r0 0
	ADDI	r30 r0 1
	SLT	r28 r2 r27
	SLT	r29 r27 r2
	OR	r29 r28 r29
	SLT	r30 r29 r30
# [ Tail IfEq ] end 
	JEQ	r30 r0 IfEq_else.54
	ADDI	r2 r0 1
#blr
	LWI	r30 r3 -4
	JR	r30
#blr

IfEq_else.54:
# [ Tail IfEq ] start 
	ADDI	r27 r0 1
	ADDI	r30 r0 1
	SLT	r28 r2 r27
	SLT	r29 r27 r2
	OR	r29 r28 r29
	SLT	r30 r29 r30
# [ Tail IfEq ] end
	JEQ	r30 r0 IfEq_else.55
	ADDI	r2 r0 1
#blr
	LWI	r30 r3 -4
	JR	r30
#blr

IfEq_else.55:
	ADDI	r5 r2 -1
	SWI	r2 r3 0
#mflr [ NonTail CallDir ]
	ADDI	r2 r5 0
#SW r31 4(r3) [ NonTail CallDir ]
	SWPC	 r3 8 # to remenber program counter
	ADDI	r3 r3 12 # This is semi-original
	JL	fib.37
	ADDI	r3 r3 -12 # This is semi-original
#LW r31 4(r3) [ NonTail CallDir ]
#mtlr [ NonTail CallDir ]
	LWI	r5 r3 0
	ADDI	r5 r5 -2
	SWI	r2 r3 4
#mflr [ NonTail CallDir ]
	ADDI	r2 r5 0
#SW r31 12(r3) [ NonTail CallDir ]
	SWPC	 r3 16 # to remenber program counter
	ADDI	r3 r3 20 # This is semi-original
	JL	fib.37
	ADDI	r3 r3 -20 # This is semi-original
#LW r31 12(r3) [ NonTail CallDir ]
#mtlr [ NonTail CallDir ]
	LWI	r5 r3 4
	ADD	r2 r5 r2
#blr
	LWI	r30 r3 -4
	JR	r30
#blr

__main__:
	ADDI	r2 r0 10
#mflr [ NonTail CallDir ]
#SW r31 4(r3) [ NonTail CallDir ]
	SWPC	 r3 8 # to remenber program counter
	ADDI	r3 r3 12 # This is semi-original
	JL	fib.37
	ADDI	r3 r3 -12 # This is semi-original
#LW r31 4(r3) [ NonTail CallDir ]
#mtlr [ NonTail CallDir ]
	ADDI	r2 r2 0
#mflr [ NonTail CallDir ]
#SW r31 4(r3) [ NonTail CallDir ]
	SWPC	 r3 8 # to remenber program counter
	ADDI	r3 r3 12 # This is semi-original
	JL	min_caml_print_int
	ADDI	r3 r3 -12 # This is semi-original
#LW r31 4(r3) [ NonTail CallDir ]
#mtlr [ NonTail CallDir ]


final:
	ADDI r25 r25 25
