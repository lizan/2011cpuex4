#include "AsmToBin.h"




static void readStandardInput(vector<string> &asm_input){
	cerr<<"read from standard input"<<endl;
	string inst;
	while(getline(cin,inst)) asm_input.pb(inst);
}


static vector<string> parse(string sentence){
	vector<string> words;
	string str;
	string blank=",%"; // all position.
	string comment="#."; // only first character.
	rep(i,sz(blank))rep(j,sz(sentence))if(blank[i]==sentence[j]) sentence[j]=' ';

	for(stringstream ss(sentence);ss>>str;){
		rep(i,sz(comment))if(comment[i]==str[0]) return words;
		words.pb(str);
	}
	return words;
}


static void decode(vector<string> &words,vector<string> &buffer){
	if(sz(words)==1 && words[0][sz(words[0])-1]==':'){
		addLabel(words,buffer);
		return;
	}
	

	rep(i,sz(words[0])) words[0][i]=toupper(words[0][i]);
	rep(i,num_of_commands){
		if(commands[i]!=words[0]) continue;
		wordsToBin(words,buffer);
		return;
	}

	cerr<<"decode:            instruction not allocated"<<endl;
	rep(i,sz(words)) cerr<<words[i]<<" "; cerr<<endl;
	exit(0);
	
}


static void writeStandardOutput(vector<string> &buffer){
	rep(i,sz(buffer))rep(j,4){
		string str=buffer[i].substr(j*8,8);
		char res=(char)0;
		rep(k,8)if(str[k]=='1') res+=bit(7-k);
		cout<<res;
	}
}


static void printDebug(vector<string> &buffer){
	ofstream ofs;
	ofs.open("debug2.dat");
	rep(i,sz(buffer)) ofs<<buffer[i]<<endl;
	ofs.close();
	ofs.open("debug16.dat");
	rep(i,sz(buffer)){
		ofs<<" X\"";
		rep(j,8){
			string F="0123456789ABCDEF";
			li num=0;
			rep(k,4)if(buffer[i][j*4+(3-k)]=='1') num|=bit(k);
			ofs<<F[num];
		}
		ofs<<"\", ";
		if(i%4==3) ofs<<endl;
	}
	ofs.close();
}



void addStopLabel(vector<string> &asm_input){
	asm_input.pb("AsmToBinStopLabel:");
}

int main(){
	checkLibrary();
	vector<string> asm_input,buffer;

	readStandardInput(asm_input);	
	addStopLabel(asm_input);
	
	rep(i,sz(asm_input)){
		vector<string> words=parse(asm_input[i]);
		#ifdef AVOID_PRINT_INT
			bool found=false;
			rep(j,sz(words))if(words[j]=="min_caml_print_int") found=true;
			if(found) continue;
		#endif
		if(sz(words)==0) continue;
		decode(words,buffer);
	}
	
	insertLabel(buffer);

	printDebug(buffer);
	
	writeStandardOutput(buffer);
}


