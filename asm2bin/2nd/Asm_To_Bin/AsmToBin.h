#ifndef AsmToBin_H
#define AsmToBin_H

// #define DEBUG
// #define AVOID_PRINT_INT
 #define INIT_HEAP 0

#include <map>
#include <set>
#include <cmath>
#include <stack>
#include <queue>
#include <string>
#include <vector>
#include <bitset>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <iostream>
#include <algorithm>
using namespace std;
#define li       	long long int
#define rep(i,to)	for(li i=0;i<((li)(to));++i)
#define pb       	push_back
#define sz(v)    	((li)(v).size())
#define bit(n)   	(1ll<<(li)(n))
#define all(vec)	(vec).begin(),(vec).end()
#define each(i,c)	for(__typeof((c).begin()) i=(c).begin();i!=(c).end();i++)
#define MP        make_pair
#define F         first
#define S         second


extern string commands[];
extern int num_of_commands;

int main();
void checkLibrary();
void wordsToBin(vector<string> &words,vector<string> &buffer);



void insertLabel(vector<string> &buffer);
void addLabel(vector<string> &words,vector<string> &buffer);
#endif
