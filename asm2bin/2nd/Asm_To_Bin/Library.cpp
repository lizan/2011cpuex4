#include "AsmToBin.h"


int num_of_commands=35;
string commands[]={
	"ADD",
	"SUB",
	"MUL",
	"DIV",
	"AND",
	"OR",
	"SRI",
	"SLI",
	"SLT",
	"SEQ",
	"FADD",
	"FSUB",
	"FMUL",
	"FDIV",
	"FLT",
	"FEQ",
	"ADDI",
	"LWI",
	"SWI",
	"FLWI",
	"FSWI",
	"JR",
	"WRITE",
	"READ",
	"JL",
	"JAL",
	"JEQL",
	"SQRT",
	"SGT",
	"FLWL",
	"FGT",
	"INIT",
	"CLOSURE_ADDRESS",
	"CLOSURE_JUMP",
	"ASITIS"
};


static map<string,li> label;
static vector<pair<string,li> > jump_label;
static vector<pair<string,li> > jump_eq_label;
static vector<pair<string,li> > closure_label;
static li init_count=0;


void checkLibrary(){
	cerr<<endl;
	if(num_of_commands!=sizeof(commands)/sizeof(string)){
		cerr<<"num_of_commands doesn't match with sizeof of commands"<<endl;
		exit(0);
	}
	#ifdef DEBUG
		cerr<<"successfully checked Library.cpp"<<endl;
	#endif
}

	

static string numToBin(li num,li length){
	if(length!=5 && bit(length-1)<=abs(num)){ cerr<<"numToBin overflow"<<endl; exit(0); }
	string res=string(length,'0');
	rep(i,length)if(num&bit(i)) res[length-i-1]='1';
	return res;
}

static li stringToNum(string str,li length){
	if(!isdigit(str[0]) && str[0]!='-'){ cerr<<"stringToNum not number : "<<str<<endl; exit(0); }
	li res=0;
	stringstream ss(str);
	ss>>res;
	rep(i,sz(str))if(str[i]=='1' && length<=i){ cerr<<"stringToNum overflow"<<endl; exit(0); }
	return res%bit(length);
}

static string parseReg(string reg,li length){
	if(isdigit(reg[0])){ cerr<<"parseReg not register : "<<reg<<endl; exit(0); }
	while(reg[0]<'0' || '9'<reg[0]) reg=reg.substr(1);
	return numToBin(stringToNum(reg,length),length);
}



static void JMPL(vector<string> &words,vector<string> &buffer,string opcode){
	jump_label.pb(MP(words[1],sz(buffer)));
	buffer.pb(opcode);
}

static void JEQL(vector<string> &words,vector<string> &buffer){
	jump_eq_label.pb(MP(words[3],sz(buffer)));
	buffer.pb("000100"+parseReg(words[1],5)+parseReg(words[2],5));
}
	
static void JR(vector<string> &words,vector<string> &buffer){
	string str="000001";
	str+=parseReg(words[1],5);
	str+=string(21,'0');
	buffer.pb(str);
}
	


static void R(vector<string> &words,vector<string> &buffer,string hd_code,string tl_code){
	string str=hd_code;
	str+=parseReg(words[2],5);
	str+=parseReg(words[3],5);
	str+=parseReg(words[1],5);
	str+=string(5,'0');
	str+=tl_code;
	buffer.pb(str);
}

static void I(vector<string> &words,vector<string> &buffer,string opcode){
	string str=opcode;
	str+=parseReg(words[2],5);
	str+=parseReg(words[1],5);
	str+=numToBin(stringToNum(words[3],16),16);
	buffer.pb(str);
}

static void RWJR(vector<string> &words,vector<string> &buffer,string opcode){
	string str=opcode;
	str+=parseReg(words[1],5);
	str+=string(21,'0');
	buffer.pb(str);
}

static void SQRT(vector<string> &words,vector<string> &buffer){
	string str="010001";
	str+=parseReg(words[2],5);
	str+=parseReg(words[1],5);
	str+=string(16,'0');
	buffer.pb(str);
}



static void regNeg(vector<string> &buffer,string reg,string hdcode){
	vector<string> sub;
	sub.pb("SUB");
	sub.pb(reg);
	sub.pb("r0");
	sub.pb(reg);
	R(sub,buffer,hdcode,"100010");
}

	
static void GT(vector<string> &words,vector<string> &buffer,string hdcode){
	// SUB,SUB,SLT,SUB,SUB
	regNeg(buffer,words[2],hdcode);
	regNeg(buffer,words[3],hdcode);
	R(words,buffer,hdcode,"101010");
	regNeg(buffer,words[3],hdcode);
	regNeg(buffer,words[2],hdcode);
}

static void CLOSURE_ADDRESS(vector<string> &words,vector<string> &buffer){
	closure_label.pb(MP(words[2],sz(buffer)));
	string str="001000";
	str+=(parseReg("r0",5));
	str+=(parseReg(words[1],5));
	buffer.pb(str);
}

static void CLOSURE_JUMP(vector<string> &words,vector<string> &buffer){
	string str0="001000";
	str0+=(parseReg("r0",5));
	str0+=(parseReg("r31",5));
	str0+=numToBin(sz(buffer)+2,16);
	buffer.pb(str0);
	string str1="000001";
	str1+=parseReg(words[1],5);
	str1+=string(21,'0');
	buffer.pb(str1);
}


static void INIT(vector<string> &words,vector<string> &buffer){
	// addi,mul,addi,swi,sp++
	union float_int{
		float f;
		int i;
	}fi;
	stringstream ss(words[1]);
	ss>>fi.f;
	bool minus=false;
	if(fi.f<0) minus=true;
	fi.f=max(fi.f,-fi.f);
//cerr<<fi.f<<endl;
	vector<string> inst;
	{
		inst.clear();
		inst.pb("ADDI");
		inst.pb("r29");
		inst.pb("r0");
		stringstream sss;
		sss<<(fi.i>>20);
		inst.pb(sss.str());
		wordsToBin(inst,buffer);
	}
	{
		inst.clear();
		inst.pb("SLI");
		inst.pb("r29");
		inst.pb("r29");
		inst.pb("10");
		wordsToBin(inst,buffer);
	}
	{
		inst.clear();
		inst.pb("ADDI");
		inst.pb("r29");
		inst.pb("r29");
		stringstream sss;
		sss<<(fi.i>>10)%bit(10);
		inst.pb(sss.str());
		wordsToBin(inst,buffer);
	}
	{
		inst.clear();
		inst.pb("SLI");
		inst.pb("r29");
		inst.pb("r29");
		inst.pb("10");
		wordsToBin(inst,buffer);
	}
	{
		inst.clear();
		inst.pb("ADDI");
		inst.pb("r29");
		inst.pb("r29");
		stringstream sss;
		sss<<fi.i%bit(10);
		inst.pb(sss.str());
		wordsToBin(inst,buffer);
	}

	{
		inst.clear();
		inst.pb("SWI");
		inst.pb("r29");
		inst.pb("r3");
		inst.pb(numToBin(0,16));
		label[words[2]]=init_count++*4+INIT_HEAP;
		wordsToBin(inst,buffer);
	}
	
	if(minus){
		inst.clear();
		inst.pb("FLWI");
		inst.pb("f29");
		inst.pb("r3");
		inst.pb("0");
		wordsToBin(inst,buffer);
	}
	
	if(minus){
		inst.clear();
		inst.pb("FSUB");
		inst.pb("f29");
		inst.pb("f0");
		inst.pb("f29");
		wordsToBin(inst,buffer);
	}
	
	if(minus){
		inst.clear();
		inst.pb("FSWI");
		inst.pb("f29");
		inst.pb("r3");
		inst.pb("0");
		wordsToBin(inst,buffer);
	}
	
	{
		inst.clear();
		inst.pb("ADDI");
		inst.pb("r3");
		inst.pb("r3");
		inst.pb("4");
		wordsToBin(inst,buffer);
	}
	
	
}

static void FLWL(vector<string> &words,vector<string> &buffer){
	vector<string> inst;
	{
		inst.pb("LWI");
		inst.pb("r0");
		inst.pb("r0");
		inst.pb("1");
		wordsToBin(inst,buffer);
	}
	{
		inst.clear();
		inst.pb("FLWI");
		inst.pb(words[1]);
		inst.pb("r0");
		if(label.find(words[2])==label.end()){
			cerr<<"label not found : "<<words[2]<<endl;
			exit(0);
		}
		stringstream ss;
		ss<<label[words[2]];
		inst.pb(ss.str());
		wordsToBin(inst,buffer);
	}
	{
		inst.clear();
		inst.pb("SUB");
		inst.pb("r0");
		inst.pb("r0");
		inst.pb("r0");
		wordsToBin(inst,buffer);
	}
}


void DIV(vector<string> &words,vector<string> &buffer){
	vector<string> inst;
	{
		inst.clear();
		inst.pb("SRI");
		inst.pb(words[1]);
		inst.pb(words[2]);
		inst.pb("1");
		wordsToBin(inst,buffer);
	}
}

void ASITIS(vector<string> &words,vector<string> &buffer){
	buffer.pb(words[1]);
}

void wordsToBin(vector<string> &words,vector<string> &buffer){
	#ifdef DEBUG
		cerr<<"wordsToBin:  ";
		rep(i,sz(words)) cerr<<words[i]<<" ";  cerr<<endl;
	#endif
	if(words[0]=="ADD"  ){ R(words,buffer,"000000","100000"); return; }
	if(words[0]=="SUB"  ){ R(words,buffer,"000000","100010"); return; }
	if(words[0]=="MUL"  ){ R(words,buffer,"000000","011000"); return; }
//	if(words[0]=="DIV"  ){ R(words,buffer,"000000","011010"); return; }
if(words[0]=="DIV"){ DIV(words,buffer); return; }
	if(words[0]=="AND"  ){ R(words,buffer,"000000","100100"); return; }
	if(words[0]=="OR"   ){ R(words,buffer,"000000","100101"); return; }
	if(words[0]=="SR"   ){ R(words,buffer,"000000","000010"); return; }
	if(words[0]=="SL"   ){ R(words,buffer,"000000","000011"); return; }
	if(words[0]=="SLT"  ){ R(words,buffer,"000000","101010"); return; }
	if(words[0]=="SEQ"  ){ R(words,buffer,"000000","101011"); return; }
	if(words[0]=="FADD" ){ R(words,buffer,"010000","100000"); return; }
	if(words[0]=="FSUB" ){ R(words,buffer,"010000","100010"); return; }
	if(words[0]=="FMUL" ){ R(words,buffer,"010000","011000"); return; }
	if(words[0]=="FDIV" ){ R(words,buffer,"010000","011010"); return; }
	if(words[0]=="FLT"  ){ R(words,buffer,"010000","101010"); return; }
	if(words[0]=="FEQ"  ){ R(words,buffer,"010000","101011"); return; }
	if(words[0]=="ADDI" ){ I(words,buffer,"001000"); return; }
	if(words[0]=="LWI"  ){ I(words,buffer,"100011"); return; }
	if(words[0]=="SWI"  ){ I(words,buffer,"100010"); return; }
	if(words[0]=="FLWI" ){ I(words,buffer,"111001"); return; }
	if(words[0]=="FSWI" ){ I(words,buffer,"111000"); return; }
	if(words[0]=="SRI"  ){ I(words,buffer,"001010"); return; }
	if(words[0]=="SLI"  ){ I(words,buffer,"001011"); return; }
	
	if(words[0]=="JR"   ){ RWJR(words,buffer,"000001"); return; }
	if(words[0]=="WRITE"){ RWJR(words,buffer,"111110"); return; }
	if(words[0]=="READ" ){ RWJR(words,buffer,"111111"); return; }
	
	if(words[0]=="SGT"  ){ GT(words,buffer,"000000"); return; }
	if(words[0]=="FGT"  ){ GT(words,buffer,"010000"); return; }
	
	if(words[0]=="JL"   ){ JMPL(words,buffer,"000010"); return; }
	if(words[0]=="JAL"  ){ JMPL(words,buffer,"000011"); return; }
	
	if(words[0]=="JEQL" ){ JEQL(words,buffer); return; }
	if(words[0]=="SQRT" ){ SQRT(words,buffer); return; }
	if(words[0]=="FLWL" ){ FLWL(words,buffer); return; }
	if(words[0]=="INIT" ){ INIT(words,buffer); return; }
	
	
	if(words[0]=="CLOSURE_ADDRESS"){ CLOSURE_ADDRESS(words,buffer); return; }
	if(words[0]=="CLOSURE_JUMP"){ CLOSURE_JUMP(words,buffer); return; }
	
	if(words[0]=="ASITIS"){ ASITIS(words,buffer); return; }

	cerr<<"not implemented at wordsToBin : "<<words[0]<<endl;
	exit(0);
}

void addLabel(vector<string> &words,vector<string> &buffer){
	label[words[0].substr(0,sz(words[0])-1)]=sz(buffer);
}
		

void insertLabel(vector<string> &buffer){
	rep(i,sz(jump_label)){
//cerr<<jump_label[i].F<<" "<<label[jump_label[i].F]<<endl;
		if(label.find(jump_label[i].F)==label.end()){
			cerr<<"label not found : "<<jump_label[i].F<<endl;
		}
		buffer[jump_label[i].S]+=numToBin(label[jump_label[i].F],26);
	}
	rep(i,sz(jump_eq_label)){
	  //cerr<<jump_eq_label[i].F<<" "<<label[jump_eq_label[i].F]<<" "<<endl;
		if(label.find(jump_eq_label[i].F)==label.end()){
			cerr<<"label not found : "<<jump_eq_label[i].F<<endl;
		}
		buffer[jump_eq_label[i].S]+=numToBin(label[jump_eq_label[i].F]-jump_eq_label[i].S-1,16);
	}
	
	rep(i,sz(closure_label)){
//cerr<<closure_label[i].F<<"*"<<label[closure_label[i].F]<<endl; 
		if(label.find(closure_label[i].F)==label.end()){
			cerr<<"label not found : "<<closure_label[i].F<<endl;
		}
		buffer[closure_label[i].S]+=numToBin(label[closure_label[i].F],16);
	}
}	

