#include "instruction.h"
#include "processor.h"
#include <functional>
#include "fpu/fpu.h"

namespace ugl0sim {

namespace {

#define DEFINE_INSTRUCTION_PART(NAME, MASK, BIT) \
  const unsigned int k##NAME##Mask = MASK; \
  const unsigned int k##NAME##Bit = BIT; \
  inline unsigned int get##NAME(const unsigned int op) { \
    return (op & k##NAME##Mask) >> k##NAME##Bit; \
  }

DEFINE_INSTRUCTION_PART(Opcode, 0xFC000000, 26)
DEFINE_INSTRUCTION_PART(Funct,  0x0000003F,  0)
DEFINE_INSTRUCTION_PART(Rs,     0x03E00000, 21)
DEFINE_INSTRUCTION_PART(Rt,     0x001F0000, 16)
DEFINE_INSTRUCTION_PART(Rd,     0x0000F800, 11)
DEFINE_INSTRUCTION_PART(ImI,    0x0000FFFF,  0)
DEFINE_INSTRUCTION_PART(ImJ,    0x03FFFFFF,  0)

#undef DEFINE_INSTRUCTION_PART

template<typename _T>
class RInstruction : public Instruction {
 protected:
  unsigned int rs, rt, rd;
  _T funct;
 public:
  RInstruction(unsigned int op, const char* opname) : Instruction(), funct() {
    rs = getRs(op);
    rt = getRt(op);
    rd = getRd(op);
    description = QString("%1 rs=%2 rt=%3 rd=%4").arg(opname).arg(rs).arg(rt).arg(rd);
  }
  virtual void run(Processor* processor) {
    processor->setRegister(
        rd,
        funct(processor->getRegister(rs), processor->getRegister(rt)));
    processor->incrementCounter();
  }
};

typedef RInstruction<std::plus<int> > AddInstruction;
typedef RInstruction<std::minus<int> > SubInstruction;
#if __APPLE__
template<typename _Tp>
  struct bit_and : public std::binary_function<_Tp, _Tp, _Tp>
  {
    _Tp
    operator()(const _Tp& __x, const _Tp& __y) const
    { return __x & __y; }
  };

template<typename _Tp>
  struct bit_or : public std::binary_function<_Tp, _Tp, _Tp>
  {
    _Tp
    operator()(const _Tp& __x, const _Tp& __y) const
    { return __x | __y; }
  };
#else
using std::bit_and;
using std::bit_or;
#endif
typedef RInstruction<bit_and<int> > AndInstruction;
typedef RInstruction<bit_or<int> > OrInstruction;
typedef RInstruction<std::less<int> > SltInstruction;
typedef RInstruction<std::equal_to<int> > SeqInstruction;

template<typename _T>
class IInstruction : public Instruction {
 protected:
  unsigned int rs, rt;
  short im;
  _T funct;
 public:
  IInstruction(unsigned int op, const char* opname) : Instruction(), funct() {
    rs = getRs(op);
    rt = getRt(op);
    im = (short)getImI(op);
    description = QString("%1 rs=%2 rt=%3 im=%4").arg(opname).arg(rs).arg(rt).arg(im);
  }
  virtual void run(Processor* processor) {
    processor->setRegister(
        rt,
        funct(processor->getRegister(rs), im));
    processor->incrementCounter();
  }
};

class BeqInstruction : public IInstruction<std::plus<int> > {
 public:
  BeqInstruction(unsigned int op, const char *opname) : IInstruction<std::plus<int> >(op, opname) {}
  virtual void run(Processor* processor) {
    if (processor->getRegister(rs) == processor->getRegister(rt)) {
      processor->setCounter(processor->getCounter() + im);
    }
    processor->incrementCounter();
  }
};

class JrInstruction : public IInstruction<std::plus<int> > {
 public:
  JrInstruction(unsigned int op, const char *opname) : IInstruction<std::plus<int> >(op, opname) {}
  virtual void run(Processor* processor) {
    processor->setCounter(processor->getRegister(rs));
  }
};

class LwInstruction : public IInstruction<std::plus<int> > {
 public:
  LwInstruction(unsigned int op, const char *opname) : IInstruction<std::plus<int> >(op, opname) {}
  virtual void run(Processor* processor) {
    processor->setRegister(
        rt,
        processor->getMemory()->getWord(
            processor->getRegister(rs) + im));
    processor->incrementCounter();
  }
};

class SwInstruction : public IInstruction<std::plus<int> > {
 public:
  SwInstruction(unsigned int op, const char *opname) : IInstruction<std::plus<int> >(op, opname) {}
  virtual void run(Processor* processor) {
    processor->getMemory()->setWord(processor->getRegister(rs) + im,
                                    processor->getRegister(rt));
    processor->incrementCounter();
  }
};

typedef IInstruction<std::plus<int> > AddiInstruction;
typedef IInstruction<std::less<int> > SltiInstruction;
typedef IInstruction<std::equal_to<int> > SeqiInstruction;

class JInstruction : public Instruction {
 protected:
  unsigned int im;
 public:
  JInstruction(unsigned int op, const char *opname) : Instruction() {
    im = getImJ(op);
    description = QString("%1 im=%2").arg(opname).arg(im);
  }
  virtual void run(Processor* processor) {
    processor->setCounter(im);
  }
};

class JalInstruction : public JInstruction {
 public:
  JalInstruction(unsigned int op, const char *opname) : JInstruction(op, opname) {}
  virtual void run(Processor* processor) {
    processor->incrementCounter();
    processor->setRegister(31, processor->getCounter());
    processor->setCounter(im);
  }
};

class ReadInstruction : public IInstruction<std::plus<int> > {
 public:
  ReadInstruction(unsigned int op, const char *opname) : IInstruction<std::plus<int> >(op, opname) {}
  virtual void run(Processor* processor) {
    processor->read(rs);
    processor->incrementCounter();
  }
};

class WriteInstruction : public IInstruction<std::plus<int> > {
 public:
  WriteInstruction(unsigned int op, const char *opname) : IInstruction<std::plus<int> >(op, opname) {}
  virtual void run(Processor* processor) {
    processor->write(rs);
    processor->incrementCounter();
  }
};

// UGL1 Instruction Set
typedef RInstruction<std::multiplies<int> > MulInstruction;
typedef RInstruction<std::divides<int> > DivInstruction;

template<typename _Tp>
struct shift_left : public std::binary_function<_Tp, _Tp, _Tp>
{
  _Tp
  operator()(const _Tp& __x, const _Tp& __y) const
  { return (unsigned int)(__x) << (__y & 0x1F); }
};

template<typename _Tp>
struct shift_right : public std::binary_function<_Tp, _Tp, _Tp>
{
  _Tp
  operator()(const _Tp& __x, const _Tp& __y) const
  { return (unsigned int)(__x) >> (__y & 0x1F); }
};
typedef RInstruction<shift_right<int> > SrInstruction;
typedef RInstruction<shift_left<int> > SlInstruction;
typedef IInstruction<shift_right<int> > SriInstruction;
typedef IInstruction<shift_left<int> > SliInstruction;

class FLwInstruction : public LwInstruction {
 public:
  FLwInstruction(unsigned int op, const char *opname) : LwInstruction(op, opname) {
    rt |= 0x20;
  }
};

class FSwInstruction : public SwInstruction {
 public:
  FSwInstruction(unsigned int op, const char *opname) : SwInstruction(op, opname) {
    rt |= 0x20;
  }
};

template<typename _T>
class FRInstruction : public RInstruction<_T> {
 public:
  FRInstruction(unsigned int op, const char *opname) : RInstruction<_T>(op, opname) {
    this->rs |= 0x20;
    this->rt |= 0x20;
    this->rd |= 0x20;
  }
};

template<typename _T>
class FRSInstruction : public RInstruction<_T> {
 public:
  FRSInstruction(unsigned int op, const char *opname) : RInstruction<_T>(op, opname) {
    this->rs |= 0x20;
    this->rt |= 0x20;
  }
};

typedef FRInstruction<Cfadd> FAddInstruction;
typedef FRInstruction<Cfsub> FSubInstruction;
typedef FRInstruction<Cfmul> FMulInstruction;
typedef FRInstruction<Cfdiv> FDivInstruction;
typedef FRSInstruction<Cfslt> FSLTInstruction;
typedef FRSInstruction<Cfeq> FEQInstruction;

class FsqrtInstruction : public FRInstruction<Csqrt> {
public:
  FsqrtInstruction(unsigned int op, const char* opname) : FRInstruction<Csqrt>(op, opname) {
    this->rd = this->rt;
  }
};

class EndInstruction : public Instruction {
public:
  EndInstruction() : Instruction() {
    description = QString("END");
  }
  virtual void run(Processor* processor) {
    processor->stop();
  }
};

// InstructionMap
class InstructionMap {
 public:
  struct InstGenerator {
    virtual Instruction* newInst(unsigned int op) const { return NULL; }
  };
 private:
  static InstGenerator defaultGenerator;
  InstructionMap() {}
 public:
  static int regist(unsigned int op, unsigned mask, InstGenerator* gen) {
    opsHash[op & mask] = gen;
    maskHash[getOpcode(op)] = mask;
    return 0;
  }
  static Instruction* get(unsigned int op) {
    const unsigned int opcode = getOpcode(op);
    const unsigned int mask = maskHash.value(opcode, 0xFC000000);
    return opsHash.value(op & mask, &defaultGenerator)->newInst(op);
  }
 private:
  static QHash<unsigned int, InstGenerator*> opsHash;
  static QHash<unsigned int, unsigned int> maskHash;
};
InstructionMap::InstGenerator InstructionMap::defaultGenerator;
QHash<unsigned int, InstructionMap::InstGenerator*> InstructionMap::opsHash;
QHash<unsigned int, unsigned int> InstructionMap::maskHash;

#define DEFINE_INSTRUCTION(OPNAME, OPCLASS, OPCODE, OPMASK) \
  struct __##OPNAME##__ { \
    static struct Generator : InstructionMap::InstGenerator { \
      virtual Instruction* newInst(unsigned int op) const { \
        return new OPCLASS(op, #OPNAME); \
      } \
    } f; \
    static int p; \
  }; \
  __##OPNAME##__::Generator __##OPNAME##__::f; \
  int __##OPNAME##__::p(InstructionMap::regist(OPCODE, OPMASK, &f))


// Instruction Set
DEFINE_INSTRUCTION(ADD,  AddInstruction,  0x00000020, 0xFC00003F);
DEFINE_INSTRUCTION(SUB,  SubInstruction,  0x00000022, 0xFC00003F);
DEFINE_INSTRUCTION(MUL,  MulInstruction,  0x00000018, 0xFC00003F);
DEFINE_INSTRUCTION(DIV,  DivInstruction,  0x0000001A, 0xFC00003F);
DEFINE_INSTRUCTION(AND,  AndInstruction,  0x00000024, 0xFC00003F);
DEFINE_INSTRUCTION(OR,   OrInstruction,   0x00000025, 0xFC00003F);
DEFINE_INSTRUCTION(SLT,  SltInstruction,  0x0000002A, 0xFC00003F);
DEFINE_INSTRUCTION(SEQ,  SeqInstruction,  0x0000002B, 0xFC00003F);
DEFINE_INSTRUCTION(SR,   SrInstruction,   0x00000002, 0xFC00003F);
DEFINE_INSTRUCTION(SL,   SlInstruction,   0x00000003, 0xFC00003F);

DEFINE_INSTRUCTION(ADDI, AddiInstruction, 0x20000000, 0xFC000000);
DEFINE_INSTRUCTION(SRI,  SriInstruction,  0x28000000, 0xFC000000);
DEFINE_INSTRUCTION(SLI,  SliInstruction,  0x2C000000, 0xFC000000);

DEFINE_INSTRUCTION(JR,   JrInstruction,   0x04000000, 0xFC000000);
DEFINE_INSTRUCTION(J,    JInstruction,    0x08000000, 0xFC000000);
DEFINE_INSTRUCTION(JAL,  JalInstruction,  0x0C000000, 0xFC000000);
DEFINE_INSTRUCTION(BEQ,  BeqInstruction,  0x10000000, 0xFC000000);

DEFINE_INSTRUCTION(FADD, FAddInstruction, 0x40000020, 0xFC00003F);
DEFINE_INSTRUCTION(FSUB, FSubInstruction, 0x40000022, 0xFC00003F);
DEFINE_INSTRUCTION(FMUL, FMulInstruction, 0x40000018, 0xFC00003F);
DEFINE_INSTRUCTION(FDIV, FDivInstruction, 0x4000001A, 0xFC00003F);
DEFINE_INSTRUCTION(FSLT, FSLTInstruction, 0x4000002A, 0xFC00003F);
DEFINE_INSTRUCTION(FSEQ, FEQInstruction,  0x4000002B, 0xFC00003F);
DEFINE_INSTRUCTION(FSQRT,FsqrtInstruction,0x44000000, 0xFC000000);

DEFINE_INSTRUCTION(LW,   LwInstruction,   0x8C000000, 0xFC000000);
DEFINE_INSTRUCTION(SW,   SwInstruction,   0x88000000, 0xFC000000);
DEFINE_INSTRUCTION(FLW,  FLwInstruction,  0xE4000000, 0xFC000000);
DEFINE_INSTRUCTION(FSW,  FSwInstruction,  0xE0000000, 0xFC000000);
DEFINE_INSTRUCTION(READ, ReadInstruction, 0xFC000000, 0xFC000000);
DEFINE_INSTRUCTION(WRITE,WriteInstruction,0xF8000000, 0xFC000000);

#undef DEFINE_INSTRUCTION
}

Instruction* newInstruction(const unsigned int& op) {
  return InstructionMap::get(op);
}

Instruction* endInstruction() {
  static Instruction* inst = new EndInstruction();
  return inst;
}

void Instruction::break_(Processor *processor) {
  processor->stop();
}

} // namespace ugl0sim
