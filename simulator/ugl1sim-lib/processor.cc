#include "processor.h"
#include "memory.h"
#include "instruction.h"
#include "ugl0sim-lib_global.h"
#include <QVector>

namespace ugl0sim {

const unsigned int kProgramMemoryStart = 0x00;

Processor::Processor(QObject* parent) :
    QObject(parent), program_counter_(0), memory_(),
    input(NULL), output(NULL) {
  register_ = new unsigned int[64];
  reset();
}

void Processor::reset() {
  for (int i = 0; i < 64; ++i) {
    register_[i] = 0;
  }
  isRunning = true;
  program_counter_ = kProgramMemoryStart;
}

void Processor::loadProgram(const Program &program) {
  program_ = program;
  program_.append(endInstruction());
  reset();
}

const Program& Processor::getProgram() {
  return program_;
}

void Processor::stop() {
  isRunning = false;
}

void Processor::run() {
  long long n = 0;
  isRunning = true;
  Instruction** prog = new Instruction*[program_.size()];
  for(int i = 0; i < program_.size(); ++i) {
    prog[i] = program_[i];
  }
  qDebug("Run: %d", isRunning);
  for(; isRunning;) {
//    qDebug("hoge");
    (*prog[program_counter_])(this);
    ++n;
  }
  isRunning = false;
  qDebug("Total Ops: %lld", n);
}

} //  namespace ugl0sim
