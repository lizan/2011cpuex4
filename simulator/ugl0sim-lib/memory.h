#ifndef MEMORY_H
#define MEMORY_H

#include <QObject>
#include <QHash>

namespace ugl0sim {

class Memory : public QObject
{
  Q_OBJECT
 public:
  explicit Memory(QObject *parent = 0);
  virtual ~Memory();
  static const int BLOCK_SIZE;
  static const int WORD_SIZE;

 private:

  QHash<int, std::string> data;
  std::string& getBlock(unsigned int index);
 signals:

 public slots:
  int getWord(unsigned int index);
  void setWord(unsigned int index, int data);
  unsigned char getByte(unsigned int index);
  void setByte(unsigned int index, unsigned char data);

};

} // namespace ugl0sim

#endif // MEMORY_H
