#ifndef UGL0SIMLIB_GLOBAL_H
#define UGL0SIMLIB_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(UGL0SIMLIB_LIBRARY)
#  define UGL0SIMLIBSHARED_EXPORT Q_DECL_EXPORT
#else
#  define UGL0SIMLIBSHARED_EXPORT Q_DECL_IMPORT
#endif

namespace ugl0sim {

extern const unsigned int kProgramMemoryStart;

} // namespace ugl0sim

#endif // UGL0SIMLIB_GLOBAL_H
