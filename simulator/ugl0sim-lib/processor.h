#ifndef PROCESSOR_H
#define PROCESSOR_H

#include <QObject>
#include <QTextStream>
#include <vector>
#include "memory.h"
#include "instruction.h"
#include "ugl0sim-lib_global.h"

namespace ugl0sim {

const unsigned int kInstructionSize = sizeof(unsigned int);

class Processor : public QObject
{
  Q_OBJECT
 public:
  explicit Processor(QIODevice* input, QIODevice* output, QObject *parent = 0);
 private:
  unsigned int register_[32];

  unsigned int program_counter_;
  Memory memory_;
  QTextStream input, output;

 signals:
  void registerChanged(int, int);

 public slots:
  void reset();
  void incrementCounter();
  void setCounter(unsigned int count);
  unsigned int getCounter();
  void setRegister(unsigned int index, int data);
  int getRegister(unsigned int index);
  Memory* getMemory();
  void run(Program* program);
  void read(unsigned int index);
  void write(unsigned int index);

};

inline void Processor::incrementCounter() {
  ++program_counter_;
}

inline void Processor::setCounter(unsigned int count) {
  program_counter_ = count / kInstructionSize;
}

inline unsigned int Processor::getCounter() {
  return program_counter_;
}

inline void Processor::setRegister(unsigned int index, int data) {
  register_[index] = data;
//  emit registerChanged(index, data);
}

inline int Processor::getRegister(unsigned int index) {
  return register_[index];
}

inline Memory* Processor::getMemory() {
  return &memory_;
}

inline void Processor::read(unsigned int index) {
  int data;
  input >> data;
  setRegister(index, data);
}

inline void Processor::write(unsigned int index) {
  output << getRegister(index) << endl;
}

} // namespace ugl0sim

#endif // PROCESSOR_H
