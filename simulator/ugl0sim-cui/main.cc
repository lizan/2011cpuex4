#include <QtCore>
#include <processor.h>
#include <instruction.h>

using namespace ugl0sim;

class MainThread : public QThread {
 public:
  MainThread(const QCoreApplication& app)
      : app(&app) {
  }

  void run() {
    connect(this, SIGNAL(finished()), qApp, SLOT(quit()));
    const QStringList& args = app->arguments();
    QFile program_file(args.at(1));

    program_file.open(QFile::ReadOnly);
    if(!program_file.isReadable()) {
      qDebug("Coundn't open %s", args.at(1).toUtf8().constData());
      return;
    }

    QDataStream program_stream(&program_file);
    program_stream.setByteOrder(QDataStream::BigEndian);

    Program program;

    qDebug("parsing...");
    while(!program_stream.atEnd()) {
      unsigned int instruction;
      program_stream >> instruction;
      Instruction* inst = newInstruction(instruction);
      if (inst == NULL) {
        qDebug("Unable parse %x at %d", instruction, program.size());
        return;
      }
      program.push_back(inst);
    }

    program_file.close();

    QFile input, output;
    input.open(stdin, QFile::ReadOnly);
    output.open(stdout, QFile::WriteOnly);

    Processor processor(&input, &output);

    qDebug("running...");
    processor.run(&program);

    for (int i = 0; i < 32; ++i) {
      int r = processor.getRegister(i);
      qDebug("r[%02x] = %08x (%d)", i, r, r);
    }

    processor.deleteLater();

    return;
  }
 private:
  const QCoreApplication* app;
};

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    MainThread thread(a);
    thread.start();

    return a.exec();
}
