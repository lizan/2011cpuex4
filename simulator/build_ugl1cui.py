#!/usr/bin/env python

import os
import sys
import commands

def run_command(command):
  print command
  status, output = commands.getstatusoutput(command)
  if output:
    print output
  if status != 0:
    print command, "exits with status", status
    sys.exit(status)

def main():
  build_dir = os.path.dirname(os.path.abspath(__file__))

  os.chdir(os.path.join(build_dir, 'ugl1sim-lib'))
  run_command('qmake')
  run_command('make')

  lib_mtime = os.stat('libugl1sim-lib.a').st_mtime

  os.chdir(os.path.join(build_dir, 'ugl1sim-cui'))
  run_command('qmake')
  try:
    if os.stat('ugl1sim-cui').st_mtime < lib_mtime:
      os.remove('ugl1sim-cui')
  except:
    pass

  run_command('make')

if __name__ == "__main__":
  main()
