#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>

QT_BEGIN_NAMESPACE
class QTableWidget;
class QLabel;
class QPushButton;
class QTextEdit;
class QByteArray;
QT_END_NAMESPACE

namespace ugl0sim {
class Processor;
}

class MainWidget : public QWidget
{
  Q_OBJECT
 public:
  explicit MainWidget(ugl0sim::Processor* processor, QWidget *parent = 0);

 signals:

 public slots:
  void syncStatus();
  void run();
  void step();
  void threadFinishied();
  void browseInputFile();
  void inputChanged();
  void viewOutput();
  void saveOutput();
  void reset();
  void editBp();

 private:
  QTableWidget* registerWidget;
  QLabel* pcLabel;
  QLabel* outputLengthLabel;
  QTextEdit* inputDataEdit;
  QPushButton* runButton;
  QPushButton* stepButton;
  QPushButton* resetButton;
  QPushButton* inputFileButton;
  QPushButton* viewOutputButton;
  QPushButton* saveOutputButton;
  ugl0sim::Processor* processor;
  bool isRunning;
  int breakPoint;

  QByteArray inputData;
  QByteArray outputData;
  QDataStream inputStream;
  QDataStream outputStream;
};

#endif // MAINWIDGET_H
