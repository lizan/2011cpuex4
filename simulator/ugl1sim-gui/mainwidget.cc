#include "mainwidget.h"
#include "processor.h"
#include "instruction.h"
#include <QtGui>

using ugl0sim::Processor;
using ugl0sim::Program;

namespace {
union Cell {
  unsigned int ui;
  int i;
  float f;
};

class RunningThread : public QThread {
 private:
  Processor* processor;
 public:
  explicit RunningThread(Processor *processor) : QThread(), processor(processor) {}
  void run() {
    connect(this, SIGNAL(finished()), this, SLOT(deleteLater()));
    processor->run();
  }
};

class ProgramDelegate : public QItemDelegate {
public:
  ProgramDelegate(QObject* parent = NULL) : QItemDelegate(parent) {}
  QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const {
    return new QCheckBox(parent);
  }
};

class ProgramModel : public QAbstractTableModel {
  const Program* program;
public:
  ProgramModel(const Program* program, QObject* parent = NULL) : QAbstractTableModel(parent), program(program) {

  }
  int rowCount(const QModelIndex &parent) const {
    return program->size();
  }
  int columnCount(const QModelIndex &parent) const {
    return 2;
  }
  QVariant data(const QModelIndex &index, int role) const {
    if (index.isValid() && role == Qt::DisplayRole) {
      if (index.column() == 1)
        return QVariant((*program)[index.row()]->getDescription());
      else
        return QVariant((*program)[index.row()]->isBreak());
    }
    else
      return QVariant();
  }
};

class EditBreakpointsWindow : public QDialog {
  QTableView* programView;
  QPushButton* doneButton;
  ProgramModel* model;
 public:
  EditBreakpointsWindow(const Program* program, QWidget* parent = NULL) : QDialog(parent) {
    QVBoxLayout* mainLayout = new QVBoxLayout;
    programView = new QTableView;
    model = new ProgramModel(program);
    programView->setModel(model);
    programView->setItemDelegate(new ProgramDelegate);
    mainLayout->addWidget(programView);
    mainLayout->addWidget(doneButton = new QPushButton("Done"));
    connect(doneButton, SIGNAL(clicked()), this, SLOT(accept()));
    setLayout(mainLayout);
  }
};

}

MainWidget::MainWidget(Processor* processor, QWidget *parent)
    : QWidget(parent), processor(processor), isRunning(false), breakPoint(-1) {
  registerWidget = new QTableWidget(32, 5);
  QStringList registerLabelList;
  QFont monospace("monospace");
  monospace.setStyleHint(QFont::TypeWriter);
  for (int i = 0; i < 32; ++i) {
    registerLabelList.append(QString("r[%1]").arg(i, 2, 16, QChar('0')));
    registerWidget->setItem(i, 0, new QTableWidgetItem);
    registerWidget->setItem(i, 1, new QTableWidgetItem);
    registerWidget->item(i, 0)->setTextAlignment(Qt::AlignRight);
    registerWidget->item(i, 1)->setTextAlignment(Qt::AlignRight);
    registerWidget->item(i, 0)->setFont(monospace);
    registerWidget->item(i, 1)->setFont(monospace);
  }
  for (int i = 0; i < 32; ++i) {
    registerWidget->setItem(i, 3, new QTableWidgetItem);
    registerWidget->setItem(i, 4, new QTableWidgetItem);
    registerWidget->item(i, 3)->setTextAlignment(Qt::AlignRight);
    registerWidget->item(i, 4)->setTextAlignment(Qt::AlignRight);
    registerWidget->item(i, 3)->setFont(monospace);
    registerWidget->item(i, 4)->setFont(monospace);
  }
  registerWidget->resizeRowsToContents();
  registerWidget->setVerticalHeaderLabels(registerLabelList);
  registerWidget->setHorizontalHeaderLabels(QString("Int Value,Int Hex, ,Float Value, Float Hex").split(","));

  QVBoxLayout* registerLayout = new QVBoxLayout;
  registerLayout->addWidget(new QLabel("Registers: "));
  registerLayout->addWidget(registerWidget);

  QFormLayout* rightLayout = new QFormLayout;
  pcLabel = new QLabel("000000");
  rightLayout->addRow("Program Counter: ", pcLabel);

  QGridLayout* buttonsLayout = new QGridLayout;
  runButton = new QPushButton("Run");
  stepButton = new QPushButton("Step");
  resetButton = new QPushButton("Reset");
  buttonsLayout->addWidget(runButton, 0, 0, 1, 2);
  buttonsLayout->addWidget(stepButton, 1, 0);
  buttonsLayout->addWidget(resetButton, 1, 1);

  connect(runButton, SIGNAL(clicked()), this, SLOT(run()));
  connect(stepButton, SIGNAL(clicked()), this, SLOT(step()));
  connect(resetButton, SIGNAL(clicked()), this, SLOT(reset()));

  rightLayout->addRow(buttonsLayout);

  inputFileButton = new QPushButton("Browse...");
  rightLayout->addRow("Load Input File: ", inputFileButton);
  connect(inputFileButton, SIGNAL(clicked()), this, SLOT(browseInputFile()));

  inputDataEdit = new QTextEdit;
  rightLayout->addRow(inputDataEdit);
  connect(inputDataEdit, SIGNAL(textChanged()), this, SLOT(inputChanged()));

  outputLengthLabel = new QLabel("000000");
  rightLayout->addRow("Output Length: ", outputLengthLabel);

  viewOutputButton = new QPushButton("View Output");
  saveOutputButton = new QPushButton("Save Output");
  rightLayout->addRow(viewOutputButton, saveOutputButton);
  connect(viewOutputButton, SIGNAL(clicked()), this, SLOT(viewOutput()));
  connect(saveOutputButton, SIGNAL(clicked()), this, SLOT(saveOutput()));

  QPushButton* editBpButton = new QPushButton("Set Break Point...");
  rightLayout->addRow(editBpButton);
  connect(editBpButton, SIGNAL(clicked()), this, SLOT(editBp()));

  QHBoxLayout* mainLayout = new QHBoxLayout;
  mainLayout->addLayout(registerLayout);
  mainLayout->addLayout(rightLayout);
  setLayout(mainLayout);
//  addWidget(new QTextEdit(this));

  syncStatus();
//  pcLabel->setEnabled(false);
//  registerWidget->setEnabled(false);
}

void MainWidget::syncStatus() {
  for (int i = 0; i < 32; ++i) {
    Cell r;
    r.ui = processor->getRegister(i);
    registerWidget->item(i, 0)->setText(QString("%1").arg(r.i, 10));
    registerWidget->item(i, 1)->setText(QString("0x%1").arg(r.ui, 8, 16, QChar('0')));
  }
  for (int i = 0; i < 32; ++i) {
    Cell r;
    r.ui = processor->getRegister(i | 0x20);
    registerWidget->item(i, 3)->setText(QString("%1").arg(r.f, 0, 'e'));
    registerWidget->item(i, 4)->setText(QString("0x%1").arg(r.ui, 8, 16, QChar('0')));
  }
  registerWidget->resizeColumnsToContents();
  pcLabel->setText(QString("%1").arg(processor->getCounter(), 6, 10, QChar('0')));
  outputLengthLabel->setText(QString("%1").arg(outputData.length(), 6, 10, QChar('0')));
  if (processor->getCounter() >= processor->getProgram().size()) {
    runButton->setEnabled(false);
    stepButton->setEnabled(false);
  } else {
    runButton->setEnabled(true);
    stepButton->setEnabled(true);
  }
}

void MainWidget::run() {
  inputDataEdit->setReadOnly(true);
  if (!isRunning) {
    runButton->setText("Pause");
    RunningThread* thread = new RunningThread(processor);
    connect(thread, SIGNAL(finished()), this, SLOT(threadFinishied()));
    thread->start();
    isRunning = true;
    pcLabel->setEnabled(false);
    registerWidget->setEnabled(false);
    stepButton->setEnabled(false);
  } else {
    processor->stop();
  }
}

void MainWidget::threadFinishied() {
  isRunning = false;
  runButton->setText("Run");
  stepButton->setEnabled(true);
  qDebug("finished");
  syncStatus();
  registerWidget->setEnabled(true);
  pcLabel->setEnabled(true);
}

void MainWidget::step() {
  inputDataEdit->setReadOnly(true);
  if (processor->getCounter() < processor->getProgram().size()) {
    (*processor->getProgram()[processor->getCounter()])(processor);
  }
  syncStatus();
}

void MainWidget::browseInputFile() {
  QString fileName = QFileDialog::getOpenFileName(this);
  if (fileName.isEmpty()) {
    return;
  }
  QFile file(fileName);
  if (!file.open(QFile::ReadOnly)) {
    QMessageBox::warning(this, "Error", QString("Couldn't open %1:\n%2.").arg(fileName).arg(file.errorString()));
    return;
  }
  QByteArray barray = file.readAll();
  inputDataEdit->setText(QString::fromUtf8(barray.constData()));
}

void MainWidget::inputChanged() {
  inputData.clear();
  inputData.append(inputDataEdit->toPlainText());
  new(&inputStream) QDataStream(inputData);
  processor->setInput(&inputStream);

  outputData.clear();
  new(&outputStream) QDataStream(&outputData, QIODevice::WriteOnly);
  processor->setOutput(&outputStream);
  qDebug("setted Input");
}

void MainWidget::reset() {
  processor->reset();
  inputChanged();
  inputDataEdit->setReadOnly(false);
  syncStatus();
}

void MainWidget::viewOutput() {
  QMessageBox::information(this, "Output", QString::fromUtf8(outputData.constData()));
}

void MainWidget::saveOutput() {
  QString fileName = QFileDialog::getSaveFileName(this);
  if (fileName.isEmpty()) {
    return;
  }
  QFile file(fileName);
  if (!file.open(QFile::WriteOnly)) {
    QMessageBox::warning(this, "Error", QString("Couldn't open %1:\n%2.").arg(fileName).arg(file.errorString()));
    return;
  }
  file.write(outputData);
  file.close();
  QMessageBox::information(this, "Info", QString("Saved output to %1.").arg(fileName));
}

void MainWidget::editBp() {
  if (breakPoint != -1) {
    processor->getProgram()[breakPoint]->setBreakPoint(false);
  }
  bool ok;
  breakPoint = QInputDialog::getInteger(this, tr("Break point"), tr("Stop After: "), breakPoint, -1, processor->getProgram().size()-1, 1, &ok);
  if (ok && breakPoint != -1) {
    qDebug("set %d as bp", breakPoint);
    processor->getProgram()[breakPoint]->setBreakPoint(true);
  }
}
