#include <QtCore>
#include <iostream>
#include <processor.h>
#include <instruction.h>

using namespace ugl0sim;

class MainThread : public QThread {
 public:
  MainThread(const QCoreApplication& app, Processor* proc)
    : app(&app), proc(proc) {
  }

  void run() {
    connect(this, SIGNAL(finished()), qApp, SLOT(quit()));
    const QStringList& args = app->arguments();
    QFile program_file(args.at(1));

    program_file.open(QFile::ReadOnly);
    if(!program_file.isReadable()) {
      qDebug("Coundn't open %s", args.at(1).toUtf8().constData());
      return;
    }

    Processor& processor = *proc;

    QDataStream program_stream(&program_file);
    program_stream.setByteOrder(QDataStream::BigEndian);

    Program program;

    qDebug("parsing...");

    int i;
    for (i = 0; !program_stream.atEnd(); ++i) {
      unsigned int instruction;
      program_stream >> instruction;
      processor.getMemory()->setWord(i, instruction);
      Instruction* inst = newInstruction(instruction);
      if (inst == NULL) {
        qDebug("Unable parse %08x at %d", instruction, program.size());
//        return;
      } else {
//        qDebug("%03d: %s", program.size());//, inst->getDescription().toUtf8());
        qDebug() << program.size() << inst->getDescription();
      }
      program.push_back(inst);
    }
    processor.getMemory()->inst_size_ = i;

    program_file.close();

    QFile input, output;
    input.open(stdin, QFile::ReadOnly);
    output.open(stdout, QFile::WriteOnly);

    QDataStream inputStream(&input), outputStream(&output);
    processor.setInput(&inputStream);
    processor.setOutput(&outputStream);

    connect(qApp, SIGNAL(unixSignal(int)), &processor, SLOT(stop()));

    qDebug("running...");
    processor.loadProgram(program);

    processor.run();

    for (int i = 0; i < 32; ++i) {
      int r = processor.getRegister(i);
      qDebug("r[%02x] = %08x (%d)", i, r, r);
    }
    for (int i = 0; i < 32; ++i) {
      int r = processor.getRegister(i | 0x20);
      qDebug("f[%02x] = %08x (%e)", i, r, *((float*)&r));
    }

    output.flush();

    return;
  }
 private:
  const QCoreApplication* app;
  Processor* proc;
};

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Processor processor;

    MainThread thread(a, &processor);
    thread.start();

//    usleep(100000);
//    qDebug("stopping");
//    processor.stop();;

    QObject::connect(&thread, SIGNAL(destroyed()), &processor, SLOT(deleteLater()));

    return a.exec();
}
